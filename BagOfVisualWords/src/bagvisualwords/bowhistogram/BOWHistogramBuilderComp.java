package bagvisualwords.bowhistogram;

import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JOptionPane;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import visualizationbasics.util.Util;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Bag of Visual Words",
name = "Histogram Builder",
description = "Creates a data set with frequency of visual words.")
public class BOWHistogramBuilderComp implements AbstractComponent {

    public void execute() throws IOException {

        float[] frequency = null;
        frequencyData = new DenseMatrix();
        float klass = 0.0f;
        ArrayList<String> ids = new ArrayList<String>();
        ArrayList<String> atts = new ArrayList<String>();
        int id = 0;

        //for each image, compute the frequency of each visual word...
        //if (VisualWordsDictionaryFilename != null && !VisualWordsDictionaryFilename.isEmpty() && filesKeyPoints != null && !filesKeyPoints.isEmpty()) {
            //AbstractMatrix dictionaryMatrix = MatrixFactory.getInstance(VisualWordsDictionaryFilename);
            //dictionaryMatrix = MatrixFactory.getInstance(VisualWordsDictionaryFilename);
            if (dictionaryMatrix != null) {
                Iterator it = filesKeyPoints.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry e = (Map.Entry) it.next();
                    ArrayList<AbstractMatrix> classWords = (ArrayList<AbstractMatrix>)e.getValue();
                    for (int i=0;i<classWords.size();i++) {
                        AbstractMatrix imageKeyPoints = classWords.get(i);
                        frequency = new float[dictionaryMatrix.getRowCount()];
                        for (int j=0;j<imageKeyPoints.getRowCount();j++) {
                            //get the correspondent visual word...
                            int index = getVisualWordIndex(imageKeyPoints.getRow(j),dictionaryMatrix);
                            if (index >= 0 && index < dictionaryMatrix.getRowCount())
                                //updating frequency...
                                frequency[index] = frequency[index] + 1;
                        }
                        int idInstance = 0;
                        if (labels != null && !labels.isEmpty()) {
                            if (Util.isParsableToInt(labels.get(id)))
                                idInstance = Integer.parseInt(labels.get(id));
                            else
                                idInstance = Util.convertToInt(labels.get(id));
                        }else
                            idInstance = id;
                        frequencyData.addRow(new DenseVector(frequency,idInstance,klass));
                        //frequencyData.addRow(new DenseVector(frequency,klass));
                        ids.add(Integer.toString(id));
                        id++;
                    }
                    klass++;
                }
                if (labels != null && !labels.isEmpty())
                    frequencyData.setLabels(labels);
                else
                    frequencyData.setLabels(ids);
                for (int i=0;i<dictionaryMatrix.getRowCount();i++)
                    atts.add("attr"+i);
                frequencyData.setAttributes(atts);
            }else {
                JOptionPane.showMessageDialog(null,"Visual Words Dictionary not informed!");
                return;
            }

        //}
    }

    private int getVisualWordIndex(AbstractVector visualWord, AbstractMatrix dicMatrix) {
        AbstractDissimilarity diss = DissimilarityFactory.getInstance(DissimilarityType.EUCLIDEAN);
        float smallestDistance = Float.MAX_VALUE;
        int smallestIndex = -1;
        for (int i=0;i<dicMatrix.getRowCount();i++) {
            AbstractVector w = dicMatrix.getRow(i);
            float d = diss.calculate(visualWord,w);
            if (d < smallestDistance) {
                smallestDistance = d;
                smallestIndex = i;
            }
        }
        return smallestIndex;
    }

    public AbstractParametersView getParametersEditor() {
//        if (paramview == null) {
//            paramview = new BOWHistogramBuilderParamView(this);
//        }
//        return paramview;
        return null;
    }

    public void reset() {
        filesKeyPoints = null;
        frequencyData = null;
    }

    public void input(@Param(name = "Words Set") TreeMap<String,ArrayList<AbstractMatrix>> filesKeyPoints) {
        this.filesKeyPoints = filesKeyPoints;
    }

    public void input(@Param(name = "Labels Set") ArrayList<String> labels) {
        this.labels = labels;
    }

    public void input(@Param(name = "Visual Words Dictionary") AbstractMatrix m) {
        this.dictionaryMatrix = m;
    }
    
    public AbstractMatrix output() {
        return frequencyData;
    }

//    public void setVisualWordsDictionary(String f) {
//        VisualWordsDictionaryFilename = f;
//    }
//
//    public String getVisualWordsDictionary() {
//        return VisualWordsDictionaryFilename;
//    }

    public static final long serialVersionUID = 1L;
    private transient TreeMap<String,ArrayList<AbstractMatrix>> filesKeyPoints;
    private transient ArrayList<String> labels;
    private transient AbstractMatrix dictionaryMatrix;
    //private transient AbstractMatrix dicMatrix;
    private transient AbstractMatrix frequencyData;
//    private String VisualWordsDictionaryFilename;
//    private transient BOWHistogramBuilderParamView paramview;
    
}
