package bagvisualwords.input;

import featureextraction.util.FeatureExtractionUtil;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;
import javax.swing.JOptionPane;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import visualizationbasics.util.PropertiesManager;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Bag of Visual Words",
name = "Images KeyPoints Reader",
description = "Reads a set of images keypoints.")
public class ImageKeyPointsDataSetReaderComp implements AbstractComponent {

    public void execute() throws IOException {

        //Verifying if user informed a directory or a zip file...
        File source = null;
        if (filename.endsWith(".zip")) {
            PropertiesManager pm = PropertiesManager.getInstance("projection.properties");
            pm.setProperty("UNZIP.DIR", System.getProperty("user.dir") + "\\tempDir");
            pm.setProperty("FEATURE.DIR", System.getProperty("user.dir") + "\\lib\\featureSelection");
            String unzipDir = pm.getProperty("UNZIP.DIR");
            if (unzipDir != null) source = new File(unzipDir);
            FeatureExtractionUtil.deleteFiles(unzipDir);
            FeatureExtractionUtil.unzipFiles(pm,filename);
        }else {
            source = new File(filename);
            if (source == null || !source.isDirectory()) {
                JOptionPane.showMessageDialog(null,"Keypoints filename invalid (Not a zip nor a directory!");
                return;
            }
        }

        filesKeyPoints = new TreeMap<String,ArrayList<AbstractMatrix>>();
        TreeMap<String,ArrayList<String>> labelsPerKlass = new TreeMap<String,ArrayList<String>>();

        for (File f: source.listFiles()) {
            if (f.isFile()) {
                int begin = f.getName().lastIndexOf("_");
                String ini = "";
                if (begin > -1)
                    ini = f.getName().substring(0, begin);
                else {
                    ini = f.getName();
                    if (f.getName().length() > 2)
                        ini = f.getName().substring(0, 2);
                }
                if (!filesKeyPoints.containsKey(ini)) {
                    filesKeyPoints.put(ini,new ArrayList<AbstractMatrix>());
                    labelsPerKlass.put(ini,new ArrayList<String>());
                }
                AbstractMatrix fileKeyPoints = MatrixFactory.getInstance(f.getCanonicalPath());
                filesKeyPoints.get(ini).add(fileKeyPoints);
                //Verifying the images extensions...
                String fn = f.getName();
                if (fn.lastIndexOf(".") != -1)
                    fn = fn.substring(0,fn.lastIndexOf("."));
                fn = fn+".jpg";
                labelsPerKlass.get(ini).add(fn);
            }
        }
        if (!labelsPerKlass.isEmpty()) {
            labels = new ArrayList<String>();
            for (ArrayList<String> l : labelsPerKlass.values()) {
                for (int i=0;i<l.size();i++)
                    labels.add(l.get(i));
            }
        }
    }

    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new ImageKeyPointsDataSetReaderParamView(this);
        }
        return paramview;
//        return null;
    }

    public void reset() {
        filesKeyPoints = null;
    }
    
    public TreeMap<String,ArrayList<AbstractMatrix>> output() {
        return filesKeyPoints;
    }

    public ArrayList<String> outputLabels() {
        return labels;
    }

    public void setFilename(String f) {
        filename = f;
    }

    public String getFilename() {
        return filename;
    }

    public static final long serialVersionUID = 1L;
    private transient TreeMap<String,ArrayList<AbstractMatrix>> filesKeyPoints;
    private transient ArrayList<String> labels;
    private String filename;
    private transient ImageKeyPointsDataSetReaderParamView paramview;
    
}
