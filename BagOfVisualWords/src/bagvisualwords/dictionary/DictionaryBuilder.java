/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bagvisualwords.dictionary;

import datamining.clustering.BKmeans;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class DictionaryBuilder {

    /*
    Para cada imagem, pego os 50 clusters, que representarão as palavras que aparecem nelas.
    Cada centroide desses clusters representará uma palavra propriamente dita. Assim, para a classe,
    terei n x 50 palavras (que serão so n x 50 centroides), onde n é o número de imagens daquela classe.
    Clusterizo novamente (50 clusters), e consigo 50 palavras visuais que aparecem naquela classe (centroides dos clusters)

    Para cada classe, pego as 10 palavras que mais aparecem (os maiores clusters)
    Verifico se existem palavras comuns neste conjunto, pois elas me atrapalharão ao final do processo.
    Ao retirar as palavras comuns, as que restarem representarão o dicionário de palavras visuais.
    */

    public DictionaryBuilder(int numVisualWords,boolean equalNumWordsPerClass,DissimilarityType disstype, boolean aleatoryWords) {
        this.numVisualWords = numVisualWords;
        this.equalNumWordsPerClass = equalNumWordsPerClass;
        this.diss = DissimilarityFactory.getInstance(disstype);
        this.aleatoryWords = aleatoryWords;
    }

    public HashMap<Integer,Integer> mapClusterSize = new HashMap<Integer,Integer>();

    public LinkedHashMap sortHashMapByValues(HashMap passedMap) {

        List mapKeys = new ArrayList(passedMap.keySet());
        List mapValues = new ArrayList(passedMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);
        Collections.reverse(mapValues);
        LinkedHashMap someMap = new LinkedHashMap();
        Iterator valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Object val = valueIt.next();
            Iterator keyIt = mapKeys.iterator();
            while (keyIt.hasNext()) {
                Object key = keyIt.next();
                if (passedMap.get(key).toString().equals(val.toString())) {
                    passedMap.remove(key);
                    mapKeys.remove(key);
                    someMap.put(key, val);
                    break;
                }
            }
        }
        return someMap;

    }

    public AbstractMatrix mergeCentroids(ArrayList<CentroidList> centroids) {

        if (centroids == null || centroids.isEmpty()) return null;
        int k = 0;
        AbstractMatrix resultMatrix = new DenseMatrix();
        for (int i=0;i<centroids.size();i++) {
            CentroidList centroid = centroids.get(i);
            resultMatrix.getRows().addAll(centroid.centroids);
            for (int j=0;j<centroid.centroids.size();j++) {
                centroid.num.set(j,k);
                k++;
            }
        }
        return resultMatrix;

    }

    public CentroidList getCentroidList(ArrayList<CentroidList> centroids, int num) {

        for (int i=0;i<centroids.size();i++) {
            CentroidList c = centroids.get(i);
            //Como os centroides estao ordenados em cada CentroidList na lista, entao posso
            //procurar apenas no CentroidList cujo maior indice eh maior ou igual ao indice sendo procurado
            if (c.num.get(c.num.size()-1) >= num)
                return c;
        }
        return null;

    }

    public CentroidList createClassDictionary(AbstractMatrix matrix, int n) throws IOException {

        BKmeans km = null;
        CentroidList finalCentroidList = null;
        
        ArrayList<ArrayList<Integer>> clusters = null;
        
        if (matrix != null) {
            km = new BKmeans(n);
            //clusterCentroids contem os clusters da classe inteira...
            clusters = km.execute(diss,matrix);
            AbstractMatrix centroids = km.getCentroids();
            //Agora vou contabilizar o tamanho de cada cluster...
            HashMap<AbstractVector,Integer> sizeClusters = new HashMap<AbstractVector,Integer>();
            for (int i=0;i<clusters.size();i++)
                sizeClusters.put(centroids.getRow(i),clusters.get(i).size());
            
            //Ordenando essa lista pelo tamanho dos clusters (maior -> menor)
            LinkedHashMap sortedClusters = sortHashMapByValues(sizeClusters);
            finalCentroidList = new CentroidList();
            if (sortedClusters != null) {
                //Criando uma lista dos centroides desta classe, e os tamanhos de seus clusters...
                Iterator it = sortedClusters.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry e = (Map.Entry) it.next();
                    AbstractVector key = (AbstractVector) e.getKey();
                    int value = (Integer)e.getValue();
                    finalCentroidList.centroids.add(key);
                    finalCentroidList.sizes.add(value);
                }
            }
        }
        return finalCentroidList;
    }

    public CentroidList createClassDictionary(File dir, int n) throws IOException {

        BKmeans km = null;
        ArrayList<ArrayList<Integer>> clusters = null;
        ArrayList<CentroidList> centroids = new ArrayList<CentroidList>();
        CentroidList finalCentroidList = null;

        //Vou fazer o agrupamento de cada imagem do diretorio da classe...
        if (dir != null & dir.isDirectory()) {
            File[] files = dir.listFiles();
            for (int i=0;i<files.length;i++) {
                File file = files[i];
                //Cada imagem esta representada por um .data, contendo todos os descritores. Eles serao agrupados em n palavras.
                AbstractMatrix matrix = MatrixFactory.getInstance(file.getCanonicalPath());
                if (matrix != null) {
                    km = new BKmeans(n);
                    clusters = km.execute(diss,matrix);
                    CentroidList cl = new CentroidList();
                    AbstractMatrix centroidsMatrix = km.getCentroids();
                    //Criando um objeto CentroidList, contendo cada centroide e seu respectivo tamanho.
                    if (centroidsMatrix != null) {
                        for (int k=0;k<centroidsMatrix.getRowCount();k++) {
                            cl.centroids.add(centroidsMatrix.getRow(k));
                            cl.sizes.add(clusters.get(k).size());
                            cl.num.add(0);
                        }
                    }
                    //Adicionando na lista de centroids. Essa lista contem todos os centroides das imagens desse diretorio.
                    centroids.add(cl);
                }
            }
            //Agora eu tenho todos os n centroides das m imagens da classe. Vou formar uma matrix com n * m centroides.
            AbstractMatrix centroidsMatrix = mergeCentroids(centroids);
            ArrayList<ArrayList<Integer>> clusterCentroids = null;
            if (centroidsMatrix != null) {
                km = new BKmeans(n);
                //clusterCentroids contem os clusters da classe inteira...
                clusterCentroids = km.execute(diss,centroidsMatrix);
                AbstractMatrix centroidClusterCentroids = km.getCentroids();
                //Agora vou contabilizar o tamanho de cada cluster...
                HashMap<Integer,Integer> sizeClusters = new HashMap<Integer,Integer>();
                for (int i=0;i<clusterCentroids.size();i++) {
                    int totalSize = 0;
                    for (int j=0;j<clusterCentroids.get(i).size();j++) {
                        CentroidList c = getCentroidList(centroids,clusterCentroids.get(i).get(j));
                        if (c != null)
                            totalSize += c.sizes.get(clusterCentroids.get(i).get(j)-c.num.get(0));
                    }
                    sizeClusters.put(i,totalSize);
                }
                //Ordenando essa lista pelo tamanho dos clusters (maior -> menor)
                LinkedHashMap sortedClusters = sortHashMapByValues(sizeClusters);
                finalCentroidList = new CentroidList();
                if (sortedClusters != null) {
                    //Criando uma lista dos centroides desta classe, e os tamanhos de seus clusters...
                    Iterator it = sortedClusters.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry e = (Map.Entry) it.next();
                        int key = (Integer) e.getKey();
                        int value = (Integer)e.getValue();
                        finalCentroidList.centroids.add(centroidClusterCentroids.getRow(key));
                        finalCentroidList.sizes.add(value);
                    }
                }
            }
        }
        return finalCentroidList;
    }

    public String getFilePath(File directory, String instance) throws IOException {
        if (directory != null) {
            File[] files = directory.listFiles();
            for (int i=0;i<files.length;i++) {
                if (files[i].isDirectory()) {
                    String path = getFilePath(files[i],instance);
                    if (!path.isEmpty()) return path;
                } else if (files[i].getCanonicalPath().contains(instance))
                    return files[i].getCanonicalPath();
            }
        }
        return "";
    }

    public CentroidList createClassDictionary(ArrayList<String> files, String directory, int n) throws IOException {

        BKmeans km = null;
        ArrayList<ArrayList<Integer>> clusters = null;
        ArrayList<CentroidList> centroids = new ArrayList<CentroidList>();
        CentroidList finalCentroidList = null;

        //Vou fazer o agrupamento de cada imagem do diretorio da classe...
        if (files != null && !files.isEmpty()) {
            for (int i=0;i<files.size();i++) {
                String fileName = files.get(i);
                if (fileName.contains(".")) {
                    int ind = fileName.lastIndexOf(".");
                    if (ind != -1) fileName = fileName.substring(0,ind);
                }
                String filePath = getFilePath(new File(directory),fileName);
                if (filePath != null && !filePath.isEmpty()) {
                    //Cada imagem esta representada por um .data, contendo todos os descritores. Eles serao agrupados em n palavras.
                    System.out.println(filePath);
                    AbstractMatrix matrix = MatrixFactory.getInstance(filePath);
                    if (matrix != null) {
                        km = new BKmeans(n);
                        clusters = km.execute(diss,matrix);
                        CentroidList cl = new CentroidList();
                        AbstractMatrix centroidsMatrix = km.getCentroids();
                        //Criando um objeto CentroidList, contendo cada centroide e seu respectivo tamanho.
                        if (centroidsMatrix != null) {
                            for (int k=0;k<centroidsMatrix.getRowCount();k++) {
                                cl.centroids.add(centroidsMatrix.getRow(k));
                                cl.sizes.add(clusters.get(k).size());
                                cl.num.add(0);
                            }
                        }
                        //Adicionando na lista de centroids. Essa lista contem todos os centroides das imagens desse diretorio.
                        centroids.add(cl);
                    }
                }
            }
            //Agora eu tenho todos os n centroides das m imagens da classe. Vou formar uma matrix com n * m centroides.
            AbstractMatrix centroidsMatrix = mergeCentroids(centroids);
            ArrayList<ArrayList<Integer>> clusterCentroids = null;
            if (centroidsMatrix != null) {
                km = new BKmeans(n);
                //clusterCentroids contem os clusters da classe inteira...
                clusterCentroids = km.execute(diss,centroidsMatrix);
                AbstractMatrix centroidClusterCentroids = km.getCentroids();
                //Agora vou contabilizar o tamanho de cada cluster...
                HashMap<Integer,Integer> sizeClusters = new HashMap<Integer,Integer>();
                for (int i=0;i<clusterCentroids.size();i++) {
                    int totalSize = 0;
                    for (int j=0;j<clusterCentroids.get(i).size();j++) {
                        CentroidList c = getCentroidList(centroids,clusterCentroids.get(i).get(j));
                        if (c != null)
                            totalSize += c.sizes.get(clusterCentroids.get(i).get(j)-c.num.get(0));
                    }
                    sizeClusters.put(i,totalSize);
                }
                //Ordenando essa lista pelo tamanho dos clusters (maior -> menor)
                LinkedHashMap sortedClusters = sortHashMapByValues(sizeClusters);
                finalCentroidList = new CentroidList();
                if (sortedClusters != null) {
                    //Criando uma lista dos centroides desta classe, e os tamanhos de seus clusters...
                    Iterator it = sortedClusters.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry e = (Map.Entry) it.next();
                        int key = (Integer) e.getKey();
                        int value = (Integer)e.getValue();
                        finalCentroidList.centroids.add(centroidClusterCentroids.getRow(key));
                        finalCentroidList.sizes.add(value);
                    }
                }
            }
        }
        return finalCentroidList;
    }

    public CentroidList createClassDictionary(ArrayList<AbstractMatrix> files, int n) throws IOException {

        BKmeans km = null;
        ArrayList<ArrayList<Integer>> clusters = null;
        ArrayList<CentroidList> CentroidListImagesClass = new ArrayList<CentroidList>();
        CentroidList finalCentroidList = null;

        //Vou fazer o agrupamento de cada imagem do diretorio da classe...
        if (files != null && !files.isEmpty()) {
            for (int i=0;i<files.size();i++) {
                km = new BKmeans(n);
                clusters = km.execute(diss,files.get(i));
                CentroidList centroidListImage = new CentroidList();
                AbstractMatrix centroidsMatrix = km.getCentroids();
                //Criando um objeto CentroidList para a imagem, contendo cada centroide e seu respectivo tamanho.
                if (centroidsMatrix != null) {
                    for (int k=0;k<centroidsMatrix.getRowCount();k++) {
                        centroidListImage.centroids.add(centroidsMatrix.getRow(k));
                        centroidListImage.sizes.add(clusters.get(k).size());
                        centroidListImage.num.add(0);
                    }
                }
                //Adicionando na lista de centroids. Essa lista contem todos os centroides das imagens desse diretorio.
                CentroidListImagesClass.add(centroidListImage);
            }
            //Agora eu tenho todos os n centroides das m imagens da classe. Vou formar uma matrix com n * m centroides.
            AbstractMatrix mergedCentroidsListImagesClass = mergeCentroids(CentroidListImagesClass);
            ArrayList<ArrayList<Integer>> clusterCentroids = null;
            if (mergedCentroidsListImagesClass != null) {
                km = new BKmeans(n);
                //clusterCentroids contem todos os centroids de todas as imagens da classe...
                clusterCentroids = km.execute(diss,mergedCentroidsListImagesClass);
                AbstractMatrix centroidClusterCentroids = km.getCentroids();
                //Agora vou contabilizar o tamanho de cada cluster...
                HashMap<Integer,Integer> sizeClusters = new HashMap<Integer,Integer>();
                for (int i=0;i<clusterCentroids.size();i++) {
                    int totalSize = 0;
                    for (int j=0;j<clusterCentroids.get(i).size();j++) {
                        CentroidList c = getCentroidList(CentroidListImagesClass,clusterCentroids.get(i).get(j));
                        if (c != null)
                            totalSize += c.sizes.get(clusterCentroids.get(i).get(j)-c.num.get(0));
                    }
                    sizeClusters.put(i,totalSize);
                }
                //Ordenando essa lista pelo tamanho dos clusters (maior -> menor)
                LinkedHashMap sortedClusters = sortHashMapByValues(sizeClusters);
                finalCentroidList = new CentroidList();
                if (sortedClusters != null) {
                    //Criando uma lista dos centroides desta classe, e os tamanhos de seus clusters...
                    Iterator it = sortedClusters.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry e = (Map.Entry) it.next();
                        int key = (Integer) e.getKey();
                        int value = (Integer)e.getValue();
                        finalCentroidList.centroids.add(centroidClusterCentroids.getRow(key));
                        finalCentroidList.sizes.add(value);
                    }
                }
            }
        }
        return finalCentroidList;
    }

    public void build(AbstractMatrix dataset,String outputPath) throws IOException {

        //Dividindo as instancias do dataset em matrizes separadas por classes...
        ArrayList<AbstractMatrix> matrices = new ArrayList<AbstractMatrix>();
        ArrayList<Float> index = new ArrayList<Float>();

        for (int i=0;i<dataset.getRowCount();i++) {
            AbstractVector instance = dataset.getRow(i);
            if (instance != null) {
                if (index.contains(instance.getKlass())) {
                    AbstractMatrix m = matrices.get(index.indexOf(instance.getKlass()));
                    if (m != null)
                        m.addRow(instance);
                }else {
                    AbstractMatrix m = new DenseMatrix();
                    m.addRow(instance);
                    matrices.add(m);
                    index.add(instance.getKlass());
                }
            }
        }

        CentroidList words = new CentroidList();
        //Criando, para cada classe, um conjunto de clusters, representando as n palavras visuais de cada classe...
        //Estas n palavras vao ser adicionadas na variavel words
        for (int i=0;i<matrices.size();i++) {
            AbstractMatrix matrix = matrices.get(i);
            if (matrix != null) {
                CentroidList classWords = createClassDictionary(matrix,numVisualWords);
                words.centroids.addAll(classWords.centroids);
                words.sizes.addAll(classWords.sizes);
                for (int k=0;k<classWords.centroids.size();k++) words.num.add(i);
            }
        }
        int numVisualWordsPerClass = 0;
        if (equalNumWordsPerClass && !matrices.isEmpty())
                numVisualWordsPerClass = numVisualWords/matrices.size();

        words.filter(DissimilarityFactory.getInstance(DissimilarityType.EUCLIDEAN),numVisualWords,numVisualWordsPerClass);
        words.save(outputPath);
    }

    public void build(String directory, String outputPath) throws IOException {

        CentroidList words = new CentroidList();
        File dir = new File(directory);
        
        if (dir != null && dir.isDirectory()) {
            File[] directories = dir.listFiles();
            int contDirectories = 0;
            for (int i=0;i<directories.length;i++) {
                if (directories[i].isDirectory()) contDirectories++;
                CentroidList classWords = createClassDictionary(directories[i],numVisualWords);
                words.centroids.addAll(classWords.centroids);
                words.sizes.addAll(classWords.sizes);
                for (int k=0;k<classWords.centroids.size();k++) words.num.add(i);
            }
            int numVisualWordsPerClass = 0;
            if (equalNumWordsPerClass && contDirectories != 0)
                numVisualWordsPerClass = numVisualWords/contDirectories;
            words.filter(DissimilarityFactory.getInstance(DissimilarityType.EUCLIDEAN),numVisualWords,numVisualWordsPerClass);
            words.save(outputPath);
        }

    }

    public AbstractMatrix build(TreeMap<String,ArrayList<AbstractMatrix>> classes) throws IOException {

        CentroidList words = new CentroidList();
        int numVisualWordsPerClass = 0;
        AbstractMatrix result = null;
        if (classes != null) {
            if (equalNumWordsPerClass && !classes.isEmpty())
                numVisualWordsPerClass = numVisualWords/classes.size();
            Iterator it = classes.entrySet().iterator();
            int klass = 0;
            while (it.hasNext()) {
                Map.Entry e = (Map.Entry) it.next();
                //Float key = (Float) e.getKey();
                ArrayList<AbstractMatrix> value = (ArrayList<AbstractMatrix>) e.getValue();
                CentroidList classWords = createClassDictionary(value,numVisualWords);
                words.centroids.addAll(classWords.centroids);
                words.sizes.addAll(classWords.sizes);
                //for (int k=0;k<classWords.centroids.size();k++) words.num.add(key.intValue());
                for (int k=0;k<classWords.centroids.size();k++) words.num.add(klass);
                klass++;
            }
            words.filter(DissimilarityFactory.getInstance(DissimilarityType.EUCLIDEAN),numVisualWords,numVisualWordsPerClass);
            result = words.createMatrix();
        }
        return result;
        
    }

    public static class CentroidList {

        public CentroidList() {
            num = new ArrayList<Integer>();
            centroids = new ArrayList<AbstractVector>();
            sizes = new ArrayList<Integer>();
        }

        public ArrayList<Integer> num;
        public ArrayList<AbstractVector> centroids;
        public ArrayList<Integer> sizes;

        public void filter(AbstractDissimilarity diss, int numWords, int numWordsPerClass) {

            float threshold = 0.005f;
            //Percorro todos os centroides, verificando se ha repeticoes. As repeticoes seo sinalizadas por distancias
            //entre os centroides menores que um threshold. Neste caso, o centroide que possuir mais individuos permanece
            //no vetor. Caso os dois possuam o mesmo numero de elementos, os dois serao retirados, pois neste caso essa
            //palavra nao distingue bem as duas classes.
            for (int i=0;i<centroids.size();i++) {
                AbstractVector c1 = centroids.get(i);
                for (int j=i+1;j<centroids.size();j++) {
                    AbstractVector c2 = centroids.get(j);
                    float distance = diss.calculate(c1,c2);
                    if (distance < threshold) {
                       if (sizes.get(i) > sizes.get(j)) {
                           num.remove(j);
                           centroids.remove(j);
                           sizes.remove(j);
                           j--;
                       }else {// if (sizes.get(i) < sizes.get(j)) {
                           num.remove(i);
                           centroids.remove(i);
                           sizes.remove(i);
                           i--;
                           break;
                       //}else {
                           //Neste caso, o ideal eh excluir os dois. TODO
                       //}
                        }
                    }
                }
            }
            //Deixando apenas numWordsPerClass palavras de cada classe...
            if (numWords < centroids.size() && numWordsPerClass != 0) {
                int currentClass = num.get(0);
                int cont = 1;
                for (int i=0;i<centroids.size();i++) {
                    if (num.get(i) != currentClass) {
                        cont = 2;
                        currentClass = num.get(i);
                    }else if (cont > numWordsPerClass) {
                        centroids.remove(i);
                        sizes.remove(i);
                        num.remove(i);
                        i--;
                    }else cont++;
                }
            }
            //Verificando se ainda existem mais palavras do que o maximo permitido. Se isso ocorrer, retiro palavras
            //aleatorias ate que o numero seja alcancado...
            while (numWords < centroids.size()) {
                Random r = new Random();
                int i = r.nextInt(centroids.size());
                centroids.remove(i);
                sizes.remove(i);
                num.remove(i);
            }

    }

        public void save(String file) {
            String eol = System.getProperty("line.separator");
            String resultado = centroids.size()+" "+centroids.get(0).getValues().length +eol + " ";
            //int n = centroids.size() < 1 ? centroids.size() : 1;
            for (int i=0;i<centroids.size();i++) {
                for (int j=0;j<centroids.get(i).getValues().length-1;j++)
                    resultado += centroids.get(i).getValue(j) + " ";
                resultado += centroids.get(i).getValue(centroids.get(i).getValues().length-1) + eol + " ";
            }
            for (int j=0;j<centroids.get(centroids.size()-1).getValues().length-1;j++)
                    resultado += centroids.get(centroids.size()-1).getValue(j) + " ";
                resultado += centroids.get(centroids.size()-1).getValue(centroids.get(centroids.size()-1).getValues().length-1);
            try{
                BufferedWriter bw = new BufferedWriter(new FileWriter(new File(file)));
                bw.write(resultado);
                bw.flush();
                bw.close();
            } catch(IOException e){
                e.printStackTrace();
            }
        }

        //This method creates an abstractmatrix using the centroids (each line is a centroid)
        public AbstractMatrix createMatrix() {
            AbstractMatrix result = new DenseMatrix();
            ArrayList<String> labels = new ArrayList<String>();
            for (int i=0;i<centroids.size();i++) {
                float[] values = centroids.get(i).getValues();
                AbstractVector row = new DenseVector(values);
                result.addRow(row);
                labels.add(Integer.toString(i));
            }
            result.setLabels(labels);
            return result;
        }

    }

    private int numVisualWords;
    private boolean equalNumWordsPerClass;
    private AbstractDissimilarity diss = DissimilarityFactory.getInstance(DissimilarityType.EUCLIDEAN);
    private boolean aleatoryWords;

}
