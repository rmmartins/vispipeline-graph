package bagvisualwords.dictionary;

import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import matrix.AbstractMatrix;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Bag of Visual Words",
name = "Dictionary Builder",
description = "Creates a Bag of Visual Words Dictionary.")
public class DictionaryBuilderComp implements AbstractComponent {

    public void execute() throws IOException {
        DictionaryBuilder db = new DictionaryBuilder(numWordsDictionary,equalNumWordsPerClass,disstype,aleatoryWords);
        dictionary = db.build(keywordsSet);
    }

    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new DictionaryBuilderParamView(this);
        }
        return paramview;
    }

    public void reset() {
        this.keywordsSet = null;
    }
    
    public void input(@Param(name = "Dataset Keywords") TreeMap<String,ArrayList<AbstractMatrix>> keywordsSet) {
        this.keywordsSet = keywordsSet;
    }

    public AbstractMatrix output() {
        return dictionary;
    }

//    public void input(@Param(name = "List of words per class") HashMap<Float,ArrayList<String>> files) {
//        this.files = files;
//    }

    public int getNumWordsDictionary() {
        return numWordsDictionary;
    }

    public boolean isEqualNumWordsPerClass() {
        return equalNumWordsPerClass;
    }

    public String getOutputDictionary() {
        return outputDictionary;
    }

    public String getInputDirectory() {
        return inputDirectory;
    }

    public void setNumWordsDictionary(int n) {
        numWordsDictionary = n;
    }

    public void setequalNumWordsPerClass(boolean op) {
        equalNumWordsPerClass = op;
    }

    public void setOutputDictionary(String text) {
        outputDictionary = text;
    }

    public void setInputDirectory(String text) {
        inputDirectory = text;
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public boolean isAleatoryWords() {
        return aleatoryWords;
    }

    public void setAleatoryWords(boolean aleatoryWords) {
        this.aleatoryWords = aleatoryWords;
    }

    public static final long serialVersionUID = 1L;
    private transient DictionaryBuilderParamView paramview;
    private transient TreeMap<String,ArrayList<AbstractMatrix>> keywordsSet;
    private transient AbstractMatrix dictionary;

    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private int numWordsDictionary;
    private boolean equalNumWordsPerClass;
    private String inputDirectory;
    private String outputDictionary;

    private HashMap<Float,ArrayList<String>> files;
    private boolean aleatoryWords;

}
