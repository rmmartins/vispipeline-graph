/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labeledgraph.model.xml;

import graph.model.xml.XMLGraphModelReader;
import labeledgraph.model.LabeledGraphModel;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import labeledgraph.model.LabeledGraphInstance;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import projection.model.Scalar;
import visualizationbasics.model.AbstractModel;
import visualizationbasics.util.Util;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class XMLLabeledGraphModelReader extends XMLGraphModelReader {
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);

        if (qName.equalsIgnoreCase(SCALAR)) {
            String name = attributes.getValue(NAME);
            String value = attributes.getValue(VALUE);

            if (name != null && value != null) {
                Scalar s = model.addScalar(name);
                tmpinstance.setScalarValue(s, Float.parseFloat(value));
            }
        } else if (qName.equalsIgnoreCase(INSTANCE)) {
            String id = attributes.getValue(ID);

            if (Util.isParsableToInt(id)) {
                tmpinstance = new LabeledGraphInstance(model,Integer.parseInt(id));
            } else {
                tmpinstance = new LabeledGraphInstance(model,Util.convertToInt(id));
            }

        } else if (qName.equalsIgnoreCase(X_COORDINATE)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                tmpinstance.setX(Float.parseFloat(value));
            }
        } else if (qName.equalsIgnoreCase(Y_COORDINATE)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                tmpinstance.setY(Float.parseFloat(value));
            }
        }
    }
    
    private LabeledGraphInstance tmpinstance;
    private LabeledGraphModel model;
    protected static final String INSTANCE = "instance";
    protected static final String NAME = "name";
    protected static final String ID = "id";
    protected static final String X_COORDINATE = "x-coordinate";
    protected static final String Y_COORDINATE = "y-coordinate";
    protected static final String VALUE = "value";
    protected static final String SCALAR = "scalar";
}
