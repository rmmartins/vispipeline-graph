/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledgraph.model.xml;

import labeledgraph.model.*;
import java.io.IOException;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "LabeledGraph.Output",
name = "Labeled Graph Model XML writer",
description = "Write a labeled graph model to a XML file.")
public class XMLLabeledGraphModelWriterComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (filename.trim().length() > 0) {
            if (model != null) {
                XMLLabeledGraphModelWriter xmw = new XMLLabeledGraphModelWriter();
                xmw.write(model, filename);
            } else {
                throw new IOException("A projection model must be provided to write.");
            }
        } else {
            throw new IOException("A projection model file name must be provided to write.");
        }
    }

    public void input(@Param(name = "model") LabeledGraphModel model) {
        this.model = model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new XMLLabeledGraphModelWriterParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        model = null;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public static final long serialVersionUID = 1L;
    private String filename = "";
    private transient XMLLabeledGraphModelWriterParamView paramview;
    private transient LabeledGraphModel model;
}
