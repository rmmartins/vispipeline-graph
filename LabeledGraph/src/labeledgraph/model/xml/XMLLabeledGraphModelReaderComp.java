/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledgraph.model.xml;

import labeledgraph.model.*;
import java.io.IOException;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "LabeledGraph.Input",
name = "Labeled Graph model XML reader",
description = "Read a labeled graph model from an XML file.")
public class XMLLabeledGraphModelReaderComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (filename.trim().length() > 0) {
            XMLLabeledGraphModelReader xmr = new XMLLabeledGraphModelReader();

            model = new LabeledGraphModel();
            xmr.read(model, filename);
        } else {
            throw new IOException("A labeled graph model file name must be provided to write.");
        }
    }

    public LabeledGraphModel output() {
        return model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new XMLLabeledGraphModelReaderParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        model = null;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public static final long serialVersionUID = 1L;
    private String filename = "";
    private transient LabeledGraphModel model;
    private transient XMLLabeledGraphModelReaderParamView paramview;
}
