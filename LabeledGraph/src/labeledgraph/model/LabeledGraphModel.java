/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledgraph.model;

import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import graph.model.Connectivity;
import graph.model.GraphModel;
import java.util.ArrayList;
import labeledprojection.util.ImageCollection;
import projection.model.Scalar;
import textprocessing.corpus.Corpus;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class LabeledGraphModel extends GraphModel {

    public LabeledGraphModel() {
        this.connectivities = new ArrayList<Connectivity>();
        this.selsconn = null;
        this.setInstanceSize(4);
    }

    @Override
    public String toString() {
        if (source == null || source.isEmpty()) return "Graph Model";
        else return source;
    }

    public ImageCollection getImageCollection() {
       return imageCollection;
    }

    public void setImageCollection(ImageCollection imageCollection) {
       this.imageCollection = imageCollection;
    }

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String s) {
        source = s;
    }

    public DissimilarityType getDissType() {
        return disstype;
    }

    public void setDissType(DissimilarityType d) {
        disstype = d;
    }

    public LabeledGraphInstance getInstanceByLabel(String label) {
        for (int i=0;i<this.getInstances().size();i++) {
            if (((LabeledGraphInstance)this.getInstances().get(i)).getLabel().equalsIgnoreCase(label))
                return ((LabeledGraphInstance)this.getInstances().get(i));
        }
        return null;
    }

    public Scalar getScalarByName(String name) {
        for (Scalar s : this.scalars)
            if (s.getName().equals(name)) return s;
        return null;
    }

    public void addScalar(Scalar scalar) {
        //Scalar scalar = new Scalar(name);

        if (!scalars.contains(scalar)) {
            scalars.add(scalar);
            //return scalar;
        } else {
            //return scalars.get(scalars.indexOf(scalar));
        }
    }

    public int getNumMatching() {
        return numMatching;
    }

    public void setNumMatching(int numMatching) {
        this.numMatching = numMatching;
    }

    public int getNumNonMatching() {
        return numNonMatching;
    }

    public void setNumNonMatching(int numNonMatching) {
        this.numNonMatching = numNonMatching;
    }

    public int getNumNonCorresponding() {
        return numNonCorresponding;
    }

    public void setNumNonCorresponding(int numNonCorresponding) {
        this.numNonCorresponding = numNonCorresponding;
    }

    public ArrayList<AbstractInstance> getValidSelectedInstances() {
        ArrayList selinsts = this.getSelectedInstances();
        ArrayList<AbstractInstance> valids = new ArrayList<AbstractInstance>();
        if (selinsts != null) {
            for (int i=0;i<selinsts.size();i++) {
                if (((LabeledGraphInstance)(selinsts.get(i))).isValid())
                    if (!valids.contains((AbstractInstance)selinsts.get(i)))
                    valids.add((AbstractInstance) selinsts.get(i));
                }
            }
        return valids;
    }

    public ArrayList<AbstractInstance> getValidInstances() {
        ArrayList selinsts = this.instances;// getInstances();
        ArrayList<AbstractInstance> valids = new ArrayList<AbstractInstance>();
        if (selinsts != null) {
            for (int i=0;i<selinsts.size();i++) {
                if (((LabeledGraphInstance)(selinsts.get(i))).isValid())
                    valids.add((AbstractInstance) selinsts.get(i));
            }
        }
        return valids;
    }

    public void removeScalar(Scalar scalar) {
        //removing the scalar from the instance
        int index = this.scalars.indexOf(scalar);
        if (index != -1) {
            for (AbstractInstance pi : instances) {
                ((LabeledGraphInstance)pi).removeScalar(index);
            }
        }
        //removing the scalar from the model
        this.scalars.remove(scalar);
    }

    public void setDrawAs(int d) {
        drawAs = d;
    }

    public int getDrawAs() {
        return drawAs;
    }

    protected String source = "";
    protected ImageCollection imageCollection;
    protected DissimilarityType disstype;
    protected Corpus corpus;
    private int numMatching = 0;
    private int numNonMatching = 0;
    private int numNonCorresponding = 0;
    private int drawAs;
    
}
