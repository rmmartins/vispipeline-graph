/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledgraph.model;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D; 
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import graph.model.GraphInstance;
import labeledprojection.util.ImageCollection;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class LabeledGraphInstance extends GraphInstance {

    public LabeledGraphInstance(LabeledGraphModel model, String l, int id, float x, float y) {
        ///super(model, id, x, y);
        super(id, x, y);
        this.setModel(model);
        model.addInstance(this);
        label = l;
        size = imageSize = 4;
        drawAs = 1;
        showlabel = true;
        setSizeFactor(0);
    }

    public LabeledGraphInstance(LabeledGraphModel model, int id) {
        this(model, "", id, 0.0f, 0.0f);
    }

    @Override
    public void draw(BufferedImage image, boolean highquality) {
        Graphics2D g2 = (Graphics2D) image.getGraphics();
        LabeledGraphModel tmodel = (LabeledGraphModel) model;
        if (highquality) g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int xaux = (((int) this.x) <= 0) ? 1 : (((int) this.x) < image.getWidth()) ? (int) this.x : image.getWidth() - 1;
        int yaux = (((int) this.y) <= 0) ? 1 : (((int) this.y) < image.getHeight()) ? (int) this.y : image.getHeight() - 1;
        int inssize = tmodel.getInstanceSize();
        float alpha = tmodel.getAlpha();
        if ((tmodel.getSelectedInstances() == null)||(tmodel.getSelectedInstances().isEmpty())) alpha = 1.0f;
            switch (drawAs) {
                case 0:
                    int rgb = color.getRGB();
                    inssize /= 2;
                    if (selected) {
                        alpha = 1.0f;
                        g2.setColor(Color.BLACK);
                        g2.drawRect(xaux-inssize-1,yaux-inssize-1,inssize*2+2,inssize*2+2);
                    }
                    for (int i=-inssize;i<=inssize;i++)
                        for (int j=-inssize;j<=inssize;j++)
                            simulateAlpha(image,alpha,xaux-i,yaux-j,rgb);
                    break;
                case 1:
                    if (selected) alpha = 1.0f;
                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, alpha));
                    g2.setColor(color);
                    g2.fillOval(xaux-inssize,yaux-inssize,inssize*2,inssize*2);
                    g2.setColor(Color.BLACK);
                    g2.drawOval(xaux-inssize,yaux-inssize,inssize*2,inssize*2);
                    g2.setStroke(new BasicStroke(1.0f));
                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                    break;
                case 2:
                    if (this.image != null) {
                        Image img = this.image.getScaledInstance(this.getImageSize()*5,this.getImageSize()*5,0);
                        //Image img = this.image.getScaledInstance(this.getSize()*5,this.getSize()*5,0);
                        int w = img.getWidth(null);
                        int h = img.getHeight(null);
                        g2.drawImage(img,xaux-(w/2),yaux-(h/2),null);
//                        int w = this.image.getWidth(null);
//                        int h = this.image.getHeight(null);
//                        g2.drawImage(this.image,xaux-(w/2),yaux-(h/2),null);
                        //Desenhando o contorno colorido para mostrar a classe...
                        if (drawFrame) {
                            g2.setStroke(new BasicStroke(3.0f));
                            g2.setColor(this.getColor());
                            g2.drawRect(xaux-(w/2)-2,yaux-(h/2)-2,w+3,h+3);
                            g2.setStroke(new BasicStroke(1.0f));
                        }
                        //Desenhando o contorno colorido para mostrar a classe...
                        if (drawFrame) {
                            g2.setStroke(new BasicStroke(3.0f));
                            g2.setColor(this.getColor());
                            g2.drawRect(xaux -w/2-2,yaux-h/2-2,w+3,h+3);
                            g2.setStroke(new BasicStroke(1.0f));
                        }
                        if (selected) {
                            g2.setStroke(new BasicStroke(2.0f));
                            g2.setColor(Color.RED);
                            g2.drawRect(((int) this.x) - w / 2, ((int) this.y) - h / 2, w, h);
                            g2.setStroke(new BasicStroke(1.0f));
                        }
                    }
                    break;
            }
        //show the label associated to this instance
        if (showlabel) {
//            java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());
//            int width = metrics.stringWidth(toString().trim());
//            int height = metrics.getAscent();
//            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
//            g2.setPaint(java.awt.Color.WHITE);
//            g2.fillRect(((int) x) + 3, ((int) y) - 1 - height, width + 4, height + 4);
//            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
//            g2.setColor(java.awt.Color.DARK_GRAY);
//            g2.drawRect(((int) x) + 3, ((int) y) - 1 - height, width + 4, height + 4);
//            g2.drawString(toString().trim(), ((int) x) + 3, ((int) y));
        } else if (showImage) {
//            if (this.image != null)
//                g2.drawImage(this.image.getScaledInstance(100,100,0),xaux+(size/2),yaux+(size/2),null);
        }
    }

    @Override
    public boolean isInside(int x, int y) {
        switch (drawAs) {
            case 2:
                return (Math.abs(x - this.x) <= getImageSize() && Math.abs(y - this.y) <= getImageSize());
            default:
                return (Math.abs(x - this.x) <= getSize() && Math.abs(y - this.y) <= getSize());
        }
    }

    public void setLabel(String l) {
        label = l;
    }

    public String getLabel() {
        return label;
    }
    
    public boolean isShowImage() {
        return showImage;
    }

    public void setShowImage(boolean i) {
        showImage = i;
    }

    public void setDrawFrame(boolean selected) {
        drawFrame = selected;
    }

    public boolean isDrawFrame() {
        return drawFrame;
    }

    public void setDrawAs(int d) {
        drawAs = d;
    }

    public int getDrawAs() {
        return drawAs;
    }

    public void setImage(Image i) {
        image = i;
    }

    public Image getImage() {
        return image;
    }

    public Image getImage(ImageCollection im) throws IOException {
        if (im != null) {
            try {
            Image img = im.getImage(label);
            return img;
            }catch (Exception e) {
                return null;
            }
        }else return null;
        //return image;
    }

    public void setSize(int i) {
        size = i;
    }

    public void setImageSize(int i) {
        imageSize = i;
    }

    public int getImageSize() {
        return imageSize;
    }

    public void removeScalar(int index) {
        if (this.scalars.size() > index) {
            this.scalars.remove(index);
        }
    }

    @Override
    public String toString() {
        return label;
    }

    protected int drawAs; //0: dot, 1: circle, 2:image
    protected int size,imageSize;
    protected String label;
    protected Image image;
    protected boolean drawFrame = false;
    protected boolean showImage = false;
}
