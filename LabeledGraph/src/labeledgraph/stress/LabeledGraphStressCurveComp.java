package labeledgraph.stress;

import java.io.IOException;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.Euclidean;
import graph.model.Connectivity;
import graph.model.Edge;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import labeledgraph.model.LabeledGraphModel;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import labeledprojection.stress.DistanceStressCurve;
import labeledprojection.stress.StressCurveComp;
import labeledprojection.stress.StressCurveDialog;

/**
 *
 * @author José Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "LabeledGraph.Basics",
name = "Calculate Stress Curve",
description = "Calculate how the distances relations of a graph differ from the original distances relations, for each instance.")
public class LabeledGraphStressCurveComp extends StressCurveComp implements AbstractComponent {

    public Connectivity createEuclideanWeightConnectivity(Connectivity con) {

        AbstractMatrix points = exportPoints(model.getInstances());

        ArrayList<Edge> connEdges = con.getEdges();
        ArrayList<Integer> ids = points.getIds();
        ArrayList<Edge> euclideanDistEdges = new ArrayList<Edge>();

        if (connEdges == null) return null;
        if (ids == null) return null;

        for (int i=0;i<connEdges.size();i++) {
            int source = connEdges.get(i).getSource();
            int target = connEdges.get(i).getTarget();
            if (ids.indexOf(source) != -1 && ids.indexOf(target) != -1) {
                float distance = (new Euclidean()).calculate(points.getRow(ids.indexOf(source)),points.getRow(ids.indexOf(target)));
                euclideanDistEdges.add(new Edge(source,target,distance));
            }
        }

        Connectivity conn = new Connectivity("Plane Euclidian",euclideanDistEdges);
        return conn;
    }

    public DistanceMatrix createDistanceMatrix(LabeledGraphModel model, boolean useWeights, Connectivity euclideanDistAsWeightsCon) {

        DistanceMatrix dm = new DistanceMatrix();

        float[][] dmat = new float[model.getInstances().size()][model.getInstances().size()];
        for (int i=0;i<dmat.length;i++)
            for (int j=0;j<dmat[i].length;j++)
                if (i == j) dmat[i][j] = 0;
                else dmat[i][j] = Float.MAX_VALUE;

        ArrayList<Edge> edges = null;

        if (euclideanDistAsWeightsCon != null) {
            edges = euclideanDistAsWeightsCon.getEdges();
        }else {
            if (model.getSelectedConnectivity() != null)
                edges = model.getSelectedConnectivity().getEdges();
            else if ((model.getConnectivities() != null)&&(model.getConnectivities().size() > 1))
                    edges = model.getConnectivities().get(1).getEdges();
        }

        if (edges == null) return null;

        for (int k=0;k<edges.size();k++) {
            Edge ed = edges.get(k);
            int x = model.getInstances().indexOf(model.getInstanceById(ed.getSource()));
            int y = model.getInstances().indexOf(model.getInstanceById(ed.getTarget()));
            if (useWeights) dmat[x][y] = dmat[y][x] = ed.getWeight();
            else dmat[x][y] = dmat[y][x] = 1.0f;
        }

        //Calculating the shortest path, in the graph, among all nodes (including virtual nodes)
        //Floyd Warshall algorithm.
        int n = dmat.length;
        for (int k=0; k<n; k++)
            for (int i=0; i<n; i++)
                for (int j=0; j<n; j++) {
                    float dd = dmat[i][k] + dmat[k][j];
                    if (dmat[i][j] > dd) dmat[i][j] = dd;
                }

        int k = -1;
        ArrayList<ArrayList<Float>> ndmat = new ArrayList<ArrayList<Float>>();
        for (int i=0;i<dmat.length;i++) {
            if (!model.getInstances().get(i).toString().isEmpty()) {
                k++;
                ndmat.add(new ArrayList<Float>());
                for (int j=0;j<dmat[i].length;j++) {
                    if (!model.getInstances().get(j).toString().isEmpty()) {
                        ndmat.get(k).add(dmat[i][j]);
                    }
                }
            }
        }

        //Create and fill the distance distmatrix
        dm.setElementCount(ndmat.size());

        float maxDistance = Float.NEGATIVE_INFINITY;
        float minDistance = Float.POSITIVE_INFINITY;
        int idMin1 = -1, idMax1 = -1, idMin2 = -1, idMax2 = -1;
        float[][] distmat = new float[ndmat.size() - 1][];
        for (int i=0; i<ndmat.size()-1; i++) {
            distmat[i] = new float[i + 1];
            for (int j=0;j<distmat[i].length; j++) {
                float distance = ndmat.get(i+1).get(j);
                if (distance < minDistance) {
                    minDistance = distance;
                    idMin1 = model.getInstances().get(i+1).getId();
                    idMin2 = model.getInstances().get(j).getId();
                }
                if (distance > maxDistance) {
                    maxDistance = distance;
                    idMax1 = model.getInstances().get(i+1).getId();
                    idMax2 = model.getInstances().get(j).getId();
                }
                if ((i+1)!=j) {
                    if ((i+1) < j) distmat[j - 1][(i+1)] = distance;
                    else distmat[(i+1) - 1][j] = distance;
                }
            }
        }
        dm.setMinDistance(minDistance);
        dm.setMaxDistance(maxDistance);
        dm.setDistmatrix(distmat);
        System.out.println("Min Distance: "+minDistance+" ("+idMin1+","+idMin2+")");
        System.out.println("Max Distance: "+maxDistance+" ("+idMax1+","+idMax2+")");

        return dm;

    }

    @Override
    public void execute() throws IOException {

        AbstractDissimilarity diss = DissimilarityFactory.getInstance(DissimilarityFactory.DissimilarityType.EUCLIDEAN);
        if (this.getDissimilarityType() != null)
            diss = DissimilarityFactory.getInstance(this.getDissimilarityType());
        
        DistanceStressCurve sc = new DistanceStressCurve();
        if (model == null)
            throw new IOException("A tree model should be provided.");

        if (this.getDataFileName() == null || this.getDataFileName().isEmpty())
            throw new IOException("A distance matrix or a points matrix should be provided.");

        //Obtaining the distances between projected points...
        AbstractMatrix validPoints = exportPoints(((LabeledGraphModel)model).getValidInstances());
        //validPoints.print();
        DistanceMatrix projectionDmat;

        if (useVisEuclideanDistance) projectionDmat = new DistanceMatrix(validPoints, new Euclidean());
        else {
            if (useEuclideanAsWeights) {
                Connectivity gCon = null;
                if (((LabeledGraphModel)model).getSelectedConnectivity() != null)
                    gCon = ((LabeledGraphModel)model).getSelectedConnectivity();
                else if ((((LabeledGraphModel)model).getConnectivities() != null)&&(((LabeledGraphModel)model).getConnectivities().size() > 1))
                        gCon = ((LabeledGraphModel)model).getConnectivities().get(1);
                if (gCon == null) throw new IOException("Error: Graph connectivity not found.");
                Connectivity euclideanWeightsConn = createEuclideanWeightConnectivity(gCon);
                projectionDmat = createDistanceMatrix((LabeledGraphModel)model,useWeight,euclideanWeightsConn);
            } else projectionDmat = createDistanceMatrix((LabeledGraphModel)model,useWeight,null);

            ArrayList<String> labels = validPoints.getLabels();
            ArrayList<Integer> ids = validPoints.getIds();

            float[] c = validPoints.getClassData();
//            ArrayList<Float> cdatas = new ArrayList<Float>();
//
//            int k=0;
//            //excluding virtual nodes...
//            for (int i=0;i<labels.size();i++) {
//                if (labels.get(i).isEmpty()) {
//                    labels.remove(i);
//                    ids.remove(i);
//                    i--;
//                }else cdatas.add(c[k]);
//                k++;
//            }
//            float[] c2 = new float[cdatas.size()];
//            for (int i=0;i<cdatas.size();i++) c2[i] = cdatas.get(i);
            projectionDmat.setLabels(labels);
            projectionDmat.setIds(ids);
            projectionDmat.setClassData(c);
        }

        //Obtaining original distance matrix..
        switch (this.getDataFileType()) {
            case 0: //points file
                try {
                    AbstractMatrix matrix = MatrixFactory.getInstance(this.getDataFileName());
                    originalDmat = new DistanceMatrix(matrix,diss);
                } catch (IOException ex) {
                    Logger.getLogger(StressCurveComp.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case 1: //distance matrix
                try {
                    originalDmat = new DistanceMatrix(this.getDataFileName());
                } catch (IOException ex) {
                    Logger.getLogger(StressCurveComp.class.getName()).log(Level.SEVERE, null, ex);
                }
        }

        //both matrix shall have the same elements...
        igualateDmats(originalDmat,projectionDmat);

        //if (originalDmat.getElementCount() != projectionDmat.getElementCount()) {
        //    JOptionPane.showMessageDialog(null,"Original dataset is different from projected dataset!");
        //}else {
            //stress = stresscalc.calculate(projectionDmat,originalDmat);
            BufferedImage image = sc.generate(new Dimension(1200,1200),1.0f,projectionDmat,originalDmat);
            StressCurveDialog.getInstance(null).display(graphicLabel,image);
        //}

        
    }

    public void input(@Param(name = "Tree Model") LabeledGraphModel model) {
        this.model = model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new LabeledGraphStressCurveParamView(this);
        }

        return paramview;
    }

    public boolean getUseWeight() {
        return useWeight;
    }

    public void setUseWeight(boolean selected) {
        this.useWeight = selected;
    }

    public boolean getUseVisEuclidianDistance() {
        return useVisEuclideanDistance;
    }

    public void setUseVisEuclidianDistance(boolean selected) {
        this.useVisEuclideanDistance = selected;
    }

    public boolean getUseEuclideanAsWeights() {
        return useEuclideanAsWeights;
    }

    public void setUseEuclideanAsWeights(boolean selected) {
        this.useEuclideanAsWeights = selected;
    }

    private void igualateDmats(DistanceMatrix dmat1, DistanceMatrix dmat2) {
        DistanceMatrix dmSmaller,dmBigger;
        if (dmat1.getElementCount() > dmat2.getElementCount()) {
            dmSmaller = dmat2;
            dmBigger = dmat1;
        }else {
            dmBigger = dmat2;
            dmSmaller = dmat1;
        }
        for (int i=0;i<dmBigger.getIds().size();i++) {
            int id = dmBigger.getIds().get(i);
            if (!dmSmaller.getIds().contains(id)) {
                dmBigger.removeElement(id);
                i--;
            }
        }
    }

    public static final long serialVersionUID = 1L;
    private boolean useWeight = true;
    private boolean useVisEuclideanDistance = false;
    private boolean useEuclideanAsWeights = false;

}
