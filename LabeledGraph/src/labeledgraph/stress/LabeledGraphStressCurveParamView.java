
package labeledgraph.stress;

import java.awt.event.ActionEvent;
import java.io.IOException;
import labeledprojection.stress.StressCurveParamView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class LabeledGraphStressCurveParamView extends StressCurveParamView {

    public LabeledGraphStressCurveParamView(LabeledGraphStressCurveComp comp) {
        super(comp);
        this.comp = comp;
        initComponents();
        this.useWeightCheckBox.setSelected(comp.getUseWeight());
        this.useVisEuclideanDistanceCheckBox.setSelected(comp.getUseVisEuclidianDistance());
        this.useEuclideanAsWeightsCheckBox.setSelected(comp.getUseEuclideanAsWeights());
        useWeightCheckBoxActionPerformed(null);
        useVisEuclideanDistanceCheckBoxActionPerformed(null);
    }

    @Override
    public void finished() throws IOException {
        super.finished();
        comp.setUseWeight(this.useWeightCheckBox.isSelected());
        comp.setUseVisEuclidianDistance(this.useVisEuclideanDistanceCheckBox.isSelected());
        comp.setUseEuclideanAsWeights(this.useEuclideanAsWeightsCheckBox.isSelected());
    }

    private void useVisEuclideanDistanceCheckBoxActionPerformed(ActionEvent evt) {
        if (useVisEuclideanDistanceCheckBox.isSelected()) {
            useWeightCheckBox.setSelected(false);
            useWeightCheckBox.setEnabled(false);
            useEuclideanAsWeightsCheckBox.setSelected(false);
            useEuclideanAsWeightsCheckBox.setEnabled(false);
        }else {
            useWeightCheckBox.setEnabled(true);
        }
    }

    private void useWeightCheckBoxActionPerformed(ActionEvent evt) {
        if (useWeightCheckBox.isSelected()) {
            useEuclideanAsWeightsCheckBox.setEnabled(true);
        }else {
            useEuclideanAsWeightsCheckBox.setSelected(false);
            useEuclideanAsWeightsCheckBox.setEnabled(false);
        }
    }

    private void initComponents() {

        java.awt.GridBagConstraints gridBagConstraints2;

        checkjPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        checkjPanel.setLayout(new java.awt.GridBagLayout());
        generalPanel.add(checkjPanel);

        useVisEuclideanDistanceCheckBox = new javax.swing.JCheckBox();
        useVisEuclideanDistanceCheckBox.setText("Use Euclidean Distance (Visualization Plane)");
        useVisEuclideanDistanceCheckBox.setSelected(false);
        useVisEuclideanDistanceCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        useVisEuclideanDistanceCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useVisEuclideanDistanceCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints2 = new java.awt.GridBagConstraints();
        gridBagConstraints2.gridx = 0;
        gridBagConstraints2.gridy = 0;
        gridBagConstraints2.insets = new java.awt.Insets(3, 3, 3, 3);
        checkjPanel.add(useVisEuclideanDistanceCheckBox,gridBagConstraints2);
        //generalPanel.add(useVisEuclideanDistanceCheckBox);

        useWeightCheckBox = new javax.swing.JCheckBox();
        useWeightCheckBox.setText("Use Edges Weights");
        useWeightCheckBox.setSelected(true);
        useWeightCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        useWeightCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useWeightCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints2 = new java.awt.GridBagConstraints();
        gridBagConstraints2.gridx = 0;
        gridBagConstraints2.gridy = 1;
        gridBagConstraints2.insets = new java.awt.Insets(3, 3, 3, 3);
        checkjPanel.add(useWeightCheckBox,gridBagConstraints2);
        //generalPanel.add(useWeightCheckBox);

        useEuclideanAsWeightsCheckBox = new javax.swing.JCheckBox();
        useEuclideanAsWeightsCheckBox.setText("Use Plane Euclidean Distances as Weights");
        useEuclideanAsWeightsCheckBox.setSelected(false);
        useEuclideanAsWeightsCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        gridBagConstraints2 = new java.awt.GridBagConstraints();
        gridBagConstraints2.gridx = 0;
        gridBagConstraints2.gridy = 2;
        gridBagConstraints2.insets = new java.awt.Insets(3, 3, 3, 3);
        checkjPanel.add(useEuclideanAsWeightsCheckBox,gridBagConstraints2);
        //generalPanel.add(useEuclideanAsWeightsCheckBox);

    }

    private javax.swing.JCheckBox useVisEuclideanDistanceCheckBox;
    private javax.swing.JCheckBox useWeightCheckBox;
    private javax.swing.JCheckBox useEuclideanAsWeightsCheckBox;
    private javax.swing.JPanel checkjPanel = new javax.swing.JPanel();
    
    protected LabeledGraphStressCurveComp comp;

}
