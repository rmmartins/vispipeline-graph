
package labeledgraph.view.forms;

import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import graph.model.Edge;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import labeledgraph.model.LabeledGraphInstance;
import labeledgraph.model.LabeledGraphModel;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;
import projection.model.ProjectionInstance;
import projection.model.Scalar;
import projection.util.ProjectionConstants;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.util.OpenDialog;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.SaveDialog;
import visualizationbasics.util.filter.AbstractFilter;
import visualizationbasics.util.filter.DATAFilter;
import visualizationbasics.util.filter.DMATFilter;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class LabeledGraphSaveClassDialog extends JDialog {

    /** Creates new form ImageProjectionModelParamView */
    public LabeledGraphSaveClassDialog(JFrame parent,LabeledGraphModel model,String source) {
        super(parent);
        initComponents();
        this.setModal(true);
        this.model = model;
        //this.modelInstances = model.getInstances();
        this.source = source;
        choice = 0;
        //if (this.source.endsWith(".data")) choice = 0;
        if (this.source.endsWith(".dmat")) choice = 1;
        this.dissimilarityComboBox.removeAllItems();
        for (DissimilarityType disstype : DissimilarityType.values()) {
            this.dissimilarityComboBox.addItem(disstype);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        dissimilarityPanel = new javax.swing.JPanel();
        dissimilarityComboBox = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        sourcePanel = new javax.swing.JPanel();
        pointsRadioButton = new javax.swing.JRadioButton();
        distanceMatrixRadioButton = new javax.swing.JRadioButton();
        distanceMatrixTextField = new javax.swing.JTextField();
        pointsTextField = new javax.swing.JTextField();
        pointsButton = new javax.swing.JButton();
        distanceMatrixButton = new javax.swing.JButton();
        outputPanel = new javax.swing.JPanel();
        outputPointsCheckBox = new javax.swing.JCheckBox();
        outputDmatCheckBox = new javax.swing.JCheckBox();
        outputPointsTextField = new javax.swing.JTextField();
        outputDmatTextField = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        outputLayoutDmatCheckBox = new javax.swing.JCheckBox();
        outputLayoutDmatTextField = new javax.swing.JTextField();
        useWeightCheckBox = new javax.swing.JCheckBox();

        getContentPane().setLayout(new java.awt.GridBagLayout());

        dissimilarityPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Dissimilarity"));
        dissimilarityPanel.setLayout(new java.awt.BorderLayout());
        dissimilarityPanel.add(dissimilarityComboBox, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        getContentPane().add(dissimilarityPanel, gridBagConstraints);

        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jPanel1.add(saveButton);

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jPanel1.add(cancelButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        getContentPane().add(jPanel1, gridBagConstraints);

        sourcePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Source File"));
        sourcePanel.setLayout(new java.awt.GridBagLayout());

        buttonGroup1.add(pointsRadioButton);
        pointsRadioButton.setSelected(true);
        pointsRadioButton.setText("Points File");
        pointsRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        pointsRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pointsRadioButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        sourcePanel.add(pointsRadioButton, gridBagConstraints);

        buttonGroup1.add(distanceMatrixRadioButton);
        distanceMatrixRadioButton.setText("Distance File");
        distanceMatrixRadioButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        distanceMatrixRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                distanceMatrixRadioButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        sourcePanel.add(distanceMatrixRadioButton, gridBagConstraints);

        distanceMatrixTextField.setColumns(35);
        distanceMatrixTextField.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        sourcePanel.add(distanceMatrixTextField, gridBagConstraints);

        pointsTextField.setColumns(35);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        sourcePanel.add(pointsTextField, gridBagConstraints);

        pointsButton.setText("Search...");
        pointsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pointsButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        sourcePanel.add(pointsButton, gridBagConstraints);

        distanceMatrixButton.setText("Search...");
        distanceMatrixButton.setEnabled(false);
        distanceMatrixButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                distanceMatrixButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        sourcePanel.add(distanceMatrixButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        getContentPane().add(sourcePanel, gridBagConstraints);

        outputPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Output File"));
        outputPanel.setLayout(new java.awt.GridBagLayout());

        outputPointsCheckBox.setLabel("Points File");
        outputPointsCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputPointsCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        outputPanel.add(outputPointsCheckBox, gridBagConstraints);

        outputDmatCheckBox.setLabel("Distance File");
        outputDmatCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputDmatCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        outputPanel.add(outputDmatCheckBox, gridBagConstraints);

        outputPointsTextField.setEnabled(false);
        outputPointsTextField.setPreferredSize(new java.awt.Dimension(465, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        outputPanel.add(outputPointsTextField, gridBagConstraints);

        outputDmatTextField.setEnabled(false);
        outputDmatTextField.setPreferredSize(new java.awt.Dimension(465, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        outputPanel.add(outputDmatTextField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        getContentPane().add(outputPanel, gridBagConstraints);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Layout Distance Matrix"));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        outputLayoutDmatCheckBox.setText("Layout Distance File");
        outputLayoutDmatCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputLayoutDmatCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel2.add(outputLayoutDmatCheckBox, gridBagConstraints);

        outputLayoutDmatTextField.setEnabled(false);
        outputLayoutDmatTextField.setPreferredSize(new java.awt.Dimension(430, 27));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel2.add(outputLayoutDmatTextField, gridBagConstraints);

        useWeightCheckBox.setText("Use Edges Weights");
        useWeightCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        useWeightCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useWeightCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel2.add(useWeightCheckBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        getContentPane().add(jPanel2, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_cancelButtonActionPerformed

    private ProjectionInstance getInstanceByID(int id) {
        ArrayList<AbstractInstance> modelInstances = model.getInstances();
        if (modelInstances != null && !modelInstances.isEmpty())
            for (int i=0;i<modelInstances.size();i++)
                if (modelInstances.get(i).getId() == id) return (ProjectionInstance)modelInstances.get(i);
        return null;
    }

    private void setClasses(AbstractMatrix matrix, Scalar scalar) {
        //int k=0,l=0;
        for (int i=0;i<matrix.getRowCount();i++) {
            AbstractVector inst = matrix.getRow(i);
            if (inst != null) {
                ProjectionInstance ins = getInstanceByID(inst.getId());
                if (ins != null) {
                    inst.setKlass(ins.getScalarValue(scalar));
                    //k++;
                } else {
                    matrix.removeRow(i);
                    i--;
                    //l++;
                }
            }
        }
    }

    private void setClasses(DistanceMatrix dmat, Scalar scalar) {

        float[] cdata = dmat.getClassData();
        ArrayList<Integer> ids = dmat.getIds();
        for (int i=0;i<dmat.getIds().size();i++) {
            ProjectionInstance ins = getInstanceByID(ids.get(i));
            if (ins != null)
               cdata[i] = ins.getScalarValue(scalar);
            else {
                dmat.removeElement(dmat.getIds().get(i));
                i--;
            }
        }
    }

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        AbstractMatrix matrix;
        DistanceMatrix dmat;
        String result = "";
        String sourceFilename = "";
        //String filename = "";
//        if (this.distanceMatrixRadioButton.isSelected()) filename = this.distanceMatrixTextField.getText();
//        else if(this.pointsRadioButton.isSelected()) filename = this.pointsTextField.getText();
//
//        if (filename.isEmpty()) {
//            JOptionPane.showMessageDialog(null,"The original data matrix must be informed!");
//            return;
//        }
        if (this.outputPointsCheckBox.isSelected()) {
            if (this.outputPointsTextField.getText().trim().isEmpty()) {
                JOptionPane.showMessageDialog(null,"The output data matrix must be informed!");
                return;
            }else {
                this.matrixFileName = this.outputPointsTextField.getText().trim();
            }
        }

        if (this.outputDmatCheckBox.isSelected()) {
            if (this.outputDmatTextField.getText().trim().isEmpty()) {
                JOptionPane.showMessageDialog(null,"The output distance matrix must be informed!");
                return;
            }else {
                this.dmatFilename = this.outputDmatTextField.getText().trim();
            }
        }

        if (this.outputLayoutDmatCheckBox.isSelected()) {
            if (this.outputLayoutDmatTextField.getText().trim().isEmpty()) {
                JOptionPane.showMessageDialog(null,"The output layout distance matrix must be informed!");
                return;
            }else {
                this.layoutDmatFilename = this.outputLayoutDmatTextField.getText().trim();
            }
        }

        if (!this.source.isEmpty()) sourceFilename = this.source;
        else if (choice == 0) sourceFilename = this.pointsTextField.getText();
        else if (choice == 1) sourceFilename = this.distanceMatrixTextField.getText();

        if (sourceFilename.isEmpty()) {
            JOptionPane.showMessageDialog(null,"The original data source must be informed!");
            return;
        }
        try {
            switch (choice) {
                case 0:
                    matrix = MatrixFactory.getInstance(sourceFilename);
                    if (matrix != null) {
//                        if (matrix.getRowCount() != modelInstances.size()) {
//                            JOptionPane.showMessageDialog(null,"Source datasets are not the same!");
//                            return;
//                        }
                        setClasses(matrix,scalar);
                        if (this.outputPointsCheckBox.isSelected()) {
                            matrix.save(matrixFileName);
                            result +="Attributes Matrix saved.\n";
                        }
                        if (this.outputDmatCheckBox.isSelected()) {
                            AbstractDissimilarity diss = DissimilarityFactory.getInstance((DissimilarityType)dissimilarityComboBox.getSelectedItem());
                            dmat = new DistanceMatrix(matrix,diss);
                            if (dmat != null) dmat.save(dmatFilename);
                            result +="Distance Matrix saved.\n";
                        }
                    }
                    break;
                case 1:
                    dmat = new DistanceMatrix(sourceFilename);
                    if (dmat != null) {
//                        if (dmat.getElementCount() != modelInstances.size()) {
//                            JOptionPane.showMessageDialog(null,"Source datasets are not the same!");
//                            return;
//                        }
                        setClasses(dmat,scalar);
                        if (this.outputDmatCheckBox.isSelected()) {
                            dmat.save(dmatFilename);
                            result +="Distance Matrix saved.\n";
                        }
                    }
                default:
            }

            //saving layout distance matrix...
            if (this.outputLayoutDmatCheckBox.isSelected()) {
                DistanceMatrix layoutDmat = createDistanceMatrix(model,this.useWeightCheckBox.isSelected());
                setClasses(layoutDmat, scalar);
                layoutDmat.save(layoutDmatFilename);
                result +="Layout Distance Matrix saved.\n";
            }

//            if (this.pointsRadioButton.isSelected()) {
//                matrix = MatrixFactory.getInstance(filename);
//                if (matrix != null) {
//                    if (matrix.getRowCount() != modelInstances.size()) {
//                        JOptionPane.showMessageDialog(null,"Datasets are not the same!");
//                        return;
//                    }
//                    setClasses(matrix,scalar);
//                    if (saveMatrixCheckBox.isSelected()) {
//                        matrix.save(matrixFileName);
//                        result +="Attributes Matrix saved.\n";
//                    }
//                    if (saveDmatCheckBox.isSelected()) {
//                        AbstractDissimilarity diss = DissimilarityFactory.getInstance((DissimilarityType)dissimilarityComboBox.getSelectedItem());
//                        dmat = new DistanceMatrix(matrix,diss);
//                        if (dmat != null) dmat.save(dmatFilename);
//                        result +="Distance Matrix saved.\n";
//                    }
//                }
//            }
//            else if (this.distanceMatrixRadioButton.isSelected()) {
//                dmat = new DistanceMatrix(filename);
//                if (dmat != null) {
//                    if (dmat.getElementCount() != modelInstances.size()) {
//                        JOptionPane.showMessageDialog(null,"Datasets are not the same!");
//                        return;
//                    }
//                    setClasses(dmat,scalar);
//                    if (saveDmatCheckBox.isSelected()) {
//                        dmat.save(dmatFilename);
//                        result +="Distance Matrix saved.\n";
//                    }
//                }
//            }
        } catch (IOException ex) {
            Logger.getLogger(LabeledGraphSaveClassDialog.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (!result.isEmpty()) {
                JOptionPane.showMessageDialog(null,result);
                this.setVisible(false);
            }
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void pointsRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pointsRadioButtonActionPerformed
        this.distanceMatrixButton.setEnabled(false);
        this.distanceMatrixTextField.setEnabled(false);
        this.outputPointsCheckBox.setEnabled(true);

        this.pointsButton.setEnabled(true);
        this.pointsTextField.setEnabled(true);

        this.distanceMatrixTextField.setText("");
        this.dissimilarityComboBox.setEnabled(true);
        this.choice = 0;
}//GEN-LAST:event_pointsRadioButtonActionPerformed

    private void distanceMatrixRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_distanceMatrixRadioButtonActionPerformed
        this.distanceMatrixButton.setEnabled(true);
        this.distanceMatrixTextField.setEnabled(true);
        this.outputPointsCheckBox.setEnabled(false);

        this.pointsButton.setEnabled(false);
        this.pointsTextField.setEnabled(false);

        this.pointsTextField.setText("");
        this.dissimilarityComboBox.setEnabled(false);
        this.choice = 1;
}//GEN-LAST:event_distanceMatrixRadioButtonActionPerformed

    private void pointsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pointsButtonActionPerformed
        pushButton(new DATAFilter(),this.pointsTextField);
}//GEN-LAST:event_pointsButtonActionPerformed

    private void distanceMatrixButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_distanceMatrixButtonActionPerformed
        pushButton(new DMATFilter(),this.distanceMatrixTextField);
}//GEN-LAST:event_distanceMatrixButtonActionPerformed

    private void outputPointsCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outputPointsCheckBoxActionPerformed

        if (this.outputPointsCheckBox.isSelected()) {
            String directory = "",filename = "";
            if (this.source.lastIndexOf("/") != -1) {
                directory = this.source.substring(0,this.source.lastIndexOf("/"));
                filename = this.source.substring(this.source.lastIndexOf("/")+1);
                filename = filename.replace(".dmat",".data");
            }
            int result = SaveDialog.showSaveDialog(new DATAFilter(),this,directory,filename);
            if (result == JFileChooser.APPROVE_OPTION) {
                this.outputPointsTextField.setText(SaveDialog.getFilename());
                this.outputPointsTextField.setEnabled(true);
            }else {
                this.outputPointsCheckBox.setSelected(false);
            }
        }else {
            this.outputPointsTextField.setEnabled(false);
        }

    }//GEN-LAST:event_outputPointsCheckBoxActionPerformed

    private void outputDmatCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outputDmatCheckBoxActionPerformed

        if (this.outputDmatCheckBox.isSelected()) {
            String directory = "",filename = "";
            if (this.source.lastIndexOf("/") != -1) {
                directory = this.source.substring(0,this.source.lastIndexOf("/"));
                filename = this.source.substring(this.source.lastIndexOf("/")+1);
                filename = filename.replace(".data",".dmat");
            }
            int result = SaveDialog.showSaveDialog(new DMATFilter(),this,directory,filename);
            if (result == JFileChooser.APPROVE_OPTION) {
                this.outputDmatTextField.setText(SaveDialog.getFilename());
                this.outputDmatTextField.setEnabled(true);
            } else {
                this.outputDmatTextField.setEnabled(false);
            }
        }else {
            this.outputDmatTextField.setEnabled(false);
        }

    }//GEN-LAST:event_outputDmatCheckBoxActionPerformed

    private void outputLayoutDmatCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outputLayoutDmatCheckBoxActionPerformed
        
        if (this.outputLayoutDmatCheckBox.isSelected()) {
            String directory = "",filename = "";
            if (this.source.lastIndexOf("/") != -1) {
                directory = this.source.substring(0,this.source.lastIndexOf("/"));
                filename = this.source.substring(this.source.lastIndexOf("/")+1);
                filename = filename.replace(".data",".dmat");
                filename = "Layout_"+filename;
            }
            int result = SaveDialog.showSaveDialog(new DMATFilter(),this,directory,filename);
            if (result == JFileChooser.APPROVE_OPTION) {
                this.outputLayoutDmatTextField.setText(SaveDialog.getFilename());
                this.outputLayoutDmatTextField.setEnabled(true);
            }else {
                this.outputLayoutDmatCheckBox.setSelected(false);
            }
        }else {
            this.outputLayoutDmatTextField.setEnabled(false);
        }
    }//GEN-LAST:event_outputLayoutDmatCheckBoxActionPerformed

    private void useWeightCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_useWeightCheckBoxActionPerformed
        
}//GEN-LAST:event_useWeightCheckBoxActionPerformed

    private void pushButton(AbstractFilter filter,javax.swing.JTextField field) {

        try {
                PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
                int result = OpenDialog.showOpenDialog(spm,filter,this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    String filename = OpenDialog.getFilename();
                    //this.source = filename;
                    field.setText(filename);
                }
            } catch (IOException ex) {
                Logger.getLogger(LabeledGraphSaveClassDialog.class.getName()).log(Level.SEVERE, null, ex);
            }

    }

    public static LabeledGraphSaveClassDialog getInstance(javax.swing.JFrame parent,LabeledGraphModel model,String source) {
        if (instance == null || instance.getParent() != parent) {
            instance = new LabeledGraphSaveClassDialog(parent,model,source);
        }
        return instance;
    }

    public void display(Scalar scalar) {
        this.scalar = scalar;
        this.dmatFilename = "";
        this.matrixFileName = "";
        this.outputPointsCheckBox.setSelected(false);
        this.outputPointsTextField.setText("");
        this.outputPointsTextField.setEnabled(false);
        this.outputDmatCheckBox.setSelected(false);
        this.outputDmatTextField.setText("");
        this.outputDmatTextField.setEnabled(false);
        if (source.isEmpty()) {
            this.sourcePanel.setVisible(true);
            this.dissimilarityPanel.setVisible(true);
            this.outputPointsCheckBox.setVisible(true);
            this.outputPointsTextField.setVisible(true);
        }else {
            this.sourcePanel.setVisible(false);
            this.dissimilarityPanel.setVisible(false);
            if (choice == 0) {
                this.outputPointsCheckBox.setVisible(true);
                this.outputPointsTextField.setVisible(true);
            }
            else if (choice == 1) {
                this.outputPointsCheckBox.setVisible(false);
                this.outputPointsTextField.setVisible(false);
            }
        }
        this.pack();
        this.setLocationRelativeTo(this.getParent());
        this.setVisible(true);
    }

    private DistanceMatrix createDistanceMatrix(LabeledGraphModel model,boolean useWeights) {

        DistanceMatrix dm = new DistanceMatrix();

        float[][] dmat = new float[model.getInstances().size()][model.getInstances().size()];
        for (int i=0;i<dmat.length;i++)
            for (int j=0;j<dmat[i].length;j++)
                if (i == j) dmat[i][j] = 0;
                else dmat[i][j] = Float.MAX_VALUE;

        ArrayList<Edge> edges = null;

        if (model.getSelectedConnectivity() != null)
            edges = model.getSelectedConnectivity().getEdges();
        else if ((model.getConnectivities() != null)&&(model.getConnectivities().size() > 1))
                edges = model.getConnectivities().get(1).getEdges();

        if (edges == null) return null;

        for (int k=0;k<edges.size();k++) {
            Edge ed = edges.get(k);
            int x = model.getInstances().indexOf(model.getInstanceById(ed.getSource()));
            int y = model.getInstances().indexOf(model.getInstanceById(ed.getTarget()));
            if (useWeights) dmat[x][y] = dmat[y][x] = ed.getWeight();
            else dmat[x][y] = dmat[y][x] = 1.0f;
        }

        //Calculating the shortest path, in the tree, among all nodes (including virtual nodes)
        //Floyd Warshall algorithm.
        int n = dmat.length;
        for (int k=0; k<n; k++)
            for (int i=0; i<n; i++)
                for (int j=0; j<n; j++) {
                    float dd = dmat[i][k] + dmat[k][j];
                    if (dmat[i][j] > dd) dmat[i][j] = dd;
                }

        int k = -1;
        ArrayList<Integer> ids = new ArrayList<Integer>();
        ArrayList<ArrayList<Float>> ndmat = new ArrayList<ArrayList<Float>>();
        for (int i=0;i<dmat.length;i++) {
            if (!model.getInstances().get(i).toString().isEmpty()) {
                ids.add(model.getInstances().get(i).getId());
                k++;
                ndmat.add(new ArrayList<Float>());
                for (int j=0;j<dmat[i].length;j++) {
                    if (!model.getInstances().get(j).toString().isEmpty()) {
                        ndmat.get(k).add(dmat[i][j]);
                    }
                }
            }
        }

        //Create and fill the distance distmatrix
        dm.setElementCount(ndmat.size());

        float maxDistance = Float.NEGATIVE_INFINITY;
        float minDistance = Float.POSITIVE_INFINITY;

        float[][] distmat = new float[ndmat.size() - 1][];
        for (int i=0; i<ndmat.size()-1; i++) {
            distmat[i] = new float[i + 1];
            for (int j=0;j<distmat[i].length; j++) {
                float distance = ndmat.get(i+1).get(j);
                if (distance < minDistance) minDistance = distance;
                if (distance > maxDistance) maxDistance = distance;
                if ((i+1)!=j) {
                    if ((i+1) < j) distmat[j - 1][(i+1)] = distance;
                    else distmat[(i+1) - 1][j] = distance;
                }
            }
        }
        dm.setMinDistance(minDistance);
        dm.setMaxDistance(maxDistance);
        dm.setDistmatrix(distmat);

        //class labels...
        ArrayList<String> labels = new ArrayList<String>();
        float[] classdata = new float[ids.size()];
        for (int i=0;i<ids.size();i++) {
            LabeledGraphInstance gi = (LabeledGraphInstance)model.getInstanceById(ids.get(i));
            if (gi != null) {
                labels.add(gi.getLabel());
                classdata[i] = gi.getScalarValue(scalar);
            }
        }

        dm.setClassData(classdata);
        dm.setIds(ids);
        dm.setLabels(labels);

        return dm;

    }

    private String matrixFileName;
    private String dmatFilename;
    private String layoutDmatFilename;
    private String source;
    private int choice; //0:.data, 1:.dmat, else:error
    private Scalar scalar;
    //private ArrayList<AbstractInstance> modelInstances;
    private static LabeledGraphSaveClassDialog instance;
    private LabeledGraphModel model;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton cancelButton;
    private javax.swing.JComboBox dissimilarityComboBox;
    private javax.swing.JPanel dissimilarityPanel;
    private javax.swing.JButton distanceMatrixButton;
    private javax.swing.JRadioButton distanceMatrixRadioButton;
    private javax.swing.JTextField distanceMatrixTextField;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JCheckBox outputDmatCheckBox;
    private javax.swing.JTextField outputDmatTextField;
    private javax.swing.JCheckBox outputLayoutDmatCheckBox;
    private javax.swing.JTextField outputLayoutDmatTextField;
    private javax.swing.JPanel outputPanel;
    private javax.swing.JCheckBox outputPointsCheckBox;
    private javax.swing.JTextField outputPointsTextField;
    private javax.swing.JButton pointsButton;
    private javax.swing.JRadioButton pointsRadioButton;
    private javax.swing.JTextField pointsTextField;
    protected javax.swing.JButton saveButton;
    private javax.swing.JPanel sourcePanel;
    private javax.swing.JCheckBox useWeightCheckBox;
    // End of variables declaration//GEN-END:variables
}
