
package labeledgraph.view.interaction;

import labeledgraph.model.LabeledGraphInstance;
import visualizationbasics.view.ModelViewer;
import visualizationbasics.view.selection.AbstractSelection;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public abstract class LabeledGraphAbstractSelection extends AbstractSelection {

    public LabeledGraphAbstractSelection(ModelViewer viewer) {
        super(viewer);
    }

    public abstract void selected(int x, int y);

    public abstract void released(LabeledGraphInstance instance);

}
