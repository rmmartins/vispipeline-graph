/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledgraph.coordination;

import labeledgraph.model.LabeledGraphInstance;
import labeledgraph.model.LabeledGraphModel;
import labeledgraph.view.LabeledGraphReportView;
import labeledgraph.view.LabeledGraphFrame; 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import projection.model.Scalar;
import visualizationbasics.coordination.IdentityCoordinator;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class GraphClassMatchCoordination extends IdentityCoordinator {

    public GraphClassMatchCoordination() {
        super();
        frames = new ArrayList<LabeledGraphFrame>();
    }

    public Scalar match() {
        Scalar cdata1,cdata2;
        LabeledGraphInstance pi1,pi2;
        Scalar scalar = new Scalar("ClassMatch");
        scalar.store(0);
        scalar.store(2);
        TreeMap<Float,HashMap<Float,Integer>> confuseData0 = new TreeMap<Float,HashMap<Float,Integer>>();
        TreeMap<Float,HashMap<Float,Integer>> confuseData1 = new TreeMap<Float,HashMap<Float,Integer>>();

        if (models.size() == 2) { 
            LabeledGraphModel model1 = ((LabeledGraphModel)models.get(0));
            LabeledGraphModel model2 = ((LabeledGraphModel)models.get(1));
            if (model1.getScalarByName("ClassMatch") == null) model1.addScalar(scalar);
            if (model2.getScalarByName("ClassMatch") == null) model2.addScalar(scalar);
            for (int i=0;i<model1.getInstances().size();i++) {
                pi1 = (LabeledGraphInstance)model1.getInstances().get(i);
                pi1.setScalarValue(scalar,0);
            }
            for (int i=0;i<model2.getInstances().size();i++) {
                pi1 = (LabeledGraphInstance)model2.getInstances().get(i);
                pi1.setScalarValue(scalar,0);
            }
            cdata1 = model1.getScalarByName("cdata");
            cdata2 = model2.getScalarByName("cdata");
            if (cdata1 == null || cdata2 == null) return null;
            for (int i=0;i<model1.getInstances().size();i++) {
                pi1 = (LabeledGraphInstance)model1.getInstances().get(i);
                if (pi1.isValid()) {
                    pi2 = model2.getInstanceByLabel(pi1.getLabel());
                    if (pi2 != null) {
                        if (pi1.getScalarValue(cdata1) == pi2.getScalarValue(cdata2)) {//class match...
                            pi1.setScalarValue(scalar,1);
                            pi2.setScalarValue(scalar,1);
                            model1.setNumMatching(model1.getNumMatching()+1);
                            model2.setNumMatching(model2.getNumMatching()+1);
                        }else { //class does not match...
                            pi1.setScalarValue(scalar,2);
                            pi2.setScalarValue(scalar,2);
                            model1.setNumNonMatching(model1.getNumNonMatching()+1);
                            model2.setNumNonMatching(model2.getNumNonMatching()+1);
                        }
                        //Counting for confusing matrix...
                        if (!confuseData0.containsKey(pi1.getScalarValue(cdata1))) {
                            confuseData0.put(pi1.getScalarValue(cdata1),new HashMap<Float,Integer>());
                        }
                        if (!confuseData0.get(pi1.getScalarValue(cdata1)).containsKey(pi2.getScalarValue(cdata2)))
                            confuseData0.get(pi1.getScalarValue(cdata1)).put(pi2.getScalarValue(cdata2),1);
                        else {
                            int qt = confuseData0.get(pi1.getScalarValue(cdata1)).get(pi2.getScalarValue(cdata2));
                            confuseData0.get(pi1.getScalarValue(cdata1)).put(pi2.getScalarValue(cdata2),qt+1);
                        }


                        if (!confuseData1.containsKey(pi2.getScalarValue(cdata2))) {
                            confuseData1.put(pi2.getScalarValue(cdata2),new HashMap<Float,Integer>());
                        }
                        if (!confuseData1.get(pi2.getScalarValue(cdata2)).containsKey(pi1.getScalarValue(cdata1)))
                            confuseData1.get(pi2.getScalarValue(cdata2)).put(pi1.getScalarValue(cdata1),1);
                        else {
                            int qt = confuseData1.get(pi2.getScalarValue(cdata2)).get(pi1.getScalarValue(cdata1));
                            confuseData1.get(pi2.getScalarValue(cdata2)).put(pi1.getScalarValue(cdata1),qt+1);
                        }
                    }
                }
            }
            model1.setNumNonCorresponding(model1.getValidInstances().size()-model1.getNumMatching()-model1.getNumNonMatching());
            model2.setNumNonCorresponding(model2.getValidInstances().size()-model2.getNumMatching()-model2.getNumNonMatching());
            frames.get(0).updateScalars(scalar);
            frames.get(1).updateScalars(scalar);
            ((LabeledGraphReportView)frames.get(0).getReportPanel()).setClassMatchingPanel(model1.getValidInstances().size(),
                                                                               model1.getNumMatching(),
                                                                               model1.getNumNonMatching(),
                                                                               model1.getNumNonCorresponding(),
                                                                               confuseData0);
            ((LabeledGraphReportView)frames.get(0).getReportPanel()).setColorTable(model1.getColorTable());
            ((LabeledGraphReportView)frames.get(1).getReportPanel()).setClassMatchingPanel(model2.getValidInstances().size(),
                                                                               model2.getNumMatching(),
                                                                               model2.getNumNonMatching(),
                                                                               model2.getNumNonCorresponding(),
                                                                               confuseData1);
            ((LabeledGraphReportView)frames.get(1).getReportPanel()).setColorTable(model2.getColorTable());
            return scalar;
        } else return null;
        
    }

    public synchronized void addFrame(LabeledGraphFrame frame) {
        if (frame == null) {
            throw new NullPointerException();
        }

        if (!frames.contains(frame)) {
            frames.add(frame);
        }
    }

    ArrayList<LabeledGraphFrame> frames;

}
