/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledgraph.coordination;

import java.io.IOException;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Coordination",
name = "Graph Class Match Coordination",
description = "Create an graph class match coordination object.")
public class GraphClassMatchCoordinationComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        coord = new GraphClassMatchCoordination();
    }

    public AbstractCoordinator output() {
        return coord;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        coord = null;
    }

    public static final long serialVersionUID = 1L;
    private transient GraphClassMatchCoordination coord;
}
