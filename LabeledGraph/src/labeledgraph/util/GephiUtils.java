/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labeledgraph.util;

import graph.model.Connectivity;
import graph.model.Edge;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.gephi.data.attributes.api.AttributeColumn;
import org.gephi.data.attributes.api.AttributeController;
import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.graph.api.EdgeIterable;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.Node;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.EdgeDefault;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.partition.api.Part;
import org.gephi.partition.api.Partition;
import org.gephi.partition.api.PartitionController;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.gephi.statistics.plugin.Modularity;
import org.openide.util.Lookup;

/**
 *
 * @author Renato
 */
public class GephiUtils {
    
    private static HashMap<String,Integer> partitionMap;
    private static double modularityValue;
    
    public static HashMap<String, Integer> getPartitionMap() {
        return partitionMap;
    }
    
    public static double getPartitionModularity() {
        return modularityValue;
    }
    
    public static Connectivity toConnectivity(Graph graph) {
        EdgeIterable edgeIterable = graph.getEdges();
        ArrayList<Edge> edges = new ArrayList();
        
        //System.out.println("Nodes count: " + graph.getNodeCount());
        
        for (org.gephi.graph.api.Edge edge: edgeIterable) {
            // "Normalize" the node's Ids
            int sourceNodeId = edge.getSource().getId() % (graph.getNodeCount() + 1);
            int targetNodeId = edge.getTarget().getId() % (graph.getNodeCount() + 1);
            
            //System.out.println("Edge: " + sourceNodeId + " - " + targetNodeId);
            
            //Edge newEdge = new Edge(edge.getSource().getId(), edge.getTarget().getId(), edge.getWeight());
            Edge newEdge = new Edge(sourceNodeId - 1, targetNodeId - 1, edge.getWeight());
            edges.add(newEdge);
        }
        
        return new Connectivity("Gephi " + graph.toString(), edges);
    }
    
    public static Graph importGephiGraph(String path) {
        
        //Init a project - and therefore a workspace
        ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
        pc.newProject();
        Workspace workspace = pc.getCurrentWorkspace(); 

        //Import file
        ImportController importController = Lookup.getDefault().lookup(ImportController.class);        
        Container container;
        
        try {
            File file = new File(path);
            container = importController.importFile(file);
            container.getLoader().setEdgeDefault(EdgeDefault.UNDIRECTED);   //Force DIRECTED
            container.setAllowAutoNode(false);  //Don’t create missing nodes
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        //Append imported data to GraphAPI
        importController.process(container, new DefaultProcessor(), workspace);
        
        org.gephi.graph.api.GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
        Graph graph = graphModel.getUndirectedGraph();        
        
        return graph;
    }
    
    public static void processFastGreedyPartition(Graph inputGraph) {
        partitionMap = new HashMap();
                
        ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
        pc.cleanWorkspace(pc.getCurrentWorkspace());
        pc.closeCurrentProject();
        pc.newProject();
        
        org.gephi.graph.api.GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
        graphModel.pushFrom(inputGraph);
        
        AttributeController ac = Lookup.getDefault().lookup(AttributeController.class);
        AttributeModel am = ac.getModel();
        
        Modularity modularity = new Modularity();
        modularity.execute(graphModel, am);       
        modularityValue = modularity.getModularity();

        PartitionController partitionController = Lookup.getDefault().lookup(PartitionController.class);
        //Partition with ‘modularity_class’, just created by Modularity algorithm
        AttributeColumn modColumn = am.getNodeTable().getColumn(Modularity.MODULARITY_CLASS);
        Partition p = partitionController.buildPartition(modColumn, graphModel.getGraph());
        
        for (Part part: p.getParts()) {
            String partName = part.getDisplayName();
            int partitionIndex = Integer.valueOf(partName);
            for (Object element: part.getObjects()) {
                String nodeId = ((Node)element).getNodeData().getId();               
                partitionMap.put(nodeId, partitionIndex);
            }
        }
    }
    
    public static Graph toGephiGraph(Connectivity conn) {
        
        //Init a project - and therefore a workspace
        ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
        pc.cleanWorkspace(pc.getCurrentWorkspace());
        pc.closeCurrentProject();
        pc.newProject();
       
        org.gephi.graph.api.GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
        Graph graph = graphModel.getUndirectedGraph();  
        HashSet<String> graphNodeIds = new HashSet();
        
        for (Edge edge: conn.getEdges()) {
            
            String sourceNodeId = Integer.toString(edge.getSource());
            Node sourceNode;
            if (!graphNodeIds.contains(sourceNodeId)) {
                sourceNode = graphModel.factory().newNode(sourceNodeId);
                graph.addNode(sourceNode);
                graphNodeIds.add(sourceNodeId);
            }
            else {
                sourceNode = graph.getNode(sourceNodeId);
            }
                                
            String targetNodeId = Integer.toString(edge.getTarget());
            Node targetNode;
            if (!graphNodeIds.contains(targetNodeId)) {
                targetNode = graphModel.factory().newNode(targetNodeId);
                graph.addNode(targetNode);
                graphNodeIds.add(targetNodeId);
            }
            else {
                targetNode = graph.getNode(targetNodeId);
            }
            
            graph.addEdge(graphModel.factory().newEdge(sourceNode, targetNode, edge.getWeight(), false));   
        }
        
        
        return graph;
    }
    
    public static Connectivity createKarateConnectivity() {
        
        ArrayList<Edge> karateEdgeList = new ArrayList<Edge>();
        karateEdgeList.add(new Edge(2, 1, 1.0f));
        karateEdgeList.add(new Edge(3, 1, 1.0f));
        karateEdgeList.add(new Edge(3, 2, 1.0f));
        karateEdgeList.add(new Edge(4, 1, 1.0f));
        karateEdgeList.add(new Edge(4, 2, 1.0f));
        karateEdgeList.add(new Edge(4, 3, 1.0f));
        karateEdgeList.add(new Edge(5, 1, 1.0f));
        karateEdgeList.add(new Edge(6, 1, 1.0f));
        karateEdgeList.add(new Edge(7, 1, 1.0f));
        karateEdgeList.add(new Edge(7, 5, 1.0f));
        karateEdgeList.add(new Edge(7, 6, 1.0f));
        karateEdgeList.add(new Edge(8, 1, 1.0f));
        karateEdgeList.add(new Edge(8, 2, 1.0f));
        karateEdgeList.add(new Edge(8, 3, 1.0f));
        karateEdgeList.add(new Edge(8, 4, 1.0f));
        karateEdgeList.add(new Edge(9, 1, 1.0f));
        karateEdgeList.add(new Edge(9, 3, 1.0f));
        karateEdgeList.add(new Edge(10, 3, 1.0f));
        karateEdgeList.add(new Edge(11, 1, 1.0f));
        karateEdgeList.add(new Edge(11, 5, 1.0f));
        karateEdgeList.add(new Edge(11, 6, 1.0f));
        karateEdgeList.add(new Edge(12, 1, 1.0f));
        karateEdgeList.add(new Edge(13, 1, 1.0f));
        karateEdgeList.add(new Edge(13, 4, 1.0f));
        karateEdgeList.add(new Edge(14, 1, 1.0f));
        karateEdgeList.add(new Edge(14, 2, 1.0f));
        karateEdgeList.add(new Edge(14, 3, 1.0f));
        karateEdgeList.add(new Edge(14, 4, 1.0f));
        karateEdgeList.add(new Edge(17, 6, 1.0f));
        karateEdgeList.add(new Edge(17, 7, 1.0f));
        karateEdgeList.add(new Edge(18, 1, 1.0f));
        karateEdgeList.add(new Edge(18, 2, 1.0f));
        karateEdgeList.add(new Edge(20, 1, 1.0f));
        karateEdgeList.add(new Edge(20, 2, 1.0f));
        karateEdgeList.add(new Edge(22, 1, 1.0f));
        karateEdgeList.add(new Edge(22, 2, 1.0f));
        karateEdgeList.add(new Edge(26, 24, 1.0f));
        karateEdgeList.add(new Edge(26, 25, 1.0f));
        karateEdgeList.add(new Edge(28, 3, 1.0f));
        karateEdgeList.add(new Edge(28, 24, 1.0f));
        karateEdgeList.add(new Edge(28, 25, 1.0f));
        karateEdgeList.add(new Edge(29, 3, 1.0f));
        karateEdgeList.add(new Edge(30, 24, 1.0f));
        karateEdgeList.add(new Edge(30, 27, 1.0f));
        karateEdgeList.add(new Edge(31, 2, 1.0f));
        karateEdgeList.add(new Edge(31, 9, 1.0f));
        karateEdgeList.add(new Edge(32, 1, 1.0f));
        karateEdgeList.add(new Edge(32, 25, 1.0f));
        karateEdgeList.add(new Edge(32, 26, 1.0f));
        karateEdgeList.add(new Edge(32, 29, 1.0f));
        karateEdgeList.add(new Edge(33, 3, 1.0f));
        karateEdgeList.add(new Edge(33, 9, 1.0f));
        karateEdgeList.add(new Edge(33, 15, 1.0f));
        karateEdgeList.add(new Edge(33, 16, 1.0f));
        karateEdgeList.add(new Edge(33, 19, 1.0f));
        karateEdgeList.add(new Edge(33, 21, 1.0f));
        karateEdgeList.add(new Edge(33, 23, 1.0f));
        karateEdgeList.add(new Edge(33, 24, 1.0f));
        karateEdgeList.add(new Edge(33, 30, 1.0f));
        karateEdgeList.add(new Edge(33, 31, 1.0f));
        karateEdgeList.add(new Edge(33, 32, 1.0f));
        karateEdgeList.add(new Edge(34, 9, 1.0f));
        karateEdgeList.add(new Edge(34, 10, 1.0f));
        karateEdgeList.add(new Edge(34, 14, 1.0f));
        karateEdgeList.add(new Edge(34, 15, 1.0f));
        karateEdgeList.add(new Edge(34, 16, 1.0f));
        karateEdgeList.add(new Edge(34, 19, 1.0f));
        karateEdgeList.add(new Edge(34, 20, 1.0f));
        karateEdgeList.add(new Edge(34, 21, 1.0f));
        karateEdgeList.add(new Edge(34, 23, 1.0f));
        karateEdgeList.add(new Edge(34, 24, 1.0f));
        karateEdgeList.add(new Edge(34, 27, 1.0f));
        karateEdgeList.add(new Edge(34, 28, 1.0f));
        karateEdgeList.add(new Edge(34, 29, 1.0f));
        karateEdgeList.add(new Edge(34, 30, 1.0f));
        karateEdgeList.add(new Edge(34, 31, 1.0f));
        karateEdgeList.add(new Edge(34, 32, 1.0f));
        karateEdgeList.add(new Edge(34, 33, 1.0f));      
        
        Connectivity conn = new Connectivity("Karate", karateEdgeList);
        
        return conn;
    }
    
    public static void main(String args[]) {
        /*ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
        pc.newProject();
        
        HashSet<String> gephiNodes = new HashSet();
        
        GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
        
        Node node1 = graphModel.factory().newNode("Um");
        if (!gephiNodes.contains(node1.toString())) {
            gephiNodes.add(node1.toString());
            graphModel.getGraph().addNode(node1);
        }

        node1 = graphModel.factory().newNode("Um");
        if (!gephiNodes.contains(node1.toString())) {
            gephiNodes.add(node1.toString());
            graphModel.getGraph().addNode(node1);
        }

        node1 = graphModel.factory().newNode("Um");
        if (!gephiNodes.contains(node1.toString())) {
            gephiNodes.add(node1.toString());
            graphModel.getGraph().addNode(node1);
        }        
        
        System.out.println("Nodes count: " + graphModel.getGraph().getNodeCount());*/
        
        //Graph graph = importGephiGraph("D:\\Temp\\karate.gml");
        Graph graph = importGephiGraph("/media/DADOS/Temp/karate.gml");
   
        Connectivity conn = createKarateConnectivity();
        
        
        //HashMap modularityMap = getGephiModularityPartition(toGephiGraph(conn));
        processFastGreedyPartition(graph);
        
        //System.out.println("VisPipeline Modularity: " + GraphModularity.getModularity(conn, (int[])modularityMap.get("partition")));
        System.out.println("File Gephi Modularity: " + getPartitionModularity());
        
        processFastGreedyPartition(toGephiGraph(conn));
        
        System.out.println("Conn Gephi Modularity: " + getPartitionModularity());
        
        /*for (org.gephi.graph.api.Edge edge: graph.getEdges()) {
            System.out.println(edge.getSource().getId() + " - " + edge.getTarget().getId() + 
                    " : " + edge.getWeight());
        }*/
        
        System.out.println("END!");
        
        
        
    }
}
