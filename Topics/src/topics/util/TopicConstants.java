/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package topics.util;

/**
 *
 * @author Fernando V. Paulovic
 */
public interface TopicConstants {

    public static final String TOPICS = "topics";
    public static final String TOPICS_CLUSTERS = "topics clusters";
    public static final String PROPFILENAME = "topics.properties";

}
