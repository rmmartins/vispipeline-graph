/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package topics.model;

import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import labeledprojection.model.LabeledProjectionModel;
import projection.model.ProjectionInstance;
import projection.model.Scalar;
import textprocessing.corpus.Corpus;
import topics.Topic;
import topics.scalar.QuerySolver;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class TopicProjectionModel extends LabeledProjectionModel {

    public TopicProjectionModel() {
        this.topics = new ArrayList<Topic>();
        this.showtopics = false;
    }

    public boolean isShowTopics() {
        return showtopics;
    }

    public void setShowTopics(boolean showtopics) {
        this.showtopics = showtopics;

        for (Topic t : topics) {
            t.setShowTopic(showtopics);
        }

        setChanged();
    }

    public void addTopic(Topic topic) {
        this.topics.add(topic);
        setChanged();
    }

    public ArrayList<Topic> getTopics() {
        return topics;
    }

    public Scalar createQueryScalar(String word) throws IOException {
        if (corpus == null) {
            throw new IOException("The corpus must be loaded!");
        }

        //Adding a new scalar
        String scalarName = "'" + word + "'";
        Scalar scalar = this.addScalar(scalarName);

        QuerySolver qS = new QuerySolver(corpus, instances);
        qS.createCdata(word, scalar);

        return scalar;
    }

    public Topic getTopicByPosition(Point point) {
        float dist = Float.MAX_VALUE;
        Topic topic = null;

        for (Topic t : this.topics) {
            float aux = t.weightDistance(point);
            if (aux != -1 && dist > aux) {
                dist = aux;
                topic = t;
            }
        }

        return topic;
    }

    public static ArrayList<ProjectionInstance> convertInstances(ArrayList<AbstractInstance> aiins) {
        ArrayList<ProjectionInstance> conv = new ArrayList<ProjectionInstance>();
        for (AbstractInstance ai : aiins) {
            if (ai instanceof ProjectionInstance) {
                conv.add((ProjectionInstance) ai);
            }
        }

        return conv;
    }

    private boolean showtopics;
    private ArrayList<Topic> topics;
    private Corpus corpus;
}
