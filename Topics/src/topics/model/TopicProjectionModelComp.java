/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package topics.model;

import java.io.IOException;
import java.util.ArrayList;
import labeledprojection.technique.radviz.RadVizStructure;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import projection.model.Scalar;
import projection.util.ProjectionConstants;
import textprocessing.corpus.Corpus;
import textprocessing.corpus.CorpusFactory;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Projection.Basics",
name = "Topic Projection Model",
description = "Create a projection model to be visualized.")
public class TopicProjectionModelComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (projection != null || radvizStructure != null) {
            model = new TopicProjectionModel();
            Scalar cdata = model.addScalar(ProjectionConstants.CDATA);
            Scalar dots = model.addScalar(ProjectionConstants.DOTS);

            if (projection == null) {
                projection = radvizStructure.getInstances();
            } else {
                int nrows = projection.getRowCount();

                for (int i = 0; i < nrows; i++) {
                    AbstractVector row = projection.getRow(i);
                    TopicProjectionInstance pi = new TopicProjectionInstance(model,
                            projection.getLabel(i),row.getId(),row.getValue(0), row.getValue(1));
                    pi.setScalarValue(cdata, row.getKlass());
                    pi.setScalarValue(dots, 0.0f);
                }
                //setting the collection...
                if (corpus != null) {
                    System.err.println("Set corpus on model");
                    model.setCorpus(corpus);
                } else if ((collection != null)&&(!collection.isEmpty())) {
                    if (model.getInstances() != null) {
                        //Este 1 esta fixo, estudar parametrizacao (ngrams)
                        Corpus c = CorpusFactory.getInstance(collection,1); 
                        model.setCorpus(c);
                    }
                }else {
                    model.setCorpus(null);
                }

                model.setRadvizStructure(radvizStructure);

            }

        } else {
            throw new IOException("A 2D projection should be provided.");
        }
    }

    public void input(@Param(name = "2D projection") AbstractMatrix projection) {
        this.projection = projection;
    }

//    public void input(@Param(name = "2D projection") AbstractMatrix projection,
//            @Param(name = "labels") ArrayList<String> labels) {
//        this.projection = projection;
//
//        if (labels.size() == projection.getRowCount()) {
//            this.labels = labels;
//        }
//    }

    public void input(@Param(name = "RadViz Structure") RadVizStructure radvizStructure) {
        this.radvizStructure = radvizStructure;
    }
    
    public void attach(Corpus corpus) {
        this.corpus = corpus;
    }

    public TopicProjectionModel output() {
        return model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new TopicProjectionModelParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        projection = null;
        model = null;
        radvizStructure = null;
    }

    public void setCollection(String c) {
        this.collection = c;
    }

    public String getCollection() {
        return collection;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String s) {
        this.source = s;
    }

    public RadVizStructure getRadvizStructure() {
        return radvizStructure;
    }

    public void setRadvizStructure(RadVizStructure radvizStructure) {
        this.radvizStructure = radvizStructure;
    }

    public static final long serialVersionUID = 1L;
    private transient TopicProjectionModel model;
    private transient TopicProjectionModelParamView paramview;
    private transient AbstractMatrix projection;
    private transient ArrayList<String> labels;
    private transient RadVizStructure radvizStructure;
    transient Corpus corpus;
    
    private String collection;
    private String source;
}
