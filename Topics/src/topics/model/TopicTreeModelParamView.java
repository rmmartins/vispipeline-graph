/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * XMLModelReaderParamView.java
 *
 * Created on 02/08/2009, 16:29:28
 */
package topics.model;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import projection.util.ProjectionConstants;
import vispipelinebasics.interfaces.AbstractParametersView;
import visualizationbasics.util.OpenDialog;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.filter.ZIPFilter;

/**
 *
 * @author Jose Gustavo
 */
public class TopicTreeModelParamView extends AbstractParametersView {

    /** Creates new form ImageProjectionModelParamView */
    public TopicTreeModelParamView(TopicTreeModelComp comp) {
        initComponents();
        this.comp = comp;
        reset();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        matrixLabel = new javax.swing.JLabel();
        collectionTextField = new javax.swing.JTextField();
        collectionButton = new javax.swing.JButton();
        namePanel = new javax.swing.JPanel();
        nameLabel = new javax.swing.JLabel();
        nameTextField = new javax.swing.JTextField();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Collection"));
        setLayout(new java.awt.GridBagLayout());

        matrixLabel.setText("File name");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(matrixLabel, gridBagConstraints);

        collectionTextField.setColumns(35);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(collectionTextField, gridBagConstraints);

        collectionButton.setText("Search...");
        collectionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                collectionButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(collectionButton, gridBagConstraints);

        namePanel.setLayout(new java.awt.GridBagLayout());

        nameLabel.setText("Name : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        namePanel.add(nameLabel, gridBagConstraints);

        nameTextField.setMinimumSize(new java.awt.Dimension(150, 20));
        nameTextField.setPreferredSize(new java.awt.Dimension(150, 20));
        nameTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameTextFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        namePanel.add(nameTextField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(namePanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void collectionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_collectionButtonActionPerformed
        try {
            PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
            int result = OpenDialog.showOpenDialog(spm, new ZIPFilter(), this);
            if (result == JFileChooser.APPROVE_OPTION) {
                String filename = OpenDialog.getFilename();
                collectionTextField.setText(filename);
            }
        } catch (IOException ex) {
            Logger.getLogger(TopicTreeModelParamView.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_collectionButtonActionPerformed

    private void nameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTextFieldActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_nameTextFieldActionPerformed

    @Override
    public void reset() {
        if (comp.getCollection() != null)
            collectionTextField.setText(comp.getCollection());
        nameTextField.setText(comp.getSource());
    }

    @Override
    public void finished() throws IOException {
        comp.setCollection(collectionTextField.getText());
        comp.setSource(nameTextField.getText());
    }

    private TopicTreeModelComp comp;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton collectionButton;
    private javax.swing.JTextField collectionTextField;
    private javax.swing.JLabel matrixLabel;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JPanel namePanel;
    private javax.swing.JTextField nameTextField;
    // End of variables declaration//GEN-END:variables
}
