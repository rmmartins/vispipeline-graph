/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package topics.model;

import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import projection.model.Scalar;
import textprocessing.corpus.Corpus;
import topics.Topic;
import topics.scalar.QuerySolver;
import tree.basics.Tree;
import tree.model.TreeInstance;
import tree.model.TreeModel;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class TopicTreeModel extends TreeModel {

    public TopicTreeModel() {
        this.topics = new ArrayList<Topic>();
        this.showtopics = false;
    }

    public TopicTreeModel(Tree tree, String collection) {
        super(tree,collection);
        this.topics = new ArrayList<Topic>();
        this.showtopics = false;
    }

    public boolean isShowTopics() {
        return showtopics;
    }

    public void setShowTopics(boolean showtopics) {
        this.showtopics = showtopics;
        for (Topic t : topics) {
            t.setShowTopic(showtopics);
        }
        setChanged();
    }

    public void addTopic(Topic topic) {
        this.topics.add(topic);
        setChanged();
    }

    public ArrayList<Topic> getTopics() {
        return topics;
    }

    public Scalar createQueryScalar(String word) throws IOException {
        if (corpus == null) {
            throw new IOException("The corpus must be loaded!");
        }

        //Adding a new scalar
        String scalarName = "'" + word + "'";
        Scalar scalar = this.addScalar(scalarName);

        QuerySolver qS = new QuerySolver(corpus, instances);
        qS.createCdata(word, scalar);

        return scalar;
    }

    public Topic getTopicByPosition(Point point) {
        float dist = Float.MAX_VALUE;
        Topic topic = null;

        for (Topic t : this.topics) {
            float aux = t.weightDistance(point);
            if (aux != -1 && dist > aux) {
                dist = aux;
                topic = t;
            }
        }

        return topic;
    }

    public static ArrayList<TreeInstance> convertInstances(ArrayList<AbstractInstance> aiins) {
        ArrayList<TreeInstance> conv = new ArrayList<TreeInstance>();
        //for (AbstractInstance ai : aiins) {
        for (int i=0;i<aiins.size();i++) {
            if (aiins.get(i) instanceof TreeInstance) {
                conv.add((TreeInstance)aiins.get(i));
            }
        }

        return conv;
    }

    private boolean showtopics;
    private ArrayList<Topic> topics;
    private Corpus corpus;
}
