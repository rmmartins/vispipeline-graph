/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package topics.model;

import java.io.IOException;
import tree.basics.Tree;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.Basics",
name = "Topic Tree Model",
description = "Create a topic tree model to be visualized.")
public class TopicTreeModelComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (tree != null) {
            model = new TopicTreeModel(tree,collection);
            model.setSource(source);
            //model.print();
        } else {
            throw new IOException("A tree structure should be provided.");
        }
    }

    public void input(@Param(name = "Tree Data") Tree tree) {
        this.tree = tree;
    }

    public TopicTreeModel output() {
        return model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new TopicTreeModelParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        //placement = null;
        model = null;
        tree = null;
    }

    public void setCollection(String c) {
        this.collection = c;
    }

    public String getCollection() {
        return collection;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String s) {
        this.source = s;
    }

    public static final long serialVersionUID = 1L;
    private String collection;
    private transient TopicTreeModelParamView paramview;
    private transient TopicTreeModel model;
    //private transient AbstractMatrix placement;
    private transient Tree tree;
    private String source;

}
