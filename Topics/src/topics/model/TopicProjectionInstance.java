/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package topics.model;

import labeledprojection.model.LabeledProjectionInstance;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class TopicProjectionInstance extends LabeledProjectionInstance {

    public TopicProjectionInstance(TopicProjectionModel model, String label, int id) {
        super(model, label, id);
    }

    public TopicProjectionInstance(TopicProjectionModel model, String label, int id, float x, float y) {
        super(model, label, id, x, y);
    }

    @Override
    public boolean isInside(int x, int y) {
        return (Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2)) <= getSize());
    }

}
