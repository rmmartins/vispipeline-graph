/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package topics.view;

import detailedMatrix.DetailedMatrix;
import distance.DetailedDistanceMatrix;
import distance.DistanceMatrix;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import labeledgraph.view.LabeledGraphReportView;
import labeledprojection.coordination.ClassMatchCoordination;
import matrix.AbstractMatrix;
import projection.model.Scalar;
import textprocessing.corpus.Corpus;
import topics.model.TopicTreeModel;
import tree.layout.radial.RadialLayoutComp;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Tree.View",
name = "Topic Tree View Frame",
description = "Display a topic tree model.")
public class TopicTreeFrameComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (model != null) {
            RadialLayoutComp rc = new RadialLayoutComp();
            rc.input(model);
            rc.execute();
            model = (TopicTreeModel) rc.output();
            TopicTreeFrame frame = new TopicTreeFrame();
            //DTopicProjectionFrame frame = new DTopicProjectionFrame();
            frame.setSize(800, 600);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
            frame.setTitle(title);
            frame.setModel(model);

            if (coordinators != null) {
                for (int i = 0; i < coordinators.size(); i++) {
                    frame.addCoordinator(coordinators.get(i));
                    if (coordinators.get(i) instanceof ClassMatchCoordination) {
                        Scalar sc = ((ClassMatchCoordination)coordinators.get(i)).match();
                    }
                }
            }
            //Setting report data...
            ((LabeledGraphReportView)frame.getReportPanel()).clean();
            Corpus c = model.getCorpus();
            if (c != null) ((LabeledGraphReportView)frame.getReportPanel()).setDataSource(c.getUrl());
            ((LabeledGraphReportView)frame.getReportPanel()).setObjects(model.getValidInstances().size());
            ((LabeledGraphReportView)frame.getReportPanel()).setVirtualObjects(model.getInstances().size() - model.getValidInstances().size());
            ((LabeledGraphReportView)frame.getReportPanel()).setType(model.getType());

            if (matrix != null && matrix instanceof DetailedMatrix) {//(matrix instanceof DetailedDenseMatrix || matrix instanceof DetailedSparseMatrix)) {
                if (matrix.getRowCount() == ((TopicTreeModel)model).getValidInstances().size()) {
                    ((LabeledGraphReportView)frame.getReportPanel()).setSource(((DetailedMatrix)matrix).getSource());
                    ((LabeledGraphReportView)frame.getReportPanel()).setDimensions(matrix.getDimensions());
                }
            }else if (dmat != null && dmat instanceof DetailedDistanceMatrix) {
                if (dmat.getElementCount() == ((TopicTreeModel)model).getValidInstances().size()) {
                    ((LabeledGraphReportView)frame.getReportPanel()).setSource(((DetailedDistanceMatrix)dmat).getSource());
                }
            }
        } else {
            throw new IOException("A topic tree model should be provided.");
        }
    }

    public void input(@Param(name = "Original Distance Matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public void input(@Param(name = "Original Data Matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "topic tree model") TopicTreeModel model) {
        this.model = model;
    }

    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new TopicTreeFrameParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        model = null;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle () {
        return title;
    }

    public static final long serialVersionUID = 1L;
    private String title = "";
    private transient TopicTreeFrameParamView paramview;
    private transient TopicTreeModel model;
    private transient ArrayList<AbstractCoordinator> coordinators;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
}
