/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * TopicProjectionFrame.java
 *
 * Created on 22/06/2009, 19:53:11
 */
package topics.view;

import datamining.clustering.HierarchicalClustering;
import datamining.clustering.HierarchicalClustering.HierarchicalClusteringType;
import datamining.neighbors.Pair;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import distance.dissimilarity.Euclidean;
import graph.model.Edge;
import graph.util.Delaunay;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import labeledprojection.model.LabeledProjectionModel;
import labeledprojection.stress.ChecksDialog;
import labeledprojection.stress.StressCurveComp;
import labeledprojection.util.KmeansClusters;
import labeledprojection.util.LabeledProjectionUtil;
import labeledprojection.util.ProjectionConnectivity;
import labeledprojection.util.ProjectionOpenDialog;
import labeledprojection.view.ReportView;
import labeledprojection.view.forms.ChangeOrderAnchorDialog;
import labeledprojection.view.forms.DragDropListItem;
import labeledprojection.view.forms.SaveClassDialog;
import labeledprojection.view.forms.klassification.SVMClassifyDialog;
import labeledprojection.view.selection.ClassAbstractSelection;
import labeledprojection.view.selection.InstanceSelection;
import labeledprojection.view.tools.ProjectionMultidimensionalClusteringView;
import labeledprojection.view.tools.silhouette.DataSilhouetteCoefficientView;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import projection.model.ProjectionInstance;
import projection.model.Scalar;
import projection.technique.idmap.IDMAPProjection.InitializationType;
import projection.technique.idmap.IDMAPProjectionComp;
import projection.util.ProjectionConstants;
import projection.util.ProjectionUtil;
import projection.util.filter.XMLFilter;
import projection.view.JExtendedComboBox;
import projection.view.selection.RangeSelection;
import projection.view.selection.coordination.CoordinationSelectionFactory;
import projection.view.tools.JoinScalars;
import textprocessing.corpus.Corpus;
import textprocessing.corpus.zip.ZipCorpus;
import textprocessing.processing.PreprocessorComp;
import topics.Topic;
import topics.TopicClusters;
import topics.TopicFactory.TopicType;
import topics.model.TopicProjectionInstance;
import topics.model.TopicProjectionModel;
import topics.model.TopicProjectionModelComp;
import topics.model.XMLTopicModelReader;
import topics.model.XMLTopicModelWriter;
import topics.selection.TopicClassSelection;
import topics.selection.TopicSelection;
import topics.util.OpenDialog;
import visualizationbasics.color.ColorScalePanel;
import visualizationbasics.coordination.AbstractCoordinator;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.model.AbstractModel;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.SaveDialog;
import visualizationbasics.util.filter.AbstractFilter;
import visualizationbasics.util.filter.DATAFilter;
import visualizationbasics.util.filter.DMATFilter;
import visualizationbasics.util.filter.PNGFilter;
import visualizationbasics.util.filter.SCALARFilter;
import visualizationbasics.view.JFrameModelViewer;
import visualizationbasics.view.MemoryCheck;
import visualizationbasics.view.MessageDialog;
import visualizationbasics.view.selection.AbstractSelection;


/**
 *
 * @author Fernando Vieira Paulovich
 */
public class TopicProjectionFrame extends JFrameModelViewer {

    /** Creates new form TopicProjectionFrame */
    public TopicProjectionFrame() {
        this.scalarComboModel = new DefaultComboBoxModel();
        this.edgeComboModel = new DefaultComboBoxModel();
        this.view = new ViewPanel();

        initComponents();
        initExtraFields();

        InstanceSelection t = new InstanceSelection(this);
        addSelection(t);
        this.view.setSelection(t);
        addSelection(new TopicSelection(this));
//        addSelection(new ViewContentSelection(this));
        addSelection(new RangeSelection(this));
        addSelection(new TopicClassSelection(this),false);
        
        edgeLabel.setVisible(false);
        edgeCombo.setVisible(false);
        
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        selectionButtonGroup = new javax.swing.ButtonGroup();
        controlPanel = new javax.swing.JPanel();
        verticaltoolbarPanel = new javax.swing.JPanel();
        selectionToolBar = new javax.swing.JToolBar();
        fixedToolBar = new javax.swing.JToolBar();
        moveInstancesToggleButton = new javax.swing.JToggleButton();
        cleanInstancesButton = new javax.swing.JButton();
        showTopicsToggleButton = new javax.swing.JToggleButton();
        showallTopicsToggleButton = new javax.swing.JToggleButton();
        cleanButton = new javax.swing.JButton();
        changeAnchorOrderButton = new javax.swing.JButton();
        windowSplitPane = new javax.swing.JSplitPane();
        pointsPanel = new javax.swing.JPanel();
        allPointsPanel = new javax.swing.JPanel();
        dataSearchPanel = new javax.swing.JPanel();
        searchLabel = new javax.swing.JLabel();
        searchTextField = new javax.swing.JTextField();
        goButton = new javax.swing.JButton();
        scrollPanePoints = new javax.swing.JScrollPane();
        pointsList = new javax.swing.JList(plistmodel);
        optionTabbedPane = new javax.swing.JTabbedPane();
        contentPanel = new javax.swing.JPanel();
        fileContentScrollPane = new javax.swing.JScrollPane();
        fileContentEditorPane = new javax.swing.JEditorPane();
        fileTitleTextField = new javax.swing.JTextField();
        dataPanel = new javax.swing.JPanel();
        scrollPanel = new javax.swing.JScrollPane(this.view);
        scalarPanel = new javax.swing.JPanel();
        colorLabel = new javax.swing.JLabel();
        scalarCombo = new JExtendedComboBox(this.scalarComboModel);
        edgeLabel = new javax.swing.JLabel();
        edgeCombo = new JExtendedComboBox(this.edgeComboModel);
        spaceLabel = new javax.swing.JLabel();
        titleLabel = new javax.swing.JLabel();
        titleComboBox = new JComboBox(new String[]{"Title","File name"});
        statusBar_jPanel = new javax.swing.JPanel();
        status_jLabel = new javax.swing.JLabel();
        horizontalToolbarPanel = new javax.swing.JPanel();
        toolBar = new javax.swing.JToolBar();
        openButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        separatorLabel1 = new javax.swing.JLabel();
        zoomInButton = new javax.swing.JButton();
        zoomOutButton = new javax.swing.JButton();
        separatorLabel2 = new javax.swing.JLabel();
        toolButton = new javax.swing.JButton();
        separatorLabel5 = new javax.swing.JLabel();
        searchToolBar = new javax.swing.JToolBar();
        findPanel = new javax.swing.JPanel();
        findTextField = new javax.swing.JTextField();
        findButton = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        fileOpen = new javax.swing.JMenuItem();
        fileSave = new javax.swing.JMenuItem();
        separator1 = new javax.swing.JSeparator();
        exportMenu = new javax.swing.JMenu();
        fileExportToPng = new javax.swing.JMenuItem();
        fileExportToProjection = new javax.swing.JMenuItem();
        menuEdit = new javax.swing.JMenu();
        editClean = new javax.swing.JMenuItem();
        editDelete = new javax.swing.JMenuItem();
        menuTool = new javax.swing.JMenu();
        memoryCheckMenuItem = new javax.swing.JMenuItem();
        separatorOptions1 = new javax.swing.JSeparator();
        scalarMenu = new javax.swing.JMenu();
        importScalarsOption = new javax.swing.JMenuItem();
        exportScalarsOption = new javax.swing.JMenuItem();
        joinScalarsOptions = new javax.swing.JMenuItem();
        separatorOptions2 = new javax.swing.JSeparator();
        clusteringMenu = new javax.swing.JMenu();
        multidimensionalMenuItem = new javax.swing.JMenuItem();
        silhouetteCoefficientMenuItem = new javax.swing.JMenuItem();
        separatorOptions4 = new javax.swing.JSeparator();
        topicMenu = new javax.swing.JMenu();
        clusterTopicsMenuItem = new javax.swing.JMenuItem();
        separatorOptions3 = new javax.swing.JSeparator();
        createHCMenu = new javax.swing.JMenu();
        sLinkMenuItem = new javax.swing.JMenuItem();
        cLinkMenuItem = new javax.swing.JMenuItem();
        aLinkMenuItem = new javax.swing.JMenuItem();
        separatorOptions5 = new javax.swing.JPopupMenu.Separator();
        delaunayMenuItem = new javax.swing.JMenuItem();
        separatorOptions6 = new javax.swing.JPopupMenu.Separator();
        viewRadVizMenuItem = new javax.swing.JCheckBoxMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        toolOptions = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        controlPanel.setLayout(new java.awt.BorderLayout());

        verticaltoolbarPanel.setLayout(new java.awt.BorderLayout(0, 20));

        selectionToolBar.setOrientation(javax.swing.SwingConstants.VERTICAL);
        verticaltoolbarPanel.add(selectionToolBar, java.awt.BorderLayout.CENTER);

        fixedToolBar.setOrientation(javax.swing.SwingConstants.VERTICAL);
        fixedToolBar.setRollover(true);

        moveInstancesToggleButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/navigation/Forward16.gif"))); // NOI18N
        moveInstancesToggleButton.setToolTipText("Move Point");
        moveInstancesToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveInstancesToggleButtonActionPerformed(evt);
            }
        });
        fixedToolBar.add(moveInstancesToggleButton);

        cleanInstancesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Edit16.gif"))); // NOI18N
        cleanInstancesButton.setToolTipText("Clean Instances");
        cleanInstancesButton.setFocusable(false);
        cleanInstancesButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cleanInstancesButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        cleanInstancesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cleanInstancesButtonActionPerformed(evt);
            }
        });
        fixedToolBar.add(cleanInstancesButton);

        showTopicsToggleButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        showTopicsToggleButton.setText(" H ");
        showTopicsToggleButton.setFocusable(false);
        showTopicsToggleButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        showTopicsToggleButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        showTopicsToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showTopicsToggleButtonActionPerformed(evt);
            }
        });
        fixedToolBar.add(showTopicsToggleButton);

        showallTopicsToggleButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        showallTopicsToggleButton.setText(" S ");
        showallTopicsToggleButton.setFocusable(false);
        showallTopicsToggleButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        showallTopicsToggleButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        showallTopicsToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showallTopicsToggleButtonActionPerformed(evt);
            }
        });
        fixedToolBar.add(showallTopicsToggleButton);

        cleanButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cleanButton.setText(" C ");
        cleanButton.setAlignmentY(0.0F);
        cleanButton.setFocusable(false);
        cleanButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cleanButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        cleanButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cleanButtonActionPerformed(evt);
            }
        });
        fixedToolBar.add(cleanButton);

        changeAnchorOrderButton.setText("Rad");
        changeAnchorOrderButton.setToolTipText("Change RadViz Anchors Order");
        changeAnchorOrderButton.setFocusable(false);
        changeAnchorOrderButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        changeAnchorOrderButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        changeAnchorOrderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeAnchorOrderButtonActionPerformed(evt);
            }
        });
        fixedToolBar.add(changeAnchorOrderButton);

        verticaltoolbarPanel.add(fixedToolBar, java.awt.BorderLayout.NORTH);

        controlPanel.add(verticaltoolbarPanel, java.awt.BorderLayout.EAST);

        windowSplitPane.setDividerLocation(250);
        windowSplitPane.setOneTouchExpandable(true);

        pointsPanel.setPreferredSize(new java.awt.Dimension(100, 256));
        pointsPanel.setLayout(new java.awt.GridLayout(2, 0));

        allPointsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Data Instances"));
        allPointsPanel.setPreferredSize(new java.awt.Dimension(100, 107));
        allPointsPanel.setLayout(new java.awt.BorderLayout(5, 5));

        dataSearchPanel.setPreferredSize(new java.awt.Dimension(10, 23));
        dataSearchPanel.setLayout(new java.awt.BorderLayout(5, 5));

        searchLabel.setText("Search");
        dataSearchPanel.add(searchLabel, java.awt.BorderLayout.WEST);

        searchTextField.setColumns(10);
        searchTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                searchTextFieldKeyPressed(evt);
            }
        });
        dataSearchPanel.add(searchTextField, java.awt.BorderLayout.CENTER);

        goButton.setText("...");
        goButton.setMaximumSize(new java.awt.Dimension(29, 23));
        goButton.setMinimumSize(new java.awt.Dimension(29, 23));
        goButton.setPreferredSize(new java.awt.Dimension(29, 23));
        goButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                goButtonActionPerformed(evt);
            }
        });
        dataSearchPanel.add(goButton, java.awt.BorderLayout.EAST);

        allPointsPanel.add(dataSearchPanel, java.awt.BorderLayout.NORTH);

        scrollPanePoints.setBorder(javax.swing.BorderFactory.createTitledBorder("Labels"));

        pointsList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        pointsList.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        pointsList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pointsListMouseClicked(evt);
            }
        });
        scrollPanePoints.setViewportView(pointsList);

        allPointsPanel.add(scrollPanePoints, java.awt.BorderLayout.CENTER);

        pointsPanel.add(allPointsPanel);

        optionTabbedPane.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);

        contentPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("File Content"));
        contentPanel.setLayout(new java.awt.BorderLayout(3, 3));

        fileContentScrollPane.setAutoscrolls(true);

        fileContentEditorPane.setEditable(false);
        fileContentScrollPane.setViewportView(fileContentEditorPane);

        contentPanel.add(fileContentScrollPane, java.awt.BorderLayout.CENTER);

        fileTitleTextField.setEditable(false);
        contentPanel.add(fileTitleTextField, java.awt.BorderLayout.NORTH);

        optionTabbedPane.addTab("Content", contentPanel);

        pointsPanel.add(optionTabbedPane);

        windowSplitPane.setLeftComponent(pointsPanel);

        dataPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        dataPanel.setLayout(new java.awt.BorderLayout());
        dataPanel.add(scrollPanel, java.awt.BorderLayout.CENTER);

        scalarPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        colorLabel.setText("Color");
        scalarPanel.add(colorLabel);

        scalarCombo.setMaximumSize(new java.awt.Dimension(85, 27));
        scalarCombo.setMinimumSize(new java.awt.Dimension(85, 27));
        scalarCombo.setPreferredSize(new java.awt.Dimension(85, 27));
        scalarCombo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                scalarComboMouseClicked(evt);
            }
        });
        scalarCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                scalarComboItemStateChanged(evt);
            }
        });
        scalarPanel.add(scalarCombo);

        edgeLabel.setText("Edge");
        scalarPanel.add(edgeLabel);

        edgeCombo.setPreferredSize(new java.awt.Dimension(85, 27));
        edgeCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                edgeComboItemStateChanged(evt);
            }
        });
        edgeCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edgeComboActionPerformed(evt);
            }
        });
        scalarPanel.add(edgeCombo);

        spaceLabel.setText("   ");
        scalarPanel.add(spaceLabel);

        titleLabel.setText("Title");
        scalarPanel.add(titleLabel);

        titleComboBox.setMaximumSize(new java.awt.Dimension(85, 27));
        titleComboBox.setMinimumSize(new java.awt.Dimension(85, 27));
        titleComboBox.setPreferredSize(new java.awt.Dimension(85, 27));
        titleComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                titleComboBoxMouseClicked(evt);
            }
        });
        titleComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                titleComboBoxItemStateChanged(evt);
            }
        });
        titleComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                titleComboBoxActionPerformed(evt);
            }
        });
        scalarPanel.add(titleComboBox);

        dataPanel.add(scalarPanel, java.awt.BorderLayout.NORTH);

        windowSplitPane.setRightComponent(dataPanel);

        controlPanel.add(windowSplitPane, java.awt.BorderLayout.CENTER);

        statusBar_jPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        statusBar_jPanel.setPreferredSize(new java.awt.Dimension(30, 30));

        status_jLabel.setText("                      ");
        statusBar_jPanel.add(status_jLabel);

        controlPanel.add(statusBar_jPanel, java.awt.BorderLayout.PAGE_END);

        getContentPane().add(controlPanel, java.awt.BorderLayout.CENTER);

        horizontalToolbarPanel.setLayout(new java.awt.BorderLayout());

        openButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Open16.gif"))); // NOI18N
        openButton.setToolTipText("Open an existing projection");
        openButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openButtonActionPerformed(evt);
            }
        });
        toolBar.add(openButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Save16.gif"))); // NOI18N
        saveButton.setToolTipText("Save the current projection");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        toolBar.add(saveButton);

        separatorLabel1.setText("       ");
        toolBar.add(separatorLabel1);

        zoomInButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/ZoomIn16.gif"))); // NOI18N
        zoomInButton.setToolTipText("Zoom in");
        zoomInButton.setMaximumSize(new java.awt.Dimension(29, 27));
        zoomInButton.setMinimumSize(new java.awt.Dimension(29, 27));
        zoomInButton.setPreferredSize(new java.awt.Dimension(29, 27));
        zoomInButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInButtonActionPerformed(evt);
            }
        });
        toolBar.add(zoomInButton);

        zoomOutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/ZoomOut16.gif"))); // NOI18N
        zoomOutButton.setToolTipText("Zoom out");
        zoomOutButton.setMaximumSize(new java.awt.Dimension(29, 27));
        zoomOutButton.setMinimumSize(new java.awt.Dimension(29, 27));
        zoomOutButton.setPreferredSize(new java.awt.Dimension(29, 27));
        zoomOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutButtonActionPerformed(evt);
            }
        });
        toolBar.add(zoomOutButton);

        separatorLabel2.setText("       ");
        toolBar.add(separatorLabel2);

        toolButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Preferences16.gif"))); // NOI18N
        toolButton.setToolTipText("Tool Preferences");
        toolButton.setMaximumSize(new java.awt.Dimension(29, 27));
        toolButton.setMinimumSize(new java.awt.Dimension(29, 27));
        toolButton.setPreferredSize(new java.awt.Dimension(29, 27));
        toolButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toolButtonActionPerformed(evt);
            }
        });
        toolBar.add(toolButton);

        separatorLabel5.setText("       ");
        toolBar.add(separatorLabel5);

        horizontalToolbarPanel.add(toolBar, java.awt.BorderLayout.WEST);

        searchToolBar.setRollover(true);

        findPanel.setOpaque(false);
        findPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        findTextField.setColumns(10);
        findTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                findTextFieldKeyPressed(evt);
            }
        });
        findPanel.add(findTextField);

        findButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Find16.gif"))); // NOI18N
        findButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                findButtonActionPerformed(evt);
            }
        });
        findPanel.add(findButton);

        searchToolBar.add(findPanel);

        horizontalToolbarPanel.add(searchToolBar, java.awt.BorderLayout.CENTER);

        getContentPane().add(horizontalToolbarPanel, java.awt.BorderLayout.NORTH);

        menuFile.setMnemonic('F');
        menuFile.setText("File");

        fileOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        fileOpen.setMnemonic('O');
        fileOpen.setText("Open Projection");
        fileOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileOpenActionPerformed(evt);
            }
        });
        menuFile.add(fileOpen);

        fileSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        fileSave.setMnemonic('S');
        fileSave.setText("Save Projection");
        fileSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileSaveActionPerformed(evt);
            }
        });
        menuFile.add(fileSave);
        menuFile.add(separator1);

        exportMenu.setText("Export");

        fileExportToPng.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        fileExportToPng.setMnemonic('P');
        fileExportToPng.setText("Export PNG File");
        fileExportToPng.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileExportToPngActionPerformed(evt);
            }
        });
        exportMenu.add(fileExportToPng);

        fileExportToProjection.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        fileExportToProjection.setMnemonic('J');
        fileExportToProjection.setText("Export 2D Points File");
        fileExportToProjection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileExportToProjectionActionPerformed(evt);
            }
        });
        exportMenu.add(fileExportToProjection);

        menuFile.add(exportMenu);

        menuBar.add(menuFile);

        menuEdit.setMnemonic('E');
        menuEdit.setText("Edit");

        editClean.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        editClean.setMnemonic('C');
        editClean.setText("Clean Projection");
        editClean.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editCleanActionPerformed(evt);
            }
        });
        menuEdit.add(editClean);

        editDelete.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_DELETE, 0));
        editDelete.setMnemonic('D');
        editDelete.setText("Delete Points");
        editDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editDeleteActionPerformed(evt);
            }
        });
        menuEdit.add(editDelete);

        menuBar.add(menuEdit);

        menuTool.setMnemonic('T');
        menuTool.setText("Tool");

        memoryCheckMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        memoryCheckMenuItem.setMnemonic('H');
        memoryCheckMenuItem.setText("Memory Check");
        memoryCheckMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                memoryCheckMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(memoryCheckMenuItem);
        menuTool.add(separatorOptions1);

        scalarMenu.setText("Scalar");

        importScalarsOption.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        importScalarsOption.setMnemonic('S');
        importScalarsOption.setText("Import Scalars");
        importScalarsOption.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importScalarsOptionActionPerformed(evt);
            }
        });
        scalarMenu.add(importScalarsOption);

        exportScalarsOption.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        exportScalarsOption.setMnemonic('x');
        exportScalarsOption.setText("Export Scalars");
        exportScalarsOption.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportScalarsOptionActionPerformed(evt);
            }
        });
        scalarMenu.add(exportScalarsOption);

        joinScalarsOptions.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_J, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        joinScalarsOptions.setMnemonic('J');
        joinScalarsOptions.setText("Join Scalars");
        joinScalarsOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                joinScalarsOptionsActionPerformed(evt);
            }
        });
        scalarMenu.add(joinScalarsOptions);

        menuTool.add(scalarMenu);
        menuTool.add(separatorOptions2);

        clusteringMenu.setText("Clustering");

        multidimensionalMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        multidimensionalMenuItem.setText("Multidimensional Data");
        multidimensionalMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                multidimensionalMenuItemActionPerformed(evt);
            }
        });
        clusteringMenu.add(multidimensionalMenuItem);

        silhouetteCoefficientMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        silhouetteCoefficientMenuItem.setText("Silhouette Coefficient");
        silhouetteCoefficientMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                silhouetteCoefficientMenuItemActionPerformed(evt);
            }
        });
        clusteringMenu.add(silhouetteCoefficientMenuItem);

        menuTool.add(clusteringMenu);
        menuTool.add(separatorOptions4);

        topicMenu.setText("Topic");

        clusterTopicsMenuItem.setText("Extract Topics Clustering");
        clusterTopicsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clusterTopicsMenuItemActionPerformed(evt);
            }
        });
        topicMenu.add(clusterTopicsMenuItem);

        menuTool.add(topicMenu);
        menuTool.add(separatorOptions3);

        createHCMenu.setText("Calculate Hierarchical Clustering");

        sLinkMenuItem.setText("Single Link");
        sLinkMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sLinkMenuItemActionPerformed(evt);
            }
        });
        createHCMenu.add(sLinkMenuItem);

        cLinkMenuItem.setText("Complete Link");
        cLinkMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cLinkMenuItemActionPerformed(evt);
            }
        });
        createHCMenu.add(cLinkMenuItem);

        aLinkMenuItem.setText("Average Link");
        aLinkMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aLinkMenuItemActionPerformed(evt);
            }
        });
        createHCMenu.add(aLinkMenuItem);

        menuTool.add(createHCMenu);
        menuTool.add(separatorOptions5);

        delaunayMenuItem.setText("Create Delaunay Triangulation");
        delaunayMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delaunayMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(delaunayMenuItem);
        menuTool.add(separatorOptions6);

        viewRadVizMenuItem.setText("View RadViz Anchors ");
        viewRadVizMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewRadVizMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(viewRadVizMenuItem);
        menuTool.add(jSeparator1);

        toolOptions.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        toolOptions.setMnemonic('O');
        toolOptions.setText("Tool Options");
        toolOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toolOptionsActionPerformed(evt);
            }
        });
        menuTool.add(toolOptions);

        menuBar.add(menuTool);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fileOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileOpenActionPerformed
        try {
            PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
            int result = OpenDialog.showOpenDialog(spm, new XMLFilter(), this);

            if (result == JFileChooser.APPROVE_OPTION) {
                String filename = OpenDialog.getFilename();

                try {
                    XMLTopicModelReader mreader = new XMLTopicModelReader();
                    TopicProjectionModel newmodel = new TopicProjectionModel();
                    mreader.read(newmodel, filename);
                    setModel(newmodel);
                } catch (IOException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    JOptionPane.showMessageDialog(this, e.getMessage(),
                            "Problems saving the file", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_fileOpenActionPerformed

    private void fileSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileSaveActionPerformed
        if (model != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
                int result = SaveDialog.showSaveDialog(spm, new XMLFilter(), this, "model.xml");

                if (result == JFileChooser.APPROVE_OPTION) {
                    String filename = SaveDialog.getFilename();

                    try {
                        XMLTopicModelWriter mwriter = new XMLTopicModelWriter();
                        mwriter.write((TopicProjectionModel) model, filename);
                    } catch (IOException e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        JOptionPane.showMessageDialog(this, e.getMessage(),
                                "Problems saving the file", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_fileSaveActionPerformed

    private void fileExportToPngActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileExportToPngActionPerformed
        try {
            PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
            int result = SaveDialog.showSaveDialog(spm, new PNGFilter(), this, "image.png");

            if (result == JFileChooser.APPROVE_OPTION) {
                String filename = SaveDialog.getFilename();

                try {
                    view.saveToPngImageFile(filename);
                } catch (IOException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    JOptionPane.showMessageDialog(this, e.getMessage(),
                            "Problems saving the file", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_fileExportToPngActionPerformed

    private void fileExportToProjectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileExportToProjectionActionPerformed
        if (model != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
                int result = SaveDialog.showSaveDialog(spm, new DATAFilter(), this, "projection.data");

                if (result == JFileChooser.APPROVE_OPTION) {
                    String filename = SaveDialog.getFilename();

                    try {
                        AbstractMatrix matrix = ProjectionUtil.modelToMatrix((TopicProjectionModel) model, getCurrentScalar());
                        matrix.save(filename);
                    } catch (IOException e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        JOptionPane.showMessageDialog(this, e.getMessage(),
                                "Problems saving the file", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_fileExportToProjectionActionPerformed

    private void editCleanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editCleanActionPerformed
        if (view != null) {
            view.cleanSelectedInstances();
        }
}//GEN-LAST:event_editCleanActionPerformed

    private void editDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editDeleteActionPerformed
        if (view != null) {
            view.removeSelectedInstances();
        }
}//GEN-LAST:event_editDeleteActionPerformed

    private void memoryCheckMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_memoryCheckMenuItemActionPerformed
        MemoryCheck.showMemoryCheck();
}//GEN-LAST:event_memoryCheckMenuItemActionPerformed

    private void importScalarsOptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importScalarsOptionActionPerformed
        if (view != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
                int result = OpenDialog.showOpenDialog(spm, new SCALARFilter(), this);

                if (result == javax.swing.JFileChooser.APPROVE_OPTION) {
                    if (model != null) {
                        final MessageDialog dialog = MessageDialog.show(this, "Importing scalars...");

                        Thread t = new Thread() {

                            @Override
                            public void run() {
                                try {
                                    String filename = OpenDialog.getFilename();
                                    //ProjectionUtil.importScalars((TopicProjectionModel) model, filename);
                                    LabeledProjectionUtil.importScalars((TopicProjectionModel) model, filename);
                                    updateScalars(null);
                                } catch (IOException ex) {
                                    Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
                                } finally {
                                    dialog.close();
                                }
                            }

                        };

                        t.start();
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_importScalarsOptionActionPerformed

    private void exportScalarsOptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportScalarsOptionActionPerformed
        if (view != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
                int result = SaveDialog.showSaveDialog(spm, new SCALARFilter(), this, "scalars.scalar");

                if (result == JFileChooser.APPROVE_OPTION) {
                    if (model != null) {
                        String filename = SaveDialog.getFilename();
                        //ProjectionUtil.exportScalars((TopicProjectionModel) model, filename);
                        LabeledProjectionUtil.exportScalars((TopicProjectionModel) model, filename);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_exportScalarsOptionActionPerformed

    private void toolOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toolOptionsActionPerformed
        TopicProjectionFrameOptions.getInstance(this).display(this);
}//GEN-LAST:event_toolOptionsActionPerformed

    private void toolButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toolButtonActionPerformed
        toolOptionsActionPerformed(evt);
}//GEN-LAST:event_toolButtonActionPerformed

    private void zoomOutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomOutButtonActionPerformed
        if (view != null) {
            view.zoomOut();
        }
}//GEN-LAST:event_zoomOutButtonActionPerformed

    private void zoomInButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomInButtonActionPerformed
        if (view != null) {
            view.zoomIn();
        }
}//GEN-LAST:event_zoomInButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        fileSaveActionPerformed(evt);
}//GEN-LAST:event_saveButtonActionPerformed

    private void openButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openButtonActionPerformed
        fileOpenActionPerformed(evt);
}//GEN-LAST:event_openButtonActionPerformed

    private void moveInstancesToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moveInstancesToggleButtonActionPerformed
        moveinstances = moveInstancesToggleButton.isSelected();
}//GEN-LAST:event_moveInstancesToggleButtonActionPerformed

    private void cleanInstancesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cleanInstancesButtonActionPerformed
        editCleanActionPerformed(evt);
}//GEN-LAST:event_cleanInstancesButtonActionPerformed

    private void joinScalarsOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_joinScalarsOptionsActionPerformed
        if (model != null) {
            Scalar s = JoinScalars.getInstance(this).display((TopicProjectionModel) model);
            updateScalars(s);
        }
}//GEN-LAST:event_joinScalarsOptionsActionPerformed

    private void multidimensionalMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_multidimensionalMenuItemActionPerformed
        if (model != null) {
            Scalar s = ProjectionMultidimensionalClusteringView.getInstance(this).display((TopicProjectionModel) model);
            updateScalars(s);
        }
}//GEN-LAST:event_multidimensionalMenuItemActionPerformed

    private void silhouetteCoefficientMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_silhouetteCoefficientMenuItemActionPerformed
        if (model != null) {
            try {
                DataSilhouetteCoefficientView.getInstance(this).display((TopicProjectionModel) model, getCurrentScalar());
                updateScalars(null);
            } catch (IOException ex) {
                Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_silhouetteCoefficientMenuItemActionPerformed

    private void searchTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchTextFieldKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            this.goButtonActionPerformed(null);
        }
}//GEN-LAST:event_searchTextFieldKeyPressed

    private void goButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_goButtonActionPerformed
        if (model != null) {
            Pattern p = Pattern.compile(this.searchTextField.getText().trim().toLowerCase());

            int begin = this.pointsList.getSelectedIndex() + 1;
            int end = model.getInstances().size() - 1;

            boolean stop = false;
            boolean restart = true;
            while (!stop) {
                for (int i = begin; i <= end; i++) {
                    Matcher m = p.matcher(plistmodel.get(i).toString().trim().toLowerCase());
                    if (m.find()) {
                        pointsList.setSelectedIndex(i);
                        pointsList.ensureIndexIsVisible(i);
                        model.setSelectedInstance((TopicProjectionInstance) plistmodel.get(i));
                        model.notifyObservers();
                        stop = true;
                        break;
                    }
                }

                if (restart) {
                    end = begin - 2;
                    begin = 0;
                    restart = false;
                } else {
                    stop = true;
                }
            }
        }
}//GEN-LAST:event_goButtonActionPerformed

    private void pointsListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pointsListMouseClicked
        if (model != null) {
            javax.swing.JList source = (javax.swing.JList) evt.getSource();
            TopicProjectionInstance instance = (TopicProjectionInstance) source.getSelectedValue();

            if (evt.getClickCount() == 1) {
                try {
                    Corpus cp = null;
                    if (((TopicProjectionModel)model).getCorpus() == null) {
                        cp = OpenDialog.checkCorpus(((TopicProjectionModel)model),this);
                        ((TopicProjectionModel)model).setCorpus(cp);
                        if (cp != null) ((ReportView)TopicProjectionFrame.this.getReportPanel()).setDataSource(cp.getUrl());
                    } else
                        cp = ((TopicProjectionModel)model).getCorpus();

                    if (cp != null) {
                        showContent(((TopicProjectionModel) model).getCorpus(), instance);
                    }

                    model.setSelectedInstance((TopicProjectionInstance) pointsList.getSelectedValue());
                    model.notifyObservers();
                } catch (IOException ex) {
                    Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (evt.getClickCount() == 2) {
                try {
                    Corpus cp = null;
                    if (((TopicProjectionModel)model).getCorpus() == null) {
                        cp = OpenDialog.checkCorpus(((TopicProjectionModel)model),this);
                        ((TopicProjectionModel)model).setCorpus(cp);
                        if (cp != null) ((ReportView)TopicProjectionFrame.this.getReportPanel()).setDataSource(cp.getUrl());
                    } else
                        cp = ((TopicProjectionModel)model).getCorpus();

                    if (cp != null) {
                        MultipleFileView.getInstance(this).display(instance,
                                ((TopicProjectionModel) model).getCorpus());
                    }

                    model.setSelectedInstance((TopicProjectionInstance) pointsList.getSelectedValue());
                    model.notifyObservers();
                } catch (IOException ex) {
                    Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
}//GEN-LAST:event_pointsListMouseClicked

    private void showTopicsToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showTopicsToggleButtonActionPerformed
        highlighttopic = showTopicsToggleButton.isSelected();
        view.repaint();
}//GEN-LAST:event_showTopicsToggleButtonActionPerformed

    private void showallTopicsToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showallTopicsToggleButtonActionPerformed
        if (model != null) {
            ((TopicProjectionModel) model).setShowTopics(showallTopicsToggleButton.isSelected());
            model.notifyObservers(Boolean.FALSE);
        }
    }//GEN-LAST:event_showallTopicsToggleButtonActionPerformed

    private void clusterTopicsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clusterTopicsMenuItemActionPerformed
        if (model != null) {
            try {
                TopicClusters tclusters = new TopicClusters();
                tclusters.execute((TopicProjectionModel) model,
                        TopicClusters.ClusteringType.KMEANS, TopicType.COVARIANCE, view);
                updateScalars(tclusters.getScalar());
            } catch (IOException ex) {
                Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_clusterTopicsMenuItemActionPerformed

    private void scalarComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_scalarComboItemStateChanged
        if (evt.getStateChange() == ItemEvent.DESELECTED) {
            Scalar scalar = (Scalar) scalarCombo.getSelectedItem();

            if (scalar != null) {
                view.colorAs(scalar);
            }
        }
    }//GEN-LAST:event_scalarComboItemStateChanged

    private void scalarComboMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_scalarComboMouseClicked
        if (evt.getClickCount() == 2) {
            Scalar scalar = (Scalar) this.scalarCombo.getSelectedItem();

            if (!scalar.getName().equals(ProjectionConstants.DOTS)) {
                scalarComboModel.removeElement(scalar);
                scalarCombo.setSelectedIndex(0);
                ((TopicProjectionModel)model).removeScalar(scalar);
                model.notifyObservers();
            }
        }
    }//GEN-LAST:event_scalarComboMouseClicked

    private void cleanButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cleanButtonActionPerformed
        if (model != null) {
            ((TopicProjectionModel) model).cleanSelectedInstances();
            ((TopicProjectionModel) model).getTopics().clear();
            //setShowTopics(showallTopicsToggleButton.isSelected());
            model.notifyObservers(Boolean.FALSE);
        }
    }//GEN-LAST:event_cleanButtonActionPerformed

    private void findTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_findTextFieldKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            findButtonActionPerformed(null);
        }
}//GEN-LAST:event_findTextFieldKeyPressed

    private void findButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_findButtonActionPerformed
        if (model != null) {
            model.cleanSelectedInstances();

            Pattern p = Pattern.compile(findTextField.getText().trim().toLowerCase());

            int begin = 0;
            int end = model.getInstances().size();

            boolean stop = false;
            boolean restart = true;

            ArrayList<AbstractInstance> foundInstances = new ArrayList<AbstractInstance>();

            while (!stop) {
                for (int i = begin; i < end; i++) {
                    Matcher m = p.matcher(model.getInstances().get(i).toString().trim().toLowerCase());
                    if (m.find()) {
                        //model.setSelectedInstance(model.getInstances().get(i));
                        foundInstances.add(model.getInstances().get(i));
                        stop = true;
                        //break;
                    } else {
                        try {
                            String content = "";
                            if (((TopicProjectionModel) model).getCorpus() == null) {
                                Corpus cp;
                                cp = OpenDialog.checkCorpus((TopicProjectionModel) model, TopicProjectionFrame.this);
                                ((TopicProjectionModel) model).setCorpus(cp);
                                if (cp != null) {
                                    ((ReportView) TopicProjectionFrame.this.getReportPanel()).setDataSource(cp.getUrl());
                                    content = cp.getFullContent(model.getInstances().get(i).toString());
                                }
                            } else {
                                content = ((TopicProjectionModel) model).getCorpus().getFullContent(model.getInstances().get(i).toString());
                            }
                            if (content != null && !content.isEmpty()) {
                                m = p.matcher(content.toLowerCase());
                                if (m.find()) {
                                    //model.setSelectedInstance(model.getInstances().get(i));
                                    foundInstances.add(model.getInstances().get(i));
                                    stop = true;
                                    //break;
                                }
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }

                if (foundInstances != null && !foundInstances.isEmpty())
                    model.setSelectedInstances(foundInstances);

                if (restart) {
                    end = begin - 2;
                    begin = 0;
                    restart = false;
                } else {
                    stop = true;
                }
            }

            model.setChanged();
            model.notifyObservers();
        }
}//GEN-LAST:event_findButtonActionPerformed

    private void titleComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_titleComboBoxMouseClicked
        
}//GEN-LAST:event_titleComboBoxMouseClicked

    private void titleComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_titleComboBoxActionPerformed
        if (((String)titleComboBox.getSelectedItem()).equalsIgnoreCase("Title")) {
            Corpus cp = null;
            if (((TopicProjectionModel)model).getCorpus() == null) {
                try {
                    cp = OpenDialog.checkCorpus(((TopicProjectionModel)model),TopicProjectionFrame.this);
                    if (cp != null) {
                        ((TopicProjectionModel)model).setCorpus(cp);
                        ((ReportView)TopicProjectionFrame.this.getReportPanel()).setDataSource(cp.getUrl());
                    }else {
                        titleComboBox.setSelectedItem("File name");
                    }
                } catch (IOException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
}//GEN-LAST:event_titleComboBoxActionPerformed

    private void edgeComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_edgeComboItemStateChanged

}//GEN-LAST:event_edgeComboItemStateChanged

    private void edgeComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edgeComboActionPerformed
        ProjectionConnectivity conn = (ProjectionConnectivity) this.edgeCombo.getSelectedItem();
        if (conn != null) {
            ((LabeledProjectionModel)model).addProjectionConnectivity(conn);
        }
        model.setChanged();
        model.notifyObservers();
}//GEN-LAST:event_edgeComboActionPerformed

    private void delaunayMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delaunayMenuItemActionPerformed
        ((LabeledProjectionModel)model).perturb();

        //Creating new Delaunay triangulation
        float[][] points = new float[model.getInstances().size()][];
        for (int i = 0; i < points.length; i++) {
            points[i] = new float[2];
            points[i][0] = ((TopicProjectionInstance)model.getInstances().get(i)).getX();
            points[i][1] = ((TopicProjectionInstance)model.getInstances().get(i)).getY();
        }

        Delaunay d = new Delaunay();
        Pair[][] neighborhood = d.execute(points);

        HashMap<Integer, TopicProjectionInstance> index = new HashMap<Integer, TopicProjectionInstance>();
        for (int i=0;i<model.getInstances().size();i++) {
            index.put(i,(TopicProjectionInstance)model.getInstances().get(i));
        }

        ArrayList<Edge> edges = new ArrayList<Edge>();

        for (int i = 0; i < neighborhood.length; i++) {
            for (int j = 0; j < neighborhood[i].length; j++) {
                edges.add(new Edge(index.get(i).getId(),index.get(neighborhood[i][j].index).getId(),neighborhood[i][j].value));
            }
        }

        ProjectionConnectivity delaunayCon = new ProjectionConnectivity("Delaunay",edges);
        ((LabeledProjectionModel)model).addProjectionConnectivity(delaunayCon);

        ProjectionConnectivity dotsCon = new ProjectionConnectivity(ProjectionConstants.DOTS, new ArrayList<Edge>());

        edgeComboModel.removeAllElements();
        edgeComboModel.addElement(dotsCon);
        edgeComboModel.addElement(delaunayCon);
        edgeComboModel.setSelectedItem(delaunayCon);

        edgeLabel.setVisible(true);
        edgeCombo.setVisible(true);

        model.setChanged();
        model.notifyObservers();
    }//GEN-LAST:event_delaunayMenuItemActionPerformed

    private void sLinkMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sLinkMenuItemActionPerformed
        createHCScalar(HierarchicalClusteringType.SLINK);
}//GEN-LAST:event_sLinkMenuItemActionPerformed

    private void cLinkMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cLinkMenuItemActionPerformed
        createHCScalar(HierarchicalClusteringType.CLINK);
}//GEN-LAST:event_cLinkMenuItemActionPerformed

    private void aLinkMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aLinkMenuItemActionPerformed
        createHCScalar(HierarchicalClusteringType.ALINK);
}//GEN-LAST:event_aLinkMenuItemActionPerformed

    private void changeAnchorOrderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeAnchorOrderButtonActionPerformed
        if (((TopicProjectionModel)model).getRadvizStructure() != null) {
            ArrayList<DragDropListItem> colors = ((TopicProjectionModel)model).getRadvizStructure().getColorAnchors();
            if (colors != null && !colors.isEmpty()) {
                ChangeOrderAnchorDialog.getInstance(this,colors).display();
            }
        }
    }//GEN-LAST:event_changeAnchorOrderButtonActionPerformed

    private void viewRadVizMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewRadVizMenuItemActionPerformed
        if (((TopicProjectionModel)model).getRadvizStructure() != null) {
            showRadViz = !showRadViz;
            viewRadVizMenuItem.setSelected(showRadViz);
            model.setChanged();
            model.notifyObservers();
        }else {
            viewRadVizMenuItem.setSelected(false);
        }
    }//GEN-LAST:event_viewRadVizMenuItemActionPerformed

    private void titleComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_titleComboBoxItemStateChanged
        
    }//GEN-LAST:event_titleComboBoxItemStateChanged

    @Override
    public void setModel(AbstractModel model) {
        if (model instanceof TopicProjectionModel) {
            if (model != null) {
                Dimension size = getSize();
                size.height = (int) (size.height * 0.75f);
                size.width = (int) (size.width * 0.95f);
                if (((TopicProjectionModel) model).getRadvizStructure() != null)
                    ((TopicProjectionModel) model).fitRadVizToSize(size);
                else
                    ((TopicProjectionModel) model).fitToSize(size);

                super.setModel(model);

                Scalar scalar = ((TopicProjectionModel) model).getSelectedScalar();

                if (scalar != null) {
                    updateScalars(scalar);
                } else {
                    updateScalars(((TopicProjectionModel) model).getScalars().get(0));
                }

                //setting the lists
                fileTitleTextField.setText("");
                fileContentEditorPane.setText("");

                plistmodel.clear();

                for (int i = 0; i < model.getInstances().size(); i++) {
                    plistmodel.add(i, model.getInstances().get(i));
                }

                pointsList.repaint();

                view.setModel((TopicProjectionModel) model);
            }
        }
    }

    public final void addSelection(final AbstractSelection selection) {
        if (selection != null) {
            JToggleButton button = new JToggleButton();
            selectionButtonGroup.add(button);
            button.setIcon(selection.getIcon());
            button.setSelected(false);
            button.setToolTipText(selection.toString());

            button.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (view != null) {
                        view.setSelection(selection);
                    }
                }

            });

            selectionToolBar.add(button);
        }
    }

    public final void addSelection(final ClassAbstractSelection selection, boolean state) {
        if (selection != null) {
            JToggleButton button = new JToggleButton();
            selectionButtonGroup.add(button);
            button.setIcon(selection.getIcon());
            button.setSelected(false);
            button.setToolTipText(selection.toString());
            button.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (getView() != null) {
                        getView().setSelection(selection);
                        selection.reset();
                    }
                }
            });
            button.setSelected(state);
            selectionToolBar.add(button);
        }
    }

    public void updateScalars(Scalar scalar) {
        scalarComboModel.removeAllElements();
        for (Scalar s : ((TopicProjectionModel) model).getScalars()) {
            scalarComboModel.addElement(s);
        }

        if (scalar != null) {
            scalarCombo.setSelectedItem(scalar);
            ((TopicProjectionModel) model).setSelectedScalar(scalar);
        } else {
            scalarCombo.setSelectedItem(((TopicProjectionModel) model).getSelectedScalar());
        }

        model.setChanged();
        model.notifyObservers();
        pointsList.repaint();
    }

    public void setViewerBackground(Color bg) {
        if (view != null) {
            view.setBackground(bg);
            view.cleanImage();
            view.repaint();
        }
    }

    public Scalar getCurrentScalar() {
        return (Scalar) scalarCombo.getSelectedItem();
    }

    public ViewPanel getView() {
        return view;
    }

    public boolean isHighQualityRender() {
        return highqualityrender;
    }

    public void setHighQualityRender(boolean highqualityrender) {
        this.highqualityrender = highqualityrender;

        view.cleanImage();
        view.repaint();
    }

    public boolean isShowInstanceLabel() {
        return showinstancelabel;
    }

    public void setShowInstanceLabel(boolean showinstancelabel) {
        this.showinstancelabel = showinstancelabel;

        view.cleanImage();
        view.repaint();
    }

    public boolean isMoveInstances() {
        return moveinstances;
    }

    public void setMoveInstance(boolean moveinstances) {
        this.moveinstances = moveinstances;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (model != null) {
            if (arg == null || arg == Boolean.TRUE) {
                view.cleanImage();
            }

            view.repaint();
            pointsList.repaint();
        }
    }

    @Override
    public void addCoordinator(AbstractCoordinator coordinator) {
        super.addCoordinator(coordinator);
        addSelection(CoordinationSelectionFactory.getInstance(coordinator, this));
    }

    public void showContent(Corpus corpus, TopicProjectionInstance instance) {
        if (corpus != null && instance != null) {
            int id = instance.getId();

//            if (filename.endsWith(".html") || filename.endsWith(".htm")) {
//                fileContentEditorPane.setContentType("text/html");
//            }

            fileTitleTextField.setText(instance.toString());
            fileTitleTextField.setCaretPosition(0);

            try {
                fileContentEditorPane.setText(corpus.getViewContent(id));
                fileContentEditorPane.setCaretPosition(0);
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void changeStatus(String status) {
        this.status_jLabel.setText(status);
        this.status_jLabel.update(this.status_jLabel.getGraphics());
        Rectangle r = this.status_jLabel.getGraphicsConfiguration().getBounds();
        //this.status_jLabel.getGraphics().fillRect(r.x, r.y, r.width, r.height);
        this.status_jLabel.getGraphics().clearRect(r.x, r.y, r.width, r.height);
        this.status_jLabel.update(this.status_jLabel.getGraphics());
    }

    private void createHCScalar(HierarchicalClusteringType hierarchicalClusteringType) {
        if (model != null) {
            try {
                javax.swing.JOptionPane.showMessageDialog(this,
                        "The Hierachical Clustering is a very expensive process." +
                        "\nIt can take several minutes!",
                        "WARNING", javax.swing.JOptionPane.WARNING_MESSAGE);

                float[][] projection = new float[model.getInstances().size()][];
                for (int i = 0; i < model.getInstances().size(); i++) {
                    projection[i] = new float[2];
                    projection[i][0] = ((TopicProjectionInstance)model.getInstances().get(i)).getX();
                    projection[i][1] = ((TopicProjectionInstance)model.getInstances().get(i)).getY();
                }

                DenseMatrix dproj = new DenseMatrix();
                for (int i = 0; i < projection.length; i++) {
                    dproj.addRow(new DenseVector(projection[i]));
                }

                HierarchicalClustering hc = new HierarchicalClustering(hierarchicalClusteringType);
                float[] hcScalars = hc.getPointsHeight(dproj, new Euclidean());

                String scalarname = "hc-slink";
                if (hierarchicalClusteringType == HierarchicalClusteringType.ALINK) scalarname = "hc-alink";
                else if (hierarchicalClusteringType == HierarchicalClusteringType.CLINK) scalarname = "hc-clink";

                Scalar scalar = ((LabeledProjectionModel)model).addScalar(scalarname);

                for (int i=0;i<model.getInstances().size();i++)
                    ((TopicProjectionInstance)model.getInstances().get(i)).setScalarValue(scalar, hcScalars[i]);

                this.updateScalars(scalar);
                model.notifyObservers();
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public class ViewPanel extends JPanel {

        public ViewPanel() {
            this.selcolor = java.awt.Color.RED;
            this.setBackground(java.awt.Color.WHITE);

            this.addMouseMotionListener(new MouseMotionListener());
            this.addMouseListener(new MouseClickedListener());

            this.setLayout(new FlowLayout(FlowLayout.LEFT));
        }

        @Override
        public void paintComponent(java.awt.Graphics g) {
            super.paintComponent(g);

            java.awt.Graphics2D g2 = (java.awt.Graphics2D) g;

            if (model != null && image == null) {
                Dimension size = ((TopicProjectionModel) model).getSize();
                image = new BufferedImage(size.width + 10, size.height + 10,
                        BufferedImage.TYPE_INT_RGB);

                java.awt.Graphics2D g2Buffer = image.createGraphics();
                g2Buffer.setColor(this.getBackground());
                g2Buffer.fillRect(0, 0, size.width + 10, size.height + 10);

                if (highqualityrender) {
                    g2Buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);
                } else {
                    g2Buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_OFF);
                }

                if (showRadViz)
                    //drawRadViz(image);
                    ((TopicProjectionModel) model).drawAnchors(image,highqualityrender);

                ((TopicProjectionModel) model).draw(image, highqualityrender);

                g2Buffer.dispose();
            }

            if (image != null) {
                g2.drawImage(image, 0, 0, null);
            }

            //drawing the topics;
            int style = java.awt.Font.PLAIN;
            if (labelBold) style = java.awt.Font.BOLD;
            java.awt.Font f = new java.awt.Font(this.getFont().getName(),style,labelSize);
            for (Topic t : ((TopicProjectionModel) model).getTopics()) {
                //t.drawTopic(g2,getFont(),((TopicProjectionModel) model).isShowTopics());
                t.drawTopic(g2,f,((TopicProjectionModel) model).isShowTopics());
            }

            //Draw he rectangle to select the instances
            if (selsource != null && seltarget != null) {
                int x = selsource.x;
                int width = width = seltarget.x - selsource.x;

                int y = selsource.y;
                int height = seltarget.y - selsource.y;

                if (selsource.x > seltarget.x) {
                    x = seltarget.x;
                    width = selsource.x - seltarget.x;
                }

                if (selsource.y > seltarget.y) {
                    y = seltarget.y;
                    height = selsource.y - seltarget.y;
                }
                g2.setColor(selcolor);
                g2.drawRect(x, y, width, height);

                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.45f));
                g2.fillRect(x, y, width, height);
            } else { //Draw the instance label                
                if (showinstancelabel && label != null && labelpos != null) {
                    if (f == null) {
                        style = java.awt.Font.PLAIN;
                        if (labelBold) style = java.awt.Font.BOLD;
                        f = new java.awt.Font(this.getFont().getName(),style,labelSize);
                    }
                    g2.setFont(f);
                    //g2.setFont(this.getFont());
                    //java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());
                    java.awt.FontMetrics metrics = g2.getFontMetrics(f);

                    //Getting the label size
                    int width = metrics.stringWidth(label);
                    int height = metrics.getAscent();

                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
                    g2.setPaint(java.awt.Color.WHITE);
                    g2.fillRect(labelpos.x - 2, labelpos.y - height, width + 4, height + 4);
                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));

                    g2.setColor(java.awt.Color.DARK_GRAY);
                    g2.drawRect(labelpos.x - 2, labelpos.y - height, width + 4, height + 4);

                    //Drawing the label
                    g2.drawString(label, labelpos.x, labelpos.y);
                }
            }

            //drawn the selection polygon
            if (selpolygon != null) {
                g2.setColor(selcolor);
                g2.drawPolygon(selpolygon);

                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.45f));
                g2.fillPolygon(selpolygon);
            }
        }

        public void saveToPngImageFile(String filename) throws IOException {
            try {
                paint(image.getGraphics());
                ImageIO.write(image, "png", new File(filename));
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }

        public void cleanImage() {
            image = null;
        }

        public void setModel(TopicProjectionModel model) {
            colorscale = new ColorScalePanel(null);
            colorscale.setColorTable(model.getColorTable());
            colorscale.setPreferredSize(new Dimension(200, 12));
            removeAll();
            add(colorscale);

            colorscale.setBackground(getBackground());

            Dimension size = null;

            if (model.getRadvizStructure() != null)
                size = model.getRadvizStructure().getDimension();
            else
                size = model.getSize();
            
            setPreferredSize(new Dimension(size.width * 2, size.height * 2));
            setSize(new Dimension(size.width * 2, size.height * 2));

            cleanImage();
            repaint();
        }

        public void setSelection(AbstractSelection selection) {
            this.selection = selection;
        }

        public void colorAs(Scalar scalar) {
            ((TopicProjectionModel) model).setSelectedScalar(scalar);
            model.notifyObservers();
        }

        public void zoomIn() {
            if (model != null) {
                if (((TopicProjectionModel)model).getRadvizStructure() != null) {
                    Dimension newD = ((TopicProjectionModel)model).getRadvizStructure().getDimension();
                    newD.width *= 1.1f;
                    newD.height *= 1.1f;
                    float[][] radvizProjection = ((TopicProjectionModel)model).getRadvizStructure().updateRadVizValues();
                    for (int i=0;i<((TopicProjectionModel)model).getInstances().size();i++) {
                        TopicProjectionInstance lpi = (TopicProjectionInstance) ((TopicProjectionModel)model).getInstances().get(i);
                        lpi.setX(radvizProjection[i][0]);
                        lpi.setY(radvizProjection[i][1]);
                    }
                    TopicProjectionFrame.this.setModel((TopicProjectionModel)getModel());
                }else
                    ((TopicProjectionModel) model).zoom(1.1f);

                //Change the size of the panel according to the projection
                Dimension size = ((TopicProjectionModel) model).getSize();
                setPreferredSize(new Dimension(size.width * 2, size.height * 2));
                setSize(new Dimension(size.width * 2, size.height * 2));

                model.notifyObservers();
            }
        }

        public void zoomOut() {
            if (model != null) {
                if (((TopicProjectionModel)model).getRadvizStructure() != null) {
                    Dimension newD = ((TopicProjectionModel)model).getRadvizStructure().getDimension();
                    newD.width *= 0.9091f;
                    newD.height *= 0.9091f;
                    float[][] radvizProjection = ((TopicProjectionModel)model).getRadvizStructure().updateRadVizValues();
                    for (int i=0;i<((TopicProjectionModel)model).getInstances().size();i++) {
                        TopicProjectionInstance lpi = (TopicProjectionInstance) ((TopicProjectionModel)model).getInstances().get(i);
                        lpi.setX(radvizProjection[i][0]);
                        lpi.setY(radvizProjection[i][1]);
                    }
                    TopicProjectionFrame.this.setModel((TopicProjectionModel)getModel());
                }else
                    ((TopicProjectionModel) model).zoom(0.9091f);

                //Change the size of the panel according to the projection
                Dimension size = ((TopicProjectionModel) model).getSize();
                setPreferredSize(new Dimension(size.width * 2, size.height * 2));
                setSize(new Dimension(size.width * 2, size.height * 2));

                model.notifyObservers();
            }
        }

        public void adjustPanel() {
            float iniX = ((ProjectionInstance) model.getInstances().get(0)).getX();
            float iniY = ((ProjectionInstance) model.getInstances().get(0)).getY();
            float max_x = iniX, max_y = iniX;
            float min_x = iniY, min_y = iniY;
            int zero = 30;

            for (int i = 1; i < model.getInstances().size(); i++) {
                float x = ((ProjectionInstance) model.getInstances().get(i)).getX();
                if (max_x < x) {
                    max_x = x;
                } else if (min_x > x) {
                    min_x = x;
                }

                float y = ((ProjectionInstance) model.getInstances().get(i)).getY();
                if (max_y < y) {
                    max_y = y;
                } else if (min_y > y) {
                    min_y = y;
                }
            }

            for (AbstractInstance ai : model.getInstances()) {
                ProjectionInstance pi = (ProjectionInstance) ai;
                pi.setX(pi.getX() + zero - min_x);
                pi.setY(pi.getY() + zero - min_y);
            }

            Dimension d = this.getSize();
            d.width = (int) max_x + zero;
            d.height = (int) max_y + zero;
            setSize(d);
            setPreferredSize(d);

            model.notifyObservers();
        }

        public void cleanSelectedInstances() {
            if (model != null) {
                model.cleanSelectedInstances();
                model.notifyObservers();
            }
            for (int i=0;i<getCoordinators().size();i++) {
                getCoordinators().get(i).coordinate(model.getSelectedInstances(),null);
            }
        }

        public void removeSelectedInstances() {
            if (model != null) {
                model.removeSelectedInstances();
                model.notifyObservers();
            }
        }

        public ArrayList<TopicProjectionInstance> getSelectedInstances(Polygon polygon) {
            ArrayList<ProjectionInstance> selected = new ArrayList<ProjectionInstance>();

            if (model != null) {
                selected = ((TopicProjectionModel) model).getInstancesByPosition(polygon);
            }

            ArrayList<TopicProjectionInstance> tselected = new ArrayList<TopicProjectionInstance>();
            for (int i=0;i<selected.size();i++) {
                if (selected.get(i) instanceof TopicProjectionInstance) {
                    tselected.add((TopicProjectionInstance) selected.get(i));
                }
            }

            return tselected;
        }

        public ArrayList<TopicProjectionInstance> getSelectedInstances(Point source, Point target) {
            ArrayList<ProjectionInstance> selinstances = new ArrayList<ProjectionInstance>();

            if (model != null) {
                int x = Math.min(source.x, target.x);
                int width = Math.abs(source.x - target.x);

                int y = Math.min(source.y, target.y);
                int height = Math.abs(source.y - target.y);

                Rectangle rect = new Rectangle(x, y, width, height);
                selinstances = ((TopicProjectionModel) model).getInstancesByPosition(rect);
            }

            ArrayList<TopicProjectionInstance> tselected = new ArrayList<TopicProjectionInstance>();
            for (int i=0;i<selinstances.size();i++) {
                if (selinstances.get(i) instanceof TopicProjectionInstance)
                    tselected.add((TopicProjectionInstance) selinstances.get(i));
            }

            return tselected;
        }

        @Override
        public void setBackground(Color bg) {
            super.setBackground(bg);

            if (this.colorscale != null) {
                this.colorscale.setBackground(bg);
            }
        }

        class MouseMotionListener extends MouseMotionAdapter {

            @Override
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                super.mouseMoved(evt);

                if (model != null) {
                    if (highlighttopic || evt.isControlDown()) {
                        for (Topic t : ((TopicProjectionModel) model).getTopics()) {
                            //t.setShowTopic(false);
                            t.setHighlightTopic(false);
                        }

                        Topic topic = ((TopicProjectionModel) model).getTopicByPosition(evt.getPoint());

                        if (topic != null) {
                            //topic.setShowTopic(true);
                            topic.setHighlightTopic(true);
                        }

                        model.notifyObservers(Boolean.FALSE);
                    } else {
                        ProjectionInstance instance = ((TopicProjectionModel) model).getInstanceByPosition(evt.getPoint());
                        if (instance != null) {
                            //Show the instance label
                            label = getTitle(instance);

                            if (label != null && label.trim().length() > 0) {
                                if (((LabeledProjectionModel)model).getRadvizStructure() != null) {
                                    int row = ((LabeledProjectionModel)model).getRadvizStructure().getInstances().getIds().indexOf(instance.getId());
                                    if (row != -1) {
                                        AbstractVector rowInstance = ((LabeledProjectionModel)model).getRadvizStructure().getInstances().getRow(row);
                                        if (rowInstance != null) {
                                            String l = " [";
                                            for (int i=0;i<rowInstance.size();i++) {
                                                l += Float.toString(rowInstance.getValue(i))+",";
                                            }
                                            if (l.lastIndexOf(",") == l.length()-1)
                                                l = l.substring(0,l.length()-1);
                                            l += "]";
                                            label += l;
                                        }
                                    }
                                }else {
                                    if (label.length() > 100) {
                                        label = label.substring(0, 96) + "...";
                                    }
                                }

                                labelpos = evt.getPoint();
                                repaint();
                            }
                        } else {
                            //Clear the label
                            label = null;
                            labelpos = null;
                            repaint();
                        }
                    }
                }
            }

            @Override
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                if (selinstance != null) {
                    if (model.hasSelectedInstances()) {
                        float x = evt.getX() - selinstance.getX();
                        float y = evt.getY() - selinstance.getY();

                        for (AbstractInstance ai : model.getSelectedInstances()) {
                            ProjectionInstance pi = (ProjectionInstance) ai;
                            pi.setX(x + pi.getX());
                            pi.setY(y + pi.getY());
                        }

                        adjustPanel();
                    }
                } else if (selsource != null) {
                    seltarget = evt.getPoint();
                } else if (selpolygon != null) {
                    selpolygon.addPoint(evt.getX(), evt.getY());
                }

                repaint();
            }

        }

        class MouseClickedListener extends MouseAdapter {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                super.mouseClicked(evt);

                if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
                    if (model != null) {
                        ProjectionInstance instance = ((TopicProjectionModel) model).getInstanceByPosition(evt.getPoint());
                        
                        if (instance != null) {
                            try {
//                                model.setSelectedInstance(instance);
//                                if (selection != null) {
//                                    ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
//                                    instances.add(instance);
//                                    selection.selected(instances);
//                                }
//                                model.notifyObservers();

                                changeStatus("Number of Instances in Selection: " + 1);
                                if (evt.isControlDown()) {
                                    ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
                                    instances.addAll(model.getSelectedInstances());
                                    model.cleanSelectedInstances();
                                    if (instances.contains(instance))
                                        instances.remove(instance);
                                    else
                                        instances.add(instance);
                                    model.setSelectedInstances(instances);
                                    if (selection != null)
                                        selection.selected(instances);
                                    model.setChanged();
                                    float perc = 100.0f * (instances.size() / ((float) model.getInstances().size()));
                                    DecimalFormat df = new DecimalFormat("0.##");
                                    changeStatus("Number of Instances in Selection: " + instances.size() + " (" + df.format(perc) + "%)");
                                }else {
                                    model.setSelectedInstance(instance);
                                    if (selection != null) {
                                        ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
                                        instances.add(instance);
                                        selection.selected(instances);
                                    }
                                    float perc = 100.0f * (1 / ((float) model.getInstances().size()));
                                    DecimalFormat df = new DecimalFormat("0.##");
                                    changeStatus("Number of Instances in Selection: 1 (" + df.format(perc) + "%)");
                                }

                                Corpus cp = null;
                                if (((TopicProjectionModel)model).getCorpus() == null) {
                                    cp = OpenDialog.checkCorpus(((TopicProjectionModel)model),TopicProjectionFrame.this);
                                    ((TopicProjectionModel)model).setCorpus(cp);
                                    if (cp != null) ((ReportView)TopicProjectionFrame.this.getReportPanel()).setDataSource(cp.getUrl());
                                } else
                                    cp = ((TopicProjectionModel)model).getCorpus();

                                if (cp != null) {
                                    if (evt.getClickCount() == 1) {
                                        showContent(((TopicProjectionModel) model).getCorpus(),
                                                (TopicProjectionInstance) instance);
                                    } else {
                                        MultipleFileView.getInstance(TopicProjectionFrame.this).display(instance,
                                                ((TopicProjectionModel) model).getCorpus());
                                    }
                                }
                            } catch (IOException ex) {
                                Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                } else if (evt.getButton() == java.awt.event.MouseEvent.BUTTON3) {
                    cleanSelectedInstances();
                }
            }

            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                super.mousePressed(evt);

                if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
                    if (model != null) {
                        ProjectionInstance instance = ((TopicProjectionModel) model).getInstanceByPosition(evt.getPoint());

                        if (instance != null) {
                            if (moveinstances) {
                                if (model.getSelectedInstances().contains(instance)) {
                                    selinstance = (TopicProjectionInstance) instance;
                                }
                            }
                        } else {
                            selsource = evt.getPoint();
                        }
                    }
                } else if (evt.getButton() == java.awt.event.MouseEvent.BUTTON3) {
                    selpolygon = new java.awt.Polygon();
                    selpolygon.addPoint(evt.getX(), evt.getY());
                }
            }

            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                super.mouseReleased(evt);

                if (model != null) {
                    if ((selsource != null && seltarget != null) || selpolygon != null) {
                        ArrayList<TopicProjectionInstance> instances = null;

                        if (selpolygon != null) {
                            instances = getSelectedInstances(selpolygon);
                        } else {
                            instances = getSelectedInstances(selsource, seltarget);
                        }

                        if (instances != null) {
                            float perc = 100.0f * (instances.size() / ((float) model.getInstances().size()));
                            DecimalFormat df = new DecimalFormat("0.##");
                            changeStatus("Number of Instances in Selection: " + instances.size() + " (" + df.format(perc) + "%)");

                            if (selection != null) {
                                selection.selected(new ArrayList<AbstractInstance>(instances));
                            }
                        }
                    }
                }

                selpolygon = null;
                selinstance = null;
                selsource = null;
                seltarget = null;
            }

        }

        private TopicProjectionInstance selinstance;
        private Polygon selpolygon;
        private Point selsource;
        private Point seltarget;
        private Color selcolor;
        private String label;
        private boolean labelBold = false;
        private int labelSize = 12;
        private Point labelpos;
        private BufferedImage image;
        private ColorScalePanel colorscale;
        private AbstractSelection selection;
    }

    private void initExtraFields() {

        projectionTabbedPane = new javax.swing.JTabbedPane();
        reportPanel = new ReportView();

        dataPanel.remove(scrollPanel);

        projectionTabbedPane.addTab("Projection",scrollPanel);
        projectionTabbedPane.addTab("Report", reportPanel);

        dataPanel.add(projectionTabbedPane, java.awt.BorderLayout.CENTER);

        saveColorButton = new javax.swing.JButton();
        saveColorButton.setText("Save");
        saveColorButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveColorButtonActionPerformed(evt);
            }
        });
        scalarPanel.add(saveColorButton);

        classifyMenu = new javax.swing.JMenu();
        svmMenuItem = new javax.swing.JMenuItem();

        classifyMenu.setText("Classifiying");

        svmMenuItem.setText("Support Vector Machine");
        svmMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                svmMenuItemActionPerformed(evt);
            }
        });
        classifyMenu.add(svmMenuItem);
        menuTool.add(classifyMenu);

        stressMenuItem = new javax.swing.JMenuItem();
        stressMenuItem.setText("Stress Curve");
        stressMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stressMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(stressMenuItem);

        createKmeansLabelsButton = new javax.swing.JButton();
        createKmeansLabelsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/About16.gif"))); // NOI18N
        createKmeansLabelsButton.setToolTipText("Group instances using Kmeans");
        createKmeansLabelsButton.setFocusable(false);
        createKmeansLabelsButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        createKmeansLabelsButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        createKmeansLabelsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createKmeansLabelsButtonActionPerformed(evt);
            }
        });
        selectionToolBar.add(createKmeansLabelsButton);

        pack();
    }

    private void saveColorButtonActionPerformed(java.awt.event.ActionEvent evt) {
        Scalar currentScalar = (Scalar) this.scalarCombo.getSelectedItem();
        if (currentScalar != null) {
            SaveClassDialog.getInstance(this,(LabeledProjectionModel)model,((ReportView)reportPanel).getDataSourceValue()).display(currentScalar);
        }
    }

    private void svmMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        SVMClassifyDialog svmd = SVMClassifyDialog.getInstance(this,model);
        svmd.display();
        if (svmd.getNumberScalars() > 0) {
            for (int i=0;i<svmd.getNumberScalars();i++) {
                this.updateScalars(svmd.getNscalar(i));
            }
            model.notifyObservers();
        }
    }

    private void stressMenuItemActionPerformed(java.awt.event.ActionEvent evt) {

        try {
            StressCurveComp comp = new StressCurveComp();
            comp.input((TopicProjectionModel) model);
            String source = ((ReportView)reportPanel).getDataSourceValue();
//            if (source == null || source.isEmpty() || !source.endsWith(".data")) {
//                PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
//                int result = OpenDialog.showOpenDialog(spm, new DATAFilter(), this);
//                if (result == JFileChooser.APPROVE_OPTION) {
//                    source = OpenDialog.getFilename();
//                }
//            }
//            comp.setDataFileName(source);
//            comp.setDataFileType(0);
            if (source == null || source.isEmpty()) {
                PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
                ArrayList<AbstractFilter> filters = new ArrayList<AbstractFilter>();
                filters.add(new DATAFilter());
                filters.add(new DMATFilter());
                int result = ProjectionOpenDialog.showOpenDialogMultipleFilter(spm, filters, this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    source = OpenDialog.getFilename();
                }
            }
            if (source.endsWith(".data"))
                comp.setDataFileType(0);
            else if (source.endsWith(".dmat"))
                comp.setDataFileType(1);
            comp.setDataFileName(source);
            ChecksDialog.getInstance(this).display(comp);
//            String message = JOptionPane.showInputDialog("Inform a label to the Stress Curve Graphic (optional):");
//            if (message != null) {
//                comp.setGraphicLabel(message);
//                comp.execute();
//            }
        } catch (IOException ex) {
            Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void createKmeansLabelsButtonActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            KmeansClusters labels = new KmeansClusters((TopicProjectionModel)getModel());
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            Scalar kmeansScalar = labels.execute(this);
            if (kmeansScalar != null)
                updateScalars(kmeansScalar);
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        } catch (IOException ex) {
            Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public javax.swing.JPanel getReportPanel() {
        return reportPanel;
    }

    public void setLabelSize(int size) {
        this.view.labelSize = size;
    }

    public void setLabelBold(boolean o) {
        this.view.labelBold = o;
    }

    public int getLabelSize() {
        return this.view.labelSize;
    }

    public boolean getLabelBold() {
        return this.view.labelBold;
    }

//    public String getTitle(String content) throws IOException {
//        String title = "";
//        StringTokenizer tokenizer = new StringTokenizer(content, "\r\n");
//        int i = 0;
//        if (tokenizer.hasMoreTokens()) {
//            String line = tokenizer.nextToken();
//            line = line.replaceAll("<.*?>", "");
//            if (line.trim().length() > 0) {
//                title += line.trim() + " ";
//                i++;
//            }
//        }
//        title = title.trim();
//        return title;
//    }
    
    String getTitle(AbstractInstance instance) {
        String label = null;
        if (titleComboBox.getSelectedItem().toString().equals("Title")) {
            // We don't need all this; the titles are already extracted
            // by default on the TextPreprocessor component
//            try {
//                if (((TopicProjectionModel) model).getCorpus() == null) {
//                    Corpus cp = OpenDialog.checkCorpus(((TopicProjectionModel) model), TopicProjectionFrame.this);
//                    ((TopicProjectionModel) model).setCorpus(cp);
//                    if (cp != null) {
//                        ((ReportView) TopicProjectionFrame.this.getReportPanel()).setDataSource(cp.getUrl());
//                    }
//                    if (cp != null) {
//                        label = getTitle(cp.getFullContent(instance.toString()));
//                    }
//                } else {
//                    label = getTitle(((TopicProjectionModel) model).getCorpus().getFullContent(instance.toString()));
//                }
//            } catch (IOException ex) {
//                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//            }

            label = instance.toString();
        } else {
            if (((TopicProjectionModel) model).getCorpus() != null) {
                label = ((ZipCorpus) ((TopicProjectionModel) model)
                        .getCorpus()).idToUrl(instance.getId());
            }
        }
        return label;
    }

    public static void main(String[] args) {
        try {
            Corpus corpus = new ZipCorpus("D:\\Corpora\\cbr-ilp-ir.zip", 1);
            PreprocessorComp ppcomp = new PreprocessorComp();
            ppcomp.input(corpus);
            ppcomp.execute();
            AbstractMatrix matrix = ppcomp.outputMatrix();
            ArrayList<String> labels = ppcomp.outputLabels();

//            MatrixReaderComp reader = new MatrixReaderComp();
//            reader.setFilename("D:\\Meus documentos\\FERNANDO\\Dados\\cbrilpirson.data");
//            reader.execute();
//            AbstractMatrix matrix = reader.output();

            IDMAPProjectionComp idmap = new IDMAPProjectionComp();
            idmap.setDissimilarityType(DissimilarityType.COSINE_BASED);
            idmap.setFractionDelta(8.0f);
            idmap.setInitialization(InitializationType.FASTMAP);
            idmap.setNumberIterations(50);
            idmap.input(matrix);
            idmap.execute();
            AbstractMatrix proj = idmap.output();

            TopicProjectionModelComp mcomp = new TopicProjectionModelComp();
            //mcomp.input(proj, labels);
            mcomp.input(proj);
            mcomp.execute();
            TopicProjectionModel model = mcomp.output();

            TopicProjectionFrameComp fcomp1 = new TopicProjectionFrameComp();
            fcomp1.input(model);
            fcomp1.execute();
        } catch (IOException ex) {
            Logger.getLogger(TopicProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private DefaultListModel plistmodel = new DefaultListModel();
    private DefaultComboBoxModel scalarComboModel;
    protected DefaultComboBoxModel edgeComboModel;
    private boolean highqualityrender = true;
    private boolean showinstancelabel = true;
    private boolean moveinstances = true;
    private boolean highlighttopic = false;
    private ViewPanel view;
    //Extra fields used to adequate this frame to LabeledProjectionFrame
    private javax.swing.JButton saveColorButton;
    private javax.swing.JMenu classifyMenu;
    private javax.swing.JMenuItem svmMenuItem;
    private javax.swing.JMenuItem stressMenuItem;
    private javax.swing.JButton createKmeansLabelsButton;
    protected javax.swing.JTabbedPane projectionTabbedPane;
    private javax.swing.JPanel reportPanel;
    private DefaultComboBoxModel titlesComboModel = new DefaultComboBoxModel();
    private boolean showRadViz = false;
    //End Extra fields
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aLinkMenuItem;
    private javax.swing.JPanel allPointsPanel;
    private javax.swing.JMenuItem cLinkMenuItem;
    private javax.swing.JButton changeAnchorOrderButton;
    private javax.swing.JButton cleanButton;
    private javax.swing.JButton cleanInstancesButton;
    private javax.swing.JMenuItem clusterTopicsMenuItem;
    private javax.swing.JMenu clusteringMenu;
    private javax.swing.JLabel colorLabel;
    private javax.swing.JPanel contentPanel;
    private javax.swing.JPanel controlPanel;
    private javax.swing.JMenu createHCMenu;
    private javax.swing.JPanel dataPanel;
    private javax.swing.JPanel dataSearchPanel;
    private javax.swing.JMenuItem delaunayMenuItem;
    protected javax.swing.JComboBox edgeCombo;
    protected javax.swing.JLabel edgeLabel;
    private javax.swing.JMenuItem editClean;
    private javax.swing.JMenuItem editDelete;
    private javax.swing.JMenu exportMenu;
    private javax.swing.JMenuItem exportScalarsOption;
    private javax.swing.JEditorPane fileContentEditorPane;
    private javax.swing.JScrollPane fileContentScrollPane;
    private javax.swing.JMenuItem fileExportToPng;
    private javax.swing.JMenuItem fileExportToProjection;
    private javax.swing.JMenuItem fileOpen;
    private javax.swing.JMenuItem fileSave;
    private javax.swing.JTextField fileTitleTextField;
    private javax.swing.JButton findButton;
    private javax.swing.JPanel findPanel;
    private javax.swing.JTextField findTextField;
    private javax.swing.JToolBar fixedToolBar;
    private javax.swing.JButton goButton;
    private javax.swing.JPanel horizontalToolbarPanel;
    private javax.swing.JMenuItem importScalarsOption;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuItem joinScalarsOptions;
    private javax.swing.JMenuItem memoryCheckMenuItem;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuEdit;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuTool;
    private javax.swing.JToggleButton moveInstancesToggleButton;
    private javax.swing.JMenuItem multidimensionalMenuItem;
    private javax.swing.JButton openButton;
    private javax.swing.JTabbedPane optionTabbedPane;
    private javax.swing.JList pointsList;
    private javax.swing.JPanel pointsPanel;
    private javax.swing.JMenuItem sLinkMenuItem;
    private javax.swing.JButton saveButton;
    private javax.swing.JComboBox scalarCombo;
    private javax.swing.JMenu scalarMenu;
    private javax.swing.JPanel scalarPanel;
    private javax.swing.JScrollPane scrollPanePoints;
    private javax.swing.JScrollPane scrollPanel;
    private javax.swing.JLabel searchLabel;
    private javax.swing.JTextField searchTextField;
    private javax.swing.JToolBar searchToolBar;
    private javax.swing.ButtonGroup selectionButtonGroup;
    private javax.swing.JToolBar selectionToolBar;
    private javax.swing.JSeparator separator1;
    private javax.swing.JLabel separatorLabel1;
    private javax.swing.JLabel separatorLabel2;
    private javax.swing.JLabel separatorLabel5;
    private javax.swing.JSeparator separatorOptions1;
    private javax.swing.JSeparator separatorOptions2;
    private javax.swing.JSeparator separatorOptions3;
    private javax.swing.JSeparator separatorOptions4;
    private javax.swing.JPopupMenu.Separator separatorOptions5;
    private javax.swing.JPopupMenu.Separator separatorOptions6;
    private javax.swing.JToggleButton showTopicsToggleButton;
    private javax.swing.JToggleButton showallTopicsToggleButton;
    private javax.swing.JMenuItem silhouetteCoefficientMenuItem;
    private javax.swing.JLabel spaceLabel;
    private javax.swing.JPanel statusBar_jPanel;
    private javax.swing.JLabel status_jLabel;
    javax.swing.JComboBox titleComboBox;
    private javax.swing.JLabel titleLabel;
    private javax.swing.JToolBar toolBar;
    private javax.swing.JButton toolButton;
    private javax.swing.JMenuItem toolOptions;
    private javax.swing.JMenu topicMenu;
    private javax.swing.JPanel verticaltoolbarPanel;
    private javax.swing.JCheckBoxMenuItem viewRadVizMenuItem;
    private javax.swing.JSplitPane windowSplitPane;
    private javax.swing.JButton zoomInButton;
    private javax.swing.JButton zoomOutButton;
    // End of variables declaration//GEN-END:variables
}
