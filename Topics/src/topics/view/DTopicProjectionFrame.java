/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package topics.view;

import javax.swing.DefaultListModel;
import labeledprojection.view.LabeledProjectionFrame;
import topics.Topic;
import topics.model.TopicProjectionModel;
import topics.selection.TopicSelection;

/**
 *
 * @author Jose Gustavo
 */
public class DTopicProjectionFrame extends LabeledProjectionFrame {

    public DTopicProjectionFrame() {
        super();
        //view.setVisible(false);
        super.view = null;
        view = null;
        topicView = new TopicViewPanel();
        addSelection(new TopicSelection(this),false);
        initComponents();
    }

    public void initComponents() {
        //getContentPane().remove(view);
        getContentPane().remove(scrollPanel);
        scrollPanel = new javax.swing.JScrollPane(this.topicView);
        dataPanel.add(scrollPanel, java.awt.BorderLayout.CENTER);
        getContentPane().invalidate();
        getContentPane().validate();

        
        
        
        //dataPanel.repaint();
        showTopicsToggleButton = new javax.swing.JToggleButton();
        showallTopicsToggleButton = new javax.swing.JToggleButton();
        cleanButton = new javax.swing.JButton();

        allPointsPanel = new javax.swing.JPanel();
        dataSearchPanel = new javax.swing.JPanel();
        searchLabel = new javax.swing.JLabel();
        searchTextField = new javax.swing.JTextField();
        goButton = new javax.swing.JButton();
        scrollPanePoints = new javax.swing.JScrollPane();
        pointsList = new javax.swing.JList(plistmodel);
        optionTabbedPane = new javax.swing.JTabbedPane();

        contentPanel = new javax.swing.JPanel();
        fileContentScrollPane = new javax.swing.JScrollPane();
        fileContentEditorPane = new javax.swing.JEditorPane();
        fileTitleTextField = new javax.swing.JTextField();

        showTopicsToggleButton.setFont(new java.awt.Font("Arial", 0, 12));
        showTopicsToggleButton.setText("HL");
        showTopicsToggleButton.setToolTipText("Highlight Labels");
        showTopicsToggleButton.setFocusable(false);
        showTopicsToggleButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        showTopicsToggleButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        showTopicsToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showTopicsToggleButtonActionPerformed(evt);
            }
        });
        selectionToolBar.add(showTopicsToggleButton);

        showallTopicsToggleButton.setFont(new java.awt.Font("Courier", 0, 12)); // NOI18N
        showallTopicsToggleButton.setText("SL");
        showallTopicsToggleButton.setToolTipText("Show all topics");
        showallTopicsToggleButton.setFocusable(false);
        showallTopicsToggleButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        showallTopicsToggleButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        showallTopicsToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showallTopicsToggleButtonActionPerformed(evt);
            }
        });
        selectionToolBar.add(showallTopicsToggleButton);

        cleanButton.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cleanButton.setText("CL");
        cleanButton.setToolTipText("Clear");
        cleanButton.setFocusable(false);
        cleanButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cleanButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        cleanButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cleanToggleButtonActionPerformed(evt);
            }
        });
        selectionToolBar.add(cleanButton);
    }

    private void showTopicsToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {
        //highlighttopic = showTopicsToggleButton.isSelected();
        view.repaint();
    }

    private void showallTopicsToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if (model != null) {
            ((TopicProjectionModel) model).setShowTopics(showallTopicsToggleButton.isSelected());
            model.notifyObservers(Boolean.FALSE);
        }
    }

    private void cleanToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if (model != null) {
            ((TopicProjectionModel) model).cleanSelectedInstances();
            ((TopicProjectionModel) model).getTopics().clear();
            model.notifyObservers(Boolean.FALSE);
        }
    }

    private DefaultListModel plistmodel = new DefaultListModel();

    private javax.swing.JToggleButton showTopicsToggleButton;
    private javax.swing.JToggleButton showallTopicsToggleButton;
    private javax.swing.JButton cleanButton;

    private javax.swing.JPanel allPointsPanel;
    private javax.swing.JPanel dataSearchPanel;
    private javax.swing.JLabel searchLabel;
    private javax.swing.JTextField searchTextField;
    private javax.swing.JButton goButton;
    private javax.swing.JScrollPane scrollPanePoints;
    private javax.swing.JList pointsList;
    private javax.swing.JTabbedPane optionTabbedPane;

    private javax.swing.JPanel contentPanel;
    private javax.swing.JScrollPane fileContentScrollPane;
    private javax.swing.JEditorPane fileContentEditorPane;
    private javax.swing.JTextField fileTitleTextField;

    public class TopicViewPanel extends ViewPanel {

        public TopicViewPanel() {
            super();
        }

        @Override
        public void paintComponent(java.awt.Graphics g) {
            System.out.println("bla bla");
            //super.paintComponent(g);
            java.awt.Graphics2D g2 = (java.awt.Graphics2D) g;
            //drawing the topics;
            for (Topic t : ((TopicProjectionModel) model).getTopics()) {
                t.drawTopic(g2,getFont(),((TopicProjectionModel) model).isShowTopics());
            }
        }

    }

    protected TopicViewPanel topicView;

}
