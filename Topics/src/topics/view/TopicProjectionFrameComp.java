/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package topics.view;

import detailedMatrix.DetailedMatrix;
import distance.DetailedDistanceMatrix;
import distance.DistanceMatrix;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import labeledprojection.coordination.ClassMatchCoordination;
import labeledprojection.view.ReportView;
import matrix.AbstractMatrix;
import projection.model.Scalar;
import textprocessing.corpus.Corpus;
import topics.model.TopicProjectionModel;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.coordination.AbstractCoordinator;


/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Projection.View",
name = "Topic Projection View Frame",
description = "Display a topic projection model.")
public class TopicProjectionFrameComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (model != null) {
            frame = new TopicProjectionFrame();
            //DTopicProjectionFrame frame = new DTopicProjectionFrame();
            frame.setSize(800, 600);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
            frame.setTitle(title);
            frame.setModel(model);

            if (coordinators != null) {
                for (int i = 0; i < coordinators.size(); i++) {
                    frame.addCoordinator(coordinators.get(i));
                    if (coordinators.get(i) instanceof ClassMatchCoordination) {
                        Scalar sc = ((ClassMatchCoordination)coordinators.get(i)).match();
                    }
                }
            }
            //Setting report data...
            ((ReportView)frame.getReportPanel()).clean();
            Corpus c = ((TopicProjectionModel)model).getCorpus();
            if (c != null) ((ReportView)frame.getReportPanel()).setDataSource(c.getUrl());
            ((ReportView)frame.getReportPanel()).setObjects(model.getInstances().size());
            ((ReportView)frame.getReportPanel()).setType(((TopicProjectionModel)model).getType());

            if (matrix != null && matrix instanceof DetailedMatrix) {//(matrix instanceof DetailedDenseMatrix || matrix instanceof DetailedSparseMatrix)) {
                if (matrix.getRowCount() == ((TopicProjectionModel)model).getInstances().size()) {
                    ((ReportView)frame.getReportPanel()).setSource(((DetailedMatrix)matrix).getSource());
                    ((ReportView)frame.getReportPanel()).setDimensions(matrix.getDimensions());
                }
            }else if (dmat != null && dmat instanceof DetailedDistanceMatrix) {
                if (dmat.getElementCount() == ((TopicProjectionModel)model).getInstances().size()) {
                    ((ReportView)frame.getReportPanel()).setSource(((DetailedDistanceMatrix)dmat).getSource());
                }
            }
        } else {
            throw new IOException("A projection model should be provided.");
        }
    }

    public void input(@Param(name = "Original Distance Matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public void input(@Param(name = "Original Data Matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "projection model") TopicProjectionModel model) {
        this.model = model;
    }

    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new TopicProjectionFrameParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        model = null;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle () {
        return title;
    }

    public static final long serialVersionUID = 1L;
    private String title = "";
    private transient TopicProjectionFrameParamView paramview;
    private transient TopicProjectionModel model;
    private transient ArrayList<AbstractCoordinator> coordinators;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
    transient TopicProjectionFrame frame;
}
