/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package topics.selection;

import java.util.ArrayList;
import labeledprojection.view.selection.ClassSelection;
import topics.view.TopicProjectionFrame;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class TopicClassSelection extends ClassSelection {

    public TopicClassSelection(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (viewer.getModel() != null) {
            setClass(selinst);
            ((TopicProjectionFrame)viewer).updateScalars(getNcdata());
            viewer.getModel().setSelectedInstances(selinst);
            viewer.getModel().notifyObservers();
        }
    }

}
