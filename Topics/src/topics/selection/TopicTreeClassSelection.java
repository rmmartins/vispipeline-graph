/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package topics.selection;

import java.util.ArrayList;
import topics.view.TopicTreeFrame;
import tree.model.TreeInstance;
import tree.view.interaction.TreeClassSelection;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class TopicTreeClassSelection extends TreeClassSelection {

    public TopicTreeClassSelection(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (viewer.getModel() != null) {
            ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
            if (selinst.size() == 1) {
                instances = selectHierarchy((TreeInstance)selinst.get(0));
            }else {
                instances.addAll(selinst);
            }
            setClass(instances);
            ((TopicTreeFrame)viewer).updateScalars(getNcdata());
            viewer.getModel().setSelectedInstances(instances);
            viewer.getModel().notifyObservers();
        }
    }

}
