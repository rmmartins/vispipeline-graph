/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.util;

import distance.dissimilarity.DissimilarityFactory;
import java.io.File;
import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import plsp.technique.PLSPProjection2D;
import plsp.technique.PLSPProjection2DComp;
import projection.util.ProjectionRunner;
import projection.util.ProjectionUtil;
import vispipelinebasics.annotations.VisComponent;

/**
 *
 * @author Rafael M. Martins <rafael.martins@lnu.se>
 */
public class PlspRunner extends ProjectionRunner {
    
    @Param(desc="Sample Type", pos=0)
    public PLSPProjection2D.SampleType sampleType = PLSPProjection2D.SampleType.CLUSTERING;
    
    @Param(desc = "Dissimilarity Type", pos = 3)
    public DissimilarityFactory.DissimilarityType dissType = DissimilarityFactory.DissimilarityType.EUCLIDEAN;
    
    @Override
    public AbstractMatrix runProjection(File f) throws IOException {        
        AbstractMatrix matrix = MatrixFactory.getInstance(f.getAbsolutePath());

        ProjectionUtil.log(false, false);

        long start = System.currentTimeMillis();
        PLSPProjection2D plsp = new PLSPProjection2D();
//        plsp.setNumberNeighbors(8);
        plsp.setSampleType(sampleType);
        AbstractMatrix projection = plsp.project(matrix, DissimilarityFactory.getInstance(dissType));
        plsp.spread(projection, 1.25f);
        long finish = System.currentTimeMillis();
        System.out.println("PLSP time: " + (finish - start) / 1000.0f + "s");

        //.save(filename + "-plsp.prj");

//        ProjectionModelComp model = new ProjectionModelComp();
//        model.input(projection);
//        model.execute();
//
//        ProjectionFrameComp frame = new ProjectionFrameComp();
//        frame.input(model.output());
//        frame.setTitle("P-LSP");
//        frame.execute();

        return projection;
    }
    
    @Override
    public String getName() {
        return PLSPProjection2DComp.class.getAnnotation(VisComponent.class).name();
    }

    @Override
    public String getShortName() {
        return "plsp";
    }
    
}
