/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.repositioning;

import datamining.clustering.BKmeans;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;

/**
 *
 * @author paulovich
 */
public class Redistribute {

    private ArrayList<ArrayList<Integer>> controlpoints_new;
    private ArrayList<ArrayList<Integer>> clusters_new;
    private AbstractMatrix cpprojection_new;

    public void execute(AbstractMatrix cpprojection_old,
            ArrayList<Integer> seeds,
            AbstractMatrix matrix, AbstractDissimilarity diss, int nrclusters) throws IOException {
        this.controlpoints_new = clusteringR2(cpprojection_old, seeds, nrclusters);
        this.clusters_new = clusteringRn(controlpoints_new, matrix, diss);
        this.cpprojection_new = mapProjection(cpprojection_old, seeds, controlpoints_new);

        report(matrix);
    }

    public ArrayList<ArrayList<Integer>> getNewControlPoints() {
        return controlpoints_new;
    }

    public AbstractMatrix getNewControlPointsProjection() {
        return cpprojection_new;
    }

    public ArrayList<ArrayList<Integer>> getNewClusters() {
        return clusters_new;
    }

    private AbstractMatrix mapProjection(AbstractMatrix cpprojection,
            ArrayList<Integer> seeds,
            ArrayList<ArrayList<Integer>> controlpoints_new) throws IOException {

        //creating a mapping between old id and new id
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < seeds.size(); i++) {
            map.put(seeds.get(i), i);
        }

        //mapping
        AbstractMatrix cpprojection_aux = MatrixFactory.getInstance(cpprojection.getClass());
        for (int i = 0; i < controlpoints_new.size(); i++) {
            for (int j = 0; j < controlpoints_new.get(i).size(); j++) {
                cpprojection_aux.addRow(cpprojection.getRow(map.get(controlpoints_new.get(i).get(j))));
            }
        }

        return cpprojection_aux;
    }

    private ArrayList<ArrayList<Integer>> clusteringRn(ArrayList<ArrayList<Integer>> controlpoints_new,
            AbstractMatrix matrix, AbstractDissimilarity diss) {
        //creating all clusters
        ArrayList<ArrayList<Integer>> clusters = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < controlpoints_new.size(); i++) {
            clusters.add(new ArrayList<Integer>());
        }

        //finding the cluster with the closest element
        for (int i = 0; i < matrix.getRowCount(); i++) {
            AbstractVector mainrow = matrix.getRow(i);
            int closest = 0;
            float mindist = Float.POSITIVE_INFINITY;

            for (int j = 0; j < controlpoints_new.size(); j++) {
                for (int k = 0; k < controlpoints_new.get(j).size(); k++) {
                    //ensuring that the control points goes to its right cluster
                    if (controlpoints_new.get(j).get(k).intValue() == i) {
                        mindist = Float.NEGATIVE_INFINITY;
                        closest = j;
                    } else {
                        AbstractVector row = matrix.getRow(controlpoints_new.get(j).get(k));
                        float dist = diss.calculate(mainrow, row);
                        if (mindist > dist) {
                            mindist = dist;
                            closest = j;
                        }
                    }
                }
            }

            clusters.get(closest).add(i);
        }

        return clusters;
    }

    private ArrayList<ArrayList<Integer>> clusteringR2(AbstractMatrix cpprojection,
            ArrayList<Integer> seeds, int nrclusters) throws IOException {
        //creating the 2D clusters
        AbstractDissimilarity diss = new Euclidean();
        BKmeans kmeans = new BKmeans(nrclusters);
        ArrayList<ArrayList<Integer>> clusters = kmeans.execute(diss, cpprojection);
        clusters = kmeans.execute(diss, cpprojection);
        AbstractMatrix centroids = kmeans.getCentroids();

        //redistributing the elements of small clusters
        for (int i = clusters.size() - 1; i >= 0; i--) {
            if (clusters.get(i).size() < 5) {
                int closest = 0;
                float mindist = Float.POSITIVE_INFINITY;

                for (int j = 0; j < clusters.size(); j++) {
                    if (i != j) {
                        for (int k = 0; k < clusters.get(i).size(); k++) {
                            float dist = diss.calculate(centroids.getRow(j), cpprojection.getRow(clusters.get(i).get(k)));

                            if (mindist > dist) {
                                mindist = dist;
                                closest = j;
                            }
                        }
                    }
                }

                clusters.get(closest).addAll(clusters.get(i));
                clusters.remove(i);
            }
        }

        //split large clusters
        int max = Math.max(6, cpprojection.getRowCount() / nrclusters);
        for (int i = clusters.size() - 1; i >= 0; i--) {
            if (clusters.get(i).size() >= max) {
                int size = clusters.get(i).size();
                ArrayList<Integer> newcluster = new ArrayList<Integer>();

                System.out.print(clusters.get(i).size() + " => ");
                for (int j = size - 1; j >= size / 2; j--) {
                    newcluster.add(clusters.get(i).get(j));
                    clusters.get(i).remove(j);
                }
                System.out.println(clusters.get(i).size());

                clusters.add(newcluster);
            }
        }

        //creating a mapping between new id and old id
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < seeds.size(); i++) {
            map.put(i, seeds.get(i));
        }

        //mapping the new ids to old ids
        ArrayList<ArrayList<Integer>> controlpoints_aux = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < clusters.size(); i++) {
            controlpoints_aux.add(new ArrayList<Integer>());
            for (int j = 0; j < clusters.get(i).size(); j++) {
                controlpoints_aux.get(i).add(map.get(clusters.get(i).get(j)));
            }
        }        

        return controlpoints_aux;
    }

    private void report(AbstractMatrix matrix) {
        System.out.println(">>>>>>>>>>");
        System.out.println(">>>>REPORT");
        for (int i = 0; i < clusters_new.size(); i++) {
            System.out.println("control points: " + controlpoints_new.get(i).size()
                    + " - cluster size: " + clusters_new.get(i).size());

            HashSet<Integer> els = new HashSet<Integer>();
            for (int j = 0; j < clusters_new.get(i).size(); j++) {
                if (!els.contains(clusters_new.get(i).get(j))) {
                    els.add(clusters_new.get(i).get(j));
                } else {
                    System.out.println("DUPLICAÇÃO");
                }
            }

            for (int j = 0; j < controlpoints_new.get(i).size(); j++) {
                if (!els.contains(controlpoints_new.get(i).get(j))) {
                    System.out.println("CONTROL POINT NÃO EXISTENTE");
                }

            }
        }
        System.out.println(">>>>REPORT");
        System.out.println(">>>>>>>>>>");
    }
}
