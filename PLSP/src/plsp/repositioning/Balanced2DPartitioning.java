/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.repositioning;

import datamining.clustering.Kmeans;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;

/**
 *
 * @author paulovich
 */
public class Balanced2DPartitioning {

    public ArrayList<ArrayList<Integer>> execute(AbstractMatrix matrix, int nrcells) throws IOException {
//        int nrpartx = (int) Math.sqrt(nrcells);
//        int nrparty = nrcells / nrpartx;
//
//        System.out.println("nrpartx: " + nrpartx);
//        System.out.println("nrparty: " + nrparty);
//
//        //first split the data considering the x-coordinate
//        ArrayList<ArrayList<Integer>> partitions = new ArrayList<ArrayList<Integer>>();
//
//        //then split the data considering the y-coordinate
//        ArrayList<ArrayList<Integer>> partitioningX = partitioningX(matrix, nrpartx);
//        for (int i = 0; i < partitioningX.size(); i++) {
//            ArrayList<ArrayList<Integer>> partitioningY = partitioningY(matrix, partitioningX.get(i), nrparty);
//            partitions.addAll(partitioningY);
//        }

        AbstractDissimilarity diss = new Euclidean();
        Kmeans kmeans = new Kmeans(nrcells);
        ArrayList<ArrayList<Integer>> partitions = kmeans.execute(diss, matrix);
        AbstractMatrix centroids = kmeans.getCentroids();

        for (int i = 0; i < partitions.size(); i++) {
            if (partitions.get(i).size() < 3) {
                int closest = 0;
                float mindist = Float.POSITIVE_INFINITY;

                for (int j = 0; j < partitions.size(); j++) {
                    if (i != j) {
                        for (int k = 0; k < partitions.get(i).size(); k++) {
                            float dist = diss.calculate(centroids.getRow(j), matrix.getRow(partitions.get(i).get(k)));

                            if (mindist > dist) {
                                mindist = dist;
                                closest = j;
                            }
                        }
                    }
                }

                partitions.get(closest).addAll(partitions.get(i));
            }
        }

        int count = 0;

        for (int i = partitions.size() - 1; i >= 0; i--) {
            if (partitions.get(i).size() < 3) {
                partitions.remove(i);
            } else {
                count += partitions.get(i).size();
            }
        }

        System.out.println("Redistribution: nr control points " + count + " == " + matrix.getRowCount());
        System.out.println("Redistribution: nr partitions " + nrcells + " => " + partitions.size());

        return partitions;
    }

    public AbstractMatrix mapProjection(ArrayList<ArrayList<Integer>> partitions, AbstractMatrix projection) throws IOException {
        AbstractMatrix newproj = MatrixFactory.getInstance(projection.getClass());

        for (int i = 0; i < partitions.size(); i++) {
            for (int j = 0; j < partitions.get(i).size(); j++) {
                newproj.addRow(projection.getRow(partitions.get(i).get(j)));
            }
        }

        return newproj;
    }

    public ArrayList<ArrayList<Integer>> mapControlPoints(ArrayList<ArrayList<Integer>> partitions,
            ArrayList<ArrayList<Integer>> cpoints) {
        ArrayList<ArrayList<Integer>> newcpoints = new ArrayList<ArrayList<Integer>>();

        HashMap<Integer, Integer> mapping = new HashMap<Integer, Integer>();
        for (int i = 0, k = 0; i < cpoints.size(); i++) {
            for (int j = 0; j < cpoints.get(i).size(); j++, k++) {
                mapping.put(k, cpoints.get(i).get(j));
            }
        }

        for (int i = 0; i < partitions.size(); i++) {
            newcpoints.add(new ArrayList<Integer>());
            for (int j = 0; j < partitions.get(i).size(); j++) {
                int newind = mapping.get(partitions.get(i).get(j));
                newcpoints.get(i).add(newind);
            }
        }

        return newcpoints;
    }

    public ArrayList<ArrayList<Integer>> redistribute(AbstractMatrix matrix, AbstractDissimilarity diss,
            ArrayList<ArrayList<Integer>> partitions) {
        //creating all clusters
        ArrayList<ArrayList<Integer>> clusters = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < partitions.size(); i++) {
            clusters.add(new ArrayList<Integer>());
        }

        //finding the partitioning with the closest element
        for (int i = 0; i < matrix.getRowCount(); i++) {
            AbstractVector mainrow = matrix.getRow(i);
            int part = 0;
            float mindist = Float.POSITIVE_INFINITY;

            for (int j = 0; j < partitions.size(); j++) {
                for (int k = 0; k < partitions.get(j).size(); k++) {
                    if (partitions.get(j).get(k).intValue() == i) {
                        mindist = Float.NEGATIVE_INFINITY;
                        part = j;
                    }

                    AbstractVector row = matrix.getRow(partitions.get(j).get(k));
                    float dist = diss.calculate(mainrow, row);
                    if (mindist > dist) {
                        mindist = dist;
                        part = j;
                    }
                }
            }

            clusters.get(part).add(i);
        }

        return clusters;
    }

    private ArrayList<ArrayList<Integer>> partitioningX(AbstractMatrix matrix, int nrparts) {
        //getting the partitions points of x-coordinate
        float[] xcoords = new float[matrix.getRowCount()];
        for (int i = 0; i < xcoords.length; i++) {
            xcoords[i] = matrix.getRow(i).getValue(0);
        }
        Arrays.sort(xcoords);

        float[] partcoordsx = new float[nrparts - 1];
        int step = matrix.getRowCount() / nrparts;
        for (int i = 1; i < nrparts; i++) {
            partcoordsx[i - 1] = xcoords[i * step];
        }

        //creting the partitions
        ArrayList<ArrayList<Integer>> partitions = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < nrparts; i++) {
            partitions.add(new ArrayList<Integer>());
        }

        for (int i = 0; i < matrix.getRowCount(); i++) {
            float xcoord = matrix.getRow(i).getValue(0);
            int partition = 0;

            while (partition < partcoordsx.length) {
                if (xcoord < partcoordsx[partition]) {
                    break;
                }
                partition++;
            }

            partitions.get(partition).add(i);
        }

        return partitions;
    }

    private ArrayList<ArrayList<Integer>> partitioningY(AbstractMatrix matrix,
            ArrayList<Integer> partition, int nrparts) {
        //getting the partitions points of y-coordinate
        float[] ycoords = new float[partition.size()];
        for (int i = 0; i < ycoords.length; i++) {
            ycoords[i] = matrix.getRow(partition.get(i)).getValue(1);
        }
        Arrays.sort(ycoords);

        float[] partcoordsy = new float[nrparts - 1];
        int step = partition.size() / nrparts;
        for (int i = 1; i < nrparts; i++) {
            partcoordsy[i - 1] = ycoords[i * step];
        }

        //creting the partitions
        ArrayList<ArrayList<Integer>> partitions = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < nrparts; i++) {
            partitions.add(new ArrayList<Integer>());
        }

        for (int i = 0; i < partition.size(); i++) {
            float ycoord = matrix.getRow(partition.get(i)).getValue(1);
            int part = 0;

            while (part < partcoordsy.length) {
                if (ycoord < partcoordsy[part]) {
                    break;
                }
                part++;
            }

            partitions.get(part).add(partition.get(i));
        }

        return partitions;
    }
}
