/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.repositioning;

import datamining.clustering.HierarchicalClustering;
import datamining.clustering.HierarchicalClustering.HierarchicalClusteringType;
import datamining.clustering.Kmeans;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;
import matrix.util.MatrixUtils;

/**
 *
 * @author paulovich
 */
public class RedistributeControlPoints2D {

    public ArrayList<ArrayList<Integer>> execute(AbstractMatrix matrix, AbstractMatrix cpprojection,
            ArrayList<ArrayList<Integer>> cpoints, int nrclusters) throws IOException {
        AbstractDissimilarity diss = new Euclidean();

        //creating the 2D clusters
        ArrayList<ArrayList<Integer>> clusters_aux = clustering(cpprojection, diss, nrclusters);

        //creating the new sets of control points based on the 2D clusters
        newcpoints = mapControlPoints(clusters_aux, cpoints);

        //re-arranging the control points projection to match the 2D clusters
        newproj = mapProjection(clusters_aux, cpprojection);

        //re-create the Rn clusters using the information provided by the R2 clusters
        ArrayList<ArrayList<Integer>> clusters = redistribute(matrix, diss, newcpoints);

        return clusters;
    }

    public ArrayList<ArrayList<Integer>> getNewControlPoints() {
        return newcpoints;
    }

    public AbstractMatrix getNewControlPointsProjection() {
        return newproj;
    }

    private ArrayList<ArrayList<Integer>> clustering(AbstractMatrix projection,
            AbstractDissimilarity diss, int nrclusters) throws IOException {
        //creating the new clusters
//        HierarchicalClustering hc = new HierarchicalClustering(nrclusters, HierarchicalClusteringType.SLINK);
//        ArrayList<ArrayList<Integer>> clusters = hc.execute(diss, projection);
        
        Kmeans kmeans = new Kmeans(nrclusters);
        ArrayList<ArrayList<Integer>> clusters = kmeans.execute(diss, projection);

        AbstractMatrix centroids = MatrixFactory.getInstance(projection.getClass());
        for (int i = 0; i < clusters.size(); i++) {
            AbstractMatrix aux = MatrixFactory.getInstance(projection.getClass());
            for (int j = 0; j < clusters.get(i).size(); j++) {
                aux.addRow(projection.getRow(clusters.get(i).get(j)));
            }
            centroids.addRow(MatrixUtils.mean(aux));
        }

        //redistributing the elements of small clusters
        for (int i = clusters.size() - 1; i >= 0; i--) {
            if (clusters.get(i).size() < 3) {
                int closest = 0;
                float mindist = Float.POSITIVE_INFINITY;

                for (int j = 0; j < clusters.size(); j++) {
                    if (i != j) {
                        for (int k = 0; k < clusters.get(i).size(); k++) {
                            float dist = diss.calculate(centroids.getRow(j), projection.getRow(clusters.get(i).get(k)));

                            if (mindist > dist) {
                                mindist = dist;
                                closest = j;
                            }
                        }
                    }
                }

                clusters.get(closest).addAll(clusters.get(i));
                clusters.remove(i);
            }
        }

        //split large clusters
        int max = Math.max(6, projection.getRowCount() / nrclusters);
        for (int i = clusters.size() - 1; i >= 0; i--) {
            if (clusters.get(i).size() >= max) {
                int size = clusters.get(i).size();
                ArrayList<Integer> newcluster = new ArrayList<Integer>();

                System.out.print(clusters.get(i).size() + " => ");
                for (int j = size - 1; j >= size / 2; j--) {
                    newcluster.add(clusters.get(i).get(j));
                    clusters.get(i).remove(j);
                }
                System.out.println(clusters.get(i).size());

                clusters.add(newcluster);
            }
        }

        int count = 0;
        for (int i = 0; i < clusters.size(); i++) {
            count += clusters.get(i).size();
        }

        System.out.println("Redistribution: nr control points " + count + " == " + projection.getRowCount());
        System.out.println("Redistribution: nr partitions " + nrclusters + " => " + clusters.size());

        return clusters;
    }

    private AbstractMatrix mapProjection(ArrayList<ArrayList<Integer>> partitions,
            AbstractMatrix projection) throws IOException {
        AbstractMatrix newproj_aux = MatrixFactory.getInstance(projection.getClass());

        for (int i = 0; i < partitions.size(); i++) {
            for (int j = 0; j < partitions.get(i).size(); j++) {
                newproj_aux.addRow(projection.getRow(partitions.get(i).get(j)));
            }
        }

        return newproj_aux;
    }

    private ArrayList<ArrayList<Integer>> mapControlPoints(ArrayList<ArrayList<Integer>> partitions,
            ArrayList<ArrayList<Integer>> cpoints) {
        ArrayList<ArrayList<Integer>> newcpoints_aux = new ArrayList<ArrayList<Integer>>();

        HashMap<Integer, Integer> mapping = new HashMap<Integer, Integer>();
        for (int i = 0, k = 0; i < cpoints.size(); i++) {
            for (int j = 0; j < cpoints.get(i).size(); j++, k++) {
                mapping.put(k, cpoints.get(i).get(j));
            }
        }

        for (int i = 0; i < partitions.size(); i++) {
            newcpoints_aux.add(new ArrayList<Integer>());
            for (int j = 0; j < partitions.get(i).size(); j++) {
                int newind = mapping.get(partitions.get(i).get(j));
                newcpoints_aux.get(i).add(newind);
            }
        }

        return newcpoints_aux;
    }

    private ArrayList<ArrayList<Integer>> redistribute(AbstractMatrix matrix,
            AbstractDissimilarity diss, ArrayList<ArrayList<Integer>> partitions) {
        //creating all clusters
        ArrayList<ArrayList<Integer>> clusters = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < partitions.size(); i++) {
            clusters.add(new ArrayList<Integer>());
        }

        //finding the partitioning with the closest element
        for (int i = 0; i < matrix.getRowCount(); i++) {
            AbstractVector mainrow = matrix.getRow(i);
            int part = 0;
            float mindist = Float.POSITIVE_INFINITY;

            for (int j = 0; j < partitions.size(); j++) {
                for (int k = 0; k < partitions.get(j).size(); k++) {
                    if (partitions.get(j).get(k).intValue() == i) {
                        mindist = Float.NEGATIVE_INFINITY;
                        part = j;
                    }

                    AbstractVector row = matrix.getRow(partitions.get(j).get(k));
                    float dist = diss.calculate(mainrow, row);
                    if (mindist > dist) {
                        mindist = dist;
                        part = j;
                    }
                }
            }

            clusters.get(part).add(i);
        }

        return clusters;
    }
    private AbstractMatrix newproj;
    private ArrayList<ArrayList<Integer>> newcpoints;
}
