/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.repositioning.music;

import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;

/**
 *
 * @author paulovich
 */
public class MusicProjectionModel extends ProjectionModel {

    public MusicProjectionModel() {
        playlists = new ArrayList<PlayList>();
        drawnote = true;
    }

    public void setDrawAsNote(boolean drawnote) {
        this.drawnote = drawnote;
        setChanged();
    }

    public boolean isDrawAsNote() {
        return drawnote;
    }

    public void addPlayList(PlayList list) {
        playlists.add(list);
        setChanged();
    }

    public ArrayList<PlayList> getPlaylists() {
        return playlists;
    }

    @Override
    public void draw(BufferedImage image, boolean highquality) {
        if (image != null) {
            for(PlayList p : playlists) {
                p.draw(image, highquality);
            }
            
//            //draw the background circles
//            int radius = 22;
//            Graphics2D g2 = (Graphics2D) image.getGraphics();
//            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.25f));
//            for (int i = 0; i < instances.size(); i++) {
//                ProjectionInstance pi = (ProjectionInstance) instances.get(i);
//                g2.setColor(pi.getColor());
//                g2.fillOval((int) (pi.getX() - (radius / 2)), (int) (pi.getY() - (radius / 2)),
//                        radius, radius);
//            }
//            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));

            //first draw the non-selected instances
            for (int i = 0; i < instances.size(); i++) {
                ProjectionInstance pi = (ProjectionInstance) instances.get(i);

                if (!pi.isSelected()) {
                    pi.draw(image, highquality);
                }
            }

            //then the selected instances
            for (int i = 0; i < instances.size(); i++) {
                ProjectionInstance pi = (ProjectionInstance) instances.get(i);

                if (pi.isSelected()) {
                    pi.draw(image, highquality);
                }
            }
        }
    }

    @Override
    public ArrayList<ProjectionInstance> getInstancesByPosition(Polygon polygon) {
        ArrayList<ProjectionInstance> selected = new ArrayList<ProjectionInstance>();

        for (int i = 0; i < instances.size(); i++) {
            MusicProjectionInstance pi = (MusicProjectionInstance) instances.get(i);

            if (pi.isVisible() && polygon.contains(pi.getX(), pi.getY())) {
                selected.add(pi);
            }
        }

        return selected;
    }

    private boolean drawnote;
    private ArrayList<PlayList> playlists;
}
