/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.repositioning.music;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import visualizationbasics.util.PropertiesManager;

/**
 *
 * @author PC
 */
public class MusicCollectionManager {

    public static void importCollection(String dir) throws IOException {
        PropertiesManager spm = PropertiesManager.getInstance(MusicBrowserConstants.PROP_FILE);
        MusicCollection collection = new MusicCollection("Entire Collection");
        collection.loadMusicCollectionFromDirectoy(dir);
        collection.writeMusicCollectionXML(spm.getProperty(MusicBrowserConstants.XML_FILE));
        Logger.getLogger(MusicCollectionManager.class.getName()).log(Level.SEVERE,
                "Importing Collection: found {0} musics.", collection.size());
    }

    public static MusicCollection loadCollection() throws IOException {
        PropertiesManager spm = PropertiesManager.getInstance(MusicBrowserConstants.PROP_FILE);
        MusicCollection collection = MusicCollection.readMusicCollectionFromXML(spm.getProperty(MusicBrowserConstants.XML_FILE));
        Logger.getLogger(MusicCollectionManager.class.getName()).log(Level.SEVERE,
                "Loading Collection: found {0} musics.", collection.size());
        return collection;
    }

    public static void createFeaturesMatrix(MusicCollection collection) throws IOException {
        PropertiesManager spm = PropertiesManager.getInstance(MusicBrowserConstants.PROP_FILE);
        MP3FeaturesDataset dataset = new MP3FeaturesDataset();
        dataset.load(spm.getProperty(MusicBrowserConstants.FEATURE_FILE));

        DenseMatrix matrix = new DenseMatrix();

        for (int i = 0; i < collection.size(); i++) {
            int id = collection.getMusicByIndex(i).getId();

            String filename = collection.getMusicByIndex(i).getFilename();
            float[] vector = dataset.getFeatures(filename);

            if (vector != null && (matrix.getDimensions() == 0 || matrix.getDimensions() == vector.length)) {
                boolean add = true;
                //removing NaNs
                for (int j = 0; j < vector.length; j++) {
                    if (Float.isNaN(vector[j]) || Float.isInfinite(vector[j])) {
                        System.out.println(">>>>REMOVED: " + filename);
                        add = false;
                        break;//vector[j] = 0.0f;
                    }
                }

                if (add) {
                    matrix.addRow(new DenseVector(vector, id));
                }
            } else {
                System.out.println("NOT FOUND>>>>" + collection.getMusicByIndex(i).getFilename());
            }
        }

        matrix.save(spm.getProperty(MusicBrowserConstants.FEATURE_MATRIX_FILE));
    }

    public static void clean(String filename) throws FileNotFoundException, IOException {
        PropertiesManager spm = PropertiesManager.getInstance(MusicBrowserConstants.PROP_FILE);

        BufferedReader in = new BufferedReader(new FileReader(new File(filename)));
        //BufferedWriter out = new BufferedWriter(new FileWriter(new File(spm.getProperty(MusicBrowserConstants.FEATURE_FILE))));
        BufferedWriter out = new BufferedWriter(new FileWriter(new File(filename + "_clean.xml")));

        String line = null;
        while ((line = in.readLine()) != null) {
            out.write(line.replaceAll("&", "&amp;"));
        }

        out.close();
        in.close();
    }

    public static AbstractMatrix loadFeaturesMatrix() throws IOException {
        PropertiesManager spm = PropertiesManager.getInstance(MusicBrowserConstants.PROP_FILE);
        DenseMatrix matrix = new DenseMatrix();
        matrix.load(spm.getProperty(MusicBrowserConstants.FEATURE_MATRIX_FILE));
        return matrix;
    }
    static int count = 0;

    private static void generateBatch(File dir) {
        File[] listFiles = dir.listFiles();

        if (listFiles != null) {
            for (File f : listFiles) {
                if (f.isFile() && f.getName().toLowerCase().endsWith(".mp3")) {
                    File newfile = new File(encodeToValidChars(f.getPath()));
                    System.out.println(newfile.getPath());
                    f.renameTo(newfile);

//                    String filename = "/home/paulovich/Desktop/OGG/" + f.getName();
//                    copyfile(f, new File(filename));
//                    System.out.println(filename);
//                    count++;
                    //f.renameTo(new File(filename));
                    //System.out.println("<file>" + convert(deConvert((f.getPath()))) + "</file>");
                } else if (f.isDirectory()) {
//                    File newfile = new File(f.getPath().replaceAll(" ", "_"));
//                    f.renameTo(newfile);
                    generateBatch(f);
                }
            }
        }
    }

    private static void copyfile(File srFile, File dtFile) {
        try {
            InputStream in = new FileInputStream(srFile);
            OutputStream out = new FileOutputStream(dtFile);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.flush();
            out.close();
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage() + " in the specified directory.");
            System.exit(0);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static String convert(String value) {
        if (value != null) {
            value = value.replaceAll("&", "&amp;");
            value = value.replaceAll("<", "&lt;");
            value = value.replaceAll(">", "&gt;");
            value = value.replaceAll("\"", "&quot;");
            value = value.replaceAll("\'", "&#39;");
        } else {
            return "";
        }
        return value;
    }

    private static String deConvert(String value) {
        if (value != null) {
            value = value.replaceAll("&amp;", "&");
            value = value.replaceAll("&lt;", "<");
            value = value.replaceAll("&gt;", ">");
            value = value.replaceAll("&quot;", "\"");
            value = value.replaceAll("&#39;", "\'");
        } else {
            return "";
        }
        return value;
    }

    private static String encodeToValidChars(String pData) {
        StringBuilder encodedData = new StringBuilder();

        //remove acentos
        pData = pData.replaceAll("[èéêë]", "e");
        pData = pData.replaceAll("[ûùúü]", "u");
        pData = pData.replaceAll("[ïîíì]", "i");
        pData = pData.replaceAll("[àâãáä]", "a");
        pData = pData.replaceAll("[ôóòöõ]", "o");
        pData = pData.replaceAll("[ýÿ]", "y");
        pData = pData.replaceAll("ñ", "n");
        pData = pData.replaceAll("ç", "c");

        pData = pData.replaceAll("[ÈÉÊË]", "E");
        pData = pData.replaceAll("[ÛÙÚÜ]", "U");
        pData = pData.replaceAll("[ÏÎÍÌ]", "I");
        pData = pData.replaceAll("[ÀÂÂÁÄ]", "A");
        pData = pData.replaceAll("[ÔÓÒÖÕ]", "O");
        pData = pData.replaceAll("Ý", "y");
        pData = pData.replaceAll("Ñ", "N");
        pData = pData.replaceAll("Ç", "C");

        pData = pData.replaceAll("'", "");
        pData = pData.replaceAll(",", "");
        pData = pData.replaceAll("\\?", "_");
//        pData = pData.replaceAll("\\.", "");
//        pData = pData.replaceAll("mp3", ".mp3");

        for (int i = 0; i < pData.length(); i++) {
            char ch = pData.charAt(i);
            int chVal = (int) ch;

            if (chVal >= 32 && chVal <= 43) {
                encodedData.append((char) chVal);
            } else if (chVal >= 45 && chVal <= 95) {
                encodedData.append((char) chVal);
            } else if (chVal >= 97 && chVal <= 125) {
                encodedData.append((char) chVal);
            }
        }

        return encodedData.toString();
    }

    public static void main(String[] args) throws IOException {
//        MusicCollectionManager.clean("/home/paulovich/Desktop/jaudio_exe/feature_values_3.xml");
        MusicCollectionManager.importCollection("D:\\music\\MP3_complete_collection");
        MusicCollectionManager.createFeaturesMatrix(MusicCollectionManager.loadCollection());




//        MusicCollectionManager.generateBatch(new File("D:\\music\\MP3_complete_collection"));
        //MusicCollectionManager.generateBatch(new File("/media/D44AB5624AB541D2/music/MP3_tagged/classic_music"));

//        File dir = new File("/home/paulovich/Desktop/OGG");
//        File[] list = dir.listFiles();
//
//        for(File f : list) {
//            if(f.getName().toLowerCase().endsWith(".mp3")) {
//                File newname = new File(f.getPath().replaceAll("__","_"));
//                f.renameTo(newname);
//            }
//        }

    }
}
