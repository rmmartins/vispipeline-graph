/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.repositioning.music;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author PC
 */
public class MP3FeatureParser extends DefaultHandler {

    public HashMap<String, float[]> read(String filename) throws IOException {
        matrix = new HashMap<String, float[]>();

        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {
            InputSource in = new InputSource(new InputStreamReader(new FileInputStream(filename), "UTF-8"));
            SAXParser sp = spf.newSAXParser();
            sp.parse(in, this);
        } catch (SAXException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return matrix;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        if (qName.equalsIgnoreCase("data_set")) {
            values.clear();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        if (qName.equalsIgnoreCase("data_set")) {
            float[] fvalues = new float[values.size()];
            for (int i = 0; i < values.size(); ++i) {
                fvalues[i] = values.get(i).floatValue();
            }
            matrix.put(tempFilename, fvalues);
        }
        if (qName.equalsIgnoreCase("data_set_id")) {
            tempFilename = line;
        }
        if (qName.equalsIgnoreCase("v")) {
            values.add(Float.parseFloat(line));
        }
        line = "";
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        line += new String(ch, start, length);
    }
    
    HashMap<String, float[]> matrix = null;
    String tempFilename = "";
    ArrayList<Float> values = new ArrayList<Float>();
    String line = "";
}
