/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.repositioning.music;

/**
 *
 * @author paulovich
 */
public class Music implements Comparable<Music> {

    public Music(int id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * @return the artist
     */
    public String getArtist() {
        return artist;
    }

    /**
     * @param artist the artist to set
     */
    public void setArtist(String artist) {
        this.artist = artist;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the album
     */
    public String getAlbum() {
        return album;
    }

    /**
     * @param album the album to set
     */
    public void setAlbum(String album) {
        this.album = album;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * @return the genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * @param genre the genre to set
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return title + " (" + artist + ")";
    }


    @Override
    public int compareTo(Music o) {
        int compare = this.artist.toLowerCase().compareTo(o.artist.toLowerCase());

        if(compare == 0) {
            compare = this.album.toLowerCase().compareTo(o.album.toLowerCase());
        }
        
        if(compare == 0) {
            compare = this.title.toLowerCase().compareTo(o.title.toLowerCase());
        }

        return compare;
    }
    
    private int id;
    private String filename;
    private String artist;
    private String title;
    private String album;
    private String year;
    private String genre;
}
