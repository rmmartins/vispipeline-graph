/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.repositioning.music;

import java.util.ArrayList;
import java.util.HashMap;
import projection.model.ProjectionInstance;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author PC
 */
public class SeedsViewPanelMusic extends ViewPanelMusic {

    public SeedsViewPanelMusic(MusicBrowser browser) {
        super(browser);
    }

    public void setMapping(HashMap<ProjectionInstance, ArrayList<ProjectionInstance>> mapping) {
        this.mapping = mapping;
    }

    @Override
    protected void mouseDragged(java.awt.event.MouseEvent evt) {
        if (selinstance != null) {
            if (model.hasSelectedInstances()) {
                float x = evt.getX() - selinstance.getX();
                float y = evt.getY() - selinstance.getY();

                for (AbstractInstance ai : model.getSelectedInstances()) {
                    ProjectionInstance pi = (ProjectionInstance) ai;
                    pi.setX(x + pi.getX());
                    pi.setY(y + pi.getY());

                    if (mapping != null) {
                        ArrayList<ProjectionInstance> relins = mapping.get(pi);
                        if (relins != null) {
                            for (ProjectionInstance relin : relins) {
                                relin.setX(x + relin.getX());
                                relin.setY(y + relin.getY());
                            }
                        }
                    }
                }

                adjustPanel();
            }
        } else if (selsource != null) {
            seltarget = evt.getPoint();
        } else if (selpolygon != null) {
            selpolygon.addPoint(evt.getX(), evt.getY());
        }

        repaint();

    }
    
    //map each seed to the nearest control points
    private HashMap<ProjectionInstance, ArrayList<ProjectionInstance>> mapping;
}
