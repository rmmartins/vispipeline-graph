/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package plsp.repositioning.music;

import java.awt.Composite;
import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

/**
 *
 * @author paulovich
 */
public class BlendComposite implements Composite {

    private static CompositeContext CONTEXT;

    @Override
    public CompositeContext createContext(ColorModel srcColorModel,
            ColorModel dstColorModel, RenderingHints hints) {
        if (CONTEXT == null) {
            CONTEXT = new BlendContext();
        }
        return CONTEXT;
    }

    private class BlendContext implements CompositeContext {

        private static final int BUFFER_SIZE = 64;

        public BlendContext() {
        }

        @Override
        public void compose(Raster src, Raster dstIn, WritableRaster dstOut) {
            for (int y = dstOut.getMinY(); y < dstOut.getHeight(); y++) {
                for (int x = dstOut.getMinY(); x < dstOut.getWidth(); x++) {
                    int pixsrc[] = new int[BUFFER_SIZE];
                    int pixin[] = new int[BUFFER_SIZE];

                    src.getPixel(x, y, pixsrc);
                    dstIn.getPixel(x, y, pixin);

                    int res[] = new int[BUFFER_SIZE];
                    for (int i = 0; i < BUFFER_SIZE; i++) {
                        res[i] = Math.min(pixsrc[i], pixin[i]);
                    }

                    dstOut.setPixel(x, y, res);
                }
            }
        }

        @Override
        public void dispose() {
        }
    }
}

