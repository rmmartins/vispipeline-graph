/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.repositioning.music;

import entagged.audioformats.AudioFile;
import entagged.audioformats.AudioFileIO;
import entagged.audioformats.exceptions.CannotReadException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import projection.model.XMLModelWriter;

/**
 *
 * @author paulovich
 */
public class MusicCollection {

    public MusicCollection(String name) {
        this.name = name;
        this.musics = new ArrayList<Music>();
    }

    public ArrayList<Music> getMusics() {
        return musics;
    }

    public void addMusic(Music music) {
        if (music != null) {
            this.musics.add(music);
        }
    }

    public Music getMusicByIndex(int index) {
        return musics.get(index);
    }

    public Music getMusicById(int id) {
        int size = musics.size();
        for (int i = 0; i < size; i++) {
            if (musics.get(i).getId() == id) {
                return musics.get(i);
            }
        }
        return null;
    }

    public int size() {
        return musics.size();
    }

    public String getName() {
        return name;
    }

    public void loadMusicCollectionFromDirectoy(String directory) {
        this.musics = new ArrayList<Music>();
        loadMusicCollectionFromDirectoy(musics, new File(directory));
    }

    public static MusicCollection readMusicCollectionFromXML(String filename) throws IOException {
        try {
            SAXParserFactory spf = SAXParserFactory.newInstance();
            XMLMusicParser parser = new XMLMusicParser();
            InputSource in = new InputSource(new InputStreamReader(new FileInputStream(filename), "ISO-8859-1"));
            SAXParser sp = spf.newSAXParser();
            sp.parse(in, parser);
            return parser.getMusicCollection();
        } catch (SAXException ex) {
            Logger.getLogger(MusicCollection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(MusicCollection.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public int writeMusicCollectionXML(String filename) throws IOException {
        BufferedWriter out = null;

        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "ISO-8859-1"));
            //writting the header
            out.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\r\n");

            //writting the music tags
            out.write("<collection name=\"");
            out.write(convert(deConvert(encodeToValidChars(name))));
            out.write("\">\r\n");

            for (int i = 0; i < musics.size(); i++) {
                Music tag = musics.get(i);

                out.write("<music id=\"");
                out.write(Integer.toString(tag.getId()));
                out.write("\">\r\n");

                out.write("<filename value=\"");
                out.write(convert(deConvert(encodeToValidChars(tag.getFilename()))));
                out.write("\"/>\r\n");

                out.write("<artist value=\"");
                out.write(convert(deConvert(encodeToValidChars(tag.getArtist()))));
                out.write("\"/>\r\n");

                out.write("<album value=\"");
                out.write(convert(deConvert(encodeToValidChars(tag.getAlbum()))));
                out.write("\"/>\r\n");

                out.write("<title value=\"");
                out.write(convert(deConvert(encodeToValidChars(tag.getTitle()))));
                out.write("\"/>\r\n");

                out.write("<year value=\"");
                out.write(convert(deConvert(encodeToValidChars(tag.getYear()))));
                out.write("\"/>\r\n");

                out.write("<genre value=\"");
                out.write(convert(deConvert(encodeToValidChars(tag.getGenre()))));
                out.write("\"/>\r\n");

                out.write("</music>\r\n");
            }

            out.write("</collection>\r\n");
        } catch (IOException e) {
            throw new IOException("Problems writiing \"" + filename + "\"");
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    Logger.getLogger(XMLModelWriter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return musics.size();
    }

    private void loadMusicCollectionFromDirectoy(ArrayList<Music> musics, File dir) {
        File[] listFiles = dir.listFiles();

        if (listFiles != null && listFiles.length > 0) {
            for (File f : listFiles) {
                if (f.getName().endsWith(".mp3")) {
                    try {
                        Music music = getMusic(f);
                        if (music != null) {
                            musics.add(music);
                        }
                    } catch (CannotReadException ex) {
                        Logger.getLogger(MusicCollection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (f.isDirectory()) {
                    loadMusicCollectionFromDirectoy(musics, f);
                }
            }
        }
    }

    private Music getMusic(File file) throws CannotReadException {
        System.out.println(file.getName());

        AudioFile audioFile = AudioFileIO.read(file);
        Music tag = new Music(file.getName().hashCode());
        tag.setFilename(file.getPath());

        //getting the artists
        StringBuilder artist = new StringBuilder();
        List lartist = audioFile.getTag().getArtist();
        for (int i = 0; i < lartist.size(); i++) {
            artist.append(lartist.get(i).toString());
            if (i < lartist.size() - 1) {
                artist.append(" & ");
            }
        }
        tag.setArtist(artist.toString());

        //setting the album
        StringBuilder album = new StringBuilder();
        List lalbum = audioFile.getTag().getAlbum();
        for (int i = 0; i < lalbum.size(); i++) {
            album.append(lalbum.get(i).toString());
            if (i < lalbum.size() - 1) {
                album.append(" & ");
            }
        }
        tag.setAlbum(album.toString());

        //setting the title
        StringBuilder title = new StringBuilder();
        List ltitle = audioFile.getTag().getTitle();
        for (int i = 0; i < ltitle.size(); i++) {
            title.append(ltitle.get(i).toString());
            if (i < ltitle.size() - 1) {
                title.append(" & ");
            }
        }
        tag.setTitle(title.toString());

        //setting the year
        tag.setYear(audioFile.getTag().getFirstYear());

        //setting the genre
        tag.setGenre(audioFile.getTag().getFirstGenre());
        return tag;
    }

    private String convert(String value) {
        if (value != null) {
            value = value.replaceAll("&", "&amp;");
            value = value.replaceAll("<", "&lt;");
            value = value.replaceAll(">", "&gt;");
            value = value.replaceAll("\"", "&quot;");
            value = value.replaceAll("\'", "&#39;");
        } else {
            return "";
        }
        return value;
    }

    private String deConvert(String value) {
        if (value != null) {
            value = value.replaceAll("&amp;", "&");
            value = value.replaceAll("&lt;", "<");
            value = value.replaceAll("&gt;", ">");
            value = value.replaceAll("&quot;", "\"");
            value = value.replaceAll("&#39;", "\'");
        } else {
            return "";
        }
        return value;
    }

    private String encodeToValidChars(String pData) {
        StringBuilder encodedData = new StringBuilder();

        for (int i = 0; i < pData.length(); i++) {
            char ch = pData.charAt(i);
            int chVal = (int) ch;

            if (chVal >= 32 && chVal <= 255) {
                encodedData.append((char) chVal);
            } else {
                encodedData.append(" ");
            }
        }

        return encodedData.toString();
    }

    public static class XMLMusicParser extends DefaultHandler {

        public static final String COLLECTION = "collection";
        public static final String NAME = "name";
        public static final String MUSIC = "music";
        public static final String ID = "id";
        public static final String VALUE = "value";
        public static final String FILENAME = "filename";
        public static final String ARTIST = "artist";
        public static final String ALBUM = "album";
        public static final String TITLE = "title";
        public static final String YEAR = "year";
        public static final String GENRE = "genre";

        public MusicCollection getMusicCollection() {
            return collection;
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            super.endElement(uri, localName, qName);

            if (qName.equalsIgnoreCase(MUSIC)) {
                collection.getMusics().add(musictmp);
            }
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

            if (qName.equalsIgnoreCase(COLLECTION)) {
                collection = new MusicCollection(attributes.getValue(NAME));
            } else if (qName.equalsIgnoreCase(MUSIC)) {
                musictmp = new Music(Integer.parseInt(attributes.getValue(ID)));
            } else if (qName.equalsIgnoreCase(FILENAME)) {
                musictmp.setFilename(attributes.getValue(VALUE));
            } else if (qName.equalsIgnoreCase(ARTIST)) {
                musictmp.setArtist(attributes.getValue(VALUE));
            } else if (qName.equalsIgnoreCase(ALBUM)) {
                musictmp.setAlbum(attributes.getValue(VALUE));
            } else if (qName.equalsIgnoreCase(TITLE)) {
                musictmp.setTitle(attributes.getValue(VALUE));
            } else if (qName.equalsIgnoreCase(YEAR)) {
                musictmp.setYear(attributes.getValue(VALUE));
            } else if (qName.equalsIgnoreCase(GENRE)) {
                musictmp.setGenre(attributes.getValue(VALUE));
            }
        }
        
        private MusicCollection collection;
        private Music musictmp;
    }
    
    private ArrayList<Music> musics;
    private String name;
}
