/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.repositioning.music;

import datamining.clustering.BKmeans;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import distance.dissimilarity.Euclidean;
import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;
import matrix.reader.MatrixReaderComp;
import plsp.repositioning.IterativeProjectionFrame;
import plsp.repositioning.RedistributeControlPoints2D;
import plsp.technique.PLSPProjection2D;
import projection.model.ProjectionModel;
import projection.model.Scalar;
import projection.technique.idmap.IDMAPProjection.InitializationType;
import projection.technique.idmap.IDMAPProjectionComp;
import projection.util.ProjectionConstants;
import projection.util.ProjectionUtil;
import projection.view.ProjectionFrameComp;
import visualizationbasics.color.ColorScaleFactory.ColorScaleType;

/**
 *
 * @author paulovich
 */
public class IterativeProjectionFrameMusic extends IterativeProjectionFrame {

    public IterativeProjectionFrameMusic() {
        this.view = new IterativeViewPanelMusic(this);
        this.scrollPanel.setViewportView(view);
    }

    @Override
    protected void init() {
        try {
            MusicCollection collection = MusicCollection.readMusicCollectionFromXML("/home/paulovich/musics.xml");

            MatrixReaderComp reader = new MatrixReaderComp();
            reader.setFilename("/home/paulovich/musics.data");
            reader.execute();
            matrix = reader.output();

            int nrclusters = (int) Math.sqrt(matrix.getRowCount());
            BKmeans bkmeans = new BKmeans(nrclusters);
            clusters = bkmeans.execute(new Euclidean(), matrix);

            PLSPProjection2D plsp = new PLSPProjection2D();
            cpoints = plsp.getControlPoints(clusters, matrix, new Euclidean(), PLSPProjection2D.SampleType.CLUSTERING);

            //creating the matrix with all control points
            AbstractMatrix cpmatrix = MatrixFactory.getInstance(matrix.getClass());
            for (int i = 0; i < cpoints.size(); i++) {
                for (int j = 0; j < cpoints.get(i).size(); j++) {
                    cpmatrix.addRow(matrix.getRow(cpoints.get(i).get(j)), matrix.getLabel(cpoints.get(i).get(j)));
                }
            }

            //////////////
            //adding the class information
            if (classInfoCheckBox.isSelected()) {
                cpmatrix = addCdata(cpmatrix, Float.parseFloat(weightTextField.getText()));
            }
            //////////////

            //initial projection of the control points
            IDMAPProjectionComp idmap = new IDMAPProjectionComp();
            idmap.setDissimilarityType(DissimilarityType.EUCLIDEAN);
            idmap.setFractionDelta(8.0f);
            idmap.setInitialization(InitializationType.FASTMAP);
            idmap.setNumberIterations(100);
            idmap.input(cpmatrix);
            idmap.execute();
            cpprojection = idmap.output();

            //creating the model
            model = new MusicProjectionModel();
            ((ProjectionModel) model).changeColorScaleType(ColorScaleType.CATEGORY_SCALE);
            Scalar cdata = ((ProjectionModel) model).addScalar(ProjectionConstants.CDATA);
            int nrows = cpprojection.getRowCount();

            for (int i = 0; i < nrows; i++) {
                AbstractVector row = cpprojection.getRow(i);
                MusicProjectionInstance mpi = new MusicProjectionInstance(row.getId(),
                        row.getValue(0), row.getValue(1));
                model.addInstance(mpi);
                mpi.setScalarValue(cdata, row.getKlass());
                mpi.setMusic(collection.getMusicById(row.getId()));
            }

            setModel(model);

        } catch (IOException ex) {
            Logger.getLogger(IterativeProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

//         try {
//            PropertiesManager spm = PropertiesManager.getInstance(PLSPConstants.PROPFILENAME);
//
//            //opening the data
//            int resultdata = OpenDialog.showOpenDialog(spm, new DATAandBINMultipleFilter(), this);
//            if (resultdata == JFileChooser.APPROVE_OPTION) {
//                String filenamedata = OpenDialog.getFilename();
//
//                MatrixReaderComp reader = new MatrixReaderComp();
//                reader.setFilename(filenamedata);
//                reader.execute();
//                matrix = reader.output();
//
//                int nrclusters = (int) Math.sqrt(matrix.getRowCount());
//                BKmeans bkmeans = new BKmeans(nrclusters);
//                clusters = bkmeans.execute(new Euclidean(), matrix);
//
//                PLSPProjection2D plsp = new PLSPProjection2D();
//                cpoints = plsp.getControlPoints(clusters, matrix, new Euclidean(), PLSPProjection2D.SampleType.CLUSTERING);
//
//                //creating the matrix with all control points
//                AbstractMatrix cpmatrix = MatrixFactory.getInstance(matrix.getClass());
//                for (int i = 0; i < cpoints.size(); i++) {
//                    for (int j = 0; j < cpoints.get(i).size(); j++) {
//                        cpmatrix.addRow(matrix.getRow(cpoints.get(i).get(j)), matrix.getLabel(cpoints.get(i).get(j)));
//                    }
//                }
//
//                //////////////
//                //adding the class information
//                if (classInfoCheckBox.isSelected()) {
//                    cpmatrix = addCdata(cpmatrix);
//                }
//                //////////////
//
//                //initial projection of the control points
//                IDMAPProjectionComp idmap = new IDMAPProjectionComp();
//                idmap.setDissimilarityType(DissimilarityType.EUCLIDEAN);
//                idmap.setFractionDelta(8.0f);
//                idmap.setInitialization(InitializationType.FASTMAP);
//                idmap.setNumberIterations(100);
//                idmap.input(cpmatrix);
//                idmap.execute();
//                cpprojection = idmap.output();
//
//                //creating the model
//                model = new MusicProjectionModel();
//                ((ProjectionModel) model).changeColorScaleType(ColorScaleType.CATEGORY_SCALE);
//                Scalar cdata = ((ProjectionModel) model).addScalar(ProjectionConstants.CDATA);
//                int nrows = cpprojection.getRowCount();
//
//                for (int i = 0; i < nrows; i++) {
//                    AbstractVector row = cpprojection.getRow(i);
//                    MusicProjectionInstance mpi = new MusicProjectionInstance(((ProjectionModel) model), row.getId(),
//                            row.getValue(0), row.getValue(1));
//                    mpi.setScalarValue(cdata, row.getKlass());
//                }
//
//                setModel(model);
//            }
//        } catch (IOException ex) {
//            Logger.getLogger(IterativeProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    @Override
    protected void finish() {
        if (model != null) {
            try {
                ////////////
                RedistributeControlPoints2D red = new RedistributeControlPoints2D();
                ArrayList<ArrayList<Integer>> clusters_aux = red.execute(matrix,
                        getRepositionedProjection(), cpoints, (int) Math.sqrt(matrix.getRowCount()));
                ArrayList<ArrayList<Integer>> cpoints_aux = red.getNewControlPoints();
                AbstractMatrix newproj = red.getNewControlPointsProjection();
                ////////////

                PLSPProjection2D plsp = new PLSPProjection2D();
                plsp.setClusters(clusters_aux);
                plsp.setControlPoints(cpoints_aux);
                plsp.setControlPointsProjection(newproj);
                AbstractMatrix projection = plsp.project(matrix, new Euclidean());

                //creating the model
                MusicProjectionModel finalmodel = new MusicProjectionModel();
                ((ProjectionModel) finalmodel).changeColorScaleType(ColorScaleType.CATEGORY_SCALE);
                Scalar cdata = ((ProjectionModel) finalmodel).addScalar(ProjectionConstants.CDATA);
                Scalar dots = ((ProjectionModel) finalmodel).addScalar(ProjectionConstants.DOTS);
                int nrows = projection.getRowCount();

                for (int i = 0; i < nrows; i++) {
                    AbstractVector row = projection.getRow(i);
                    MusicProjectionInstance mpi = new MusicProjectionInstance(row.getId(),
                            row.getValue(0), row.getValue(1));
                    finalmodel.addInstance(mpi);
                    mpi.setScalarValue(cdata, row.getKlass());
                    mpi.setScalarValue(dots, 0);
                }

                finalmodel.changeColorScaleType(((ProjectionModel) model).getColorTable().getColorScaleType());

                ProjectionFrameComp frame = new ProjectionFrameComp();
                frame.setTitle("Complete Projection");
                frame.setSize(new Dimension(800, 800));
                frame.input(finalmodel);
                frame.execute();
            } catch (IOException ex) {
                Logger.getLogger(IterativeProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        ProjectionUtil.log(false, false);
        IterativeProjectionFrameMusic frame = new IterativeProjectionFrameMusic();
        frame.setSize(800, 800);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);

    }
}
