/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.repositioning.image;

import datamining.clustering.BKmeans;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import distance.dissimilarity.Euclidean;
import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;
import matrix.reader.MatrixReaderComp;
import plsp.repositioning.IterativeProjectionFrame;
import plsp.repositioning.RedistributeControlPoints2D;
import plsp.technique.PLSPProjection2D;
import plsp.util.PLSPConstants;
import projection.model.ProjectionModel;
import projection.model.Scalar;
import projection.technique.idmap.IDMAPProjection.InitializationType;
import projection.technique.idmap.IDMAPProjectionComp;
import projection.util.ProjectionConstants;
import projection.util.ProjectionUtil;
import projection.view.ProjectionFrameComp;
import visualizationbasics.color.ColorScaleFactory.ColorScaleType;
import visualizationbasics.util.OpenDialog;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.filter.DATAandBINMultipleFilter;
import visualizationbasics.util.filter.ZIPFilter;

/**
 *
 * @author paulovich
 */
public class IterativeProjectionFrameImage extends IterativeProjectionFrame {

    public IterativeProjectionFrameImage() {
        buttonPanel.remove(classInfoCheckBox);
        buttonPanel.remove(weightTextField);
        buttonPanel.revalidate();
        buttonPanel.repaint();
    }

    @Override
    protected void init() {
        try {
            PropertiesManager spm = PropertiesManager.getInstance(PLSPConstants.PROPFILENAME);

            //opening the images dataset
            int resultim = OpenDialog.showOpenDialog(spm, new ZIPFilter(), this);

            if (resultim == JFileChooser.APPROVE_OPTION) {
                String filenameim = OpenDialog.getFilename();
                imcol = new ImageCollection(filenameim);

                //opening the data
                int resultdata = OpenDialog.showOpenDialog(spm, new DATAandBINMultipleFilter(), this);

                if (resultdata == JFileChooser.APPROVE_OPTION) {
                    String filenamedata = OpenDialog.getFilename();

                    MatrixReaderComp reader = new MatrixReaderComp();
                    reader.setFilename(filenamedata);
                    reader.execute();
                    matrix = reader.output();

                     matrix = addCdata(matrix, 5.0f);

                    int nrclusters = (int) Math.sqrt(matrix.getRowCount());
                    BKmeans bkmeans = new BKmeans(nrclusters);
                    clusters = bkmeans.execute(new Euclidean(), matrix);

                    PLSPProjection2D plsp = new PLSPProjection2D();
                    plsp.setExponent(0.7f);
                    cpoints = plsp.getControlPoints(clusters, matrix, new Euclidean(), PLSPProjection2D.SampleType.CLUSTERING);

                    //creating the matrix with all control points
                    AbstractMatrix cpmatrix = MatrixFactory.getInstance(matrix.getClass());
                    for (int i = 0; i < cpoints.size(); i++) {
                        for (int j = 0; j < cpoints.get(i).size(); j++) {
                            cpmatrix.addRow(matrix.getRow(cpoints.get(i).get(j)), matrix.getLabel(cpoints.get(i).get(j)));
                        }
                    }

//                    //////////////
//                    //adding the class information
//                    if (classInfoCheckBox.isSelected()) {
//                        cpmatrix = addCdata(cpmatrix, Float.parseFloat(weightTextField.getText()));
//                    }
//                    //////////////

                    //initial projection of the control points
                    IDMAPProjectionComp idmap = new IDMAPProjectionComp();
                    idmap.setDissimilarityType(DissimilarityType.EUCLIDEAN);
                    idmap.setFractionDelta(8.0f);
                    idmap.setInitialization(InitializationType.FASTMAP);
                    idmap.setNumberIterations(100);
                    idmap.input(cpmatrix);
                    idmap.execute();
                    cpprojection = idmap.output();

                    //creating the model
                    model = new ProjectionModel();
                    ((ProjectionModel) model).changeColorScaleType(ColorScaleType.CATEGORY_SCALE);
                    Scalar cdata = ((ProjectionModel) model).addScalar(ProjectionConstants.CDATA);
                    int nrows = cpprojection.getRowCount();

                    for (int i = 0; i < nrows; i++) {
                        AbstractVector row = cpprojection.getRow(i);
                        ImageProjectionInstance impi = new ImageProjectionInstance(row.getId(),
                                row.getValue(0), row.getValue(1));
                        model.addInstance(impi);
                        impi.setImage(imcol.getImage(cpmatrix.getLabel(i)));
                        impi.setScalarValue(cdata, row.getKlass());
                    }

                    setModel(model);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(IterativeProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void finish() {
        if (model != null) {
            try {
//                AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
                
               

                ////////////
//                Redistribute red = new Redistribute();
//                red.execute(getRepositionedProjection(), cpoints, matrix, diss, (int) Math.sqrt(matrix.getRowCount()));
//                ArrayList<ArrayList<Integer>> clusters_aux = red.getNewClusters();
//                ArrayList<ArrayList<Integer>> cpoints_aux = red.getNewControlPoints();
//                AbstractMatrix newproj = red.getNewControlPointsProjection();
                RedistributeControlPoints2D red = new RedistributeControlPoints2D();
                ArrayList<ArrayList<Integer>> clusters_aux = red.execute(matrix,
                        getRepositionedProjection(), cpoints, (int) Math.sqrt(matrix.getRowCount()));
                ArrayList<ArrayList<Integer>> cpoints_aux = red.getNewControlPoints();
                AbstractMatrix newproj = red.getNewControlPointsProjection();
                ////////////                

                PLSPProjection2D plsp = new PLSPProjection2D();
                plsp.setClusters(clusters_aux);
                plsp.setControlPoints(cpoints_aux);
                plsp.setControlPointsProjection(newproj);
                AbstractMatrix projection = plsp.project(matrix, new Euclidean());
                plsp.spread(projection, 2.0f);

                //creating the model
                ProjectionModel finalmodel = new ProjectionModel();
                ((ProjectionModel) finalmodel).changeColorScaleType(ColorScaleType.CATEGORY_SCALE);
                Scalar cdata = ((ProjectionModel) finalmodel).addScalar(ProjectionConstants.CDATA);
                Scalar dots = ((ProjectionModel) finalmodel).addScalar(ProjectionConstants.DOTS);
                int nrows = projection.getRowCount();

                for (int i = 0; i < nrows; i++) {
                    AbstractVector row = projection.getRow(i);
                    ImageProjectionInstance impi = new ImageProjectionInstance(row.getId(),
                            row.getValue(0), row.getValue(1));
                    finalmodel.addInstance(impi);

                    if (imcol != null) {
                        impi.setImage(imcol.getImage(matrix.getLabel(i)));
                    }

                    impi.setScalarValue(cdata, row.getKlass());
                    impi.setScalarValue(dots, 0);
                }

                finalmodel.changeColorScaleType(((ProjectionModel) model).getColorTable().getColorScaleType());

                ProjectionFrameComp frame = new ProjectionFrameComp();
                frame.setTitle("Complete Projection");
                frame.setSize(new Dimension(800, 800));
                frame.input(finalmodel);
                frame.execute();
            } catch (IOException ex) {
                Logger.getLogger(IterativeProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        ProjectionUtil.log(false, false);
        IterativeProjectionFrameImage frame = new IterativeProjectionFrameImage();
        frame.setSize(800, 800);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }
    
    private ImageCollection imcol;
}
