/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.repositioning.image;

import java.awt.*;
import java.awt.image.BufferedImage;
import projection.model.ProjectionInstance;

/**
 *
 * @author paulovich
 */
public class ImageProjectionInstance extends ProjectionInstance {

    public ImageProjectionInstance(int id, float x, float y) {
        super(id, x, y);
    }

    public void setImage(Image image) {
        if (image != null) {
            origimage = image;
            redimage = image.getScaledInstance(40, 40, 0);
        }
    }

    public Image getImage() {
        return origimage;
    }

    public void setImageDimension(int width, int height) {
        redimage = origimage.getScaledInstance(width, height, Image.SCALE_DEFAULT);
    }

    @Override
    public void draw(BufferedImage image, boolean highquality) {
        Graphics2D g2 = (Graphics2D) image.getGraphics();

        if (highquality) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }

        if (redimage != null) {
            int w = redimage.getWidth(null);
            int h = redimage.getHeight(null);
            g2.drawImage(redimage, ((int) x) - w / 2, ((int) y) - h / 2, null);

            g2.setStroke(new BasicStroke(2.0f));
            g2.setColor(getColor());
            g2.drawRect(((int) x) - w / 2 - 2, ((int) y) - h / 2 - 2, w + 3, h + 3);

            if (selected) {
                g2.setStroke(new BasicStroke(2.0f));
                g2.setColor(Color.RED);
                g2.drawRect(((int) x) - w / 2, ((int) y) - h / 2, w, h);
            }
        }

        if (showlabel) {
            drawLabel(g2, (int) x, (int) y);
        }

        g2.dispose();
    }

    @Override
    public void drawLabel(Graphics2D g2, int x, int y) {
        int w = origimage.getWidth(null);
        int h = origimage.getHeight(null);
        g2.drawImage(origimage, x - w / 2, y - h / 2, null);
    }

    @Override
    public boolean isInside(int x, int y) {
        if (redimage != null) {
            int w = redimage.getWidth(null) / 2;
            int h = redimage.getHeight(null) / 2;
            return (Math.abs((this.x - x)) <= w && Math.abs((this.y - y)) <= h);
        }

        return false;
    }

    @Override
    public boolean isInside(Rectangle rect) {
        return ((x >= rect.x) && (x <= rect.x + rect.width))
                && ((y >= rect.y) && (y <= rect.y + rect.height));
    }
    
    private Image origimage;
    private Image redimage;
}
