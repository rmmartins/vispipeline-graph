/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.technique;

import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Projection.Technique",
name = "Piecewise Least Square Projection (P-LSP)",
description = "Project points from a multidimensional space to the plane "
+ "preserving the neighborhood relations.")
public class PLSPProjection2DComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        //projecting
        PLSPProjection2D plsp = new PLSPProjection2D();
//        plsp.setNumberNeighbors(nrneighbors);
        plsp.setClusters(clusters);
        plsp.setControlPoints(cpoints);
        plsp.setControlPointsProjection(cpprojection);

        if (matrix != null) { //using a matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            projection = plsp.project(matrix, diss);
        } else {
            throw new IOException("A points matrix should be provided.");
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix,
            @Param(name = "control points") ArrayList<ArrayList<Integer>> cpoints,
            @Param(name = "clusters") ArrayList<ArrayList<Integer>> clusters,
            @Param(name = "control points projection") AbstractMatrix cpprojection) {
        this.matrix = matrix;
        this.cpoints = cpoints;
        this.clusters = clusters;
        this.cpprojection = cpprojection;
    }

    public AbstractMatrix output() {
        return projection;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new PLSPProjection2DParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        projection = null;
        matrix = null;
    }

    /**
//     * @return the nrneighbors
//     */
//    public int getNumberNeighbors() {
//        return nrneighbors;
//    }
//
//    /**
//     * @param nrneighbors the nrneighbors to set
//     */
//    public void setNumberNeighbors(int nrneighbors) {
//        this.nrneighbors = nrneighbors;
//    }

    /**
     * @return the disstype
     */
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    /**
     * @param disstype the disstype to set
     */
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public int getNumberInstances() {
        if (matrix != null) {
            return matrix.getRowCount();
        }

        return 0;
    }

    public static final long serialVersionUID = 1L;
//    private int nrneighbors = 10;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private transient PLSPProjection2DParamView paramview;
    private transient AbstractMatrix projection;
    private transient AbstractMatrix matrix;
    private transient AbstractMatrix cpprojection;
    private transient ArrayList<ArrayList<Integer>> cpoints;
    private transient ArrayList<ArrayList<Integer>> clusters;
}
