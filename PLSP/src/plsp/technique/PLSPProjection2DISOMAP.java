/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package plsp.technique;

import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import projection.model.ProjectionModelComp;
import projection.technique.isomap.ISOMAPProjection;
import projection.util.ProjectionUtil;
import projection.view.ProjectionFrameComp;

/**
 *
 * @author paulovich
 */
public class PLSPProjection2DISOMAP extends PLSPProjection2D {

    @Override
    protected AbstractMatrix projectControlPoints(AbstractMatrix matrix, AbstractDissimilarity diss) throws IOException {
        ISOMAPProjection isomap = new ISOMAPProjection();
        isomap.setNumberNeighbors(8);
        return isomap.project(matrix, diss);
    }

     public static void main(String args[]) throws IOException {
        String filename = "/home/paulovich/Dropbox/dados/swissroll10000.data";
        AbstractMatrix matrix = MatrixFactory.getInstance(filename);

        ProjectionUtil.log(false, false);

        long start = System.currentTimeMillis();
        PLSPProjection2DISOMAP plsp = new PLSPProjection2DISOMAP();
//        plsp.setNumberNeighbors(8);
        plsp.setSampleType(SampleType.RANDOM);
        AbstractMatrix projection = plsp.project(matrix, new Euclidean());
        long finish = System.currentTimeMillis();
        System.out.println("PLSP time: " + (finish - start) / 1000.0f + "s");

        //.save(filename + "-plsp.prj");

        ProjectionModelComp model = new ProjectionModelComp();
        model.input(projection);
        model.execute();

        ProjectionFrameComp frame = new ProjectionFrameComp();
        frame.input(model.output());
        frame.setTitle("P-LSP");
        frame.execute();
    }

}
