/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.technique;

import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.linalg.CholeskyDecomposition;
import datamining.clustering.BKmeans;
import datamining.neighbors.KNN;
import datamining.neighbors.Pair;
import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import lspsolver.Solver;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import matrix.util.MatrixUtils;
import projection.technique.Projection;
import projection.technique.fastmap.FastmapProjection2D;

/**
 *
 * @author paulovich
 */
public class PLSPProjection2D implements Projection {

    public enum SampleType {

        RANDOM("Random sampling"),
        CLUSTERING("Clustering sampling");

        private SampleType(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
        private final String name;
    }

    public enum SolverType {

        PARALLEL("Parallel solver"),
        SEQUENTIAL("Sequential solver");

        private SolverType(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
        private final String name;
    }

    public PLSPProjection2D() {
        this.exp = 0.5f;
//        this.nrneighbors = 8;
        this.samptype = SampleType.RANDOM;
        this.threadpoolsize = 3;
        this.solvertype = SolverType.PARALLEL;
    }

    @Override
    public AbstractMatrix project(DistanceMatrix dmat) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public AbstractMatrix project(AbstractMatrix matrix, AbstractDissimilarity diss) throws IOException {
        long start = System.currentTimeMillis();

        if (clusters == null || cpoints == null || cpprojection == null) {
            //split the data into clusters with approximate equal sizes
            clusters = createClusters(matrix, diss);

            //get the control points
            cpoints = getControlPoints(clusters, matrix, diss, samptype);

            //creating the matrix with all control points
            AbstractMatrix cpmatrix = MatrixFactory.getInstance(matrix.getClass());
            for (int i = 0; i < cpoints.size(); i++) {
                for (int j = 0; j < cpoints.get(i).size(); j++) {
                    cpmatrix.addRow(matrix.getRow(cpoints.get(i).get(j)));
                }
            }

            //project the control points
            cpprojection = projectControlPoints(cpmatrix, diss);
        }

        //projecting the clusters
        float[][] finalproj = new float[matrix.getRowCount()][];
        int nrcluster = clusters.size();
        int initcpproj = 0;
        if (solvertype == SolverType.SEQUENTIAL) { //projectig sequentially
            for (int i = 0; i < nrcluster; i++) {
                projectCluster(finalproj,
                        clusters.get(i),
                        cpprojection,
                        initcpproj,
                        cpoints.get(i),
                        matrix,
                        diss);

                initcpproj += cpoints.get(i).size();
            }
        } else { //projecting in parallel
            ArrayList<ParallelSolver> threads = new ArrayList<ParallelSolver>();
            for (int i = 0; i < nrcluster; i++) {
                ParallelSolver ps = new ParallelSolver(finalproj,
                        clusters.get(i),
                        cpprojection,
                        initcpproj,
                        cpoints.get(i),
                        matrix,
                        diss);
                threads.add(ps);

                initcpproj += cpoints.get(i).size();
            }

            try {
                ExecutorService executor = Executors.newFixedThreadPool(threadpoolsize);
                executor.invokeAll(threads);
                executor.shutdown();
            } catch (InterruptedException ex) {
                Logger.getLogger(PLSPProjection2D.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        long finish = System.currentTimeMillis();
        Logger.getLogger(this.getClass().getName()).log(Level.INFO,
                "Piecewise Least Square Projection (P-LSP) time: {0}s",
                (finish - start) / 1000.0f);

        //creating the final projection
        AbstractMatrix projection = new DenseMatrix();
        ArrayList<String> attributes = new ArrayList<String>();
        attributes.add("x");
        attributes.add("y");
        projection.setAttributes(attributes);

        ArrayList<Integer> ids = matrix.getIds();
        float[] cdata = matrix.getClassData();
        ArrayList<String> labels = matrix.getLabels();

        for (int i = 0; i < finalproj.length; i++) {
            AbstractVector vector = new DenseVector(finalproj[i], ids.get(i), cdata[i]);

            if (labels.isEmpty()) {
                projection.addRow(vector);
            } else {
                projection.addRow(vector, labels.get(i));
            }
        }

        return projection;
    }

    public void setClusters(ArrayList<ArrayList<Integer>> clusters) {
        this.clusters = clusters;
    }

    public void setControlPoints(ArrayList<ArrayList<Integer>> cpoints) {
        this.cpoints = cpoints;
    }

    public void setControlPointsProjection(AbstractMatrix cpprojection) {
        this.cpprojection = cpprojection;
    }

//    public void setNumberNeighbors(int nrneighbors) {
//        this.nrneighbors = nrneighbors;
//    }
    public void setSampleType(SampleType samptype) {
        this.samptype = samptype;
    }

    public void setExponent(float exp) {
        this.exp = exp;
    }

    public void spread(AbstractMatrix projection, float factor) throws IOException {
        //calculating the centroids of each cluster on the projection
        AbstractMatrix centroids = MatrixFactory.getInstance(projection.getClass());
        for (int i = 0; i < clusters.size(); i++) {
            AbstractMatrix aux = MatrixFactory.getInstance(projection.getClass());
            for (int j = 0; j < clusters.get(i).size(); j++) {
                aux.addRow(projection.getRow(clusters.get(i).get(j)));
            }
            centroids.addRow(MatrixUtils.mean(aux));
        }

        //move the points from the centroid of the cluster
        for (int i = 0; i < clusters.size(); i++) {
            float[] centroidvec = centroids.getRow(i).toArray();

            //getting the width and height of the cluster
            float maxx = Float.NEGATIVE_INFINITY;
            float maxy = Float.NEGATIVE_INFINITY;
            float minx = Float.POSITIVE_INFINITY;
            float miny = Float.POSITIVE_INFINITY;

            for (int j = 0; j < clusters.get(i).size(); j++) {
                float[] projvec = projection.getRow(clusters.get(i).get(j)).toArray();

                if (maxx < projvec[0]) {
                    maxx = projvec[0];
                }

                if (minx > projvec[0]) {
                    minx = projvec[0];
                }

                if (maxy < projvec[1]) {
                    maxy = projvec[1];
                }

                if (miny > projvec[1]) {
                    miny = projvec[1];
                }
            }

            float width = maxx - minx;
            float height = maxy - miny;

            for (int j = 0; j < clusters.get(i).size(); j++) {
                float[] projvec = projection.getRow(clusters.get(i).get(j)).toArray();

                //remove the cluster centroid
                projvec[0] = projvec[0] - centroidvec[0];
                projvec[1] = projvec[1] - centroidvec[1];

                if (width > height) {
                    projvec[0] = projvec[0] * factor;
                    projvec[1] = projvec[1] * (width / height) * factor;
                } else {
                    projvec[0] = projvec[0] * (height / width) * factor;
                    projvec[1] = projvec[1] * factor;
                }

                projvec[0] = projvec[0] + centroidvec[0];
                projvec[1] = projvec[1] + centroidvec[1];

                projection.getRow(clusters.get(i).get(j)).setValue(0, projvec[0]);
                projection.getRow(clusters.get(i).get(j)).setValue(1, projvec[1]);
            }
        }
    }

    /**
     * This method defines which technique is used to project the control points
     * @param matrix The control points data
     * @param diss The employed dissimilarity
     * @return The projection of the control points
     * @throws IOException
     */
    protected AbstractMatrix projectControlPoints(AbstractMatrix matrix,
            AbstractDissimilarity diss) throws IOException {
        FastmapProjection2D fastmap = new FastmapProjection2D();
        AbstractMatrix proj = fastmap.project(matrix, diss);
        FastForceScheme2D ffs = new FastForceScheme2D();
        ffs.setProjection(proj);
        ffs.setNumberIterations(50);
        return ffs.project(matrix, diss);
    }

    private ArrayList<ArrayList<Integer>> createClusters(AbstractMatrix matrix,
            AbstractDissimilarity diss) throws IOException {
        int nrclusters = (int) Math.sqrt(matrix.getRowCount());
        BKmeans bkmeans = new BKmeans(nrclusters);
        return bkmeans.execute(diss, matrix);
    }

    public ArrayList<ArrayList<Integer>> getControlPoints(ArrayList<ArrayList<Integer>> clusters,
            AbstractMatrix matrix, AbstractDissimilarity diss, SampleType sampletype) throws IOException {
        ArrayList<ArrayList<Integer>> cpointslocal = new ArrayList<ArrayList<Integer>>();

        //percentage of points of each cluster to use
        float perc = (float) Math.pow(matrix.getRowCount(), exp) / matrix.getRowCount();

        if (sampletype == SampleType.CLUSTERING) {
            //for each patch
            for (int i = 0; i < clusters.size(); i++) {
                //defining the number of clusters to be found
                int nrcluster = (int) (clusters.get(i).size() * perc);
                nrcluster = (nrcluster > 3) ? nrcluster
                        : (clusters.get(i).size() > 3) ? 3 : clusters.get(i).size();

                if (clusters.get(i).size() > 3) {
                    //creating the matrix with the points on the patch
                    AbstractMatrix matcluster = MatrixFactory.getInstance(matrix.getClass());
                    for (int j = 0; j < clusters.get(i).size(); j++) {
                        matcluster.addRow(matrix.getRow(clusters.get(i).get(j)));
                    }

                    BKmeans intbkmeans = new BKmeans(nrcluster);
                    intbkmeans.execute(diss, matcluster);
                    int[] med = intbkmeans.getMedoids(matcluster);

                    ArrayList<Integer> cpoints_aux = new ArrayList<Integer>();
                    for (int j = 0; j < med.length; j++) {
                        cpoints_aux.add(clusters.get(i).get(med[j]));
                    }

                    cpointslocal.add(cpoints_aux);
                } else {
                    ArrayList<Integer> cpoints_aux = new ArrayList<Integer>();
                    for (int j = 0; j < clusters.get(i).size(); j++) {
                        cpoints_aux.add(clusters.get(i).get(j));
                    }

                    cpointslocal.add(cpoints_aux);
                }
            }
        } else if (sampletype == SampleType.RANDOM) {
            for (int i = 0; i < clusters.size(); i++) {
                //defining the number of control points on the cluster
                int samplesize = (int) (clusters.get(i).size() * perc) - 1;
                samplesize = (samplesize > 2) ? samplesize : 2;

                HashSet<Integer> sample = new HashSet<Integer>();
                Random random = new Random(System.currentTimeMillis());

                while (sample.size() < samplesize) {
                    int index = (int) (random.nextFloat() * (clusters.get(i).size()));
                    if (index < matrix.getRowCount()) {
                        sample.add(clusters.get(i).get(index));
                    }
                }

                ArrayList<Integer> cpoints_aux = new ArrayList<Integer>();
                Iterator<Integer> it = sample.iterator();
                while (it.hasNext()) {
                    int index = it.next();
                    cpoints_aux.add(index);
                }

                cpointslocal.add(cpoints_aux);
            }
        }

        return cpointslocal;
    }

    private void projectCluster(float[][] finalproj, //final projection
            ArrayList<Integer> cluster, //clusters
            AbstractMatrix cpprojection, //control points projection
            int initcpproj, //indicates the first line of control points projection
            ArrayList<Integer> cpoints, //control points
            AbstractMatrix matrix, //all points
            AbstractDissimilarity diss) throws IOException {
        projectClusterColt(finalproj, cluster, cpprojection, initcpproj, cpoints, matrix, diss);
//        projectClusterDLL(finalproj, cluster, cpprojection, initcpproj, cpoints, matrix, diss);
    }

    private void projectClusterColt(float[][] finalproj, //final projection
            ArrayList<Integer> cluster, //clusters
            AbstractMatrix cpprojection, //control points projection
            int initcpproj, //indicates the first line of control points projection
            ArrayList<Integer> cpoints, //control points
            AbstractMatrix matrix, //all points
            AbstractDissimilarity diss) throws IOException {

        if (cluster.size() > 3) {
            long start = System.currentTimeMillis();

            int clsize = cluster.size(); //cluster size
            int cpsize = cpoints.size(); //control points size

            //creating the matrix with all points on the cluster
            AbstractMatrix projmatrix = MatrixFactory.getInstance(matrix.getClass());
            for (int i = 0; i < clsize; i++) {
                projmatrix.addRow(matrix.getRow(cluster.get(i)));
            }

            //creating the neighborhood graph
            int local_nrneighbors = calculateNrNeighbors(clsize);
            local_nrneighbors = (local_nrneighbors < clsize)
                    ? local_nrneighbors : clsize - 1;

            KNN ann = new KNN(local_nrneighbors);
            Pair[][] neighbors = ann.execute(projmatrix, diss);

            //creating the mesh
//            MeshGenerator meshgen = new MeshGenerator();
//            neighbors = meshgen.execute(neighbors, projmatrix, diss);
            CreateNNG nng = new CreateNNG();
            nng.execute(neighbors, projmatrix, diss);

            //map the elements on the cluster to sequential ids
            HashMap<Integer, Integer> indexes = new HashMap<Integer, Integer>();
            for (int i = 0; i < clsize; i++) {
                indexes.put(cluster.get(i), i);
            }

            int nrows = clsize + cpsize;
            int ncolumns = clsize;

            ////////////////////////////////////////////
            //creating matrix A
            DoubleMatrix2D A = new DenseDoubleMatrix2D(nrows, ncolumns);

            for (int i = 0; i < clsize; i++) {
                A.setQuick(i, i, 1.0f);

                for (int j = 0; j < neighbors[i].length; j++) {
                    A.setQuick(i, neighbors[i][j].index, (-(1.0f / neighbors[i].length)));
                }
            }

            float w = 20.0f; //weigthing the control points

            for (int i = 0; i < cpsize; i++) {
                A.setQuick((clsize + i), indexes.get(cpoints.get(i)), w);
            }

            ////////////////////////////////////////////
            //creating matrix B
            DoubleMatrix2D B = new DenseDoubleMatrix2D(nrows, 2);

            for (int i = 0; i < cpsize; i++) {
                float[] array = cpprojection.getRow(i + initcpproj).toArray();
                B.setQuick((clsize + i), 0, array[0] * w);
                B.setQuick((clsize + i), 1, array[1] * w);
            }

            ///////////////////////////////////////////
            //solving the system
            DoubleMatrix2D AtA = A.zMult(A, null, 1.0, 1.0, true, false);
            DoubleMatrix2D AtB = A.zMult(B, null, 1.0, 1.0, true, false);

            CholeskyDecomposition chol = new CholeskyDecomposition(AtA);
            DoubleMatrix2D result = chol.solve(AtB);

            int resultsize = result.rows();
            for (int i = 0; i < resultsize; i++) {
                int index = cluster.get(i);
                finalproj[index] = new float[2];
                finalproj[index][0] = (float) result.getQuick(i, 0);
                finalproj[index][1] = (float) result.getQuick(i, 1);
            }

            long finish = System.currentTimeMillis();
            Logger.getLogger(this.getClass().getName()).log(Level.INFO,
                    "Solving the system using Colt time: {0}s", (finish - start) / 1000.0f);
        } else { //cluster with less than 4 elements
            //copying the control points to the output
            for (int i = 0; i < cpoints.size(); i++) {
                int index = cpoints.get(i);
                finalproj[index] = cpprojection.getRow(i).toArray();
            }

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,
                    "PLSP: projecting a cluster with less than 3 elements!");
        }
    }

    private void projectClusterDLL(float[][] finalproj, //final projection
            ArrayList<Integer> cluster, //clusters
            AbstractMatrix cpprojection, //control points projection
            int initcpproj, //indicates the first line of control points projection
            ArrayList<Integer> cpoints, //control points
            AbstractMatrix matrix, //all points
            AbstractDissimilarity diss) throws IOException {

        if (cluster.size() > 3) {
            long start = System.currentTimeMillis();

            int clsize = cluster.size(); //cluster size
            int cpsize = cpoints.size(); //controlo points size

            //creatng the matrix with all points on the cluster
            AbstractMatrix projmatrix = MatrixFactory.getInstance(matrix.getClass());
            for (int i = 0; i < clsize; i++) {
                projmatrix.addRow(matrix.getRow(cluster.get(i)));
            }

            //creating the neighborhood graph
            int local_nrneighbors = calculateNrNeighbors(clsize);
            local_nrneighbors = (local_nrneighbors < clsize)
                    ? local_nrneighbors : clsize - 1;

            KNN ann = new KNN(local_nrneighbors);
            Pair[][] neighbors = ann.execute(projmatrix, diss);

//            MeshGenerator meshgen = new MeshGenerator();
//            neighbors = meshgen.execute(neighbors, projmatrix, diss);
            CreateNNG nng = new CreateNNG();
            nng.execute(neighbors, projmatrix, diss);

            //map the elements on the cluster to sequential ids
            HashMap<Integer, Integer> indexes = new HashMap<Integer, Integer>();
            for (int i = 0; i < clsize; i++) {
                indexes.put(cluster.get(i), i);
            }

            int nrows = clsize + cpsize;
            int ncolumns = clsize;

            Solver solver = new Solver(nrows, ncolumns);

            ////////////////////////////////////////////
            //creating matrix A
            for (int i = 0; i < clsize; i++) {
                solver.addToA(i, i, 1.0f);

                for (int j = 0; j < neighbors[i].length; j++) {
                    solver.addToA(i, neighbors[i][j].index, (-(1.0f / neighbors[i].length)));
                }
            }

            float w = 20.0f; //weigthing the control points

            for (int i = 0; i < cpsize; i++) {
                solver.addToA((clsize + i), indexes.get(cpoints.get(i)), w);
            }

            ////////////////////////////////////////////
            //creating matrix B
            for (int i = 0; i < cpsize; i++) {
                float[] array = cpprojection.getRow(i + initcpproj).toArray();
                solver.addToB((clsize + i), 0, array[0] * w);
                solver.addToB((clsize + i), 1, array[1] * w);
            }

            ///////////////////////////////////////////
            //solving the system
            float[] result = solver.solve();
            for (int i = 0; i < result.length; i += 2) {
                int index = cluster.get(i / 2);
                finalproj[index] = new float[2];
                finalproj[index][0] = result[i];
                finalproj[index][1] = result[i + 1];
            }

            long finish = System.currentTimeMillis();
            Logger.getLogger(this.getClass().getName()).log(Level.INFO,
                    "Solving the system using Colt time: {0}s", (finish - start) / 1000.0f);
        } else { //cluster with less than 4 elements
            //copying the control points to the output
            for (int i = 0; i < cpoints.size(); i++) {
                int index = cpoints.get(i);
                finalproj[index] = cpprojection.getRow(i).toArray();
            }

            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,
                    "PLSP: projecting a cluster with less than 3 elements!");
        }
    }

    private int calculateNrNeighbors(int nrpoints) {
        if (nrpoints < 400) {
            return 5;
        } else if (nrpoints < 800) {
            return 8;
        } else if (nrpoints < 1500) {
            return 10;
        } else if (nrpoints < 15000) {
            return 15;
        } else {
            return 20;
        }
    }

    class ParallelSolver implements Callable<Integer> {

        public ParallelSolver(float[][] finalproj,
                ArrayList<Integer> cluster,
                AbstractMatrix cpprojection,
                int initcpproj,
                ArrayList<Integer> cpoints,
                AbstractMatrix matrix,
                AbstractDissimilarity diss) {
            this.finalproj = finalproj;
            this.cluster = cluster;
            this.cpprojection = cpprojection;
            this.initcpproj = initcpproj;
            this.cpoints = cpoints;
            this.matrix = matrix;
            this.diss = diss;
        }

        @Override
        public Integer call() throws Exception {
            try {
                projectCluster(finalproj,
                        cluster,
                        cpprojection,
                        initcpproj,
                        cpoints,
                        matrix,
                        diss);
            } catch (IOException ex) {
                Logger.getLogger(PLSPProjection2D.class.getName()).
                        log(Level.SEVERE, null, ex);
            }

            return 0;
        }
        private float[][] finalproj;
        private ArrayList<Integer> cluster;
        private AbstractMatrix cpprojection;
        private int initcpproj;
        private ArrayList<Integer> cpoints;
        private AbstractMatrix matrix;
        private AbstractDissimilarity diss;
    }

    
    private float exp;
    private SolverType solvertype;
    private SampleType samptype;
    private ArrayList<ArrayList<Integer>> clusters;
    private ArrayList<ArrayList<Integer>> cpoints;
    private AbstractMatrix cpprojection;
//    private int nrneighbors;
    private int threadpoolsize;
}
