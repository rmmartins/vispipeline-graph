/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.technique;

import datamining.clustering.BKmeans;
import distance.dissimilarity.AbstractDissimilarity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;

/**
 *
 * @author Fernando
 */
public class PLSPSampling {

    public enum SampleTypePLSP {

        RANDOM("Random sampling"),
        CLUSTERING("Clustering sampling");

        private SampleTypePLSP(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
        private final String name;
    }

    public PLSPSampling(SampleTypePLSP sampletype) {
        this.sampletype = sampletype;
    }

    public AbstractMatrix execute(ArrayList<ArrayList<Integer>> clusters,
            AbstractMatrix matrix, AbstractDissimilarity diss) throws IOException {
        sampleinst = new ArrayList<ArrayList<Integer>>();

        //percentage of points of each cluster to use
        float perc = (float) Math.pow(matrix.getRowCount(), 0.75) / matrix.getRowCount();

        if (sampletype == SampleTypePLSP.CLUSTERING) {
            //for each patch
            for (int i = 0; i < clusters.size(); i++) {
                //defining the number of clusters to be found
                int nrcluster = (int) (clusters.get(i).size() * perc);
                nrcluster = (nrcluster > 3) ? nrcluster
                        : (clusters.get(i).size() > 3) ? 3 : clusters.get(i).size();

                if (clusters.get(i).size() > 3) {
                    //creating the matrix with the points on the patch
                    AbstractMatrix matcluster = MatrixFactory.getInstance(matrix.getClass());
                    for (int j = 0; j < clusters.get(i).size(); j++) {
                        matcluster.addRow(matrix.getRow(clusters.get(i).get(j)));
                    }

                    BKmeans intbkmeans = new BKmeans(nrcluster);
                    intbkmeans.execute(diss, matcluster);
                    int[] med = intbkmeans.getMedoids(matcluster);

                    ArrayList<Integer> cpoints_aux = new ArrayList<Integer>();
                    for (int j = 0; j < med.length; j++) {
                        cpoints_aux.add(clusters.get(i).get(med[j]));
                    }

                    sampleinst.add(cpoints_aux);
                } else {
                    ArrayList<Integer> cpoints_aux = new ArrayList<Integer>();
                    for (int j = 0; j < clusters.get(i).size(); j++) {
                        cpoints_aux.add(clusters.get(i).get(j));
                    }

                    sampleinst.add(cpoints_aux);
                }
            }
        } else if (sampletype == SampleTypePLSP.RANDOM) {
            for (int i = 0; i < clusters.size(); i++) {
                //defining the number of control points on the cluster
                int samplesize = (int) (clusters.get(i).size() * perc) - 1;
                samplesize = (samplesize > 2) ? samplesize : 2;

                HashSet<Integer> sample = new HashSet<Integer>();
                Random random = new Random(System.currentTimeMillis());

                while (sample.size() < samplesize) {
                    int index = (int) (random.nextFloat() * (clusters.get(i).size()));
                    if (index < matrix.getRowCount()) {
                        sample.add(clusters.get(i).get(index));
                    }
                }

                ArrayList<Integer> cpoints_aux = new ArrayList<Integer>();
                Iterator<Integer> it = sample.iterator();
                while (it.hasNext()) {
                    int index = it.next();
                    cpoints_aux.add(index);
                }

                sampleinst.add(cpoints_aux);
            }
        }

        //creating the matrix with all points
        AbstractMatrix samplematrix = MatrixFactory.getInstance(matrix.getClass());
        for (int i = 0; i < sampleinst.size(); i++) {
            for (int j = 0; j < sampleinst.get(i).size(); j++) {
                samplematrix.addRow(matrix.getRow(sampleinst.get(i).get(j)));
            }
        }

        return samplematrix;
    }

    public ArrayList<ArrayList<Integer>> getSampleInstances() {
        return sampleinst;
    }

    private ArrayList<ArrayList<Integer>> sampleinst;
    private SampleTypePLSP sampletype;
}
