/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plsp.technique;

import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import plsp.technique.PLSPSampling.SampleTypePLSP;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author paulovich
 */
@VisComponent(hierarchy = "Projection.Technique.PLSP",
name = "Sampling",
description = "Get a sampling of a given matrix.")
public class PLSPSamplingComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (matrix != null) {
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            sampling = new PLSPSampling(sampletype);
            samplematrix = sampling.execute(clusters, matrix, diss);
        } else {
            throw new IOException("A points matrix should be provided.");
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix, 
            @Param(name = "clusters") ArrayList<ArrayList<Integer>> clusters) {
        this.matrix = matrix;
        this.clusters = clusters;
    }

    public AbstractMatrix outputSampleMatrix() {
        return samplematrix;
    }

    public ArrayList<ArrayList<Integer>> outputSampleInstances() {
        if (sampling == null) {
            return null;
        }

        return sampling.getSampleInstances();
    }

    @Override
    public void reset() {
        matrix = null;
        samplematrix = null;
        clusters = null;
        sampling = null;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    public void setSampleSize(int samplesize) {
        this.samplesize = samplesize;
    }

    public int getSampleSize() {
        return this.samplesize;
    }

    public SampleTypePLSP getSampleType() {
        return sampletype;
    }

    public void setSampleType(SampleTypePLSP sampletype) {
        this.sampletype = sampletype;
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }
    
    public static final long serialVersionUID = 1L;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private int samplesize = 0;
    private SampleTypePLSP sampletype = SampleTypePLSP.RANDOM;
    private transient AbstractMatrix matrix;
    private transient ArrayList<ArrayList<Integer>> clusters;
    private transient AbstractMatrix samplematrix;
    private transient PLSPSampling sampling;
}
