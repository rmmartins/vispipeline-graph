/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package matrix.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.reader.MatrixReaderComp;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author DaniloEler
 */
@VisComponent(hierarchy = "Points.Util",
name = "Points Difference (Subtractor)",
description = "It subtracts a sample of points from the original matrix of points.")
public class DifferenceComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (matrix != null && samplematrix != null) {
            diffMatrix = new DenseMatrix();
            ArrayList<String> attrs = new ArrayList<String>();
            attrs.addAll(matrix.getAttributes());
            diffMatrix.setAttributes(attrs);
            ArrayList<Integer> matrixIds = matrix.getIds();
            ArrayList<Integer> samplematrixIds = samplematrix.getIds();
            for (int i = 0; i < matrix.getRowCount(); i++) {
                Integer id = matrixIds.get(i);
                if (!samplematrixIds.contains(id)) {
                    diffMatrix.addRow(matrix.getRow(i), matrix.getLabel(i));
                }
            }
        } else {
            throw new IOException("An original and sample matrix should be provided.");
        }
    }

    public void input(@Param(name = "original matrix") AbstractMatrix matrix,
            @Param(name = "sample matrix") AbstractMatrix samplematrix) {
        this.matrix = matrix;
        this.samplematrix = samplematrix;
    }

    public AbstractMatrix output(){
        return this.diffMatrix;
    }

    @Override
    public void reset() {
        matrix = null;
        samplematrix = null;
        diffMatrix = null;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    public static void main(String[] args) {
        try {
            String filename = "C:\\Users\\DaniloEler\\Documents\\Fibras\\brain1_scan1_fiber_track_mni.data";
            MatrixReaderComp reader = new MatrixReaderComp();
            reader.setFilename(filename);
            reader.execute();

            filename = "C:\\Users\\DaniloEler\\Documents\\Fibras\\brain1_scan1_fiber_track_mni_class.data";
            MatrixReaderComp readerSample = new MatrixReaderComp();
            readerSample.setFilename(filename);
            readerSample.execute();

            DifferenceComp diff = new DifferenceComp();
            diff.input(reader.output(), readerSample.output());
            diff.execute();

            System.out.println("# of original points: " + reader.output().getRowCount());
            System.out.println("# of sample   points: " + readerSample.output().getRowCount());
            System.out.println("# of diff     points: " + diff.output().getRowCount());
            System.out.println("# of attributs: " + diff.output().getAttributes().size());
            System.out.println("# of labels: " + diff.output().getLabels().size());


        } catch (IOException ex) {
            Logger.getLogger(DifferenceComp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static final long serialVersionUID = 1L;
    private transient AbstractMatrix matrix;
    private transient AbstractMatrix samplematrix;
    private transient AbstractMatrix diffMatrix;
}
