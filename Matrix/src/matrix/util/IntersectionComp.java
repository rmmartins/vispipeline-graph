/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package matrix.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.reader.MatrixReaderComp;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author DaniloEler
 */
@VisComponent(hierarchy = "Points.Util",
name = "Points Intersection",
description = "It returns a matrix with the intersection between matrix A and B. Only the attributes of matrix A will be considered.")
public class IntersectionComp implements AbstractComponent {
    @Override
    public void execute() throws IOException {
        if (matrixA != null && matrixB != null) {
            intersecMatrix = new DenseMatrix();
            ArrayList<String> attrs = new ArrayList<String>();
            attrs.addAll(matrixA.getAttributes());
            intersecMatrix.setAttributes(attrs);
            ArrayList<Integer> matrixAIds = matrixA.getIds();
            ArrayList<Integer> matrixBIds = matrixB.getIds();
            for (int i = 0; i < matrixA.getRowCount(); i++) {
                Integer id = matrixAIds.get(i);
                if (matrixBIds.contains(id)) {
                    intersecMatrix.addRow(matrixA.getRow(i), matrixA.getLabel(i));
                }
            } 
        } else {
            throw new IOException("Matrix A and B should be provided.");
        }
    }

    public void input(@Param(name = "matrix A") AbstractMatrix matrix,
            @Param(name = "matrix B") AbstractMatrix samplematrix) {
        this.matrixA = matrix;
        this.matrixB = samplematrix;
    }

    public AbstractMatrix output(){
        return this.intersecMatrix;
    }

    @Override
    public void reset() {
        matrixA = null;
        matrixB = null;
        intersecMatrix = null;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    public static void main(String[] args) {
        try {
            String filename = "C:\\Users\\DaniloEler\\Documents\\Fibras\\brain1_scan1_fiber_track_mni.data";
            MatrixReaderComp reader = new MatrixReaderComp();
            reader.setFilename(filename);
            reader.execute();

            filename = "C:\\Users\\DaniloEler\\Documents\\Fibras\\brain1_scan1_fiber_track_mni_class.data";
            MatrixReaderComp readerSample = new MatrixReaderComp();
            readerSample.setFilename(filename);
            readerSample.execute();

            IntersectionComp intersection = new IntersectionComp();
            intersection.input(reader.output(), readerSample.output());
            intersection.execute();

            System.out.println("# of matrix A           : " + reader.output().getRowCount());
            System.out.println("# of matrix B           : " + readerSample.output().getRowCount());
            System.out.println("# of intersection matrix: " + intersection.output().getRowCount());
            System.out.println("# of attributs          : " + intersection.output().getAttributes().size());
            System.out.println("# of labels             : " + intersection.output().getLabels().size());
        } catch (IOException ex) {
            Logger.getLogger(IntersectionComp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static final long serialVersionUID = 1L;
    private transient AbstractMatrix matrixA;
    private transient AbstractMatrix matrixB;
    private transient AbstractMatrix intersecMatrix;
}
