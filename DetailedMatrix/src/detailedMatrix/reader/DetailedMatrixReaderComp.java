/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package detailedMatrix.reader;

import detailedMatrix.*;
import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.reader.MatrixReaderComp;
import matrix.reader.MatrixReaderParamView;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Points.Input",
name = "Detailed Points matrix reader",
description = "Read a Detailed Points Matrix from a file.")
public class DetailedMatrixReaderComp extends MatrixReaderComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
//        matrix = new DetailedDenseMatrix();
//        matrix.load(filename);
        matrix = DetailedMatrixFactory.getInstance(filename);
    }

    @Override
    public AbstractMatrix output() {
        return matrix;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new MatrixReaderParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        matrix = null;
    }

    /**
     * @return the filename
     */
    @Override
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    @Override
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public static final long serialVersionUID = 1L;
    private String filename = "";
    private transient AbstractMatrix matrix;
    private transient MatrixReaderParamView paramview;
}
