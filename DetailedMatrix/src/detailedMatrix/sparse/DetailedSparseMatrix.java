package detailedMatrix.sparse;

import java.io.IOException;
import matrix.AbstractVector;
import detailedMatrix.DetailedMatrix;
import matrix.sparse.SparseMatrix;
import matrix.sparse.SparseVector;

public class DetailedSparseMatrix extends SparseMatrix implements DetailedMatrix {
    
    @Override
    public void load(String filename) throws IOException {
        super.load(filename);
        source = filename;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DetailedSparseMatrix clone = new DetailedSparseMatrix();
        clone.dimensions = this.dimensions;

        for (String lab : this.labels) {
            clone.labels.add(lab);
        }

        for (String attr : this.attributes) {
            clone.attributes.add(attr);
        }

        for (AbstractVector v : this.rows) {
            clone.rows.add((SparseVector) v.clone());
        }
        clone.source = this.source;
        return clone;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    private String source;

}
