package detailedMatrix.dense;

import java.io.IOException;
import matrix.AbstractVector;
import detailedMatrix.DetailedMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;

public class DetailedDenseMatrix extends DenseMatrix implements DetailedMatrix {
    
    @Override
    public void load(String filename) throws IOException {
        super.load(filename);
        source = filename;
        int begin = source.lastIndexOf("\\");
        int end = source.lastIndexOf(".");
        if (begin == -1) begin = 0;
        else if ((begin+1)<=end)
                begin = begin+1;
        if (end == -1) end = source.length();
        this.name = source.substring(begin, end);

    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DetailedDenseMatrix clone = new DetailedDenseMatrix();
        clone.dimensions = this.dimensions;

        for (String lab : this.labels) {
            clone.labels.add(lab);
        }

        for (String attr : this.attributes) {
            clone.attributes.add(attr);
        }

        for (AbstractVector v : this.rows) {
            clone.rows.add((DenseVector) v.clone());
        }
        clone.source = this.source;
        clone.name = this.name;
        return clone;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String source = "";
    private String name = "";

}
