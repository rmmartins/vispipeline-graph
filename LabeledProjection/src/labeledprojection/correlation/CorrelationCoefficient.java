package labeledprojection.correlation;

import distance.DetailedDistanceMatrix;
import distance.DistanceMatrix;
import java.io.IOException;
import java.util.ArrayList;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;

/**
 *
 * @author Jose Gustavo
 */
public class CorrelationCoefficient {

    public String execute(DistanceMatrix dmat, ArrayList<ProjectionModel> models, ArrayList<DetailedDistanceMatrix> projectionDmats) throws IOException {

//        if (dataFileName.trim().length() > 0) {
//            if (dataFileType == 0) {
//                try {
//                    AbstractMatrix matrix = MatrixFactory.getInstance(dataFileName);
//                    originalDmat = new DistanceMatrix(matrix, diss);
//                    matrix = null;
//                } catch (IOException ex) {
//                    Logger.getLogger(CorrelationCoefficient.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            } else if (dataFileType == 1) {
//                try {
//                    originalDmat = new DistanceMatrix(dataFileName);
//                } catch (IOException ex) {
//                    Logger.getLogger(CorrelationCoefficient.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        } else {
//            System.out.println("Data filename not informed.");
//        }

//        if ((models == null || models.isEmpty()) &&
//            (projectionMatrices == null || projectionMatrices.isEmpty()) &&
//            (projectionDmats == null || projectionDmats.isEmpty())) {
//            System.out.println("No input informed.");
//            return null;
//        }

//        //Creating dmats with the matrix informed...
//        if (projectionMatrices != null && !projectionMatrices.isEmpty()) {
//            projectionDmats = new ArrayList<DistanceMatrix>();
//            for (int i=0;i<projectionMatrices.size();i++)
//                projectionDmats.add(new DistanceMatrix(projectionMatrices.get(i),diss));
//        }

        String result = "";
        ArrayList<float[]> distanceVectorsO = new ArrayList<float[]>();
        ArrayList<float[]> distanceVectorsP = new ArrayList<float[]>();

        for (int i=0;i<dmat.getElementCount();i++) {
            distanceVectorsO.add(new float[dmat.getElementCount()]);
            for (int k=0;k<dmat.getElementCount();k++) {
                distanceVectorsO.get(i)[k] = dmat.getDistance(i,k);
            }
        }

        //Calculating correlation coefficients for models provided...
        if (models != null && !models.isEmpty()) {
            for (int i=0;i<models.size();i++) {
                ProjectionModel model = models.get(i);
                distanceVectorsP = new ArrayList<float[]>();
                for (int j=0;j<model.getInstances().size();j++) {
                    distanceVectorsP.add(new float[model.getInstances().size()]);
                    float[] c1 = new float[2];
                    c1[0] = ((ProjectionInstance)model.getInstances().get(j)).getX();
                    c1[1] = ((ProjectionInstance)model.getInstances().get(j)).getY();
                    for (int k=0;k<model.getInstances().size();k++) {
                        float[] c2 = new float[2];
                        c2[0] = ((ProjectionInstance)model.getInstances().get(k)).getX();
                        c2[1] = ((ProjectionInstance)model.getInstances().get(k)).getY();
                        float dist = (float)Math.sqrt(Math.pow(c1[0]-c2[0],2)+Math.pow(c1[1]-c2[1],2));
                        distanceVectorsP.get(j)[k] = dist;
                    }
                }
                float correlation = getCorrelationCoefficient(distanceVectorsO,distanceVectorsP);
                result += model.toString()+" correlation coefficient: "+correlation+"\r\n";
            }
        }

        //Calculating correlation coefficients from datasets (points matrix or distance matrix) provided...
        if (projectionDmats != null && !projectionDmats.isEmpty()) {
            for (int i=0;i<projectionDmats.size();i++) {
                distanceVectorsP = new ArrayList<float[]>();
                for (int j=0;j<projectionDmats.get(i).getElementCount();j++) {
                    distanceVectorsP.add(new float[projectionDmats.get(i).getElementCount()]);
                    for (int k=0;k<projectionDmats.get(i).getElementCount();k++) {
                        distanceVectorsP.get(j)[k] = projectionDmats.get(i).getDistance(j,k);
                    }
                }
                float correlation = getCorrelationCoefficient(distanceVectorsO,distanceVectorsP);
                if (projectionDmats.get(i) instanceof DetailedDistanceMatrix)
                    result += ((DetailedDistanceMatrix)projectionDmats.get(i)).getName()+" correlation coefficient: "+correlation+"\r\n";
                else
                    result += "Distance Matrix "+i+" correlation coefficient: "+correlation+"\r\n";
            }
        }
        return result;

    }

//    public String execute(DistanceMatrix dmat, ProjectionModel model) {
//
//        originalDmat = dmat;
//
//        String result = "";
//        ArrayList<float[]> distanceVectorsO = new ArrayList<float[]>();
//        ArrayList<float[]> distanceVectorsP = new ArrayList<float[]>();
//
//        for (int i=0;i<originalDmat.getElementCount();i++) {
//            distanceVectorsO.add(new float[originalDmat.getElementCount()]);
//            for (int k=0;k<originalDmat.getElementCount();k++) {
//                distanceVectorsO.get(i)[k] = originalDmat.getDistance(i,k);
//            }
//        }
//
//        //Calculating correlation coefficients for models provided...
//        distanceVectorsP = new ArrayList<float[]>();
//        for (int j=0;j<model.getInstances().size();j++) {
//            distanceVectorsP.add(new float[model.getInstances().size()]);
//            float[] c1 = new float[2];
//            c1[0] = ((ProjectionInstance)model.getInstances().get(j)).getX();
//            c1[1] = ((ProjectionInstance)model.getInstances().get(j)).getY();
//            for (int k=0;k<model.getInstances().size();k++) {
//                float[] c2 = new float[2];
//                c2[0] = ((ProjectionInstance)model.getInstances().get(k)).getX();
//                c2[1] = ((ProjectionInstance)model.getInstances().get(k)).getY();
//                float dist = (float)Math.sqrt(Math.pow(c1[0]-c2[0],2)+Math.pow(c1[1]-c2[1],2));
//                distanceVectorsP.get(j)[k] = dist;
//            }
//        }
//        float correlation = getCorrelationCoefficient(distanceVectorsO,distanceVectorsP);
//        result += model.toString()+" correlation coefficient: "+correlation+"\r\n";
//
//        return result;
//
//    }

    private float getMean(float[] vector) {
        if (vector.length < 1) return Float.NaN;
        float sum = 0.0f;
        for (int i=0;i<vector.length;i++)
            sum += vector[i];
        return sum/vector.length;
    }

    private float getStdDev(float[] vector) {
        float mean = getMean(vector);
        float sum = 0;
        for (int i=0;i<vector.length;i++)
            sum += Math.pow((vector[i]-mean),2);
        return (float)Math.sqrt(sum/(vector.length-1));
    }

    private float[] product(float[] vec1, float[] vec2) {
        float[] ret = new float[vec1.length];
        for (int i=0;i<vec1.length;i++)
            ret[i] = vec1[i]*vec2[i];
        return ret;
    }

    private float getCorrelationCoefficient(ArrayList<float[]> distanceVectorsO, ArrayList<float[]> distanceVectorsP) {

        if (distanceVectorsO.size() != distanceVectorsP.size()) return Float.NaN;

        float meanProduct, meanO, meanP, stDevO, stDevP;
        float correlation = 0;

        for (int i=0;i<distanceVectorsO.size();i++) {
            meanProduct = getMean(product(distanceVectorsO.get(i),distanceVectorsP.get(i)));
            meanO = getMean(distanceVectorsO.get(i));
            meanP = getMean(distanceVectorsP.get(i));
            stDevO = getStdDev(distanceVectorsO.get(i));
            stDevP = getStdDev(distanceVectorsP.get(i));
            correlation += (meanProduct-(meanO*meanP))/(stDevO*stDevP);
        }
        return correlation/distanceVectorsO.size();

    }

}
