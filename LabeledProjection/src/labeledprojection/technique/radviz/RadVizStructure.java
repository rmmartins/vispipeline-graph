package labeledprojection.technique.radviz;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import labeledprojection.view.forms.DragDropListItem;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.dense.DenseVector;

/**
 *
 * @author Jose Gustavo
 */
public class RadVizStructure {

    private AbstractMatrix instances;
    private ArrayList<AbstractVector> anchors;
    private ArrayList<DragDropListItem> colorAnchors;
    private Dimension size;

    public RadVizStructure(AbstractMatrix instances, ArrayList<AbstractVector> anchors, int[] orderAnchors, boolean update) {
        this.instances = instances;
        this.anchors = anchors;
        this.colorAnchors = new ArrayList<DragDropListItem>();
        if (anchors != null) {
            for (int i=0;i<anchors.size();i++)
                colorAnchors.add(new DragDropListItem(anchors.get(i).getId(),Color.BLACK));
        }
    }

    public RadVizStructure(AbstractMatrix instances, int[] orderAnchors) {
        this.instances = instances;
        long init = System.currentTimeMillis();
        normalize(this.instances);
        ArrayList<DragDropListItem> listColorAnchors = new ArrayList<DragDropListItem>();
        Random colorGen = new Random();
        for (int i=0;i<orderAnchors.length;i++)
            listColorAnchors.add(new DragDropListItem(orderAnchors[i],new Color(colorGen.nextInt(256),colorGen.nextInt(256),colorGen.nextInt(256))));
        updateAnchors(listColorAnchors);
        long end = System.currentTimeMillis();
        long diff = end - init;
        System.out.println("Time spent (RadViz Structure creation) -> " + (diff/1000.0f) + " seconds");
        setIdClass(0.0f);
    }

    public Dimension getDimension() {
        return size;
    }

    public void setDimension(Dimension d) {
        if (d != null)
            if (d.width > d.height)
                size = new Dimension(d.width,d.width);
            else
                size = new Dimension(d.height,d.height);
    }

    public void updateAnchors(ArrayList<DragDropListItem> orderAnchors) {

        this.colorAnchors = orderAnchors;
        if (instances == null) return;
        if (anchors == null)
            anchors = new ArrayList<AbstractVector>();
        double angleSlope = (2*Math.PI)/instances.getDimensions();
        for (int i=0;i<instances.getDimensions();i++) {
            float[] v = new float[2];
            v[0] = (float) Math.cos(orderAnchors.get(i).index*angleSlope);
            v[1] = (float) Math.sin(orderAnchors.get(i).index*angleSlope);
            AbstractVector anchor = new DenseVector(v);
            anchor.setId(orderAnchors.get(i).index);
            if (anchors.size() > i)
                anchors.set(i, anchor);
            else {
                anchors.add(anchor);
            }
        }

    }

    public float[][] updateRadVizValues() {

        if (instances == null) return null;
        float[][] projection = new float[instances.getRowCount()][];

        long init = System.currentTimeMillis();
        //Calculating RadViz positions...
        for (int i=0;i<instances.getRowCount();i++) {
            AbstractVector instance = instances.getRow(i);
            float sumInstance = 0;
            AbstractVector sumAnchors = new DenseVector(new float[anchors.get(0).size()]);
            for (int j=0;j<instance.size();j++) {
                //Calculating the sum of the instance values...
                sumInstance += instance.getValue(j);
                //Calculating the sum of products between instance values and correspondent anchor vector...
                sumVector(sumAnchors,multiVectorScalar(anchors.get(j),instance.getValue(j)));
            }
            AbstractVector u = multiVectorScalar(sumAnchors,1/sumInstance);
            projection[i] = u.toArray();
        }
        long end = System.currentTimeMillis();
        long diff = end - init;
        System.out.println("Time spent (RadViz Objects mapping) -> " + (diff/1000.0f) + " seconds");

        return projection;
    }

    public void setIdClass(ArrayList<Float> idClass) {
        if (idClass.size() != anchors.size()) return;
        for (int i=0;i<anchors.size();i++)
            anchors.get(i).setKlass(idClass.get(i));
    }

    public void setIdClass(float klass) {
        for (int i=0;i<anchors.size();i++)
            anchors.get(i).setKlass(klass);
    }
    
    public void normalize(AbstractMatrix matrix) {

        if (matrix == null) return;

        float[] min = new float[matrix.getDimensions()];
        float[] max = new float[matrix.getDimensions()];
        Arrays.fill(min,Float.MAX_VALUE);
        Arrays.fill(max,Float.MIN_VALUE);

        for (int i=0;i<matrix.getRowCount();i++) {
            AbstractVector instance = matrix.getRow(i);
            for (int j=0;j<instance.size();j++) {
                if (instance.getValue(j) < min[j]) min[j] = instance.getValue(j);
                if (instance.getValue(j) > max[j]) max[j] = instance.getValue(j);
            }
        }

        for (int i=0;i<matrix.getRowCount();i++) {
            AbstractVector instance = matrix.getRow(i);
            for (int j=0;j<instance.size();j++) {
                float newValue = instance.getValue(j);
                if ((max[j]-min[j]) != 0)
                    newValue = (instance.getValue(j) - min[j])/(max[j]-min[j]);
                instance.setValue(j,newValue);
            }
        }

    }

    public AbstractVector multiVectorScalar(AbstractVector v, float x) {
        float[] vec = v.toArray();
        float[] ret = new float[vec.length];
        for (int i=0;i<ret.length;i++)
            ret[i] = vec[i]*x;
        AbstractVector retV = new DenseVector(ret);
        return retV;
    }

    public void sumVector(AbstractVector v1,AbstractVector v2) {
        for (int i=0;i<v1.size();i++) {
            v1.setValue(i,v1.getValue(i)+v2.getValue(i));
        }
    }

    public AbstractMatrix getInstances() {
        return instances;
    }

    public ArrayList<AbstractVector> getAnchors() {
        return anchors;
    }

    public ArrayList<DragDropListItem> getColorAnchors() {
        return colorAnchors;
    }

}
