/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * LSPProjection2DParamView.java
 *
 * Created on 29/05/2009, 11:29:24
 */
package labeledprojection.technique.radviz;

import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class RadVizProjectionParamView extends AbstractParametersView {

    public RadVizProjectionParamView(RadVizProjectionComp comp) {
        initComponents();
        this.comp = comp;
        reset();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        anchorOrderingLabel = new javax.swing.JLabel();
        anchorOrderingComboBox = new javax.swing.JComboBox();

        setBorder(javax.swing.BorderFactory.createTitledBorder("RadViz Parameters"));
        setLayout(new java.awt.GridBagLayout());

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jPanel4.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel2.add(jPanel4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(jPanel2, gridBagConstraints);

        anchorOrderingLabel.setText("Anchor Ordering : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(anchorOrderingLabel, gridBagConstraints);

        anchorOrderingComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Correlation", "Covariance" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(anchorOrderingComboBox, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void reset() {
        anchorOrderingComboBox.setSelectedIndex(comp.getAnchorOrdering());
    }

    @Override
    public void finished() {
        comp.setAnchorOrdering(anchorOrderingComboBox.getSelectedIndex());
    }

    private RadVizProjectionComp comp;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox anchorOrderingComboBox;
    private javax.swing.JLabel anchorOrderingLabel;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    // End of variables declaration//GEN-END:variables
}
