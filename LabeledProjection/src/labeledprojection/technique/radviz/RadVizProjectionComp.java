/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.technique.radviz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 *
 */
@VisComponent(hierarchy = "Projection.Technique",
name = "Radviz Projection",
description = "Project points from a projection technique, using radviz.",
howtocite = "Hoffman P., Grinstein G., Pinkney D.: Dimensional Anchors: "
        + "A Graphic Primitive for Multidimensional Multivariate Information Visualizations."
        + " In Workshop on New Paradigms in Inform. Vis. and Manip. in Conjunction with ACM CIKM"
        + " (Kansas City, MO, USA, 1999), pp. 9–16.")
public class RadVizProjectionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {

        int[] order = null;
        if (inputProjection != null) {
            switch (anchorOrdering) {
                case 1: //Ordering by correlation...
                    order = getOrder(getCorrelationMatrix(inputProjection));
                    break;
                case 2: //Ordering by covariance...
                    order = getOrder(getCovarianceMatrix(inputProjection));
                    break;
            }
            if (order == null) {
                order = new int[inputProjection.getDimensions()];
                for (int i=0;i<inputProjection.getDimensions();i++) order[i] = i;
            }
            radvizStructure = new RadVizStructure(inputProjection, order);
        } else
            throw new IOException("A distance matrix or a points matrix should be provided.");
        
    }
    
    public static void main(String[] a) {
        
        AbstractMatrix projection = new DenseMatrix();
        float[] values = new float[3];
        values[0] = 4; values[1] = 2; values[2] = 0.6f;
        projection.addRow(new DenseVector(values));
        values = new float[3];
        values[0] = 4.2f; values[1] = 2.1f; values[2] = 0.59f;
        projection.addRow(new DenseVector(values));
        values = new float[3];
        values[0] = 3.9f; values[1] = 2; values[2] = 0.58f;
        projection.addRow(new DenseVector(values));
        values = new float[3];
        values[0] = 4.3f; values[1] = 2.1f; values[2] = 0.62f;
        projection.addRow(new DenseVector(values));
        values = new float[3];
        values[0] = 4.1f; values[1] = 2.2f; values[2] = 0.63f;
        projection.addRow(new DenseVector(values));
        
//        AbstractMatrix projection = new DenseMatrix();
//        float[] values = new float[4];
//        values[0] = 1; values[1] = 9; values[2] = 6; values[3] = 3;
//        projection.addRow(new DenseVector(values));
//        values = new float[4];
//        values[0] = 2; values[1] = 6; values[2] = 5; values[3] = 2;
//        projection.addRow(new DenseVector(values));
//        values = new float[4];
//        values[0] = 3; values[1] = 4; values[2] = 3; values[3] = 2;
//        projection.addRow(new DenseVector(values));
//        values = new float[4];
//        values[0] = 4; values[1] = 3; values[2] = 1; values[3] = 1;
//        projection.addRow(new DenseVector(values));
//        values = new float[4];
//        values[0] = 5; values[1] = 3; values[2] = 4; values[3] = 1;
//        projection.addRow(new DenseVector(values));
//        values = new float[4];
//        values[0] = 6; values[1] = 5; values[2] = 3; values[3] = 3;
//        projection.addRow(new DenseVector(values));
//        values = new float[4];
//        values[0] = 7; values[1] = 8; values[2] = 6; values[3] = 3;
//        projection.addRow(new DenseVector(values));
//        values = new float[4];
//        values[0] = 8; values[1] = 2; values[2] = 2; values[3] = 1;
//        projection.addRow(new DenseVector(values));
//        values = new float[4];
//        values[0] = 9; values[1] = 7; values[2] = 4; values[3] = 2;
//        projection.addRow(new DenseVector(values));
//        values = new float[4];
//        values[0] = 10; values[1] = 4; values[2] = 2; values[3] = 2;
//        projection.addRow(new DenseVector(values));
        
//        getOrder(getCorrelationMatrix(projection));
    }

    public float[][] getCovarianceMatrix(AbstractMatrix projection) {
        
        //Calculating average of each feature...
        float[] mean = new float[projection.getDimensions()];
        
        for (int i=0;i<projection.getDimensions();i++) {
            for (int j=0;j<projection.getRowCount();j++) {
                mean[i] += projection.getRow(j).getValue(i);
            }
            mean[i] /= projection.getRowCount();
        }
        
        float[][] covarianceMatrix = new float[projection.getDimensions()][projection.getDimensions()];
        
        for (int colA=0;colA<projection.getDimensions();colA++) {
            for (int colB=0;colB<projection.getDimensions();colB++) {
//                if (colA == colB) {
//                    covarianceMatrix[colA][colB] = Float.MIN_VALUE;
//                }else {
                    for (int row=0;row<projection.getRowCount();row++) {
                        covarianceMatrix[colA][colB] += ((projection.getRow(row).getValue(colA) - mean[colA])*(projection.getRow(row).getValue(colB) - mean[colB]));
                    }
                    covarianceMatrix[colA][colB] /= (float)(projection.getRowCount() - 1);
//                }
            }
        }
        
        return covarianceMatrix;
        
    }
    
    public float[][] getCorrelationMatrix(AbstractMatrix projection) {
        
        float[][] correlationMatrix = getCovarianceMatrix(projection);
        
        for (int i=0;i<correlationMatrix.length;i++) {
            for (int j=0;j<i;j++) {
                correlationMatrix[i][j] = correlationMatrix[j][i] = (float)(correlationMatrix[i][j]/(Math.sqrt(correlationMatrix[i][i])*Math.sqrt(correlationMatrix[j][j])));
            }
        }
        for (int i=0;i<correlationMatrix.length;i++) correlationMatrix[i][i] = 1;
        
        return correlationMatrix;
        
    }
    
    public int[] getOrder(float[][] matrix) {
        
        ArrayList<CovCorValue> tempOrder = new ArrayList<CovCorValue>();
        int kk = 0, tam = matrix.length;
        
        CovCorValue maxCov = new CovCorValue();
        while (!isMatrixZero(matrix)) {
            for (int i=0;i<matrix.length;i++) {
                for (int j=0;j<i;j++) {
                    if (matrix[i][j] > maxCov.covcorValue) {
                        maxCov.covcorValue = matrix[i][j];
                        maxCov.i = i;
                        maxCov.j = j;
                    }
                }
            }
            tempOrder.add(maxCov);
            kk++;
            matrix[maxCov.i][maxCov.j] = matrix[maxCov.j][maxCov.i] = -Float.MAX_VALUE;
            maxCov = new CovCorValue();
        }
        
        ArrayList<Integer> finalOrder = new ArrayList<Integer>();
        
        if (!tempOrder.isEmpty()) {
            finalOrder.add(tempOrder.get(0).i);
            finalOrder.add(tempOrder.get(0).j);
            tempOrder.remove(0);
            while (finalOrder.size() < tam && !tempOrder.isEmpty()) {
                int i=0;
                while (i<tempOrder.size()) {
                    if (!finalOrder.contains(tempOrder.get(i).j) && (tempOrder.get(i).i == finalOrder.get(0))) {
                        finalOrder.add(0,tempOrder.get(i).j);
                        break;
                    }
                    if (!finalOrder.contains(tempOrder.get(i).i) && (tempOrder.get(i).j == finalOrder.get(0))) {
                        finalOrder.add(0,tempOrder.get(i).i);
                        break;
                    }
                    
                    if (!finalOrder.contains(tempOrder.get(i).j) && (tempOrder.get(i).i == finalOrder.get(finalOrder.size()-1))) {
                        finalOrder.add(tempOrder.get(i).j);
                        break;
                    }
                    if (!finalOrder.contains(tempOrder.get(i).i) && (tempOrder.get(i).j == finalOrder.get(finalOrder.size()-1))) {
                        finalOrder.add(tempOrder.get(i).i);
                        break;
                    }
                    i++;
                }
                tempOrder.remove(i);
            }
        }
        
        int[] ret = null;
        if (finalOrder != null && !finalOrder.isEmpty()) {
            ret = new int[finalOrder.size()];
            for (int i=0;i<finalOrder.size();i++) ret[i] = finalOrder.get(i);
        }
        
        return ret;
        
//        for (int i=0;i<tempOrder.size();i++) {
//            if (!finalOrder.contains(tempOrder.get(i).i))
//                finalOrder.add(tempOrder.get(i).i);
//            if (!finalOrder.contains(tempOrder.get(i).j))
//                finalOrder.add(tempOrder.get(i).j);
//        }
        
    }
    
    public boolean isMatrixZero(float[][] covarianceMatrix) {
        for (int i=0;i<covarianceMatrix.length;i++)
            for (int j=0;j<covarianceMatrix[i].length;j++)
                if (i != j && covarianceMatrix[i][j] != -Float.MAX_VALUE)
                    return false;
        return true;
    }

    class CovCorValue {
        int i,j;
        float covcorValue = -Float.MAX_VALUE;
    }
    
    public void input(@Param(name = "projection points") AbstractMatrix iprojection) {
        inputProjection = iprojection;
    }

    public RadVizStructure output() {
        return radvizStructure;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new RadVizProjectionParamView(this);
        }
        return paramview;
    }

    public int getAnchorOrdering() {
        return anchorOrdering;
    }

    public void setAnchorOrdering(int selectedIndex) {
        anchorOrdering = selectedIndex;
    }
    
    @Override
    public void reset() {
        inputProjection = null;
    }

//    public void normalize(AbstractMatrix matrix) {
//
//        float[] min = new float[matrix.getDimensions()];
//        float[] max = new float[matrix.getDimensions()];
//        Arrays.fill(min,Float.MAX_VALUE);
//        Arrays.fill(max,-Float.MAX_VALUE);
//
//        for (int i=0;i<matrix.getRowCount();i++) {
//            AbstractVector instance = matrix.getRow(i);
//            for (int j=0;j<instance.size();j++) {
//                if (instance.getValue(j) < min[j]) min[j] = instance.getValue(j);
//                if (instance.getValue(j) > max[j]) max[j] = instance.getValue(j);
//            }
//        }
//
//        for (int i=0;i<matrix.getRowCount();i++) {
//            AbstractVector instance = matrix.getRow(i);
//            for (int j=0;j<instance.size();j++) {
//                float newValue = (instance.getValue(j) - min[j])/(max[j]-min[j]);
//                instance.setValue(j,newValue);
//            }
//        }
//
//    }
//
//    public AbstractVector multiVectorScalar(AbstractVector v, float x) {
//        float[] vec = v.getValues();
//        float[] ret = new float[vec.length];
//        for (int i=0;i<ret.length;i++)
//            ret[i] = vec[i]*x;
//        AbstractVector retV = new DenseVector(ret);
//        return retV;
//    }
//
//    public void sumVector(AbstractVector v1,AbstractVector v2) {
//        for (int i=0;i<v1.size();i++) {
//            v1.setValue(i,v1.getValue(i)+v2.getValue(i));
//        }
//    }
//
//    public AbstractMatrix createRadVizProjection(AbstractMatrix instances, int[] orderAnchors, ArrayList<Float> idClass) {
//
//        double angleSlope = (2*Math.PI)/instances.getDimensions();
//        double angle = 0.0;
//        ArrayList<String> attributes = new ArrayList<String>();
//        attributes.add("x");
//        attributes.add("y");
//
//        //Normalizing instances...
//        normalize(instances);
//
//        //Creating anchors...
//        AbstractMatrix anchors = new DenseMatrix();
//        anchors.setAttributes(attributes);
//        for (int i=0;i<instances.getDimensions();i++) {
//            float[] v = new float[2];
//            v[0] = (float) Math.cos(angle);
//            v[1] = (float) Math.sin(angle);
//            AbstractVector anchor = new DenseVector(v);
//            if (idClass != null && !idClass.isEmpty() && idClass.size() > i)
//                anchor.setKlass(idClass.get(i));
//            else
//                anchor.setKlass(0.0f);
//            anchors.addRow(anchor);
//            angle += angleSlope;
//        }
//
//        AbstractMatrix RadVizProjection = new DenseMatrix();
//        RadVizProjection.setAttributes(attributes);
//        ArrayList<String> labels = new ArrayList<String>();
//
//        //Calculating RadViz positions...
//        for (int i=0;i<instances.getRowCount();i++) {
//            AbstractVector instance = instances.getRow(i);
//            float sumInstance = 0;
//            AbstractVector sumAnchors = new DenseVector(new float[anchors.getDimensions()]);
//            for (int j=0;j<instance.size();j++) {
//                //Calculating the sum of the instance values...
//                sumInstance += instance.getValue(j);
//                //Calculating the sum of products between instance values and correspondent anchor vector...
//                sumVector(sumAnchors,multiVectorScalar(anchors.getRow(orderAnchors[j]),instance.getValue(j)));
//            }
//            AbstractVector u = multiVectorScalar(sumAnchors,1/sumInstance);
//            u.setId(instances.getRow(i).getId());
//            u.setKlass(instances.getRow(i).getKlass());
//
//            RadVizProjection.addRow(u);
//            labels.add(instances.getLabel(i));
//        }
//
//        //Adding the anchors to the projection, just to show the axes on the visualization plane
//        for (int i=0;i<anchors.getRowCount();i++) {
//            RadVizProjection.addRow(anchors.getRow(i));
//            labels.add("Axis_"+i);
//        }
//
//        RadVizProjection.setLabels(labels);
//
//        return RadVizProjection;
//    }

    public void setNumberPC(int n) {
        numberPC = n;
    }

    public int getNumberPC() {
        return numberPC;
    }

    public static final long serialVersionUID = 1L;
    private int numberPC, anchorOrdering = 1;
    private transient AbstractMatrix inputProjection;
    private transient RadVizProjectionParamView paramview;
    private transient RadVizStructure radvizStructure;

}
