package labeledprojection.technique.set;

import distance.DistanceMatrix;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import labeledprojection.model.LabeledProjectionModel;
import labeledprojection.model.LabeledProjectionModelComp;
import projection.technique.sammon.SammonMappingProjectionComp;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Projection.Technique.Projection Sets",
name = "Sammon Mapping Projection IDMAP Set Builder",
description = "Builds several Sammon Mapping Projections, for each distance matrix provided.")
public class SammonMappingProjectionSetBuilderComp extends SammonMappingProjectionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {

        if ((dmats == null)||(dmats.isEmpty()))
                throw new IOException("A distance matrix set should be provided.");

        //Initializing components that will execute the process..
        SammonMappingProjectionComp smc = new SammonMappingProjectionComp();
        smc.setMagicFactor(this.getMagicFactor());
        smc.setNumberIterations(this.getNumberIterations());
        LabeledProjectionModelComp pmc = new LabeledProjectionModelComp();
        pmc.setCollection(collection);
        
        //Building the models...
        models = new ArrayList<LabeledProjectionModel>();
        for (int i=0;i<dmats.size();i++) models.add(null);
        
        Iterator it = dmats.entrySet().iterator();
        String chave = "";
        DistanceMatrix dm = null;
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            chave = (String) e.getKey();
            dm = (DistanceMatrix)e.getValue();
            //executing idmap component with each distance matrix...
            smc.input(dm);
            smc.execute();
            //creating labeled projection model with matrix produced by idmap connection component...
            pmc.input(smc.output());
            pmc.execute();
            LabeledProjectionModel m = (LabeledProjectionModel)pmc.output();
            m.setSource(chave.substring(chave.indexOf("-")+1).trim());
            int pos = Integer.parseInt(chave.substring(0,chave.indexOf("-")).trim());
            models.set(pos, m);
        }
    }

    public void input(@Param(name = "distance matrices") HashMap<String,DistanceMatrix> dmats) {
        this.dmats = dmats;
    }

    public void attach(@Param(name = "distance matrix") DistanceMatrix dmat) {
        if (dmats == null)
            dmats = new HashMap<String,DistanceMatrix>();
        if (dmats != null) {
            dmats.put(Integer.toString(dmats.size())+"-Dmat "+Integer.toString(dmats.size()),dmat);
        }
    }

    public ArrayList<LabeledProjectionModel> outputModels() {
        return models;
    }
    
    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new SammonMappingProjectionSetBuilderParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        dmats = null;
    }

    public void setCollection(String c) {
        this.collection = c;
    }

    public String getCollection() {
        return collection;
    }

    public static final long serialVersionUID = 1L;
    private transient SammonMappingProjectionSetBuilderParamView paramview;
    private transient ArrayList<LabeledProjectionModel> models;
    private transient HashMap<String,DistanceMatrix> dmats;
    private transient String collection;
}
