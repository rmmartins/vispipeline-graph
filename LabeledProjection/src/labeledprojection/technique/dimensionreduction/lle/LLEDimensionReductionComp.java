/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.technique.dimensionreduction.lle;

import java.io.IOException;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import matrix.AbstractMatrix;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Transformation.Dimension Reduction",
name = "LLE Dimensionality Reduction",
description = "Reduce the number of dimension of instances of a data set using LLE.",
howtocite = "Roweis, S. T.; Saul, L. K. Nonlinear dimensionality " +
"reduction by locally linear embedding. Science, v. 290, n. 5500, " +
"p. 2323�2326, 2000.")
public class LLEDimensionReductionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        //projecting
        LLEDimensionReduction lle = new LLEDimensionReduction();
        lle.setNumberNeighbors(nrneighbors);

        if (matrix != null) { //using a matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            projection = lle.reduce(nrdimensions, matrix, diss);
        } else {
            throw new IOException("A points matrix should be provided.");
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public AbstractMatrix output() {
        return projection;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new LLEDimensionReductionParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        projection = null;
        matrix = null;
    }

    public int getNumberNeighbors() {
        return nrneighbors;
    }

    public void setNumberNeighbors(int nrneighbors) {
        this.nrneighbors = nrneighbors;
    }

    public int getNumberDimensions() {
        return nrdimensions;
    }

    public void setNumberDimensions(int ndim) {
        this.nrdimensions = ndim;
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public static final long serialVersionUID = 1L;
    private int nrneighbors = 10, nrdimensions = 2;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private transient LLEDimensionReductionParamView paramview;
    private transient AbstractMatrix projection;
    private transient AbstractMatrix matrix;
}
