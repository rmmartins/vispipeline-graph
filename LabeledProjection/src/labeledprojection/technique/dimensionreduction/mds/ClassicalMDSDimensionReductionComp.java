/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.technique.dimensionreduction.mds;

import java.io.IOException;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import matrix.AbstractMatrix;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Transformation.Dimension Reduction",
name = "MDS Dimensionality Reduction",
description = "Reduce the number of dimension of instances of a data set using Classical MDS.",
howtocite = "Cox, T. F.; Cox, M. A. A. Multidimensional scaling. " +
"Second ed. Chapman & Hall/CRC,2000.")
public class ClassicalMDSDimensionReductionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        //projecting
        ClassicalMDSDimensionReduction mds = new ClassicalMDSDimensionReduction(mdsType);

        if (matrix != null) { //using a matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            projection = mds.reduce(nrdimensions, matrix, diss);
        } else if (dmat != null) { //using a distance matrix
            projection = mds.reduce(nrdimensions, dmat);
        } else {
            throw new IOException("A distance matrix or a points matrix should " +
                    "be provided.");
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "distance matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public AbstractMatrix output() {
        return projection;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new ClassicalMDSDimensionReductionParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        projection = null;
        matrix = null;
        dmat = null;
    }

    /**
     * @return the disstype
     */
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    /**
     * @param disstype the disstype to set
     */
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public boolean isDistanceMatrixProvided() {
        return (dmat != null);
    }

    public int getNumberDimensions() {
        return nrdimensions;
    }

    public void setNumberDimensions(int ndim) {
        this.nrdimensions = ndim;
    }

    public int getMDSType() {
        return mdsType;
    }

    public void setMDSType(int t) {
        mdsType = t;
    }

    public static final long serialVersionUID = 1L;
    private int nrdimensions = 2, mdsType = 0;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private transient ClassicalMDSDimensionReductionParamView paramview;
    private transient AbstractMatrix projection;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
}
