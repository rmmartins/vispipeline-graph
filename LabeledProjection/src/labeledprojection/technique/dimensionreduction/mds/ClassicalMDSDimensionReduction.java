/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */
package labeledprojection.technique.dimensionreduction.mds;

import java.io.IOException;
import java.util.ArrayList;
import distance.DistanceMatrix;
import distance.LightWeightDistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import matrix.reader.MatrixReaderComp;
import mdsj.ClassicalScaling;
import projection.model.ProjectionModelComp;
import projection.technique.Projection;
import projection.util.ProjectionUtil;
import projection.view.ProjectionFrameComp;

/**
 *
 * @author Fernando Vieira Paulovich modified by Jose Gustavo de Souza Paiva
 */
public class ClassicalMDSDimensionReduction {

    public ClassicalMDSDimensionReduction(int mdsType) {
        this.mdsType = mdsType;
    }

    public AbstractMatrix reduce(int nrdimensions, AbstractMatrix matrix, AbstractDissimilarity diss) throws IOException {
        LightWeightDistanceMatrix dmat = new LightWeightDistanceMatrix(matrix, diss);
        long init = System.currentTimeMillis();
        AbstractMatrix m = reduce(nrdimensions,dmat);
        long end = System.currentTimeMillis();
        long diff = end - init;
        switch (mdsType) {
            case 1:
                System.out.println("Time spent (Pivot MDS) -> " + (diff/1000.0f) + " seconds");
                break;
            default:
                System.out.println("Time spent (Classical MDS) -> " + (diff/1000.0f) + " seconds");
        }
        return m;
    }

    public AbstractMatrix reduce(int nrdimensions, DistanceMatrix dmat) throws IOException {
        int size = dmat.getElementCount();

        //creating the squared distance matrix
        double[][] input = new double[size][size];

        for (int i=0;i<size;i++) {
            for (int j=0;j<size;j++) {
                input[i][j] = input[j][i] = dmat.getDistance(i, j);
            }
        }

        double[][] output = new double[nrdimensions][size];

        switch (mdsType) {
            case 1:
                ClassicalScaling.pivotmds(input, output);
                break;
            default:
                ClassicalScaling.fullmds(input, output);
                break;
        }
        

        input = null;
        System.gc();

        //creating the final projection
        AbstractMatrix projection = new DenseMatrix();
        ArrayList<String> attributes = new ArrayList<String>();
        for (int i=0;i<nrdimensions;i++)
            attributes.add("x"+i);
        projection.setAttributes(attributes);

        ArrayList<Integer> ids = dmat.getIds();
        ArrayList<String> labels = dmat.getLabels();
        float[] classData = dmat.getClassData();

        for (int i=0;i<size;i++) {
            float[] vect = new float[nrdimensions];
            for (int j=0;j<nrdimensions;j++)
                vect[j] = (float) output[j][i];

            if (labels.isEmpty()) {
                projection.addRow(new DenseVector(vect, ids.get(i), classData[i]));
            } else {
                projection.addRow(new DenseVector(vect, ids.get(i), classData[i]), labels.get(i));
            }
        }

        return projection;

    }

    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.out.println("ERROR! It should be: ClassicalMDSProjection filename.data");
            System.exit(1);
        }

        ProjectionUtil.log(false, false);

        MatrixReaderComp reader = new MatrixReaderComp();
        reader.setFilename(args[0]);
        reader.execute();
        AbstractMatrix matrix = reader.output();

        ClassicalMDSDimensionReductionComp cmds = new ClassicalMDSDimensionReductionComp();
        cmds.setDissimilarityType(DissimilarityType.EUCLIDEAN);
        cmds.input(matrix);
        cmds.execute();
        AbstractMatrix projection = cmds.output();

        ProjectionModelComp model = new ProjectionModelComp();
        model.input(projection);
        model.execute();

        ProjectionFrameComp frame = new ProjectionFrameComp();
        frame.input(model.output());
        frame.execute();
    }

    private int mdsType = 0; //0: Classical, 1: PivotMDS

}
