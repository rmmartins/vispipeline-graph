/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ISOMAPProjectionParamView.java
 *
 * Created on 19/06/2009, 14:27:48
 */
package labeledprojection.technique.dimensionreduction.isomap;

import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import vispipelinebasics.interfaces.AbstractParametersView;
/**
 *
 * @author Fernando Vieira Paulovich
 */
public class ISOMAPDimensionReductionParamView extends AbstractParametersView {

    /** Creates new form ISOMAPProjectionParamView */
    public ISOMAPDimensionReductionParamView(ISOMAPDimensionReductionComp comp) {
        initComponents();

        this.comp = comp;

        for (DissimilarityType disstype : DissimilarityType.values()) {
            this.dissimilarityComboBox.addItem(disstype);
        }

        reset();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        parametersPanel = new javax.swing.JPanel();
        nrNeighborsLabel = new javax.swing.JLabel();
        nrNeighborsTextField = new javax.swing.JTextField();
        nrDimensionsLabel = new javax.swing.JLabel();
        nrDimensionsTextField = new javax.swing.JTextField();
        dissimilarityPanel = new javax.swing.JPanel();
        dissimilarityComboBox = new javax.swing.JComboBox();

        setBorder(javax.swing.BorderFactory.createTitledBorder("ISOMAP Parameters"));
        setLayout(new java.awt.GridBagLayout());

        parametersPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Parameters"));
        parametersPanel.setLayout(new java.awt.GridBagLayout());

        nrNeighborsLabel.setText("Number of Neighbors");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        parametersPanel.add(nrNeighborsLabel, gridBagConstraints);

        nrNeighborsTextField.setColumns(10);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        parametersPanel.add(nrNeighborsTextField, gridBagConstraints);

        nrDimensionsLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        nrDimensionsLabel.setText("Number of Dimensions");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        parametersPanel.add(nrDimensionsLabel, gridBagConstraints);

        nrDimensionsTextField.setColumns(10);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        parametersPanel.add(nrDimensionsTextField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(parametersPanel, gridBagConstraints);

        dissimilarityPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Dissimilarity"));
        dissimilarityPanel.setLayout(new java.awt.BorderLayout());
        dissimilarityPanel.add(dissimilarityComboBox, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(dissimilarityPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void reset() {
        nrDimensionsTextField.setText(Integer.toString(comp.getNumberDimensions()));
        nrNeighborsTextField.setText(Integer.toString(comp.getNumberNeighbors()));

        if (comp.isDistanceMatrixProvided()) {
            dissimilarityComboBox.setEnabled(false);
        } else {
            dissimilarityComboBox.setEnabled(true);
            dissimilarityComboBox.setSelectedItem(comp.getDissimilarityType());
        }
    }

    @Override
    public void finished() throws IOException {
        if (nrNeighborsTextField.getText().trim().length() > 0) {
            int nneighbors = Integer.parseInt(nrNeighborsTextField.getText());
            if (nneighbors > 0) {
                comp.setNumberNeighbors(nneighbors);
            } else {
                throw new IOException("The number of neighbors should be positive.");
            }
        } else {
            throw new IOException("The number of neighbors should be provided.");
        }
        if (nrDimensionsTextField.getText().trim().length() > 0) {
            int ndimensions = Integer.parseInt(nrDimensionsTextField.getText());
            if (ndimensions > 0) {
                comp.setNumberDimensions(ndimensions);
            } else {
                throw new IOException("The number of dimensions should be positive.");
            }
        } else {
            throw new IOException("The number of dimensions should be provided.");
        }

        if (!comp.isDistanceMatrixProvided()) {
            comp.setDissimilarityType((DissimilarityType) dissimilarityComboBox.getSelectedItem());
        }
    }

    private ISOMAPDimensionReductionComp comp;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox dissimilarityComboBox;
    private javax.swing.JPanel dissimilarityPanel;
    private javax.swing.JLabel nrDimensionsLabel;
    private javax.swing.JTextField nrDimensionsTextField;
    private javax.swing.JLabel nrNeighborsLabel;
    private javax.swing.JTextField nrNeighborsTextField;
    private javax.swing.JPanel parametersPanel;
    // End of variables declaration//GEN-END:variables
}
