/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.technique.dimensionreduction.isomap;

import java.io.IOException;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import matrix.AbstractMatrix;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Transformation.Dimension Reduction",
name = "ISOMAP Dimensionality Reduction",
description = "Reduce the number of dimension of instances of a data set using ISOMAP",
howtocite = "Tenenbaum, J. B.; Silva, V.; Langford, J. C. A global " +
"geometric framework for nonlinear dimensionality reduction. Science, " +
"v. 290, n. 5500, p. 2319-2323, 2000.")
public class ISOMAPDimensionReductionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        //projecting
        ISOMAPDimensionReduction isomap = new ISOMAPDimensionReduction();
        isomap.setNumberNeighbors(nrneighbors);

        if (matrix != null) { //using a matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            projection = isomap.reduce(nrdimensions, matrix, diss);
        } else if (dmat != null) { //using a distance matrix
            projection = isomap.reduce(nrdimensions, dmat);
        } else {
            throw new IOException("A distance matrix or a points matrix should " +
                    "be provided.");
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "distance matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public AbstractMatrix output() {
        return projection;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new ISOMAPDimensionReductionParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        projection = null;
        matrix = null;
        dmat = null;
    }

    /**
     * @return the nrneighbors
     */
    public int getNumberNeighbors() {
        return nrneighbors;
    }

    /**
     * @param nrneighbors the nrneighbors to set
     */
    public void setNumberNeighbors(int nrneighbors) {
        this.nrneighbors = nrneighbors;
    }

    /**
     * @return the disstype
     */
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    /**
     * @param disstype the disstype to set
     */
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public boolean isDistanceMatrixProvided() {
        return (dmat != null);
    }

    public int getNumberDimensions() {
        return nrdimensions;
    }

    public void setNumberDimensions(int ndim) {
        this.nrdimensions = ndim;
    }

    public static final long serialVersionUID = 1L;
    private int nrneighbors = 10, nrdimensions = 2;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private transient ISOMAPDimensionReductionParamView paramview;
    private transient AbstractMatrix projection;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
}
