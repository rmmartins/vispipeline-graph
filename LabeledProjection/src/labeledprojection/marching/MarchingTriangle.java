package labeledprojection.marching;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import labeledprojection.model.LabeledProjectionInstance;
import projection.model.Scalar;

/**
 *
 * @author Jose Gustavo
 */
public class MarchingTriangle {

    private ArrayList<LabeledProjectionInstance> vertices;

    public MarchingTriangle() {
        vertices = new ArrayList<LabeledProjectionInstance>();
    }

    public MarchingTriangle(LabeledProjectionInstance v1, LabeledProjectionInstance v2, LabeledProjectionInstance v3) {
        vertices = new ArrayList<LabeledProjectionInstance>();
        vertices.add(v1);
        vertices.add(v2);
        vertices.add(v3);
    }

    public void addVertice(LabeledProjectionInstance v) {
        if (!vertices.isEmpty()&&vertices.size() < 3)
            vertices.add(v);
    }

    public boolean isEqual(MarchingTriangle mt) {
        for (int i=0;i<mt.vertices.size();i++)
            if (!this.vertices.contains(mt.vertices.get(i)))
                return false;
        return true;
    }

    public void drawContour(BufferedImage image,Scalar scalar,float edge,boolean selected) {

        Graphics2D g2 = (Graphics2D) image.getGraphics();

//        boolean v0 = vertices.get(0).getScalarValue(scalar) >= edge;
//        boolean v1 = vertices.get(1).getScalarValue(scalar) >= edge;
//        boolean v2 = vertices.get(2).getScalarValue(scalar) >= edge;

        boolean v0 = vertices.get(0).getScalarValue(scalar) == edge;
        boolean v1 = vertices.get(1).getScalarValue(scalar) == edge;
        boolean v2 = vertices.get(2).getScalarValue(scalar) == edge;

        int x1,x2,y1,y2;
        Color color = null;
        ArrayList<Point> points = new ArrayList<Point>();

        //Descobrindo qual vertice eh o diferente. A reta sera paralela ao lado formado pelos outros lados iguais...
        if (v1 != v0 && v1 != v2) { //v1 eh o vertice diferente
            x1 = (int) ((vertices.get(1).getX() + vertices.get(0).getX()) / 2);
            y1 = (int) ((vertices.get(1).getY() + vertices.get(0).getY()) / 2);
            x2 = (int) ((vertices.get(1).getX() + vertices.get(2).getX()) / 2);
            y2 = (int) ((vertices.get(1).getY() + vertices.get(2).getY()) / 2);
            if (v1) {
                color = vertices.get(1).getColor();
                points.add(new Point((int) vertices.get(1).getX(),(int) vertices.get(1).getY()));
                points.add(new Point(x1,y1));
                points.add(new Point(x2,y2));
            }
            else {
                color = vertices.get(0).getColor();
                points.add(new Point(x1,y1));
                points.add(new Point(x2,y2));
                points.add(new Point((int) vertices.get(2).getX(),(int) vertices.get(2).getY()));
                points.add(new Point((int) vertices.get(0).getX(),(int) vertices.get(0).getY()));
            }
        }
        else if (v2 != v0 && v2 != v1) { //v2 eh o vertice diferente
            x1 = (int) ((vertices.get(2).getX() + vertices.get(0).getX()) / 2);
            y1 = (int) ((vertices.get(2).getY() + vertices.get(0).getY()) / 2);
            x2 = (int) ((vertices.get(2).getX() + vertices.get(1).getX()) / 2);
            y2 = (int) ((vertices.get(2).getY() + vertices.get(1).getY()) / 2);
            if (v2) {
                color = vertices.get(2).getColor();
                points.add(new Point((int) vertices.get(2).getX(),(int) vertices.get(2).getY()));
                points.add(new Point(x1,y1));
                points.add(new Point(x2,y2));
            }
            else {
                color = vertices.get(0).getColor();
                points.add(new Point(x1,y1));
                points.add(new Point(x2,y2));
                points.add(new Point((int) vertices.get(1).getX(),(int) vertices.get(1).getY()));
                points.add(new Point((int) vertices.get(0).getX(),(int) vertices.get(0).getY()));
            }
        }
        else if (v0 != v1 && v0 != v2) { //v0 eh o vertice diferente
            x1 = (int) ((vertices.get(0).getX() + vertices.get(1).getX()) / 2);
            y1 = (int) ((vertices.get(0).getY() + vertices.get(1).getY()) / 2);
            x2 = (int) ((vertices.get(0).getX() + vertices.get(2).getX()) / 2);
            y2 = (int) ((vertices.get(0).getY() + vertices.get(2).getY()) / 2);
            if (v0) {
                color = vertices.get(0).getColor();
                points.add(new Point((int) vertices.get(0).getX(),(int) vertices.get(0).getY()));
                points.add(new Point(x1,y1));
                points.add(new Point(x2,y2));
            }
            else {
                color = vertices.get(1).getColor();
                points.add(new Point(x1,y1));
                points.add(new Point(x2,y2));
                points.add(new Point((int) vertices.get(2).getX(),(int) vertices.get(2).getY()));
                points.add(new Point((int) vertices.get(1).getX(),(int) vertices.get(1).getY()));
            }
        }else if (v0 && v1 && v2) {
            x1 = x2 = y1 = y2 = 0;
            color = vertices.get(0).getColor();
            points.add(new Point((int) vertices.get(0).getX(),(int) vertices.get(0).getY()));
            points.add(new Point((int) vertices.get(1).getX(),(int) vertices.get(1).getY()));
            points.add(new Point((int) vertices.get(2).getX(),(int) vertices.get(2).getY()));
        }else
            return;
        
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
        if (selected)
            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER,0.85f));
        else
            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER,0.45f));
        g2.setColor(Color.BLACK);
        g2.setStroke(new BasicStroke(1.3f));
        g2.drawLine(x1,y1,x2,y2);
        g2.setStroke(new BasicStroke(1.0f));

        g2.setColor(color);
        Polygon p = new Polygon();
        for (int i=0;i<points.size();i++)
            p.addPoint(points.get(i).x,points.get(i).y);

        //g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER,0.45f));
//        g2.setStroke(new BasicStroke(0.1f));
//        g2.drawPolygon(p);
//        g2.setStroke(new BasicStroke(1.0f));
        g2.fillPolygon(p);
        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER,1.0f));

    }

    public boolean isInside(Point target) {

        float ABC = getArea(vertices.get(0).getX(),vertices.get(0).getY(),
                         vertices.get(1).getX(),vertices.get(1).getY(),
                         vertices.get(2).getX(),vertices.get(2).getY());
        
        float ACT = getArea(vertices.get(0).getX(),vertices.get(0).getY(),
                         vertices.get(2).getX(),vertices.get(2).getY(),
                         target.x,target.y);
        
        float ABT = getArea(vertices.get(0).getX(),vertices.get(0).getY(),
                         vertices.get(1).getX(),vertices.get(1).getY(),
                         target.x,target.y);
        
        float CTB = getArea(vertices.get(2).getX(),vertices.get(2).getY(),
                         target.x,target.y,
                         vertices.get(1).getX(),vertices.get(1).getY());

        if(ABC == ACT+ABT+CTB) return true;
        return false;
    }

    public LabeledProjectionInstance getVertice(int i) {
        return vertices.get(i);
    }

    public float getArea() {
        float[][] matrix = new float[3][3];
        for (int i=0;i<vertices.size();i++) {
            matrix[i][0] = vertices.get(i).getX();
            matrix[i][1] = vertices.get(i).getY();
            matrix[i][2] = 1;
        }
        //Calculating the determinant...
        float det = (matrix[0][0]*matrix[1][1]*matrix[2][2])+(matrix[0][1]*matrix[1][2]*matrix[2][0])+(matrix[0][2]*matrix[1][0]*matrix[2][1])-
                    (matrix[0][1]*matrix[1][0]*matrix[2][2])-(matrix[0][0]*matrix[1][2]*matrix[2][1])-(matrix[0][2]*matrix[1][1]*matrix[2][0]);
        return (Math.abs(det)/2.0f);
    }

    public float getNormalizedArea(float minX, float maxX, float minY, float maxY) {
        float[][] matrix = new float[3][3];
        for (int i=0;i<vertices.size();i++) {
            matrix[i][0] = (vertices.get(i).getX() - minX)/(maxX - minX);
            matrix[i][1] = (vertices.get(i).getY() - minY)/(maxY - minY);
            matrix[i][2] = 1;
        }
        //Calculating the determinant...
        float det = (matrix[0][0]*matrix[1][1]*matrix[2][2])+(matrix[0][1]*matrix[1][2]*matrix[2][0])+(matrix[0][2]*matrix[1][0]*matrix[2][1])-
                    (matrix[0][1]*matrix[1][0]*matrix[2][2])-(matrix[0][0]*matrix[1][2]*matrix[2][1])-(matrix[0][2]*matrix[1][1]*matrix[2][0]);
        return (Math.abs(det)/2.0f);
    }

    public float getArea(float x1, float y1, float x2, float y2, float x3, float y3) {
        return Math.abs(((x2 - x1)*(y3 - y1) - (x3 - x1) * (y2 - y1))/2);
    }

}


