package labeledprojection.marching;

import graph.model.Edge;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import labeledprojection.model.LabeledProjectionInstance;
import labeledprojection.model.LabeledProjectionModel;
import labeledprojection.model.xml.XMLProjectionModelReaderComp;
import labeledprojection.util.LabeledProjectionConstants;
import labeledprojection.util.ProjectionConnectivity;
import projection.model.Scalar;

/**
 *
 * @author Jose Gustavo
 */
public class Principal {

    public ArrayList<MarchingTriangle> mesh = new ArrayList<MarchingTriangle>();

    public ArrayList<Integer> getCommonVertices(ProjectionConnectivity dc, LabeledProjectionInstance v1, LabeledProjectionInstance v2) {
        ArrayList<Integer> retV1 = new ArrayList<Integer>();
        ArrayList<Integer> retV2 = new ArrayList<Integer>();
        //Pegando todos os edges nos quais v1 e v2 participam...
        for (int i=0;i<dc.getEdges().size();i++) {
            Edge edge = dc.getEdges().get(i);
            if (edge.getSource() == v1.getId())
                retV1.add(edge.getTarget());
            else if (edge.getTarget() == v1.getId())
                retV1.add(edge.getSource());

            if (edge.getSource() == v2.getId())
                retV2.add(edge.getTarget());
            else if (edge.getTarget() == v2.getId())
                retV2.add(edge.getSource());
        }

        ArrayList<Integer> commons = new ArrayList<Integer>();
        //Armazenando todos os vertices que sao comuns em arestas dos dois conjuntos...

        for (int i=0;i<retV1.size();i++)
            if (retV2.contains(retV1.get(i)))
                commons.add(retV1.get(i));
        
        return commons;
    }

    public void compress() {

        for (int i=0;i<mesh.size();i++) {
            for (int j=i+1;j<mesh.size();j++) {
                if (mesh.get(i).isEqual(mesh.get(j))) {
                    mesh.remove(j);
                    j--;
                }
            }
        }

    }

    public void createTriangleMesh(LabeledProjectionModel model) {

        ProjectionConnectivity dc = (ProjectionConnectivity) model.getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY);
        for (int i=0;i<dc.getEdges().size();i++) {
            Edge edge = dc.getEdges().get(i);
            ArrayList<Integer> common = getCommonVertices(dc,model.getInstanceById(edge.getSource()),model.getInstanceById(edge.getTarget()));
            if (common != null && !common.isEmpty())
                for (int j=0;j<common.size();j++) {
                    MarchingTriangle mt = new MarchingTriangle(model.getInstanceById(edge.getSource()),
                                                               model.getInstanceById(edge.getTarget()),
                                                               model.getInstanceById(common.get(j)));
                    mesh.add(mt);
                }
        }
        compress();

    }

    public static void main(String a[]) throws IOException {

        XMLProjectionModelReaderComp mc = new XMLProjectionModelReaderComp();
        mc.setFilename("E:\\Corel.xml");
        mc.execute();
        LabeledProjectionModel model = mc.output();
        if (model == null) System.out.println("Erro ao carregar model.");
        
        Principal p = new Principal();
        p.createTriangleMesh(model);

        Scalar sc = model.getSelectedScalar();
        if (sc == null) sc = model.getScalarByName("cdata");
        model.setSelectedScalar(sc);

        MarchingFrame frame = new MarchingFrame(p.mesh);
        frame.setTitle("teste");
        frame.setModel(model);
        frame.setSize(600, 600);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }


}
