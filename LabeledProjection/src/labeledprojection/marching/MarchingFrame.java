package labeledprojection.marching;

import datamining.clustering.HierarchicalClustering;
import datamining.clustering.HierarchicalClustering.HierarchicalClusteringType;
import datamining.neighbors.Pair;
import distance.dissimilarity.Euclidean;
import graph.model.Edge;
import graph.util.Delaunay;
import visualizationbasics.view.MemoryCheck;
import projection.model.ProjectionInstance;
import visualizationbasics.view.selection.AbstractSelection;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import labeledprojection.coordination.ClassMatchCoordination;
import matrix.AbstractMatrix;
import labeledprojection.model.LabeledProjectionInstance;
import labeledprojection.model.LabeledProjectionModel;
import labeledprojection.model.xml.XMLProjectionModelReader;
import labeledprojection.model.xml.XMLProjectionModelWriter;
import labeledprojection.stress.ChecksDialog;
import projection.model.Scalar;
import visualizationbasics.util.SaveDialog;
import visualizationbasics.color.ColorScalePanel;
import labeledprojection.stress.StressCurveComp;
import labeledprojection.util.ProjectionConnectivity;
import labeledprojection.util.ImageCollection;
import labeledprojection.util.KmeansClusters;
import labeledprojection.util.ProjectionOpenDialog;
import labeledprojection.util.LabeledProjectionConstants;
import projection.util.ProjectionUtil;
import labeledprojection.util.filter.XMLFilter;
import labeledprojection.view.ReportView;
import labeledprojection.view.forms.ExportInstancesDialog;
import labeledprojection.view.forms.ImportScalarDialog;
import labeledprojection.view.forms.klassification.SVMClassifyDialog;
import labeledprojection.view.forms.SaveClassDialog;
import labeledprojection.view.selection.ClassAbstractSelection;
import labeledprojection.view.selection.ClassSelection;
import visualizationbasics.util.filter.PNGFilter;
import visualizationbasics.util.filter.SCALARFilter;
import labeledprojection.view.selection.InstanceSelection;
import labeledprojection.view.selection.ViewContentSelection;
import projection.view.tools.JoinScalars;
import projection.view.tools.MultidimensionalClusteringView;
import labeledprojection.view.tools.silhouette.DataSilhouetteCoefficientView;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import projection.view.JExtendedComboBox;
import visualizationbasics.coordination.AbstractCoordinator;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.model.AbstractModel;
import visualizationbasics.util.OpenDialog;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.filter.AbstractFilter;
import visualizationbasics.util.filter.DATAFilter;
import visualizationbasics.util.filter.DMATFilter;
import visualizationbasics.util.filter.ZIPFilter;
import visualizationbasics.view.JFrameModelViewer;
import visualizationbasics.view.MessageDialog;


/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class MarchingFrame extends JFrameModelViewer {

    /** Creates new form ProjectionFrame */
    public MarchingFrame(ArrayList<MarchingTriangle> mesh) {
        this.mesh = mesh;
        this.scalarComboModel = new DefaultComboBoxModel();
        this.edgeComboModel = new DefaultComboBoxModel();
        this.view = new ViewPanel();

        initComponents();
        initExtraComponents();

        InstanceSelection t = new InstanceSelection(this);
        addSelection(t,true);
        this.view.setSelection(t);
        addSelection(new ViewContentSelection(this),false);
        addSelection(new ClassSelection(this),false);

        edgeLabel.setVisible(false);
        edgeCombo.setVisible(false);

    }

    public void addSelection(final ClassAbstractSelection selection, boolean state) {
        if (selection != null) {
            JToggleButton button = new JToggleButton();
            selectionButtonGroup.add(button);
            button.setIcon(selection.getIcon());
            button.setSelected(false);
            button.setToolTipText(selection.toString());
            button.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (getView() != null) {
                        getView().setSelection(selection);
                        selection.reset();
                    }
                }
            });
            button.setSelected(state);
            selectionToolBar.add(button);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        selectionButtonGroup = new javax.swing.ButtonGroup();
        toolBar = new javax.swing.JToolBar();
        openButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        separatorLabel1 = new javax.swing.JLabel();
        zoomInButton = new javax.swing.JButton();
        zoomOutButton = new javax.swing.JButton();
        separatorLabel2 = new javax.swing.JLabel();
        toolButton = new javax.swing.JButton();
        separatorLabel5 = new javax.swing.JLabel();
        findPanel = new javax.swing.JPanel();
        findTextField = new javax.swing.JTextField();
        findButton = new javax.swing.JButton();
        controlPanel = new javax.swing.JPanel();
        dataPanel = new javax.swing.JPanel();
        projectionTabbedPane = new javax.swing.JTabbedPane();
        scrollPanel = new javax.swing.JScrollPane(this.view);
        reportPanel = new ReportView();
        scalarPanel = new javax.swing.JPanel();
        showInstancesCheckBox = new javax.swing.JCheckBox();
        colorLabel = new javax.swing.JLabel();
        scalarCombo = new JExtendedComboBox(this.scalarComboModel);
        edgeLabel = new javax.swing.JLabel();
        edgeCombo = new JExtendedComboBox(this.edgeComboModel);
        saveColorButton = new javax.swing.JButton();
        statusBar_jPanel = new javax.swing.JPanel();
        status_jLabel = new javax.swing.JLabel();
        toolbarPanel = new javax.swing.JPanel();
        fixedToolBar = new javax.swing.JToolBar();
        moveInstancesToggleButton = new javax.swing.JToggleButton();
        cleanInstancesButton = new javax.swing.JButton();
        selectionToolBar = new javax.swing.JToolBar();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        fileOpen = new javax.swing.JMenuItem();
        fileSave = new javax.swing.JMenuItem();
        separator1 = new javax.swing.JSeparator();
        exportMenu = new javax.swing.JMenu();
        fileExportToPng = new javax.swing.JMenuItem();
        fileExportToProjection = new javax.swing.JMenuItem();
        exportDatasetMenuItem = new javax.swing.JMenuItem();
        importMenu = new javax.swing.JMenu();
        importScalarsMenuItem = new javax.swing.JMenuItem();
        menuEdit = new javax.swing.JMenu();
        editClean = new javax.swing.JMenuItem();
        editDelete = new javax.swing.JMenuItem();
        menuTool = new javax.swing.JMenu();
        memoryCheckMenuItem = new javax.swing.JMenuItem();
        separatorOptions1 = new javax.swing.JSeparator();
        scalarMenu = new javax.swing.JMenu();
        importScalarsOption = new javax.swing.JMenuItem();
        exportScalarsOption = new javax.swing.JMenuItem();
        joinScalarsOptions = new javax.swing.JMenuItem();
        separatorOptions2 = new javax.swing.JSeparator();
        clusteringMenu = new javax.swing.JMenu();
        multidimensionalMenuItem = new javax.swing.JMenuItem();
        silhouetteCoefficientMenuItem = new javax.swing.JMenuItem();
        classifyMenu = new javax.swing.JMenu();
        svmMenuItem = new javax.swing.JMenuItem();
        separatorOptions3 = new javax.swing.JSeparator();
        createHCMenu = new javax.swing.JMenu();
        sLinkMenuItem = new javax.swing.JMenuItem();
        cLinkMenuItem = new javax.swing.JMenuItem();
        aLinkMenuItem = new javax.swing.JMenuItem();
        stressMenuItem = new javax.swing.JMenuItem();
        delaunayMenuItem = new javax.swing.JMenuItem();
        generateDictionaryMenuItem = new javax.swing.JMenuItem();
        toolOptions = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        openButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Open16.gif"))); // NOI18N
        openButton.setToolTipText("Open an existing projection");
        openButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openButtonActionPerformed(evt);
            }
        });
        toolBar.add(openButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Save16.gif"))); // NOI18N
        saveButton.setToolTipText("Save the current projection");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        toolBar.add(saveButton);

        separatorLabel1.setText("       ");
        toolBar.add(separatorLabel1);

        zoomInButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/ZoomIn16.gif"))); // NOI18N
        zoomInButton.setToolTipText("Zoom in");
        zoomInButton.setMaximumSize(new java.awt.Dimension(29, 27));
        zoomInButton.setMinimumSize(new java.awt.Dimension(29, 27));
        zoomInButton.setPreferredSize(new java.awt.Dimension(29, 27));
        zoomInButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInButtonActionPerformed(evt);
            }
        });
        toolBar.add(zoomInButton);

        zoomOutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/ZoomOut16.gif"))); // NOI18N
        zoomOutButton.setToolTipText("Zoom out");
        zoomOutButton.setMaximumSize(new java.awt.Dimension(29, 27));
        zoomOutButton.setMinimumSize(new java.awt.Dimension(29, 27));
        zoomOutButton.setPreferredSize(new java.awt.Dimension(29, 27));
        zoomOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutButtonActionPerformed(evt);
            }
        });
        toolBar.add(zoomOutButton);

        separatorLabel2.setText("       ");
        toolBar.add(separatorLabel2);

        toolButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Preferences16.gif"))); // NOI18N
        toolButton.setToolTipText("Tool Preferences");
        toolButton.setMaximumSize(new java.awt.Dimension(29, 27));
        toolButton.setMinimumSize(new java.awt.Dimension(29, 27));
        toolButton.setPreferredSize(new java.awt.Dimension(29, 27));
        toolButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toolButtonActionPerformed(evt);
            }
        });
        toolBar.add(toolButton);

        separatorLabel5.setText("       ");
        toolBar.add(separatorLabel5);

        findPanel.setOpaque(false);
        findPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        findTextField.setColumns(10);
        findTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                findTextFieldKeyPressed(evt);
            }
        });
        findPanel.add(findTextField);

        findButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Find16.gif"))); // NOI18N
        findButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                findButtonActionPerformed(evt);
            }
        });
        findPanel.add(findButton);

        toolBar.add(findPanel);

        getContentPane().add(toolBar, java.awt.BorderLayout.NORTH);

        controlPanel.setLayout(new java.awt.BorderLayout());

        dataPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        dataPanel.setLayout(new java.awt.BorderLayout());

        projectionTabbedPane.addTab("Projection", scrollPanel);
        projectionTabbedPane.addTab("Report", reportPanel);

        dataPanel.add(projectionTabbedPane, java.awt.BorderLayout.CENTER);

        scalarPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        showInstancesCheckBox.setText("Show Instances");
        showInstancesCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showInstancesCheckBoxActionPerformed(evt);
            }
        });
        scalarPanel.add(showInstancesCheckBox);

        colorLabel.setText("Color");
        scalarPanel.add(colorLabel);

        scalarCombo.setMaximumSize(new java.awt.Dimension(85, 27));
        scalarCombo.setMinimumSize(new java.awt.Dimension(85, 27));
        scalarCombo.setPreferredSize(new java.awt.Dimension(85, 27));
        scalarCombo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                scalarComboMouseClicked(evt);
            }
        });
        scalarCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                scalarComboItemStateChanged(evt);
            }
        });
        scalarCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                scalarComboActionPerformed(evt);
            }
        });
        scalarPanel.add(scalarCombo);

        edgeLabel.setText("Edge");
        scalarPanel.add(edgeLabel);

        edgeCombo.setPreferredSize(new java.awt.Dimension(85, 27));
        edgeCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                edgeComboItemStateChanged(evt);
            }
        });
        edgeCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edgeComboActionPerformed(evt);
            }
        });
        scalarPanel.add(edgeCombo);

        saveColorButton.setText("Save");
        saveColorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveColorButtonActionPerformed(evt);
            }
        });
        scalarPanel.add(saveColorButton);

        dataPanel.add(scalarPanel, java.awt.BorderLayout.NORTH);

        controlPanel.add(dataPanel, java.awt.BorderLayout.CENTER);

        statusBar_jPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        statusBar_jPanel.setPreferredSize(new java.awt.Dimension(30, 30));

        status_jLabel.setText("                      ");
        statusBar_jPanel.add(status_jLabel);

        controlPanel.add(statusBar_jPanel, java.awt.BorderLayout.PAGE_END);

        toolbarPanel.setLayout(new java.awt.BorderLayout(0, 20));

        fixedToolBar.setOrientation(1);
        fixedToolBar.setRollover(true);

        moveInstancesToggleButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/navigation/Forward16.gif"))); // NOI18N
        moveInstancesToggleButton.setToolTipText("Move Point");
        moveInstancesToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveInstancesToggleButtonActionPerformed(evt);
            }
        });
        fixedToolBar.add(moveInstancesToggleButton);

        cleanInstancesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Edit16.gif"))); // NOI18N
        cleanInstancesButton.setToolTipText("Clean Instances");
        cleanInstancesButton.setFocusable(false);
        cleanInstancesButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cleanInstancesButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        cleanInstancesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cleanInstancesButtonActionPerformed(evt);
            }
        });
        cleanInstancesButton.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cleanInstancesButtonFocusLost(evt);
            }
        });
        fixedToolBar.add(cleanInstancesButton);

        toolbarPanel.add(fixedToolBar, java.awt.BorderLayout.NORTH);

        selectionToolBar.setOrientation(1);
        toolbarPanel.add(selectionToolBar, java.awt.BorderLayout.CENTER);

        controlPanel.add(toolbarPanel, java.awt.BorderLayout.EAST);

        getContentPane().add(controlPanel, java.awt.BorderLayout.CENTER);

        menuFile.setMnemonic('F');
        menuFile.setText("File");

        fileOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        fileOpen.setMnemonic('O');
        fileOpen.setText("Open Projection");
        fileOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileOpenActionPerformed(evt);
            }
        });
        menuFile.add(fileOpen);

        fileSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        fileSave.setMnemonic('S');
        fileSave.setText("Save Projection");
        fileSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileSaveActionPerformed(evt);
            }
        });
        menuFile.add(fileSave);
        menuFile.add(separator1);

        exportMenu.setText("Export");

        fileExportToPng.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        fileExportToPng.setMnemonic('P');
        fileExportToPng.setText("Export PNG File");
        fileExportToPng.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileExportToPngActionPerformed(evt);
            }
        });
        exportMenu.add(fileExportToPng);

        fileExportToProjection.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        fileExportToProjection.setMnemonic('J');
        fileExportToProjection.setText("Export 2D Points File");
        fileExportToProjection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileExportToProjectionActionPerformed(evt);
            }
        });
        exportMenu.add(fileExportToProjection);

        exportDatasetMenuItem.setText("Export Dataset");
        exportDatasetMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportDatasetMenuItemActionPerformed(evt);
            }
        });
        exportMenu.add(exportDatasetMenuItem);

        menuFile.add(exportMenu);

        importMenu.setText("Import");

        importScalarsMenuItem.setText("Import Scalars");
        importScalarsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importScalarsMenuItemActionPerformed(evt);
            }
        });
        importMenu.add(importScalarsMenuItem);

        menuFile.add(importMenu);

        menuBar.add(menuFile);

        menuEdit.setMnemonic('E');
        menuEdit.setText("Edit");

        editClean.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        editClean.setMnemonic('C');
        editClean.setText("Clean Projection");
        editClean.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editCleanActionPerformed(evt);
            }
        });
        menuEdit.add(editClean);

        editDelete.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_DELETE, 0));
        editDelete.setMnemonic('D');
        editDelete.setText("Delete Points");
        editDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editDeleteActionPerformed(evt);
            }
        });
        menuEdit.add(editDelete);

        menuBar.add(menuEdit);

        menuTool.setText("Tool");
        menuTool.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuToolActionPerformed(evt);
            }
        });

        memoryCheckMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        memoryCheckMenuItem.setMnemonic('H');
        memoryCheckMenuItem.setText("Memory Check");
        memoryCheckMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                memoryCheckMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(memoryCheckMenuItem);
        menuTool.add(separatorOptions1);

        scalarMenu.setText("Scalars");

        importScalarsOption.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        importScalarsOption.setMnemonic('S');
        importScalarsOption.setText("Import Scalars");
        importScalarsOption.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importScalarsOptionActionPerformed(evt);
            }
        });
        scalarMenu.add(importScalarsOption);

        exportScalarsOption.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        exportScalarsOption.setMnemonic('x');
        exportScalarsOption.setText("Export Scalars");
        exportScalarsOption.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportScalarsOptionActionPerformed(evt);
            }
        });
        scalarMenu.add(exportScalarsOption);

        joinScalarsOptions.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_J, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        joinScalarsOptions.setMnemonic('J');
        joinScalarsOptions.setText("Join Scalars");
        joinScalarsOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                joinScalarsOptionsActionPerformed(evt);
            }
        });
        scalarMenu.add(joinScalarsOptions);

        menuTool.add(scalarMenu);
        menuTool.add(separatorOptions2);

        clusteringMenu.setMnemonic('C');
        clusteringMenu.setText("Clustering");

        multidimensionalMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        multidimensionalMenuItem.setText("Multidimensional Data");
        multidimensionalMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                multidimensionalMenuItemActionPerformed(evt);
            }
        });
        clusteringMenu.add(multidimensionalMenuItem);

        silhouetteCoefficientMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        silhouetteCoefficientMenuItem.setText("Silhouette Coefficient");
        silhouetteCoefficientMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                silhouetteCoefficientMenuItemActionPerformed(evt);
            }
        });
        clusteringMenu.add(silhouetteCoefficientMenuItem);

        menuTool.add(clusteringMenu);

        classifyMenu.setText("Classifying");

        svmMenuItem.setText("Support Vector Machine");
        svmMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                svmMenuItemActionPerformed(evt);
            }
        });
        classifyMenu.add(svmMenuItem);

        menuTool.add(classifyMenu);
        menuTool.add(separatorOptions3);

        createHCMenu.setText("Calculate Hierarchical Clustering");

        sLinkMenuItem.setText("Single Link");
        sLinkMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sLinkMenuItemActionPerformed(evt);
            }
        });
        createHCMenu.add(sLinkMenuItem);

        cLinkMenuItem.setText("Complete Link");
        cLinkMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cLinkMenuItemActionPerformed(evt);
            }
        });
        createHCMenu.add(cLinkMenuItem);

        aLinkMenuItem.setText("Average Link");
        aLinkMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aLinkMenuItemActionPerformed(evt);
            }
        });
        createHCMenu.add(aLinkMenuItem);

        menuTool.add(createHCMenu);

        stressMenuItem.setText("Stress Curve");
        stressMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stressMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(stressMenuItem);

        delaunayMenuItem.setText("Create Delaunay Triangulation");
        delaunayMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delaunayMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(delaunayMenuItem);

        generateDictionaryMenuItem.setText("Create Visual Words Dictionary");
        generateDictionaryMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateDictionaryMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(generateDictionaryMenuItem);

        toolOptions.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        toolOptions.setMnemonic('O');
        toolOptions.setText("Tool Options");
        toolOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toolOptionsActionPerformed(evt);
            }
        });
        menuTool.add(toolOptions);

        menuBar.add(menuTool);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    protected void fileOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileOpenActionPerformed
        try {
            PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
            //int result = ImageOpenDialog.showOpenDialog(spm, new XMLFilter(), this);
            int result = OpenDialog.showOpenDialog(spm, new XMLFilter(), this);

            if (result == JFileChooser.APPROVE_OPTION) {
//                String filename = ImageOpenDialog.getFilename();
                String filename = OpenDialog.getFilename();

                try {
                    XMLProjectionModelReader mreader = new XMLProjectionModelReader();
                    LabeledProjectionModel newmodel = new LabeledProjectionModel();
                    mreader.read(newmodel, filename);
                    setModel(newmodel);
                } catch (IOException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    JOptionPane.showMessageDialog(this, e.getMessage(),
                            "Problems opening the file", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(MarchingFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_fileOpenActionPerformed

    protected void fileSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileSaveActionPerformed
        if (model != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
                int result = SaveDialog.showSaveDialog(spm, new XMLFilter(), this, "model.xml");

                if (result == JFileChooser.APPROVE_OPTION) {
                    String filename = SaveDialog.getFilename();

                    try {
                        XMLProjectionModelWriter mwriter = new XMLProjectionModelWriter();
                        mwriter.write((LabeledProjectionModel) model, filename);
                    } catch (IOException e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        JOptionPane.showMessageDialog(this, e.getMessage(),
                                "Problems saving the file", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(MarchingFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_fileSaveActionPerformed

    private void fileExportToPngActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileExportToPngActionPerformed
        try {
            PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
            int result = SaveDialog.showSaveDialog(spm, new PNGFilter(), this, "image.png");

            if (result == JFileChooser.APPROVE_OPTION) {
                String filename = SaveDialog.getFilename();

                try {
                    view.saveToPngImageFile(filename);
                } catch (IOException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    JOptionPane.showMessageDialog(this, e.getMessage(),
                            "Problems saving the file", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(MarchingFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_fileExportToPngActionPerformed

    private void fileExportToProjectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileExportToProjectionActionPerformed
        if (model != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
                int result = SaveDialog.showSaveDialog(spm, new DATAFilter(), this, "projection.data");

                if (result == JFileChooser.APPROVE_OPTION) {
                    String filename = SaveDialog.getFilename();

                    try {
                        AbstractMatrix matrix = ProjectionUtil.modelToMatrix((LabeledProjectionModel) model, getCurrentScalar());
                        matrix.save(filename);
                    } catch (IOException e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        JOptionPane.showMessageDialog(this, e.getMessage(),
                                "Problems saving the file", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(MarchingFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_fileExportToProjectionActionPerformed

    private void editCleanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editCleanActionPerformed
        if (view != null) {
            view.cleanSelectedInstances();
        }
}//GEN-LAST:event_editCleanActionPerformed

    private void editDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editDeleteActionPerformed
        if (view != null) {
            view.removeSelectedInstances();
        }
}//GEN-LAST:event_editDeleteActionPerformed

    private void toolButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toolButtonActionPerformed
        toolOptionsActionPerformed(evt);
}//GEN-LAST:event_toolButtonActionPerformed

    private void zoomOutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomOutButtonActionPerformed
        if (view != null) {
            view.zoomOut();
        }
}//GEN-LAST:event_zoomOutButtonActionPerformed

    private void zoomInButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomInButtonActionPerformed
        if (view != null) {
            view.zoomIn();
        }
}//GEN-LAST:event_zoomInButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        fileSaveActionPerformed(evt);
}//GEN-LAST:event_saveButtonActionPerformed

    private void openButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openButtonActionPerformed
        fileOpenActionPerformed(evt);
}//GEN-LAST:event_openButtonActionPerformed

    private void moveInstancesToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moveInstancesToggleButtonActionPerformed
        moveinstances = moveInstancesToggleButton.isSelected();
}//GEN-LAST:event_moveInstancesToggleButtonActionPerformed

    private void cleanInstancesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cleanInstancesButtonActionPerformed
        editCleanActionPerformed(evt);
}//GEN-LAST:event_cleanInstancesButtonActionPerformed

    private void scalarComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_scalarComboItemStateChanged
        if (evt.getStateChange() == ItemEvent.DESELECTED) {
            Scalar scalar = (Scalar) this.scalarCombo.getSelectedItem();
            if (scalar != null) {
                view.colorAs(scalar);
            }
        }
    }//GEN-LAST:event_scalarComboItemStateChanged

    private void toolOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toolOptionsActionPerformed
        
}//GEN-LAST:event_toolOptionsActionPerformed

    private void silhouetteCoefficientMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_silhouetteCoefficientMenuItemActionPerformed
        if (model != null) {
            try {
                DataSilhouetteCoefficientView.getInstance(this).display((LabeledProjectionModel) model, getCurrentScalar());
                updateScalars(null);
            } catch (IOException ex) {
                Logger.getLogger(MarchingFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_silhouetteCoefficientMenuItemActionPerformed

    private void multidimensionalMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_multidimensionalMenuItemActionPerformed
        if (model != null) {
            Scalar s = MultidimensionalClusteringView.getInstance(this).display((LabeledProjectionModel) model);
            updateScalars(s);
        }
}//GEN-LAST:event_multidimensionalMenuItemActionPerformed

    private void joinScalarsOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_joinScalarsOptionsActionPerformed
        if (model != null) {
            Scalar s = JoinScalars.getInstance(this).display((LabeledProjectionModel) model);
            updateScalars(s);
        }
}//GEN-LAST:event_joinScalarsOptionsActionPerformed

    private void exportScalarsOptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportScalarsOptionActionPerformed
        if (view != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
                int result = SaveDialog.showSaveDialog(spm, new SCALARFilter(), this, "scalars.scalar");

                if (result == JFileChooser.APPROVE_OPTION) {
                    if (model != null) {
                        String filename = SaveDialog.getFilename();
                        ProjectionUtil.exportScalars((LabeledProjectionModel) model, filename);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(MarchingFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_exportScalarsOptionActionPerformed

    private void importScalarsOptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importScalarsOptionActionPerformed
        if (view != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
//                int result = ImageOpenDialog.showOpenDialog(spm, new SCALARFilter(), this);
                int result = OpenDialog.showOpenDialog(spm, new SCALARFilter(), this);

                if (result == javax.swing.JFileChooser.APPROVE_OPTION) {
                    if (model != null) {
                        final MessageDialog dialog = MessageDialog.show(this, "Importing scalars...");

                        Thread t = new Thread() {

                            @Override
                            public void run() {
                                try {
                                    //String filename = ImageOpenDialog.getFilename();
                                    String filename = OpenDialog.getFilename();
                                    ProjectionUtil.importScalars((LabeledProjectionModel) model, filename);
                                    updateScalars(null);
                                } catch (IOException ex) {
                                    Logger.getLogger(MarchingFrame.class.getName()).log(Level.SEVERE, null, ex);
                                } finally {
                                    dialog.close();
                                }
                            }
                        };

                        t.start();
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(MarchingFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_importScalarsOptionActionPerformed

    private void memoryCheckMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_memoryCheckMenuItemActionPerformed
        MemoryCheck.showMemoryCheck();
}//GEN-LAST:event_memoryCheckMenuItemActionPerformed

    private void findButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_findButtonActionPerformed
        if (model != null) {
            model.cleanSelectedInstances();

            Pattern p = Pattern.compile(findTextField.getText().trim().toLowerCase());

            int begin = 0;
            int end = model.getInstances().size();

            boolean stop = false;
            boolean restart = true;

            while (!stop) {
                for (int i = begin; i < end; i++) {
                    Matcher m = p.matcher(model.getInstances().get(i).toString().trim().toLowerCase());

                    if (m.find()) {
                        model.setSelectedInstance(model.getInstances().get(i));
                        stop = true;
                        break;
                    }
                }

                if (restart) {
                    end = begin - 2;
                    begin = 0;
                    restart = false;
                } else {
                    stop = true;
                }
            }

            model.setChanged();
            model.notifyObservers();
        }
    }//GEN-LAST:event_findButtonActionPerformed

    private void findTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_findTextFieldKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            findButtonActionPerformed(null);
        }
    }//GEN-LAST:event_findTextFieldKeyPressed

    protected void scalarComboMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_scalarComboMouseClicked
        if (evt.getClickCount() == 2) {
            Scalar scalar = (Scalar) this.scalarCombo.getSelectedItem();

            if (!scalar.getName().equals(LabeledProjectionConstants.DOTS)) {
                scalarComboModel.removeElement(scalar);
                scalarCombo.setSelectedIndex(0);
                ((LabeledProjectionModel)model).removeScalar(scalar);
                model.notifyObservers();
            }
        }
    }//GEN-LAST:event_scalarComboMouseClicked

    private void cleanInstancesButtonFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cleanInstancesButtonFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_cleanInstancesButtonFocusLost

    private void saveColorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveColorButtonActionPerformed
        Scalar currentScalar = (Scalar) this.scalarCombo.getSelectedItem();
        if (currentScalar != null) {
            SaveClassDialog.getInstance(this,(LabeledProjectionModel)model,((ReportView)getReportPanel()).getDataSourceValue()).display(currentScalar);
        }
    }//GEN-LAST:event_saveColorButtonActionPerformed

    private void exportDatasetMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportDatasetMenuItemActionPerformed
        ImageCollection im = ((LabeledProjectionModel)model).getImageCollection();
        String imFilename = "";
        if (im != null) imFilename = im.getFilename();
        ExportInstancesDialog.getInstance(this).display(model,imFilename);
    }//GEN-LAST:event_exportDatasetMenuItemActionPerformed

    private void importScalarsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importScalarsMenuItemActionPerformed
        ImportScalarDialog isd = ImportScalarDialog.getInstance(this);
        isd.display();
        if (isd.getScalar() != null) {
            this.updateScalars(isd.getScalar());
        }
        model.notifyObservers();
    }//GEN-LAST:event_importScalarsMenuItemActionPerformed

    private void svmMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_svmMenuItemActionPerformed
        SVMClassifyDialog svmd = SVMClassifyDialog.getInstance(this,model);
        svmd.display();
        if (svmd.getNumberScalars() > 0) {
            for (int i=0;i<svmd.getNumberScalars();i++) {
                this.updateScalars(svmd.getNscalar(i));
            }
            model.notifyObservers();
        }
    }//GEN-LAST:event_svmMenuItemActionPerformed

    private void stressMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stressMenuItemActionPerformed
        try {
            StressCurveComp comp = new StressCurveComp();
            comp.input((LabeledProjectionModel) model);
            String source = ((ReportView)getReportPanel()).getDataSourceValue();
            //if (source == null || source.isEmpty() || !source.endsWith(".data")) {
            if (source == null || source.isEmpty()) {
                PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
                ArrayList<AbstractFilter> filters = new ArrayList<AbstractFilter>();
                filters.add(new DATAFilter());
                filters.add(new DMATFilter());
                int result = ProjectionOpenDialog.showOpenDialogMultipleFilter(spm, filters, this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    source = OpenDialog.getFilename();
                }
            }
            if (source.endsWith(".data"))
                comp.setDataFileType(0);
            else if (source.endsWith(".dmat"))
                comp.setDataFileType(1);
            comp.setDataFileName(source);
            ChecksDialog.getInstance(this).display(comp);
//            String message = JOptionPane.showInputDialog("Inform a label to the Stress Curve Graphic (optional):");
//            if (message != null) {
//                comp.setGraphicLabel(message);
//                comp.execute();
//            }
        } catch (IOException ex) {
            Logger.getLogger(MarchingFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_stressMenuItemActionPerformed

    private void generateDictionaryMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateDictionaryMenuItemActionPerformed
        
    }//GEN-LAST:event_generateDictionaryMenuItemActionPerformed

    private void scalarComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_scalarComboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_scalarComboActionPerformed

    private void delaunayMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delaunayMenuItemActionPerformed

        ((LabeledProjectionModel)model).perturb();

        //Creating new Delaunay triangulation
        float[][] points = new float[model.getInstances().size()][];
        for (int i = 0; i < points.length; i++) {
            points[i] = new float[2];
            points[i][0] = ((LabeledProjectionInstance)model.getInstances().get(i)).getX();
            points[i][1] = ((LabeledProjectionInstance)model.getInstances().get(i)).getY();
        }

        Delaunay d = new Delaunay();
        Pair[][] neighborhood = d.execute(points);

        HashMap<Integer, LabeledProjectionInstance> index = new HashMap<Integer, LabeledProjectionInstance>();
        for (int i=0;i<model.getInstances().size();i++) {
            index.put(i,(LabeledProjectionInstance)model.getInstances().get(i));
        }

        ArrayList<Edge> edges = new ArrayList<Edge>();

        for (int i = 0; i < neighborhood.length; i++) {
            for (int j = 0; j < neighborhood[i].length; j++) {
                edges.add(new Edge(index.get(i).getId(),index.get(neighborhood[i][j].index).getId(),neighborhood[i][j].value));
            }
        }

        ProjectionConnectivity delaunayCon = new ProjectionConnectivity(LabeledProjectionConstants.DELAUNAY,edges);
        ((LabeledProjectionModel)model).addProjectionConnectivity(delaunayCon);

        ProjectionConnectivity dotsCon = new ProjectionConnectivity(LabeledProjectionConstants.DOTS, new ArrayList<Edge>());
                
        edgeComboModel.removeAllElements();
        edgeComboModel.addElement(dotsCon);
        edgeComboModel.addElement(delaunayCon);
        edgeComboModel.setSelectedItem(delaunayCon);

        edgeLabel.setVisible(true);
        edgeCombo.setVisible(true);

        model.setChanged();
        model.notifyObservers();

    }//GEN-LAST:event_delaunayMenuItemActionPerformed

    private void edgeComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_edgeComboItemStateChanged
        
}//GEN-LAST:event_edgeComboItemStateChanged

    private void menuToolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuToolActionPerformed
        
    }//GEN-LAST:event_menuToolActionPerformed

    private void edgeComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edgeComboActionPerformed
        ProjectionConnectivity conn = (ProjectionConnectivity) this.edgeCombo.getSelectedItem();
        if (conn != null) {
            if (conn.getName().equalsIgnoreCase(LabeledProjectionConstants.DOTS))
                ((LabeledProjectionModel)model).addProjectionConnectivity(null);
            else
                ((LabeledProjectionModel)model).addProjectionConnectivity(conn);
        }
        model.setChanged();
        model.notifyObservers();
    }//GEN-LAST:event_edgeComboActionPerformed

    private void sLinkMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sLinkMenuItemActionPerformed
        createHCScalar(HierarchicalClusteringType.SLINK);
    }//GEN-LAST:event_sLinkMenuItemActionPerformed

    private void cLinkMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cLinkMenuItemActionPerformed
        createHCScalar(HierarchicalClusteringType.CLINK);
    }//GEN-LAST:event_cLinkMenuItemActionPerformed

    private void aLinkMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aLinkMenuItemActionPerformed
        createHCScalar(HierarchicalClusteringType.ALINK);
    }//GEN-LAST:event_aLinkMenuItemActionPerformed

    private void showInstancesCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showInstancesCheckBoxActionPerformed
        showInstances = this.showInstancesCheckBox.isSelected();
        model.setChanged();
        model.notifyObservers();
    }//GEN-LAST:event_showInstancesCheckBoxActionPerformed

    @Override
    public void setModel(AbstractModel model) {
        if (model instanceof LabeledProjectionModel) {
            if (model != null) {

                Dimension size = getSize();
                size.height = (int) (size.height * 0.75f);
                size.width = (int) (size.width * 0.95f);
                ((LabeledProjectionModel) model).fitToSize(size);

                super.setModel(model);

                Scalar scalar = ((LabeledProjectionModel) model).getSelectedScalar();

                if (scalar != null) {
                    updateScalars(scalar);
                } else {
                    updateScalars(((LabeledProjectionModel) model).getScalars().get(0));
                }

                if (((LabeledProjectionModel)model).getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY) != null) {
                    ProjectionConnectivity dotsCon = new ProjectionConnectivity(LabeledProjectionConstants.DOTS, new ArrayList<Edge>());
                    ProjectionConnectivity delaunayCon = (ProjectionConnectivity) ((LabeledProjectionModel)model).getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY);
                    edgeComboModel.removeAllElements();
                    edgeComboModel.addElement(dotsCon);
                    edgeComboModel.addElement(delaunayCon);
                    edgeComboModel.setSelectedItem(delaunayCon);

                    edgeLabel.setVisible(true);
                    edgeCombo.setVisible(true);
                }

                view.setModel((LabeledProjectionModel) model);
            }
        }
    }

    public void addSelection(final AbstractSelection selection, boolean state) {
        if (selection != null) {
            JToggleButton button = new JToggleButton();
            selectionButtonGroup.add(button);
            button.setIcon(selection.getIcon());
            button.setSelected(false);
            button.setToolTipText(selection.toString());
            button.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (view != null) {
                        view.setSelection(selection);
                    }
                }
            });
            button.setSelected(state);
            selectionToolBar.add(button);
        }
    }

    public void updateScalars(Scalar scalar) {
        scalarComboModel.removeAllElements();
        for (Scalar s : ((LabeledProjectionModel) model).getScalars()) {
            scalarComboModel.addElement(s);
        }

        if (scalar != null) {
            scalarCombo.setSelectedItem(scalar);
            ((LabeledProjectionModel) model).setSelectedScalar(scalar);
        } else {
            scalarCombo.setSelectedItem(((LabeledProjectionModel) model).getSelectedScalar());
        }

        model.setChanged();
        model.notifyObservers();
    }

    public void setViewerBackground(Color bg) {
        if (view != null) {
            view.setBackground(bg);
            view.cleanImage();
            view.repaint();
        }
    }

    public Scalar getCurrentScalar() {
        return (Scalar) scalarCombo.getSelectedItem();
    }

    public ViewPanel getView() {
        return view;
    }

    public boolean isHighQualityRender() {
        return highqualityrender;
    }

    public void setHighQualityRender(boolean highqualityrender) {
        this.highqualityrender = highqualityrender;

        view.cleanImage();
        view.repaint();
    }

    public void setDrawAs(int d) {
        ArrayList<AbstractInstance> instances = getModel().getInstances();
        if ((instances != null)&&(!instances.isEmpty())) {
            for (int i=0;i<instances.size();i++)
                if (instances.get(i) instanceof LabeledProjectionInstance)
                    ((LabeledProjectionInstance)instances.get(i)).setDrawAs(d);
        }
        view.cleanImage();
        view.repaint();
    }

    public boolean isShowInstanceLabel() {
        return showinstancelabel;
    }

    public void setShowInstanceLabel(boolean showinstancelabel) {
        this.showinstancelabel = showinstancelabel;
        this.showinstanceImage = !showinstancelabel;
        view.cleanImage();
        view.repaint();
    }

    public void setShowInstanceImage(boolean showinstanceImage) {
        this.showinstanceImage = showinstanceImage;
        this.showinstancelabel = !showinstanceImage;
        view.cleanImage();
        view.repaint();
    }

    public boolean isMoveInstances() {
        return moveinstances;
    }

    public void setMoveInstance(boolean moveinstances) {
        this.moveinstances = moveinstances;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (model != null) {
            view.cleanImage();
            view.repaint();
        }
    }

    public void changeStatus(String status) {
        this.status_jLabel.setText(status);
        this.status_jLabel.update(this.status_jLabel.getGraphics());
        Rectangle r = this.status_jLabel.getGraphicsConfiguration().getBounds();
        //this.status_jLabel.getGraphics().fillRect(r.x, r.y, r.width, r.height);
        this.status_jLabel.getGraphics().clearRect(r.x, r.y, r.width, r.height);
        this.status_jLabel.update(this.status_jLabel.getGraphics());
    }

    public void updateImage() {
        if (this.view != null) {
            this.view.cleanImage();
            this.view.adjustPanel();
            this.view.repaint();
        }
    }

    void setDrawFrame(boolean selected) {

         ArrayList<AbstractInstance> instances = getModel().getInstances();
         if ((instances != null)&&(!instances.isEmpty())) {
             for (int i=0;i<instances.size();i++) {
                 if (instances.get(i) instanceof LabeledProjectionInstance) {
                     ((LabeledProjectionInstance)instances.get(i)).setDrawFrame(selected);
                 }
             }
         }
        view.cleanImage();
        view.repaint();
    }

    private void createHCScalar(HierarchicalClusteringType hierarchicalClusteringType) {
        if (model != null) {
            try {
                javax.swing.JOptionPane.showMessageDialog(this,
                        "The Hierachical Clustering is a very expensive process." +
                        "\nIt can take several minutes!",
                        "WARNING", javax.swing.JOptionPane.WARNING_MESSAGE);

                float[][] projection = new float[model.getInstances().size()][];
                for (int i = 0; i < model.getInstances().size(); i++) {
                    projection[i] = new float[2];
                    projection[i][0] = ((LabeledProjectionInstance)model.getInstances().get(i)).getX();
                    projection[i][1] = ((LabeledProjectionInstance)model.getInstances().get(i)).getY();
                }

                DenseMatrix dproj = new DenseMatrix();
                for (int i = 0; i < projection.length; i++) {
                    dproj.addRow(new DenseVector(projection[i]));
                }

                HierarchicalClustering hc = new HierarchicalClustering(hierarchicalClusteringType);
                float[] hcScalars = hc.getPointsHeight(dproj, new Euclidean());

                String scalarname = "hc-slink";
                if (hierarchicalClusteringType == HierarchicalClusteringType.ALINK) scalarname = "hc-alink";
                else if (hierarchicalClusteringType == HierarchicalClusteringType.CLINK) scalarname = "hc-clink";

                Scalar scalar = ((LabeledProjectionModel)model).addScalar(scalarname);

                for (int i=0;i<model.getInstances().size();i++)
                    ((LabeledProjectionInstance)model.getInstances().get(i)).setScalarValue(scalar, hcScalars[i]);
                
                this.updateScalars(scalar);
                model.notifyObservers();
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public class ViewPanel extends JPanel {

        public ViewPanel() {
            this.selcolor = java.awt.Color.RED;
            this.setBackground(java.awt.Color.WHITE);

            this.addMouseMotionListener(new MouseMotionListener());
            this.addMouseListener(new MouseClickedListener());

            this.setLayout(new FlowLayout(FlowLayout.LEFT));
        }

        @Override
        public void paintComponent(java.awt.Graphics g) {
            super.paintComponent(g);

            java.awt.Graphics2D g2 = (java.awt.Graphics2D) g;

            if (model != null && image == null) {
                Dimension size = ((LabeledProjectionModel) model).getSize();
                image = new BufferedImage(size.width + 10, size.height + 10,
                        BufferedImage.TYPE_INT_RGB);

                java.awt.Graphics2D g2Buffer = image.createGraphics();
                g2Buffer.setColor(this.getBackground());
                g2Buffer.fillRect(0, 0, size.width + 10, size.height + 10);

                if (highqualityrender) {
                    g2Buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);
                } else {
                    g2Buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_OFF);
                }

                if (showInstances)
                    ((LabeledProjectionModel) model).draw(image, highqualityrender);

                Scalar sc = ((LabeledProjectionModel) model).getSelectedScalar();

                for (int k=0;k<=9;k++) {
                    for (int i=0;i<mesh.size();i++) {
                        mesh.get(i).drawContour(image,((LabeledProjectionModel) model).getSelectedScalar(),(float)k,false);
                    }
                }

//                for (int i=0;i<mesh.size();i++) {
//                    mesh.get(i).drawContour(image,((LabeledProjectionModel) model).getSelectedScalar(),9.0f,java.awt.Color.RED);
//                }
//
//                for (int i=0;i<mesh.size();i++) {
//                    mesh.get(i).drawContour(image,((LabeledProjectionModel) model).getSelectedScalar(),8.0f,java.awt.Color.GREEN);
//                }
//
//                for (int i=0;i<mesh.size();i++) {
//                    mesh.get(i).drawContour(image,((LabeledProjectionModel) model).getSelectedScalar(),7.0f,java.awt.Color.BLUE);
//                }
//
//                for (int i=0;i<mesh.size();i++) {
//                    mesh.get(i).drawContour(image,((LabeledProjectionModel) model).getSelectedScalar(),6.0f,java.awt.Color.BLACK);
//                }

                g2Buffer.dispose();
            }

            if (image != null) {
                g2.drawImage(image, 0, 0, null);
            }

            //Draw he rectangle to select the instances
            if (selsource != null && seltarget != null) {
                int x = selsource.x;
                int width = width = seltarget.x - selsource.x;

                int y = selsource.y;
                int height = seltarget.y - selsource.y;

                if (selsource.x > seltarget.x) {
                    x = seltarget.x;
                    width = selsource.x - seltarget.x;
                }

                if (selsource.y > seltarget.y) {
                    y = seltarget.y;
                    height = selsource.y - seltarget.y;
                }
                g2.setColor(selcolor);
                g2.drawRect(x, y, width, height);

                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.45f));
                g2.fillRect(x, y, width, height);
            } else { //Draw the instance label                
                if (showinstancelabel && labelText != null && labelpos != null) {
                    int style = java.awt.Font.PLAIN;
                    if (labelBold) style = java.awt.Font.BOLD;
                    java.awt.Font f = new java.awt.Font(this.getFont().getName(),style,labelSize);
                    g2.setFont(f);
                    //g2.setFont(this.getFont());
                    //java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());
                    java.awt.FontMetrics metrics = g2.getFontMetrics(f);

                    //Getting the label size
                    int width = metrics.stringWidth(labelText);
                    int height = metrics.getAscent();

                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
                    g2.setPaint(java.awt.Color.WHITE);
                    g2.fillRect(labelpos.x - 2, labelpos.y - height, width + 4, height + 4);
                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));

                    g2.setColor(java.awt.Color.DARK_GRAY);
                    g2.drawRect(labelpos.x - 2, labelpos.y - height, width + 4, height + 4);

                    //Drawing the label
                    g2.drawString(labelText, labelpos.x, labelpos.y);
                }else if (showinstanceImage && labelImage != null && labelpos != null) {
                    g2.drawImage(this.labelImage.getScaledInstance(100,100,0),labelpos.x,labelpos.y,null);
                }
            }

            //drawn the selection polygon
            if (selpolygon != null) {
                g2.setColor(selcolor);
                g2.drawPolygon(selpolygon);

                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.45f));
                g2.fillPolygon(selpolygon);
            }
        }

        public void saveToPngImageFile(String filename) throws IOException {
            try {
                Dimension size = ((LabeledProjectionModel) model).getSize();
                BufferedImage buffer = new BufferedImage(size.width + 10, size.height + 10,
                        BufferedImage.TYPE_INT_RGB);
                paint(buffer.getGraphics());
                ImageIO.write(buffer, "png", new File(filename));
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }

        public void cleanImage() {
            image = null;
        }

        public void setModel(LabeledProjectionModel model) {
            colorscale = new ColorScalePanel(null);
            colorscale.setColorTable(model.getColorTable());
            colorscale.setPreferredSize(new Dimension(200, 12));
            removeAll();
            add(colorscale);

            colorscale.setBackground(getBackground());

            Dimension size = model.getSize();
            setPreferredSize(new Dimension(size.width * 2, size.height * 2));
            setSize(new Dimension(size.width * 2, size.height * 2));

            cleanImage();
            repaint();
        }

        public void setSelection(AbstractSelection selection) {
            this.selection = selection;
        }

        public void colorAs(Scalar scalar) {
            if (model != null) {
                ((LabeledProjectionModel) model).setSelectedScalar(scalar);
                model.notifyObservers();
            }
        }

        public void zoomIn() {
            if (model != null) {
                ((LabeledProjectionModel) model).zoom(1.1f);

                //Change the size of the panel according to the projection
                Dimension size = ((LabeledProjectionModel) model).getSize();
                setPreferredSize(new Dimension(size.width * 2, size.height * 2));
                setSize(new Dimension(size.width * 2, size.height * 2));

                model.notifyObservers();
            }
        }

        public void zoomOut() {
            if (model != null) {
                ((LabeledProjectionModel) model).zoom(0.9091f);

                //Change the size of the panel according to the projection
                Dimension size = ((LabeledProjectionModel) model).getSize();
                setPreferredSize(new Dimension(size.width * 2, size.height * 2));
                setSize(new Dimension(size.width * 2, size.height * 2));

                model.notifyObservers();
            }
        }

        public void adjustPanel() {
            float iniX = ((ProjectionInstance) model.getInstances().get(0)).getX();
            float iniY = ((ProjectionInstance) model.getInstances().get(0)).getY();
            float max_x = iniX, max_y = iniX;
            float min_x = iniY, min_y = iniY;
            int zero = 30;

            for (int i = 1; i < model.getInstances().size(); i++) {
                float x = ((ProjectionInstance) model.getInstances().get(i)).getX();
                if (max_x < x) {
                    max_x = x;
                } else if (min_x > x) {
                    min_x = x;
                }

                float y = ((ProjectionInstance) model.getInstances().get(i)).getY();
                if (max_y < y) {
                    max_y = y;
                } else if (min_y > y) {
                    min_y = y;
                }
            }

            for (AbstractInstance ai : model.getInstances()) {
                ProjectionInstance pi = (ProjectionInstance) ai;
                pi.setX(pi.getX() + zero - min_x);
                pi.setY(pi.getY() + zero - min_y);
            }

            Dimension d = this.getSize();
            d.width = (int) max_x + zero;
            d.height = (int) max_y + zero;
            setSize(d);
            setPreferredSize(d);

            model.notifyObservers();
        }

        public void cleanSelectedInstances() {
            if (model != null) {
                model.cleanSelectedInstances();
                model.notifyObservers();
            }
            for (int i=0;i<getCoordinators().size();i++) {
                getCoordinators().get(i).coordinate(model.getSelectedInstances(),null);
            }
        }

        public void removeSelectedInstances() {
            if (model != null) {
                model.removeSelectedInstances();
                model.notifyObservers();
            }
        }

        public ArrayList<ProjectionInstance> getSelectedInstances(Polygon polygon) {
            ArrayList<ProjectionInstance> selected = new ArrayList<ProjectionInstance>();

            if (model != null) {
                selected = ((LabeledProjectionModel) model).getInstancesByPosition(polygon);
            }

            return selected;
        }

        public ArrayList<ProjectionInstance> getSelectedInstances(Point source, Point target) {
            ArrayList<ProjectionInstance> selinstances = new ArrayList<ProjectionInstance>();

            if (model != null) {
                int x = Math.min(source.x, target.x);
                int width = Math.abs(source.x - target.x);

                int y = Math.min(source.y, target.y);
                int height = Math.abs(source.y - target.y);

                Rectangle rect = new Rectangle(x, y, width, height);
                selinstances = ((LabeledProjectionModel) model).getInstancesByPosition(rect);
            }

            return selinstances;
        }

        @Override
        public void setBackground(Color bg) {
            super.setBackground(bg);

            if (this.colorscale != null) {
                this.colorscale.setBackground(bg);
            }
        }

        public void removeInstancesWithScalar(float val) {
            if (model != null) {
                ArrayList<AbstractInstance> insts = new ArrayList<AbstractInstance>();
                Scalar scalar = ((LabeledProjectionModel) model).addScalar("cdata");
                for (AbstractInstance ai : model.getInstances()) {
                    if (((ProjectionInstance) ai).getScalarValue(scalar) == val) {
                        insts.add(ai);
                    }
                }

                model.removeInstances(insts);
                model.notifyObservers();
            }
        }

        class MouseMotionListener extends MouseMotionAdapter {

            @Override
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                super.mouseMoved(evt);

                if (model != null) {
                    LabeledProjectionInstance instance = (LabeledProjectionInstance)((LabeledProjectionModel) model).getInstanceByPosition(evt.getPoint());

                    if (instance != null) {
                        //Show the instance label
                        if (instance.isShowLabel()) {
                            labelText = instance.getLabel();
                            if (labelText.trim().length() > 0) {
                                if (labelText.length() > 100) {
                                    labelText = labelText.substring(0, 96) + "...";
                                }
                                labelpos = evt.getPoint();
                                repaint();
                            }
                        }else {//if (instance instanceof LabeledProjectionInstance) {
//                            if (((LabeledProjectionInstance)instance).isShowImage()) {
//                                if (((LabeledProjectionInstance)instance).getImage() != null)
//                                    labelImage = ((LabeledProjectionInstance)instance).getImage().getScaledInstance(100,100,0);
//                                labelpos = evt.getPoint();
//                                repaint();
//                            }
                            if (instance instanceof LabeledProjectionInstance) {
                                if (instance.isShowImage()) {
                                    labelImage = null;
                                    try {
                                        Image im = instance.getImage(((LabeledProjectionModel) instance.getModel()).getImageCollection());
                                        if (im != null) {
                                            labelImage = im.getScaledInstance(100, 100, 0);
                                        }
                                        labelpos = evt.getPoint();
                                        repaint();
                                    } catch (IOException ex) {
                                        Logger.getLogger(MarchingFrame.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }
                        }
                    } else {
                        //Clear the label
                        labelText = null;
                        labelImage = null;
                        labelpos = null;
                        repaint();
                    }
                }
            }

            @Override
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                if (selinstance != null) {
                    if (model.hasSelectedInstances()) {
                        float x = evt.getX() - selinstance.getX();
                        float y = evt.getY() - selinstance.getY();

                        for (AbstractInstance ai : model.getSelectedInstances()) {
                            ProjectionInstance pi = (ProjectionInstance) ai;
                            pi.setX(x + pi.getX());
                            pi.setY(y + pi.getY());
                        }

                        adjustPanel();
                    }
                } else if (selsource != null) {
                    seltarget = evt.getPoint();
                } else if (selpolygon != null) {
                    selpolygon.addPoint(evt.getX(), evt.getY());
                }

                repaint();
            }
        }

        class MouseClickedListener extends MouseAdapter {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                super.mouseClicked(evt);

                if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
                    if (model != null) {
                        LabeledProjectionInstance instance = (LabeledProjectionInstance) ((LabeledProjectionModel) model).getInstanceByPosition(evt.getPoint());
                        if (instance != null) {
                            changeStatus("Number of Instances in Selection: " + 1);
                            if (evt.isControlDown()) {
                                ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
                                instances.addAll(model.getSelectedInstances());
                                model.cleanSelectedInstances();
                                if (instances.contains(instance))
                                    instances.remove(instance);
                                else
                                    instances.add(instance);
                                model.setSelectedInstances(instances);
                                if (selection != null)
                                    selection.selected(instances);
                                model.setChanged();
                                float perc = 100.0f * (instances.size() / ((float) model.getInstances().size()));
                                DecimalFormat df = new DecimalFormat("0.##");
                                changeStatus("Number of Instances in Selection: " + instances.size() + " (" + df.format(perc) + "%)");
                            }else {
                                model.setSelectedInstance(instance);
                                if (selection != null) {
                                    ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
                                    instances.add(instance);
                                    selection.selected(instances);
                                }
                                float perc = 100.0f * (1 / ((float) model.getInstances().size()));
                                DecimalFormat df = new DecimalFormat("0.##");
                                changeStatus("Number of Instances in Selection: 1 (" + df.format(perc) + "%)");
                            }
                            model.notifyObservers();
                        }
                    }
                } else if (evt.getButton() == java.awt.event.MouseEvent.BUTTON3) {
                    cleanSelectedInstances();
                    changeStatus("Number of Instances in Selection: " + 0);
                }
            }

            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                super.mousePressed(evt);

                if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
                    if (model != null) {
                        LabeledProjectionInstance instance = (LabeledProjectionInstance) ((LabeledProjectionModel) model).getInstanceByPosition(evt.getPoint());

                        if (instance != null) {
                            if (moveinstances) {
                                if (model.getSelectedInstances().contains(instance)) {
                                    selinstance = instance;
                                }
                            }
                        } else {
                            selsource = evt.getPoint();
                        }
                    }
                } else if (evt.getButton() == java.awt.event.MouseEvent.BUTTON3) {
                    selpolygon = new java.awt.Polygon();
                    selpolygon.addPoint(evt.getX(), evt.getY());
                }
            }

            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                super.mouseReleased(evt);

                if (model != null) {
                    if ((selsource != null && seltarget != null) || selpolygon != null) {
                        ArrayList<ProjectionInstance> instances = null;

                        if (selpolygon != null) {
                            instances = getSelectedInstances(selpolygon);
                        } else {
                            instances = getSelectedInstances(selsource, seltarget);
                        }

                        if (instances != null) {
                            float perc = 100.0f * (instances.size() / ((float) model.getInstances().size()));
                            DecimalFormat df = new DecimalFormat("0.##");
                            changeStatus("Number of Instances in Selection: " + instances.size() + " (" + df.format(perc) + "%)");

                            if (selection != null) {
                                selection.selected(new ArrayList<AbstractInstance>(instances));
                            }
                        }
                    }
                }

                selpolygon = null;
                selinstance = null;
                selsource = null;
                seltarget = null;
            }
        }
        
        private ProjectionInstance selinstance;
        private Polygon selpolygon;
        private Point selsource;
        private Point seltarget;
        private Color selcolor;
        private String labelText;
        private boolean labelBold = false;
        private int labelSize = 12;
//        private int imageSize = 4;
        private Image labelImage;
        private Point labelpos;
        private BufferedImage image;
        private ColorScalePanel colorscale;
        private AbstractSelection selection;
    }

    public boolean isShowInstanceImage() {
        return showinstanceImage;
    }

    public void setShowInstances(boolean label, boolean image) {
        showinstanceImage = image;
        showinstancelabel = label;
    }

    public int setlabelType(int option) {
        ArrayList<AbstractInstance> instances = getModel().getInstances();
        if ((instances != null)&&(!instances.isEmpty())) {
            switch (option) {
                case 0:
                    if (this.isShowInstanceImage() || this.isShowInstanceLabel()) {
                        for (int i=0;i<instances.size();i++) {
                            if (instances.get(i) instanceof LabeledProjectionInstance) {
                                ((LabeledProjectionInstance)instances.get(i)).setShowLabel(false);
                                ((LabeledProjectionInstance)instances.get(i)).setShowImage(false);
                            }
                        }
                        this.setShowInstances(false,false);
                    }
                    return 0;
                case 1:
                    if (!this.isShowInstanceLabel()) {
                        this.setShowInstanceLabel(true);
                        for (int i=0;i<instances.size();i++) {
                            if (instances.get(i) instanceof LabeledProjectionInstance) {
                                ((LabeledProjectionInstance)instances.get(i)).setShowLabel(true);
                                ((LabeledProjectionInstance)instances.get(i)).setShowImage(false);
                            }
                        }
                    }
                    return 1;
                case 2:
                    if (!this.isShowInstanceImage()) {
                        int oldIndex = 0;
                        if (isShowInstanceLabel()) oldIndex = 1;
                        else if (isShowInstanceImage()) oldIndex = 2;
                        ImageCollection im = ((LabeledProjectionModel)model).getImageCollection();
                        if ((im == null)||(!ProjectionOpenDialog.checkImages(im,model))) {
                            try {
                                PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
                                im = ProjectionOpenDialog.openImages(spm,new ZIPFilter(),this);
                                if (im == null) {
                                    //Usuario desistiu de aplicar as imagens, voltar opcao antiga
                                    return oldIndex;
                                }else {
                                    if (ProjectionOpenDialog.checkImages(im,model)) {
                                       ((LabeledProjectionModel)model).setImageCollection(im);
                                       ((ReportView)(getReportPanel())).setDataSource(im.getFilename());
                                       this.setShowInstanceImage(true);
                                       //Populando imagem na instancia...
                                       for (int i=0;i<instances.size();i++) {
                                           if (instances.get(i) instanceof LabeledProjectionInstance) {
    //                                           Image image = im.getImage(((LabeledProjectionInstance)instances.get(i)).getLabel());
    //                                           ((LabeledProjectionInstance)instances.get(i)).setImage(image);
                                               ((LabeledProjectionInstance)instances.get(i)).setShowLabel(false);
                                               ((LabeledProjectionInstance)instances.get(i)).setShowImage(true);
                                           }
                                       }
                                   }else {
                                       JOptionPane.showMessageDialog(this,
                                            "The image collection file do not correspond to the projected image collection!",
                                            "Error",
                                            JOptionPane.ERROR_MESSAGE,null);
                                       //colecao escolhida nao correspondeo a colecao projetada, voltar opcao antiga
                                       return oldIndex;
                                   }
                                }
                            } catch (IOException ex) {
                                Logger.getLogger(MarchingFrame.class.getName()).log(Level.SEVERE, null, ex);
                                //voltar opcao antiga
                                return oldIndex;
                            }
                        }else {
                            this.setShowInstanceImage(true);
                            for (int i=0;i<instances.size();i++) {
                                if (instances.get(i) instanceof LabeledProjectionInstance) {
    //                                try {
    //                                    Image image = im.getImage(((LabeledProjectionInstance)instances.get(i)).getLabel());
    //                                    ((LabeledProjectionInstance)instances.get(i)).setImage(image);
                                        ((LabeledProjectionInstance)instances.get(i)).setShowLabel(false);
                                        ((LabeledProjectionInstance)instances.get(i)).setShowImage(true);
    //                                } catch (IOException ex) {
    //                                   Logger.getLogger(ProjectionFrameOptions.class.getName()).log(Level.SEVERE, null, ex);
    //                                   //voltar opcao antiga
    //                                   return oldIndex;
    //                                }
                                }
                            }
                        }
                        return 2;
                    }
            }
            getView().cleanImage();
            getView().repaint();
        }
        return option;
    }

    public void setLabelSize(int size) {
        this.view.labelSize = size;
    }

    public void setLabelBold(boolean o) {
        this.view.labelBold = o;
    }

//    public void setImageSize(int size) {
//        ((LabeledProjectionModel)this.getModel()).setImageSize(size);
//        view.cleanImage();
//        view.repaint();
//    }
//
//    public int getImageSize() {
//        return ((LabeledProjectionModel)this.getModel()).getImageSize();
//    }

    public int getLabelSize() {
        return this.view.labelSize;
    }

    public boolean getLabelBold() {
        return this.view.labelBold;
    }

    public javax.swing.JPanel getReportPanel() {
        return reportPanel;
    }

    private void initExtraComponents() {

        createKmeansLabelsButton = new javax.swing.JButton();
        createKmeansLabelsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/About16.gif"))); // NOI18N
        createKmeansLabelsButton.setToolTipText("Group instances using Kmeans");
        createKmeansLabelsButton.setFocusable(false);
        createKmeansLabelsButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        createKmeansLabelsButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        createKmeansLabelsButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createKmeansLabelsButtonActionPerformed(evt);
            }
        });
        selectionToolBar.add(createKmeansLabelsButton);

    }

    private void createKmeansLabelsButtonActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            KmeansClusters labels = new KmeansClusters((LabeledProjectionModel)getModel());
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            Scalar kmeansScalar = labels.execute(this);
            if (kmeansScalar != null)
                updateScalars(kmeansScalar);
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        } catch (IOException ex) {
            Logger.getLogger(MarchingFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected ArrayList<MarchingTriangle> mesh;
    protected boolean showInstances = false;
    protected DefaultComboBoxModel scalarComboModel;
    protected DefaultComboBoxModel edgeComboModel;
    private boolean highqualityrender = true;
    private boolean showinstancelabel = true;
    private boolean showinstanceImage = false;
    private boolean moveinstances = true;
    protected ViewPanel view;
    private javax.swing.JButton createKmeansLabelsButton;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aLinkMenuItem;
    private javax.swing.JMenuItem cLinkMenuItem;
    private javax.swing.JMenu classifyMenu;
    private javax.swing.JButton cleanInstancesButton;
    private javax.swing.JMenu clusteringMenu;
    private javax.swing.JLabel colorLabel;
    private javax.swing.JPanel controlPanel;
    private javax.swing.JMenu createHCMenu;
    protected javax.swing.JPanel dataPanel;
    private javax.swing.JMenuItem delaunayMenuItem;
    protected javax.swing.JComboBox edgeCombo;
    protected javax.swing.JLabel edgeLabel;
    private javax.swing.JMenuItem editClean;
    private javax.swing.JMenuItem editDelete;
    private javax.swing.JMenuItem exportDatasetMenuItem;
    protected javax.swing.JMenu exportMenu;
    private javax.swing.JMenuItem exportScalarsOption;
    private javax.swing.JMenuItem fileExportToPng;
    private javax.swing.JMenuItem fileExportToProjection;
    private javax.swing.JMenuItem fileOpen;
    private javax.swing.JMenuItem fileSave;
    private javax.swing.JButton findButton;
    private javax.swing.JPanel findPanel;
    private javax.swing.JTextField findTextField;
    protected javax.swing.JToolBar fixedToolBar;
    private javax.swing.JMenuItem generateDictionaryMenuItem;
    private javax.swing.JMenu importMenu;
    private javax.swing.JMenuItem importScalarsMenuItem;
    private javax.swing.JMenuItem importScalarsOption;
    private javax.swing.JMenuItem joinScalarsOptions;
    private javax.swing.JMenuItem memoryCheckMenuItem;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuEdit;
    protected javax.swing.JMenu menuFile;
    protected javax.swing.JMenu menuTool;
    private javax.swing.JToggleButton moveInstancesToggleButton;
    private javax.swing.JMenuItem multidimensionalMenuItem;
    private javax.swing.JButton openButton;
    protected javax.swing.JTabbedPane projectionTabbedPane;
    private javax.swing.JPanel reportPanel;
    private javax.swing.JMenuItem sLinkMenuItem;
    private javax.swing.JButton saveButton;
    private javax.swing.JButton saveColorButton;
    protected javax.swing.JComboBox scalarCombo;
    private javax.swing.JMenu scalarMenu;
    protected javax.swing.JPanel scalarPanel;
    protected javax.swing.JScrollPane scrollPanel;
    protected javax.swing.ButtonGroup selectionButtonGroup;
    protected javax.swing.JToolBar selectionToolBar;
    private javax.swing.JSeparator separator1;
    private javax.swing.JLabel separatorLabel1;
    private javax.swing.JLabel separatorLabel2;
    private javax.swing.JLabel separatorLabel5;
    private javax.swing.JSeparator separatorOptions1;
    private javax.swing.JSeparator separatorOptions2;
    private javax.swing.JSeparator separatorOptions3;
    private javax.swing.JCheckBox showInstancesCheckBox;
    private javax.swing.JMenuItem silhouetteCoefficientMenuItem;
    private javax.swing.JPanel statusBar_jPanel;
    private javax.swing.JLabel status_jLabel;
    private javax.swing.JMenuItem stressMenuItem;
    private javax.swing.JMenuItem svmMenuItem;
    protected javax.swing.JToolBar toolBar;
    private javax.swing.JButton toolButton;
    private javax.swing.JMenuItem toolOptions;
    private javax.swing.JPanel toolbarPanel;
    private javax.swing.JButton zoomInButton;
    private javax.swing.JButton zoomOutButton;
    // End of variables declaration//GEN-END:variables
}
