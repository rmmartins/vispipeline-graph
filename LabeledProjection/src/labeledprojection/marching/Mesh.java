/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.marching;

import graph.model.Edge;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import labeledprojection.model.LabeledProjectionInstance;
import labeledprojection.model.LabeledProjectionModel;
import labeledprojection.util.LabeledProjectionConstants;
import labeledprojection.util.ProjectionConnectivity;
import projection.model.Scalar;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author Jose Gustavo
 */
public class Mesh {

    public ArrayList<MarchingTriangle> triangles;

    public Mesh() {
        triangles = new ArrayList<MarchingTriangle>();
    }

    public Mesh(LabeledProjectionModel model) {
        triangles = new ArrayList<MarchingTriangle>();
        ProjectionConnectivity dc = (ProjectionConnectivity) model.getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY);
        for (int i=0;i<dc.getEdges().size();i++) {
            Edge edge = dc.getEdges().get(i);
            ArrayList<Integer> common = getCommonVertices(dc,model.getInstanceById(edge.getSource()),model.getInstanceById(edge.getTarget()));
            if (common != null && !common.isEmpty())
                for (int j=0;j<common.size();j++) {
                    MarchingTriangle mt = new MarchingTriangle(model.getInstanceById(edge.getSource()),
                                                               model.getInstanceById(edge.getTarget()),
                                                               model.getInstanceById(common.get(j)));
                    triangles.add(mt);
                }
        }
        //Excluding repeated triangles...
        for (int i=0;i<triangles.size();i++) {
            for (int j=i+1;j<triangles.size();j++) {
                if (triangles.get(i).isEqual(triangles.get(j))) {
                    triangles.remove(j);
                    j--;
                }
            }
        }
    }

    public Mesh(ProjectionConnectivity dc, ArrayList instances) {
        triangles = new ArrayList<MarchingTriangle>();
        for (int i=0;i<dc.getEdges().size();i++) {
            Edge edge = dc.getEdges().get(i);
            LabeledProjectionInstance source = getInstanceById(edge.getSource(),instances);
            LabeledProjectionInstance target = getInstanceById(edge.getTarget(),instances);
            ArrayList<Integer> commons = getCommonVertices(dc,source,target);
            if (commons != null && !commons.isEmpty())
                for (int j=0;j<commons.size();j++) {
                    LabeledProjectionInstance common = getInstanceById(commons.get(j),instances);
                    MarchingTriangle mt = new MarchingTriangle(source,target,common);
                    triangles.add(mt);
                }
        }
        //Excluding repeated triangles...
        for (int i=0;i<triangles.size();i++) {
            for (int j=i+1;j<triangles.size();j++) {
                if (triangles.get(i).isEqual(triangles.get(j))) {
                    triangles.remove(j);
                    j--;
                }
            }
        }
    }

    private LabeledProjectionInstance getInstanceById(int id, ArrayList<AbstractInstance> instances) {
        for (int i=0;i<instances.size();i++)
            if (instances.get(i).getId() == id)
                return (LabeledProjectionInstance) instances.get(i);
        return null;
    }

    private ArrayList<Integer> getCommonVertices(ProjectionConnectivity dc, LabeledProjectionInstance v1, LabeledProjectionInstance v2) {
        if (v1 == null || v2 == null) return null;
        ArrayList<Integer> retV1 = new ArrayList<Integer>();
        ArrayList<Integer> retV2 = new ArrayList<Integer>();
        //Pegando todos os edges nos quais v1 e v2 participam...
        for (int i=0;i<dc.getEdges().size();i++) {
            Edge edge = dc.getEdges().get(i);
            if (edge.getSource() == v1.getId())
                retV1.add(edge.getTarget());
            else if (edge.getTarget() == v1.getId())
                retV1.add(edge.getSource());

            if (edge.getSource() == v2.getId())
                retV2.add(edge.getTarget());
            else if (edge.getTarget() == v2.getId())
                retV2.add(edge.getSource());
        }

        ArrayList<Integer> commons = new ArrayList<Integer>();
        //Armazenando todos os vertices que sao comuns em arestas dos dois conjuntos...

        for (int i=0;i<retV1.size();i++)
            if (retV2.contains(retV1.get(i)))
                commons.add(retV1.get(i));

        return commons;
    }

    public void draw(BufferedImage image,Scalar scalar,float boundary,boolean selected) {
        for (int i=0;i<triangles.size();i++) {
            triangles.get(i).drawContour(image,scalar,boundary,selected);
        }
    }

    public float getLevelSelected(Point point, Scalar scalar) {
        int ret = -1;
        for (int i=0;i<triangles.size();i++) {
            if (triangles.get(i).isInside(point)) {
                float c1 = triangles.get(i).getVertice(0).getScalarValue(scalar);
                float c2 = triangles.get(i).getVertice(1).getScalarValue(scalar);
                float c3 = triangles.get(i).getVertice(2).getScalarValue(scalar);
                if (c1 == c2 && c2 == c3) return c1;
                else if (c1 != c2 && c2 != c3) return -1.0f;
                else if (c1 == c2) return c1;
                else if (c2 == c3) return c2;
                else return c3;
            }
        }
        return ret;
    }

    public float getArea() {
        //The area will be the sum of the area of all the triangles of this mesh...
        float sumAreas = 0;
        for (int i=0;i<triangles.size();i++)
            sumAreas += triangles.get(i).getArea();
        return sumAreas;
    }

    public float getNormalizedArea() {
        //The area will be the sum of the area of all the triangles of this mesh...
        float minX = Float.MAX_VALUE, maxX = Float.MIN_VALUE;
        float minY = Float.MAX_VALUE, maxY = Float.MIN_VALUE;

        for (int i=0;i<this.triangles.size();i++) {
            for (int j=0;j<3;j++) {
                if (triangles.get(i).getVertice(j).getX() < minX) minX = triangles.get(i).getVertice(j).getX();
                if (triangles.get(i).getVertice(j).getX() > maxX) maxX = triangles.get(i).getVertice(j).getX();
                if (triangles.get(i).getVertice(j).getY() < minY) minY = triangles.get(i).getVertice(j).getY();
                if (triangles.get(i).getVertice(j).getY() > maxY) maxY = triangles.get(i).getVertice(j).getY();
            }
        }

        float sumAreas = 0;
        for (int i=0;i<triangles.size();i++)
            sumAreas += triangles.get(i).getNormalizedArea(minX, maxX, minY, maxY);
        return sumAreas;
    }

    public int getNumTriangles() {
        return triangles.size();
    }
}
