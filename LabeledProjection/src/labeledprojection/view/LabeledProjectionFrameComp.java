/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.view;

import detailedMatrix.DetailedMatrix;
import distance.DetailedDistanceMatrix;
import distance.DistanceMatrix;
import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import labeledprojection.coordination.ClassMatchCoordination;
import labeledprojection.model.LabeledProjectionModel;
import labeledprojection.util.ImageCollection;
import matrix.AbstractMatrix;
import projection.model.Scalar;
import textprocessing.corpus.Corpus;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Projection.View",
name = "Labeled Projection View Frame",
description = "Display a labeled projection model.")
public class LabeledProjectionFrameComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (model != null) {
            LabeledProjectionFrame frame = new LabeledProjectionFrame();
            frame.setSize(600, 600);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
            frame.setTitle(title);
            frame.setModel(model);
            if (coordinators != null) {
                for (int i = 0; i < coordinators.size(); i++) {
                    frame.addCoordinator(coordinators.get(i));
                    if (coordinators.get(i) instanceof ClassMatchCoordination) {
                        Scalar sc = ((ClassMatchCoordination)coordinators.get(i)).match();
                    }
                }
            }
            //Setting report data...
            ((ReportView)frame.getReportPanel()).clean();
            boolean isText = model.getInstances().get(0).toString().contains(".txt");
            ImageCollection im = null;
            Corpus c = null;
            if (!isText) im = model.getImageCollection();
            else c = model.getCorpus();
            if (im != null) ((ReportView)frame.getReportPanel()).setDataSource(im.getFilename());
            else if (c != null) ((ReportView)frame.getReportPanel()).setDataSource(c.getUrl());
            ((ReportView)frame.getReportPanel()).setObjects(model.getInstances().size());

            ((ReportView)frame.getReportPanel()).setType(model.getType());

            if (matrix != null && matrix instanceof DetailedMatrix) {
                if (matrix.getRowCount() == model.getInstances().size()) {
                    ((ReportView)frame.getReportPanel()).setSource(((DetailedMatrix)matrix).getSource());
                    ((ReportView)frame.getReportPanel()).setDimensions(matrix.getDimensions());
                }
            }else if (dmat != null && dmat instanceof DetailedDistanceMatrix) {
                if (dmat.getElementCount() == model.getInstances().size()) {
                    ((ReportView)frame.getReportPanel()).setSource(((DetailedDistanceMatrix)dmat).getSource());
                }
            }
            //Setting class matching informations...
//            ((ReportView)frame.getReportPanel()).setClassMatchingPanel(model.getInstances().size(),
//                                  ((LabeledProjectionModel)model).getNumMatching(),
//                                  ((LabeledProjectionModel)model).getNumNonMatching(),
//                                  ((LabeledProjectionModel)model).getNumNonCorresponding());
        } else {
            throw new IOException("A projection model should be provided.");
        }
    }

    public void input(@Param(name = "Original Distance Matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public void input(@Param(name = "Original Data Matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "Labeled projection model") LabeledProjectionModel model) {
        this.model = model;
    }

    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
          if (paramview == null) {
            paramview = new LabeledProjectionFrameParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        model = null;
        coordinators = null;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle () {
        return title;
    }

    public void setSize(Dimension size) {
        this.size = size;
    }

    public Dimension getSize() {
        return size;
    }

    public static final long serialVersionUID = 1L;
    private String title = "";
    private Dimension size = new Dimension(600, 600);
    private transient LabeledProjectionModel model;
    private transient LabeledProjectionFrameParamView paramview;
    private transient ArrayList<AbstractCoordinator> coordinators;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
}
