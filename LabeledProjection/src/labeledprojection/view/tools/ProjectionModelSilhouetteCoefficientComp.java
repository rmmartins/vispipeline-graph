package labeledprojection.view.tools;

import labeledprojection.view.tools.silhouette.DataSilhouetteCoefficient;
import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import labeledprojection.model.LabeledProjectionModel;
import matrix.AbstractMatrix;
import projection.model.Scalar;
import projection.util.ProjectionUtil;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Data Analysis.Silhouette",
name = "Labeled Projection Model Silhouette Coefficient",
description = "Display the silhouette coefficient of a clustered dataset.")
public class ProjectionModelSilhouetteCoefficientComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {

        AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
        DataSilhouetteCoefficient sc = new DataSilhouetteCoefficient();
        AbstractMatrix matrix = null;
        DistanceMatrix dmat = null;
        float[] silhouette = null;
        
        if (model != null) {
            
            Scalar scalar = model.getScalarByName("cdata");
            if (scalar == null)
                if (model.getScalars() != null && !model.getScalars().isEmpty())
                    scalar = model.getScalars().get(0);
            matrix = ProjectionUtil.modelToMatrix(model,scalar);
            silhouette = sc.execute(matrix,diss);
            
            String result = "";
            if (silhouette != null) {
                //showing the average silhouette on console
                float average = sc.average(silhouette);
                result = "Silhouette coefficient:\r\n";
                result += "Original Clustering: " + average + "\r\n";
                result += "---\r\n\n";
            }else {
                result += "Error generating Silhouette coefficient.";
            }
            ProjectionModelSilhouetteCoefficientConsole.getInstance(null).display(title,result);
        }
    }

    public void input(@Param(name = "Labeled Projection Model") LabeledProjectionModel model) {
        this.model = model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
          if (paramview == null) {
            paramview = new ProjectionModelSilhouetteCoefficientParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType t) {
        disstype = t;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String t) {
        title = t;
    }

    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private String title = "";
    public static final long serialVersionUID = 1L;
    private transient LabeledProjectionModel model;
    private transient ProjectionModelSilhouetteCoefficientParamView paramview;
        
}
