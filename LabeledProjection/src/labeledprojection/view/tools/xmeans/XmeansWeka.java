/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labeledprojection.view.tools.xmeans;

import datamining.clustering.Clustering;
import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import weka.clusterers.XMeans;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;


/**
 *
 * @author thiago
 */
public class XmeansWeka extends Clustering {

    private int minClusters;
    private int maxClusters;
    private int clustersFinal;
    private XMeans xmeans;

    public XmeansWeka(int minClusters, int maxClusters) {
        super(minClusters);
        this.minClusters = minClusters;
        this.maxClusters = maxClusters;
        
        xmeans = new XMeans();
        xmeans.setMinNumClusters(minClusters);
        xmeans.setMaxNumClusters(maxClusters);
        xmeans.setUseKDTree(true);
        System.out.print("CutOffFactor: " + xmeans.getCutOffFactor());
        //xmeans.setDebugLevel(2);
    }

    public Instances transformAbstractMatrixInstances(AbstractMatrix matrix) {
        Instances instances = null;
        if(matrix != null) {
            ArrayList<Attribute> labels = transformeLabelsAttributes(matrix.getDimensions());
            instances = new Instances("AbstractMatrix", labels, nrclusters);
            Instance inst = null;
            for(int i = 0; i < matrix.getRowCount(); i++) {
                inst = new DenseInstance(1, transformeFloatArrayToDoubleArray(matrix.getRow(i).toArray()));
                instances.add(inst);
            }
        }
        return instances;
    }
    
    private double[] transformeFloatArrayToDoubleArray(float[] array) {
        double[] values = null;
        if(array != null) {
            values = new double[array.length];
            for(int i = 0; i < array.length; i++)
                values[i] = array[i];
        }
        return values;
    }
    
    private ArrayList<Attribute> transformeLabelsAttributes(int size) {
        ArrayList<Attribute> att = null;
        if(size > 0) {
            att = new ArrayList<Attribute>(size);
            for(int i = 0; i < size; i++)
                att.add(new Attribute("Att"+i));
        }
        return att;
    }


    @Override
    public ArrayList<ArrayList<Integer>> execute(AbstractDissimilarity diss, AbstractMatrix matrix) throws IOException {
        
        ArrayList<ArrayList<Integer>> list = null;
        Instances inst = transformAbstractMatrixInstances(matrix);
        try {
            //for(int i = minClusters; i < maxClusters; i++) {
            xmeans.buildClusterer(inst);
            //System.out.println("--Processando x-means: " + i + " - Bin Value: " +xmeans.getBinValue() );
            //}
            Instances centers = xmeans.getClusterCenters();
            list = new ArrayList<ArrayList<Integer>>(centers.size());
            for(int i = 0; i < centers.numInstances(); i++)
                list.add(new ArrayList<Integer>());
            for(int i = 0; i < inst.size(); i++) {
                int idClusters = xmeans.clusterInstance(inst.instance(i));
                list.get(idClusters).add(i);
            }
                    
        } catch (Exception ex) {
            Logger.getLogger(XmeansWeka.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return list;
    }
    
    public int getClusters() {
        return xmeans.getClusterCenters().size();
    }

    public AbstractMatrix getCentroids() {

        AbstractMatrix ret = new DenseMatrix();
        Instances centers = xmeans.getClusterCenters();
        for (int i=0;i<centers.size();i++) {
            Instance ins = centers.get(i);
            float[] values = new float[ins.numValues()];
            for (int j=0;j<ins.numValues();j++)
                values[j] = (float)ins.value(j);
            AbstractVector row = new DenseVector(values);
            ret.addRow(row);
        }
        return ret;
    }

    @Override
    public ArrayList<ArrayList<Integer>> execute(DistanceMatrix dmat) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
