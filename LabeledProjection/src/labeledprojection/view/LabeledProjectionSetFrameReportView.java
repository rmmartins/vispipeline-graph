/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.view;

/**
 *
 * @author Jose Gustavo
 */
public class LabeledProjectionSetFrameReportView extends ReportView {

    public LabeledProjectionSetFrameReportView() {
        super();
        initComponents();
    }

    private void initComponents() {

        java.awt.GridBagConstraints gridBagConstraints;

        perturbingPanel = new javax.swing.JPanel();
        approximatingLabel = new javax.swing.JLabel();
        distancingLabel = new javax.swing.JLabel();
        distancingTextField = new javax.swing.JTextField();
        approximatingTextField = new javax.swing.JTextField();

        perturbingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Perturbing Parameters"));
        perturbingPanel.setLayout(new java.awt.GridBagLayout());

        approximatingLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        approximatingLabel.setText("% Approximating Factor : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        perturbingPanel.add(approximatingLabel, gridBagConstraints);

        distancingLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        distancingLabel.setText("% Distancing Factor : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        perturbingPanel.add(distancingLabel, gridBagConstraints);

        distancingTextField.setEditable(false);
        distancingTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        distancingTextField.setMinimumSize(new java.awt.Dimension(50, 20));
        distancingTextField.setPreferredSize(new java.awt.Dimension(50, 20));
        distancingTextField.setRequestFocusEnabled(false);
        
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        perturbingPanel.add(distancingTextField, gridBagConstraints);

        approximatingTextField.setEditable(false);
        approximatingTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        approximatingTextField.setMinimumSize(new java.awt.Dimension(50, 20));
        approximatingTextField.setPreferredSize(new java.awt.Dimension(50, 20));
        approximatingTextField.setRequestFocusEnabled(false);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        perturbingPanel.add(approximatingTextField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(perturbingPanel, gridBagConstraints);
        perturbingPanel.setVisible(true);

    }

    public void setApproximatingFactor(String f) {
        approximatingTextField.setText(f);
    }

    public void setDistancingFactor(String f) {
        distancingTextField.setText(f);
    }

    private javax.swing.JPanel perturbingPanel;
    private javax.swing.JLabel approximatingLabel;
    private javax.swing.JTextField approximatingTextField;
    private javax.swing.JLabel distancingLabel;
    private javax.swing.JTextField distancingTextField;

}
