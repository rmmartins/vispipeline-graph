/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.view.selection;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import labeledprojection.model.LabeledProjectionInstance;
import labeledprojection.model.LabeledProjectionModel;
import projection.model.Scalar;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;
import visualizationbasics.view.selection.AbstractSelection;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class SameClassSelection extends AbstractSelection {

    public SameClassSelection(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {

        if (viewer.getModel() != null) {
            if (selinst != null) {
                ArrayList<Float> classesSelected = new ArrayList<Float>();
                //Selecting everyone of the same class than selinst
                Scalar sc = ((LabeledProjectionModel)viewer.getModel()).getSelectedScalar();
                if (sc != null) {
                ArrayList<AbstractInstance> selinst2 = new ArrayList<AbstractInstance>();
                for (int i=0;i<selinst.size();i++)
                    //Creating set of selected classes...
                    if (!classesSelected.contains(((LabeledProjectionInstance)selinst.get(i)).getScalarValue(sc)))
                        classesSelected.add(((LabeledProjectionInstance)selinst.get(i)).getScalarValue(sc));

                    ArrayList<AbstractInstance> instances = ((LabeledProjectionModel)viewer.getModel()).getInstances();

                    //Creating set with all the instances of the selected classes
                    for (int i=0;i<instances.size();i++) {
                        LabeledProjectionInstance lpi = (LabeledProjectionInstance)instances.get(i);
                        if (classesSelected.contains(lpi.getScalarValue(sc)))
                            selinst2.add(lpi);
                    }
                    viewer.getModel().setSelectedInstances(selinst2);
                    for (int i=0;i<viewer.getCoordinators().size();i++) {
                        viewer.getCoordinators().get(i).coordinate(selinst,null);
                    }
                    viewer.getModel().notifyObservers();
                }
            }
        }
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/AlignJustifyHorizontal16.gif"));
    }

    @Override
    public String toString() {
        return "Same Class Selection";
    }

}
