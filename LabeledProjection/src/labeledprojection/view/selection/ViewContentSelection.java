/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.view.selection;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import visualizationbasics.view.selection.AbstractSelection;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import labeledprojection.model.LabeledProjectionInstance;
import labeledprojection.model.LabeledProjectionModel;
import labeledprojection.util.ImageCollection;
import labeledprojection.util.LabeledProjectionConstants;
import labeledprojection.util.ProjectionOpenDialog;
import labeledprojection.view.LabeledProjectionFrame;
import labeledprojection.view.ReportView;
import textprocessing.corpus.Corpus;
import textprocessing.processing.view.TextOpenDialog;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.filter.ZIPFilter;
import visualizationbasics.view.ModelViewer;
import labeledprojection.view.forms.MultipleImageView;
import textprocessing.processing.view.MultipleFileView;



/**
 *
 * @author Fernando Vieira Paulovich
 */
public class ViewContentSelection extends AbstractSelection {

    public ViewContentSelection(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if ((selinst == null)||(selinst.isEmpty())) return;
        if (viewer.getModel() != null) {
            viewer.getModel().setSelectedInstances(selinst);
            boolean isText = ((LabeledProjectionInstance)selinst.get(0)).toString().contains(".txt");
            ImageCollection im = null;
            Corpus c = null;
            if (!isText) im = ((LabeledProjectionModel)viewer.getModel()).getImageCollection();
            else c = ((LabeledProjectionModel)viewer.getModel()).getCorpus();

            if (!isText) {
                if ((im != null)&&(ProjectionOpenDialog.checkImages(im,viewer.getModel()))) {
//                    MultipleImageView.getInstance(viewer).display(im, selinst,viewer, false);
                    MultipleImageView.getInstance((JFrame)viewer.getContainer()).display(im,selinst);
                }else {
                    try {
                        PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
                        im = ProjectionOpenDialog.openImages(spm,new ZIPFilter(),viewer.getContainer());
                        if (ProjectionOpenDialog.checkImages(im,viewer.getModel())) {
                            ((LabeledProjectionModel)viewer.getModel()).setImageCollection(im);
                            ((ReportView)(((LabeledProjectionFrame)viewer).getReportPanel())).setDataSource(im.getFilename());
                            //MultipleImageView.getInstance(viewer).display(im, selinst, viewer, false);
                            MultipleImageView.getInstance((JFrame) viewer.getContainer()).display(im,selinst);
                        }else {
                            String message = "There are non-correponding files on the chosen images file. Would like to proceed?";
                            int answer = JOptionPane.showOptionDialog(viewer.getContainer(),message,"Openning Warning",
                                                                      JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE,null,null,null);
                            if (answer == JOptionPane.YES_OPTION) {
                                //MultipleImageView.getInstance(viewer).display(im, selinst, viewer, false);
                                MultipleImageView.getInstance((JFrame) viewer.getContainer()).display(im,selinst);
                            }
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(ViewContentSelection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else {
                if ((c != null)&&(TextOpenDialog.checkCorpus(c,viewer.getModel()))) {
                    MultipleFileView.getInstance((JFrame) viewer.getContainer()).display(c,selinst);
                    //NewMultipleFileView.getInstance((JFrame) viewer.getContainer()).display(c,selinst);
                }else {
                    try {
                        PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
                        c = TextOpenDialog.openCorpus(spm,new ZIPFilter(),viewer.getContainer());
                        if (TextOpenDialog.checkCorpus(c,viewer.getModel())) {
                            ((LabeledProjectionModel)viewer.getModel()).setCorpus(c);
                            MultipleFileView.getInstance((JFrame) viewer.getContainer()).display(c,selinst);
                            //NewMultipleFileView.getInstance((JFrame) viewer.getContainer()).display(c,selinst);
                        }else {
                            String message = "There are non-correponding files on the chosen file. Would like to proceed?";
                            int answer = JOptionPane.showOptionDialog(viewer.getContainer(),message,"Openning Warning",
                                                                      JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE,null,null,null);
                            if (answer == JOptionPane.YES_OPTION) {
                                MultipleFileView.getInstance((JFrame) viewer.getContainer()).display(c,selinst);
                                //NewMultipleFileView.getInstance((JFrame) viewer.getContainer()).display(c,selinst);
                            }
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(ViewContentSelection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

//            if ((im == null)||(!ImageOpenDialog.checkImages(im,viewer.getModel()))) {
//                try {
//                    PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
//                    im = ImageOpenDialog.openImages(spm,new ZIPFilter(), viewer);
//                    if (ImageOpenDialog.checkImages(im,viewer.getModel())) {
//                        ((ProjectionModel)viewer.getModel()).setImageCollection(im);
//                        MultipleImageView.getInstance(viewer).display(im, selinst);
//                    }else {
//                        String message = "There are non-correponding files on the chosen images file. Would like to proceed?";
//                        int answer = JOptionPane.showOptionDialog(viewer,message,"Openning Warning",
//                                                                  JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE,null,null,null);
//                        if (answer == JOptionPane.YES_OPTION) {
//                            MultipleImageView.getInstance(viewer).display(im, selinst);
//                        }
//                    }
//                } catch (IOException ex) {
//                    Logger.getLogger(ViewContentSelection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }else {
//                MultipleImageView.getInstance(viewer).display(im, selinst);
//            }
            viewer.getModel().notifyObservers();
        }
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Copy16.gif"));
    }

    @Override
    public String toString() {
        return "View Content";
    }

}
