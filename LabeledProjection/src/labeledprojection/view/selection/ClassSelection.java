/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.view.selection;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import labeledprojection.model.LabeledProjectionInstance;
import labeledprojection.model.LabeledProjectionModel;
import projection.model.ProjectionModel;
import projection.model.Scalar;
import labeledprojection.view.LabeledProjectionFrame;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class ClassSelection extends ClassAbstractSelection {

    private Scalar ncdata = null;
    private float indexCdata = 0.0f;
    private int indexScalar;


    public ClassSelection(ModelViewer viewer) {
        super(viewer);
    }

    public boolean isElement(ArrayList<Scalar> scalars,String name) {
        if (scalars == null || scalars.isEmpty()) return false;
        for (int i=0;i<scalars.size();i++)
            if (scalars.get(i).getName().equalsIgnoreCase(name))
                return true;
        return false;
    }

//    protected void setClass(ArrayList<AbstractInstance> selinst) {
//        //Verifying if the scalar was deleted...
//        if (selinst == null || selinst.isEmpty()) return;
//        if (((ProjectionModel)viewer.getModel()).getScalars().indexOf(ncdata)==-1) ncdata = null;
//
//        if (ncdata == null) {
//            Scalar oldScalar = ((LabeledProjectionModel)viewer.getModel()).getSelectedScalar(); //ScalarByName("cdata");
//            while (isElement(((ProjectionModel)viewer.getModel()).getScalars(),"Pseudo Class "+indexScalar))
//                indexScalar++;
//            ncdata = ((ProjectionModel)viewer.getModel()).addScalar("Pseudo Class "+indexScalar);
//            indexScalar++;
//            indexCdata = -1;
////            for (int i=0;i<viewer.getModel().getInstances().size();i++) {
////                LabeledProjectionInstance pi = (LabeledProjectionInstance)viewer.getModel().getInstances().get(i);
////                pi.setPseudoColor(ncdata.getName(),Color.WHITE);
////                pi.setScalarValue(ncdata,indexCdata);
////            }
////            indexCdata++;
//
//            for (int i=0;i<viewer.getModel().getInstances().size();i++) {
//                LabeledProjectionInstance pi = (LabeledProjectionInstance)viewer.getModel().getInstances().get(i);
//                pi.setPseudoColor(ncdata.getName(),pi.getColor());
//                if (oldScalar != null)
//                    pi.setScalarValue(ncdata,pi.getScalarValue(oldScalar));
//            }
//            //indexCdata++;
//            
//            //Setting all the instances with class -1 of this scalar...
////            for (int i=0;i<viewer.getModel().getInstances().size();i++) {
////                ProjectionInstance pi = (ProjectionInstance) viewer.getModel().getInstances().get(i);
////                pi.setScalarValue(ncdata,indexCdata);
////            }
////            indexCdata++;
//        }
//        //Setting just the selected instances with the scalar...
//        Color color = javax.swing.JColorChooser.showDialog(null,"Choose the Instance(s) Color", java.awt.Color.BLACK);
//
//        float maxValue = ncdata.getMax();
//
//        if (color != null) {
//            for (int i=0;i<selinst.size();i++) {
//                LabeledProjectionInstance pi = (LabeledProjectionInstance) selinst.get(i);
//                pi.setPseudoColor(ncdata.getName(),color);
//                pi.setScalarValue(ncdata,maxValue+1);
//            }
//            //indexCdata++;
//        }else {
////            for (int i=0;i<selinst.size();i++) {
////                ProjectionInstance pi = (ProjectionInstance) selinst.get(i);
////                pi.setScalarValue(ncdata,indexCdata);
////            }
////            indexCdata++;
//        }
//
//
////        LabeledColorTable ct = (LabeledColorTable) ((LabeledProjectionModel)viewer.getModel()).getColorTable();
////        if (ct != null) {
////            ArrayList<Color> colors = new ArrayList<Color>();
////            int minindex = (int) ((ct.colorScale.getNumberColors() - 1) * ct.colorScale.getMin());
////            int maxindex = (int) ((ct.colorScale.getNumberColors() - 1) * ct.colorScale.getMax());
////            for (int i=0;i<ct.colorScale.getNumberColors();i++)
////                colors.add(ct.getColor((float)(i-minindex)/(maxindex-minindex)));
////            ColorsDialog.getInstance(null).display(colors);
////        }
//        
//
//
//
////        if (color == null) {
////            for (int i=0;i<selinst.size();i++) {
////                ProjectionInstance pi = (ProjectionInstance) selinst.get(i);
////                pi.setScalarValue(ncdata,indexCdata);
////            }
////            indexCdata++;
////        }else {
////            LabeledColorTable ct = (LabeledColorTable) ((LabeledProjectionModel)viewer.getModel()).getColorTable();
////
////            if (ct != null) {
////                ArrayList<Color> colors = new ArrayList<Color>();
////                int minindex = (int) ((ct.colorScale.getNumberColors() - 1) * ct.colorScale.getMin());
////                int maxindex = (int) ((ct.colorScale.getNumberColors() - 1) * ct.colorScale.getMax());
////                for (int i=0;i<ct.colorScale.getNumberColors();i++)
////                    colors.add(ct.getColor((float)(i-minindex)/(maxindex-minindex)));
////
////
////
////                for (int i=0;i<ct.colorScale.getNumberColors();i++) {
////                    if (compareColors(((LabeledColorScale)ct.colorScale).getColorByIndex(i),color)) {
////                        for (int j=0;j<selinst.size();j++) {
////                            ProjectionInstance pi = (ProjectionInstance) selinst.get(j);
////                            pi.setScalarValue(ncdata,(float)i);
////                        }
////                    }
////                }
////            }
////        }
//    }

    protected void setClass(ArrayList<AbstractInstance> selinst) {
        //Verifying if the scalar was deleted...
        if (((LabeledProjectionModel)viewer.getModel()).getScalars().indexOf(ncdata)==-1) ncdata = null;

        if (ncdata == null) {
            while (isElement(((LabeledProjectionModel)viewer.getModel()).getScalars(),"New Class "+indexScalar))
                indexScalar++;
            ncdata = ((LabeledProjectionModel)viewer.getModel()).addScalar("New Class "+indexScalar);
            indexScalar++;
            indexCdata = 0;
            //Setting all the instances with class 0 of this scalar...
            for (int i=0;i<viewer.getModel().getInstances().size();i++) {
                LabeledProjectionInstance pi = (LabeledProjectionInstance) viewer.getModel().getInstances().get(i);
                pi.setScalarValue(ncdata,indexCdata);
            }
            indexCdata++;
        }
        //Setting just the selected instances with the scalar...
        for (int i=0;i<selinst.size();i++) {
            LabeledProjectionInstance pi = (LabeledProjectionInstance) selinst.get(i);
            pi.setScalarValue(ncdata,indexCdata);
        }
        indexCdata++;
    }
    
    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (viewer.getModel() != null) {
            setClass(selinst);
            ((LabeledProjectionFrame)viewer).updateScalars(getNcdata());
            viewer.getModel().setSelectedInstances(selinst);
            viewer.getModel().notifyObservers();
        }
    }

    public void setNcdata(Scalar c) {
        ncdata = c;
    }

    public Scalar getNcdata() {
        return ncdata;
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/AlignCenter16.gif"));
    }

    @Override
    public String toString() {
        return "Class Selection";
    }

    @Override
    public void reset() {
        ncdata = null;
        indexScalar = 1;
    }

}
