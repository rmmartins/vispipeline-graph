package labeledprojection.view;

import distance.DetailedDistanceMatrix;
import distance.DistanceMatrix;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import matrix.AbstractMatrix;
import detailedMatrix.dense.DetailedDenseMatrix;
import labeledprojection.model.LabeledProjectionModel;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Projection.View",
name = "Projection Set View Frame",
description = "Display a set of projection models.")
public class LabeledProjectionSetFrameComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if ((models != null)&&(!models.isEmpty())) {
            LabeledProjectionSetFrame frame = new LabeledProjectionSetFrame(models.size());
            frame.setSize(600, 600);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
            frame.setTitle(title);
            frame.setModels(models);
            if (coordinators != null) {
                for (int k=0;k<coordinators.size();k++) {
                    frame.addCoordinator(coordinators.get(k));
                }
            }
            if (originalMatrix != null && originalMatrix instanceof DetailedDenseMatrix) {
                if (originalMatrix.getRowCount() == ((LabeledProjectionModel)models.get(0)).getInstances().size()) {
                    ((LabeledProjectionSetFrameReportView)frame.getReportPanel()).setSource(((DetailedDenseMatrix)originalMatrix).getSource());
                    ((LabeledProjectionSetFrameReportView)frame.getReportPanel()).setDimensions(originalMatrix.getDimensions());
                }
            }else if (originalDmat != null && originalDmat instanceof DetailedDistanceMatrix) {
                if (originalDmat.getElementCount() == ((LabeledProjectionModel)models.get(0)).getInstances().size()) {
                    ((LabeledProjectionSetFrameReportView)frame.getReportPanel()).setSource(((DetailedDistanceMatrix)originalDmat).getSource());
                }else ((LabeledProjectionSetFrameReportView)frame.getReportPanel()).setSource(null);
                ((LabeledProjectionSetFrameReportView)frame.getReportPanel()).setDimensions(0);
            }else {
                ((LabeledProjectionSetFrameReportView)frame.getReportPanel()).setSource(null);
                ((LabeledProjectionSetFrameReportView)frame.getReportPanel()).setDimensions(0);
            }
        } else {
            throw new IOException("A projection model set should be provided.");
        }
    }

    public void input(@Param(name = "Original Distance Matrix") DistanceMatrix dmat) {
        this.originalDmat = dmat;
    }

    public void input(@Param(name = "Original Data Matrix") AbstractMatrix matrix) {
        this.originalMatrix = matrix;
    }

    public void input(@Param(name = "projection models set") ArrayList<LabeledProjectionModel> models) {
        this.models = models;
    }

    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
          if (paramview == null) {
            ///paramview = new TreeFrameParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        models = null;
        coordinators = null;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle () {
        return title;
    }

    public static final long serialVersionUID = 1L;
    private String title = "";    
    private transient ArrayList<LabeledProjectionModel> models;
    private transient LabeledProjectionFrameParamView paramview;
    private transient ArrayList<AbstractCoordinator> coordinators;
    private transient AbstractMatrix originalMatrix;
    private transient DistanceMatrix originalDmat;
}
