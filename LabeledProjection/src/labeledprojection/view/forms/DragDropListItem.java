/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.view.forms;

import java.awt.Color;

/**
 *
 * @author Jose Gustavo
 */
public class DragDropListItem {

    public int index;
    public Color color;

    public DragDropListItem(int i, Color c) {
        index = i;
        color = c;
    }

}
