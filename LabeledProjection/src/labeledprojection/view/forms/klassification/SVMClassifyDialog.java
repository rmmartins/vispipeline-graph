/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.view.forms.klassification;

import classifiers.svm.SVMComp;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;
import labeledprojection.model.LabeledProjectionModel;
import projection.model.ProjectionInstance;
import projection.model.Scalar;
import visualizationbasics.model.AbstractModel;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class SVMClassifyDialog extends ClassifyDialog {

    public SVMClassifyDialog(JFrame parent,AbstractModel model) {
        super(parent,model);
        initComponents();
    }

    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;
        svmPanel = new javax.swing.JPanel();
        svmTypeLabel = new javax.swing.JLabel();
        svmTypeComboBox = new javax.swing.JComboBox();
        coeffLabel = new javax.swing.JLabel();
        costLabel = new javax.swing.JLabel();
        degreeKernelLabel = new javax.swing.JLabel();
        epsLabel = new javax.swing.JLabel();
        gammaLabel = new javax.swing.JLabel();
        kernelTypeLabel = new javax.swing.JLabel();
        nuLabel = new javax.swing.JLabel();
        kernelTypeComboBox = new javax.swing.JComboBox();
        coeffTextField = new javax.swing.JTextField();
        degreeKernelTextField = new javax.swing.JTextField();
        costTextField = new javax.swing.JTextField();
        epsTextField = new javax.swing.JTextField();
        gammaTextField = new javax.swing.JTextField();
        nuTextField = new javax.swing.JTextField();
        shrinkingCheckBox = new javax.swing.JCheckBox();
        normalizeCheckBox = new javax.swing.JCheckBox();

        svmPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("SVM Properties"));
        svmPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        svmPanel.setLayout(new java.awt.GridBagLayout());

        svmTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        svmTypeLabel.setText("SVM Type : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        svmPanel.add(svmTypeLabel, gridBagConstraints);

        svmTypeComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "C-SVC", "nu-SVC" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        svmPanel.add(svmTypeComboBox, gridBagConstraints);

        coeffLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        coeffLabel.setText("Coefficient : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        svmPanel.add(coeffLabel, gridBagConstraints);

        costLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        costLabel.setText("Cost : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        svmPanel.add(costLabel, gridBagConstraints);

        degreeKernelLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        degreeKernelLabel.setText("Degree of the Kernel :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        svmPanel.add(degreeKernelLabel, gridBagConstraints);

        epsLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        epsLabel.setText("Tolerance of Termination Criterion (eps) : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        svmPanel.add(epsLabel, gridBagConstraints);

        gammaLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        gammaLabel.setText("Gamma : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        svmPanel.add(gammaLabel, gridBagConstraints);

        kernelTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        kernelTypeLabel.setText("Kernel Type : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        svmPanel.add(kernelTypeLabel, gridBagConstraints);

        nuLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        nuLabel.setText("nu Value : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        svmPanel.add(nuLabel, gridBagConstraints);

        kernelTypeComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "linear: (u*v)", "polynomial: (gamma*u'*v+coefficient)^degree", "radial basis function: exp(-gamma*|u-v|^2)", "sigmoid: tanh(gamma*u'*v+coefficient)" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        svmPanel.add(kernelTypeComboBox, gridBagConstraints);

        coeffTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        coeffTextField.setText("0.0");
        coeffTextField.setMinimumSize(new java.awt.Dimension(10, 20));
        coeffTextField.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        svmPanel.add(coeffTextField, gridBagConstraints);

        degreeKernelTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        degreeKernelTextField.setText("3");
        degreeKernelTextField.setPreferredSize(new java.awt.Dimension(50, 20));

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        svmPanel.add(degreeKernelTextField, gridBagConstraints);

        costTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        costTextField.setText("1.0");
        costTextField.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        svmPanel.add(costTextField, gridBagConstraints);

        epsTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        epsTextField.setText("0.0010");
        epsTextField.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        svmPanel.add(epsTextField, gridBagConstraints);

        gammaTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        gammaTextField.setText("0.0");
        gammaTextField.setPreferredSize(new java.awt.Dimension(50, 20));

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        svmPanel.add(gammaTextField, gridBagConstraints);

        nuTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        nuTextField.setText("0.5");
        nuTextField.setPreferredSize(new java.awt.Dimension(50, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        svmPanel.add(nuTextField, gridBagConstraints);

        shrinkingCheckBox.setSelected(true);
        shrinkingCheckBox.setText("Use Shrinking heuristic");
        shrinkingCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        shrinkingCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        svmPanel.add(shrinkingCheckBox, gridBagConstraints);

        normalizeCheckBox.setSelected(true);
        normalizeCheckBox.setText("Normalize data");
        normalizeCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        normalizeCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        svmPanel.add(normalizeCheckBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(svmPanel, gridBagConstraints);
        classifierParameterPanel.add(svmPanel);
        pack();

    }

    public static SVMClassifyDialog getInstance(javax.swing.JFrame parent,AbstractModel model) {
        if (instance == null || instance.getParent() != parent) {
            instance = new SVMClassifyDialog(parent,model);
        }
        return (SVMClassifyDialog) instance;
    }

    protected String serializeFields() {
        String params = "";
        params += " -S "+this.svmTypeComboBox.getSelectedIndex();
        params += " -R "+this.coeffTextField.getText();
        params += " -C "+this.costTextField.getText();
        params += " -D "+this.degreeKernelTextField.getText();
        params += " -E "+this.epsTextField.getText();
        params += " -G "+this.gammaTextField.getText();
        params += " -K "+this.kernelTypeComboBox.getSelectedIndex();
        params += " -N "+this.nuTextField.getText();
        if (this.normalizeCheckBox.isSelected()) params += " -Z";
        if (!this.shrinkingCheckBox.isSelected()) params += " -H";
        return params.trim();
    }

    private float getClass(AbstractMatrix matrix,int id) {

        for (int i=0;i<matrix.getRowCount();i++) {
            AbstractVector ins = matrix.getRow(i);
            if (ins.getId() == id) return ins.getKlass();
        }
        return -1;
        
    }

    private boolean isElement(ArrayList<Scalar> scalars,String name) {
        if (scalars == null || scalars.isEmpty()) return false;
        for (int i=0;i<scalars.size();i++)
            if (scalars.get(i).getName().equalsIgnoreCase(name))
                return true;
        return false;
    }

    private int isMatrixInstance(AbstractMatrix matrix, int id) {
        for (int i=0;i<matrix.getRowCount();i++)
            if (matrix.getRow(i).getId() == id) return i;
        return -1;
    }

    public void excludeTrainingData(AbstractMatrix test, AbstractMatrix train) {

        for (int i=0;i<train.getRowCount();i++) {
            int pos = isMatrixInstance(test,train.getRow(i).getId());
            if (pos != -1) test.removeRow(pos);
        }

    }

    @Override
    protected void runButtonActionPerformed(java.awt.event.ActionEvent evt) {

        SVMComp svmc = new SVMComp();

        
        svmc.setDissimilarityType(DissimilarityType.EUCLIDEAN);
        svmc.setDmatFilename(null);
        svmc.setMatrixFilename(null);
        
        try {
            svmc.setTrainingSet(this.trainingSetTextField.getText());
            AbstractMatrix trainingMatrix = MatrixFactory.getInstance(this.trainingSetTextField.getText());
            svmc.setTrainingMatrix(trainingMatrix);
            svmc.setArgsClassifier(serializeFields());
            svmc.setSaveDmat(false);
            svmc.setSaveMatrix(false);
            AbstractMatrix testMatrix = MatrixFactory.getInstance(this.testSetTextField.getText());
            if (testMatrix != null) {
                if (!this.useTrainingSetCheckBox.isSelected()) excludeTrainingData(testMatrix,trainingMatrix);
                svmc.input(testMatrix);
                svmc.execute();
                testMatrix = svmc.output();
                int indexScalar = 1;
                while (isElement(((LabeledProjectionModel) model).getScalars(), "SVM Class " + indexScalar)) {
                    indexScalar++;
                }
                Scalar SVMcdata = ((LabeledProjectionModel) model).addScalar("SVM Class " + indexScalar);
                //Creating, to this scalar, the same range used on cdata scalar, to maintain the colors of the instances...
                Scalar cdata = ((LabeledProjectionModel) model).getScalarByName("cdata");
                if (cdata != null) {
                    for (float i=cdata.getMin();i<=cdata.getMax();i++)
                        SVMcdata.store(i);
                }

                Scalar traincdata = ((LabeledProjectionModel) model).addScalar("SVM Training Instances");
                boolean existTrainingData = false;
                //Setting all the instances with this scalar...
                for (int i = 0; i < model.getInstances().size(); i++) {
                    ProjectionInstance pi = (ProjectionInstance) model.getInstances().get(i);
                    float klass = getClass(testMatrix, pi.getId());
                    if (klass != -1) {
                        pi.setScalarValue(SVMcdata, klass);
                        klass = getClass(trainingMatrix, pi.getId());
                        if (klass != -1) {
                            pi.setScalarValue(traincdata,1.0f);
                            existTrainingData = true;
                        }
                        else pi.setScalarValue(traincdata,0.0f);
                    }else {
                        //Verifying if this instance belongs to the training set
                        //in this case, it means that the training set was excluded from the classifying process,
                        //so the original class will be kept
                        klass = getClass(trainingMatrix, pi.getId());
                        if (klass != -1) {
                            pi.setScalarValue(SVMcdata, klass);
                            pi.setScalarValue(traincdata,1.0f);
                            existTrainingData = true;
                        }
                    }
                }
                nscalar = new ArrayList<Scalar>();
                if (!existTrainingData) ((LabeledProjectionModel) model).removeScalar(traincdata);
                else nscalar.add(traincdata);
                nscalar.add(SVMcdata);
                JOptionPane.showMessageDialog(this,"Classification completed.");
                this.setVisible(false);
            }
        } catch (IOException ex) {
            Logger.getLogger(SVMClassifyDialog.class.getName()).log(Level.SEVERE, null, ex);
        }
        



//        try {
//            SVM svm = new SVM(this.trainingSetTextField.getText());
//            AbstractMatrix testMatrix;
//            try {
//                testMatrix = MatrixFactory.getInstance(this.testSetTextField.getText());
//                if (testMatrix != null) {
//                    //excluding training instances from test set
//                    if (!this.useTrainingSetCheckBox.isSelected())
//                        excludeTrainingData(testMatrix,svm.getTrainingSet());
//                    testMatrix = svm.run(testMatrix, serializeFields());
//                    int indexScalar = 1;
//                    while (isElement(((ProjectionModel) model).getScalars(), "SVM Class " + indexScalar)) {
//                        indexScalar++;
//                    }
//                    Scalar SVMcdata = ((ProjectionModel) model).addScalar("SVM Class " + indexScalar);
//                    //Setting all the instances with class 0 of this scalar...
//                    for (int i = 0; i < model.getInstances().size(); i++) {
//                        ProjectionInstance pi = (ProjectionInstance) model.getInstances().get(i);
//                        float klass = getClass(testMatrix, pi.getId());
//                        if (klass != -1) {
//                            pi.setScalarValue(SVMcdata, klass);
//                        }
//                    }
//                    //((LabeledProjectionFrame) viewer).updateScalars(SVMcdata);
//                    nscalar = SVMcdata;
//                    //model.notifyObservers();
//                    JOptionPane.showMessageDialog(this,"Classification completed.");
//                    this.setVisible(false);
//                }
//            } catch (IOException ex) {
//                Logger.getLogger(SVMClassifyDialog.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } catch (IOException ex) {
//            Logger.getLogger(SVMClassifyDialog.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    //private static SVMClassifyDialog instance;
    private javax.swing.JPanel svmPanel;
    private javax.swing.JLabel coeffLabel;
    private javax.swing.JTextField coeffTextField;
    private javax.swing.JLabel costLabel;
    private javax.swing.JTextField costTextField;
    private javax.swing.JLabel degreeKernelLabel;
    private javax.swing.JTextField degreeKernelTextField;
    private javax.swing.JLabel epsLabel;
    private javax.swing.JTextField epsTextField;
    private javax.swing.JLabel gammaLabel;
    private javax.swing.JTextField gammaTextField;
    private javax.swing.JComboBox kernelTypeComboBox;
    private javax.swing.JLabel kernelTypeLabel;
    private javax.swing.JLabel matrixLabel;
    private javax.swing.JCheckBox normalizeCheckBox;
    private javax.swing.JLabel nuLabel;
    private javax.swing.JTextField nuTextField;
    private javax.swing.JCheckBox shrinkingCheckBox;
    private javax.swing.JComboBox svmTypeComboBox;
    private javax.swing.JLabel svmTypeLabel;
    
}
