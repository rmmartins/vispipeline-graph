package labeledprojection.view;

import bagvisualwords.dictionary.DictionaryBuilderComp;
import bagvisualwords.dictionary.DictionaryBuilderParamView;
import bagvisualwords.dictionary.DictionaryBuilderParamViewDialog;
import datamining.clustering.HierarchicalClustering;
import datamining.clustering.HierarchicalClustering.HierarchicalClusteringType;
import datamining.neighbors.Pair;
import distance.DistanceMatrix;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import distance.dissimilarity.Euclidean;
import featureextraction.util.FeatureExtractionUtil;
import graph.forcelayout.ForceData;
import graph.model.Edge;
import graph.util.Delaunay;
import visualizationbasics.view.MemoryCheck;
import projection.model.ProjectionInstance;
import visualizationbasics.view.selection.AbstractSelection;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Observable;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import labeledprojection.coordination.ClassMatchCoordination;
import labeledprojection.forcelayout.ForceDirectLayout;
import labeledprojection.marching.Mesh;
import labeledprojection.model.LabeledProjectionAnchor;
import matrix.AbstractMatrix;
import labeledprojection.model.LabeledProjectionInstance;
import labeledprojection.model.LabeledProjectionModel;
import labeledprojection.model.xml.XMLProjectionModelReader;
import labeledprojection.model.xml.XMLProjectionModelWriter;
import labeledprojection.stress.ChecksDialog;
import projection.model.Scalar;
import visualizationbasics.util.SaveDialog;
import visualizationbasics.color.ColorScalePanel;
import labeledprojection.stress.StressCurveComp;
import labeledprojection.util.LabeledProjectionUtil;
import labeledprojection.util.ProjectionConnectivity;
import labeledprojection.util.ImageCollection;
import labeledprojection.util.KmeansClusters;
import labeledprojection.util.ProjectionOpenDialog;
import labeledprojection.util.LabeledProjectionConstants;
import projection.util.ProjectionUtil;
import labeledprojection.util.filter.XMLFilter;
import labeledprojection.view.forms.ChangeOrderAnchorDialog;
import labeledprojection.view.forms.DragDropListItem;
import labeledprojection.view.forms.ExportInstancesDialog;
import labeledprojection.view.forms.ImportScalarDialog;
import labeledprojection.view.forms.klassification.SVMClassifyDialog;
import labeledprojection.view.forms.SaveClassDialog;
import labeledprojection.view.forms.klassification.ClassificationEvaluationDialog;
import labeledprojection.view.forms.klassification.CreateUpdateLWPRModelDialog;
import labeledprojection.view.forms.klassification.LWPRClassifierDialog;
import labeledprojection.view.selection.ClassAbstractSelection;
import labeledprojection.view.selection.ClassSelection;
import visualizationbasics.util.filter.PNGFilter;
import visualizationbasics.util.filter.SCALARFilter;
import labeledprojection.view.selection.InstanceSelection;
import labeledprojection.view.selection.SameClassSelection;
import labeledprojection.view.selection.ViewContentSelection;
import labeledprojection.view.tools.CorrelationCoefficientView;
import projection.view.tools.JoinScalars;
import projection.view.tools.MultidimensionalClusteringView;
import labeledprojection.view.tools.silhouette.DataSilhouetteCoefficientView;
import labeledprojection.view.tools.ProjectionMultidimensionalClusteringView;
import matrix.AbstractVector;
import matrix.MatrixFactory;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import projection.view.JExtendedComboBox;
import visualizationbasics.coordination.AbstractCoordinator;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.model.AbstractModel;
import visualizationbasics.util.OpenDialog;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.filter.AbstractFilter;
import visualizationbasics.util.filter.DATAFilter;
import visualizationbasics.util.filter.DMATFilter;
import visualizationbasics.util.filter.ZIPFilter;
import visualizationbasics.view.JFrameModelViewer;
import visualizationbasics.view.MessageDialog;


/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class LabeledProjectionFrame extends JFrameModelViewer {

    /** Creates new form ProjectionFrame */
    public LabeledProjectionFrame() {
        this.scalarComboModel = new DefaultComboBoxModel();
        this.edgeComboModel = new DefaultComboBoxModel();
        this.view = new ViewPanel();

        initComponents();
        initExtraComponents();

        InstanceSelection t = new InstanceSelection(this);
        addSelection(t,true);
        this.view.setSelection(t);
        addSelection(new ViewContentSelection(this),false);
        addSelection(new ClassSelection(this),false);
        addSelection(new SameClassSelection(this),false);

        edgeLabel.setVisible(false);
        edgeCombo.setVisible(false);

        //showInstancesCheckBox.setVisible(false);
        //combineLevelsCheckBox.setVisible(false);
        contourPanel.setVisible(false);
        contourLevelSlider.setVisible(false);
        showContourCheckBox.setVisible(false);
        showContourCheckBox.setSelected(false);

        runForceButton.setVisible(false);

    }

    public void addSelection(final ClassAbstractSelection selection, boolean state) {
        if (selection != null) {
            JToggleButton button = new JToggleButton();
            selectionButtonGroup.add(button);
            button.setIcon(selection.getIcon());
            button.setSelected(false);
            button.setToolTipText(selection.toString());
            button.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (getView() != null) {
                        getView().setSelection(selection);
                        selection.reset();
                    }
                }
            });
            button.setSelected(state);
            selectionToolBar.add(button);
        }
    }

    @Override
    public void addCoordinator(AbstractCoordinator coordinator) {
        super.addCoordinator(coordinator);
        if (coordinator != null && coordinator instanceof ClassMatchCoordination) {
            ((ClassMatchCoordination)coordinator).addFrame(this);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        selectionButtonGroup = new javax.swing.ButtonGroup();
        toolBar = new javax.swing.JToolBar();
        openButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        separatorLabel1 = new javax.swing.JLabel();
        zoomInButton = new javax.swing.JButton();
        zoomOutButton = new javax.swing.JButton();
        separatorLabel2 = new javax.swing.JLabel();
        toolButton = new javax.swing.JButton();
        separatorLabel5 = new javax.swing.JLabel();
        runForceButton = new javax.swing.JButton();
        findPanel = new javax.swing.JPanel();
        findTextField = new javax.swing.JTextField();
        findButton = new javax.swing.JButton();
        controlPanel = new javax.swing.JPanel();
        dataPanel = new javax.swing.JPanel();
        projectionTabbedPane = new javax.swing.JTabbedPane();
        scrollPanel = new javax.swing.JScrollPane(this.view);
        reportPanel = new ReportView();
        scalarPanel = new javax.swing.JPanel();
        colorLabel = new javax.swing.JLabel();
        scalarCombo = new JExtendedComboBox(this.scalarComboModel);
        edgeLabel = new javax.swing.JLabel();
        edgeCombo = new JExtendedComboBox(this.edgeComboModel);
        saveColorButton = new javax.swing.JButton();
        contourLevelSlider = new javax.swing.JSlider();
        contourPanel = new javax.swing.JPanel();
        showInstancesCheckBox = new javax.swing.JCheckBox();
        combineLevelsCheckBox = new javax.swing.JCheckBox();
        showContourCheckBox = new javax.swing.JCheckBox();
        statusBar_jPanel = new javax.swing.JPanel();
        status_jLabel = new javax.swing.JLabel();
        toolbarPanel = new javax.swing.JPanel();
        fixedToolBar = new javax.swing.JToolBar();
        moveInstancesToggleButton = new javax.swing.JToggleButton();
        cleanInstancesButton = new javax.swing.JButton();
        changeColorButton = new javax.swing.JButton();
        changeAnchorOrderButton = new javax.swing.JButton();
        selectionToolBar = new javax.swing.JToolBar();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        fileOpen = new javax.swing.JMenuItem();
        fileSave = new javax.swing.JMenuItem();
        separator1 = new javax.swing.JSeparator();
        exportMenu = new javax.swing.JMenu();
        fileExportToPng = new javax.swing.JMenuItem();
        fileExportToProjection = new javax.swing.JMenuItem();
        exportDatasetMenuItem = new javax.swing.JMenuItem();
        importMenu = new javax.swing.JMenu();
        importScalarsMenuItem = new javax.swing.JMenuItem();
        menuEdit = new javax.swing.JMenu();
        editClean = new javax.swing.JMenuItem();
        editDelete = new javax.swing.JMenuItem();
        menuTool = new javax.swing.JMenu();
        memoryCheckMenuItem = new javax.swing.JMenuItem();
        separatorOptions1 = new javax.swing.JSeparator();
        scalarMenu = new javax.swing.JMenu();
        importScalarsOption = new javax.swing.JMenuItem();
        exportScalarsOption = new javax.swing.JMenuItem();
        joinScalarsOptions = new javax.swing.JMenuItem();
        separatorOptions2 = new javax.swing.JSeparator();
        clusteringMenu = new javax.swing.JMenu();
        multidimensionalMenuItem = new javax.swing.JMenuItem();
        silhouetteCoefficientMenuItem = new javax.swing.JMenuItem();
        correlationMenuItem = new javax.swing.JMenuItem();
        classifyMenu = new javax.swing.JMenu();
        svmMenuItem = new javax.swing.JMenuItem();
        trainLWPRModelMenuItem = new javax.swing.JMenuItem();
        lwprClassifierMenuItem = new javax.swing.JMenuItem();
        classificationEvaluationMenuItem = new javax.swing.JMenuItem();
        separatorOptions3 = new javax.swing.JSeparator();
        createHCMenu = new javax.swing.JMenu();
        sLinkMenuItem = new javax.swing.JMenuItem();
        cLinkMenuItem = new javax.swing.JMenuItem();
        aLinkMenuItem = new javax.swing.JMenuItem();
        stressMenuItem = new javax.swing.JMenuItem();
        delaunayMenuItem = new javax.swing.JMenuItem();
        viewRadVizMenuItem = new javax.swing.JCheckBoxMenuItem();
        generateDictionaryMenuItem = new javax.swing.JMenuItem();
        contourMenuItem = new javax.swing.JMenuItem();
        toolOptions = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        openButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Open16.gif"))); // NOI18N
        openButton.setToolTipText("Open an existing projection");
        openButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openButtonActionPerformed(evt);
            }
        });
        toolBar.add(openButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Save16.gif"))); // NOI18N
        saveButton.setToolTipText("Save the current projection");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        toolBar.add(saveButton);

        separatorLabel1.setText("       ");
        toolBar.add(separatorLabel1);

        zoomInButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/ZoomIn16.gif"))); // NOI18N
        zoomInButton.setToolTipText("Zoom in");
        zoomInButton.setMaximumSize(new java.awt.Dimension(29, 27));
        zoomInButton.setMinimumSize(new java.awt.Dimension(29, 27));
        zoomInButton.setPreferredSize(new java.awt.Dimension(29, 27));
        zoomInButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInButtonActionPerformed(evt);
            }
        });
        toolBar.add(zoomInButton);

        zoomOutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/ZoomOut16.gif"))); // NOI18N
        zoomOutButton.setToolTipText("Zoom out");
        zoomOutButton.setMaximumSize(new java.awt.Dimension(29, 27));
        zoomOutButton.setMinimumSize(new java.awt.Dimension(29, 27));
        zoomOutButton.setPreferredSize(new java.awt.Dimension(29, 27));
        zoomOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutButtonActionPerformed(evt);
            }
        });
        toolBar.add(zoomOutButton);

        separatorLabel2.setText("       ");
        toolBar.add(separatorLabel2);

        toolButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Preferences16.gif"))); // NOI18N
        toolButton.setToolTipText("Tool Preferences");
        toolButton.setMaximumSize(new java.awt.Dimension(29, 27));
        toolButton.setMinimumSize(new java.awt.Dimension(29, 27));
        toolButton.setPreferredSize(new java.awt.Dimension(29, 27));
        toolButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toolButtonActionPerformed(evt);
            }
        });
        toolBar.add(toolButton);

        separatorLabel5.setText("       ");
        toolBar.add(separatorLabel5);

        runForceButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/media/Play16.gif"))); // NOI18N
        runForceButton.setToolTipText("Run Force Directed Layout");
        runForceButton.setFocusable(false);
        runForceButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        runForceButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        runForceButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runForceButtonActionPerformed(evt);
            }
        });
        toolBar.add(runForceButton);

        findPanel.setOpaque(false);
        findPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        findTextField.setColumns(10);
        findTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                findTextFieldKeyPressed(evt);
            }
        });
        findPanel.add(findTextField);

        findButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Find16.gif"))); // NOI18N
        findButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                findButtonActionPerformed(evt);
            }
        });
        findPanel.add(findButton);

        toolBar.add(findPanel);

        getContentPane().add(toolBar, java.awt.BorderLayout.NORTH);

        controlPanel.setLayout(new java.awt.BorderLayout());

        dataPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        dataPanel.setLayout(new java.awt.BorderLayout());

        projectionTabbedPane.addTab("Projection", scrollPanel);
        projectionTabbedPane.addTab("Report", reportPanel);

        dataPanel.add(projectionTabbedPane, java.awt.BorderLayout.CENTER);

        scalarPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        scalarPanel.setLayout(new java.awt.GridBagLayout());

        colorLabel.setText("Color");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        scalarPanel.add(colorLabel, gridBagConstraints);

        scalarCombo.setMaximumSize(new java.awt.Dimension(85, 27));
        scalarCombo.setMinimumSize(new java.awt.Dimension(85, 27));
        scalarCombo.setPreferredSize(new java.awt.Dimension(85, 27));
        scalarCombo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                scalarComboMouseClicked(evt);
            }
        });
        scalarCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                scalarComboItemStateChanged(evt);
            }
        });
        scalarCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                scalarComboActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        scalarPanel.add(scalarCombo, gridBagConstraints);

        edgeLabel.setText("Edge");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        scalarPanel.add(edgeLabel, gridBagConstraints);

        edgeCombo.setPreferredSize(new java.awt.Dimension(85, 27));
        edgeCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                edgeComboItemStateChanged(evt);
            }
        });
        edgeCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edgeComboActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        scalarPanel.add(edgeCombo, gridBagConstraints);

        saveColorButton.setText("Save");
        saveColorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveColorButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        scalarPanel.add(saveColorButton, gridBagConstraints);

        contourLevelSlider.setMajorTickSpacing(1);
        contourLevelSlider.setMaximum(3);
        contourLevelSlider.setMinorTickSpacing(1);
        contourLevelSlider.setPaintLabels(true);
        contourLevelSlider.setPaintTicks(true);
        contourLevelSlider.setSnapToTicks(true);
        contourLevelSlider.setValue(0);
        contourLevelSlider.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        contourLevelSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                contourLevelSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        scalarPanel.add(contourLevelSlider, gridBagConstraints);

        contourPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        contourPanel.setLayout(new java.awt.GridBagLayout());

        showInstancesCheckBox.setText("Show Instances");
        showInstancesCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));
        showInstancesCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showInstancesCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        contourPanel.add(showInstancesCheckBox, gridBagConstraints);

        combineLevelsCheckBox.setText("Combine Levels");
        combineLevelsCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));
        combineLevelsCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                combineLevelsCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        contourPanel.add(combineLevelsCheckBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        scalarPanel.add(contourPanel, gridBagConstraints);

        showContourCheckBox.setText("Show Contour");
        showContourCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showContourCheckBoxActionPerformed(evt);
            }
        });
        scalarPanel.add(showContourCheckBox, new java.awt.GridBagConstraints());

        dataPanel.add(scalarPanel, java.awt.BorderLayout.NORTH);

        controlPanel.add(dataPanel, java.awt.BorderLayout.CENTER);

        statusBar_jPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        statusBar_jPanel.setPreferredSize(new java.awt.Dimension(30, 30));

        status_jLabel.setText("                      ");
        statusBar_jPanel.add(status_jLabel);

        controlPanel.add(statusBar_jPanel, java.awt.BorderLayout.PAGE_END);

        toolbarPanel.setLayout(new java.awt.BorderLayout(0, 20));

        fixedToolBar.setOrientation(javax.swing.SwingConstants.VERTICAL);
        fixedToolBar.setRollover(true);

        moveInstancesToggleButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/navigation/Forward16.gif"))); // NOI18N
        moveInstancesToggleButton.setToolTipText("Move Point");
        moveInstancesToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveInstancesToggleButtonActionPerformed(evt);
            }
        });
        fixedToolBar.add(moveInstancesToggleButton);

        cleanInstancesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Edit16.gif"))); // NOI18N
        cleanInstancesButton.setToolTipText("Clean Instances");
        cleanInstancesButton.setFocusable(false);
        cleanInstancesButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cleanInstancesButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        cleanInstancesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cleanInstancesButtonActionPerformed(evt);
            }
        });
        cleanInstancesButton.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cleanInstancesButtonFocusLost(evt);
            }
        });
        fixedToolBar.add(cleanInstancesButton);

        changeColorButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Redo16.gif"))); // NOI18N
        changeColorButton.setToolTipText("Change Instance Color");
        changeColorButton.setFocusable(false);
        changeColorButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        changeColorButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        changeColorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeColorButtonActionPerformed(evt);
            }
        });
        changeColorButton.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                changeColorButtonFocusLost(evt);
            }
        });
        fixedToolBar.add(changeColorButton);

        changeAnchorOrderButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/axes.png"))); // NOI18N
        changeAnchorOrderButton.setToolTipText("Change RadViz Anchors Order");
        changeAnchorOrderButton.setFocusable(false);
        changeAnchorOrderButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        changeAnchorOrderButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        changeAnchorOrderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeAnchorOrderButtonActionPerformed(evt);
            }
        });
        changeAnchorOrderButton.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                changeAnchorOrderButtonFocusLost(evt);
            }
        });
        fixedToolBar.add(changeAnchorOrderButton);

        toolbarPanel.add(fixedToolBar, java.awt.BorderLayout.NORTH);

        selectionToolBar.setOrientation(javax.swing.SwingConstants.VERTICAL);
        toolbarPanel.add(selectionToolBar, java.awt.BorderLayout.CENTER);

        controlPanel.add(toolbarPanel, java.awt.BorderLayout.EAST);

        getContentPane().add(controlPanel, java.awt.BorderLayout.CENTER);

        menuFile.setMnemonic('F');
        menuFile.setText("File");

        fileOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        fileOpen.setMnemonic('O');
        fileOpen.setText("Open Projection");
        fileOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileOpenActionPerformed(evt);
            }
        });
        menuFile.add(fileOpen);

        fileSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        fileSave.setMnemonic('S');
        fileSave.setText("Save Projection");
        fileSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileSaveActionPerformed(evt);
            }
        });
        menuFile.add(fileSave);
        menuFile.add(separator1);

        exportMenu.setText("Export");

        fileExportToPng.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        fileExportToPng.setMnemonic('P');
        fileExportToPng.setText("Export PNG File");
        fileExportToPng.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileExportToPngActionPerformed(evt);
            }
        });
        exportMenu.add(fileExportToPng);

        fileExportToProjection.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        fileExportToProjection.setMnemonic('J');
        fileExportToProjection.setText("Export 2D Points File");
        fileExportToProjection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileExportToProjectionActionPerformed(evt);
            }
        });
        exportMenu.add(fileExportToProjection);

        exportDatasetMenuItem.setText("Export Dataset");
        exportDatasetMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportDatasetMenuItemActionPerformed(evt);
            }
        });
        exportMenu.add(exportDatasetMenuItem);

        menuFile.add(exportMenu);

        importMenu.setText("Import");

        importScalarsMenuItem.setText("Import Scalars");
        importScalarsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importScalarsMenuItemActionPerformed(evt);
            }
        });
        importMenu.add(importScalarsMenuItem);

        menuFile.add(importMenu);

        menuBar.add(menuFile);

        menuEdit.setMnemonic('E');
        menuEdit.setText("Edit");

        editClean.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        editClean.setMnemonic('C');
        editClean.setText("Clean Projection");
        editClean.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editCleanActionPerformed(evt);
            }
        });
        menuEdit.add(editClean);

        editDelete.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_DELETE, 0));
        editDelete.setMnemonic('D');
        editDelete.setText("Delete Points");
        editDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editDeleteActionPerformed(evt);
            }
        });
        menuEdit.add(editDelete);

        menuBar.add(menuEdit);

        menuTool.setText("Tool");
        menuTool.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuToolActionPerformed(evt);
            }
        });

        memoryCheckMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        memoryCheckMenuItem.setMnemonic('H');
        memoryCheckMenuItem.setText("Memory Check");
        memoryCheckMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                memoryCheckMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(memoryCheckMenuItem);
        menuTool.add(separatorOptions1);

        scalarMenu.setText("Scalars");

        importScalarsOption.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        importScalarsOption.setMnemonic('S');
        importScalarsOption.setText("Import Scalars");
        importScalarsOption.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importScalarsOptionActionPerformed(evt);
            }
        });
        scalarMenu.add(importScalarsOption);

        exportScalarsOption.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        exportScalarsOption.setMnemonic('x');
        exportScalarsOption.setText("Export Scalars");
        exportScalarsOption.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportScalarsOptionActionPerformed(evt);
            }
        });
        scalarMenu.add(exportScalarsOption);

        joinScalarsOptions.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_J, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        joinScalarsOptions.setMnemonic('J');
        joinScalarsOptions.setText("Join Scalars");
        joinScalarsOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                joinScalarsOptionsActionPerformed(evt);
            }
        });
        scalarMenu.add(joinScalarsOptions);

        menuTool.add(scalarMenu);
        menuTool.add(separatorOptions2);

        clusteringMenu.setMnemonic('C');
        clusteringMenu.setText("Clustering");

        multidimensionalMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        multidimensionalMenuItem.setText("Multidimensional Data");
        multidimensionalMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                multidimensionalMenuItemActionPerformed(evt);
            }
        });
        clusteringMenu.add(multidimensionalMenuItem);

        silhouetteCoefficientMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        silhouetteCoefficientMenuItem.setText("Silhouette Coefficient");
        silhouetteCoefficientMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                silhouetteCoefficientMenuItemActionPerformed(evt);
            }
        });
        clusteringMenu.add(silhouetteCoefficientMenuItem);

        correlationMenuItem.setText("Correlation Coefficient");
        correlationMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                correlationMenuItemActionPerformed(evt);
            }
        });
        clusteringMenu.add(correlationMenuItem);

        menuTool.add(clusteringMenu);

        classifyMenu.setText("Classifying");

        svmMenuItem.setText("Support Vector Machine");
        svmMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                svmMenuItemActionPerformed(evt);
            }
        });
        classifyMenu.add(svmMenuItem);

        trainLWPRModelMenuItem.setText("Create/Update LWPR Model");
        trainLWPRModelMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                trainLWPRModelMenuItemActionPerformed(evt);
            }
        });
        classifyMenu.add(trainLWPRModelMenuItem);

        lwprClassifierMenuItem.setText("LWPR Classifier");
        lwprClassifierMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lwprClassifierMenuItemActionPerformed(evt);
            }
        });
        classifyMenu.add(lwprClassifierMenuItem);

        classificationEvaluationMenuItem.setText("Classification Evaluation");
        classificationEvaluationMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                classificationEvaluationMenuItemActionPerformed(evt);
            }
        });
        classifyMenu.add(classificationEvaluationMenuItem);

        menuTool.add(classifyMenu);
        menuTool.add(separatorOptions3);

        createHCMenu.setText("Calculate Hierarchical Clustering");

        sLinkMenuItem.setText("Single Link");
        sLinkMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sLinkMenuItemActionPerformed(evt);
            }
        });
        createHCMenu.add(sLinkMenuItem);

        cLinkMenuItem.setText("Complete Link");
        cLinkMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cLinkMenuItemActionPerformed(evt);
            }
        });
        createHCMenu.add(cLinkMenuItem);

        aLinkMenuItem.setText("Average Link");
        aLinkMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aLinkMenuItemActionPerformed(evt);
            }
        });
        createHCMenu.add(aLinkMenuItem);

        menuTool.add(createHCMenu);

        stressMenuItem.setText("Stress Curve");
        stressMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stressMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(stressMenuItem);

        delaunayMenuItem.setText("Create Delaunay Triangulation");
        delaunayMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delaunayMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(delaunayMenuItem);

        viewRadVizMenuItem.setText("View RadViz Anchors");
        viewRadVizMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewRadVizMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(viewRadVizMenuItem);

        generateDictionaryMenuItem.setText("Create Visual Words Dictionary");
        generateDictionaryMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateDictionaryMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(generateDictionaryMenuItem);

        contourMenuItem.setText("Create Contour");
        contourMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contourMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(contourMenuItem);

        toolOptions.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        toolOptions.setMnemonic('O');
        toolOptions.setText("Tool Options");
        toolOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toolOptionsActionPerformed(evt);
            }
        });
        menuTool.add(toolOptions);

        menuBar.add(menuTool);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    protected void fileOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileOpenActionPerformed
        try {
            PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
            //int result = ImageOpenDialog.showOpenDialog(spm, new XMLFilter(), this);
            int result = OpenDialog.showOpenDialog(spm, new XMLFilter(), this);

            if (result == JFileChooser.APPROVE_OPTION) {
//                String filename = ImageOpenDialog.getFilename();
                String filename = OpenDialog.getFilename();

                try {
                    XMLProjectionModelReader mreader = new XMLProjectionModelReader();
                    LabeledProjectionModel newmodel = new LabeledProjectionModel();
                    mreader.read(newmodel, filename);
                    setModel(newmodel);
                } catch (IOException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    JOptionPane.showMessageDialog(this, e.getMessage(),
                            "Problems opening the file", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_fileOpenActionPerformed

    protected void fileSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileSaveActionPerformed
        if (model != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
                int result = SaveDialog.showSaveDialog(spm, new XMLFilter(), this, "model.xml");

                if (result == JFileChooser.APPROVE_OPTION) {
                    String filename = SaveDialog.getFilename();

                    try {
                        XMLProjectionModelWriter mwriter = new XMLProjectionModelWriter();
                        mwriter.write((LabeledProjectionModel) model, filename);
                    } catch (IOException e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        JOptionPane.showMessageDialog(this, e.getMessage(),
                                "Problems saving the file", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_fileSaveActionPerformed

    private void fileExportToPngActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileExportToPngActionPerformed
        try {
            PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
            int result = SaveDialog.showSaveDialog(spm, new PNGFilter(), this, "image.png");

            if (result == JFileChooser.APPROVE_OPTION) {
                String filename = SaveDialog.getFilename();

                try {
                    view.saveToPngImageFile(filename);
                } catch (IOException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    JOptionPane.showMessageDialog(this, e.getMessage(),
                            "Problems saving the file", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_fileExportToPngActionPerformed

    private void fileExportToProjectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileExportToProjectionActionPerformed
        if (model != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
                int result = SaveDialog.showSaveDialog(spm, new DATAFilter(), this, "projection.data");

                if (result == JFileChooser.APPROVE_OPTION) {
                    String filename = SaveDialog.getFilename();

                    try {
                        AbstractMatrix matrix = ProjectionUtil.modelToMatrix((LabeledProjectionModel) model, getCurrentScalar());
                        matrix.save(filename);
                    } catch (IOException e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        JOptionPane.showMessageDialog(this, e.getMessage(),
                                "Problems saving the file", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_fileExportToProjectionActionPerformed

    private void editCleanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editCleanActionPerformed
        if (view != null) {
            view.cleanSelectedInstances();
        }
}//GEN-LAST:event_editCleanActionPerformed

    private void editDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editDeleteActionPerformed
        if (view != null) {
            view.removeSelectedInstances();
        }
}//GEN-LAST:event_editDeleteActionPerformed

    private void toolButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toolButtonActionPerformed
        toolOptionsActionPerformed(evt);
}//GEN-LAST:event_toolButtonActionPerformed

    private void zoomOutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomOutButtonActionPerformed
        if (view != null) {
            view.zoomOut();
        }
}//GEN-LAST:event_zoomOutButtonActionPerformed

    private void zoomInButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomInButtonActionPerformed
        if (view != null) {
            view.zoomIn();
        }
}//GEN-LAST:event_zoomInButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        fileSaveActionPerformed(evt);
}//GEN-LAST:event_saveButtonActionPerformed

    private void openButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openButtonActionPerformed
        fileOpenActionPerformed(evt);
}//GEN-LAST:event_openButtonActionPerformed

    private void moveInstancesToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moveInstancesToggleButtonActionPerformed
        moveinstances = moveInstancesToggleButton.isSelected();
}//GEN-LAST:event_moveInstancesToggleButtonActionPerformed

    private void cleanInstancesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cleanInstancesButtonActionPerformed
        editCleanActionPerformed(evt);
}//GEN-LAST:event_cleanInstancesButtonActionPerformed

    private void scalarComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_scalarComboItemStateChanged
        if (evt.getStateChange() == ItemEvent.DESELECTED) {
            Scalar scalar = (Scalar) this.scalarCombo.getSelectedItem();
            if (scalar != null) {
//                if (scalar.getName().startsWith("Pseudo")) {
//                    if (((LabeledProjectionModel)model).getScalars().contains(scalar)) {
//                        ((LabeledProjectionModel)model).setSelectedScalar(scalar);
//                        ArrayList<AbstractInstance> instances = ((LabeledProjectionModel)model).getInstances();
//                        //change the color of each instance
//                        for (int i = 0; i < instances.size(); i++) {
//                            LabeledProjectionInstance pi = (LabeledProjectionInstance) instances.get(i);
//                            Color c = pi.getPseudoColor(scalar.getName());
//                            if (c != null)
//                                pi.setColor(c);
//                        }
//                    }else
//                        view.colorAs(scalar);
//                }else
                    view.colorAs(scalar);
                    if (scalar.getName().contains("ClassMatch")) {
                        ((ReportView)reportPanel).updateReportData(scalar);
                    }else {
                        ((ReportView)reportPanel).showHideClassMatchPanel(false);
                    }
                if (view.mesh2 != null)
                    updateSlider(scalar);
            }
        }
    }//GEN-LAST:event_scalarComboItemStateChanged

    private void toolOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toolOptionsActionPerformed
        LabeledProjectionFrameOptions.getInstance(this).display(this);
}//GEN-LAST:event_toolOptionsActionPerformed

    private void silhouetteCoefficientMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_silhouetteCoefficientMenuItemActionPerformed
        if (model != null) {
            try {
                DataSilhouetteCoefficientView.getInstance(this).display((LabeledProjectionModel) model, getCurrentScalar());
                updateScalars(null);
            } catch (IOException ex) {
                Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_silhouetteCoefficientMenuItemActionPerformed

    private void multidimensionalMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_multidimensionalMenuItemActionPerformed
        if (model != null) {
            Scalar s = ProjectionMultidimensionalClusteringView.getInstance(this).display((LabeledProjectionModel) model);
            updateScalars(s);
        }
}//GEN-LAST:event_multidimensionalMenuItemActionPerformed

    private void joinScalarsOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_joinScalarsOptionsActionPerformed
        if (model != null) {
            Scalar s = JoinScalars.getInstance(this).display((LabeledProjectionModel) model);
            updateScalars(s);
        }
}//GEN-LAST:event_joinScalarsOptionsActionPerformed

    private void exportScalarsOptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportScalarsOptionActionPerformed
        if (view != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
                int result = SaveDialog.showSaveDialog(spm, new SCALARFilter(), this, "scalars.scalar");

                if (result == JFileChooser.APPROVE_OPTION) {
                    if (model != null) {
                        String filename = SaveDialog.getFilename();
                        //ProjectionUtil.exportScalars((LabeledProjectionModel) model, filename);
                        LabeledProjectionUtil.exportScalars((LabeledProjectionModel) model, filename);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_exportScalarsOptionActionPerformed

    private void importScalarsOptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importScalarsOptionActionPerformed
        if (view != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
//                int result = ImageOpenDialog.showOpenDialog(spm, new SCALARFilter(), this);
                int result = OpenDialog.showOpenDialog(spm, new SCALARFilter(), this);

                if (result == javax.swing.JFileChooser.APPROVE_OPTION) {
                    if (model != null) {
                        final MessageDialog dialog = MessageDialog.show(this, "Importing scalars...");

                        Thread t = new Thread() {

                            @Override
                            public void run() {
                                try {
                                    //String filename = ImageOpenDialog.getFilename();
                                    String filename = OpenDialog.getFilename();
                                    //ProjectionUtil.importScalars((LabeledProjectionModel) model, filename);
                                    LabeledProjectionUtil.importScalars((LabeledProjectionModel) model, filename);
                                    updateScalars(null);
                                } catch (IOException ex) {
                                    Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
                                } finally {
                                    dialog.close();
                                }
                            }
                        };

                        t.start();
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_importScalarsOptionActionPerformed

    private void memoryCheckMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_memoryCheckMenuItemActionPerformed
        MemoryCheck.showMemoryCheck();
}//GEN-LAST:event_memoryCheckMenuItemActionPerformed

    private void findButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_findButtonActionPerformed
        if (model != null) {
            model.cleanSelectedInstances();

            Pattern p = Pattern.compile(findTextField.getText().trim().toLowerCase());

            int begin = 0;
            int end = model.getInstances().size();

            boolean stop = false;
            boolean restart = true;

            while (!stop) {
                for (int i = begin; i < end; i++) {
                    Matcher m = p.matcher(model.getInstances().get(i).toString().trim().toLowerCase());

                    if (m.find()) {
                        model.setSelectedInstance(model.getInstances().get(i));
                        stop = true;
                        break;
                    }
                }

                if (restart) {
                    end = begin - 2;
                    begin = 0;
                    restart = false;
                } else {
                    stop = true;
                }
            }

            model.setChanged();
            model.notifyObservers();
        }
    }//GEN-LAST:event_findButtonActionPerformed

    private void findTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_findTextFieldKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            findButtonActionPerformed(null);
        }
    }//GEN-LAST:event_findTextFieldKeyPressed

    protected void scalarComboMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_scalarComboMouseClicked
        if (evt.getClickCount() == 2) {
            Scalar scalar = (Scalar) this.scalarCombo.getSelectedItem();

            if (!scalar.getName().equals(LabeledProjectionConstants.DOTS)) {
                scalarComboModel.removeElement(scalar);
                scalarCombo.setSelectedIndex(0);
                ((LabeledProjectionModel)model).removeScalar(scalar);
                if (scalar.getName().contains("ClassMatch")) {
                    ((ReportView)reportPanel).removeScalarData(scalar);
                }
                model.notifyObservers();
            }
        }
    }//GEN-LAST:event_scalarComboMouseClicked

    private void cleanInstancesButtonFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cleanInstancesButtonFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_cleanInstancesButtonFocusLost

    private void saveColorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveColorButtonActionPerformed
        Scalar currentScalar = (Scalar) this.scalarCombo.getSelectedItem();
        if (currentScalar != null) {
            SaveClassDialog.getInstance(this,(LabeledProjectionModel)model,((ReportView)getReportPanel()).getDataSourceValue()).display(currentScalar);
        }
    }//GEN-LAST:event_saveColorButtonActionPerformed

    private void exportDatasetMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportDatasetMenuItemActionPerformed
        ImageCollection im = ((LabeledProjectionModel)model).getImageCollection();
        String imFilename = "";
        if (im != null) imFilename = im.getFilename();
        ExportInstancesDialog.getInstance(this).display(model,imFilename);
    }//GEN-LAST:event_exportDatasetMenuItemActionPerformed

    private void importScalarsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importScalarsMenuItemActionPerformed
        ImportScalarDialog isd = ImportScalarDialog.getInstance(this);
        isd.display();
        if (isd.getScalar() != null) {
            this.updateScalars(isd.getScalar());
        }
        model.notifyObservers();
    }//GEN-LAST:event_importScalarsMenuItemActionPerformed

    private void svmMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_svmMenuItemActionPerformed
        SVMClassifyDialog svmd = SVMClassifyDialog.getInstance(this,model);
        svmd.display();
        if (svmd.getNumberScalars() > 0) {
            for (int i=0;i<svmd.getNumberScalars();i++) {
                this.updateScalars(svmd.getNscalar(i));
            }
            model.notifyObservers();
        }
    }//GEN-LAST:event_svmMenuItemActionPerformed

    private void stressMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stressMenuItemActionPerformed
        try {
            StressCurveComp comp = new StressCurveComp();
            comp.input((LabeledProjectionModel) model);
            String source = ((ReportView)getReportPanel()).getDataSourceValue();
            //if (source == null || source.isEmpty() || !source.endsWith(".data")) {
            if (source == null || source.isEmpty()) {
                PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
                ArrayList<AbstractFilter> filters = new ArrayList<AbstractFilter>();
                filters.add(new DATAFilter());
                filters.add(new DMATFilter());
                int result = ProjectionOpenDialog.showOpenDialogMultipleFilter(spm, filters, this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    source = OpenDialog.getFilename();
                }
            }
            if (source.endsWith(".data"))
                comp.setDataFileType(0);
            else if (source.endsWith(".dmat"))
                comp.setDataFileType(1);
            comp.setDataFileName(source);
            ChecksDialog.getInstance(this).display(comp);
//            String message = JOptionPane.showInputDialog("Inform a label to the Stress Curve Graphic (optional):");
//            if (message != null) {
//                comp.setGraphicLabel(message);
//                comp.execute();
//            }
        } catch (IOException ex) {
            Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_stressMenuItemActionPerformed

    private void generateDictionaryMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateDictionaryMenuItemActionPerformed
        try {
            ArrayList<AbstractInstance> instances = ((LabeledProjectionModel)getModel()).getInstances();
            if (instances == null) return;
            Scalar scalar = ((LabeledProjectionModel)getModel()).getSelectedScalar();
            if (scalar == null) scalar = ((LabeledProjectionModel)getModel()).getScalarByName("cdata");
            if (scalar == null) return;
            ArrayList<String> files = new ArrayList<String>();
            for (int i=0;i<instances.size();i++)
                files.add(((LabeledProjectionInstance)instances.get(i)).getLabel());
            DictionaryBuilderComp dbComp = new DictionaryBuilderComp();
            DictionaryBuilderParamView dbParam = new DictionaryBuilderParamView(dbComp);
            DictionaryBuilderParamViewDialog.getInstance(this,dbParam).display();
            //Extracting selected files...
            PropertiesManager pm = PropertiesManager.getInstance("projection.properties");
            pm.setProperty("UNZIP.DIR", System.getProperty("user.dir") + "\\tempDir");
            pm.setProperty("FEATURE.DIR", System.getProperty("user.dir") + "\\lib\\featureSelection");
            String unzipDir = pm.getProperty("UNZIP.DIR");
            FeatureExtractionUtil.unzipSpecificFiles(pm,dbComp.getInputDirectory(),files,true);

            //Separando as instancias em grupos, de acordo com a classe de cada uma...
            TreeMap<String,ArrayList<AbstractMatrix>> classes = new TreeMap<String,ArrayList<AbstractMatrix>>();
            for (int i=0;i<instances.size();i++) {
                LabeledProjectionInstance lpi = (LabeledProjectionInstance) instances.get(i);
                String classe = Float.toString(lpi.getScalarValue(scalar));
                if (!classes.containsKey(classe))
                    classes.put(classe,new ArrayList<AbstractMatrix>());
                AbstractMatrix am = MatrixFactory.getInstance(unzipDir+"\\"+lpi.getLabel().replace("jpg","data"));
                if (am != null)
                    classes.get(classe).add(am);
            }
            dbComp.input(classes);
            dbComp.execute();
            if (dbComp.output() != null) {
                dbComp.output().save(dbComp.getOutputDictionary());
                JOptionPane.showMessageDialog(this,"Dictionary generated.");
            }else {
                JOptionPane.showMessageDialog(this,"Error creating dictionary.");
            }
        } catch (IOException ex) {
            Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_generateDictionaryMenuItemActionPerformed

    private void scalarComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_scalarComboActionPerformed
        
    }//GEN-LAST:event_scalarComboActionPerformed

    private void delaunayMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delaunayMenuItemActionPerformed

        ((LabeledProjectionModel)model).perturb();

        //Creating new Delaunay triangulation
//        float[][] points = new float[model.getInstances().size()][];
//        for (int i = 0; i < points.length; i++) {
//            points[i] = new float[2];
//            points[i][0] = ((LabeledProjectionInstance)model.getInstances().get(i)).getX();
//            points[i][1] = ((LabeledProjectionInstance)model.getInstances().get(i)).getY();
//        }
//
//        Delaunay d = new Delaunay();
//        Pair[][] neighborhood = d.execute(points);
//
//        HashMap<Integer, LabeledProjectionInstance> index = new HashMap<Integer, LabeledProjectionInstance>();
//        for (int i=0;i<model.getInstances().size();i++) {
//            index.put(i,(LabeledProjectionInstance)model.getInstances().get(i));
//        }
//
//        ArrayList<Edge> edges = new ArrayList<Edge>();
//
//        for (int i = 0; i < neighborhood.length; i++) {
//            for (int j = 0; j < neighborhood[i].length; j++) {
//                edges.add(new Edge(index.get(i).getId(),index.get(neighborhood[i][j].index).getId(),neighborhood[i][j].value));
//            }
//        }
//
//        ProjectionConnectivity delaunayCon = new ProjectionConnectivity(LabeledProjectionConstants.DELAUNAY,edges);
        ProjectionConnectivity delaunayCon = createDelaunayConnectivity(model.getInstances());
        ((LabeledProjectionModel)model).addProjectionConnectivity(delaunayCon);
        ((LabeledProjectionModel)model).setSelectedConnectivity(delaunayCon);

        ProjectionConnectivity dotsCon = new ProjectionConnectivity(LabeledProjectionConstants.DOTS, new ArrayList<Edge>());
        ((LabeledProjectionModel)model).addProjectionConnectivity(dotsCon);
                
        edgeComboModel.removeAllElements();
        edgeComboModel.addElement(dotsCon);
        edgeComboModel.addElement(delaunayCon);
        edgeComboModel.setSelectedItem(delaunayCon);

        edgeLabel.setVisible(true);
        edgeCombo.setVisible(true);
        runForceButton.setVisible(true);

        model.setChanged();
        model.notifyObservers();

    }//GEN-LAST:event_delaunayMenuItemActionPerformed

    public ProjectionConnectivity createDelaunayConnectivity(ArrayList instances) {

        float[][] points = new float[instances.size()][];
        for (int i = 0; i < points.length; i++) {
            points[i] = new float[2];
            points[i][0] = ((LabeledProjectionInstance)instances.get(i)).getX();
            points[i][1] = ((LabeledProjectionInstance)instances.get(i)).getY();
        }

        Delaunay d = new Delaunay();
        Pair[][] neighborhood = d.execute(points);

        HashMap<Integer, LabeledProjectionInstance> index = new HashMap<Integer, LabeledProjectionInstance>();
        for (int i=0;i<instances.size();i++) {
            index.put(i,(LabeledProjectionInstance)instances.get(i));
        }

        ArrayList<Edge> edges = new ArrayList<Edge>();

        for (int i = 0; i < neighborhood.length; i++) {
            for (int j = 0; j < neighborhood[i].length; j++) {
                edges.add(new Edge(index.get(i).getId(),index.get(neighborhood[i][j].index).getId(),neighborhood[i][j].value));
            }
        }

        ProjectionConnectivity delaunayCon = new ProjectionConnectivity(LabeledProjectionConstants.DELAUNAY,edges);

        return delaunayCon;
    }

    private void edgeComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_edgeComboItemStateChanged
        
}//GEN-LAST:event_edgeComboItemStateChanged

    private void menuToolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuToolActionPerformed
        
    }//GEN-LAST:event_menuToolActionPerformed

    private void edgeComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edgeComboActionPerformed
        ProjectionConnectivity conn = (ProjectionConnectivity) this.edgeCombo.getSelectedItem();
        if (conn != null) {
                //((LabeledProjectionModel)model).addProjectionConnectivity(conn);
                ((LabeledProjectionModel)model).setSelectedConnectivity(conn);
            if (conn.getName().equalsIgnoreCase(LabeledProjectionConstants.DELAUNAY))
                runForceButton.setVisible(true);
            else
                runForceButton.setVisible(false);
        }
        model.setChanged();
        model.notifyObservers();
    }//GEN-LAST:event_edgeComboActionPerformed

    private void sLinkMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sLinkMenuItemActionPerformed
        createHCScalar(HierarchicalClusteringType.SLINK);
    }//GEN-LAST:event_sLinkMenuItemActionPerformed

    private void cLinkMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cLinkMenuItemActionPerformed
        createHCScalar(HierarchicalClusteringType.CLINK);
    }//GEN-LAST:event_cLinkMenuItemActionPerformed

    private void aLinkMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aLinkMenuItemActionPerformed
        createHCScalar(HierarchicalClusteringType.ALINK);
    }//GEN-LAST:event_aLinkMenuItemActionPerformed

    private void contourMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contourMenuItemActionPerformed

        if (((LabeledProjectionModel)model).getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY) == null) {
            JOptionPane.showMessageDialog(controlPanel,"No Delaunay Connectivity created!");
        }else {
            //Principal p = new Principal();
            //p.createTriangleMesh((LabeledProjectionModel) model);
            view.mesh2 = new Mesh((LabeledProjectionModel)model);
            //view.mesh = p.mesh;
            showContourCheckBox.setVisible(true);
            showContourCheckBox.setSelected(true);
            showInstancesCheckBox.setVisible(true);
            showInstancesCheckBox.setSelected(true);
            contourPanel.setVisible(true);
            contourLevelSlider.setVisible(true);
            //combineLevelsCheckBox.setVisible(true);
            combineLevelsCheckBox.setSelected(true);
            updateSlider(((LabeledProjectionModel)model).getSelectedScalar());

            model.setChanged();
            model.notifyObservers();
        }
    }//GEN-LAST:event_contourMenuItemActionPerformed

    private void showInstancesCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showInstancesCheckBoxActionPerformed
        view.showInstances = this.showInstancesCheckBox.isSelected();
        model.setChanged();
        model.notifyObservers();
}//GEN-LAST:event_showInstancesCheckBoxActionPerformed

    private void contourLevelSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_contourLevelSliderStateChanged
        model.setChanged();
        model.notifyObservers();
    }//GEN-LAST:event_contourLevelSliderStateChanged

    private void combineLevelsCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_combineLevelsCheckBoxActionPerformed
        model.setChanged();
        model.notifyObservers();
    }//GEN-LAST:event_combineLevelsCheckBoxActionPerformed

    private void runForceButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runForceButtonActionPerformed
        boolean s = runForce();
        if (!s) {
            this.runForceButton.setIcon(new javax.swing.ImageIcon(getClass().
                    getResource("/toolbarButtonGraphics/media/Stop16.gif")));
            this.runForceButton.setToolTipText("Stop Force Directed Layout");
        } else {
            this.runForceButton.setIcon(new javax.swing.ImageIcon(getClass().
                    getResource("/toolbarButtonGraphics/media/Play16.gif")));
            this.runForceButton.setToolTipText("Run Force Directed Layout");
        }
}//GEN-LAST:event_runForceButtonActionPerformed

    private void showContourCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showContourCheckBoxActionPerformed
        if (showContourCheckBox.isSelected()) {
            contourPanel.setVisible(true);
            contourLevelSlider.setVisible(true);
        }else {
            contourPanel.setVisible(false);
            contourLevelSlider.setVisible(false);
            showInstancesCheckBox.setSelected(true);
            view.showInstances = true;
        }
        model.setChanged();
        model.notifyObservers();
    }//GEN-LAST:event_showContourCheckBoxActionPerformed

    private void changeColorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeColorButtonActionPerformed
        if (view.selection != null)
            view.selection.selected(((LabeledProjectionModel)getModel()).getSelectedInstances());
        model.setChanged();
    }//GEN-LAST:event_changeColorButtonActionPerformed

    private void changeColorButtonFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_changeColorButtonFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_changeColorButtonFocusLost

    private void viewRadVizMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewRadVizMenuItemActionPerformed
        if (((LabeledProjectionModel)model).getRadvizStructure() != null) {
            showRadViz = !showRadViz;
            viewRadVizMenuItem.setSelected(showRadViz);
            model.setChanged();
            model.notifyObservers();
        }else {
            viewRadVizMenuItem.setSelected(false);
        }
    }//GEN-LAST:event_viewRadVizMenuItemActionPerformed

    private void changeAnchorOrderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeAnchorOrderButtonActionPerformed
        if (((LabeledProjectionModel)model).getRadvizStructure() != null) {
            ArrayList<DragDropListItem> colors = ((LabeledProjectionModel)model).getRadvizStructure().getColorAnchors();
            if (colors != null && !colors.isEmpty()) {
                ChangeOrderAnchorDialog.getInstance(this,colors).display();
            }
        }
    }//GEN-LAST:event_changeAnchorOrderButtonActionPerformed

    private void changeAnchorOrderButtonFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_changeAnchorOrderButtonFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_changeAnchorOrderButtonFocusLost

    private void correlationMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_correlationMenuItemActionPerformed
        if (model != null) {
            try {
                CorrelationCoefficientView.getInstance(this).display((LabeledProjectionModel) model);
            } catch (IOException ex) {
                Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_correlationMenuItemActionPerformed

    private void trainLWPRModelMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_trainLWPRModelMenuItemActionPerformed
        if (model.getSelectedInstances() == null || model.getSelectedInstances().isEmpty()) {
            JOptionPane.showMessageDialog(controlPanel,"No instances selected!");
            return;
        }
        CreateUpdateLWPRModelDialog.getInstance(this,(LabeledProjectionModel)model).display();
    }//GEN-LAST:event_trainLWPRModelMenuItemActionPerformed

    private void lwprClassifierMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lwprClassifierMenuItemActionPerformed
        if (model == null ||
            ((LabeledProjectionModel)model).getInstances() == null || 
            ((LabeledProjectionModel)model).getInstances().isEmpty()) {
            JOptionPane.showMessageDialog(controlPanel,"No instances available!");
            return;
        }
        LWPRClassifierDialog diag = LWPRClassifierDialog.getInstance(this,(LabeledProjectionModel)model);
        diag.display();
        if (diag.getNumberScalars() > 0) {
            for (int i=0;i<diag.getNumberScalars();i++) {
                this.updateScalars(diag.getNscalar(i));
            }
            model.notifyObservers();
        }
    }//GEN-LAST:event_lwprClassifierMenuItemActionPerformed

    private void classificationEvaluationMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_classificationEvaluationMenuItemActionPerformed
        if (model == null ||
            ((LabeledProjectionModel)model).getInstances() == null || 
            ((LabeledProjectionModel)model).getInstances().isEmpty()) {
            JOptionPane.showMessageDialog(controlPanel,"No instances available!");
            return;
        }
        ClassificationEvaluationDialog diag = ClassificationEvaluationDialog.getInstance(this);
        diag.display();
    }//GEN-LAST:event_classificationEvaluationMenuItemActionPerformed

    @Override
    public void setModel(AbstractModel model) {
        if (model instanceof LabeledProjectionModel) {
            if (model != null) {
                Dimension size = getSize();
                size.height = (int) (size.height * 0.75f);
                size.width = (int) (size.width * 0.95f);
                
                if (((LabeledProjectionModel) model).getRadvizStructure() != null)
                    ((LabeledProjectionModel) model).fitRadVizToSize(size);
                else
                    ((LabeledProjectionModel) model).fitToSize(size);

                super.setModel(model);

                Scalar scalar = ((LabeledProjectionModel) model).getSelectedScalar();

                if (scalar != null) {
                    updateScalars(scalar);
                } else {
                    updateScalars(((LabeledProjectionModel) model).getScalars().get(0));
                }

                if (((LabeledProjectionModel)model).getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY) != null) {
                    ProjectionConnectivity dotsCon = new ProjectionConnectivity(LabeledProjectionConstants.DOTS, new ArrayList<Edge>());
                    ProjectionConnectivity delaunayCon = (ProjectionConnectivity) ((LabeledProjectionModel)model).getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY);
                    edgeComboModel.removeAllElements();
                    edgeComboModel.addElement(dotsCon);
                    edgeComboModel.addElement(delaunayCon);
                    edgeComboModel.setSelectedItem(delaunayCon);

                    edgeLabel.setVisible(true);
                    edgeCombo.setVisible(true);
                }

                view.setModel((LabeledProjectionModel) model);

            }
        }
    }

    public void addSelection(final AbstractSelection selection, boolean state) {
        if (selection != null) {
            JToggleButton button = new JToggleButton();
            selectionButtonGroup.add(button);
            button.setIcon(selection.getIcon());
            button.setSelected(false);
            button.setToolTipText(selection.toString());
            button.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (view != null) {
                        view.setSelection(selection);
                    }
                }
            });
            button.setSelected(state);
            selectionToolBar.add(button);
        }
    }

    public void updateScalars(Scalar scalar) {
        scalarComboModel.removeAllElements();
        for (Scalar s : ((LabeledProjectionModel) model).getScalars()) {
            scalarComboModel.addElement(s);
        }

        if (scalar != null) {
            scalarCombo.setSelectedItem(scalar);
            ((LabeledProjectionModel) model).setSelectedScalar(scalar);
        } else {
            scalarCombo.setSelectedItem(((LabeledProjectionModel) model).getSelectedScalar());
        }

        model.setChanged();
        model.notifyObservers();
    }

    public void updateSlider(Scalar scalar) {
        ArrayList<Float> scalars = getScalars(scalar);

        contourLevelSlider.setMaximum(scalars.size());
        contourLevelSlider.setValue(scalars.size());

        if (scalars.size() <= 15) {
            contourLevelSlider.setPaintLabels(true);
            Hashtable labelTable = new Hashtable();
            labelTable.put(new Integer(0),new JLabel("N"));
            System.out.println("Available Scalars [");
            for (int i=0;i<scalars.size();i++) {
                //System.out.print(scalars.get(i)+" ");
                //labelTable.put(new Integer(scalars.get(i).intValue()),new JLabel(scalars.get(i).toString()));
                labelTable.put(new Integer(i+1),new JLabel(Integer.toString(scalars.get(i).intValue())));
            }
            //System.out.println("] ");
            contourLevelSlider.setLabelTable(labelTable);
        }else {
            contourLevelSlider.setPaintLabels(false);
        }


//        if (scalars.size() <= 15) {
//            contourLevelSlider.setPaintLabels(true);
//            Enumeration e = contourLevelSlider.getLabelTable().keys();
//            while (e.hasMoreElements()) {
//                Integer i = (Integer) e.nextElement();
//                JLabel label = (JLabel) contourLevelSlider.getLabelTable().get(i);
//                if (i == 0)
//                    label.setText("N");
//                else
//                    label.setText(Integer.toString((scalars.get(i-1).intValue())));
//            }
//        }else {
//            contourLevelSlider.setPaintLabels(false);
//        }

        
    }

    public void setViewerBackground(Color bg) {
        if (view != null) {
            view.setBackground(bg);
            view.cleanImage();
            view.repaint();
        }
    }

    public Scalar getCurrentScalar() {
        return (Scalar) scalarCombo.getSelectedItem();
    }

    public ViewPanel getView() {
        return view;
    }

    public boolean isHighQualityRender() {
        return highqualityrender;
    }

    public void setHighQualityRender(boolean highqualityrender) {
        this.highqualityrender = highqualityrender;

        view.cleanImage();
        view.repaint();
    }

    public void setDrawAs(int d) {
        ArrayList<AbstractInstance> instances = getModel().getInstances();
        if ((instances != null)&&(!instances.isEmpty())) {
            for (int i=0;i<instances.size();i++)
                if (instances.get(i) instanceof LabeledProjectionInstance)
                    ((LabeledProjectionInstance)instances.get(i)).setDrawAs(d);
        }
        view.cleanImage();
        view.repaint();
    }

    public boolean isShowInstanceLabel() {
        return showinstancelabel;
    }

    public void setShowInstanceLabel(boolean showinstancelabel) {
        this.showinstancelabel = showinstancelabel;
        this.showinstanceImage = !showinstancelabel;
        view.cleanImage();
        view.repaint();
    }

    public void setShowInstanceImage(boolean showinstanceImage) {
        this.showinstanceImage = showinstanceImage;
        this.showinstancelabel = !showinstanceImage;
        view.cleanImage();
        view.repaint();
    }

    public boolean isMoveInstances() {
        return moveinstances;
    }

    public void setMoveInstance(boolean moveinstances) {
        this.moveinstances = moveinstances;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (model != null) {
            view.cleanImage();
            view.repaint();
        }
    }

    public void changeStatus(String status) {
        if (status.contains("<br>")) {
            statusBar_jPanel.setMinimumSize(new Dimension(30,50));
            statusBar_jPanel.setPreferredSize(new Dimension(30,50));
        }else {
            statusBar_jPanel.setMinimumSize(new Dimension(30,30));
            statusBar_jPanel.setPreferredSize(new Dimension(30,30));
        }
        this.status_jLabel.setText("<html><center>"+status+"</center></html>");
        this.status_jLabel.update(this.status_jLabel.getGraphics());
        Rectangle r = this.status_jLabel.getGraphicsConfiguration().getBounds();
        //this.status_jLabel.getGraphics().fillRect(r.x, r.y, r.width, r.height);
        this.status_jLabel.getGraphics().clearRect(r.x, r.y, r.width, r.height);
        this.status_jLabel.update(this.status_jLabel.getGraphics());
    }

    public boolean runForce() {
        if (model != null) {
            if (((LabeledProjectionModel)model).getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY) != null) {
                if (start) {
                    for (AbstractInstance ai : model.getInstances()) {
                        LabeledProjectionInstance li = (LabeledProjectionInstance) ai;
                        if (li.fdata == null) {
                            li.fdata = new ForceData();
                        }
                    }

                    force = new ForceDirectLayout((LabeledProjectionModel) model, this);
                    force.start(((LabeledProjectionModel)model).getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY));
                    this.start = false;
                } else {
                    this.force.stop();
                    this.start = true;
                }
            }
        }
        model.setChanged();
        model.notifyObservers();

        return this.start;
    }

    public void updateImage() {
        if (this.view != null) {
            this.view.cleanImage();
            this.view.adjustPanel();
            this.view.repaint();
        }
    }

    void setDrawFrame(boolean selected) {

         ArrayList<AbstractInstance> instances = getModel().getInstances();
         if ((instances != null)&&(!instances.isEmpty())) {
             for (int i=0;i<instances.size();i++) {
                 if (instances.get(i) instanceof LabeledProjectionInstance) {
                     ((LabeledProjectionInstance)instances.get(i)).setDrawFrame(selected);
                 }
             }
         }
        view.cleanImage();
        view.repaint();
    }

    private void createHCScalar(HierarchicalClusteringType hierarchicalClusteringType) {
        if (model != null) {
            try {
                javax.swing.JOptionPane.showMessageDialog(this,
                        "The Hierachical Clustering is a very expensive process." +
                        "\nIt can take several minutes!",
                        "WARNING", javax.swing.JOptionPane.WARNING_MESSAGE);

                float[][] projection = new float[model.getInstances().size()][];
                for (int i = 0; i < model.getInstances().size(); i++) {
                    projection[i] = new float[2];
                    projection[i][0] = ((LabeledProjectionInstance)model.getInstances().get(i)).getX();
                    projection[i][1] = ((LabeledProjectionInstance)model.getInstances().get(i)).getY();
                }

                DenseMatrix dproj = new DenseMatrix();
                for (int i = 0; i < projection.length; i++) {
                    dproj.addRow(new DenseVector(projection[i]));
                }

                HierarchicalClustering hc = new HierarchicalClustering(hierarchicalClusteringType);
                float[] hcScalars = hc.getPointsHeight(dproj, new Euclidean());

                String scalarname = "hc-slink";
                if (hierarchicalClusteringType == HierarchicalClusteringType.ALINK) scalarname = "hc-alink";
                else if (hierarchicalClusteringType == HierarchicalClusteringType.CLINK) scalarname = "hc-clink";

                Scalar scalar = ((LabeledProjectionModel)model).addScalar(scalarname);

                for (int i=0;i<model.getInstances().size();i++)
                    ((LabeledProjectionInstance)model.getInstances().get(i)).setScalarValue(scalar, hcScalars[i]);
                
                this.updateScalars(scalar);
                model.notifyObservers();
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private ArrayList<Float> getScalars(Scalar scalar) {
        ArrayList<Float> ret = new ArrayList<Float>();
        for (int i=0;i<model.getInstances().size();i++) {
            LabeledProjectionInstance li = (LabeledProjectionInstance) model.getInstances().get(i);
            if (!ret.contains(li.getScalarValue(scalar)))
                ret.add(li.getScalarValue(scalar));
        }
        Collections.sort(ret);
        return ret;
    }

    public void setShowDensity(boolean selected) {
        showDensity = selected;
    }

    public boolean getShowDensity() {
        return showDensity;
    }

    public class ViewPanel extends JPanel {

        public ViewPanel() {
            this.selcolor = java.awt.Color.RED;
            this.setBackground(java.awt.Color.WHITE);

            this.addMouseMotionListener(new MouseMotionListener());
            this.addMouseListener(new MouseClickedListener());

            this.setLayout(new FlowLayout(FlowLayout.LEFT));
        }

        @Override
        public void paintComponent(java.awt.Graphics g) {
            super.paintComponent(g);

            java.awt.Graphics2D g2 = (java.awt.Graphics2D) g;

            if (model != null && image == null) {
                Dimension size = ((LabeledProjectionModel) model).getSize();

                if (size.height > size.width)
                    size.width = size.height;
                else
                    size.height = size.width;

                image = new BufferedImage(size.width + 10, size.height + 10,
                        BufferedImage.TYPE_INT_RGB);

                java.awt.Graphics2D g2Buffer = image.createGraphics();
                g2Buffer.setColor(this.getBackground());
                g2Buffer.fillRect(0, 0, size.width + 10, size.height + 10);

                if (highqualityrender) {
                    g2Buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);
                } else {
                    g2Buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_OFF);
                }

                //((LabeledProjectionModel) model).draw(image, highqualityrender);
//                ProjectionConnectivity del = (ProjectionConnectivity)((LabeledProjectionModel)model).getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY);
//                if (del != null) {
//                    if (LabeledProjectionFrame.this.edgeCombo.getSelectedItem().equals(del)) {
//                    del.draw((LabeledProjectionModel)model, image, highqualityrender);
//                }
//                }

                if (showRadViz)
                    //drawRadViz(image);
                    ((LabeledProjectionModel) model).drawAnchors(image,highqualityrender);

                if (showInstances)
                    ((LabeledProjectionModel) model).draw(image, highqualityrender);

                //Scalar sc = ((LabeledProjectionModel) model).getSelectedScalar();

                if (showContourCheckBox.isSelected()) {
                    if (mesh2 != null && contourLevelSlider != null && contourLevelSlider.getValue() != 0) {
                        boolean sel = false;
                        if (combineLevelsCheckBox.isSelected()) {
                            for (int i=1;i<=contourLevelSlider.getValue();i++) {
                                if (levelSelected == (float)i-1) sel = true;
                                //mesh2.draw(image,((LabeledProjectionModel) model).getSelectedScalar(),(float)i-1,sel);
                                mesh2.draw(image,((LabeledProjectionModel) model).getSelectedScalar(),Float.parseFloat(((JLabel)contourLevelSlider.getLabelTable().get(i)).getText()),sel);
                            }
                        }else {
    //                        System.out.println(contourLevelSlider.getValue()-1);
    //                        System.out.println(((JLabel)contourLevelSlider.getLabelTable().get(contourLevelSlider.getValue())).getText());
                            if (levelSelected == (float)contourLevelSlider.getValue()-1) sel = true;
                            //mesh2.draw(image,((LabeledProjectionModel) model).getSelectedScalar(),(float)contourLevelSlider.getValue()-1,sel);
                            mesh2.draw(image,((LabeledProjectionModel) model).getSelectedScalar(),Float.parseFloat(((JLabel)contourLevelSlider.getLabelTable().get(contourLevelSlider.getValue())).getText()),sel);
                        }
                    }
                }

//                if (mesh != null)
//                    for (int k=0;k<=9;k++) {
//                        for (int i=0;i<mesh.size();i++) {
//                            mesh.get(i).drawContour(image,((LabeledProjectionModel) model).getSelectedScalar(),(float)k);
//                        }
//                    }

                g2Buffer.dispose();
            }

            if (image != null) {
                g2.drawImage(image, 0, 0, null);
            }

            //Draw he rectangle to select the instances
            if (showInstances) {
                if (selsource != null && seltarget != null) {
                    int x = selsource.x;
                    int width = width = seltarget.x - selsource.x;

                    int y = selsource.y;
                    int height = seltarget.y - selsource.y;

                    if (selsource.x > seltarget.x) {
                        x = seltarget.x;
                        width = selsource.x - seltarget.x;
                    }

                    if (selsource.y > seltarget.y) {
                        y = seltarget.y;
                        height = selsource.y - seltarget.y;
                    }
                    g2.setColor(selcolor);
                    g2.drawRect(x, y, width, height);

                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.45f));
                    g2.fillRect(x, y, width, height);
                } else { //Draw the instance label
                    if (showinstancelabel && labelText != null && labelpos != null) {
                        int style = java.awt.Font.PLAIN;
                        if (labelBold) style = java.awt.Font.BOLD;
                        java.awt.Font f = new java.awt.Font(this.getFont().getName(),style,labelSize);
                        g2.setFont(f);
                        //g2.setFont(this.getFont());
                        //java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());
                        java.awt.FontMetrics metrics = g2.getFontMetrics(f);

                        //Getting the label size
                        int width = metrics.stringWidth(labelText);
                        int height = metrics.getAscent();

                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
                        g2.setPaint(java.awt.Color.WHITE);
                        g2.fillRect(labelpos.x - 2, labelpos.y - height, width + 4, height + 4);
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));

                        g2.setColor(java.awt.Color.DARK_GRAY);
                        g2.drawRect(labelpos.x - 2, labelpos.y - height, width + 4, height + 4);

                        //Drawing the label
                        g2.drawString(labelText, labelpos.x, labelpos.y);
                    }else if (showinstanceImage && labelImage != null && labelpos != null) {
                        g2.drawImage(this.labelImage.getScaledInstance(100,100,0),labelpos.x,labelpos.y,null);
                    }
                }

                //drawn the selection polygon
                if (selpolygon != null) {
                    g2.setColor(selcolor);
                    g2.drawPolygon(selpolygon);

                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.45f));
                    g2.fillPolygon(selpolygon);
                }
            }
            //Draw anchor label...
//            if (showRadViz) {
//                if (selectedAnchor != null && selectedAnchorLabelPosition != null)
//                    selectedAnchor.drawLabel(null,selectedAnchorLabelPosition.x,selectedAnchorLabelPosition.y);
//            }
        }

        public void saveToPngImageFile(String filename) throws IOException {
            try {
                Dimension size = ((LabeledProjectionModel) model).getSize();
                BufferedImage buffer = new BufferedImage(size.width + 10, size.height + 10,
                        BufferedImage.TYPE_INT_RGB);
                paint(buffer.getGraphics());
                ImageIO.write(buffer, "png", new File(filename));
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }

        public void cleanImage() {
            image = null;
        }

        public void setModel(LabeledProjectionModel model) {
            colorscale = new ColorScalePanel(null);
            colorscale.setColorTable(model.getColorTable());
            colorscale.setPreferredSize(new Dimension(200, 12));
            removeAll();
            add(colorscale);

            colorscale.setBackground(getBackground());

            Dimension size = null;

            if (model.getRadvizStructure() != null)
                size = model.getRadvizStructure().getDimension();
            else
                size = model.getSize();

            setPreferredSize(new Dimension(size.width * 2, size.height * 2));
            setSize(new Dimension(size.width * 2, size.height * 2));

            cleanImage();
            repaint();
        }

        public void setSelection(AbstractSelection selection) {
            this.selection = selection;
        }

        public void colorAs(Scalar scalar) {
            if (model != null) {
                ((LabeledProjectionModel) model).setSelectedScalar(scalar);
                model.notifyObservers();
            }
        }

        public void zoomIn() {
            if (model != null) {
                if (((LabeledProjectionModel)model).getRadvizStructure() != null) {
                    Dimension newD = ((LabeledProjectionModel)model).getRadvizStructure().getDimension();
                    newD.width *= 1.1f;
                    newD.height *= 1.1f;
                    float[][] radvizProjection = ((LabeledProjectionModel)model).getRadvizStructure().updateRadVizValues();
                    for (int i=0;i<((LabeledProjectionModel)model).getInstances().size();i++) {
                        LabeledProjectionInstance lpi = (LabeledProjectionInstance) ((LabeledProjectionModel)model).getInstances().get(i);
                        lpi.setX(radvizProjection[i][0]);
                        lpi.setY(radvizProjection[i][1]);
                    }
                    LabeledProjectionFrame.this.setModel((LabeledProjectionModel)getModel());
                }else
                    ((LabeledProjectionModel) model).zoom(1.1f);
                //Change the size of the panel according to the projection
                Dimension size = ((LabeledProjectionModel) model).getSize();
                Dimension newSize = new Dimension(size.width * 2, size.height * 2);
                setPreferredSize(newSize);
                setSize(newSize);
                model.notifyObservers();
            }
        }

        public void zoomOut() {
            if (model != null) {
                if (((LabeledProjectionModel)model).getRadvizStructure() != null) {
                    Dimension newD = ((LabeledProjectionModel)model).getRadvizStructure().getDimension();
                    newD.width *= 0.9091f;
                    newD.height *= 0.9091f;
                    float[][] radvizProjection = ((LabeledProjectionModel)model).getRadvizStructure().updateRadVizValues();
                    for (int i=0;i<((LabeledProjectionModel)model).getInstances().size();i++) {
                        LabeledProjectionInstance lpi = (LabeledProjectionInstance) ((LabeledProjectionModel)model).getInstances().get(i);
                        lpi.setX(radvizProjection[i][0]);
                        lpi.setY(radvizProjection[i][1]);
                    }
                    LabeledProjectionFrame.this.setModel((LabeledProjectionModel)getModel());

                    //setModel((LabeledProjectionModel)getModel());

                    //((LabeledProjectionModel) model).fitRadVizToSize(newD);
                }else
                    ((LabeledProjectionModel) model).zoom(0.9091f);
                //Change the size of the panel according to the projection
                Dimension size = ((LabeledProjectionModel) model).getSize();
                Dimension newSize = new Dimension(size.width * 2, size.height * 2);
                setPreferredSize(newSize);
                setSize(newSize);
                model.notifyObservers();
            }
        }

        public void adjustPanel() {
            float iniX = ((ProjectionInstance) model.getInstances().get(0)).getX();
            float iniY = ((ProjectionInstance) model.getInstances().get(0)).getY();
            float max_x = iniX, max_y = iniX;
            float min_x = iniY, min_y = iniY;
            int zero = 30;

            for (int i = 1; i < model.getInstances().size(); i++) {
                float x = ((ProjectionInstance) model.getInstances().get(i)).getX();
                if (max_x < x) {
                    max_x = x;
                } else if (min_x > x) {
                    min_x = x;
                }

                float y = ((ProjectionInstance) model.getInstances().get(i)).getY();
                if (max_y < y) {
                    max_y = y;
                } else if (min_y > y) {
                    min_y = y;
                }
            }

            for (AbstractInstance ai : model.getInstances()) {
                ProjectionInstance pi = (ProjectionInstance) ai;
                pi.setX(pi.getX() + zero - min_x);
                pi.setY(pi.getY() + zero - min_y);
            }

            Dimension d = this.getSize();
            d.width = (int) max_x + zero;
            d.height = (int) max_y + zero;
            setSize(d);
            setPreferredSize(d);

            model.notifyObservers();
        }

        public void cleanSelectedInstances() {
            if (model != null) {
                model.cleanSelectedInstances();
                model.notifyObservers();
            }
            for (int i=0;i<getCoordinators().size();i++) {
                getCoordinators().get(i).coordinate(model.getSelectedInstances(),null);
            }
        }

        public void removeSelectedInstances() {
            if (model != null) {
                model.removeSelectedInstances();
                model.notifyObservers();
            }
        }

        public ArrayList<ProjectionInstance> getSelectedInstances(Polygon polygon) {
            ArrayList<ProjectionInstance> selected = new ArrayList<ProjectionInstance>();

            if (model != null) {
                selected = ((LabeledProjectionModel) model).getInstancesByPosition(polygon);
            }

            return selected;
        }

        public ArrayList<ProjectionInstance> getSelectedInstances(Point source, Point target) {
            ArrayList<ProjectionInstance> selinstances = new ArrayList<ProjectionInstance>();

            if (model != null) {
                int x = Math.min(source.x, target.x);
                int width = Math.abs(source.x - target.x);

                int y = Math.min(source.y, target.y);
                int height = Math.abs(source.y - target.y);

                Rectangle rect = new Rectangle(x, y, width, height);
                selinstances = ((LabeledProjectionModel) model).getInstancesByPosition(rect);
            }

            return selinstances;
        }

        @Override
        public void setBackground(Color bg) {
            super.setBackground(bg);

            if (this.colorscale != null) {
                this.colorscale.setBackground(bg);
            }
        }

        public void removeInstancesWithScalar(float val) {
            if (model != null) {
                ArrayList<AbstractInstance> insts = new ArrayList<AbstractInstance>();
                Scalar scalar = ((LabeledProjectionModel) model).addScalar("cdata");
                for (AbstractInstance ai : model.getInstances()) {
                    if (((ProjectionInstance) ai).getScalarValue(scalar) == val) {
                        insts.add(ai);
                    }
                }

                model.removeInstances(insts);
                model.notifyObservers();
            }
        }

        class MouseMotionListener extends MouseMotionAdapter {

            @Override
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                super.mouseMoved(evt);
                if (model != null) {
                    LabeledProjectionInstance instance = (LabeledProjectionInstance)((LabeledProjectionModel) model).getInstanceByPosition(evt.getPoint());
                    if (instance != null) {
                        //Show the instance label
                        if (instance.isShowLabel()) {
                            labelText = instance.getLabel();
                            if (labelText.trim().length() > 0) {
                                //if (labelText.length() > 100) {
                                //    labelText = labelText.substring(0, 96) + "...";
                                //}
                                //In case of a RadViz projection, show the points coordinate...
                                if (((LabeledProjectionModel)model).getRadvizStructure() != null) {
                                    int row = ((LabeledProjectionModel)model).getRadvizStructure().getInstances().getIds().indexOf(instance.getId());
                                    if (row != -1) {
                                        AbstractVector rowInstance = ((LabeledProjectionModel)model).getRadvizStructure().getInstances().getRow(row);
                                        if (rowInstance != null) {
                                            ArrayList<AbstractVector> anchors = ((LabeledProjectionModel)model).getRadvizStructure().getAnchors();
                                            if (anchors != null && (anchors.size() == rowInstance.size())) {
                                                String l = " [";
                                                for (int i=0;i<anchors.size();i++) {
                                                    l += Float.toString(rowInstance.getValue(anchors.get(i).getId()))+",";
                                                }
                                                if (l.lastIndexOf(",") == l.length()-1)
                                                    l = l.substring(0,l.length()-1);
                                                l += "]";
                                                labelText += l;
                                            }
//                                            String l = " [";
//                                            for (int i=0;i<rowInstance.size();i++) {
//                                                l += Float.toString(rowInstance.getValue(i))+",";
//                                            }
//                                            if (l.lastIndexOf(",") == l.length()-1)
//                                                l = l.substring(0,l.length()-1);
//                                            l += "]";
//                                            labelText += l;
                                        }
                                    }
                                }
                                labelpos = evt.getPoint();
                                repaint();
                            }
                        }else {
                            if (instance instanceof LabeledProjectionInstance) {
                                if (instance.isShowImage()) {
                                    labelImage = null;
                                    try {
                                        Image im = instance.getImage(((LabeledProjectionModel) instance.getModel()).getImageCollection());
                                        if (im != null) {
                                            labelImage = im.getScaledInstance(100, 100, 0);
                                        }
                                        labelpos = evt.getPoint();
                                        repaint();
                                    } catch (IOException ex) {
                                        Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }
                        }
                    } else {
                        //Clear the label
                        labelText = null;
                        labelImage = null;
                        labelpos = null;
                        repaint();
                    }

                    //if (showRadViz)
                       //selectedAnchor = ((LabeledProjectionModel) model).getAnchorByPosition(evt.getPoint());
                       //if (selectedAnchor != null)
                       //    selectedAnchorLabelPosition = evt.getPoint();

//                    if (mesh2 != null) {
//                        levelSelected = mesh2.getLevelSelected(evt.getPoint(),((LabeledProjectionModel)model).getSelectedScalar());
//                        if (levelSelected != -1.0f) {
//                            System.out.println(" ");
//                            model.setChanged();
//                            model.notifyObservers();
//                        }
//                    }
                }
            }

            @Override
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                if (selinstance != null) {
                    if (model.hasSelectedInstances()) {
                        float x = evt.getX() - selinstance.getX();
                        float y = evt.getY() - selinstance.getY();

                        for (AbstractInstance ai : model.getSelectedInstances()) {
                            ProjectionInstance pi = (ProjectionInstance) ai;
                            pi.setX(x + pi.getX());
                            pi.setY(y + pi.getY());
                        }

                        adjustPanel();
                    }
                } else if (selsource != null) {
                    seltarget = evt.getPoint();
                } else if (selpolygon != null) {
                    selpolygon.addPoint(evt.getX(), evt.getY());
                }

                repaint();
            }
        }

        class MouseClickedListener extends MouseAdapter {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                super.mouseClicked(evt);

                if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
                    if (model != null) {
                        LabeledProjectionInstance instance = (LabeledProjectionInstance) ((LabeledProjectionModel) model).getInstanceByPosition(evt.getPoint());
                        if (instance != null) {
                            changeStatus("Number of Instances in Selection: " + 1);
                            if (evt.isControlDown()) {
                                ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
                                instances.addAll(model.getSelectedInstances());
                                model.cleanSelectedInstances();
                                if (instances.contains(instance))
                                    instances.remove(instance);
                                else
                                    instances.add(instance);
                                model.setSelectedInstances(instances);
                                if (selection != null)
                                    selection.selected(instances);
                                model.setChanged();
                                float perc = 100.0f * (instances.size() / ((float) model.getInstances().size()));
                                DecimalFormat df = new DecimalFormat("0.##");
                                String densityInformation = "";
                                if (showDensity && instances != null && !instances.isEmpty()) {
                                    try {
                                        densityInformation = getDensityValues(instances);
                                    } catch (IOException ex) {
                                        Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                                changeStatus("Number of Instances in Selection: " + instances.size() + " (" + df.format(perc) + "%) "+densityInformation);
                            }else {
                                model.setSelectedInstance(instance);
                                if (selection != null) {
                                    ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
                                    instances.add(instance);
                                    selection.selected(instances);
                                }
//                                float perc = 100.0f * (1 / ((float) model.getInstances().size()));
//                                DecimalFormat df = new DecimalFormat("0.##");
//                                changeStatus("Number of Instances in Selection: 1 (" + df.format(perc) + "%)");
                                int numSelInst = 1;
                                if (model.getSelectedInstances() != null)
                                    numSelInst = model.getSelectedInstances().size();
                                float perc = 100.0f * (numSelInst / ((float) model.getInstances().size()));
                                DecimalFormat df = new DecimalFormat("0.##");
                                changeStatus("Number of Instances in Selection: "+numSelInst+" (" + df.format(perc) + "%)");
                            }
                            model.notifyObservers();
                        }
                    }
                } else if (evt.getButton() == java.awt.event.MouseEvent.BUTTON3) {
                    cleanSelectedInstances();
                    changeStatus("Number of Instances in Selection: " + 0);
                }
            }

            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                super.mousePressed(evt);

                if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
                    if (model != null) {
                        LabeledProjectionInstance instance = (LabeledProjectionInstance) ((LabeledProjectionModel) model).getInstanceByPosition(evt.getPoint());

                        if (instance != null) {
                            if (moveinstances) {
                                if (model.getSelectedInstances().contains(instance)) {
                                    selinstance = instance;
                                }
                            }
                        } else {
                            selsource = evt.getPoint();
                        }
                    }
                } else if (evt.getButton() == java.awt.event.MouseEvent.BUTTON3) {
                    selpolygon = new java.awt.Polygon();
                    selpolygon.addPoint(evt.getX(), evt.getY());
                }
            }

            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                super.mouseReleased(evt);

                if (model != null) {
                    if ((selsource != null && seltarget != null) || selpolygon != null) {
                        ArrayList<ProjectionInstance> instances = null;

                        if (selpolygon != null) {
                            instances = getSelectedInstances(selpolygon);
                        } else {
                            instances = getSelectedInstances(selsource, seltarget);
                        }

                        if (instances != null) {
                            if (selection != null) {
                                selection.selected(new ArrayList<AbstractInstance>(instances));
                            }
                            //float perc = 100.0f * (instances.size() / ((float) model.getInstances().size()));
                            float perc = 100.0f * (model.getSelectedInstances().size()/((float) model.getInstances().size()));
                            DecimalFormat df = new DecimalFormat("0.##");
                            String densityInformation = "";
                            if (showDensity && instances != null && !instances.isEmpty()) {
                                try {
                                    densityInformation = getDensityValues(instances);
                                } catch (IOException ex) {
                                    Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            //changeStatus("Number of Instances in Selection: " + instances.size() + " (" + df.format(perc) + "%) "+densityInformation);
                            changeStatus("Number of Instances in Selection: " + model.getSelectedInstances().size() + " (" + df.format(perc) + "%) "+densityInformation);
                        }
                    }
                }

                selpolygon = null;
                selinstance = null;
                selsource = null;
                seltarget = null;
            }
        }
        
        private ProjectionInstance selinstance;
        private Polygon selpolygon;
        private Point selsource;
        private Point seltarget;
        private Color selcolor;
        private String labelText;
        private boolean labelBold = false;
        private int labelSize = 12;
//        private int imageSize = 4;
        private Image labelImage;
        private Point labelpos;
        private BufferedImage image;
        private ColorScalePanel colorscale;
        private AbstractSelection selection;
        private Mesh mesh2;
        private float levelSelected = -1.0f;
        //private ArrayList<MarchingTriangle> mesh;
        private boolean showInstances = true;
    }

    public boolean isShowInstanceImage() {
        return showinstanceImage;
    }

    public void setShowInstances(boolean label, boolean image) {
        showinstanceImage = image;
        showinstancelabel = label;
    }

    public int setlabelType(int option) {
        ArrayList<AbstractInstance> instances = getModel().getInstances();
        if ((instances != null)&&(!instances.isEmpty())) {
            switch (option) {
                case 0:
                    if (this.isShowInstanceImage() || this.isShowInstanceLabel()) {
                        for (int i=0;i<instances.size();i++) {
                            if (instances.get(i) instanceof LabeledProjectionInstance) {
                                ((LabeledProjectionInstance)instances.get(i)).setShowLabel(false);
                                ((LabeledProjectionInstance)instances.get(i)).setShowImage(false);
                            }
                        }
                        this.setShowInstances(false,false);
                    }
                    return 0;
                case 1:
                    if (!this.isShowInstanceLabel()) {
                        this.setShowInstanceLabel(true);
                        for (int i=0;i<instances.size();i++) {
                            if (instances.get(i) instanceof LabeledProjectionInstance) {
                                ((LabeledProjectionInstance)instances.get(i)).setShowLabel(true);
                                ((LabeledProjectionInstance)instances.get(i)).setShowImage(false);
                            }
                        }
                    }
                    return 1;
                case 2:
                    if (!this.isShowInstanceImage()) {
                        int oldIndex = 0;
                        if (isShowInstanceLabel()) oldIndex = 1;
                        else if (isShowInstanceImage()) oldIndex = 2;
                        ImageCollection im = ((LabeledProjectionModel)model).getImageCollection();
                        if ((im == null)||(!ProjectionOpenDialog.checkImages(im,model))) {
                            try {
                                PropertiesManager spm = PropertiesManager.getInstance(LabeledProjectionConstants.PROPFILENAME);
                                im = ProjectionOpenDialog.openImages(spm,new ZIPFilter(),this);
                                if (im == null) {
                                    //Usuario desistiu de aplicar as imagens, voltar opcao antiga
                                    return oldIndex;
                                }else {
                                    if (ProjectionOpenDialog.checkImages(im,model)) {
                                       ((LabeledProjectionModel)model).setImageCollection(im);
                                       ((ReportView)(getReportPanel())).setDataSource(im.getFilename());
                                       this.setShowInstanceImage(true);
                                       //Populando imagem na instancia...
                                       for (int i=0;i<instances.size();i++) {
                                           if (instances.get(i) instanceof LabeledProjectionInstance) {
    //                                           Image image = im.getImage(((LabeledProjectionInstance)instances.get(i)).getLabel());
    //                                           ((LabeledProjectionInstance)instances.get(i)).setImage(image);
                                               ((LabeledProjectionInstance)instances.get(i)).setShowLabel(false);
                                               ((LabeledProjectionInstance)instances.get(i)).setShowImage(true);
                                           }
                                       }
                                   }else {
                                       JOptionPane.showMessageDialog(this,
                                            "The image collection file do not correspond to the projected image collection!",
                                            "Error",
                                            JOptionPane.ERROR_MESSAGE,null);
                                       //colecao escolhida nao correspondeo a colecao projetada, voltar opcao antiga
                                       return oldIndex;
                                   }
                                }
                            } catch (IOException ex) {
                                Logger.getLogger(LabeledProjectionFrameOptions.class.getName()).log(Level.SEVERE, null, ex);
                                //voltar opcao antiga
                                return oldIndex;
                            }
                        }else {
                            this.setShowInstanceImage(true);
                            for (int i=0;i<instances.size();i++) {
                                if (instances.get(i) instanceof LabeledProjectionInstance) {
    //                                try {
    //                                    Image image = im.getImage(((LabeledProjectionInstance)instances.get(i)).getLabel());
    //                                    ((LabeledProjectionInstance)instances.get(i)).setImage(image);
                                        ((LabeledProjectionInstance)instances.get(i)).setShowLabel(false);
                                        ((LabeledProjectionInstance)instances.get(i)).setShowImage(true);
    //                                } catch (IOException ex) {
    //                                   Logger.getLogger(ProjectionFrameOptions.class.getName()).log(Level.SEVERE, null, ex);
    //                                   //voltar opcao antiga
    //                                   return oldIndex;
    //                                }
                                }
                            }
                        }
                        return 2;
                    }
            }
            getView().cleanImage();
            getView().repaint();
        }
        return option;
    }

    public void setLabelSize(int size) {
        this.view.labelSize = size;
    }

    public void setLabelBold(boolean o) {
        this.view.labelBold = o;
    }

    public int getLabelSize() {
        return this.view.labelSize;
    }

    public boolean getLabelBold() {
        return this.view.labelBold;
    }

    public javax.swing.JPanel getReportPanel() {
        return reportPanel;
    }

    private void initExtraComponents() {

        createKmeansLabelsButton = new javax.swing.JButton();
        createKmeansLabelsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/About16.gif"))); // NOI18N
        createKmeansLabelsButton.setToolTipText("Group instances using Kmeans");
        createKmeansLabelsButton.setFocusable(false);
        createKmeansLabelsButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        createKmeansLabelsButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        createKmeansLabelsButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createKmeansLabelsButtonActionPerformed(evt);
            }
        });
        selectionToolBar.add(createKmeansLabelsButton);

    }

    private void createKmeansLabelsButtonActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            KmeansClusters labels = new KmeansClusters((LabeledProjectionModel)getModel());
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            Scalar kmeansScalar = labels.execute(this);
            if (kmeansScalar != null)
                updateScalars(kmeansScalar);
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        } catch (IOException ex) {
            Logger.getLogger(LabeledProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String getDensityValues(ArrayList instances) throws IOException {

        //Calculating Rn density: (num instances)/Da
        //Da = area of the cluster in Rn = larger distance between instances
        float dn = 0, d2 = 0;
        String source = ((ReportView)this.reportPanel).getDataSourceValue();
        if (source != null && !source.isEmpty()) {
            DistanceMatrix dmat = null;
            if (source.endsWith(".data")) {
                AbstractMatrix matrix = MatrixFactory.getInstance(source);
                if (matrix != null) {
                    dmat = new DistanceMatrix(matrix,DissimilarityFactory.getInstance(DissimilarityType.EUCLIDEAN));
                }
            }else if (source.endsWith(".dmat")) {
                dmat = new DistanceMatrix(source);
            }
            if (dmat != null) {
                //Filtering instances that do not belong to the selected cluster...
                for (int i=0;i<dmat.getElementCount();i++) {
                    if (!isInstance(dmat.getIds().get(i),instances)) {
                        dmat.removeElement(dmat.getIds().get(i));
                        i--;
                    }
                }
            }
            if (dmat.getElementCount() > 1) {
//                dmat.save("E:\\dmatResult.dmat");
//                System.out.println("Max distance: "+dmat.getMaxDistance());
//                System.out.println("Min distance: "+dmat.getMinDistance());
                dn = instances.size()/dmat.getMaxDistance();
            }
        }
        //Calculating D2 density: (num_instances)/Db
        //Db = area of the cluster in R2 = sum of the triangles areas, in the triangulation of the selected cluster...
        ProjectionConnectivity delaunayCon = createDelaunayConnectivity(instances);
        if (delaunayCon != null) {
            Mesh m = new Mesh(delaunayCon,instances);
            if (m != null && m.getNumTriangles() > 0)
                d2 = instances.size()/m.getArea();
                //d2 = instances.size()/m.getNormalizedArea();
        }
        return "<br>Original Space Density: "+dn+", Projection Space Density: "+d2;
    }

    private boolean isInstance(int id, ArrayList<ProjectionInstance> instances) {

        for (int i=0;i<instances.size();i++)
            if (instances.get(i).getId() == id)
                return true;
        return false;

    }

    protected DefaultComboBoxModel scalarComboModel;
    protected DefaultComboBoxModel edgeComboModel;
    private boolean highqualityrender = true;
    private boolean showinstancelabel = true;
    private boolean showinstanceImage = false;
    private boolean moveinstances = true;
    private boolean showDensity = false;
    protected ViewPanel view;
    protected ForceDirectLayout force;
    protected boolean start = true;
    private javax.swing.JButton createKmeansLabelsButton;
    //ArrayList<Float> classesRadViz = null;
    private boolean showRadViz = false;
    //private LabeledProjectionAnchor selectedAnchor;
    //private Point selectedAnchorLabelPosition;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aLinkMenuItem;
    private javax.swing.JMenuItem cLinkMenuItem;
    private javax.swing.JButton changeAnchorOrderButton;
    private javax.swing.JButton changeColorButton;
    private javax.swing.JMenuItem classificationEvaluationMenuItem;
    private javax.swing.JMenu classifyMenu;
    private javax.swing.JButton cleanInstancesButton;
    private javax.swing.JMenu clusteringMenu;
    private javax.swing.JLabel colorLabel;
    private javax.swing.JCheckBox combineLevelsCheckBox;
    private javax.swing.JSlider contourLevelSlider;
    private javax.swing.JMenuItem contourMenuItem;
    private javax.swing.JPanel contourPanel;
    private javax.swing.JPanel controlPanel;
    private javax.swing.JMenuItem correlationMenuItem;
    private javax.swing.JMenu createHCMenu;
    protected javax.swing.JPanel dataPanel;
    private javax.swing.JMenuItem delaunayMenuItem;
    protected javax.swing.JComboBox edgeCombo;
    protected javax.swing.JLabel edgeLabel;
    private javax.swing.JMenuItem editClean;
    private javax.swing.JMenuItem editDelete;
    private javax.swing.JMenuItem exportDatasetMenuItem;
    protected javax.swing.JMenu exportMenu;
    private javax.swing.JMenuItem exportScalarsOption;
    private javax.swing.JMenuItem fileExportToPng;
    private javax.swing.JMenuItem fileExportToProjection;
    private javax.swing.JMenuItem fileOpen;
    private javax.swing.JMenuItem fileSave;
    private javax.swing.JButton findButton;
    private javax.swing.JPanel findPanel;
    private javax.swing.JTextField findTextField;
    protected javax.swing.JToolBar fixedToolBar;
    private javax.swing.JMenuItem generateDictionaryMenuItem;
    private javax.swing.JMenu importMenu;
    private javax.swing.JMenuItem importScalarsMenuItem;
    private javax.swing.JMenuItem importScalarsOption;
    private javax.swing.JMenuItem joinScalarsOptions;
    private javax.swing.JMenuItem lwprClassifierMenuItem;
    private javax.swing.JMenuItem memoryCheckMenuItem;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuEdit;
    protected javax.swing.JMenu menuFile;
    protected javax.swing.JMenu menuTool;
    private javax.swing.JToggleButton moveInstancesToggleButton;
    private javax.swing.JMenuItem multidimensionalMenuItem;
    private javax.swing.JButton openButton;
    protected javax.swing.JTabbedPane projectionTabbedPane;
    private javax.swing.JPanel reportPanel;
    protected javax.swing.JButton runForceButton;
    private javax.swing.JMenuItem sLinkMenuItem;
    private javax.swing.JButton saveButton;
    private javax.swing.JButton saveColorButton;
    protected javax.swing.JComboBox scalarCombo;
    private javax.swing.JMenu scalarMenu;
    protected javax.swing.JPanel scalarPanel;
    protected javax.swing.JScrollPane scrollPanel;
    protected javax.swing.ButtonGroup selectionButtonGroup;
    protected javax.swing.JToolBar selectionToolBar;
    private javax.swing.JSeparator separator1;
    private javax.swing.JLabel separatorLabel1;
    private javax.swing.JLabel separatorLabel2;
    private javax.swing.JLabel separatorLabel5;
    private javax.swing.JSeparator separatorOptions1;
    private javax.swing.JSeparator separatorOptions2;
    private javax.swing.JSeparator separatorOptions3;
    private javax.swing.JCheckBox showContourCheckBox;
    private javax.swing.JCheckBox showInstancesCheckBox;
    private javax.swing.JMenuItem silhouetteCoefficientMenuItem;
    private javax.swing.JPanel statusBar_jPanel;
    private javax.swing.JLabel status_jLabel;
    private javax.swing.JMenuItem stressMenuItem;
    private javax.swing.JMenuItem svmMenuItem;
    protected javax.swing.JToolBar toolBar;
    private javax.swing.JButton toolButton;
    private javax.swing.JMenuItem toolOptions;
    private javax.swing.JPanel toolbarPanel;
    private javax.swing.JMenuItem trainLWPRModelMenuItem;
    private javax.swing.JCheckBoxMenuItem viewRadVizMenuItem;
    private javax.swing.JButton zoomInButton;
    private javax.swing.JButton zoomOutButton;
    // End of variables declaration//GEN-END:variables
}
