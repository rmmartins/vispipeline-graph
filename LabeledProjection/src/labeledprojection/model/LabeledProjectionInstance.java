/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labeledprojection.model;

import graph.forcelayout.ForceData;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import labeledprojection.util.ImageCollection;
import projection.model.ProjectionInstance;
import projection.model.Scalar;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class LabeledProjectionInstance extends ProjectionInstance {

    public LabeledProjectionInstance(LabeledProjectionModel model, String label, int id, float x, float y) {
        ///super(model, id, x, y);
        super(id, x, y);
        this.setModel(model);
        model.addInstance(this);
        this.label = label;
        this.size = this.imageSize = 4;
        this.drawAs = 1;
        this.showlabel = true;
        this.pseudoColors = new HashMap<String,Color>();
    }

    public LabeledProjectionInstance(LabeledProjectionModel model, String label, int id) {
        this(model, label, id, 0, 0);
    }

    @Override
    public void draw(BufferedImage image, boolean highquality) {
        Graphics2D g2 = (Graphics2D) image.getGraphics();
        LabeledProjectionModel tmodel = (LabeledProjectionModel) model;
        if (highquality) g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        //int xaux = (((int)this.x)+image.getWidth())/2;
        //int yaux = (((int)this.y)+image.getHeight())/2;

        int xaux = (((int) this.x) <= 0) ? 1 : (((int) this.x) < image.getWidth()) ? (int) this.x : image.getWidth() - 1;
        int yaux = (((int) this.y) <= 0) ? 1 : (((int) this.y) < image.getHeight()) ? (int) this.y : image.getHeight() - 1;

        //xaux = (xaux + image.getWidth())/4;
        //yaux = (yaux + image.getHeight())/4;

        int inssize = tmodel.getInstanceSize();
        float alpha = tmodel.getAlpha();
        if ((tmodel.getSelectedInstances() == null)||(tmodel.getSelectedInstances().isEmpty())) alpha = 1.0f;

        Scalar sc = ((LabeledProjectionModel)this.getModel()).getSelectedScalar();
        if (sc != null) {
            if (sc.getName().startsWith("Pseudo")) {
                Color c = pseudoColors.get(sc.getName());
                if (c != null)
                    color = pseudoColors.get(sc.getName());
            }
        }


        switch (drawAs) {
            case 0:
                int rgb = color.getRGB();
                inssize /= 2;
                if (selected) {
                    alpha = 1.0f;
                    g2.setColor(Color.BLACK);
                    g2.drawRect(xaux-inssize-1,yaux-inssize-1,inssize*2+2,inssize*2+2);
                }
                for (int i=-inssize;i<=inssize;i++)
                    for (int j=-inssize;j<=inssize;j++)
                        simulateAlpha(image,alpha,xaux-i,yaux-j,rgb);
                break;
            case 1:
                if (selected) alpha = 1.0f;
                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, alpha));
                g2.setColor(color);
                g2.fillOval(xaux-inssize,yaux-inssize,inssize*2,inssize*2);
                g2.setColor(Color.BLACK);
                g2.drawOval(xaux-inssize,yaux-inssize,inssize*2,inssize*2);
                g2.setStroke(new BasicStroke(1.0f));
                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                break;
            case 2:
                if (this.image != null) {
                    Image img = this.image.getScaledInstance(this.getImageSize()*5,this.getImageSize()*5,0);
                    int w = img.getWidth(null);
                    int h = img.getHeight(null);
                    g2.drawImage(img,xaux-(w/2),yaux-(h/2),null);
                    //Desenhando o contorno colorido para mostrar a classe...
                    if (drawFrame) {
                        g2.setStroke(new BasicStroke(3.0f));
                        g2.setColor(this.getColor());
                        g2.drawRect(xaux-(w/2)-2,yaux-(h/2)-2,w+3,h+3);
                        g2.setStroke(new BasicStroke(1.0f));
                    }
                    //Desenhando o contorno colorido para mostrar a classe...
                    if (drawFrame) {
                        g2.setStroke(new BasicStroke(3.0f));
                        g2.setColor(this.getColor());
                        g2.drawRect(xaux -w/2-2,yaux-h/2-2,w+3,h+3);
                        g2.setStroke(new BasicStroke(1.0f));
                    }
                    if (selected) {
                        g2.setStroke(new BasicStroke(2.0f));
                        g2.setColor(Color.RED);
                        g2.drawRect(((int) this.x) - w / 2, ((int) this.y) - h / 2, w, h);
                        g2.setStroke(new BasicStroke(1.0f));
                    }
                }
                break;
        }
        
    }

    //TESTE RONAK
    public void draw(BufferedImage image, boolean highquality, Scalar sc) {
        Graphics2D g2 = (Graphics2D) image.getGraphics();
        LabeledProjectionModel tmodel = (LabeledProjectionModel) model;
        if (highquality) g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int xaux = (((int) this.x) <= 0) ? 1 : (((int) this.x) < image.getWidth()) ? (int) this.x : image.getWidth() - 1;
        int yaux = (((int) this.y) <= 0) ? 1 : (((int) this.y) < image.getHeight()) ? (int) this.y : image.getHeight() - 1;
        int inssize = tmodel.getInstanceSize();
        float alpha = tmodel.getAlpha();

        switch ((int)this.getScalarValue(sc)) {
            case -1: //WHITE
                color = new Color(255,255,255);
                break;
            case 0: //PURPLE
                color = new Color(124,61,131);
                break;
            case 1: //BLACK
                color = new Color(0,0,0);
                break;
            case 2: //LIGHT PINK
                color = new Color(220,141,219);
                break;
            case 3: //RED
                color = new Color(255,0,0);
                break;
            case 4: //GREEN
                color = new Color(43,219,8);
                break;
            case 5: //NAVY
                color = new Color(7,7,185);
                break;
            case 6: //CYAN
                color = Color.CYAN;
                break;
            case 7: //YELLOW
                color = new Color(255,240,0);
                break;
            case 8: //ORANGE
                color = new Color(255,144,0);
                break;
            case 9: //GRAY
                color = new Color(129,129,129);
                break;
            case 10:
                color = Color.DARK_GRAY;
                break;
            case 11:
                color = Color.MAGENTA;
                break;
            case 12:
                color = Color.LIGHT_GRAY;
                break;
            default:
        }

        //Scalar sc = ((LabeledProjectionModel)this.getModel()).getSelectedScalar();
        if (sc != null) {
            if (sc.getName().startsWith("Pseudo")) {
                Color c = pseudoColors.get(sc.getName());
                if (c != null)
                    color = pseudoColors.get(sc.getName());
            }
        }

        if ((tmodel.getSelectedInstances() == null)||(tmodel.getSelectedInstances().isEmpty())) alpha = 1.0f;
            switch (drawAs) {
                case 0:
                    int rgb = color.getRGB();
                    inssize /= 2;
                    if (selected) {
                        alpha = 1.0f;
                        g2.setColor(Color.BLACK);
                        g2.drawRect(xaux-inssize-1,yaux-inssize-1,inssize*2+2,inssize*2+2);
                    }
                    for (int i=-inssize;i<=inssize;i++)
                        for (int j=-inssize;j<=inssize;j++)
                            simulateAlpha(image,alpha,xaux-i,yaux-j,rgb);
                    break;
                case 1:
                    if (selected) alpha = 1.0f;
                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, alpha));
                    g2.setColor(color);
                    g2.fillOval(xaux-inssize,yaux-inssize,inssize*2,inssize*2);
                    g2.setColor(Color.BLACK);
                    g2.drawOval(xaux-inssize,yaux-inssize,inssize*2,inssize*2);
                    g2.setStroke(new BasicStroke(1.0f));
                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                    break;
                case 2:
                    if (this.image != null) {
                        Image img = this.image.getScaledInstance(this.getImageSize()*5,this.getImageSize()*5,0);
                        int w = img.getWidth(null);
                        int h = img.getHeight(null);
                        g2.drawImage(img,xaux-(w/2),yaux-(h/2),null);
                        //Desenhando o contorno colorido para mostrar a classe...
                        if (drawFrame) {
                            g2.setStroke(new BasicStroke(3.0f));
                            g2.setColor(this.getColor());
                            g2.drawRect(xaux-(w/2)-2,yaux-(h/2)-2,w+3,h+3);
                            g2.setStroke(new BasicStroke(1.0f));
                        }
                        //Desenhando o contorno colorido para mostrar a classe...
                        if (drawFrame) {
                            g2.setStroke(new BasicStroke(3.0f));
                            g2.setColor(this.getColor());
                            g2.drawRect(xaux -w/2-2,yaux-h/2-2,w+3,h+3);
                            g2.setStroke(new BasicStroke(1.0f));
                        }
                        if (selected) {
                            g2.setStroke(new BasicStroke(2.0f));
                            g2.setColor(Color.RED);
                            g2.drawRect(((int) this.x) - w / 2, ((int) this.y) - h / 2, w, h);
                            g2.setStroke(new BasicStroke(1.0f));
                        }
                    }
                    break;
            }
    }
    //TESTE RONAK

    @Override
    public void drawLabel(Graphics2D g2, int x, int y) {
        //Show the instance label

        if (label.trim().length() > 0) {
            if (label.length() > 100) {
                label = label.substring(0, 96) + "...";
            }
        }

        java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());

        int width = metrics.stringWidth(label);
        int height = metrics.getAscent();

        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
        g2.setPaint(java.awt.Color.WHITE);
        g2.fillRect(x + 3, y - 1 - height, width + 4, height + 4);
        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
        g2.setColor(java.awt.Color.DARK_GRAY);
        g2.drawRect(x + 3, y - 1 - height, width + 4, height + 4);
        g2.drawString(label, x + 3, y);
    }


    @Override
    public String toString() {
        return label;
    }

    @Override
    public boolean isInside(int x, int y) {
        switch (drawAs) {
            case 2:
                return (Math.abs(x - this.x) <= imageSize && Math.abs(y - this.y) <= imageSize);
            default:
                return (Math.abs(x - this.x) <= size && Math.abs(y - this.y) <= size);
        }
    }

    public void setDrawAs(int d) {
        drawAs = d;
    }

    public int getDrawAs() {
        return drawAs;
    }

    public void setImage(Image i) {
        image = i;
    }

    public Image getImage() {
        return image;
    }

    public Image getImage(ImageCollection im) throws IOException {
        if (im != null) {
            try {
            Image img = im.getImage(label);
            return img;
            }catch (Exception e) {
                return null;
            }
        }else return null;
        //return image;
    }

    public void setSize(int i) {
        size = i;
    }

    public int getSize() {
        return size;
    }

    public void setImageSize(int i) {
        imageSize = i;
    }

    public int getImageSize() {
        return imageSize;
    }

    public boolean getShowLabel() {
        return showlabel;
    }

    public boolean isShowImage() {
        return showImage;
    }

    public void setShowImage(boolean i) {
        showImage = i;
    }

    public void setDrawFrame(boolean selected) {
        drawFrame = selected;
    }

    public boolean isDrawFrame() {
        return drawFrame;
    }

    public void setLabel(String l) {
        label = l;
    }

    public String getLabel() {
        return label;
    }

    public void removeScalar(int index) {
        if (this.scalars.size() > index) {
            this.scalars.remove(index);
        }
    }

    public Color getPseudoColor(String name) {
        return pseudoColors.get(name);
    }

    public void setPseudoColor(String name, Color value) {
        pseudoColors.put(name,value);
    }

    protected int drawAs; //0: dot, 1: circle, 2:image
    private int size;
    protected String label;
    protected Image image;
    protected int imageSize;
    protected boolean drawFrame = false;
    protected boolean showImage = false;
    public ForceData fdata; //Use to repositioning the points
    protected HashMap<String,Color> pseudoColors;

}
