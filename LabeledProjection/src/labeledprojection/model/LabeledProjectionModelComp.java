/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.model;

import java.io.IOException;
import labeledprojection.technique.radviz.RadVizStructure;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import labeledprojection.util.LabeledProjectionConstants;
import textprocessing.corpus.Corpus;
import textprocessing.corpus.CorpusFactory;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import labeledprojection.util.ImageCollection;
import projection.model.Scalar;

/**
 *
 * @author PC
 */
@VisComponent(hierarchy = "Projection.Basics",
name = "Labeled Projection Model",
description = "Create a labeled projection model to be visualized.")
public class LabeledProjectionModelComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (projection != null || radvizStructure != null) {
            model = new LabeledProjectionModel();
            model.setSource(source);
            Scalar cdata = model.addScalar(LabeledProjectionConstants.CDATA);
            Scalar dots = model.addScalar(LabeledProjectionConstants.DOTS);
            Scalar dColor = model.addScalar(LabeledProjectionConstants.DYNAMIC_COLOR_SCALAR);

            if (projection == null) projection = radvizStructure.getInstances();

            if (projection != null) {

                int nrows = projection.getRowCount();

                for (int i = 0; i < nrows; i++) {
                    AbstractVector row = projection.getRow(i);
                    LabeledProjectionInstance lpi = new LabeledProjectionInstance(model,
                            projection.getLabel(i), row.getId(),row.getValue(0), row.getValue(1));
                    lpi.setScalarValue(cdata, row.getKlass());
                    lpi.setScalarValue(dots, 0.0f);
                    lpi.setScalarValue(dColor, 0.0f);
                }

                //setting the collection...
                if ((collection != null)&&(!collection.isEmpty())) {
                    if (model.getInstances() != null) {
                        if (((LabeledProjectionInstance)model.getInstances().get(0)).toString().endsWith(".txt")) {
                            Corpus c = CorpusFactory.getInstance(collection,1); //Este 1 esta fixo, estudar parametrizacao (ngrams)
                            model.corpus = c;
                        } else {
                            ImageCollection im = new ImageCollection(collection);
                            model.imageCollection = im;
                        }
                    }
                }else {
                    model.corpus = null;
                    model.imageCollection = null;
                }

                model.setRadvizStructure(radvizStructure);

            }
        } else {
            throw new IOException("A 2D projection should be provided.");
        }
    }

    public void input(@Param(name = "2D projection") AbstractMatrix projection) {
        this.projection = projection;
    }

    public void input(@Param(name = "RadViz Structure") RadVizStructure radvizStructure) {
        this.radvizStructure = radvizStructure;
    }

    public LabeledProjectionModel output() {
        return model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new LabeledProjectionModelParamView(this);
        }
        return paramview;
    }

    public void setCollection(String c) {
        this.collection = c;
    }

    public String getCollection() {
        return collection;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String s) {
        this.source = s;
    }

    @Override
    public void reset() {
        projection = null;
        model = null;
        radvizStructure = null;
    }

    public RadVizStructure getRadvizStructure() {
        return radvizStructure;
    }

    public void setRadvizStructure(RadVizStructure radvizStructure) {
        this.radvizStructure = radvizStructure;
    }

    private String collection;
    public static final long serialVersionUID = 1L;
    private transient LabeledProjectionModelParamView paramview;
    private transient LabeledProjectionModel model;
    private transient AbstractMatrix projection;
    private transient RadVizStructure radvizStructure;
    private String source;
}
