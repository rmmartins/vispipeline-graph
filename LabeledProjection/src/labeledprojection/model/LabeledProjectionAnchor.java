package labeledprojection.model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import projection.model.Scalar;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class LabeledProjectionAnchor {

    public LabeledProjectionAnchor(LabeledProjectionModel model, String label, float x, float y) {
        this.model = model;
        this.label = label;
        this.x = x;
        this.y = y;
        this.scalars = new ArrayList<Float>();
    }

    public void draw(BufferedImage image, boolean highquality) {

        Graphics2D g2 = (Graphics2D) image.getGraphics();
        g2.setColor(Color.BLACK);
        int sizeAnchor = 6;
        int sx = 0, sy = 0;
        sx = sy;
        if (x >= 0) sx = -sizeAnchor;
        if (y >= 0) sy = -sizeAnchor;
        Point p = new Point((int)(image.getWidth()+x*image.getWidth())/2+sx,(int)(image.getHeight()+y*image.getHeight())/2+sy);
        g2.fillOval(p.x,p.y,sizeAnchor,sizeAnchor);
        
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setScalarValue(Scalar scalar, float value) {
        if (scalar != null) {
            int index = model.getScalars().indexOf(scalar);

            if (scalars.size() > index) {
                scalars.set(index, value);
            } else {
                int size = scalars.size();
                for (int i = 0; i < index - size; i++) {
                    scalars.add(0.0f);
                }
                scalars.add(value);
            }

            scalar.store(value);
        }
    }

    public float getScalarValue(Scalar scalar) {
        if (scalar != null) {
            int index = model.getScalars().indexOf(scalar);

            if (scalars.size() > index && index > -1) {
                return scalars.get(index);
            }
        }

        return 0.0f;
    }

    public float getNormalizedScalarValue(Scalar scalar) {
        if (scalar != null) {
            int index = model.getScalars().indexOf(scalar);

            if (scalars.size() > index && index > -1) {
                float value = scalars.get(index);
                return (value - scalar.getMin()) / (scalar.getMax() - scalar.getMin());
            }
        }

        return 0.0f;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public LabeledProjectionModel getModel() {
        return model;
    }

    public void setModel(LabeledProjectionModel model) {
        this.model = model;
    }

    public boolean isInside(int x, int y) {
        return (Math.abs((this.x - x)) <= 1 && Math.abs((this.y - y)) <= 1);
    }

    public void drawLabel(Graphics2D g2, int x, int y) {
        //Show the anchor label
        if (label.trim().length() > 0) {
            if (label.length() > 100) {
                label = label.substring(0, 96) + "...";
            }
        }

        java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());

        int width = metrics.stringWidth(label);
        int height = metrics.getAscent();

        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
        g2.setPaint(java.awt.Color.WHITE);
        g2.fillRect(x + 3, y - 1 - height, width + 4, height + 4);
        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
        g2.setColor(java.awt.Color.DARK_GRAY);
        g2.drawRect(x + 3, y - 1 - height, width + 4, height + 4);
        g2.drawString(label, x + 3, y);
    }

    private LabeledProjectionModel model;
    private ArrayList<Float> scalars;
    private float x;
    private float y;
    private String label;
    private Color color;

}
