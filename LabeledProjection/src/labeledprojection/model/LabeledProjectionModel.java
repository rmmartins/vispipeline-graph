package labeledprojection.model;

import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import graph.model.Connectivity;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;
import labeledprojection.technique.radviz.RadVizStructure;
import labeledprojection.util.ProjectionConnectivity;
import labeledprojection.util.ImageCollection;
import textprocessing.corpus.Corpus;
import labeledprojection.util.PseudoClass;
import labeledprojection.util.TablePScalar;
import labeledprojection.view.forms.DragDropListItem;
import matrix.AbstractVector;
import projection.model.ProjectionModel;
import projection.model.Scalar;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class LabeledProjectionModel extends ProjectionModel {

    public LabeledProjectionModel() {
        super();
        instancesize = 4;
        alpha = 0.2f;
    }

    /***Add by Laura Florian***/
    public Scalar getScalarByName(String name) {
        for (Scalar s : this.scalars)
            if (s.getName().equals(name)) return s;
        return null;
    }

    public void setTablePScalar(TablePScalar table) {
        this.tablePScalar = table;
    }

    public TablePScalar getTablePScalar() {
        return this.tablePScalar;
    }

    public void updateTablePScalar(TablePScalar tablemodel) {
        // tiene todas las clases con sus colores de todos los vertices
        String filename;
        int indexTablePScalar, indexTableModel;
        Float scalarValue = null;
        for (int j=0;j<instances.size();j++) {
            LabeledProjectionInstance pins = (LabeledProjectionInstance)instances.get(j);
            if (tablemodel==null) indexTableModel=-1;
            else indexTableModel= tablemodel.getNumRowGiveName(pins.getLabel());
            if(indexTableModel >= 0) {
                filename = (String)tablemodel.getValueAt(indexTableModel,1);
                scalarValue = Float.valueOf(tablemodel.getValueAt(indexTableModel,3).toString().trim()).floatValue();
                //actualizar tablepc
                indexTablePScalar = tablePScalar.getNumRowGiveName(pins.getLabel());
                if(indexTablePScalar >= 0) {
                    this.tablePScalar.setValueAt(filename,indexTablePScalar,1);
                    this.tablePScalar.setValueAt(scalarValue,indexTablePScalar,3);
                }
            }
        }
    }

    public void updateTablePScalar(Scalar s) {
        // tiene todas las clases con sus colores de todos los vertices
        String label;
        int indexTablePScalar;
        PseudoClass pseudoClass = new PseudoClass();
        ArrayList<PseudoClass> pClasses = new ArrayList<PseudoClass>();
        Float scalarValue= new Float(0);
        for (int j=0;j<instances.size();j++) {
            LabeledProjectionInstance v = (LabeledProjectionInstance)instances.get(j);
            label = v.getLabel();
            if(this.tablePScalar == null) indexTablePScalar = -1;
            else indexTablePScalar= this.tablePScalar.getNumRowGiveName(label);
            if(indexTablePScalar < 0) {
                scalarValue = v.getScalarValue(s);
                label = "class"+  scalarValue.intValue();
            } else{
                label = (String)this.tablePScalar.getValueAt(indexTablePScalar, 1);
                scalarValue = Float.valueOf(this.tablePScalar.getValueAt(indexTablePScalar,3).toString().trim()).floatValue();
            }
            pseudoClass = new PseudoClass(v.getId(),v.getLabel(),label,true,scalarValue);
            pClasses.add(pseudoClass);
        }
        try {
            this.tablePScalar = new TablePScalar(pClasses,null);
        }catch(Exception ex) {
            System.err.println("Caught: " + ex);
            ex.printStackTrace();
        }
    }
    
    protected TablePScalar tablePScalar;

    /***by Laura Florian***/
    
    public ImageCollection getImageCollection() {
       return imageCollection;
    }

    public void setImageCollection(ImageCollection imageCollection) {
       this.imageCollection = imageCollection;
    }

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String s) {
        source = s;
    }

    public DissimilarityType getDissType() {
        return disstype;
    }

    public void setDissType(DissimilarityType d) {
        disstype = d;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LabeledProjectionInstance getInstanceByLabel(String label) {
        for (int i=0;i<this.getInstances().size();i++) {
            if (((LabeledProjectionInstance)this.getInstances().get(i)).getLabel().equalsIgnoreCase(label))
                return ((LabeledProjectionInstance)this.getInstances().get(i));
        }
        return null;
    }

    public LabeledProjectionInstance getInstanceById(int id) {
        for (int i=0;i<this.instances.size();i++) {
            if (this.instances.get(i).getId() == id) {
                return (LabeledProjectionInstance)this.instances.get(i);
            }
        }
        return null;
    }

    public void addScalar(Scalar scalar) {
        //Scalar scalar = new Scalar(name);

        if (!scalars.contains(scalar)) {
            scalars.add(scalar);
            //return scalar;
        } else {
            //return scalars.get(scalars.indexOf(scalar));
        }
    }

    public void removeScalar(Scalar scalar) {
        //removing the scalar from the instance
        int index = this.scalars.indexOf(scalar);
        if (index != -1) {
            for (AbstractInstance pi : instances) {
                ((LabeledProjectionInstance)pi).removeScalar(index);
            }
        }
        //removing the scalar from the model
        this.scalars.remove(scalar);
    }

    public void fitRadVizToSize(Dimension size) {

        if (radvizStructure.getDimension() == null)
            radvizStructure.setDimension(size);

        size = radvizStructure.getDimension();
        
//        float minx = -0.4f;
//        float miny = -0.4f;
//        float maxx = 0.4f;
//        float maxy = 0.4f;

        float maxx = Float.NEGATIVE_INFINITY;
        float minx = Float.POSITIVE_INFINITY;
        float maxy = Float.NEGATIVE_INFINITY;
        float miny = Float.POSITIVE_INFINITY;

        for (int i = 0; i < instances.size(); i++) {
            LabeledProjectionInstance pi = (LabeledProjectionInstance) instances.get(i);

            if (maxx < pi.getX()) {
                maxx = pi.getX();
            }

            if (maxy < pi.getY()) {
                maxy = pi.getY();
            }

            if (minx > pi.getX()) {
                minx = pi.getX();
            }

            if (miny > pi.getY()) {
                miny = pi.getY();
            }

        }

        if (maxx > maxy) maxy = maxx;
        else maxx = maxy;

        if (minx < miny) miny = minx;
        else minx = miny;

        if (Math.abs(miny) > Math.abs(maxy)) {
            if (miny < 0) {
                minx = miny;
                maxx = maxy = -miny;
            }else {
                maxx = maxy = miny;
                minx = miny = -miny;
            }
        }else {
            if (maxy < 0) {
                minx = miny = maxy;
                maxx = maxy = -maxy;
            }else {
                minx = miny = -maxy;
                maxx = maxy;
            }
        }


        float beginx = 30.0f;
        float beginy = 30.0f;
        float endy = (size.width)-beginx;
        float endx = (size.height)-beginy;

        for (int i = 0; i < instances.size(); i++) {
            LabeledProjectionInstance pi = (LabeledProjectionInstance) instances.get(i);
            if (maxx != minx)
                pi.setX((((pi.getX() - minx) / (maxx - minx)) * (endx - beginx)) + beginx);
            else 
                pi.setX(beginx);

            if (maxy != miny)
                pi.setY((((pi.getY() - miny) / (maxy - miny)) * (endy - beginy)) + beginy);
            else
                pi.setY(beginy);
        }
        setChanged();
    }

    public void zoomRadViz(float rate) {
        float maxX = Float.NEGATIVE_INFINITY;
        float minX = Float.POSITIVE_INFINITY;
        float maxY = Float.NEGATIVE_INFINITY;
        float minY = Float.POSITIVE_INFINITY;

        for (int i = 0; i < instances.size(); i++) {
            LabeledProjectionInstance pi = (LabeledProjectionInstance) instances.get(i);

            if (maxX < pi.getX()) {
                maxX = pi.getX();
            }

            if (minX > pi.getX()) {
                minX = pi.getX();
            }

            if (maxY < pi.getY()) {
                maxY = pi.getY();
            }

            if (minY > pi.getY()) {
                minY = pi.getY();
            }

        }

        float endX = maxX * rate;
        float endY = maxY * rate;

        for (int i = 0; i < instances.size(); i++) {
            LabeledProjectionInstance pi = (LabeledProjectionInstance) instances.get(i);

            if (maxX != minX) {
                pi.setX((((pi.getX() - minX) / (maxX - minX)) * (endX - minX)) + minX);
            } else {
                pi.setX(minX);
            }

            if (maxY != minY) {
                pi.setY(((((pi.getY() - minY) / (maxY - minY)) * (endY - minY)) + minY));
            } else {
                pi.setY(minY);
            }
        }

        setChanged();
    }

    @Override
    public void draw(BufferedImage image, boolean highquality) {

        if (selsconn != null) {
            selsconn.draw(this, image, highquality);
        }

//        if (getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY) != null) {
//            ((ProjectionConnectivity)getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY)).draw(this, image, highquality);
//        }
        super.draw(image,highquality);
    }

    //TESTE RONAK
//    @Override
//    public void draw(BufferedImage image, boolean highquality) {
//        if (getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY) != null) {
//            ((ProjectionConnectivity)getProjectionConnectivity(LabeledProjectionConstants.DELAUNAY)).draw(this, image, highquality);
//        }
//
//        //super.draw(image,highquality);
//
//        if (image != null) {
//            //first draw the non-selected instances
//            for (int i = 0; i < instances.size(); i++) {
//                LabeledProjectionInstance pi = (LabeledProjectionInstance) instances.get(i);
//
//                if (!pi.isSelected()) {
//                    pi.draw(image, highquality, this.getSelectedScalar());
//                }
//            }
//
//            //then the selected instances
//            for (int i = 0; i < instances.size(); i++) {
//                LabeledProjectionInstance pi = (LabeledProjectionInstance) instances.get(i);
//
//                if (pi.isSelected()) {
//                    pi.draw(image, highquality, this.getSelectedScalar());
//                }
//            }
//        }
//    }
    //TESTE RONAK

    @Override
    public String toString() {
        if (source == null || source.isEmpty()) return "Projection Model";
        else return source;
    }

    public int getNumMatching() {
        return numMatching;
    }

    public void setNumMatching(int numMatching) {
        this.numMatching = numMatching;
    }

    public int getNumNonMatching() {
        return numNonMatching;
    }

    public void setNumNonMatching(int numNonMatching) {
        this.numNonMatching = numNonMatching;
    }

    public int getNumNonCorresponding() {
        return numNonCorresponding;
    }

    public void setNumNonCorresponding(int numNonCorresponding) {
        this.numNonCorresponding = numNonCorresponding;
    }

    public int getInstanceSize() {
        return instancesize;
    }

    public void setInstanceSize(int instancesize) {
        this.instancesize = instancesize;
        setChanged();
    }

    public void perturb() {
        Random rand = new Random(7);

        float maxx = Float.NEGATIVE_INFINITY;
        float minx = Float.POSITIVE_INFINITY;
        float maxy = Float.NEGATIVE_INFINITY;
        float miny = Float.POSITIVE_INFINITY;

        for (int i = 0; i < instances.size(); i++) {
            LabeledProjectionInstance pi = (LabeledProjectionInstance) instances.get(i);

            if (maxx < pi.getX()) {
                maxx = pi.getX();
            }

            if (minx > pi.getX()) {
                minx = pi.getX();
            }

            if (maxy < pi.getY()) {
                maxy = pi.getY();
            }

            if (miny > pi.getY()) {
                miny = pi.getY();
            }
        }

        float diffx = (maxx - minx) / 1000;
        float diffy = (maxy - miny) / 1000;

        for (int i = 0; i < instances.size(); i++) {
            LabeledProjectionInstance pi = (LabeledProjectionInstance) instances.get(i);

            pi.setX(pi.getX() + diffx * rand.nextFloat());
            pi.setY(pi.getY() + diffy * rand.nextFloat());
        }
    }

    public Connectivity getProjectionConnectivity(String name) {
        if (connectivities == null || connectivities.isEmpty()) return null;
        for (int i=0;i<connectivities.size();i++)
            if (connectivities.get(i).getName().equalsIgnoreCase(name))
                return connectivities.get(i);
        return null;
    }

    public void addProjectionConnectivity(ProjectionConnectivity connectivity) {
        if (connectivities == null) connectivities = new ArrayList<ProjectionConnectivity>();
        for (int i=0;i<connectivities.size();i++)
            if (connectivities.get(i).getName().equalsIgnoreCase(connectivity.getName()))
                return;
        connectivities.add(connectivity);
    }

    @Override
    public Dimension getSize() {
        if (radvizStructure != null && radvizStructure.getDimension() != null) {
            return radvizStructure.getDimension();
        }
        if (instances.size() > 0) {
            float maxx = Float.MIN_VALUE;
            float maxy = Float.MIN_VALUE;

            for (int i = 0; i < instances.size(); i++) {
                LabeledProjectionInstance pi = (LabeledProjectionInstance) instances.get(i);

                if (maxx < pi.getX()) {
                    maxx = pi.getX();
                }

                if (maxy < pi.getY()) {
                    maxy = pi.getY();
                }
            }

            int w = (int) (maxx) + 30;
            int h = (int) (maxy) + 30;

            return new Dimension(w, h);
        } else {
            return new Dimension(0, 0);
        }
    }

    public ArrayList<LabeledProjectionAnchor> getAnchors() {
        return anchors;
    }

    public void addAnchor(LabeledProjectionAnchor lpi) {
        if (anchors == null) anchors = new ArrayList<LabeledProjectionAnchor>();
        anchors.add(lpi);
    }

    public void drawAnchors(BufferedImage image, boolean highquality) {

        ArrayList<AbstractVector> localAnchors = radvizStructure.getAnchors();
        ArrayList<DragDropListItem> localColorAnchors = radvizStructure.getColorAnchors();
        if (localAnchors == null || localAnchors.isEmpty()) return;

        Graphics2D g2 = (Graphics2D) image.getGraphics();
        if (highquality)
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(1.3f));
        int sizeAnchor = 6;

        float maxx = 1.0f;
        float minx = -1.0f;
        float maxy = 1.0f;
        float miny = -1.0f;
        
        float beginx = 30.0f;
        float beginy = 30.0f;
        float endy = (radvizStructure.getDimension().width)-beginx;
        float endx = (radvizStructure.getDimension().height)-beginy;
        
        int anchorx, anchory;
        g2.setColor(Color.BLACK);

        for (int i=0;i<localAnchors.size();i++) {

            if (maxx != minx) {
                anchorx = (int) ((((localAnchors.get(i).getValue(0) - minx) / (maxx - minx)) * (endx - beginx)) + beginx);
            } else {
                anchorx = (int) beginx;
            }

            if (maxy != miny) {
                anchory = (int) ((((localAnchors.get(i).getValue(1) - miny) / (maxy - miny)) * (endy - beginy)) + beginy);
            } else {
                anchory = (int) beginy;
            }

            //anchorx -= (radvizStructure.getDimension().width*(1-radvizStructure.getRate()));
            //anchory -= (radvizStructure.getDimension().height*(1-radvizStructure.getRate()));

            anchorx = ((anchorx) <= 0) ? 1 : ((anchorx) < radvizStructure.getDimension().width) ? anchorx : radvizStructure.getDimension().width - 1;
            anchory = ((anchory) <= 0) ? 1 : ((anchory) < radvizStructure.getDimension().height) ? anchory : radvizStructure.getDimension().height - 1;

            java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());
            int height = metrics.getAscent();
           
            if (localAnchors.get(i).getValue(1) >= 0)
                g2.drawString(Integer.toString(localColorAnchors.get(localColorAnchors.get(i).index).index),anchorx-(sizeAnchor/2),anchory+sizeAnchor+height);
            else
                g2.drawString(Integer.toString(localColorAnchors.get(localColorAnchors.get(i).index).index),anchorx-(sizeAnchor/2),anchory-sizeAnchor-2);
            g2.setColor(localColorAnchors.get(localColorAnchors.get(i).index).color);
            g2.fillRect(anchorx-(sizeAnchor/2),anchory-(sizeAnchor/2),sizeAnchor,sizeAnchor);
            g2.drawLine(anchorx,anchory,radvizStructure.getDimension().width/2,radvizStructure.getDimension().height/2);
            g2.setColor(Color.BLACK);
        }



    }

    public boolean hasAnchors() {
        return (anchors != null && anchors.isEmpty());
    }

    public LabeledProjectionAnchor getAnchorByPosition(Point point) {
        if (anchors != null) {
            for (int i = 0; i < anchors.size(); i++) {
                LabeledProjectionAnchor lpa = anchors.get(i);
                if (lpa.isInside(point.x, point.y)) {
                    return lpa;
                }
            }
            return null;
        }else
            return null;
    }

    public Connectivity getSelectedConnectivity() {
        return selsconn;
    }

    public void setSelectedConnectivity(ProjectionConnectivity conn) {
        if (connectivities.contains(conn)) {
            selsconn = conn;
        }
        setChanged();
    }

    public RadVizStructure getRadvizStructure() {
        return radvizStructure;
    }

    public void setRadvizStructure(RadVizStructure radvizStructure) {
        if (radvizStructure != null) {
            this.radvizStructure = radvizStructure;
            float[][] radvizProjection = this.radvizStructure.updateRadVizValues();
            for (int i=0;i<this.getInstances().size();i++) {
                LabeledProjectionInstance lpi = (LabeledProjectionInstance) this.getInstances().get(i);
                lpi.setX(radvizProjection[i][0]);
                lpi.setY(radvizProjection[i][1]);
            }
        }
    }

    protected String source = "";
    protected ImageCollection imageCollection;
    protected Corpus corpus;
    protected DissimilarityType disstype;
    private String type;
    private int numMatching = 0;
    private int numNonMatching = 0;
    private int numNonCorresponding = 0;
    private int instancesize;
    private ProjectionConnectivity selsconn;
    private ArrayList<ProjectionConnectivity> connectivities = null;
    private ArrayList<LabeledProjectionAnchor> anchors = null;
    protected RadVizStructure radvizStructure;

}
