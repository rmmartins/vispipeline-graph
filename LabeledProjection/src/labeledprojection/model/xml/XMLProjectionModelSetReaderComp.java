/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.model.xml;

import java.io.IOException;
import java.util.ArrayList;
import labeledprojection.model.LabeledProjectionModel;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Projection.Input",
name = "Projection Set Reader",
description = "Read a set of projection models.")
public class XMLProjectionModelSetReaderComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (filenames.trim().length() > 0) {
            XMLProjectionModelReaderComp xmc = new XMLProjectionModelReaderComp();
            String[] files = filenames.split(";");
            if (files.length > 0) {
                for (int i=0;i<files.length;i++) {
                    xmc.setFilename(files[i]);
                    xmc.execute();
                    models.add((LabeledProjectionModel) xmc.output());
                }
            }
        } else {
            throw new IOException("A projection set file name must be provided to write.");
        }
    }

    public ArrayList<LabeledProjectionModel> output() {
        return models;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new XMLProjectionModelSetReaderParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        models = new ArrayList<LabeledProjectionModel>();
    }

    /**
     * @return the filename
     */
    public String getFilesname() {
        return filenames;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilesname(String filename) {
        this.filenames = filename;
    }

    public static final long serialVersionUID = 1L;
    protected String filenames = "";
    protected transient ArrayList<LabeledProjectionModel> models;
    protected transient XMLProjectionModelSetReaderParamView paramview;
}
