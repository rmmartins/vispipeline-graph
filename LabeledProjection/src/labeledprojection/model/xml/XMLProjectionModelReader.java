/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labeledprojection.model.xml;

import graph.model.Edge;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import labeledprojection.model.LabeledProjectionInstance;
import labeledprojection.model.LabeledProjectionModel;
import labeledprojection.util.ProjectionConnectivity;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import projection.model.Scalar;
import visualizationbasics.util.Util;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class XMLProjectionModelReader extends DefaultHandler {

    public void read(LabeledProjectionModel model, String filename) throws IOException {
        this.model = model;
        this.model.setSource(filename.substring(filename.lastIndexOf("\\")+1,filename.lastIndexOf(".")));

        SAXParserFactory spf = SAXParserFactory.newInstance();

        try {
            InputSource in = new InputSource(new InputStreamReader(new FileInputStream(filename), "ISO-8859-1"));
            SAXParser sp = spf.newSAXParser();
            sp.parse(in, this);
        } catch (SAXException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);

        if (qName.equalsIgnoreCase(SCALAR)) {
            String name = attributes.getValue(NAME);
            String value = attributes.getValue(VALUE);

            if (name != null && value != null) {
                Scalar s = model.addScalar(name);
                tmpinstance.setScalarValue(s, Float.parseFloat(value));
            }
        } else if (qName.equalsIgnoreCase(INSTANCE)) {
            String id = attributes.getValue(ID);
            if (Util.isParsableToInt(id)) {
                tmpinstance = new LabeledProjectionInstance(model,"",Integer.parseInt(id));
            } else {
                tmpinstance = new LabeledProjectionInstance(model,"",Util.convertToInt(id));
            }
        } else if (qName.equalsIgnoreCase(X_COORDINATE)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                tmpinstance.setX(Float.parseFloat(value));
            }
        } else if (qName.equalsIgnoreCase(Y_COORDINATE)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                tmpinstance.setY(Float.parseFloat(value));
            }
        } else if (qName.equalsIgnoreCase(LABEL)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                tmpinstance.setLabel(value);
            }
        } else if (qName.equalsIgnoreCase(EDGES)) {
            edges = new ArrayList<Edge>();
            con = new ProjectionConnectivity(attributes.getValue(NAME),edges);
            model.addProjectionConnectivity(con);
        } else if (qName.equalsIgnoreCase(EDGE)) {
            int v1 = Integer.parseInt(attributes.getValue(SOURCE));
            int v2 = Integer.parseInt(attributes.getValue(TARGET));
            float length = Edge.NO_SIZE;
            String aux_len = attributes.getValue(LENGTH);
            if (aux_len != null) {
                length = Float.parseFloat(aux_len);
            }
            Edge ed = new Edge(v1,v2,length);
            edges.add(ed);
        }
    }

    private ProjectionConnectivity con;
    ArrayList<Edge> edges;
    private LabeledProjectionInstance tmpinstance;
    private LabeledProjectionModel model;
    private static final String INSTANCE = "instance";
    private static final String NAME = "name";
    private static final String ID = "id";
    private static final String X_COORDINATE = "x-coordinate";
    private static final String Y_COORDINATE = "y-coordinate";
    private static final String VALUE = "value";
    private static final String SCALAR = "scalar";
    private static final String LABEL = "label";
    private static final String EDGES = "edges";
    private static final String EDGE = "edge";
    private static final String SOURCE = "source";
    private static final String TARGET = "target";
    private static final String LENGTH = "length";
}
