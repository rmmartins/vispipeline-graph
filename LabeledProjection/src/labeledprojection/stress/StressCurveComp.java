/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.stress;

import java.io.IOException;
import projection.stress.StressFactory.StressType;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import distance.dissimilarity.Euclidean;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import labeledprojection.model.LabeledProjectionModel;

import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;
import projection.stress.Stress;
import projection.stress.StressFactory;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Data Analysis",
name = "Projection Stress Curve",
description = "Calculate how the distances relations of a projection differ from the original distances relations, for each instance.")
public class StressCurveComp implements AbstractComponent {

    protected AbstractMatrix exportPoints(ArrayList<AbstractInstance> instances) {

        AbstractMatrix matrix = new DenseMatrix();
        ArrayList<String> labels = new ArrayList<String>();

        for (int i = 0; i < instances.size(); i++) {
            float[] point = new float[2];
            point[0] = ((ProjectionInstance)instances.get(i)).getX();
            point[1] = ((ProjectionInstance)instances.get(i)).getY();

            Integer id = ((ProjectionInstance)instances.get(i)).getId();
            labels.add(((ProjectionInstance)instances.get(i)).toString());

            matrix.addRow(new DenseVector(point,id,0.0f));
        }

        matrix.setLabels(labels);

        ArrayList<String> attributes = new ArrayList<String>();
        attributes.add("x");
        attributes.add("y");

        matrix.setAttributes(attributes);

        return matrix;
    }

    @Override
    public void execute() throws IOException {
        AbstractDissimilarity diss = DissimilarityFactory.getInstance(DissimilarityFactory.DissimilarityType.EUCLIDEAN);
        if (this.getDissimilarityType() != null)
            diss = DissimilarityFactory.getInstance(this.getDissimilarityType());
        Float stress = null;
        Stress stresscalc = StressFactory.getInstance(stresstype);
        DistanceStressCurve sc = new DistanceStressCurve();

        if (model == null)
            throw new IOException("A projection model should be provided.");

        if (dataFileName == null || dataFileName.isEmpty())
            throw new IOException("A distance matrix or a points matrix should be provided.");

        //Obtaining the distances between projected points...
        AbstractMatrix points = exportPoints(model.getInstances());
        DistanceMatrix projectionDmat = new DistanceMatrix(points, new Euclidean());
        
        //Obtaining original distance matrix..
        switch (dataFileType) {
            case 0: //points file
                try {
                    AbstractMatrix matrix = MatrixFactory.getInstance(dataFileName);
                    originalDmat = new DistanceMatrix(matrix,diss);
                } catch (IOException ex) {
                    Logger.getLogger(StressCurveComp.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case 1: //distance matrix
                try {
                    originalDmat = new DistanceMatrix(dataFileName);
                } catch (IOException ex) {
                    Logger.getLogger(StressCurveComp.class.getName()).log(Level.SEVERE, null, ex);
                }
        }

        if (originalDmat.getElementCount() != projectionDmat.getElementCount()) {
            JOptionPane.showMessageDialog(null,"Original dataset is different from projected dataset!");
        }else {
            //stress = stresscalc.calculate(projectionDmat,originalDmat);
            BufferedImage image = sc.generate(new Dimension(1200,1200),1.0f,projectionDmat,originalDmat);
            StressCurveDialog.getInstance(null).display(graphicLabel,image);
        }
    }

    public void input(@Param(name = "Projection Model") LabeledProjectionModel model) {
        this.model = model;
    }

    public void input(@Param(name = "Projection Model") ProjectionModel model) {
        this.model = model;
    }

//    public Float output() {
//        return stress;
//    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new StressCurveParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        model = null;
        originalMatrix = null;
        originalDmat = null;
    }

    /**
     * @return the disstype
     */
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    /**
     * @param disstype the disstype to set
     */
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }
    
    /**
     * @return the stresstype
     */
    public StressType getStressType() {
        return stresstype;
    }

    /**
     * @param stresstype the stresstype to set
     */
    public void setStressType(StressType stresstype) {
        this.stresstype = stresstype;
    }

    public String getDataFileName() {
        return dataFileName;
    }

    public void setDataFileName(String dataFileName) {
        this.dataFileName = dataFileName;
    }

    public int getDataFileType() {
        return dataFileType;
    }

    public void setDataFileType(int dataFileType) {
        this.dataFileType = dataFileType;
    }

    public String getGraphicLabel() {
        return graphicLabel;
    }

    public void setGraphicLabel(String s) {
        graphicLabel = s;
    }

    public static final long serialVersionUID = 1L;
    private StressType stresstype = StressType.KRUSKAL;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    protected transient StressCurveParamView paramview;
    protected transient ProjectionModel model;
    protected transient AbstractMatrix originalMatrix;
    protected transient DistanceMatrix originalDmat;
    private String dataFileName = "";
    private int dataFileType;
    protected String graphicLabel = "";
}
