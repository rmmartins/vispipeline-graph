/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.coordination;

import java.io.IOException;
import projection.model.ProjectionModel;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Coordination",
name = "Class Match Coordination",
description = "Create an class match coordination object.")
public class ClassMatchCoordinationComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        coord = new ClassMatchCoordination();
    }

    public AbstractCoordinator output() {
        return coord;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        coord = null;
    }

    public static final long serialVersionUID = 1L;
    private transient ClassMatchCoordination coord;
}
