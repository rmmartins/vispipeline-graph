/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.coordination;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import labeledprojection.model.LabeledProjectionInstance;
import labeledprojection.model.LabeledProjectionModel;
import projection.model.Scalar;
import labeledprojection.view.LabeledProjectionFrame;
import labeledprojection.view.ReportView;
import visualizationbasics.coordination.IdentityCoordinator;
import visualizationbasics.model.AbstractModel;

/**
 *
* @author Jose Gustavo de Souza Paiva
 */
public class ClassMatchCoordination extends IdentityCoordinator {

    public ClassMatchCoordination() {
        super();
        frames = new ArrayList<LabeledProjectionFrame>();
    }

    public Scalar match() {
        Scalar cdata1,cdata2;//,sc1,sc2;
        LabeledProjectionInstance pi1,pi2;
        Scalar scalar = new Scalar("ClassMatch");
        scalar.store(0);
        scalar.store(2);
        TreeMap<Float,HashMap<Float,Integer>> confuseData0 = new TreeMap<Float,HashMap<Float,Integer>>();
        TreeMap<Float,HashMap<Float,Integer>> confuseData1 = new TreeMap<Float,HashMap<Float,Integer>>();

        if (models.size() == 2) {
            LabeledProjectionModel model1 = ((LabeledProjectionModel)models.get(0));
            LabeledProjectionModel model2 = ((LabeledProjectionModel)models.get(1));
            if (model1.getScalarByName("ClassMatch") == null) model1.addScalar(scalar);
            if (model2.getScalarByName("ClassMatch") == null) model2.addScalar(scalar);
            for (int i=0;i<model1.getInstances().size();i++) {
                pi1 = (LabeledProjectionInstance)model1.getInstances().get(i);
                pi1.setScalarValue(scalar,0);
            }
            for (int i=0;i<model2.getInstances().size();i++) {
                pi1 = (LabeledProjectionInstance)model2.getInstances().get(i);
                pi1.setScalarValue(scalar,0);
            }
            cdata1 = model1.getScalarByName("cdata");
            cdata2 = model2.getScalarByName("cdata");
            if (cdata1 == null || cdata2 == null) return null;
            for (int i=0;i<model1.getInstances().size();i++) {
                pi1 = (LabeledProjectionInstance)model1.getInstances().get(i);
                pi2 = model2.getInstanceByLabel(pi1.getLabel());
                if (pi2 != null) {
                    if (pi1.getScalarValue(cdata1) == pi2.getScalarValue(cdata2)) {//class match...
                        pi1.setScalarValue(scalar,1);
                        pi2.setScalarValue(scalar,1);
                        model1.setNumMatching(model1.getNumMatching()+1);
                        model2.setNumMatching(model2.getNumMatching()+1);
                    }else { //class does not match...
                        pi1.setScalarValue(scalar,2);
                        pi2.setScalarValue(scalar,2);
                        model1.setNumNonMatching(model1.getNumNonMatching()+1);
                        model2.setNumNonMatching(model2.getNumNonMatching()+1);
                    }
                    //Counting for confusing matrix...
                    if (!confuseData0.containsKey(pi1.getScalarValue(cdata1))) {
                        confuseData0.put(pi1.getScalarValue(cdata1),new HashMap<Float,Integer>());
                    }
                    if (!confuseData0.get(pi1.getScalarValue(cdata1)).containsKey(pi2.getScalarValue(cdata2)))
                        confuseData0.get(pi1.getScalarValue(cdata1)).put(pi2.getScalarValue(cdata2),1);
                    else {
                        int qt = confuseData0.get(pi1.getScalarValue(cdata1)).get(pi2.getScalarValue(cdata2));
                        confuseData0.get(pi1.getScalarValue(cdata1)).put(pi2.getScalarValue(cdata2),qt+1);
                    }


                    if (!confuseData1.containsKey(pi2.getScalarValue(cdata2))) {
                        confuseData1.put(pi2.getScalarValue(cdata2),new HashMap<Float,Integer>());
                    }
                    if (!confuseData1.get(pi2.getScalarValue(cdata2)).containsKey(pi1.getScalarValue(cdata1)))
                        confuseData1.get(pi2.getScalarValue(cdata2)).put(pi1.getScalarValue(cdata1),1);
                    else {
                        int qt = confuseData1.get(pi2.getScalarValue(cdata2)).get(pi1.getScalarValue(cdata1));
                        confuseData1.get(pi2.getScalarValue(cdata2)).put(pi1.getScalarValue(cdata1),qt+1);
                    }
                }
            }
            model1.setNumNonCorresponding(model1.getInstances().size()-model1.getNumMatching()-model1.getNumNonMatching());
            model2.setNumNonCorresponding(model2.getInstances().size()-model2.getNumMatching()-model2.getNumNonMatching());
            frames.get(0).updateScalars(scalar);
            frames.get(1).updateScalars(scalar);
//            ((ReportView)frames.get(0).getReportPanel()).setClassMatchingPanel(model1.getInstances().size(),
//                                                                               model1.getNumMatching(),
//                                                                               model1.getNumNonMatching(),
//                                                                               model1.getNumNonCorresponding());
//            ((ReportView)frames.get(1).getReportPanel()).setClassMatchingPanel(model2.getInstances().size(),
//                                                                               model2.getNumMatching(),
//                                                                               model2.getNumNonMatching(),
//                                                                               model2.getNumNonCorresponding());
            ((ReportView)frames.get(0).getReportPanel()).setClassMatchingPanel(model1.getInstances().size(),
                                                                               model1.getNumMatching(),
                                                                               model1.getNumNonMatching(),
                                                                               model1.getNumNonCorresponding(),
                                                                               confuseData0);
            ((ReportView)frames.get(0).getReportPanel()).setColorTable(model1.getColorTable());
            ((ReportView)frames.get(1).getReportPanel()).setClassMatchingPanel(model2.getInstances().size(),
                                                                               model2.getNumMatching(),
                                                                               model2.getNumNonMatching(),
                                                                               model2.getNumNonCorresponding(),
                                                                               confuseData1);
            ((ReportView)frames.get(1).getReportPanel()).setColorTable(model2.getColorTable());
            return scalar;
        } else return null;
        
    }

    @Override
    public synchronized void addModel(AbstractModel model) {
        super.addModel(model);
        //match();
    }

    public synchronized void addFrame(LabeledProjectionFrame frame) {
        if (frame == null) {
            throw new NullPointerException();
        }

        if (!frames.contains(frame)) {
            frames.add(frame);
        }
    }

    ArrayList<LabeledProjectionFrame> frames;

}
