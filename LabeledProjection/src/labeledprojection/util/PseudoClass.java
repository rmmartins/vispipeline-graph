/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.util;

/**
 * @author Laura Florian
 */

public class PseudoClass {
    int id;
    String originalFile;
    String oldClass;
    Float color;
    Boolean option;
        
    public PseudoClass(){}

   // public PseudoClass( String fileOr, String oldC,  Float colorS){
    public PseudoClass(int id, String fileOr, String oldC, Boolean opt, Float colorS){
        this.id = id;
        this.originalFile=fileOr;
        this.oldClass=oldC;
        this.color=colorS;
        this.option=opt;
    }
  
}
