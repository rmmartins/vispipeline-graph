/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.util;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import java.io.*;
import javax.swing.*;
import java.io.File;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;


/**
 * @author Laura Florian
 */
public class TablePScalar extends AbstractTableModel{

    List list;
    Vector rows = new Vector();
    Vector columns = new Vector();       // Vector de columns
    Class columnType[];
    TablePScalar pcs;

     public TablePScalar(List ls, TablePScalar ps) throws Exception {
        this.list = ls;
        this.pcs=ps;
        Class head[] = {String.class, String.class, Boolean.class, Float.class, Integer.class};
        this.columnType = head;        
        columns.add("File Name");
        columns.add("PseudoClass Name");
        columns.add("+");
        columns.add("Color");
        columns.add("ID");
        this.fill();
    }

     public void addFila(PseudoClass detalle){
         Vector row = new java.util.Vector();
         for (int i=0;i<columns.size();i++){
                switch (i){
                    case 4:{
                        row.add(detalle.id);
                        break;
                    }
                    case 0:{
                        row.add(detalle.originalFile);
                        break;
                    }
                    case 1: {
                        row.add(detalle.oldClass);
                        break;
                    }
                    case 2:{
                        row.add(detalle.option);
                        break;
                    }
                    case 3:{
                        Float color = this.getColor((String)detalle.oldClass,rows.size()+1);
                        row.add(color);
                        break;
                    }
                }
            }
            rows.add(row);
            this.fireTableRowsInserted(rows.size(), rows.size());
            fireTableDataChanged();

     }

    @Override
    public String getColumnName(int c) {
        return (String) columns.elementAt(c);
    }

    /*************** getColumnCount() ******************/
    public int getColumnCount() {
        return columns.size();
    }

    /****************** getRowCount() *******************/
    public int getRowCount() {
        return rows.size();
    }

    public Object getValueAt(int row, int col) {
        Vector v = (Vector) rows.elementAt(row);
        return v.elementAt(col);
    }

    @Override
    public Class getColumnClass(int c) {
        return columnType[c];
    }


    public Float getMaxColor()
    {
        Float color=new Float(0);
        Float f;
        for (int i=0;i<rows.size();i++){
            f = Float.valueOf(getValueAt(i,3).toString().trim()).floatValue();
            if(f>color)
            {
                color=f;
            }
        }
        return color;
    }

    @Override
    public void setValueAt(Object valor, int row, int col) {
        Vector v = (Vector) rows.elementAt(row);
        v.set(col, valor);
        if(( col == 1 ) && (pcs!=null))
        {
            Float color = this.getColor((String)valor, row);
            v.set(3, color);
        }
    }

    @Override
    public boolean isCellEditable(int row, int col) {
           if (col < 1)  {
            return false;
        } else {
            return true;
        }
    }

    private float getColor(String clase, int fila)
    {
        Float color = new Float(0);
        int i= this.pcs.getNumRowGiveClass((String)clase,fila);
            if (i>=0){
                color = Float.valueOf(this.pcs.getValueAt(i,3).toString().trim()).floatValue();
            } else
            {
                int j= getNumRowGiveClass((String)clase,fila);

                if ((j>=0) && (j!=fila)){
                    color = Float.valueOf(getValueAt(j,3).toString().trim()).floatValue();
                } else
                {
                    color= this.pcs.getMaxColor() +1;
                }
            }
        return color;
    }

    public int getNumRowGiveId(int id) {
        int r=-1;
        int valor;
        for (int i=0;i<rows.size();i++){
                valor=(Integer)getValueAt(i,4);
                if(id == valor)
                {
                    r=i;
                    break;
                }
        }
        return r;
    }

    public int getNumRowGiveName(String object) {
        int r=-1;
        String valor;
        for (int i=0;i<rows.size();i++){
                valor=(String)getValueAt(i,0);
                if(object.equals(valor))
                {
                    r=i;
                    break;
                }
        }
        return r;
    }

    private int getNumRowGiveClass( String object, int fila){
        int r=-1;
        String valor;
        for (int i=0;i<rows.size();i++){
                valor=(String)getValueAt(i,1);
                if((object.equals(valor))&&(i!=fila))
                {
                    r=i;
                    break;
                }
        }
        return r;
    }


    /*public boolean setSelectionMode(int row, int col) {
            return true;
    }*/

    public void writeTablePS(String path)
    {
        File fileC = new File(path);
        String rowdata, filename, scalar, aux;
        int longitud=rows.size();
        try{
                FileWriter fstream = new FileWriter(fileC.getAbsolutePath());
                BufferedWriter out = new BufferedWriter(fstream);
                rowdata= "'cdata'"+ "\n" ;
                out.write(rowdata);
                for(int i=0;i<longitud;i++) {
                    filename = this.getValueAt(i,0).toString();
                    scalar = this.getValueAt(i,3).toString();
                    rowdata= filename + ";" + scalar + "\n" ;
                    out.write(rowdata);
                 }
                 out.close();
             }catch (Exception e1){
                        JOptionPane.showMessageDialog(null, "Could not write tablePS" + e1.getMessage(), "error", JOptionPane.ERROR_MESSAGE);
                        System.out.println("Error: Could not write tablePS" + e1.getMessage());
            }

    }


    public void writeTablePS(String path, String temp)
    {
        File fileC = new File(path);
        File fileT = new File(temp);
        String rowdata, filename,scalar, aux;
        int longitud=rows.size();
        try{
                fileT.delete();
                    boolean renameOk=fileC.renameTo(fileT);
                    if (!renameOk)
                    {
                        throw new RuntimeException("Could not rename the file "+fileC.getAbsolutePath()+" to "+fileT.getAbsolutePath());
                    }

                FileWriter fstream = new FileWriter(fileC);
                BufferedWriter out = new BufferedWriter(fstream);

                FileReader freader = new FileReader(fileT);
                BufferedReader in = new BufferedReader(freader);

                while ((rowdata=in.readLine())!=null){
				out.write(rowdata);
				out.newLine();
			}
                in.close();

                for(int i=0;i<longitud;i++) {
                    filename = this.getValueAt(i,0).toString();
                    scalar = this.getValueAt(i,3).toString();
                    rowdata= filename + ";" + scalar + "\n" ;
                    out.write(rowdata);
                 }
                 out.close();
                 fileT.delete();

             }catch (Exception e1){
                        JOptionPane.showMessageDialog(null, "Could not write tablePS" + e1.getMessage(), "error", JOptionPane.ERROR_MESSAGE);
                        System.out.println("Error: Could not write tablePS" + e1.getMessage());
            }

    }


    public void printTablePS()
    {
        System.out.println( "********************************************************");
        for(int i=0;i<rows.size();i++) {
                   System.out.println( "filename>> " + this.getValueAt(i,0));
                   System.out.println(  "class>> " + this.getValueAt(i,1));
                   System.out.println(  "scalar>> "  + this.getValueAt(i,3));
                   System.out.println( "--------------------------");
        }
    }

    public void fill(){
        this.rows.clear();
        Vector row;
        int j=0;
        PseudoClass detalle = new PseudoClass();

        for (Iterator it = this.list.iterator(); it.hasNext();){
            detalle = (PseudoClass) it.next();
            row = new java.util.Vector();
            j++;

            for (int i=0;i<columns.size();i++){
                switch (i){
                    case 4:{
                        row.add(detalle.id);
                        break;
                    }
                    case 0:{
                        row.add(detalle.originalFile);
                        break;
                    }
                    case 1:{
                        row.add(detalle.oldClass);
                        break;
                    }
                    case 2:{
                        row.add(detalle.option);
                        break;
                    }
                    case 3:{
                        row.add(detalle.color);
                        break;
                    }
                }
            }
            rows.add(row);
            this.fireTableRowsInserted(rows.size(), rows.size());
            fireTableDataChanged();
        }

    }
    
}
