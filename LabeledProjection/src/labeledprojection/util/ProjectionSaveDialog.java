package labeledprojection.util;

import java.awt.Component;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.filter.AbstractFilter;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class ProjectionSaveDialog {

    public static int showDirSaveDialog(Component parent, String directory) {
        
        dialog = new JFileChooser();
        int oldSelectionMode = dialog.getFileSelectionMode();
        String oldFilename = _filename;
        dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        dialog.setDialogTitle("Directory to save");
        dialog.setCurrentDirectory(new File(directory));
        int result = dialog.showOpenDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            _filename = dialog.getSelectedFile().getAbsolutePath();
        }
        dialog.setFileSelectionMode(oldSelectionMode);
        return result;
    }

    public static int showSaveDialog(AbstractFilter filter, Component parent,
            String directory, String filename) {
        if (ProjectionSaveDialog.dialog == null) {
            ProjectionSaveDialog.createDialog();
        }

        _filename = null;

        dialog.resetChoosableFileFilters();
        dialog.setAcceptAllFileFilterUsed(false);
        if (filter != null) {
            dialog.setFileFilter(filter);
        }
        dialog.setMultiSelectionEnabled(false);
        dialog.setDialogTitle("Save file");
        dialog.setCurrentDirectory(new File(directory));

        if (filename != null && filename.length() > 0) {
            dialog.setSelectedFile(new File(filename));
        } else {
            dialog.setSelectedFile(new File(""));
        }

        int result = dialog.showSaveDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            _filename = dialog.getSelectedFile().getAbsolutePath();

            //checking if the name finishes with the correct extension
            if (!_filename.toLowerCase().endsWith("." + filter.getFileExtension())) {
                _filename = _filename.concat("." + filter.getFileExtension());
            }
        }

        return result;
    }

    public static int showSaveDialog(PropertiesManager spm,
            AbstractFilter filter, Component parent, String filename) {
        if (ProjectionSaveDialog.dialog == null) {
            ProjectionSaveDialog.createDialog();
        }

        _filename = null;

        dialog.resetChoosableFileFilters();
        dialog.setAcceptAllFileFilterUsed(false);

        if (filter != null) {
            dialog.setFileFilter(filter);
        }

        dialog.setMultiSelectionEnabled(false);
        dialog.setDialogTitle("Save file");

        if (filename != null && filename.length() > 0) {
            filename = filename.substring(0, filename.lastIndexOf('.')) + "." +
                    filter.getFileExtension().toLowerCase();
            dialog.setSelectedFile(new File(filename));
        } else {
            dialog.setSelectedFile(new File(""));
        }

        dialog.setCurrentDirectory(new File(spm.getProperty(filter.getProperty())));

        int result = dialog.showSaveDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            _filename = dialog.getSelectedFile().getAbsolutePath();
            spm.setProperty(filter.getProperty(), dialog.getSelectedFile().getParent());

            //checking if the name finishes with the correct extension
            if (!_filename.toLowerCase().endsWith("." + filter.getFileExtension())) {
                _filename = _filename.concat("." + filter.getFileExtension());
            }
        }

        return result;
    }

    public static int showSaveDialog(PropertiesManager spm,
            AbstractFilter filter, Component parent) {
        return showSaveDialog(spm, filter, parent, "newfile" + "." + filter.getFileExtension());
    }

    public static String getFilename() {
        return _filename;
    }

    private static void createDialog() {
        dialog = new JFileChooser() {

            @Override
            public void approveSelection() {
                File file = getSelectedFile();
                if (file != null && file.exists()) {
                    String message = "The file \"" + file.getName() + "\" already exists. \n" +
                            "Do you want to replace the existing file?";
                    int answer = JOptionPane.showOptionDialog(this, message, "Save Warning",
                            JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);

                    if (answer == JOptionPane.NO_OPTION) {
                        return;
                    }
                }

                super.approveSelection();
            }

        };
    }

    private static String _filename;
    private static JFileChooser dialog;

}
