/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labeledprojection.util;

import distance.dissimilarity.Euclidean;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import javax.imageio.ImageIO;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import matrix.dense.DenseMatrix;
import labeledprojection.stress.StressCurve;

/**
 *
 * @author paulovich
 */
public class GenerateStressCurve {

    public static void main(String[] args) throws IOException {
//        {
//            String dirname = "/home/paulovich/Dropbox/dados/PLSP/shuttle/";
//            String matrixname = dirname + "shuttle_trn_corr-normcols.data";
//            String filenamename = dirname + "shuttle_trn_corr-normcols.data-l-isomap.prj";
//            GenerateStressCurve gsc = new GenerateStressCurve();
//            gsc.executeFile(filenamename, matrixname, new Dimension(1000, 1000), 0.0000125f);
//        }
//
//        {
//            String dirname = "/home/paulovich/Dropbox/dados/PLSP/shuttle/";
//            String matrixname = dirname + "shuttle_trn_corr-normcols.data";
//            String filenamename = dirname + "shuttle_trn_corr-normcols.data-pivotmds.prj";
//            GenerateStressCurve gsc = new GenerateStressCurve();
//            gsc.executeFile(filenamename, matrixname, new Dimension(1000, 1000), 0.0000125f);
//        }

        {
            String dirname = "E:\\";
            String matrixname = dirname + "CorelOrig.data";
            String filenamename = dirname + "CorelProj.data";
            GenerateStressCurve gsc = new GenerateStressCurve();
            gsc.executeFile(filenamename, matrixname, new Dimension(1200, 1200), 1);
        }

//        {
//            String dirname = "/home/paulovich/Dropbox/dados/PLSP/fibers/";
//            String matrixname = dirname + "fiber_5_beginEnd_All-normrows.data";
//            String filenamename = dirname + "fiber_5_beginEnd_All-normrows.data-[20N]LSP.prj";
//            GenerateStressCurve gsc = new GenerateStressCurve();
//            gsc.executeFile(filenamename, matrixname, new Dimension(1200, 1200), 0.000000125f);
//        }





//        //Directories
//        {
//            String dirname = "/home/paulovich/Dropbox/dados/PLSP/wdbc/";
//            String matrixname = dirname + "wdbc-std.data";
//            GenerateStressCurve gsc = new GenerateStressCurve();
//            gsc.executeDirectory(dirname, matrixname, new Dimension(800, 800), 0.25f);
//        }
//
//        {
//            String dirname = "/home/paulovich/Dropbox/dados/PLSP/wine-red/";
//            String matrixname = dirname + "winequality-red-std.data";
//            GenerateStressCurve gsc = new GenerateStressCurve();
//            gsc.executeDirectory(dirname, matrixname, new Dimension(800, 800), 0.125f);
//        }
//
//        {
//            String dirname = "/home/paulovich/Dropbox/dados/PLSP/wine-white/";
//            String matrixname = dirname + "winequality-white-std.data";
//            GenerateStressCurve gsc = new GenerateStressCurve();
//            gsc.executeDirectory(dirname, matrixname, new Dimension(800, 800), 0.05f);
//        }
//
//        {
//            String dirname = "/home/paulovich/Dropbox/dados/PLSP/segmentation/";
//            String matrixname = dirname + "segmentation-normcols.data";
//            GenerateStressCurve gsc = new GenerateStressCurve();
//            gsc.executeDirectory(dirname, matrixname, new Dimension(800, 800), 0.05f);
//        }
//
//        {
//            String dirname = "/home/paulovich/Dropbox/dados/PLSP/mammals/";
//            String matrixname = dirname + "mammals-10000-normcols.bin";
//            GenerateStressCurve gsc = new GenerateStressCurve();
//            gsc.executeDirectory(dirname, matrixname, new Dimension(800, 800), 0.0025f);
//        }
//
//        {
//            String dirname = "/home/paulovich/Dropbox/dados/PLSP/viscontest/";
//            String matrixname = dirname + "multifield.0099-normcols.bin-30000.bin";
//            GenerateStressCurve gsc = new GenerateStressCurve();
//            gsc.executeDirectory(dirname, matrixname, new Dimension(800, 800), 0.00125f);
//        }
//
//        {
//            String dirname = "/home/paulovich/Dropbox/dados/PLSP/shuttle/";
//            String matrixname = dirname + "shuttle_trn_corr-normcols.data";
//            GenerateStressCurve gsc = new GenerateStressCurve();
//            gsc.executeDirectory(dirname, matrixname, new Dimension(800, 800), 0.000125f);
//        }
    }

    public void executeDirectory(String dirname, String matrixname, Dimension size, float alpha) throws IOException {
        AbstractMatrix data = MatrixFactory.getInstance(matrixname);

        File dir = new File(dirname);
        File[] files = dir.listFiles(new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                return pathname.getPath().endsWith(".prj");
            }
        });

        for (File f : files) {
            executeFile(f.getPath(), matrixname, size, alpha);
        }
    }

    public void executeFile(String filename, String matrixname, Dimension size, float alpha) throws IOException {
        AbstractMatrix data = MatrixFactory.getInstance(matrixname);

        System.out.println(filename);
        AbstractMatrix proj = new DenseMatrix();
        proj.load(filename);

        StressCurve sc = new StressCurve();
        BufferedImage image = sc.generate(size, alpha, proj, data, new Euclidean());
        ImageIO.write(image, "png", new File(filename + "-stresscurve_new.png"));
    }
}
