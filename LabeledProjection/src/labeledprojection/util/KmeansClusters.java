/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package labeledprojection.util;

import datamining.clustering.BKmeans;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;
import projection.model.Scalar;

/**
 *
 * @author Fernando Vieira Paulovich, Roberto Pinho
 */
public class KmeansClusters {

    /** Creates a new instance of TopicClusters
     * @param model
     */
    public KmeansClusters(ProjectionModel model) {
        this.model = model;
    }

    public Scalar execute(javax.swing.JFrame parent) throws IOException {

        //Create the clusters
        AbstractMatrix matrix = new DenseMatrix();

        for (int i = 0; i < model.getInstances().size(); i++) {
            if (!model.getInstances().get(i).toString().isEmpty()) {
                float[] point = new float[2];
                point[0] = ((ProjectionInstance)this.model.getInstances().get(i)).getX();
                point[1] = ((ProjectionInstance)this.model.getInstances().get(i)).getY();
                matrix.addRow(new DenseVector(point));
            }
        }

        ArrayList<ArrayList<Integer>> clusters = null;
        String scalarName = null;

        String inputValue = (String) JOptionPane.showInputDialog(null,
                "Choose the number of clusters:", "Defining the Number of Clusters",
                JOptionPane.QUESTION_MESSAGE, null, null,
                (Object) Integer.toString((int) Math.sqrt(this.model.getInstances().size())));

        if (inputValue == null) {
            return null;
        }

        int nclusters = Integer.parseInt(inputValue);

        try {
            BKmeans km = new BKmeans(nclusters);
            clusters = km.execute(new Euclidean(), matrix);
            scalarName = "kmeans-" + clusters.size();
            //km.printClusters(matrix);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        Scalar s = model.addScalar(scalarName);
        for (int c = 0; c < clusters.size(); c++) {
            for (int v = 0; v < clusters.get(c).size(); v++) {
                ((ProjectionInstance)this.model.getInstances().get(clusters.get(c).get(v))).setScalarValue(s, c);
            }
        }

        return s;
        //if (s != null) return s;
        //else return model.getScalarByName("topics");
    }

    private ProjectionModel model;
}
