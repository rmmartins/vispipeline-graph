package labeledprojection.util;

import java.awt.Component;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import javax.swing.JFileChooser;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.model.AbstractModel;
import visualizationbasics.util.OpenDialog;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.filter.AbstractFilter;


/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class ProjectionOpenDialog extends OpenDialog {
    
    public static int showOpenDirDialog(Component parent, String directory) {

        dialog = new JFileChooser();
        int oldSelectionMode = dialog.getFileSelectionMode();
        dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        dialog.setDialogTitle("Open Directory");
        dialog.setCurrentDirectory(new File(directory));
        int result = dialog.showOpenDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            _filename = dialog.getSelectedFile().getAbsolutePath();
        }
        dialog.setFileSelectionMode(oldSelectionMode);
        return result;
    }

    public static int showOpenDialogMultipleFiles(PropertiesManager spm,
            AbstractFilter filter, Component parent) {
        if (ProjectionOpenDialog.dialog == null) {
            ProjectionOpenDialog.dialog = new javax.swing.JFileChooser();
        }

        _filename = null;

        dialog.resetChoosableFileFilters();
        dialog.setAcceptAllFileFilterUsed(false);
        if (filter != null) {
            dialog.setFileFilter(filter);
        }
        dialog.setMultiSelectionEnabled(false);
        dialog.setDialogTitle("Open file");
        dialog.setSelectedFile(new File(""));

        dialog.setCurrentDirectory(new File(spm.getProperty(filter.getProperty())));

        dialog.setMultiSelectionEnabled(true);
        int result = dialog.showOpenDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            File[] files = dialog.getSelectedFiles();
            files = sortFilesByLastModDate(files,0);
            String filesS = "";
            if (files.length > 0) {
                for (int i=0;i<files.length;i++)
                    filesS += files[i] +";";
                filesS = filesS.substring(0,filesS.length()-1);
            }
            //_filename = dialog.getSelectedFile().getAbsolutePath();
            _filename = filesS;
            spm.setProperty(filter.getProperty(), dialog.getSelectedFile().getParent());
        }

        return result;
    }

    public static int showOpenDialogMultipleFilter(PropertiesManager spm, ArrayList<AbstractFilter> filters, Component parent) {

        if (OpenDialog.dialog == null) {
            OpenDialog.dialog = new javax.swing.JFileChooser();
        }

        _filename = null;

        dialog.resetChoosableFileFilters();
        dialog.setAcceptAllFileFilterUsed(false);
        if (filters != null && !filters.isEmpty()) {
            for (int i=0;i<filters.size();i++)
                dialog.addChoosableFileFilter(filters.get(i));

            dialog.setMultiSelectionEnabled(false);
            dialog.setDialogTitle("Open file");
            dialog.setSelectedFile(new File(""));

            dialog.setCurrentDirectory(new File(spm.getProperty(filters.get(0).getProperty())));

            int result = dialog.showOpenDialog(parent);
            if (result == JFileChooser.APPROVE_OPTION) {
                _filename = dialog.getSelectedFile().getAbsolutePath();
                spm.setProperty(filters.get(0).getProperty(), dialog.getSelectedFile().getParent());
            }

            return result;


        }
        return -1;
    }

    /**
    * Sorts an array of Files by the last modified date property;
    * @param fList : An array of Java "File" objects, not sorted
    * @param order : ordering type - 0: ascending, 1: descending
    * @return File[] : An array of Java "File" objects, sorted by last modified date
    * @author C. Peter Chen http://dev-notes.com
    * @date 20080527
    */
    public static File[] sortFilesByLastModDate(File[] fList, final int order) {

	Arrays.sort(fList, new Comparator() {
            public int compare(Object file1, Object file2) {
                if (file1 instanceof File && file2 instanceof File) {
                    if (order == 1) { //Descending order...
                        return (int)(((File)file2).lastModified() - ((File)file1).lastModified());
                    } else { //ascending order...
                        return (int)(((File)file1).lastModified() - ((File)file2).lastModified());
                    }
                } else return 0;
            }
	});
	return fList;
}

    public static ImageCollection openImages(PropertiesManager spm, AbstractFilter filter, java.awt.Component parent) {
        ImageCollection im = null;
        int r = showOpenDialog(spm,filter,parent);
        if (r == JFileChooser.APPROVE_OPTION) im = new ImageCollection(_filename);
        return im;
    }

    public static boolean checkImages(ImageCollection ic, AbstractModel model) {
        //checking how many instances of the model are in the corpus
        int count = 0;
        int valid = 0;
        if (ic == null) return false;
        for (AbstractInstance ins : model.getInstances()) {
           String filename = ins.toString().trim();
           if (ic.getImageUrls().contains(filename)) {
              count++;
           }
           if (!ins.toString().trim().isEmpty()) {
              valid++;
           }
        }
        return !(count != valid);
   }

    public static String getFilename() {
        return _filename;
    }

}
