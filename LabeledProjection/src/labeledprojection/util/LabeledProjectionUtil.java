/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labeledprojection.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import java.util.logging.Level;
import labeledprojection.model.LabeledProjectionInstance;
import labeledprojection.model.LabeledProjectionModel;
import projection.model.Scalar;
import projection.util.ProjectionUtil;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author Frizzi
 */
public class LabeledProjectionUtil extends ProjectionUtil{
   
    //cdata;year
    //filename1.txt;1.3;0.70
    //filename2.txt;4.0;0.06
    //filename3.txt;6.7;0.40
    //filename4.txt;3.0;0.12
    //filename5.txt;8.9;0.11
    public static void importScalars(LabeledProjectionModel model, String filename) throws IOException {

        BufferedReader in = null;

        try {
            in = new BufferedReader(new java.io.FileReader(filename));
            ArrayList<String> scalars = new ArrayList<String>();

            //Capturing the scalar names
            int linenumber = 0;
            String line = null;
            while ((line = in.readLine()) != null) {
                linenumber++;

                //ignore comments
                if (line.trim().length() > 0 && line.lastIndexOf('#') == -1) {
                    StringTokenizer t = new StringTokenizer(line, ";");

                    while (t.hasMoreTokens()) {
                        scalars.add(t.nextToken().trim());
                    }

                    break;
                }
            }

            int indexScalar = 1;
            for (int i=0;i<scalars.size();i++) {
                if (scalars.get(i).equalsIgnoreCase("...") && model.getScalarByName(scalars.get(i)) != null) {
                    while (model.getScalarByName(scalars.get(i)+"_"+indexScalar) != null)
                        indexScalar++;
                    model.addScalar(scalars.get(i)+"_"+indexScalar);
                    scalars.set(i,scalars.get(i)+"_"+indexScalar);
                }else
                    model.addScalar(scalars.get(i));
            }

            //index for the instances
            //HashMap<Integer, AbstractInstance> index = new HashMap<Integer, AbstractInstance>();
            HashMap<String, AbstractInstance> index = new HashMap<String, AbstractInstance>();
            
            for (AbstractInstance pi : model.getInstances()) {
                //index.put(pi.getId(), pi);
                index.put(pi.toString(), pi);
            }

            int instancesProcessed = 0;

            //reading the scalars            
            while ((line = in.readLine()) != null) {
                linenumber++;
                ArrayList<Float> values = new ArrayList<Float>();

                //ignore comments
                if (line.trim().length() > 0 && line.lastIndexOf('#') == -1) {
                    StringTokenizer t = new StringTokenizer(line, ";", false);

                    //Capturing the filename
                    String fname = t.nextToken().trim();

                    //Capturing the scalar values
                    while (t.hasMoreTokens()) {
                        float value = Float.parseFloat(t.nextToken().trim());
                        values.add(value);
                    }

                    //checking the data
                    if (scalars.size() != values.size()) {
                        throw new IOException("The number of values for one scalar " +
                                "does not match with the number of declared scalars.\r\n" +
                                "Check line " + linenumber + " of the file.");
                    }

                    //Adding the scalar values to the instance
                    //AbstractInstance pi = index.get(Integer.parseInt(fname));
                    AbstractInstance pi = index.get(fname);
//                    Scalar s = null;

                    if (pi != null) {
                        for (int i = 0; i < scalars.size(); i++) {
//                            int indexScalar = 1;
//                            if (model.getScalarByName(scalars.get(i)) != null) {
//                                while (model.getScalarByName(scalars.get(i)+"_"+indexScalar) != null)
//                                    indexScalar++;
//                                s = model.addScalar(scalars.get(i)+"_"+indexScalar);
//                            }else
//                                s = model.addScalar(scalars.get(i));
                            if (model.getScalarByName(scalars.get(i)) != null)
                                ((LabeledProjectionInstance) pi).setScalarValue(model.getScalarByName(scalars.get(i)),values.get(i));
                        }
                    }
                    instancesProcessed++;
                    if (instancesProcessed >= model.getInstances().size()) break;
                }
            }
        } catch (FileNotFoundException ex) {
            throw new IOException(ex.getMessage());
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(ProjectionUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static void exportScalars(LabeledProjectionModel model, String filename) throws IOException {
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new FileWriter(filename));

            //writing the scalar names
            for (int i = 0; i < model.getScalars().size(); i++) {
                out.write(model.getScalars().get(i).getName().replaceAll(";", "_"));
                if (i < model.getScalars().size() - 1) {
                    out.write(";");
                }
            }

            out.write("\r\n");

            //writing the scalar values
            for (AbstractInstance pi : model.getInstances()) {
                out.write(pi.toString() + ";");

                for (int i = 0; i < model.getScalars().size(); i++) {
                    float scalar = ((LabeledProjectionInstance) pi).getScalarValue(model.getScalars().get(i));
                    out.write(Float.toString(scalar).replaceAll(";", "_"));
                    if (i < model.getScalars().size() - 1) {
                        out.write(";");
                    }
                }

                out.write("\r\n");
            }

        } catch (IOException e) {
            throw new IOException(e.getMessage());
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    Logger.getLogger(ProjectionUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
        
 
    
}
