/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package labeledprojection.util;

import projection.util.ProjectionConstants;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public interface LabeledProjectionConstants extends ProjectionConstants {

    public static final String DELAUNAY = "Delaunay";
    public static final String AXIS_PREFIX = "Axis_";
    public static final String XMEANS = "X-means";
    public static final String CLUSTREAM = "Clustream";
    public static final String KMEANS = "Kmeans";
    
}
