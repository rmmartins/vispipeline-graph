/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package classifiers.util;

import visualizationbasics.util.filter.AbstractFilter;

/**
 *
 * @author Fernando V. Paulovic
 */
public class ModelFilter extends AbstractFilter {

    @Override
    public String getDescription() {
        return "SVM Model (*.model)";
    }

    @Override
    public String getProperty() {
        return "XML.DIR";
    }

    @Override
    public String getFileExtension() {
        return "model";
    }

}
