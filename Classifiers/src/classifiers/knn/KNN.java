/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package classifiers.knn;

import distance.dissimilarity.AbstractDissimilarity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;

/**
 *
 * @author Jose Gustavo
 */
public class KNN {

    private AbstractMatrix trainingSet;
    private AbstractDissimilarity diss;
    private int k;
    private boolean useWeight;

    public KNN(AbstractMatrix ts, AbstractDissimilarity d, int n, boolean u) {
        trainingSet = ts;
        diss = d;
        k = n;
        useWeight = u;
    }

    public KNN(String tsfile, AbstractDissimilarity d, int n, boolean u) throws IOException {
        this(MatrixFactory.getInstance(tsfile),d,n,u);
    }

    private ArrayList<KNNDistance> calculateDistances(AbstractVector input) {

        int nrElements = trainingSet.getRowCount();
        //float distance;
        ArrayList<KNNDistance> distances = new ArrayList<KNNDistance>();

        for (int i=0;i<nrElements;i++) {
            float distance = diss.calculate(input,trainingSet.getRow(i));
            distances.add(new KNNDistance(trainingSet.getRow(i).getKlass(),distance));
        }
        
        //Ordenando o Arraylist de acordo com a distancia...
        Collections.sort(distances,new Comparator() {
                                            public int compare(Object o1, Object o2) {
                                                KNNDistance p1 = (KNNDistance) o1;
                                                KNNDistance p2 = (KNNDistance) o2;
                                                if (p1.distance < p2.distance) return -1;
                                                else if (p1.distance > p2.distance) return +1;
                                                else return 0;
                                            }
                                        });
	return distances;
        
    }

    public Float classifyInstance(AbstractVector input) {

	ArrayList<KNNDistance> distances = calculateDistances(input);
        HashMap t = new HashMap();
        int tam = 0;
        if (this.k > distances.size()) tam = distances.size();
        else tam = this.k;
        Float qt;
        //Contando, dentre os k vizinhos mais proximos, quantos sao de cada classe...
        for (int i=0;i<tam;i++) {
            if (t.containsKey((Float)distances.get(i).klass)) {
                qt = (Float)t.get((Float)distances.get(i).klass);
                if (useWeight)
                    qt = qt + (1.0f/distances.get(i).distance);
                else qt++;
                t.put((Float)distances.get(i).klass,qt);
            }else {
                if (useWeight)
                    t.put((Float)distances.get(i).klass,(1.0f/distances.get(i).distance));
                else t.put((Float) distances.get(i).klass,1.0f);
            }
        }
        //Selecionando a classe que mais aparece entre os k vizinhos...
        Float key,klass = Float.NaN;
        qt = 0.0f;
        Float valor;
        Iterator it = t.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            key = (Float) e.getKey();
            valor = (Float)e.getValue();
            if (valor > qt) {
                qt = valor;
                klass = key;
            }
        }
        return klass;
	}

    public void classify(AbstractMatrix testSet) {
        int nrElements = testSet.getRowCount();
        for (int i=0;i<nrElements;i++)
            testSet.getRow(i).setKlass(classifyInstance(testSet.getRow(i)));
    }

}

class KNNDistance {
    float klass;
    float distance;

    public KNNDistance(float classe,float distancia) {
        this.klass = classe;
        this.distance = distancia;
    }
}
