/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package classifiers.knn;

import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo
 */
@VisComponent(hierarchy = "Classification.KNN",
name = "K-Nearest Neighbors",
description = "Classify an image collection using KNN.")
public class KNNComp implements AbstractComponent {

    public void execute() throws IOException {

        if (trainingMatrix == null)
            trainingMatrix = MatrixFactory.getInstance(trainingSet);
        AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
        KNN knn = new KNN(trainingMatrix,diss,numNeighbors,useWeight);
        knn.classify(testMatrix);
        dmat = new DistanceMatrix(testMatrix,diss);

        //saving the matrix
        if (testMatrix != null && saveMatrix && matrixFilename != null && !matrixFilename.isEmpty()) {
            //saving the points matrix
            System.out.println("Saving points matrix...");
            testMatrix.save(matrixFilename);
        }

        if (dmat != null && saveDmat && dmatFilename != null && !dmatFilename.isEmpty()) {
            //saving distance matrix
            System.out.println("Saving distance matrix...");
            dmat.save(dmatFilename);
        }
        
    }

    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new KNNParamView(this);
        }
        return paramview;
    }

    public void reset() {
        trainingMatrix = null;
        testMatrix = null;
        dmat = null;
        //numNeighbors = 1;
        //disstype = DissimilarityFactory.getInstance(DissimilarityType.EUCLIDEAN);
        //trainingSet = "";
    }
    
    public void input(@Param(name = "test matrix") AbstractMatrix testMatrix) {
        try {
            this.testMatrix = (AbstractMatrix) testMatrix.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(KNNComp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public AbstractMatrix output() {
        return testMatrix;
    }

    public DistanceMatrix outputDmat() {
        return dmat;
    }

    public AbstractMatrix getTrainingMatrix() {
        return trainingMatrix;
    }

    public void setTrainingMatrix(AbstractMatrix trainingMatrix) {
        this.trainingMatrix = trainingMatrix;
    }

    public int getNumNeighbors() {
        return numNeighbors;
    }

    public void setNumNeighbors(int numNeighbors) {
        this.numNeighbors = numNeighbors;
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public String getTrainingSet() {
        return trainingSet;
    }

    public void setTrainingSet(String ts) {
        trainingSet = ts;
    }

    public boolean isSaveMatrix() {
        return saveMatrix;
    }

    public void setSaveMatrix(boolean saveMatrix) {
        this.saveMatrix = saveMatrix;
    }

    public boolean isSaveDmat() {
        return saveDmat;
    }

    public void setSaveDmat(boolean saveDmat) {
        this.saveDmat = saveDmat;
    }

    public String getMatrixFilename() {
        return matrixFilename;
    }

    public void setMatrixFilename(String matrixFilename) {
        this.matrixFilename = matrixFilename;
    }

    public String getDmatFilename() {
        return dmatFilename;
    }

    public void setDmatFilename(String dmatFilename) {
        this.dmatFilename = dmatFilename;
    }

    public boolean isUseWeight() {
        return useWeight;
    }

    public void setUseWeight(boolean useWeight) {
        this.useWeight = useWeight;
    }

    public static final long serialVersionUID = 1L;
    private transient KNNParamView paramview;
    private transient AbstractMatrix trainingMatrix;
    private transient AbstractMatrix testMatrix;
    private transient DistanceMatrix dmat;
    private int numNeighbors = 1;
    private boolean useWeight = false;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private String trainingSet = "";
    private transient boolean saveMatrix, saveDmat;
    private transient String matrixFilename, dmatFilename;

}
