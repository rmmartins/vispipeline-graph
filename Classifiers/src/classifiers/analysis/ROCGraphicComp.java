/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package classifiers.analysis;

import distance.DistanceMatrix;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo
 */
@VisComponent(hierarchy = "Classification.Classifier Analysis",
name = "ROC Graphic",
description = "Creates a ROC Graphic of a classifier.")
public class ROCGraphicComp implements AbstractComponent {

    public void execute() throws IOException {
        dts = new ArrayList<DataAnalysis>();
        
        if ((classificationMatrices == null || classificationMatrices.isEmpty())&&
            (classificationDmats == null || classificationDmats.isEmpty()))    
            throw new IOException("A classified points matrix or distance matrix should be provided.");
        
        if (this.dataFileName.trim().length() > 0) {
            if (this.dataFileType == 0) {
                try {
                    groundTruthMatrix = MatrixFactory.getInstance(this.dataFileName);
                    if (classificationMatrices != null && !classificationMatrices.isEmpty())
                        for (int i=0;i<classificationMatrices.size();i++) {
                            DataAnalysis dt = new DataAnalysis();
                            dt.performDataAnalysis(classificationMatrices.get(i),dt.getClasses(groundTruthMatrix),null);
                            dt.setSource(classificationMatrices.toString());
                            dts.add(dt);
                        }
                    if (classificationDmats != null && !classificationDmats.isEmpty()) 
                        for (int i=0;i<classificationDmats.size();i++) {
                            DataAnalysis dt = new DataAnalysis();
                            dt.performDataAnalysis(classificationDmats.get(i),dt.getClasses(groundTruthMatrix),null);
                            dt.setSource(classificationDmats.toString());
                            dts.add(dt);
                        }
                } catch (IOException ex) {
                    Logger.getLogger(ClassifierAnalysisComp.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (this.dataFileType == 1) {
                try {
                    groundTruthDmat = new DistanceMatrix(this.dataFileName);
                    if (classificationMatrices != null && !classificationMatrices.isEmpty()) 
                        for (int i=0;i<classificationMatrices.size();i++) {
                            DataAnalysis dt = new DataAnalysis();
                            dt.performDataAnalysis(classificationMatrices.get(i),dt.getClasses(groundTruthDmat),null);
                            dt.setSource(classificationMatrices.toString());
                            dts.add(dt);
                        }
                    if (classificationDmats != null && !classificationDmats.isEmpty()) 
                        for (int i=0;i<classificationDmats.size();i++) {
                            DataAnalysis dt = new DataAnalysis();
                            dt.performDataAnalysis(classificationDmats.get(i),dt.getClasses(groundTruthDmat),null);
                            dt.setSource(classificationDmats.toString());
                            dts.add(dt);
                        }
                } catch (IOException ex) {
                    Logger.getLogger(ClassifierAnalysisComp.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            System.out.println("Data filename not informed.");
        }
        ROCGraphicConsole.getInstance(null,dts).display("Title");
    }

    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new ROCGraphicParamView(this);
        }
        return paramview;
    }

    public void reset() {
        classificationMatrices = null;
        classificationDmats = null;
        dts = null;
    }
    
    public void attach(@Param(name = "Classified Point Matrices") AbstractMatrix m) {
        if (classificationMatrices == null)
            classificationMatrices = new ArrayList<AbstractMatrix>();
        if (classificationMatrices != null)
            classificationMatrices.add(m);
    }
    
    public void attach(@Param(name = "Classified Distance Matrices") DistanceMatrix dm) {
        if (classificationDmats == null)
            classificationDmats = new ArrayList<DistanceMatrix>();
        if (classificationDmats != null)
            classificationDmats.add(dm);
    }

    public String getDataFileName() {
        return dataFileName;
    }

    public void setDataFileName(String dataFileName) {
        this.dataFileName = dataFileName;
    }

    public int getDataFileType() {
        return dataFileType;
    }

    public void setDataFileType(int dataFileType) {
        this.dataFileType = dataFileType;
    }
    
    public static final long serialVersionUID = 1L;
    private transient ROCGraphicParamView paramview;
    private String dataFileName = "";
    private int dataFileType = 0;
    private String windowLabel = "";
    private transient ArrayList<AbstractMatrix> classificationMatrices;
    private transient ArrayList<DistanceMatrix> classificationDmats;
    private transient ArrayList<DataAnalysis> dts;
    private transient AbstractMatrix groundTruthMatrix;
    private transient DistanceMatrix groundTruthDmat;

    public void setWindowLabel(String text) {
        windowLabel = text;
    }

    public String getWindowLabel() {
        return windowLabel;
    }
    
}
