/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package classifiers.analysis;

import distance.DistanceMatrix;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import visualizationbasics.color.ColorTable;

/**
 *
 * @author Jose Gustavo
 */
@VisComponent(hierarchy = "Classification.Classifier Analysis",
name = "Classifier Analysis",
description = "Performs several analysis about a classifier.")
public class ClassifierAnalysisComp implements AbstractComponent {

    public void execute() throws IOException {

        dt = new DataAnalysis();
        if (classificationMatrix == null && classificationDmat == null)
            throw new IOException("A classified points matrix or distance matrix should be provided.");
        
        if (this.groundTruthMatrix == null && this.groundTruthDmat == null) {
            if (this.dataFileName.trim().length() > 0) {
                if (this.dataFileType == 0) {
                    try {
                        groundTruthMatrix = MatrixFactory.getInstance(this.dataFileName);
//                        if (classificationMatrix != null)
//                            dt.performDataAnalysis(classificationMatrix,dt.getClasses(groundTruthMatrix));
//                        else if (classificationDmat != null)
//                            dt.performDataAnalysis(classificationDmat,dt.getClasses(groundTruthMatrix));
                    } catch (IOException ex) {
                        Logger.getLogger(ClassifierAnalysisComp.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (this.dataFileType == 1) {
                    try {
                        groundTruthDmat = new DistanceMatrix(this.dataFileName);
//                        if (classificationMatrix != null)
//                            dt.performDataAnalysis(classificationMatrix,dt.getClasses(groundTruthDmat));
//                        else if (classificationDmat != null)
//                            dt.performDataAnalysis(classificationDmat,dt.getClasses(groundTruthDmat));
                    } catch (IOException ex) {
                        Logger.getLogger(ClassifierAnalysisComp.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                System.out.println("Data filename not informed.");
                return;
            }
        }
        if (this.groundTruthMatrix != null) {
            if (classificationMatrix != null)
                dt.performDataAnalysis(classificationMatrix,dt.getClasses(groundTruthMatrix),colortable);
            else if (classificationDmat != null)
                dt.performDataAnalysis(classificationDmat,dt.getClasses(groundTruthMatrix),colortable);
        }else if (this.groundTruthDmat != null) {
            if (classificationMatrix != null)
                dt.performDataAnalysis(classificationMatrix,dt.getClasses(groundTruthDmat),colortable);
            else if (classificationDmat != null)
                dt.performDataAnalysis(classificationDmat,dt.getClasses(groundTruthDmat),colortable);
        }
        dt.setSource(sourceLabel);
        ClassifierAnalysisConsole.getInstance(parent,dt).display(sourceLabel);
            
    }

    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new ClassifierAnalysisParamView(this);
        }
        return paramview;
    }

    public void reset() {
        groundTruthMatrix = null;
        classificationMatrix = null;
        groundTruthDmat = null;
        classificationDmat = null;
    }
    
    public void input(@Param(name = "Classified points matrix") AbstractMatrix classifiedMatrix) {
        try {
            this.classificationMatrix = (AbstractMatrix) classifiedMatrix.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(ClassifierAnalysisComp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void input(@Param(name = "Classified distance matrix") DistanceMatrix classifiedDmat) {
        try {
            this.classificationDmat = (DistanceMatrix) classifiedDmat.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(ClassifierAnalysisComp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getDataFileName() {
        return dataFileName;
    }

    public void setDataFileName(String dataFileName) {
        this.dataFileName = dataFileName;
    }

    public int getDataFileType() {
        return dataFileType;
    }

    public void setDataFileType(int dataFileType) {
        this.dataFileType = dataFileType;
    }

    public AbstractMatrix getGroundTruthMatrix() {
        return groundTruthMatrix;
    }

    public void setGroundTruthMatrix(AbstractMatrix gtMatrix) {
        this.groundTruthMatrix = gtMatrix;
    }

    public void setSourceLabel(String text) {
        sourceLabel = text;
    }

    public String getSourceLabel() {
        return sourceLabel;
    }
    
    public void setColorTable(ColorTable ct) {
        this.colortable = ct;
    }
    
    public void setParent(JDialog p) {
        this.parent = p;
    }

    public static final long serialVersionUID = 1L;
    private transient ClassifierAnalysisParamView paramview;
    private String dataFileName = "", sourceLabel = "";
    private int dataFileType = 0;
    private transient AbstractMatrix groundTruthMatrix, classificationMatrix;
    private transient DistanceMatrix groundTruthDmat, classificationDmat;
    private transient DataAnalysis dt;
    private ColorTable colortable;
    private JDialog parent;

}
