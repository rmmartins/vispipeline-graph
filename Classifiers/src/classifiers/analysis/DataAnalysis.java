package classifiers.analysis;

import distance.DistanceMatrix;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import visualizationbasics.color.ColorTable;

/**
 *
 * @author Jose Gustavo
 */
public class DataAnalysis {

    String source;
    public int [][] confusionMatrix;
    public float FPR[];
    float FNR[];
    public float[] precision;
    public float[] recall;
    public float[] classes;
    public float[] accuracy;
    public Color[] colorClasses;
    
    public String getHMTL() {

        NumberFormat formatter = new DecimalFormat("0.00");
        String result = "";

        result = "<html><center>"+source+"</center><br><br>"
                + "<table border=\"1\" cellpadding=\"2\" cellspacing=\"2\" bgcolor=\"#FFFFFF\"><tbody>"
                + "<tr><td colspan=\"2\" rowspan=\"2\"></td><td align=\"center\""
                + " colspan=\""+classes.length+"\" rowspan=\"1\"><b>Classification</b></td><td></td></tr><tr>";

        for (int i=0;i<classes.length;i++)
            result += "<td align=\"center\" bgcolor=\""+Integer.toHexString(colorClasses[i].getRGB()&0x00ffffff)+"\"><b>"+Float.toString(classes[i])+"</b></td>";

        result += "<td align=\"center\"><b>FNR</b></td></tr>";

        boolean first = true;
        for (int row=0;row<confusionMatrix.length;row++) {
            result += "<tr>";
            if (first) {
                result += "<td align=\"center\" colspan=\"1\" rowspan=\""+classes.length+"\"><b>Ground Truth</b></td>";
                first = false;
            }
            result += "<td align=\"center\" bgcolor=\""+Integer.toHexString(colorClasses[row].getRGB()&0x00ffffff)+"\"><b>"+Float.toString(classes[row])+"</b></td>";
            for (int col=0;col<confusionMatrix[row].length;col++) {
                result += "<td align=\"center\">"+Integer.toString(confusionMatrix[row][col])+"</td>";
            }
            //result += "<td align=\"center\">"+formatter.format(FNR[row])+"</td></tr>";
            result += "<td align=\"center\">"+formatter.format(FNR[row]*100)+"%</td></tr>";
        }

        result += "<tr><td align=\"center\" colspan=\"2\"><b>FPR</b></td>";
        
        for (int i=0;i<FPR.length;i++) {
            //result += "<td>"+formatter.format(FPR[i])+"</td>";
            result += "<td>"+formatter.format(FPR[i]*100)+"%</td>";
        }
        result += "<td></td></tr>";

        result += "</tbody></table>";

        result += "<br><pre>Accuracy : "+formatter.format(getAvgAccuracy()*100)+"%";
        result += "<br>Precision : "+formatter.format(getAvgPrecision()*100)+"%";
        result += "<br>Recall : "+formatter.format(getAvgRecall()*100)+"%";
        result += "<br>F1 : "+formatter.format(getF1())+"</pre>";
        
        result += "</html>";

        return result;
    }

    public int[] getSizeClasses() {
        int sizeClasses[] = new int[classes.length];
        for (int row=0;row<confusionMatrix.length;row++)
            for (int col=0;col<confusionMatrix[row].length;col++)
                sizeClasses[row] += confusionMatrix[row][col];
        return sizeClasses;

    }

    public int getTotalElements() {
        int totalSum = 0;
        for (int row=0;row<confusionMatrix.length;row++)
            for (int col=0;col<confusionMatrix[row].length;col++)
                totalSum += confusionMatrix[row][col];
        return totalSum;
    }

    public float getTFP() {

        int sizeClasses[] = getSizeClasses();
        int totalSum = getTotalElements();

        float avgFPR = 0;
        for (int i=0;i<FPR.length;i++) {
            avgFPR += FPR[i]*sizeClasses[i];
        }
        if (totalSum != 0)
            avgFPR /= totalSum;

        return avgFPR;

    }

    public float getAvgPrecision() {
        float avgPrecision = 0;
        int sizeClasses[] = getSizeClasses();
        int totalSum = getTotalElements();
        for (int i=0;i<classes.length;i++)
            avgPrecision += precision[i]*sizeClasses[i];
        avgPrecision /= totalSum;
        return avgPrecision;
    }

    public float getAvgRecall() {
        float avgRecall = 0;
        int sizeClasses[] = getSizeClasses();
        int totalSum = getTotalElements();
        for (int i=0;i<classes.length;i++)
            avgRecall += recall[i]*sizeClasses[i];
        avgRecall /= totalSum;
        return avgRecall;
    }

    public float getAvgAccuracy() {
        float avgAccuracy = 0;
        int sizeClasses[] = getSizeClasses();
        int totalSum = getTotalElements();
        for (int i=0;i<classes.length;i++)
            avgAccuracy += accuracy[i]*sizeClasses[i];
        avgAccuracy /= totalSum;
        return avgAccuracy;
    }

    public float getF1() {
        float p = getAvgPrecision();
        float r = getAvgRecall();
        if ((r+p) != 0)
            return (2.0f*r*p)/(r+p);
        else
            return -1;
    }

    public void setSource(String sourceLabel) {
        source = sourceLabel;
    }

    public String getSource() {
        return source;
    }

    public TreeMap<Float,ArrayList<Integer>> getClasses(AbstractMatrix groundTruthMatrix) {
        TreeMap<Float,ArrayList<Integer>> lClasses = new TreeMap<Float,ArrayList<Integer>>();
        if (groundTruthMatrix != null) {
            for (int i=0;i<groundTruthMatrix.getRowCount();i++) {
                AbstractVector row = groundTruthMatrix.getRow(i);
                if (!lClasses.containsKey(row.getKlass()))
                    lClasses.put(row.getKlass(),new ArrayList<Integer>());
                lClasses.get(row.getKlass()).add(row.getId());
            }
            return lClasses;
        }
        return null;
    }
    
    public TreeMap<Float,ArrayList<Integer>> getClasses(DistanceMatrix groundTruthDmat) {
        TreeMap<Float,ArrayList<Integer>> lClasses = new TreeMap<Float,ArrayList<Integer>>();
        if (groundTruthDmat != null) {
            for (int i=0;i<groundTruthDmat.getElementCount();i++) {
                float klass = groundTruthDmat.getClassData()[i];
                if (!lClasses.containsKey(klass))
                    lClasses.put(klass,new ArrayList<Integer>());
                lClasses.get(klass).add(groundTruthDmat.getIds().get(i));
            }
        }
        return null;
    }
    
    public void performDataAnalysis(AbstractMatrix classificationMatrix, TreeMap<Float,ArrayList<Integer>> lClasses, ColorTable ct) {

        if (lClasses == null) {
            System.out.println("Error - No groundtruth dataset (dmat or data points) retrieved.");
            return;// null;
        }

        int numClasses = lClasses.size();

        //DataAnalysis this = new DataAnalysis();

        this.classes = new float[lClasses.size()];

        this.colorClasses = new Color[this.classes.length];
        
        for (int i=0;i<lClasses.keySet().toArray().length;i++)
            this.classes[i] = (Float)lClasses.keySet().toArray()[i];
        
        for (int i = 0; i < this.classes.length; i++) {
            if (ct == null)
                this.colorClasses[i] = Color.getHSBColor((float) i / this.classes.length, 1, 1);
            else {
                float normValue = this.classes[i]/this.classes[this.classes.length-1];
                this.colorClasses[i] = ct.getColor(normValue);
            }
        }

        if (classificationMatrix != null)
            this.confusionMatrix = getConfusingMatrix(lClasses, classificationMatrix);
        else
            System.out.println("Error - No classification dataset (dmat or data points) retrieved.");

        ArrayList<Integer> elementsClass = new ArrayList<Integer>();
        int totalElements = 0;
        Iterator it = lClasses.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            int numberElementsClass = ((ArrayList<Integer>)e.getValue()).size();
            elementsClass.add(numberElementsClass);
            totalElements += numberElementsClass;
        }

        this.FPR =  new float[numClasses];
        this.FNR =  new float[numClasses];
        this.precision = new float[numClasses];
        this.recall = new float[numClasses];
        this.accuracy = new float[numClasses];

        for (int i=0;i<this.confusionMatrix.length;i++) {
            float sumFN = 0;
            float sumFP = 0;
            for (int j=0;j<this.confusionMatrix[i].length;j++) {
                if (i != j) {
                    sumFN += this.confusionMatrix[i][j];
                    sumFP += this.confusionMatrix[j][i];
                }
            }
            this.FNR[i] = sumFN/elementsClass.get(i);
            this.FPR[i] = sumFP/((totalElements - elementsClass.get(i))+sumFP);
            float t = this.confusionMatrix[i][i]+(totalElements - elementsClass.get(i));
            this.accuracy[i] = t/(t+sumFP+sumFN);
            //Calculating Precision

            if ((this.confusionMatrix[i][i]+sumFN) != 0)
                this.recall[i] = this.confusionMatrix[i][i]/(this.confusionMatrix[i][i]+sumFN);
            else
                this.recall[i] = 0;
            if ((this.confusionMatrix[i][i]+sumFP) != 0)
                this.precision[i] = this.confusionMatrix[i][i]/(this.confusionMatrix[i][i]+sumFP);
            else
                this.precision[i] = 0;
        }

        //return this;
    }
    
    public void performDataAnalysis(DistanceMatrix classificationDmat, TreeMap<Float,ArrayList<Integer>> lClasses, ColorTable ct) {

        if (lClasses == null) {
            System.out.println("Error - No groundtruth dataset (dmat or data points) retrieved.");
            return;
        }

        int numClasses = lClasses.size();

        classes = new float[lClasses.size()];

        colorClasses = new Color[classes.length];
        
        for (int i=0;i<lClasses.keySet().toArray().length;i++)
            this.classes[i] = (Float)lClasses.keySet().toArray()[i];
        
        for (int i = 0; i < this.classes.length; i++) {
            if (ct == null)
                this.colorClasses[i] = Color.getHSBColor((float) i / this.classes.length, 1, 1);
            else {
                float normValue = this.classes[i]/this.classes[this.classes.length];
                this.colorClasses[i] = ct.getColor(normValue);
            }
        }
        
//        for (int i = 0; i < classes.length; i++)
//            colorClasses[i] = Color.getHSBColor((float) i / classes.length, 1, 1);
//
//        for (int i=0;i<lClasses.keySet().toArray().length;i++)
//            classes[i] = (Float)lClasses.keySet().toArray()[i];

        if (classificationDmat != null)
            confusionMatrix = getConfusingMatrix(lClasses, classificationDmat);
        else
            System.out.println("Error - No classification dataset (dmat or data points) retrieved.");

        ArrayList<Integer> elementsClass = new ArrayList<Integer>();
        int totalElements = 0;
        Iterator it = lClasses.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            int numberElementsClass = ((ArrayList<Integer>)e.getValue()).size();
            elementsClass.add(numberElementsClass);
            totalElements += numberElementsClass;
        }

        FPR =  new float[numClasses];
        FNR =  new float[numClasses];
        precision = new float[numClasses];
        recall = new float[numClasses];
        accuracy = new float[numClasses];

        for (int i=0;i<confusionMatrix.length;i++) {
            float sumFN = 0;
            float sumFP = 0;
            for (int j=0;j<confusionMatrix[i].length;j++) {
                if (i != j) {
                    sumFN += confusionMatrix[i][j];
                    sumFP += confusionMatrix[j][i];
                }
            }
            FNR[i] = sumFN/elementsClass.get(i);
            FPR[i] = sumFP/((totalElements - elementsClass.get(i))+sumFP);
            float t = confusionMatrix[i][i]+(totalElements - elementsClass.get(i));
            accuracy[i] = t/(t+sumFP+sumFN);
            //Calculating Precision

            if ((confusionMatrix[i][i]+sumFN) != 0)
                recall[i] = confusionMatrix[i][i]/(confusionMatrix[i][i]+sumFN);
            else
                recall[i] = 0;
            if ((confusionMatrix[i][i]+sumFP) != 0)
                precision[i] = confusionMatrix[i][i]/(confusionMatrix[i][i]+sumFP);
            else
                precision[i] = 0;
        }
    }
    
    public int[][] getConfusingMatrix(TreeMap<Float,ArrayList<Integer>> classes, AbstractMatrix m) {

        int[][] lConfusionMatrix = new int[classes.size()][];
        int k = 0;

        Iterator it = classes.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            ArrayList<Integer> values = (ArrayList<Integer>)e.getValue();
            lConfusionMatrix[k] = new int[classes.size()];
            for (int j=0;j<values.size();j++) {
                int index = m.getIds().indexOf(values.get(j));
                if (index > -1) {
                    AbstractVector classifiedRow = m.getRow(index);
                    if (classifiedRow != null) {
                        int classIndex = getKlassIndex(classes,classifiedRow.getKlass());
                        if (classIndex > -1)
                            lConfusionMatrix[k][classIndex]++;
                    }
                }
            }
            k++;
        }
        return lConfusionMatrix;

    }

    public int[][] getConfusingMatrix(TreeMap<Float,ArrayList<Integer>> classes, DistanceMatrix dm) {

        int[][] lConfusionMatrix = new int[classes.size()][];
        int k = 0;

        Iterator it = classes.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            ArrayList<Integer> values = (ArrayList<Integer>)e.getValue();
            lConfusionMatrix[k] = new int[classes.size()];
            for (int j=0;j<values.size();j++) {
                int index = dm.getIds().indexOf(values.get(j));
                if (index > -1) {
                    int classIndex = getKlassIndex(classes,dm.getClassData()[index]);
                    if (classIndex > -1)
                        lConfusionMatrix[k][classIndex]++;
                }
            }
            k++;
        }
        return lConfusionMatrix;

    }

    public int getKlassIndex(TreeMap<Float,ArrayList<Integer>> classes, float klass) {
        if (classes == null) return -1;
        int index = 0;
        Iterator iter = classes.keySet().iterator();
        while (iter.hasNext()) {
            float curKlass = (Float)iter.next();
            if (klass == curKlass)
                return index;
            index++;
        }
        if (index == classes.size())
            return -1;
        else
            return index;
    }
    
}
