/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package classifiers.analysis;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author Jose Gustavo
 */
public class ImagePanel extends JPanel {

    private BufferedImage image;
    public BufferedImage imgSave;
    private String label;

    public ImagePanel(String label, BufferedImage image) {
        this.label = label;
        this.image = image;
    }

    @Override
    public void paintComponent(Graphics g) {
        if (label != null && !label.isEmpty()) {
            imgSave = new BufferedImage(500,500,BufferedImage.TYPE_INT_RGB);
            int d = 60;
            Graphics2D g2 = imgSave.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setStroke(new BasicStroke(2.0f));
            g2.setColor(Color.WHITE);
            //g2.fillRect(0,0,315,325);
            g2.fillRect(0,0,500,500);
            Font f = new Font("Arial",Font.BOLD,16);
            g2.setFont(f);
            java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());
            int width = metrics.stringWidth(label);
            g2.drawImage(image,d+16,d+0,300,300,null);
            g2.setColor(Color.black);
            //g2.drawRect(200-(width/2)-3,318,width+5,25);
            //g2.drawString(label,200-(width/2),325+height-6);
            f = new Font("Arial",Font.PLAIN,18);
            g2.setFont(f);
            width = metrics.stringWidth("False Positive Rate");
            g2.drawString("False Positive Rate",d+150-(width/2),d+310);
            AffineTransform fontAT = new AffineTransform();
            Font theFont = g2.getFont();
            fontAT.rotate(-(Math.PI/2));
            Font theDerivedFont = theFont.deriveFont(fontAT);
            g2.setFont(theDerivedFont);
            width = metrics.stringWidth("True Positive Rate");
            g2.drawString("True Positive Rate",d+20,d+150+(width/2));
            g2.setFont(theFont);
            g.drawImage(imgSave,5,0,imgSave.getWidth(),imgSave.getHeight(),null);
        }else g.drawImage(image,5,0,300,300,null);
    }
}
