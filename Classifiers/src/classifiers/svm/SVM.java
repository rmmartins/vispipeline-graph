/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package classifiers.svm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import libsvm.svm_model;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;
import visualizationbasics.util.BasicsContants;
import visualizationbasics.util.PropertiesManager;

/**
 *
 * @author Jose Gustavo
 */
public class SVM {
    
    //private AbstractMatrix trainingSet;

//    public SVM(String ts) throws IOException {
//        this.trainingSet = MatrixFactory.getInstance(ts);
//    }

    public libsvm.svm_parameter parameters;

    public SVM() {}

    public SVM(int svmType,float coeff,float cost,float degree,float eps,float gamma,int kernelType,float nu,boolean shrinking) {
        parameters = new libsvm.svm_parameter();
        parameters.svm_type = svmType;
        parameters.coef0 = coeff;
        parameters.C = cost;
        parameters.degree = degree;
        parameters.eps = eps;
        parameters.gamma = gamma;
        parameters.kernel_type = kernelType;
        parameters.nu = nu;
        if (shrinking)
            parameters.shrinking = 1;
        else
            parameters.shrinking = 0;
    }

    private void createWekaFile(AbstractMatrix matrix, AbstractMatrix trainingSet, String file) {

        String result = "";
        String eol = System.getProperty("line.separator");

        result = "@RELATION Matrix"+eol+eol;

        for (int i=0;i<matrix.getDimensions();i++)
            result += "@ATTRIBUTE "+i+" REAL"+eol;

        ArrayList classes = new ArrayList();
        if (trainingSet.getClassData() == null || trainingSet.getClassData().length == 0) {
            for (int i=0;i<matrix.getClassData().length;i++) {
                if (!classes.contains(matrix.getClassData()[i]))
                    classes.add(matrix.getClassData()[i]);
            }
        }else {
            for (int i=0;i<trainingSet.getClassData().length;i++) {
                if (!classes.contains(trainingSet.getClassData()[i]))
                    classes.add(trainingSet.getClassData()[i]);
            }
        }

        //if (trainingSet.getClassData() == null || trainingSet.getClassData().length == 0) {
            result += "@ATTRIBUTE class {";
            for (int i=0;i<classes.size()-1;i++) {
                result += classes.get(i)+",";
            }
            result += classes.get(classes.size()-1)+"}";
//        }else {
//            result += "@ATTRIBUTE class {";
//            for (int i=0;i<trainingSet.getClassData().length-1;i++) {
//                result += trainingSet.getClassData()[i]+",";
//            }
//            result += trainingSet.getClassData()[trainingSet.getClassData().length-1]+"}";
//        }

        result += eol+eol+"@DATA"+eol;

        for (int i=0;i<matrix.getRowCount();i++) {
            AbstractVector instance = matrix.getRow(i);
            if (instance != null) {
                for (int k=0;k<instance.size();k++) {
                    result += instance.getValue(k)+",";
                }
                //if (isElement(instance.getKlass(),trainingSet.getClassData()))
                if (classes.contains(instance.getKlass()))
                    result += instance.getKlass()+eol;
                else {
                    if (classes.isEmpty()) result += "0.0"+eol;
                    else result += classes.get(0)+eol;
                }
            }
        }

        result += "%"+eol+"%"+eol+"%"+eol;

        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(file)));
            bw.write(result);
            bw.flush();
            bw.close();
        } catch(IOException e){
            e.printStackTrace();
        }

    }

    private void setClasses(AbstractMatrix testMatrix,String result) {

        String[] lines = result.split("\n");
        int j = 0;

        for (int i=0;i<lines.length;i++) {
            if ((lines[i].trim().isEmpty())||
                (lines[i].trim().startsWith("==="))||
                (lines[i].trim().startsWith("inst"))) {
                continue;
            }

            int bi = lines[i].lastIndexOf(':');

            if (bi != -1) {
                int ei = lines[i].indexOf(' ',bi);
                if (ei != -1) {
                    String k = lines[i].substring(bi+1,ei).trim();
                    try {
                        float klass = Float.parseFloat(k);
                        testMatrix.getRow(j).setKlass(klass);
                        j++;
                    }catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        }
    }

    public AbstractMatrix run(AbstractMatrix trainingSet, AbstractMatrix testSet, String argsClassifier, String modelOutFilename) throws IOException {
        
        String wekaTrainFile = "", wekaTestFile = "";
        PropertiesManager pm = PropertiesManager.getInstance(BasicsContants.PROPFILENAME);
        if (pm != null) {
            wekaTrainFile = wekaTestFile = pm.getProperty("UNZIP.DIR");
            if (wekaTrainFile.isEmpty()) wekaTrainFile = wekaTestFile = System.getProperty("user.dir") + "\\tempDir";
        }
        wekaTrainFile += "\\svmTrain.arff";
        wekaTestFile += "\\svmTest.arff";
        createWekaFile(trainingSet, trainingSet, wekaTrainFile);
        createWekaFile(testSet, trainingSet, wekaTestFile);
        String layoutParameters = "-t###"+wekaTrainFile+"###-T###"+wekaTestFile+"###-no-cv###-p###0";
        LibSVM svm = new LibSVM();

        System.out.println("LIB SVM Parameters -> ["+argsClassifier+"].");
        System.out.println("Layout  Parameters -> ["+layoutParameters+"].");

        layoutParameters += "###" + argsClassifier.replace(" ","###");

        String result = "";
        try {
            result = svm.execute(layoutParameters.split("###"));
        } catch (Exception ex) {
            Logger.getLogger(SVM.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        if ((result != null)&&(!result.isEmpty()))
            setClasses(testSet,result);

        if (modelOutFilename != null && !modelOutFilename.isEmpty()) {
            libsvm.svm.svm_save_model(modelOutFilename,(svm_model)svm.m_Model);
        }

        //Deleting weka files...
        File f = new File(wekaTrainFile);
        if (f != null) {
            f.delete();
        }
        f = new File(wekaTestFile);
        if (f != null) {
            f.delete();
        }
        return testSet;
        
    }

    public AbstractMatrix run(AbstractMatrix trainingSet, AbstractMatrix testSet, String modelOutFilename) throws IOException {

        libsvm.svm_node[][] x = new libsvm.svm_node[trainingSet.getRowCount()][trainingSet.getDimensions()];
        libsvm.svm_node[] xr = null;
        double[] y = new double[trainingSet.getRowCount()];

        for (int i=0;i<trainingSet.getRowCount();i++) {
            AbstractVector row = trainingSet.getRow(i);
            xr = new libsvm.svm_node[row.size()];
            for (int j=0;j<row.size();j++) {
                xr[j] = new libsvm.svm_node();
                xr[j].index = j;
                xr[j].value = row.getValue(j);
            }
            x[i] = xr;
            y[i] = (int)row.getKlass();
        }

        libsvm.svm_problem p = new libsvm.svm_problem();
        p.l = x.length;
        p.x = x;
        p.y = y;

        long init = System.currentTimeMillis();
        libsvm.svm_model model = libsvm.svm.svm_train(p, parameters);
        long end = System.currentTimeMillis();
        long diff1 = end - init;
        System.out.println("Time spent (SVM training - Model creation) -> " + (diff1/1000.0f) + " seconds");

        if (modelOutFilename != null && !modelOutFilename.isEmpty()) {
            libsvm.svm.svm_save_model(modelOutFilename,model);
        }

        init = System.currentTimeMillis();
        for (int i=0;i<testSet.getRowCount();i++) {
            AbstractVector row = testSet.getRow(i);
            xr = new libsvm.svm_node[row.size()];
            for (int j=0;j<row.size();j++) {
                xr[j] = new libsvm.svm_node();
                xr[j].index = j;
                xr[j].value = row.getValue(j);
            }
            double value = libsvm.svm.svm_predict(model,xr);
            //System.out.println("Value: "+value);
            row.setKlass((float) value);
        }
        end = System.currentTimeMillis();
        long diff2 = end - init;
        System.out.println("Time spent (SVM classification) -> " + (diff2/1000.0f) + " seconds");
        System.out.println("Time spent (Total SVM classification process) -> " + ((diff1+diff2)/1000.0f) + " seconds");
        return testSet;
        
    }

    public AbstractMatrix run(AbstractMatrix testMatrix, String modelInFilename) throws IOException {

        long init = System.currentTimeMillis();
        libsvm.svm_model model = libsvm.svm.svm_load_model(modelInFilename);
        long end = System.currentTimeMillis();
        long diff1 = end - init;
        System.out.println("Time spent (SVM Model loading) -> " + (diff1/1000.0f) + " seconds");
        init = System.currentTimeMillis();
        for (int i=0;i<testMatrix.getRowCount();i++) {
            AbstractVector row = testMatrix.getRow(i);
            libsvm.svm_node[] x = new libsvm.svm_node[row.size()];
            for (int j=0;j<row.size();j++) {
                x[j] = new libsvm.svm_node();
                x[j].index = j;
                x[j].value = row.getValue(j);
            }
            double value = libsvm.svm.svm_predict(model,x);
            //System.out.println("Value: "+value);
            row.setKlass((float) value);
        }
        end = System.currentTimeMillis();
        long diff2 = end - init;
        System.out.println("Time spent (SVM classification) -> " + (diff2/1000.0f) + " seconds");
        System.out.println("Time spent (Total SVM classification process) -> " + ((diff1+diff2)/1000.0f) + " seconds");
        return testMatrix;

    }

//    private boolean isElement(float klass, float[] klasses) {
//        for (int i=0;i<klasses.length;i++)
//            if (klass == klasses[i]) return true;
//        return false;
//    }

//    public AbstractMatrix getTrainingSet() {
//        return trainingSet;
//    }


}
