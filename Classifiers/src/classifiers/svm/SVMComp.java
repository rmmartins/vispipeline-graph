/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package classifiers.svm;

import distance.DistanceMatrix;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo
 */
@VisComponent(hierarchy = "Classification.SVM",
name = "Support Vector Machine",
description = "Classify an image collection using LibSVM from WEKA package.")
public class SVMComp implements AbstractComponent {

    public void execute() throws IOException {
        
        if (testMatrix == null)
            JOptionPane.showMessageDialog(null,"A test set should be informed!");
        else {
            if (modelInFilename != null && !modelInFilename.isEmpty()) {
                SVM svm = new SVM();
                testMatrix = svm.run(testMatrix,modelInFilename);
            }else {
                if (trainingMatrix == null && (trainingSet == null || trainingSet.trim().isEmpty())) {
                    JOptionPane.showMessageDialog(null,"A training set should be informed!");
                    throw new IOException("A training set should be informed!");
                }else {
                    SVM svm = new SVM(svmType,coeff,cost,degree,eps,gamma,kernelType,nu,shrinking);
                    if (trainingMatrix == null)
                        trainingMatrix = MatrixFactory.getInstance(trainingSet);
                    //testMatrix = svm.run(trainingMatrix, testMatrix, argsClassifier, modelOutFilename);
                    testMatrix = svm.run(trainingMatrix, testMatrix, modelOutFilename);
                }
            }
            dmat = new DistanceMatrix(testMatrix,DissimilarityFactory.getInstance(disstype));

            //saving the matrix
            if (testMatrix != null && saveMatrix && matrixFilename != null && !matrixFilename.isEmpty()) {
                //saving the points matrix
                System.out.println("Saving points matrix...");
                testMatrix.save(matrixFilename);
            }

            //saving distance matrix
            if (dmat != null && saveDmat && dmatFilename != null && !dmatFilename.isEmpty()) {
                System.out.println("Saving distance matrix...");
                dmat.save(dmatFilename);
            }
        }
        
    }

    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new SVMParamView(this);
        }
        return paramview;
    }

    public void reset() {
        trainingMatrix = null;
        testMatrix = null;
        dmat = null;
    }
    
    public void input(@Param(name = "test matrix") AbstractMatrix testMatrix) {
        try {
            this.testMatrix = (AbstractMatrix) testMatrix.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(SVMComp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public AbstractMatrix output() {
        return testMatrix;
    }

    public DistanceMatrix outputDmat() {
        return dmat;
    }

    public AbstractMatrix getTrainingMatrix() {
        return trainingMatrix;
    }

    public void setTrainingMatrix(AbstractMatrix trainingMatrix) {
        this.trainingMatrix = trainingMatrix;
    }

    public String getArgsClassifier() {
        return argsClassifier;
    }

    public void setArgsClassifier(String a) {
        this.argsClassifier = a;
    }

    public String getTrainingSet() {
        return trainingSet;
    }

    public void setTrainingSet(String ts) {
        trainingSet = ts;
    }

    public boolean isSaveMatrix() {
        return saveMatrix;
    }

    public void setSaveMatrix(boolean saveMatrix) {
        this.saveMatrix = saveMatrix;
    }

    public boolean isSaveDmat() {
        return saveDmat;
    }

    public void setSaveDmat(boolean saveDmat) {
        this.saveDmat = saveDmat;
    }

    public String getMatrixFilename() {
        return matrixFilename;
    }

    public void setMatrixFilename(String matrixFilename) {
        this.matrixFilename = matrixFilename;
    }

    public String getDmatFilename() {
        return dmatFilename;
    }

    public void setDmatFilename(String dmatFilename) {
        this.dmatFilename = dmatFilename;
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

//    public String getLibSVMParameters() {
//        return libSVMParameters;
//    }
//
//    public void setLibSVMParameters(String libSVMParameters) {
//        this.libSVMParameters = libSVMParameters;
//    }

    public boolean isSaveModel() {
        return saveModel;
    }

    public void setSaveModel(boolean saveModel) {
        this.saveModel = saveModel;
    }

    public String getModelOutFilename() {
        return modelOutFilename;
    }

    public void setModelOutFilename(String f) {
        this.modelOutFilename = f;
    }

    public String getModelInFilename() {
        return modelInFilename;
    }

    public void setModelInFilename(String f) {
        this.modelInFilename = f;
    }

    //SVM parameters...
    public void setSvmType(int selectedIndex) {
        svmType = selectedIndex;
    }

    public void setCoeff(float parseFloat) {
        coeff = parseFloat;
    }

    public void setCost(float parseFloat) {
        cost = parseFloat;
    }

    public void setDegreeKernel(float parseInt) {
        degree = parseInt;
    }

    public void setEps(float parseFloat) {
        eps = parseFloat;
    }

    public void setKernelType(int selectedIndex) {
        kernelType = selectedIndex;
    }

    public void setGamma(float parseFloat) {
        gamma = parseFloat;
    }

    public void setNu(float parseFloat) {
        nu = parseFloat;
    }

    public void setShrinking(boolean selected) {
        shrinking = selected;
    }

    public float getCoeff() {
        return coeff;
    }

    public float getCost() {
        return cost;
    }

    public float getDegreeKernel() {
        return degree;
    }

    public float getEps() {
        return eps;
    }

    public float getGamma() {
        return gamma;
    }

    public float getNu() {
        return nu;
    }

    public boolean isShrinking() {
        return shrinking;
    }

    public int getKernelType() {
        return kernelType;
    }

    public int getSvmType() {
        return svmType;
    }

    public static final long serialVersionUID = 1L;
    private transient SVMParamView paramview;
    private transient AbstractMatrix trainingMatrix;
    private transient AbstractMatrix testMatrix;
    private transient DistanceMatrix dmat;
    private String argsClassifier = "";
    private String trainingSet = "";
    private transient boolean saveMatrix, saveDmat;
    private transient String matrixFilename, dmatFilename;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private String modelInFilename = "", modelOutFilename = "";
    private boolean saveModel = false;
    //SVM parameters...
    private int svmType = 0;
    private float coeff = 0.0f;
    private float cost = 1.0f;
    private float degree = 3.0f;
    private float eps = 0.001f;
    private float gamma = 0.0f;
    private int kernelType = 0;
    private float nu = 0.5f;
    private boolean shrinking = true;

}
