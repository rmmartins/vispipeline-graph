/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network.topics;

import network.input.VNAReaderComp;
import network.input.VNAGraphData;
import network.layout.ConnectivityProjectionLayoutComp;
import graph.model.Connectivities;
import network.model.LabeledNetGraphModel;
import network.model.LabeledNetGraphModelComp;
import network.view.LabeledNetGraphFrame;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import matrix.AbstractMatrix;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import projection.technique.fastmap.FastmapProjection2DComp;
import textprocessing.corpus.zip.ZipCorpus;

/**
 *
 * @author rmartins
 */
public class NetTopicSelectionTest {
    AbstractMatrix matrix;
    VNAGraphData graphData;
    Connectivities conns;
    LabeledNetGraphModel model;

    @Before
    public void setup() throws Exception {
        String input = getClass().getResource("../test2.bib").getPath();
        VNAReaderComp graphReader = new VNAReaderComp();
        graphReader.setVnaFilename(input);
        graphReader.execute();
        graphData = graphReader.outputGraphData();        
        conns = graphReader.outputConnectivities();
        ConnectivityProjectionLayoutComp connLayout = new ConnectivityProjectionLayoutComp();
        connLayout.input(graphData);
        connLayout.getConnectivitiesToConsider().addAll(Arrays.asList(new Integer[]{0, 1, 2, 3}));
        connLayout.execute();
        FastmapProjection2DComp fastmap = new FastmapProjection2DComp();
        fastmap.input(connLayout.outputMatrix());
        fastmap.execute();
        matrix = fastmap.output();
        LabeledNetGraphModelComp modelComp = new LabeledNetGraphModelComp();
        modelComp.input(matrix,  graphData);
        //modelComp.attach(connLayout.outputConnectivity());
//        modelComp.attach(conns);
        modelComp.execute();
        model = modelComp.output();
    }
    
    @Test
    public void testGraphData() throws IOException {
        ZipCorpus corpus = (ZipCorpus) graphData.getCorpus();
        for (int id : corpus.getIds()) {
            System.out.println(id + ": ngrams " + corpus.getNgrams(id));
        }
    }
    
    @Test
    public void testSelection() throws IOException {
        LabeledNetGraphFrame mockFrame = mock(LabeledNetGraphFrame.class);
        when(mockFrame.getModel()).thenReturn(model);
        NetTopicSelection topicSel = new NetTopicSelection(mockFrame);
        topicSel.selected(model.getInstances());
    }
}
