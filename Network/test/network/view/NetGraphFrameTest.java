/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network.view;

import network.input.VNAReaderComp;
import network.input.VNAGraphData;
import network.layout.ConnectivityProjectionLayoutComp;
import graph.model.Connectivities;
import network.model.LabeledNetGraphModel;
import network.model.LabeledNetGraphModelComp;
import matrix.AbstractMatrix;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import projection.technique.fastmap.FastmapProjection2DComp;

/**
 *
 * @author rmartins
 */
public class NetGraphFrameTest {
    
    AbstractMatrix matrix;
    VNAGraphData graphData;
    Connectivities conns;
    LabeledNetGraphModel model;

    @Before
    public void setup() throws Exception {
        String input = getClass().getResource("../test.bib").getPath();
        VNAReaderComp graphReader = new VNAReaderComp();
        graphReader.setVnaFilename(input);
        graphReader.execute();
        graphData = graphReader.outputGraphData();
        conns = graphReader.outputConnectivities();
        ConnectivityProjectionLayoutComp connLayout = new ConnectivityProjectionLayoutComp();
        connLayout.input(graphData);
        connLayout.execute();
        FastmapProjection2DComp fastmap = new FastmapProjection2DComp();
        fastmap.input(connLayout.outputMatrix());
        fastmap.execute();
        matrix = fastmap.output();
        LabeledNetGraphModelComp modelComp = new LabeledNetGraphModelComp();
        modelComp.input(matrix,  graphData);
//        modelComp.attach(conns);
        modelComp.execute();
        model = modelComp.output();
    }

    @Test
    public void testBasicFrameFeatures() throws Exception {
        NetGraphFrameComp comp = new NetGraphFrameComp();
        comp.input(model);
        comp.execute();
        NetGraphFrame frame = comp.frame;
        // Model
        assertNotNull(frame.getModel());
        // Shapes
        ShapeOptions shapeOpts = new ShapeOptions(frame);
        assertNotNull(shapeOpts);
        shapeOpts.display(true);
        shapeOpts.dispose();
        // Color
        assertEquals(model.getAttributes().size() + 2, 
                frame.colorCombo.getItemCount());
        // Size
        assertEquals(model.getAttributes().size() + 2, 
                frame.sizeCombo.getItemCount());
        // Titles
        assertEquals(model.getAttributes().size(), 
                frame.titleCombo.getItemCount());
        // Edges
        assertEquals(model.getConnectivities().size(),
                frame.edgeCombo.getItemCount());
                
        Thread.currentThread().join();
    }
}
