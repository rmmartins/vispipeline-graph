/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network.view;

import network.input.VNAReaderComp;
import network.input.VNAGraphData;
import network.layout.ConnectivityProjectionLayoutComp;
import graph.model.Connectivities;
import graph.view.GraphReportView;
import network.model.LabeledNetGraphModel;
import network.model.LabeledNetGraphModelComp;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Observable;
import matrix.AbstractMatrix;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import projection.technique.fastmap.FastmapProjection2DComp;

/**
 *
 * @author rmartins
 */
public class LabeledNetGraphFrameTest {
    
    AbstractMatrix matrix;
    VNAGraphData graphData;
    Connectivities conns;
    LabeledNetGraphModel model;
    LabeledNetGraphFrame frame;

    @Before
    public void setup() throws Exception {
        String input = getClass().getResource("../test2.bib").getPath();
        VNAReaderComp graphReader = new VNAReaderComp();
        graphReader.setVnaFilename(input);
        graphReader.execute();
        graphData = graphReader.outputGraphData();
        conns = graphReader.outputConnectivities();
        ConnectivityProjectionLayoutComp connLayout = new ConnectivityProjectionLayoutComp();
        connLayout.input(graphData);
        connLayout.getConnectivitiesToConsider().addAll(Arrays.asList(new Integer[]{0, 1, 2, 3}));
        connLayout.execute();
        FastmapProjection2DComp fastmap = new FastmapProjection2DComp();
        fastmap.input(connLayout.outputMatrix());
        fastmap.execute();
        matrix = fastmap.output();
        LabeledNetGraphModelComp modelComp = new LabeledNetGraphModelComp();
        modelComp.input(matrix,  graphData);
//        modelComp.attach(conns);
        modelComp.execute();
        model = modelComp.output();
    }
    
    @Test
    public void testBasicFeatures() throws Exception {        
        LabeledNetGraphFrameComp comp = new LabeledNetGraphFrameComp();
        comp.input(model);
        comp.execute();
        frame = comp.frame;
        // report
        assertEquals(model.getSource(),
                ((GraphReportView)frame.getReportPanel()).getSourceValue());
        assertEquals(model.getGraphData().getUrl(),
                ((GraphReportView)frame.getReportPanel()).getDataSourceValue());        
        assertEquals(String.valueOf(model.getAttributes().size()),
                ((GraphReportView)frame.getReportPanel()).getNumberDimensionsValue());
        assertEquals(String.valueOf(model.getInstances().size()),
                ((GraphReportView)frame.getReportPanel()).getNumberObjectsValue());
                
//        Thread.currentThread().join();
    }
    
    @Test
    public void testObserver() {
        LabeledNetGraphFrame spyFrame = spy(new LabeledNetGraphFrame());
        spyFrame.setModel(model); // 3 calls to update...?
        model.setSelectedInstances(model.getInstances());
        model.cleanSelectedInstances();
        verify(spyFrame, times(5)).update(any(Observable.class), any());
    }
}
