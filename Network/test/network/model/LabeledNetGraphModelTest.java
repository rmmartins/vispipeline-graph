/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network.model;

import network.input.VNAReaderComp;
import network.input.VNAGraphData;
import network.layout.ConnectivityProjectionLayoutComp;
import graph.model.Connectivities;
import graph.model.Connectivity;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import matrix.AbstractMatrix;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import projection.model.Scalar;
import projection.technique.fastmap.FastmapProjection2DComp;
import textprocessing.corpus.zip.ZipCorpus;

/**
 *
 * @author rmartins
 */
public class LabeledNetGraphModelTest {
    
    AbstractMatrix matrix;
    VNAGraphData graphData;
    Connectivities conns;
    LabeledNetGraphModel model;

    @Before
    public void setup() throws Exception {
        String input = getClass().getResource("../test2.bib").getPath();
        VNAReaderComp graphReader = new VNAReaderComp();
        graphReader.setVnaFilename(input);
        graphReader.execute();
        graphData = graphReader.outputGraphData();
        conns = graphReader.outputConnectivities();
        ConnectivityProjectionLayoutComp connLayout = new ConnectivityProjectionLayoutComp();
        connLayout.input(graphData);
        connLayout.getConnectivitiesToConsider().addAll(Arrays.asList(new Integer[]{0, 1, 2, 3}));
        connLayout.execute();
        FastmapProjection2DComp fastmap = new FastmapProjection2DComp();
        fastmap.input(connLayout.outputMatrix());
        fastmap.execute();
        matrix = fastmap.output();
        LabeledNetGraphModelComp modelComp = new LabeledNetGraphModelComp();
        modelComp.input(matrix,  graphData);
        //modelComp.attach(connLayout.outputConnectivity());
//        modelComp.attach(conns);
        modelComp.execute();
        model = modelComp.output();
    }
    
    @Test
    public void testChangeTitles() throws IOException {        
        model.changeTitles("node_type");
        assertEquals("author", ((LabeledNetGraphInstance)model.getInstances()
                .get(0)).getTitle());
        assertEquals("paper", ((LabeledNetGraphInstance)model.getInstances()
                .get(2849)).getTitle());
    }
    
    @Test
    public void testCentralityMeasures() throws Exception {        
        Connectivity conn = model.getConnectivityByName("all");
        model.setSelectedConnectivity(conn);
        model.calculateScalarsFromSelectedConn();
        Scalar scalar = model.getScalarByName("betweeness: all");        
        assertEquals(0.2857143f, ((LabeledNetGraphInstance)model.getInstances()
                .get(1)).getScalarValue(scalar), 0.001f);
    }
    
    @Test
    public void testIdsMatchCorpus() throws Exception {
        ZipCorpus zip = (ZipCorpus) graphData.getCorpus();
        HashMap<Integer, String> corpusIds = new HashMap<Integer, String>();
        HashMap<Integer, String> graphIds = new HashMap<Integer, String>();
        for (int id : zip.getIds()) {
            corpusIds.put(id, zip.idToUrl(id));
            graphIds.put(id, ((LabeledNetGraphInstance)model.getInstanceById(id)).getLabel());
        }
        assertEquals(corpusIds, graphIds);
    }
    
}
