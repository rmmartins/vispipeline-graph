/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network.model;

import network.input.VNAReaderComp;
import network.input.VNAGraphData;
import network.layout.ConnectivityProjectionLayoutComp;
import graph.model.Connectivities;
import matrix.AbstractMatrix;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import projection.technique.fastmap.FastmapProjection2DComp;

/**
 *
 * @author rmartins
 */
public class LabeledNetGraphModelCompTest {

    AbstractMatrix matrix;
    VNAGraphData graphData;
    Connectivities conns;

    @Before
    public void setup() throws Exception {
        String input = getClass().getResource("../test.bib").getPath();
        VNAReaderComp graphReader = new VNAReaderComp();
        graphReader.setVnaFilename(input);
        graphReader.execute();
        graphData = graphReader.outputGraphData();
        conns = graphReader.outputConnectivities();
        ConnectivityProjectionLayoutComp connLayout = new ConnectivityProjectionLayoutComp();
        connLayout.input(graphData);
        connLayout.execute();
        FastmapProjection2DComp fastmap = new FastmapProjection2DComp();
        fastmap.input(connLayout.outputMatrix());
        fastmap.execute();
        matrix = fastmap.output();
    }

    @Test
    public void testModel() throws Exception {
        LabeledNetGraphModelComp modelComp = new LabeledNetGraphModelComp();
        modelComp.input(matrix, graphData);
//        modelComp.attach(conns);
        modelComp.execute();
        LabeledNetGraphModel model = modelComp.output();
        assertEquals(301, model.getInstances().size());
        assertEquals(5, model.getConnectivities().size());
        int[] expecteds = {0, 255, 379, 26, 660};
        int[] actuals = new int[5];
        for (int i = 0; i < model.getConnectivities().size(); i++) {
            actuals[i] = model.getConnectivities().get(i).getEdges().size();
        }
        assertArrayEquals(expecteds, actuals);
    }
    
    @Test
    public void testSetCorpus() throws Exception {
        LabeledNetGraphModelComp modelComp = new LabeledNetGraphModelComp();
        modelComp.input(matrix, graphData);
        modelComp.execute();
        assertNotNull(modelComp.output().getCorpus());
    }
}
