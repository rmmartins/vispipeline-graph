/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network.coordination;

import network.model.LabeledNetGraphModel;
import labeledprojection.model.LabeledProjectionModel;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author rmartins
 */
public class GraphCoordinatorCompTest {
    
    static LabeledNetGraphModel graphModel;
    static LabeledProjectionModel projModel;

    @BeforeClass
    public static void setup() throws Exception {
//        String input = GraphCoordinatorCompTest.class.getResource(
//                "../test.bib").getPath();
//        GraphDataReaderComp graphReader = new GraphDataReaderComp();
//        graphReader.setFilename(input);
//        graphReader.execute();
//        // Graph        
//        ConnectivityProjectionLayoutComp connLayout = 
//                new ConnectivityProjectionLayoutComp();
//        connLayout.input(graphReader.outputGraphData());
//        connLayout.execute();
//        LabeledNetGraphModelComp modelComp = new LabeledNetGraphModelComp();
//        modelComp.input(connLayout.outputMatrix(), connLayout.outputLayoutGraphData());
//        modelComp.attach(connLayout.outputConnectivity());
//        modelComp.execute();
//        graphModel = modelComp.output();
//        // Projection        
//        PreprocessorComp process = new PreprocessorComp();
//        process.setNumberLines(0);
//        process.input(graphReader.outputCorpus());
//        process.execute();
//        FastmapProjection2DComp fastmap = new FastmapProjection2DComp();
//        fastmap.input(process.outputMatrix());
//        fastmap.execute();
//        TopicProjectionModelComp proj = new TopicProjectionModelComp();
//        proj.input(fastmap.output());
//        proj.attach(graphReader.outputCorpus());
//        proj.setCollection(graphReader.outputCorpus().getUrl());
//        proj.execute();
//        projModel = proj.output();
    }

    /**
     * Test of execute method, of class GraphCoordinatorComp.
     */
    @Test
    public void testExecute() throws Exception {
//        GraphCoordinatorComp instance = new GraphCoordinatorComp();
//        instance.execute();
//        GraphCoordinator coord = (GraphCoordinator) instance.output();
//        coord.addModel(graphModel);
//        coord.addModel(projModel);
//        // select random instances
//        ArrayList selins = new ArrayList();
//        Random r = new Random();
//        for (AbstractInstance i : projModel.getInstances()) {
//            if (r.nextBoolean()) {
//                selins.add(i);
//            }
//        }
//        projModel.setSelectedInstances(selins);
//        // coordinate
//        coord.coordinate(projModel.getSelectedInstances(), null);
//        // and check selected instances on both models
//        assertEquals(projModel.getSelectedInstances().toString(), 
//                graphModel.getSelectedInstances().toString());
//        // then clean and coordinate again
//        projModel.cleanSelectedInstances();
//        coord.coordinate(projModel.getSelectedInstances(), null);
//        assertEquals(0, graphModel.getSelectedInstances().size());
    }

}
