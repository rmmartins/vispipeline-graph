/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package network.view;

import network.model.LabeledNetGraphModel;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Network.View",
name = "Net Graph View Frame",
description = "Display a graph model.")
public class NetGraphFrameComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (model != null) {
            frame = new NetGraphFrame();
            frame.setSize(800, 600);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
            frame.setTitle(title);
            frame.setModel(model);

            if (coordinators != null) {
                for (AbstractCoordinator coordinator : coordinators) {
                    frame.addCoordinator(coordinator);
                }
            }
        } else {
            throw new IOException("A projection model should be provided.");
        }
    }

    public void input(@Param(name = "graph model") LabeledNetGraphModel model) {
        this.model = model;
    }

    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
          if (paramview == null) {
            paramview = new NetGraphFrameParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        model = null;
        coordinators = null;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle () {
        return title;
    }

    public static final long serialVersionUID = 1L;
    private String title = "";    
    private transient LabeledNetGraphModel model;
    private transient NetGraphFrameParamView paramview;
    private transient ArrayList<AbstractCoordinator> coordinators;
    transient NetGraphFrame frame;
}
