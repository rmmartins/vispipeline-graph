/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package network.view;

import graph.model.GraphModel;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import labeledgraph.view.LabeledGraphFrameComp;
import labeledgraph.view.LabeledGraphFrameParamView;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Network.View",
name = "Labeled Net View Frame",
description = "Display a graph model.")
public class LabeledNetGraphFrameComp extends LabeledGraphFrameComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
//        System.out.println("****** LABEL GRAPH COMP");        
//        for (int i = 0; i < model.getInstances().size(); i++) {
//            GraphInstance gi = (GraphInstance)model.getInstances().get(i);
//            System.out.printf("model #%d = %f, %f\n", i, gi.getX(), gi.getY());
//        }
        if (model != null) {
            frame = new LabeledNetGraphFrame();
            frame.setSize(900, 600);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
            frame.setTitle(title);
            frame.setModel(model);
            frame.setLocationRelativeTo(null);
//            frame.updateTitlesCombo();

            if (coordinators != null) {
                for (int i = 0; i < coordinators.size(); i++) {
                    frame.addCoordinator(coordinators.get(i));
                }
            }
        } else {
            throw new IOException("A projection model should be provided.");
        }
    }

    @Override
    public void input(@Param(name = "graph model") GraphModel model) {
        this.model = model;
    }

    @Override
    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
          if (paramview == null) {
            paramview = new LabeledGraphFrameParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        model = null;
        coordinators = null;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getTitle () {
        return title;
    }
    
    private String title = "";    
    private transient GraphModel model;
    private transient LabeledGraphFrameParamView paramview;
    private transient ArrayList<AbstractCoordinator> coordinators;
    transient LabeledNetGraphFrame frame;
}
