/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network.view;

import graph.analysis.NeighborhoodPreservationComp;
import graph.input.GraphData;
import network.input.VNAGraphData;
import graph.model.GraphInstance;
import graph.model.GraphModel;
import graph.util.VNAFilter;
import graph.view.GraphReportView;
import graph.view.SaveClassDialog;
import graph.view.interaction.ShowVertexLabel;
import network.model.LabeledNetGraphModel;
import network.view.interaction.AttributesSlider;
import network.view.interaction.ConnectivitySlider;
import network.view.interaction.GraphSelection;
import network.view.interaction.GraphSplitSelection;
import network.view.interaction.NeighborhoodPreservation;
import network.view.interaction.SelectCommonNeighborsSelection;
import network.view.interaction.SelectNeighborsSelection;
import network.view.interaction.ShortestPathBetweenTwoVertices;
import network.view.interaction.ViewContent;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import labeledgraph.model.LabeledGraphModel;
import labeledprojection.view.forms.klassification.SVMClassifyDialog;
import labeledprojection.view.selection.ClassAbstractSelection;
import projection.model.Scalar;
import projection.util.ProjectionConstants;
import vispipelinebasics.interfaces.AbstractParametersView;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.model.AbstractModel;
import visualizationbasics.util.BasicsContants;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.SaveDialog;


/**
 *
 * @author Jose Gustavo, Henry, Rafael
 */
public class LabeledNetGraphFrame extends NetGraphFrame {

    public LabeledNetGraphFrame() {
        super();
        initComponents();
        GraphSelection g = new GraphSelection(this);
        addSelection(g, true);
        this.view.setSelection(g);
        addSelection(new SelectNeighborsSelection(this), false);
        addSelection(new SelectCommonNeighborsSelection(this), false);
        addSelection(new GraphSplitSelection(this), false);
        addSelection(new ShowVertexLabel(this), false);
        addSelection(new ShortestPathBetweenTwoVertices(this), false);
        addSelection(new ViewContent(this), false);
        addSelection(new NeighborhoodPreservation(this), false);
        //addSelection(new NeighborhoodPreservation(this), false);
        addShowLabelsWithinTheVerticesButton();
        addExportVNA();
        addNumericAttributeSlider();
        addConnectivitySlider();
        //addCountCrossingEdgesButton();

//        addSelection(new GraphViewContentSelection(this),false);
//        addSelection(new GraphDivideSelection(this),false);
//        addSelection(new GraphSplitSelection(this),false);
//        addSelection(new GraphRearrangeSelection(this),false);
//        addSelection(new GraphClassSelection(this),false);



    }

    public void addSelection(final ClassAbstractSelection selection, boolean state) {
        if (selection != null) {
            JToggleButton button = new JToggleButton();
            selectionButtonGroup.add(button);
            button.setIcon(selection.getIcon());
            button.setSelected(false);
            button.setToolTipText(selection.toString());
            button.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (getView() != null) {
                        getView().setSelection(selection);
                        selection.reset();
                    }
                }
            });
            button.setSelected(state);
            selectionToolBar.add(button);
        }
    }

//    protected void colorComboMouseClicked(java.awt.event.MouseEvent evt) {
//        if (evt.getClickCount() == 2) {
//            Scalar scalar = (Scalar) this.colorCombo.getSelectedItem();
//
//            if (!scalar.getName().equals(ProjectionConstants.DOTS)) {
//                scalarComboModel.removeElement(scalar);
//                sizeScalarComboModel.removeElement(scalar);
//
//                colorCombo.setSelectedIndex(0);
//                sizeCombo.setSelectedIndex(0);
//
//                ((LabeledGraphModel) model).removeScalar(scalar);
//                model.notifyObservers();
//            }
//        }
//    }

    protected void sizeComboMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            Scalar scalar = (Scalar) this.sizeCombo.getSelectedItem();

            if (!scalar.getName().equals(ProjectionConstants.DOTS)) {
                scalarComboModel.removeElement(scalar);
                sizeScalarComboModel.removeElement(scalar);

                colorCombo.setSelectedIndex(0);
                sizeCombo.setSelectedIndex(0);

                ((LabeledGraphModel) model).removeScalar(scalar);
                model.notifyObservers();
            }
        }
    }

    private void saveColorButtonActionPerformed(java.awt.event.ActionEvent evt) {
        Scalar currentScalar = (Scalar) this.colorCombo.getSelectedItem();
        if (currentScalar != null) {
            SaveClassDialog.getInstance(this, ((LabeledGraphModel) model).getValidInstances(),
                    ((GraphReportView) getReportPanel()).getSourceValue()).display(currentScalar);
        }
    }

    private void svmMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        SVMClassifyDialog svmd = SVMClassifyDialog.getInstance(this, model);
        svmd.display();
        if (svmd.getNumberScalars() > 0) {
            for (int i = 0; i < svmd.getNumberScalars(); i++) {
                this.updateScalars(svmd.getNscalar(i));
            }
            model.notifyObservers();
        }
    }

    private void npMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            NeighborhoodPreservationComp comp = new NeighborhoodPreservationComp();
            comp.attach((GraphModel) model);
            comp.setUseVisEuclidianDistance(true);
            final JDialog frame = new JDialog((JFrame) null, true);
            frame.setLayout(new BorderLayout());
            final AbstractParametersView param = comp.getParametersEditor();
            frame.add(param);
            JButton ok = new JButton("Ok");
            ok.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        param.finished();
                        frame.dispose();
                    } catch (IOException ex) {
                        Logger.getLogger(LabeledNetGraphFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            JPanel south = new JPanel();
            south.setLayout(new FlowLayout(FlowLayout.RIGHT));
            south.add(ok);
            frame.add(south, BorderLayout.SOUTH);
            frame.pack();
            frame.setVisible(true);
//            JOptionPane.showMessageDialog(null, "dmat #: " + comp.getDmat().getElementCount());
            comp.execute();

//            String source = ((GraphReportView)getReportPanel()).getSourceValue();
//            if (source == null || source.isEmpty() || !source.endsWith(".data")) {
//                PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
//                int result = OpenDialog.showOpenDialog(spm, new DMATFilter(), this);
//                if (result == JFileChooser.APPROVE_OPTION) {
//                    source = OpenDialog.getFilename();
//                    comp.setDataFileName(filename);
//                    comp.setDataFileType(0);
//                    String message = JOptionPane.showInputDialog("Inform a label to the Stress Curve Graphic (optional):");
//                    comp.setGraphicLabel(message);
//                    comp.execute();
//                }
//            }
//            comp.setDataFileName(source);
//            comp.setDataFileType(1); //distance matrix
//            String message = JOptionPane.showInputDialog("Inform a label to the Stress Curve Graphic (optional):");
//            if (message != null) {
//                comp.setGraphicLabel(message);
//                comp.execute();
//            }
        } catch (IOException ex) {
            Logger.getLogger(LabeledNetGraphFrame.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    private void stressMenuItemActionPerformed(java.awt.event.ActionEvent evt) {

//        try {
//            StressComp comp = new StressComp();
//            comp.input((GraphModel) model);
//            String source = ((GraphReportView) getReportPanel()).getSourceValue();
//            if (source == null || source.isEmpty() || !source.endsWith(".data")) {
//                PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
//                int result = OpenDialog.showOpenDialog(spm, new DATAFilter(), this);
//                if (result == JFileChooser.APPROVE_OPTION) {
//                    source = OpenDialog.getFilename();
//                } else if (result == JFileChooser.CANCEL_OPTION) {
//                    return;
//                }
//            }
//            comp.setDataFileName(source);
//            comp.setDataFileType(0);
//            ChecksDialog.getInstance(this).display(comp);
//
//
////            String message = JOptionPane.showInputDialog("Inform a label to the Stress Curve Graphic (optional):");
////            if (message != null) {
////                comp.setGraphicLabel(message);
////                int option = JOptionPane.showOptionDialog(this,"Use Euclidean Distances?","",JOptionPane.YES_NO_OPTION,JOptionPane.INFORMATION_MESSAGE,null,null,null);
////                if (option == 0) comp.setUseVisEuclidianDistance(true);
////                else comp.setUseVisEuclidianDistance(false);
////                comp.setUseEuclideanAsWeights(false);
////                comp.setUseWeight(false);
////                comp.execute();
////            }
//        } catch (IOException ex) {
//            Logger.getLogger(LabeledNetGraphFrame.class.getName()).log(Level.SEVERE, null, ex);
//        }

        // José Gustavo:
//        try {
//            StressCurveComp comp = new StressCurveComp();
//            comp.input((GraphModel) model);
//            String source = ((GraphReportView) getReportPanel()).getSourceValue();
//            if (source == null || source.isEmpty() || !source.endsWith(".data")) {
//                PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
//                int result = OpenDialog.showOpenDialog(spm, new DMATFilter(), this);
//                if (result == JFileChooser.APPROVE_OPTION) {
//                    source = OpenDialog.getFilename();
//                    comp.setDataFileName(filename);
//                    comp.setDataFileType(0);
//                    String message = JOptionPane.showInputDialog("Inform a label to the Stress Curve Graphic (optional):");
//                    comp.setGraphicLabel(message);
//                    comp.execute();
//                }
//            }
//            comp.setDataFileName(source);
//            comp.setDataFileType(1); //distance matrix
//            String message = JOptionPane.showInputDialog("Inform a label to the Stress Curve Graphic (optional):");
//            if (message != null) {
//                comp.setGraphicLabel(message);
//                comp.execute();
//            }
//        } catch (IOException ex) {
//            Logger.getLogger(LabeledNetGraphFrame.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

//    private void createKmeansLabelsButtonActionPerformed(java.awt.event.ActionEvent evt) {
//        try {
//            KmeansClusters labels = new KmeansClusters((LabeledProjectionModel) getModel());
//            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
//            Scalar kmeansScalar = labels.execute(this);
//            if (kmeansScalar != null) {
//                updateScalars(kmeansScalar);
//            }
//            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
//        } catch (IOException ex) {
//            Logger.getLogger(LabeledNetGraphFrame.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }

    private void initComponents() {

        graphTabbedPane = new javax.swing.JTabbedPane();
        reportPanel = new GraphReportView();

        dataPanel.remove(scrollPanel);

        graphTabbedPane.addTab("Graph", scrollPanel);
        graphTabbedPane.addTab("Report", reportPanel);

        dataPanel.add(graphTabbedPane, java.awt.BorderLayout.CENTER);

        saveColorButton = new javax.swing.JButton();
        saveColorButton.setText("Save");
        saveColorButton.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveColorButtonActionPerformed(evt);
            }
        });
        scalarPanel.add(saveColorButton);

        classifyMenu = new javax.swing.JMenu();
        svmMenuItem = new javax.swing.JMenuItem();

        classifyMenu.setText("Classifiying");

        svmMenuItem.setText("Support Vector Machine");
        svmMenuItem.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                svmMenuItemActionPerformed(evt);
            }
        });
        classifyMenu.add(svmMenuItem);
        menuTool.add(classifyMenu);

        stressMenuItem = new javax.swing.JMenuItem();
        stressMenuItem.setText("Stress Curve");
        stressMenuItem.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stressMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(stressMenuItem);

        JMenuItem npMenuItem = new javax.swing.JMenuItem();
        npMenuItem.setText("Neighborhood Preservation");
        npMenuItem.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                npMenuItemActionPerformed(evt);
            }
        });
        menuTool.add(npMenuItem);

//        createKmeansLabelsButton = new javax.swing.JButton();
//        createKmeansLabelsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/About16.gif"))); // NOI18N
//        createKmeansLabelsButton.setToolTipText("Group instances using Kmeans");
//        createKmeansLabelsButton.setFocusable(false);
//        createKmeansLabelsButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
//        createKmeansLabelsButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
//        createKmeansLabelsButton.addActionListener(new java.awt.event.ActionListener() {
//            @Override
//            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                createKmeansLabelsButtonActionPerformed(evt);
//            }
//        });
//        selectionToolBar.add(createKmeansLabelsButton);


        pack();
    }

    public javax.swing.JPanel getReportPanel() {
        return reportPanel;
    }

//    public void setDrawAs(Shape d) {
//        ArrayList<AbstractInstance> instances = getModel().getInstances();
//        if ((instances != null) && (!instances.isEmpty())) {
//            for (int i = 0; i < instances.size(); i++) {
//                if (instances.get(i) instanceof LabeledGraphInstance) {
//                    ((LabeledNetGraphInstance) instances.get(i)).setDrawAsShape(d);
//                }
//            }
//        }
//        model.setChanged();
//        model.notifyObservers();
//    }

    private void addShowLabelsWithinTheVerticesButton() {
        final JToggleButton button = new JToggleButton();


        ClassLoader cldr = this.getClass().getClassLoader();
        java.net.URL imageURL = cldr.getResource("graph/icons/showLabelsWithinVertices.gif");
        button.setIcon(new ImageIcon(imageURL));
        button.setSelected(false);
        button.setToolTipText("Show Labels within the Vertices");
        button.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                ((LabeledGraphModel) model).setDrawAsLabels(!((LabeledGraphModel) model).isDrawAsLabels());
//                button.setSelected(((LabeledGraphModel) model).isDrawAsLabels());
//                model.setChanged();
//                model.notifyObservers();
            }
        });
        //button.setSelected(false);
        fixedToolBar.add(button);
    }

    private void addExportVNA() {
        JMenuItem item = new JMenuItem("Export VNA");
        this.exportMenu.add(item);
        final LabeledNetGraphFrame thisparent = this;
        item.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                ArrayList<Integer> ids = new ArrayList<>();
                ArrayList<AbstractInstance> instances = model.getInstances();
                for (AbstractInstance ins : instances) {
                    if (((GraphInstance) ins).isEnabled()) {
                        ids.add(ins.getId());
                    }
                }
                try {
                    
                PropertiesManager spm = PropertiesManager.getInstance(BasicsContants.PROPFILENAME);
                    int result = SaveDialog.showSaveDialog(spm, new VNAFilter(), thisparent);
                    String filename = SaveDialog.getFilename();
                    if (!filename.toLowerCase().endsWith(".vna")) {
                        filename += ".vna";
                    }

                    ((VNAGraphData) ((LabeledGraphModel) model).getGraphData()).export(ids, filename);
                } catch (IOException ex) {
                    Logger.getLogger(LabeledNetGraphFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    private void addNumericAttributeSlider() {
        JMenuItem item = new JMenuItem("Numeric Attribute Filter");
        this.menuTool.add(item);
        final LabeledNetGraphFrame thisparent = this;
        item.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
//                if (attributesSlider == null) {
//                    try {
//                        attributesSlider = new AttributesSlider((LabeledGraphModel) model);
//                    } catch (Exception ex) {
//                        Logger.getLogger(LabeledNetGraphFrame.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                } else {
//                    //attributesSlider.setVisible(true);
//                }
            }
        });
    }

    private void addConnectivitySlider() {
        JMenuItem item = new JMenuItem("Connectivity Filter");
        this.menuTool.add(item);
        final LabeledNetGraphFrame thisparent = this;
        item.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    connSlider = new ConnectivitySlider((LabeledGraphModel) model);
                } catch (Exception ex) {
                    Logger.getLogger(LabeledNetGraphFrame.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
    }

    @Override
    public void setModel(AbstractModel model) {        
        LabeledNetGraphModel m = (LabeledNetGraphModel) model;
        GraphData gdata = m.getGraphData();
        ((GraphReportView)reportPanel).setDataSource(gdata.getUrl());
        ((GraphReportView)reportPanel).setSource(m.getSource());
        ((GraphReportView)reportPanel).setDimensions(m.getAttributes().size());
        ((GraphReportView)reportPanel).setObjects(m.getInstances().size());
        super.setModel(m);
    }
    
    private ConnectivitySlider connSlider = null;
    private AttributesSlider attributesSlider = null;
    private GraphModel originalModel;
    private javax.swing.JButton saveColorButton;
    private javax.swing.JMenu classifyMenu;
    private javax.swing.JMenuItem svmMenuItem;
    private javax.swing.JMenuItem stressMenuItem;
    private javax.swing.JButton createKmeansLabelsButton;
    private javax.swing.JTabbedPane graphTabbedPane;
    private javax.swing.JPanel reportPanel;
}
