package network.view.interaction;

import datamining.neighbors.Pair;
import graph.model.Connectivity;
import graph.model.Edge;
import graph.model.GraphInstance;
import graph.model.GraphModel;
import graph.util.Dijkstra;
import graph.view.interaction.GraphAbstractSelection;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.ImageIcon;
import labeledgraph.model.LabeledGraphModel;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Henry Heberle
 */
public class ShortestPathBetweenTwoVertices extends GraphAbstractSelection {

    ArrayList<AbstractInstance> currentPath = null;
    GraphInstance source = null;
    GraphInstance target = null;
    Connectivity currentConn = null;
    ArrayList<AbstractInstance> instances = null;
    private Dijkstra dijkstra;

    public ShortestPathBetweenTwoVertices(ModelViewer viewer) {
        super(viewer);
        this.currentPath = new ArrayList<AbstractInstance>();
    }

    @Override
    public void selected(int x, int y) {
    }

    @Override
    public void released(GraphInstance instance) {
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        float[] s;
        ArrayList<Integer> p;
        AbstractInstance ins;
        ((LabeledGraphModel) viewer.getModel()).updateAllConnectivities();
        LabeledGraphModel model = (LabeledGraphModel) viewer.getModel();


        for (AbstractInstance i : model.getInstances()) {
            ((GraphInstance) i).setEnabled(true);
            model.setChanged();
            model.notifyObservers();
        }

        //            if (selinst != null && selinst.size() == 1) {
//                if (!((GraphModel) viewer.getModel()).getSelsconn().equals(currentConn)) {
//                    clear();
//                }
        if (selinst != null && selinst.size() == 1) {

            if (this.source == null) {
                this.source = (GraphInstance) selinst.get(0);
                if (!this.source.isEnabled()) {
                    viewer.getModel().cleanSelectedInstances();
                    clear();
                    for (int i = 0; i < viewer.getCoordinators().size(); i++) {
                        viewer.getCoordinators().get(i).coordinate(new ArrayList<AbstractInstance>(), null);
                    }
                    viewer.getModel().setChanged();
                    viewer.getModel().notifyObservers();
                    return;
                }
                this.currentConn = ((GraphModel) viewer.getModel()).getSelectedConnectivity();
                // this.dijkstra = new Dijkstra(currentPath, null, Integer.SIZE, found);   
            } else {
                if (!((GraphModel) viewer.getModel()).getSelectedConnectivity().equals(this.currentConn)) {
                    viewer.getModel().cleanSelectedInstances();
                    clear();
                    for (int i = 0; i < viewer.getCoordinators().size(); i++) {
                        viewer.getCoordinators().get(i).coordinate(new ArrayList<AbstractInstance>(), null);
                    }
                    viewer.getModel().setChanged();
                    viewer.getModel().notifyObservers();
                } else {

                    if (this.target == null) {
                        this.target = (GraphInstance) selinst.get(0);
                        if (!this.target.isEnabled()) {
                            viewer.getModel().cleanSelectedInstances();
                            clear();
                            for (int i = 0; i < viewer.getCoordinators().size(); i++) {
                                viewer.getCoordinators().get(i).coordinate(new ArrayList<AbstractInstance>(), null);
                            }
                            viewer.getModel().setChanged();
                            viewer.getModel().notifyObservers();
                            return;
                        }
                        this.instances = ((GraphModel) viewer.getModel()).getInstances();

                        //System.out.println("Instances size: "+ instances.size());
                        HashMap<Integer, Integer> inversemap = new HashMap<Integer, Integer>(instances.size());
                        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>(instances.size());
                        HashMap<Integer, AbstractInstance> instancemap = new HashMap<Integer, AbstractInstance>(instances.size());


//                        ArrayList<AbstractInstance> instancesToConsider = new ArrayList<AbstractInstance>();
//                        ArrayList<Edge> edgesToConsider = new ArrayList<Edge>();
//                        ArrayList<Integer> ids = new ArrayList<Integer>();
//                        for (AbstractInstance ai : instances) {
////                            if(((GraphInstance)ai).isEnabled()){
////                                instancesToConsider.add(ai);
//                            ids.add(ai.getId());
//                        }
//                        }
//                        this.instances = instancesToConsider;
//                        
//                        for(Edge e : this.currentConn.getEdges()){
//                            if (ids.contains(e.getTargetID()) && ids.contains(e.getSourceID())){
//                                edgesToConsider.add(e);
//                            }
//                        }


                        for (int i = 0; i < instances.size(); i++) {
                            ins = instances.get(i);
                            map.put(ins.getId(), i);
                            inversemap.put(i, ins.getId());
                            instancemap.put(ins.getId(), ins);
                        }

                        ArrayList<ArrayList<Pair>> neigh_aux = new ArrayList<ArrayList<Pair>>(instances.size());

                        for (int i = 0; i < instances.size(); i++) {
                            neigh_aux.add(new ArrayList<Pair>());
                        }

                        //System.out.println("Edges size: " + this.currentConn.getEdges().size());
                        //   for (Edge edge : edgesToConsider) {
                        for (Edge edge : this.currentConn.getEdges()) {
                            //if (ids.contains(edge.getSource()) && ids.contains(edge.getTarget())) {
                            neigh_aux.get(map.get(edge.getSource())).add(new Pair(map.get(edge.getTarget()), 1));
                            neigh_aux.get(map.get(edge.getTarget())).add(new Pair(map.get(edge.getSource()), 1));
                            //}
                        }


                        Pair[][] neighborhood = new Pair[instances.size()][];

                        for (int i = 0; i < neigh_aux.size(); i++) {
                            neighborhood[i] = new Pair[neigh_aux.get(i).size()];

                            for (int j = 0; j < neigh_aux.get(i).size(); j++) {
                                neighborhood[i][j] = neigh_aux.get(i).get(j);
                            }
                        }

                        Dijkstra d = new Dijkstra(neighborhood);

                        System.out.println(this.source);
                        System.out.println(this.target);
//                        System.out.println(this.target.getId());
//                        System.out.println(map.get(this.target.getId()));
//                        System.out.println(inversemap.get(this.target.getId()));
                        s = d.execute(map.get(this.source.getId()));


                        if (s != null) {
                            System.out.println("Menor distância calculada: " + s[map.get(this.target.getId())]);
                        }

                        p = d.getPath(map.get(this.target.getId()));

                        System.out.println("Menor distância desenhada: " + (p.size() - 1));

                        this.currentPath.clear();
                        for (int i = 0; i < p.size(); i++) {
                            this.currentPath.add(instancemap.get(inversemap.get(p.get(i))));
                        }

                        if (this.currentPath.isEmpty() || this.currentPath.size() == 1) {
                            viewer.getModel().cleanSelectedInstances();
                            clear();
                            for (int i = 0; i < viewer.getCoordinators().size(); i++) {
                                viewer.getCoordinators().get(i).coordinate(new ArrayList<AbstractInstance>(), null);
                            }
                            viewer.getModel().setChanged();
                            viewer.getModel().notifyObservers();
                        } else {
                            for (int i = 0; i < viewer.getCoordinators().size(); i++) {
                                viewer.getCoordinators().get(i).coordinate(currentPath, null);
                            }
                            viewer.getModel().cleanSelectedInstances();
                            viewer.getModel().setSelectedInstances(currentPath);
                            viewer.getModel().setChanged();
                            viewer.getModel().notifyObservers();
                            clear();
                        }
                    }
                }
            }
        }//se mais de uma instancia selecionada
        else {
            viewer.getModel().cleanSelectedInstances();
            clear();
            for (int i = 0; i < viewer.getCoordinators().size(); i++) {
                viewer.getCoordinators().get(i).coordinate(new ArrayList<AbstractInstance>(), null);
            }
            viewer.getModel().setChanged();
            viewer.getModel().notifyObservers();
        }
    }

    @Override
    public ImageIcon getIcon() {
        ClassLoader cldr = this.getClass().getClassLoader();
        java.net.URL imageURL = cldr.getResource("graph/icons/shortestPath.gif");
        return new ImageIcon(imageURL);
    }

    @Override
    public String toString() {
        return "Show a shortest path. Select two vertex, one at a time.";
    }

    private void clear() {
        this.currentPath.clear();
        this.source = null;
        this.target = null;
        this.currentConn = null;
    }
}