package network.view.interaction;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import graph.model.GraphInstance;
import graph.view.interaction.GraphAbstractSelection;
import network.view.NetGraphFrame;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Henry Heberle
 */
public class GraphSelection extends GraphAbstractSelection {

    public GraphSelection(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(int x, int y) {
    }

    public void selected(ArrayList<AbstractInstance> selinst) {
        AbstractInstance inst;
        if (viewer.getModel() != null) {
            if (selinst.size() == 1) {
                // selinst = selectHierarchy((GraphInstance)selinst.get(0));
            }
            viewer.getModel().setSelectedInstances(selinst);
            if (viewer instanceof NetGraphFrame) {
                if (selinst != null && !selinst.isEmpty()) {
                    ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
                    for (int i = 0; i < selinst.size(); i++) {
                        inst = selinst.get(i);
                        if (((GraphInstance) inst).isEnabled()) {
                            instances.add(inst);
                        }
                    }
                    ((NetGraphFrame) viewer).getModel().setSelectedInstances(instances);
                    //AbstractInstance inst = instances.get(0);
                    //((NetGraphFrame) viewer).getModel().centerVertex(inst);
                }
            }
            for (int i = 0; i < viewer.getCoordinators().size(); i++) {
                viewer.getCoordinators().get(i).coordinate(selinst, null);
            }
            viewer.getModel().notifyObservers();
        }
    }

    @Override
    public void released(GraphInstance instance) {
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/AlignRight16.gif"));
    }

    @Override
    public String toString() {
        return "Select Graph";
    }
}
