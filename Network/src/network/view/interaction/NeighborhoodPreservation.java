/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network.view.interaction;

import graph.model.Connectivity;
import graph.model.GraphInstance;
import graph.model.GraphModel;
import graph.view.interaction.GraphAbstractSelection;
import network.model.LabeledNetGraphModel;
import network.view.LabeledNetGraphFrame;
import network.view.NetGraphFrame;
import static network.view.NetGraphFrame.neighbor_cache_nd;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import projection.model.Scalar;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author rmmartins@gmail.com
 */
public class NeighborhoodPreservation extends GraphAbstractSelection {

    public NeighborhoodPreservation(ModelViewer viewer) {
        super(viewer);        
    }

    @Override
    public void selected(int x, int y) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void released(GraphInstance instance) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        
        if (model == null && viewer.getModel() != null) {
            model = (LabeledNetGraphModel)viewer.getModel();
            scalar = model.addScalar("NP");
            ((LabeledNetGraphFrame)viewer).updateScalars(scalar);
        }
        
        // compute (and cache) a shortest-path matrix for the selected connectivity        
        if (model != null) {
            Connectivity con = ((GraphModel) model).getSelectedConnectivity();
            if (NetGraphFrame.neighbor_cache_nd.containsKey(con)) {
                
                int n = model.getInstances().size();
                
                ArrayList<AbstractInstance> new_sel = new ArrayList<>();                

                // first clear the scalar
                scalar.reset();
                for (AbstractInstance inst : model.getInstances()) {
                    ((GraphInstance) inst).setScalarValue(scalar, 0.0f);
                }

                // now update the selection and the scalar
                new_sel.add(selinst.get(0));
                int sel_index = model.getInstances().indexOf(selinst.get(0));
                int k = ((NetGraphFrame) viewer).getK();
                boolean keep_going = false;
                for (int i = 0; i < k || keep_going; ++i) {
                    int i2d = NetGraphFrame.neighbor_cache_2d[sel_index][i].index;
                    int ind = NetGraphFrame.neighbor_cache_nd.get(con)[sel_index][i].index;
                    // KNN will return index = -1 if there are no more neighbors
                    if (ind == -1) {
                        break;
                    }                    
                    GraphInstance inst = (GraphInstance) model.getInstances().get(i2d);
                    float val = inst.getScalarValue(scalar);
                    // false neighbors are 1
                    inst.setScalarValue(scalar, val + 1);
                    if (!new_sel.contains(inst)) new_sel.add(inst);
                    inst = (GraphInstance) model.getInstances().get(ind);
                    val = inst.getScalarValue(scalar);
                    // missing neighbors are 2
                    inst.setScalarValue(scalar, val + 2);
                    if (!new_sel.contains(inst)) new_sel.add(inst);
                    // and true neighbors are 3
                    
                    if (i < (n - 2))
                        keep_going = (neighbor_cache_nd.get(con)[sel_index][i].value == neighbor_cache_nd.get(con)[sel_index][i+1].value);
                    else
                        keep_going = false;
                }
                
                // this has to be done independent of the results, to ensure
                // a consistent color coding
                scalar.setMin(0.0f);
                scalar.setMax(3.0f);

                Scalar setdiff = model.getScalarByName("NP: Set Diff.");                
                for (AbstractInstance inst : new_sel) {
                    int index = model.getInstances().indexOf(inst);                    
                }

                // this has to be done to update instances' colors
                model.setSelectedScalar(scalar);                
                
                model.setSelectedInstances(new_sel);
                model.notifyObservers();
            }
        }
    }

    @Override
    public ImageIcon getIcon() {
        //throw new UnsupportedOperationException("Not supported yet.");
        ClassLoader cldr = this.getClass().getClassLoader();
        java.net.URL imageURL = cldr.getResource("graph/icons/selectNeighbors16x16.gif");
        return new ImageIcon(imageURL);
    }

    @Override
    public String toString() {
        return "Neighborhood Preservation";
    }
    
    Scalar scalar;
    LabeledNetGraphModel model;
    Connectivity currentCon;
    
    
}
