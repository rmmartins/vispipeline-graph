package network.view.interaction;

import graph.model.Connectivity;
import graph.model.Edge;
import graph.model.GraphInstance;
import graph.model.GraphModel;
import graph.view.interaction.GraphAbstractSelection;
import network.view.NetGraphFrame;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Henry Heberle
 */
public class SelectNeighborsSelection extends GraphAbstractSelection {

    public SelectNeighborsSelection(ModelViewer viewer) {
        super(viewer);
    }

    public Edge getEdgeByPosition(Connectivity con, int x, int y) {

        ArrayList<Edge> edges;

        if (con != null) {
            edges = con.getEdges();
            if (edges != null) {
                for (Edge e : edges) {
                    if (isInsideEdge(e, x, y)) {
                        return e;
                    }
                }
            }
        }
        return null;
    }

    public boolean isInsideEdge(Edge e, int x, int y) {
        float xs, ys, xt, yt;
        GraphInstance ti;
        GraphModel gm = (GraphModel) this.viewer.getModel();
        ti = e.getSourceInst();
        if (ti != null) {
            xs = ti.getX();
            ys = ti.getY();
        } else {
            return false;
        }
        ti = e.getTargetInst();
        if (ti != null) {
            xt = ti.getX();
            yt = ti.getY();
        } else {
            return false;
        }

        if (xs > xt) {
            if ((x < (int) xt) || (x > (int) xs)) {
                return false;
            }
        } else {
            if ((x < (int) xs) || (x > (int) xt)) {
                return false;
            }
        }

        if (ys > yt) {
            if ((y < (int) yt) || (y > (int) ys)) {
                return false;
            }
        } else {
            if ((y < (int) ys) || (y > (int) yt)) {
                return false;
            }
        }

        float a = ((yt - ys) / (xt - xs));
        float b = ys - (a * xs);
        float yr = ((a * x) + b);
        //System.out.println("y: "+y+", r: "+((a*x)+b)+", yy: "+(int)(yr)+".");
        return (Math.abs(y - (int) yr) <= 3);

        //return ((y == Math.round(yr))||(y == (int)yr));
    }

    @Override
    public void selected(int x, int y) {
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (viewer.getModel() != null) {
            ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
            instances.addAll(selinst);
            ArrayList<AbstractInstance> neighbors = new ArrayList<AbstractInstance>();
            for (AbstractInstance selinst1 : selinst) {
                addNeighbors(neighbors, (AbstractInstance) selinst1);
            }

            instances.addAll(neighbors);

            for (int i = 0; i < viewer.getCoordinators().size(); i++) {
                viewer.getCoordinators().get(i).coordinate(instances, null);
            }

            viewer.getModel().setSelectedInstances(instances);
            viewer.getModel().notifyObservers();
        }
    }

    @Override
    public ImageIcon getIcon() {
        ClassLoader cldr = this.getClass().getClassLoader();
        java.net.URL imageURL = cldr.getResource("graph/icons/selectNeighbors16x16.gif");
        return new ImageIcon(imageURL);
    }

    @Override
    public String toString() {
        return "Select Neighbors";
    }

    @Override
    public void released(GraphInstance instance) {
    }

    private void addNeighbors(ArrayList<AbstractInstance> neighbors, AbstractInstance abstractInstance) {
        //Connectivity conn = viewer
        GraphModel model = (GraphModel) viewer.getModel();
        NetGraphFrame frame = (NetGraphFrame) viewer;
        Connectivity currentConn = frame.getCurrentConnectivity();
        int id = abstractInstance.getId();

        for (Edge e : currentConn.getEdges()) {
            if (e.getTarget() == id && !neighbors.contains(e.getSource())) {
                neighbors.add(e.getSourceInst());
            } else {
                if (e.getSource() == id && !neighbors.contains(e.getTarget())) {
                    neighbors.add(e.getTargetInst());
                }
            }
        }
    }
}
