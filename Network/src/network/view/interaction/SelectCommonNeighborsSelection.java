package network.view.interaction;

import graph.model.Connectivity;
import graph.model.Edge;
import graph.model.GraphInstance;
import graph.model.GraphModel;
import graph.view.interaction.GraphAbstractSelection;
import network.view.NetGraphFrame;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Henry Heberle
 */
public class SelectCommonNeighborsSelection extends GraphAbstractSelection {

    public SelectCommonNeighborsSelection(ModelViewer viewer) {
        super(viewer);
    }

    public Edge getEdgeByPosition(Connectivity con, int x, int y) {

        ArrayList<Edge> edges = new ArrayList<Edge>();

        if (con != null) {
            edges = con.getEdges();
            if (edges != null) {
                for (Edge e : edges) {
                    if (isInsideEdge(e, x, y)) {
                        return e;
                    }
                }
            }
        }
        return null;
    }

    public boolean isInsideEdge(Edge e, int x, int y) {
        float xs, ys, xt, yt;
        GraphInstance ti;
        GraphModel gm = (GraphModel) this.viewer.getModel();
        ti = (GraphInstance) gm.getInstanceById(e.getSource());
        if (ti != null) {
            xs = ti.getX();
            ys = ti.getY();
        } else {
            return false;
        }
        ti = (GraphInstance) gm.getInstanceById(e.getTarget());
        if (ti != null) {
            xt = ti.getX();
            yt = ti.getY();
        } else {
            return false;
        }

        if (xs > xt) {
            if ((x < (int) xt) || (x > (int) xs)) {
                return false;
            }
        } else {
            if ((x < (int) xs) || (x > (int) xt)) {
                return false;
            }
        }

        if (ys > yt) {
            if ((y < (int) yt) || (y > (int) ys)) {
                return false;
            }
        } else {
            if ((y < (int) ys) || (y > (int) yt)) {
                return false;
            }
        }

        float a = ((yt - ys) / (xt - xs));
        float b = ys - (a * xs);
        float yr = ((a * x) + b);
        //System.out.println("y: "+y+", r: "+((a*x)+b)+", yy: "+(int)(yr)+".");
        return (Math.abs(y - (int) yr) <= 3);

        //return ((y == Math.round(yr))||(y == (int)yr));
    }

    @Override
    public void selected(int x, int y) {
    }

    
    public synchronized void  selected(ArrayList<AbstractInstance> selinst) {
        ArrayList<AbstractInstance> nexNeighbors;
        if (viewer.getModel() != null) {
            ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
            instances.addAll(selinst);

            ArrayList<AbstractInstance> neighbors = getNeighbors((AbstractInstance) selinst.get(0));
            ArrayList<AbstractInstance> neighborsAux = (ArrayList<AbstractInstance>) neighbors.clone();


            if (neighbors != null && !neighbors.isEmpty()) {
                for (int i = 1; i < selinst.size(); i++) {
                    nexNeighbors = getNeighbors((AbstractInstance) selinst.get(i));
                    for (AbstractInstance inst : neighbors){
                        if(!nexNeighbors.contains(inst)){
                            neighborsAux.remove(inst);
                        }
                    }
                }
            }

            instances.addAll(neighborsAux);

            for (int i = 0; i < viewer.getCoordinators().size(); i++) {
                viewer.getCoordinators().get(i).coordinate(instances, null);
            }

            viewer.getModel().setSelectedInstances(instances);
            viewer.getModel().notifyObservers();
        }
    }

    @Override
    public ImageIcon getIcon() {
        ClassLoader cldr = this.getClass().getClassLoader();
        java.net.URL imageURL   = cldr.getResource("graph/icons/selectCommonNeighbor16x16.gif");
        return new ImageIcon(imageURL);     
    }

    @Override
    public String toString() {
        return "Select Common Neighbors";
    }

    @Override
    public void released(GraphInstance instance) {
    }

    private ArrayList<AbstractInstance> getNeighbors(AbstractInstance abstractInstance) {
        GraphModel model = (GraphModel) viewer.getModel();
        NetGraphFrame frame = (NetGraphFrame) viewer;
        Connectivity currentConn = frame.getCurrentConnectivity();
        int id = abstractInstance.getId();
        ArrayList<AbstractInstance> neighbors = new ArrayList<AbstractInstance>();

        for (Edge e : currentConn.getEdges()) {
            if (e.getTarget() == id && !neighbors.contains(e.getSource())) {
                neighbors.add(e.getSourceInst());
            } else {
                if (e.getSource() == id && !neighbors.contains(e.getTarget())) {
                    neighbors.add(e.getTargetInst());
                }
            }
        }
        return neighbors;
    }
}
