package network.view.interaction;

import graph.input.GraphData;
import graph.input.GraphData.Attribute;
import graph.model.GraphInstance;
import graph.view.interaction.GraphAbstractSelection;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import labeledgraph.model.LabeledGraphModel;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Henry Heberle
 */
public class ViewContent extends GraphAbstractSelection {

    public ViewContent(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(int x, int y) {
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {

        if (viewer.getModel() != null) {

            final JScrollPane jp = new JScrollPane();
            final JFrame jf = new JFrame();
            final JTable table = new JTable() {
                @Override
                public boolean isCellEditable(int rowIndex, int colIndex) {
                    return false;
                }
            };

            jf.setMinimumSize(new Dimension(500, 400));
            jp.setMinimumSize(new Dimension(480, 380));

            jf.getContentPane().setLayout(new BorderLayout());
            jf.getContentPane().add(jp);
            jp.setViewportView(table);

            jf.setVisible(false);
            GraphData gdata = ((LabeledGraphModel) viewer.getModel()).getGraphData();
            ArrayList<Attribute> attributes = gdata.getAttributes();
            ArrayList<ArrayList<String>> matrix = gdata.getMatrix();
            ArrayList<ArrayList<String>> dataArrayList = new ArrayList<ArrayList<String>>();
            final HashMap<String, Integer> idHash = new HashMap<String, Integer>();
            for (AbstractInstance ai : selinst) {
                dataArrayList.add(matrix.get(ai.getId()));
                idHash.put(matrix.get(ai.getId()).get(0), ai.getId());
            }

            String data[][] = new String[selinst.size()][attributes.size()];
            String col[] = new String[attributes.size()];
            for (int i = 0; i < dataArrayList.get(0).size(); i++) {
                col[i] = attributes.get(i).getName();
            }

            for (int i = 0; i < dataArrayList.size(); i++) {
                for (int j = 0; j < dataArrayList.get(0).size(); j++) {
                    data[i][j] = dataArrayList.get(i).get(j);
                }
            }


            final DefaultTableModel dtm = new DefaultTableModel(data, col);

            final ArrayList<AbstractInstance> selectedInstances = new ArrayList<AbstractInstance>();

            table.getSelectionModel().addListSelectionListener(
                    new ListSelectionListener() {

                        public void valueChanged(ListSelectionEvent lse) {
                            int[] selectedRows = table.getSelectedRows();
                            selectedInstances.clear();
                            for (int i = 0; i < selectedRows.length; i++) {
                                String id = (String) dtm.getValueAt(selectedRows[i], 0);
                                selectedInstances.add(((LabeledGraphModel) viewer.getModel()).getInstanceById(idHash.get(id)));
                            }
                            ((LabeledGraphModel) viewer.getModel()).setSelectedInstances(selectedInstances);
                            ((LabeledGraphModel) viewer.getModel()).setChanged();
                            ((LabeledGraphModel) viewer.getModel()).notifyObservers();
                        }
                    });



            table.setModel(dtm);
            table.setDragEnabled(false);
            JTableHeader header = table.getTableHeader();
            header.setBackground(Color.yellow);

            jf.setVisible(true);
            viewer.getModel().setChanged();
            viewer.getModel().notifyObservers();

        }
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Copy16.gif"));
    }

    @Override
    public String toString() {
        return "Show Attributes Values of Selected Vertices";
    }

    @Override
    public void released(GraphInstance instance) {
    }
}
