package network.view.interaction;

import network.input.VNAGraphData;
import graph.model.Connectivity;
import graph.model.Edge;
import graph.model.GraphInstance;
import graph.model.GraphModel;
import graph.util.VNAFilter;
import graph.view.interaction.GraphAbstractSelection;
import network.model.LabeledNetGraphInstance;
import network.model.LabeledNetGraphModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import labeledgraph.model.LabeledGraphInstance;
import labeledgraph.model.LabeledGraphModel;
import projection.model.Scalar;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.util.BasicsContants;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.SaveDialog;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Henry Heberle
 */
public class GraphSplitSelection extends GraphAbstractSelection {

    public GraphSplitSelection(ModelViewer viewer) {
        super(viewer);
    }

    private boolean isInInstances(ArrayList<AbstractInstance> instances, int id) {
        for (int i = 0; i < instances.size(); i++) {
            if (instances.get(i).getId() == id) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void selected(int x, int y) {
    }

    private GraphModel getNewModel(LabeledGraphModel model, ArrayList<AbstractInstance> instances) throws CloneNotSupportedException {

        LabeledNetGraphModel newModel = new LabeledNetGraphModel();
        
        // just a cache
        List<String> labels = new ArrayList<>();
        for (AbstractInstance inst : instances) {
            labels.add(((LabeledNetGraphInstance)inst).getTitle());
        }
        
        // of course we need to have an adapted graphdata
        VNAGraphData new_gdata = ((VNAGraphData) model.getGraphData()).clone();
        
        Iterator<ArrayList<String>> iter = new_gdata.getMatrix().iterator();
        while (iter.hasNext()) {
            ArrayList<String> tuple = iter.next();
            if (!labels.contains(tuple.get(0)))
                iter.remove();
        }
        iter = new_gdata.getRelationshipMatrix().iterator();
        while (iter.hasNext()) {
            ArrayList<String> tuple = iter.next();
            if (!labels.contains(tuple.get(0)) || !labels.contains(tuple.get(1)))
                iter.remove();
        }
        
        newModel.setGraphData(new_gdata);
        
        for (int i = 0; i < instances.size(); i++) {
            if (((LabeledGraphInstance) instances.get(i)).isEnabled()) {
                LabeledNetGraphInstance gi = ((LabeledNetGraphInstance) instances.get(i)).createClone(newModel);
                gi.setSelected(false);
                gi.setModel(newModel);
            }
        }
                
        for (Scalar scalar : model.getScalars()) {
            
            // new scalars must be added in the same order as the originals
            Scalar newScalar = newModel.addScalar(scalar.getName());
            
            // the cloned instances already have the scalar values; as long as
            // the new scalars follow the exact same order as the originals
            for (AbstractInstance inst : newModel.getInstances())
                newScalar.store(((GraphInstance)inst).getScalarValue(scalar));
        }
        newModel.setSelectedScalar(model.getSelectedScalar());

        Connectivity conn = model.getSelectedConnectivity();
        ArrayList<Edge> newEdges = new ArrayList<>();

        if (conn != null) {
            ArrayList<Edge> edges = conn.getEdges();
            if (edges != null) {
                for (int i = 0; i < edges.size(); i++) {
                    if ((isInInstances(newModel.getInstances(), edges.get(i).getSource()))
                            && (isInInstances(newModel.getInstances(), edges.get(i).getTarget()))) {
                        newEdges.add(new Edge(edges.get(i).getSource(), edges.get(i).getTarget(), edges.get(i).getWeight()));
                    }
                }
            }
            Connectivity newCon = new Connectivity(conn.getName(), newEdges);
            newCon.setShowArrow(conn.isShowArrow());
            
            newModel.addConnectivity(newCon);
            newModel.setSelectedConnectivity(newCon);
        }

//        ArrayList<Connectivity> conns = newModel.getConnectivities();
//        Connectivity c;
//        ArrayList<Edge> edges;
//        pos = -1;
//        for (int i = 0; i < model.getConnectivities().size(); i++) {
//            if (!model.getConnectivities().get(i).equals(conn)) {
//                edges = new ArrayList<>();
//                for (int j = 0; j < model.getConnectivities().get(i).getEdges().size(); j++) {
//                    if ((isInInstances(newModel.getInstances(), model.getConnectivities().get(i).getEdges().get(j).getSource()))
//                            && (isInInstances(newModel.getInstances(), model.getConnectivities().get(i).getEdges().get(j).getTarget()))) {
//                        edges.add(new Edge(model.getConnectivities().get(i).getEdges().get(j).getSource(),
//                                model.getConnectivities().get(i).getEdges().get(j).getTarget(),
//                                model.getConnectivities().get(i).getEdges().get(j).getWeight()));
//                    }
//                }
//                c = new Connectivity(model.getConnectivities().get(i).getName(), edges);
//                conns.add(c);
//            } else {
//                pos = i;
//            }
//        }
//        if (pos != -1) {
//            if (pos == conns.size()) {
//                conns.add(newCon);
//            } else {
//                conns.add(pos, newCon);
//            }
//        }


//        newModel.setConnectivities(conns);

        
        newModel.cleanSelectedInstances();
        
        newModel.setInstanceSize(model.getInstanceSize());
        newModel.updateAllConnectivities();
        
        newModel.setAlpha(model.getAlpha());

        newModel.setColortable(model.getColorTable());
        
        return newModel;
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (viewer.getModel() != null) {
            
            LabeledNetGraphModel netmodel = (LabeledNetGraphModel)viewer.getModel();
            
//            VNAGraphData new_gdata = ((VNAGraphData) netmodel.getGraphData()).clone();
//            Iterator<ArrayList<String>> iter = new_gdata.getMatrix().iterator();
//            while (iter.hasNext()) {
//                ArrayList<String> tuple = iter.next();
//                if (!labels.contains(tuple.get(0))) {
//                    iter.remove();
//                }
//            }
//            iter = new_gdata.getRelationshipMatrix().iterator();
//            while (iter.hasNext()) {
//                ArrayList<String> tuple = iter.next();
//                if (!labels.contains(tuple.get(0)) || !labels.contains(tuple.get(1))) {
//                    iter.remove();
//                }
//            }
            

            try {
                ArrayList<Integer> ids = new ArrayList<>();
                for (AbstractInstance inst : selinst) {
                    ids.add(((LabeledGraphInstance) inst).getId());
                }

                PropertiesManager spm = PropertiesManager.getInstance(BasicsContants.PROPFILENAME);
                int result = SaveDialog.showSaveDialog(spm, new VNAFilter(), null);
                String filename = SaveDialog.getFilename();
                if (!filename.toLowerCase().endsWith(".vna")) {
                    filename += ".vna";
                }

                ((VNAGraphData) netmodel.getGraphData()).export(ids, filename);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Couldn't save VNA!");
            }
           

            
            
            
            //Criando um novo modelo, apenas com os vertices selecionados...
//            GraphModel newModel = null;
//            try {
//                newModel = getNewModel((LabeledGraphModel) viewer.getModel(), selinst);
//            } catch (CloneNotSupportedException ex) {
//                Logger.getLogger(GraphSplitSelection.class.getName()).log(Level.SEVERE, null, ex);
//            }

            // Criando um novo frame com o novo modelo...
            
//            LabeledNetGraphFrame frame = new LabeledNetGraphFrame();
//            frame.setSize(600, 600);
//            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//            frame.setTitle(((NetGraphFrame)viewer).getTitle() + " (split)");
//            frame.setModel(newModel);
//            frame.updateTitlesCombo();

//            if (viewer.getCoordinators() != null) {
//                for (int i = 0; i < viewer.getCoordinators().size(); i++) {
//                    frame.addCoordinator(viewer.getCoordinators().get(i));
//                }
//            }
//            frame.setVisible(true);
//            selinst.clear();
            // }
//            else {
//                NetGraphFrame graphFrame = new NetGraphFrame();
//                graphFrame.setSize(600, 600);
//                graphFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//                graphFrame.setVisible(true);
//                graphFrame.setTitle(viewer.getTitle() + " (split)");
//                graphFrame.setModel(newModel);
//                if (viewer.getCoordinators() != null) {
//                    for (int i = 0; i < viewer.getCoordinators().size(); i++) {
//                        graphFrame.addCoordinator(viewer.getCoordinators().get(i));
//                    }
//                }
//                selinst.clear();
//            }
        }
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Cut16.gif"));
    }

    @Override
    public String toString() {
        return "Split Graph";
    }

    @Override
    public void released(GraphInstance instance) {
    }
}
