/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package network.coordination;

import java.io.IOException;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Henry Heberle
 */
@VisComponent(hierarchy = "Coordination",
name = "Graph Coordination",
description = "Create an identity coordination between graphs and/or projections.")
public class GraphCoordinatorComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        coord = new GraphCoordinator();
    }

    public AbstractCoordinator output() {
        return coord;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        coord = null;
    }

    public static final long serialVersionUID = 1L;
    private transient GraphCoordinator coord;
}
