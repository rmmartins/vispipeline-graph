/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network.coordination;

import graph.input.GraphData;
import java.util.ArrayList;
import java.util.HashSet;
import labeledgraph.model.LabeledGraphModel;
import labeledprojection.model.LabeledProjectionModel;
import textprocessing.corpus.zip.ZipCorpus;
import visualizationbasics.coordination.AbstractCoordinator;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.model.AbstractModel;

/**
 *
 * @author Henry Heberle, Rafael Martins
 */
public class GraphCoordinator extends AbstractCoordinator {

    @Override
    public void coordinate(ArrayList<AbstractInstance> selins, Object parameter) {
        AbstractModel sourceModel;

        if (selins.size() > 0) {
            sourceModel = selins.get(0).getModel();
            
            // creating an index to speed-up the coordination process            
            HashSet<String> selids = new HashSet<String>();
            for (AbstractInstance sel : selins) {
                String id = "";
                // model-specific behavior (could be in a subclass)
                if (sourceModel instanceof LabeledGraphModel) {
                    id = ((LabeledGraphModel)sourceModel).getGraphData()
                            .getId(sel.getId());                    
                } else if (sourceModel instanceof LabeledProjectionModel) {
                    id = ((ZipCorpus)((LabeledProjectionModel)sourceModel).getCorpus()).
                            idToUrl(sel.getId());
                }
                selids.add(id);
            }

            String id = null;
            AbstractInstance ai = null;
            for (AbstractModel targetModel : models) {
                // don't coordinate with itself
                if (targetModel != sourceModel) {
                    ArrayList<AbstractInstance> toCoord = new ArrayList<AbstractInstance>();
                    ArrayList<AbstractInstance> allTargetInstances = targetModel.getInstances();
                    for (int i = 0; i < allTargetInstances.size(); i++) {
                        ai = allTargetInstances.get(i);
                        // model-specific behavior (could be in a subclass)
                        if (targetModel instanceof LabeledGraphModel) {
                            GraphData gdata = ((LabeledGraphModel) targetModel).getGraphData();
                            id = gdata.getId(allTargetInstances.get(i).getId());
                        } else if (targetModel instanceof LabeledProjectionModel) {
                            id = ((ZipCorpus)((LabeledProjectionModel)targetModel).getCorpus()).
                                    idToUrl(ai.getId());
                        }
                        if (selids.contains(id)) {
                            toCoord.add(ai);
                        }
                    }
                    targetModel.setSelectedInstances(toCoord);
                    targetModel.notifyObservers();
                }
            }
        } else {
            for (AbstractModel am : models) {
                am.cleanSelectedInstances();
            }
        }
    }
}
