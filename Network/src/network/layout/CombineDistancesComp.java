/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2012-2016 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of VisPipeline-Graph (a branch of VisPipeline).
 *
 * How to cite this work:
 *  
@article{Martins2012,
  title={Multidimensional projections for visual analysis of social networks},
  author={Martins, Rafael Messias and Andery, Gabriel Faria and Heberle, Henry and Paulovich, 
        Fernando Vieira and de Andrade Lopes, Alneu and Pedrini, Helio and Minghim, Rosane},
  journal={Journal of Computer Science and Technology},
  volume={27},
  number={4},
  pages={791--810},
  year={2012},
  publisher={Springer US}
}
 *  
 * VisPipeline-Graph is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * VisPipeline-Graph is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Visualization, Imaging and Computer Graphcs Group 
 * (http://vicg.icmc.usp.br) at Instituto de Ciencias Matematicas e de Computacao - ICMC - 
 * (http://www.icmc.usp.br) of Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial 
 * developer of VisPipeline was Fernando Vieira Paulovich <fpaulovich@gmail.com>. VisPipeline-Graph
 * is a branch of VisPipeline developed initially by Gabriel Faria Andery <gfandery@gmail.com>,
 * Henry Heberle <henryhbrl@gmail.com> and Rafael Messias Martins <rmmartins@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with VisPipeline-Graph. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package network.layout;

import distance.DistanceMatrix;
import java.io.IOException;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Rafael Messias Martins
 */
@VisComponent(hierarchy = "Network.Layout",
        name = "Combine Distances",
        description = "...")
public class CombineDistancesComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (attribDmat.getElementCount() != connDmat.getElementCount()) {
            throw new IOException();
        }
        int size = attribDmat.getElementCount();
        outputDmat = new DistanceMatrix(size);
        // Do I actually need to do this?
        for (int i = 0; i < size; ++i) {            
            outputDmat.setDistance(i, i, 0.0f);
        }
        // first merge attribDmat (normalized)
        float min = attribDmat.getMinDistance();
        float max = attribDmat.getMaxDistance();
        for (int i = 0; i < size - 1; ++i) {
            for (int j = i + 1; j < size; ++j) {
                outputDmat.setDistance(i, j, ((attribDmat.getDistance(i, j) - min) / (max - min)));
            }
        }
        // then merge connDmat (normalized)
        min = connDmat.getMinDistance();
        max = connDmat.getMaxDistance();
        for (int i = 0; i < size - 1; ++i) {
            for (int j = i + 1; j < size; ++j) {
                outputDmat.setDistance(i, j, outputDmat.getDistance(i, j) + 
                        ((connDmat.getDistance(i, j) - min) / (max - min)));
            }
        }
    }

    public void input(@Param(name = "Attribute-based") DistanceMatrix attribDmat, 
            @Param(name = "Connectivity-based") DistanceMatrix connDmat) {
        this.attribDmat = attribDmat;
        this.connDmat = connDmat;
    }
    
    public DistanceMatrix outputDistanceMatrix() {
        return outputDmat;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        attribDmat = null;
        connDmat = null;
        outputDmat = null;
    }

    private transient DistanceMatrix attribDmat;
    private transient DistanceMatrix connDmat;
    private transient DistanceMatrix outputDmat;

    public static final long serialVersionUID = 1L;
}
