/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2012-2016 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of VisPipeline-Graph (a branch of VisPipeline).
 *
 * How to cite this work:
 *  
@article{Martins2012,
  title={Multidimensional projections for visual analysis of social networks},
  author={Martins, Rafael Messias and Andery, Gabriel Faria and Heberle, Henry and Paulovich, 
        Fernando Vieira and de Andrade Lopes, Alneu and Pedrini, Helio and Minghim, Rosane},
  journal={Journal of Computer Science and Technology},
  volume={27},
  number={4},
  pages={791--810},
  year={2012},
  publisher={Springer US}
}
 *  
 * VisPipeline-Graph is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * VisPipeline-Graph is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Visualization, Imaging and Computer Graphcs Group 
 * (http://vicg.icmc.usp.br) at Instituto de Ciencias Matematicas e de Computacao - ICMC - 
 * (http://www.icmc.usp.br) of Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial 
 * developer of VisPipeline was Fernando Vieira Paulovich <fpaulovich@gmail.com>. VisPipeline-Graph
 * is a branch of VisPipeline developed initially by Gabriel Faria Andery <gfandery@gmail.com>,
 * Henry Heberle <henryhbrl@gmail.com> and Rafael Messias Martins <rmmartins@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with VisPipeline-Graph. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package network.layout;

import distance.DistanceMatrix;
import graph.input.GraphData;
import network.input.VNAGraphData;
import graph.util.FloydWarshallShortestPath;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Henry Heberle, Rafael Martins
 */
@VisComponent(hierarchy = "Network.Layout",
name = "Connectivity-based Distances",
description = "Creates a distance (or adjacency) matrix based on the graph's connectivity.")
public class ConnectivityProjectionLayoutComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (graph != null) {
            ids = new ArrayList<>(graph.getKeys());
            ArrayList<ArrayList<String>> relMatrix = graph.getRelationshipMatrix();

            // Creates an empty adjacency/distance matrix where all distances are maximum
            float[][] dist = new float[ids.size()][ids.size()];
            for (int i = 0; i < ids.size(); i++) {
                for (int j = 0; j < ids.size(); j++) {
                    if (i == j) {
                        dist[i][j] = 0.0f;
                    } else {
                        dist[i][j] = Float.MAX_VALUE;
                    }
                }
            }

            // Now fill the adjacency matrix with the correct distances
            for (ArrayList<String> edge : relMatrix) {
                // combine all user-chosen connectivities into one single weight
                Float weight = 0.0f;
                if (connectivitiesToConsider.isEmpty()) {
                    System.err.println("No connectivity was chosen; using the first one.");
                    connectivitiesToConsider.add(0);
                }                
                for (Integer p : connectivitiesToConsider) {
                    if (ignoreWeights)
                        weight += (Float.parseFloat(edge.get(p + 2)) == 0.0f ? 0.0f : 1.0f);
                    else
                        weight += Float.parseFloat(edge.get(p + 2));
                }
                
                // if final weight is 0 then there's no edge to process
                if (weight == 0.0f) {
                    continue;
                }

                // get from and to vertices ids
                Integer from = ids.indexOf(edge.get(0));
                Integer to = ids.indexOf(edge.get(1));
                
                if (weightAsStrength) {
                    dist[from][to] = 1.0f / weight;
                } else {
                    dist[from][to] = weight;
                }
                dist[to][from] = dist[from][to];
            }

            // turns the adjacency matrix into a shortest-path matrix
            if (!asAdjacency) {
                FloydWarshallShortestPath.fw(dist);
            }
            
            float max = Float.MIN_VALUE;

            // Float.MAX_VALUE's represent pairs of vertices without connection
            // so ignore them (there won't be a connection between them anyway,
            // even after inverting [min, max] scale)
            for (int i = 0; i < ids.size(); i++) {
                for (int j = 0; j < ids.size(); j++) {
                    if (dist[i][j] < Float.MAX_VALUE && dist[i][j] > max) {
                        max = dist[i][j];
                    }
                }
            }

            for (float[] row : dist) {
                for (int j = 0; j < dist.length; j++) {
                    if (row[j] == Float.MAX_VALUE) {
                        row[j] = 2 * max + 1;
                        //row[j] = 2 * max;
                    }
                }
            }
            
            // Always generates both; use whichever you want
            generateDistanceMatrix(dist);
            generatePointsMatrix(dist);
        } else {
            throw new IOException("A VNA or a BIB should be provided.");
        }        
    }
    
    private void generateDistanceMatrix(float d[][]) {
        dmat = new DistanceMatrix(d.length);        
        
        for (int i = 0; i < d.length; i++) {
            for (int j = i + 1; j < d.length; j++) {
                dmat.setDistance(i, j, d[i][j]);
            }
        }
        
        // there's no need to set ids because the constructor created them as
        // a sequence of integers
        dmat.setLabels(new ArrayList<>(graph.getKeys()));
    }

    private void generatePointsMatrix(float d[][]) {
        matrix = new DenseMatrix();

        for (int i = 0; i < d.length; i++) {
            matrix.addRow(new DenseVector(d[i], i), ids.get(i));
        }
        
        matrix.setAttributes(ids);
    }

    public void input(@Param(name = "Graph Data") GraphData gdata) throws CloneNotSupportedException {
        this.graph = ((VNAGraphData)gdata).clone();
    }

    public AbstractMatrix outputMatrix() {
        return matrix;
    }

    public DistanceMatrix outputDmat() {
        return dmat;
    }
    
    @Override
    public AbstractParametersView getParametersEditor() {        
//        if (this.paramview == null) {            
//            try {
//                this.paramview = new ConnectivityProjectionParamView(this);
//            } catch (IOException ex) {
//                Logger.getLogger(ConnectivityProjectionLayoutComp.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        return this.paramview;

        // Trying this new approach, let's see if it works
        return new ConnectivityProjectionParamView(this);
    }

    @Override
    public void reset() {
        matrix = null;
        graph = null;
        //conn = null;
        dmat = null;
        ids = null;
    }

    
            
    // non-transient:
    @Getter @Setter private boolean asAdjacency = false;
    @Getter @Setter private boolean weightAsStrength = true;
    @Getter @Setter private boolean ignoreWeights = false;
    @Getter private final Set<Integer> connectivitiesToConsider = new HashSet<>();
    
    private transient AbstractMatrix matrix;
    @Getter private transient GraphData graph;
    //private transient ConnectivityProjectionParamView paramview = null;
    private transient DistanceMatrix dmat;
    private transient ArrayList<String> ids;
    
    public static final long serialVersionUID = 1L;
}
