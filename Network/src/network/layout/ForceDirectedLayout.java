/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network.layout;

import network.view.NetGraphFrame;
import labeledgraph.forcelayout.ForceDirectLayout;
import labeledgraph.model.LabeledGraphModel;

/**
 *
 * @author rmartins
 */
public class ForceDirectedLayout extends ForceDirectLayout {    
    
    private NetGraphFrame frame;

    public ForceDirectedLayout(LabeledGraphModel model, NetGraphFrame frame) {
        super(model, null);
        this.frame = frame;
    }

    @Override
    protected void updateImage() {
        frame.updateImage();
    }
    
}
