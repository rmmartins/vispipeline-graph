/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2012-2016 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of VisPipeline-Graph (a branch of VisPipeline).
 *
 * How to cite this work:
 *  
@article{Martins2012,
  title={Multidimensional projections for visual analysis of social networks},
  author={Martins, Rafael Messias and Andery, Gabriel Faria and Heberle, Henry and Paulovich, 
        Fernando Vieira and de Andrade Lopes, Alneu and Pedrini, Helio and Minghim, Rosane},
  journal={Journal of Computer Science and Technology},
  volume={27},
  number={4},
  pages={791--810},
  year={2012},
  publisher={Springer US}
}
 *  
 * VisPipeline-Graph is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * VisPipeline-Graph is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Visualization, Imaging and Computer Graphcs Group 
 * (http://vicg.icmc.usp.br) at Instituto de Ciencias Matematicas e de Computacao - ICMC - 
 * (http://www.icmc.usp.br) of Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial 
 * developer of VisPipeline was Fernando Vieira Paulovich <fpaulovich@gmail.com>. VisPipeline-Graph
 * is a branch of VisPipeline developed initially by Gabriel Faria Andery <gfandery@gmail.com>,
 * Henry Heberle <henryhbrl@gmail.com> and Rafael Messias Martins <rmmartins@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with VisPipeline-Graph. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package network.layout;

import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Rafael Messias Martins
 */
public final class DimensionBasedDistancesParamView extends AbstractParametersView {

    /** Creates new form SimilarityMatrixCreationParamView
     * @param comp */
    public DimensionBasedDistancesParamView(DimensionBasedDistancesComp comp) {
        initComponents();

        this.comp = comp;

        for (DissimilarityType disstype : DissimilarityType.values()) {
            this.dissimilarityComboBox.addItem(disstype);
        }

        reset();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        parametersPanel = new javax.swing.JPanel();
        dissimilarityPanel = new javax.swing.JPanel();
        dissimilarityComboBox = new javax.swing.JComboBox();

        setLayout(new java.awt.BorderLayout());

        parametersPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Similarity Matrix Parameters"));
        parametersPanel.setLayout(new java.awt.GridBagLayout());

        dissimilarityPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Dissimilarity"));
        dissimilarityPanel.setLayout(new java.awt.BorderLayout());
        dissimilarityPanel.add(dissimilarityComboBox, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        parametersPanel.add(dissimilarityPanel, gridBagConstraints);

        add(parametersPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void reset() {        
        dissimilarityComboBox.setSelectedItem(comp.getDissimilarityType());
    }

    @Override
    public void finished() throws IOException {        
        comp.setDissimilarityType((DissimilarityType) dissimilarityComboBox.getSelectedItem());
        
    }

    private final DimensionBasedDistancesComp comp;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox dissimilarityComboBox;
    private javax.swing.JPanel dissimilarityPanel;
    private javax.swing.JPanel parametersPanel;
    // End of variables declaration//GEN-END:variables
}
