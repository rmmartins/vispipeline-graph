/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2012-2016 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of VisPipeline-Graph (a branch of VisPipeline).
 *
 * How to cite this work:
 *  
@article{Martins2012,
  title={Multidimensional projections for visual analysis of social networks},
  author={Martins, Rafael Messias and Andery, Gabriel Faria and Heberle, Henry and Paulovich, 
        Fernando Vieira and de Andrade Lopes, Alneu and Pedrini, Helio and Minghim, Rosane},
  journal={Journal of Computer Science and Technology},
  volume={27},
  number={4},
  pages={791--810},
  year={2012},
  publisher={Springer US}
}
 *  
 * VisPipeline-Graph is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * VisPipeline-Graph is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Visualization, Imaging and Computer Graphcs Group 
 * (http://vicg.icmc.usp.br) at Instituto de Ciencias Matematicas e de Computacao - ICMC - 
 * (http://www.icmc.usp.br) of Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial 
 * developer of VisPipeline was Fernando Vieira Paulovich <fpaulovich@gmail.com>. VisPipeline-Graph
 * is a branch of VisPipeline developed initially by Gabriel Faria Andery <gfandery@gmail.com>,
 * Henry Heberle <henryhbrl@gmail.com> and Rafael Messias Martins <rmmartins@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with VisPipeline-Graph. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package network.layout;

import distance.DistanceMatrix;
import distance.dissimilarity.StringDistance;
import graph.input.GraphData;
import graph.input.GraphData.Attribute;
import java.io.IOException;
import java.text.ParseException;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import lombok.Getter;
import lombok.Setter;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Henry Heberle, Rafael Martins
 */
@VisComponent(hierarchy = "Network.Layout",
        name = "Per Attribute Distances",
        description = "Creates a unified distance matrix by combining each attribute.")
public class AttributesProjectionLayoutComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (gdata != null) {
            DistanceMatrix dm = new DistanceMatrix(gdata.getMatrix().size());

            for (int i = 0; i < gdata.getMatrix().size(); i++) {
                for (int j = i + 1; j < gdata.getMatrix().size(); j++) {
                    dm.setDistance(i, j, 0);
                }
            }
            
            // By default, consider all attributes (except "id")
            if (attributesToConsider.isEmpty()) {
                for (int i = 0; i < gdata.getAttributes().size() - 1; ++i) {
                    attributesToConsider.add(i);
                }
            }

            // for each attribute (except id), calculates distances
            for (int attPos = 1; attPos < gdata.getAttributes().size(); attPos++) {
                Attribute current_att = gdata.getAttributes().get(attPos);
                
                if (!attributesToConsider.contains(attPos - 1)) {
                    System.out.println("Desconsiderou o atributo " + current_att.getName().toLowerCase());
                    continue;
                }

                DistanceMatrix dmAux = new DistanceMatrix(gdata.getMatrix().size());
                
                // verifies attribute type                
                switch (current_att.getType()) {
                    // if numerical, calculates the difference between each pair
                    case "numerical":
                        for (int i = 0; i < gdata.getMatrix().size() - 1; i++) {                            
                            String from = gdata.getMatrix().get(i).get(attPos);
                            for (int j = i + 1; j < gdata.getMatrix().size(); j++) {
                                String to = gdata.getMatrix().get(j).get(attPos);
                                if (!"[NA]".equals(from) && !"[NA]".equals(to)) {
                                    dmAux.setDistance(i, j, Math.abs(Float.valueOf(to) - Float.valueOf(from)));
                                }
                            }
                        }
                        break;
                    // if date, calculates the difference (in time) between each pair                
                    case "date":
                        for (int i = 0; i < gdata.getMatrix().size() - 1; i++) {
                            try {
                                long from = current_att.getDateFormat().parse(gdata.getMatrix().get(i).get(attPos)).getTime();
                                for (int j = i + 1; j < gdata.getMatrix().size(); j++) {
                                    long to = current_att.getDateFormat().parse(gdata.getMatrix().get(j).get(attPos)).getTime();
                                    dmAux.setDistance(i, j, Math.abs(to - from));
                                }
                            } catch (ParseException ex) {
                                // Either the date format is wrong, or found an "[NA]"; do nothing.
                                Logger.getLogger(AttributesProjectionLayoutComp.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        break;
                    // if string, calculates distance using Levenshtein distance
                    default:
                        for (int i = 0; i < gdata.getMatrix().size() - 1; i++) {
                            for (int j = i + 1; j < gdata.getMatrix().size(); j++) {
                                dmAux.setDistance(i, j, StringDistance.getLevenshteinDistance(
                                        gdata.getMatrix().get(i).get(attPos), 
                                        gdata.getMatrix().get(j).get(attPos)));
                            }
                        }
                        break;
                }

                if (dmAux.getMinDistance() != dmAux.getMaxDistance()) {                    
                    // normalizes and adds to distance matrix
                    int weight1 = current_att.getWeight();
                    int weight2 = current_att.getValues().size();
                    for (int i = 0; i < gdata.getMatrix().size() - 1; i++) {
                        for (int j = i + 1; j < gdata.getMatrix().size(); j++) {
                            float norm_dist = ((dmAux.getDistance(i, j) - dmAux.getMinDistance())
                                    / (dmAux.getMaxDistance() - dmAux.getMinDistance()));                            
                            if (equalizeAttributes) {
                                norm_dist *= weight2;
                            } else {
                                norm_dist *= weight1;
                            }
                            dm.setDistance(i, j, dm.getDistance(i, j) + norm_dist);
                        }
                    }
                }
            }
            this.dmat = dm;

//            ArrayList<graph.model.Edge> edgesToConnectivity = new ArrayList<graph.model.Edge>();
//            for (int i = 0; i < dm.getElementCount(); i++) {
//                for (int j = i + 1; j < dm.getElementCount(); j++) {
//                    if (dm.getDistance(i, j) < dm.getMaxDistance()) {
//                        edgesToConnectivity.add(new Edge(i, j, dm.getDistance(i, j)));
//                    }
//                }
//            }
//
//            this.conn = new Connectivity("connFromDmat", edgesToConnectivity);
        } else {
            Logger.getLogger(this.getClass().getCanonicalName()).warning("Null input.");
        }
    }

//    private void generatePointsMatrix(float d[][]) {
//        this.matrix = new DenseMatrix();
//
//        // when there isn't a connection between a pair of vertices,
//        // set it to zero
//        for (int i = 0; i < d.length; i++) {
//            for (int j = 0; j < d.length; j++) {
//                if (d[i][j] == Float.MAX_VALUE) {
//                    d[i][j] = 0;
//                }
//            }
//        }
//
//        for (int j = 0; j < d.length; j++) {
//            matrix.addRow(new DenseVector(d[j], j));
//        }
//
//    }
    public void input(@Param(name = "Graph Data") final GraphData gdata) throws CloneNotSupportedException {
        //this.gdata = ((VNAGraphData)gdata).clone();
        this.gdata = gdata;
    }

    public DistanceMatrix outputDistanceMatrix() {
        return dmat;
    }

//    public Connectivity outputConnectivity() {
//        return conn;
//    }

    @Override
    public AbstractParametersView getParametersEditor() {
//        if (this.paramview == null || this.pdata == null) {
//            refreshPdata();
//            this.paramview = new AttributesProjectionParamView(this);
//            if (this.gdata != null) {                
////                this.pdata.setAttributeNames(this.gdata.getAttributeNames());
//                //Projection não pode incluir Graph (Attributes) na biblioteca por ser cíclico
//                //Então, adicionando atributos na própría paramview; e apenas strings na pdata
//                this.paramview.setAttributes(this.gdata.getAttributes());
//            }
//        }
//        return this.paramview;
        return new AttributesProjectionParamView(this);
    }

    @Override
    public void reset() {
        dmat = null;
        gdata = null;
//        conn = null;
    }

    @Getter
    private final Set<Integer> attributesToConsider = new TreeSet<>();
    @Getter
    @Setter
    private boolean equalizeAttributes = false;

    private transient DistanceMatrix dmat;
    @Getter
    private transient GraphData gdata;
    //private transient AttributesProjectionParamView paramview = null;
//    private transient Connectivity conn;

    public static final long serialVersionUID = 1L;
}
