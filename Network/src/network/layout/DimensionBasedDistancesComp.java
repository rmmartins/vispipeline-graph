/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2012-2016 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of VisPipeline-Graph (a branch of VisPipeline).
 *
 * How to cite this work:
 *  
@article{Martins2012,
  title={Multidimensional projections for visual analysis of social networks},
  author={Martins, Rafael Messias and Andery, Gabriel Faria and Heberle, Henry and Paulovich, 
        Fernando Vieira and de Andrade Lopes, Alneu and Pedrini, Helio and Minghim, Rosane},
  journal={Journal of Computer Science and Technology},
  volume={27},
  number={4},
  pages={791--810},
  year={2012},
  publisher={Springer US}
}
 *  
 * VisPipeline-Graph is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * VisPipeline-Graph is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Visualization, Imaging and Computer Graphcs Group 
 * (http://vicg.icmc.usp.br) at Instituto de Ciencias Matematicas e de Computacao - ICMC - 
 * (http://www.icmc.usp.br) of Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial 
 * developer of VisPipeline was Fernando Vieira Paulovich <fpaulovich@gmail.com>. VisPipeline-Graph
 * is a branch of VisPipeline developed initially by Gabriel Faria Andery <gfandery@gmail.com>,
 * Henry Heberle <henryhbrl@gmail.com> and Rafael Messias Martins <rmmartins@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with VisPipeline-Graph. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package network.layout;

import distance.DistanceMatrix;
import distance.DistanceMatrixFactory;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import graph.input.GraphData;
import graph.input.GraphData.Attribute;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.Setter;
import matrix.AbstractMatrix;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Rafael Messias Martins
 */
@VisComponent(hierarchy = "Network.Layout",
        name = "Dimension-based Distances",
        description = "Creates a distance matrix from the graph's dimensions.")
public class DimensionBasedDistancesComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {        
        if (matrix != null) {            
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(dissimilarityType);
            this.dmat = new DistanceMatrix(matrix, diss);
        } else if (gdata != null) {
            // Start by verifying whether all attributes are indeed numerical
            for (Attribute att : gdata.getAttributes()) {
                if (!"id".equals(att.getName()) && !"numerical".equals(att.getType())) {
                    Logger.getLogger(this.getClass().getCanonicalName()).log(Level.WARNING, 
                            "Attribute ({0}) is not numerical (type: {1}).", 
                            new Object[]{att.getName(), att.getType()});
                    throw new IOException();
                }
            }            
            ArrayList<ArrayList<String>> string_data = gdata.getMatrix();            
            // Turn the points into a float matrix (obs.: the first attribute 'id' is skipped)
            float[][] points = new float[string_data.size()][string_data.get(0).size() - 1];
            for (int i = 0; i < string_data.size(); ++i) {
                for (int j = 1; j < string_data.get(i).size(); ++j) {
                    points[i][j-1] = Float.valueOf(string_data.get(i).get(j));
                }
            }
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(dissimilarityType);
            this.dmat = new DistanceMatrix(matrix, diss);
        } else {
            Logger.getLogger(this.getClass().getCanonicalName()).warning("Null input.");
            throw new IOException();
        }
        for (int i = 0; i < dmat.getElementCount() - 1; ++i)
            for (int j = i + 1; j < dmat.getElementCount(); ++j)
                dmat.setDistance(i, j, ((dmat.getDistance(i, j) - dmat.getMinDistance()) /
                        (dmat.getMaxDistance() - dmat.getMinDistance())));
    }

    public void input(@Param(name = "Graph Data") final GraphData gdata) {
        this.gdata = gdata;
    }
    
    public void input(@Param(name = "Points") final AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public DistanceMatrix outputDistanceMatrix() {
        return dmat;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return new DimensionBasedDistancesParamView(this);
    }

    @Override
    public void reset() {
        dmat = null;
        gdata = null;
        matrix = null;
    }

    @Getter private final Set<Integer> attributesToConsider = new TreeSet<>();
    @Getter @Setter private boolean equalizeAttributes = false;
    @Getter @Setter private DissimilarityType dissimilarityType = DissimilarityType.EUCLIDEAN;

    private transient DistanceMatrix dmat;
    @Getter private transient GraphData gdata;
    private transient AbstractMatrix matrix;

    public static final long serialVersionUID = 1L;
}
