/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2012-2016 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of VisPipeline-Graph (a branch of VisPipeline).
 *
 * How to cite this work:
 *  
@article{Martins2012,
  title={Multidimensional projections for visual analysis of social networks},
  author={Martins, Rafael Messias and Andery, Gabriel Faria and Heberle, Henry and Paulovich, 
        Fernando Vieira and de Andrade Lopes, Alneu and Pedrini, Helio and Minghim, Rosane},
  journal={Journal of Computer Science and Technology},
  volume={27},
  number={4},
  pages={791--810},
  year={2012},
  publisher={Springer US}
}
 *  
 * VisPipeline-Graph is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * VisPipeline-Graph is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Visualization, Imaging and Computer Graphcs Group 
 * (http://vicg.icmc.usp.br) at Instituto de Ciencias Matematicas e de Computacao - ICMC - 
 * (http://www.icmc.usp.br) of Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial 
 * developer of VisPipeline was Fernando Vieira Paulovich <fpaulovich@gmail.com>. VisPipeline-Graph
 * is a branch of VisPipeline developed initially by Gabriel Faria Andery <gfandery@gmail.com>,
 * Henry Heberle <henryhbrl@gmail.com> and Rafael Messias Martins <rmmartins@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with VisPipeline-Graph. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package network.input;

import network.input.bib.BibGraphUtils;
import java.io.File;
import java.io.IOException;
import lombok.Getter;
import lombok.Setter;
import textprocessing.corpus.Corpus;
import textprocessing.corpus.zip.ZipCorpus;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Rafael Martins
 */
@VisComponent(
        hierarchy = "Network.Input",
        name = "BibTeX Graph Reader",
        description = "Read BibTeX file into a graph."
)
public class BibtexReaderComp extends VNAReaderComp implements AbstractComponent {

    public enum NetworkType { AUTHORS, PAPERS, HETERO };

    @Override
    public void execute() throws IOException {
        // there must be an input file:
        if (bibFilename == null || bibFilename.isEmpty()) {
            throw new IOException("Please enter an input file (.bib).");
        }

        // determine which type of input we're dealing with:
        if (!bibFilename.toLowerCase().endsWith(".bib")) {
            throw new IOException("The input file must be \".bib\".");
        }

        // read the file:
        File bibFile = new File(this.bibFilename);

        bibFile = BibGraphUtils.clean(bibFile);
//            BibGraphUtils.createCon(bibFile);
//            BibGraphUtils.printInfo(bibFile);

        switch (networkType) {
            case AUTHORS: // authors
                this.vnaFilename = BibGraphUtils.createAuthorsVNA(bibFile);
                break;
            case PAPERS: // papers
                this.vnaFilename = BibGraphUtils.createPapersVNA(bibFile);
                break;
            case HETERO:
                this.vnaFilename = BibGraphUtils.createHeteroVNA(bibFile);
                break;
        }
        
        if (generateCorpus) {
            String zipPath = BibGraphUtils.createZipCorpus(bibFile, includeAuthors);
            corpus = new ZipCorpus(zipPath, 1);
        }       

        // Create the graph from the generated VNA
        super.execute();
        
        gdata.setCorpus(corpus);
    }

    @Override
    public VNAGraphData outputGraphData() throws CloneNotSupportedException {
        return super.outputGraphData();
    }
    
    public Corpus outputCorpus() {
        return this.corpus;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new BibtexReaderParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        gdata = null;
        connectivities = null;
        corpus = null;
    }

    // non-transient:
    @Getter @Setter private String bibFilename = "";
    @Getter @Setter private NetworkType networkType = NetworkType.HETERO;
    @Getter @Setter private boolean generateCorpus = true;
    @Getter @Setter private boolean includeAuthors = true;

    private transient BibtexReaderParamView paramview;
    private transient Corpus corpus = null;
}
