package network.input;

import graph.input.GraphData;
import graph.model.Connectivities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.Setter;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Henry Heberle, Rafael Martins
 */
@VisComponent(
        hierarchy = "Network.Input",
        name = "VNA Graph Reader",
        description = "Read VNA file."
)
public class VNAReaderComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {        
        try {
            // there must be an input file:
            if (vnaFilename == null || vnaFilename.isEmpty()) {
                throw new IOException("Please enter an input file (.vna).");
            }            
            
            if (!vnaFilename.toLowerCase().endsWith(".vna")) {
                throw new IOException("The input file must be \".vna\".");
            }
            
            // read the file:            
            this.gdata = new VNAGraphData(this.vnaFilename, true);            

            this.connectivities = gdata.getConnectivities();
            
            // Start by verifying whether all attributes are indeed numerical
            for (GraphData.Attribute att : gdata.getAttributes()) {
                if (!"id".equals(att.getName()) && !"numerical".equals(att.getType())) {
                    Logger.getLogger(this.getClass().getCanonicalName()).log(Level.WARNING, 
                            "Attribute ({0}) is not numerical (type: {1}).", 
                            new Object[]{att.getName(), att.getType()});                    
                    return;
                }
            }            
            this.matrix = new DenseMatrix();
            ArrayList<ArrayList<String>> string_data = gdata.getMatrix();            
            // Turn the points into a float matrix (obs.: the first attribute 'id' is skipped)            
            for (int i = 0; i < string_data.size(); ++i) {
                String label = string_data.get(i).get(0);
                float[] row = new float[string_data.get(i).size() - 1];
                for (int j = 1; j < string_data.get(i).size(); ++j) {
                    row[j-1] = Float.valueOf(string_data.get(i).get(j));
                }                
                this.matrix.addRow(new DenseVector(row, label.hashCode(), 0.0f), label);
            }
            this.matrix.setAttributes(gdata.getAttributeNames());
            
//            VNAGraphData vnagdata = ((VNAGraphData)gdata);
//            int paper_count = 0, author_count = 0, node_count = 0, edge_count = 0;            
//            edge_count = vnagdata.getRelationshipMatrix().size();
//            node_count = vnagdata.getIds().size();
//            for (int i = 0; i < gdata.getMatrix().size(); ++i) {
//                ArrayList<String> row = gdata.getMatrix().get(i);
//                if (this.graphDataType == GraphData.BIB) {
//                    switch (row.get(1)) {
//                        case "paper":
//                            paper_count++;
//                            break;
//                        case "author":
//                            author_count++;
//                            break;
//                    }
//                }
//            }
            
//            System.err.println("Node count: " + node_count);
//            System.err.println("Edge count: " + edge_count);
//            System.err.println("Paper count: " + paper_count);
//            System.err.println("Authr count: " + author_count);
            
            
        } catch (IOException ex) {
            Logger.getLogger(VNAReaderComp.class.getName()).log(Level.SEVERE, null, ex);
//            JOptionPane.showMessageDialog(null, ex.toString(), "Graph Reader", 
//                    JOptionPane.ERROR_MESSAGE);
            throw new IOException(ex);
        }
        
    }

    public VNAGraphData outputGraphData() throws CloneNotSupportedException {
        //return this.gdata.clone(); nao adianta usar clone aqui
        return this.gdata;
    }

    public Connectivities outputConnectivities() {
        return this.connectivities;
    }
    
    public AbstractMatrix outputMatrix() {
        return this.matrix;
    }
    
    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new GraphReaderParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        gdata = null;
        connectivities = null;
    }
    
    // non-transient:
    @Getter @Setter protected String vnaFilename = "";
        
    private transient GraphReaderParamView paramview;
    protected transient VNAGraphData gdata;
    protected transient Connectivities connectivities;        
    private transient AbstractMatrix matrix;
    
    public static final long serialVersionUID = 1L;
}
