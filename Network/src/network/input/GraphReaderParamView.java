package network.input;

import graph.util.VNAFilter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import vispipelinebasics.interfaces.AbstractParametersView;
import visualizationbasics.util.BasicsContants;
import visualizationbasics.util.OpenDialog;
import visualizationbasics.util.PropertiesManager;

/**
 *
 * @author Henry Heberle
 */
public final class GraphReaderParamView extends AbstractParametersView {

    /** Creates new form GraphReaderParamView
     * @param comp */
    public GraphReaderParamView(VNAReaderComp comp) {
        initComponents();
        this.comp = comp;
        reset();        
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        filePanel = new javax.swing.JPanel();
        vnaLabel = new javax.swing.JLabel();
        pathTextField = new javax.swing.JTextField();
        OptionsPanel = new javax.swing.JPanel();
        searchButton = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Graph Reader"));
        setMaximumSize(new java.awt.Dimension(190, 87));
        setLayout(new java.awt.GridBagLayout());

        filePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("VNA"));
        filePanel.setMaximumSize(new java.awt.Dimension(400, 60));
        filePanel.setPreferredSize(new java.awt.Dimension(600, 60));

        vnaLabel.setText("File name");
        filePanel.add(vnaLabel);

        pathTextField.setColumns(35);
        filePanel.add(pathTextField);

        OptionsPanel.setLayout(new java.awt.GridBagLayout());
        filePanel.add(OptionsPanel);

        searchButton.setText("Search...");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });
        filePanel.add(searchButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        add(filePanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        try {
            PropertiesManager spm = PropertiesManager.getInstance(BasicsContants.PROPFILENAME);
            int result = OpenDialog.showOpenDialog(spm, new VNAFilter(), this);
            if (result == JFileChooser.APPROVE_OPTION) {
                String filename = OpenDialog.getFilename();
                if (!filename.toLowerCase().endsWith(".vna")) {
                    throw new IOException("Only bib or vna files.");
                }
                pathTextField.setText(filename);
            }
        } catch (IOException ex) {
            Logger.getLogger(GraphReaderParamView.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_searchButtonActionPerformed

    @Override
    public void reset() {
//        if(comp.getFilename().toLowerCase().endsWith(".vna")){
//            bibOptions(false);
//        }else{
//            if(comp.getFilename().toLowerCase().endsWith(".bib")){
//                bibOptions(true);
//            }
//        }
        if (comp.getVnaFilename() != null) {
            pathTextField.setText(comp.getVnaFilename());
        }
    }

    @Override
    public void finished() throws IOException {
//        File file;
//        String vnaPath = null;
//
        if (pathTextField.getText().trim().length() > 0) {
//            comp.setFilename(pathTextField.getText());
//
//            if (pathTextField.getText().toLowerCase().endsWith(".vna")) {
//                comp.setGraphDataType(GraphData.VNA);
//            } else {
//                if (pathTextField.getText().toLowerCase().endsWith(".bib")) {
//                    comp.setGraphDataType(GraphData.BIB);
//                    comp.setFilename(pathTextField.getText());
//                    if (this.authorsRadioButton.isSelected()) {
//                        vnaPath = pathTextField.getText().substring(0, pathTextField.getText().length() - 4) + "-authors.vna";
//                        comp.setNetworkType(VNAReaderComp.AUTHORS);
//                    } else {
//                        if (this.papersRadioButton.isSelected()) {
//                            vnaPath = pathTextField.getText().substring(0, pathTextField.getText().length() - 4) + "-papers.vna";
//                            comp.setNetworkType(VNAReaderComp.PAPERS);
//                        } else {
//                            if (this.heteroRadioButton.isSelected()) {
//                                vnaPath = pathTextField.getText().substring(0, pathTextField.getText().length() - 4) + "-hetero.vna";
//                                comp.setNetworkType(VNAReaderComp.HETERO);
//                            }
//                        }
//                    }
//                    comp.setVnaPath(vnaPath);
//                }
////                comp.setIncludeAuthors(authorsCheckBox.isSelected());
//            }
            comp.setVnaFilename(pathTextField.getText());
        } else {
            throw new IOException("A vna or a bib file name must be provided.");
        }
    }

    private final VNAReaderComp comp;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel OptionsPanel;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel filePanel;
    private javax.swing.JTextField pathTextField;
    private javax.swing.JButton searchButton;
    private javax.swing.JLabel vnaLabel;
    // End of variables declaration//GEN-END:variables
}
