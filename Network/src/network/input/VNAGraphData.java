/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */
package network.input;


import graph.XMLGraphWriter;
import graph.input.GraphData;
import graph.model.Connectivities;
import graph.model.Connectivity;
import graph.model.Edge;
import graph.model.GraphModel;
import graph.model.ScalarsFromAttributes;
import graph.model.ScalarsFromAttributesTable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import projection.model.Scalar;
import textprocessing.corpus.Corpus;
import textprocessing.processing.Ngram;
import textprocessing.processing.TermExtractor;

/**
 *
 * @author Gabriel de Faria Andery
 */
public class VNAGraphData extends GraphData {

    private transient BufferedReader file;
    private String location;
    private ArrayList<String> tuple;
    private Corpus corpus = null;
    private HashMap<Integer, ArrayList<Ngram>> ngrams = new HashMap<Integer, ArrayList<Ngram>>();

    /**
     * Constructor.
     */
    public VNAGraphData(String location, boolean run) throws InvalidFileException {
        super(location, 1);

        try {
            this.location = location;
            this.file = new BufferedReader(new FileReader(this.location));
            this.matrix = new ArrayList<ArrayList<String>>();
            this.attributes = new ArrayList<Attribute>();
            this.visualMatrix = new ArrayList<ArrayList<String>>();
            this.visualAttributes = new ArrayList<Attribute>();
            this.relationshipMatrix = new ArrayList<ArrayList<String>>();
            this.relationships = new ArrayList<Attribute>();
            this.connectivities = new Connectivities();
            this.scalarsTable = new ScalarsFromAttributesTable();
            this.titles = new ArrayList<String>();
            this.scalars = new ArrayList<Scalar>();
            this.ids = new ArrayList<Integer>();
            this.tuple = new ArrayList<String>();
            if (run) {
                this.run();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getLocation() {
        return location;
    }

    @Override
    public void run() {
        ArrayList<String> tuple_aux;
        int count_rel_tuples = 0;
        try {
            // Read section
            String buffer = this.file.readLine();

            if (buffer.trim().equalsIgnoreCase("*node data")) {
                buffer = this.file.readLine();

                // Read attributes and types
                parseAttributes(buffer);

                if (!this.attributes.get(0).getName().equalsIgnoreCase("id")) {
                    throw new InvalidFileException("First column, " + this.attributes.get(0).getName() + ", must be id.");
                }

                buffer = this.file.readLine();
                int id_count = 0;
                while ((buffer != null)
                        && (!buffer.trim().equalsIgnoreCase("*node data"))
                        && (!buffer.trim().equalsIgnoreCase("*node properties"))
                        && (!buffer.trim().equalsIgnoreCase("*tie data"))) {
                    if (buffer.trim().equals("")) {
                        buffer = this.file.readLine();
                        continue;
                    }
                    tuple_aux = parse(buffer);
                    
                    // TEMPORARY, PLEASE DELETE ME!!!!!!!!!!!
//                    if (tuple_aux.get(1).equals("author")) {
//                        buffer = this.file.readLine();
//                        continue;
//                    }                        

                    if (tuple_aux.size() != attributes.size()) {
                        throw new InvalidFileException("Node data: invalid number of attributes: " + tuple_aux + ". Size is " + attributes.size() + ".");
                    }

                    // Add the attributes to the matrix
                    this.matrix.add(tuple_aux);                    

                    if (keyIndex.containsKey(tuple_aux.get(0))) {
                        throw new InvalidFileException("Ids must be unique: " + tuple_aux.get(0));
                    }
                    int id = id_count++;
                    ids.add(id);
                    keyIndex.put(tuple_aux.get(0), id);

                    // Add each different value into a hashtable
                    for (int i = 0; i < tuple_aux.size(); i++) {
                        this.attributes.get(i).putValue(tuple_aux.get(i));
                    }

                    // Read next line
                    buffer = this.file.readLine();
                }

                this.attributes.get(0).setId(true);
            }

            if ((buffer != null) && (buffer.trim().equalsIgnoreCase("*node properties"))) {
                buffer = this.file.readLine();
                ArrayList<String> atts = parse(buffer);
                ArrayList<String> possibleAtts = new ArrayList<String>();

                possibleAtts.add("id");
                possibleAtts.add("x");
                possibleAtts.add("y");
                possibleAtts.add("shape");
                possibleAtts.add("size");
                possibleAtts.add("color");
                possibleAtts.add("shortlabel");

                for (String att : atts) {
                    if (!possibleAtts.contains(att)) {
                        throw new InvalidFileException("Invalid property.");
                    }
                    possibleAtts.remove(att);
                    this.visualAttributes.add(new Attribute(att));
                }

                if (!this.visualAttributes.get(0).getName().equalsIgnoreCase("id")) {
                    throw new InvalidFileException("Node properties: first column must be id.");
                }

                // Read each line from file and add it to matrix
                buffer = this.file.readLine();
                while ((buffer != null)
                        && (!buffer.trim().equalsIgnoreCase("*node data"))
                        && (!buffer.trim().equalsIgnoreCase("*node properties"))
                        && (!buffer.trim().equalsIgnoreCase("*tie data"))) {
                    if (buffer.trim().equals("")) {
                        buffer = this.file.readLine();
                        continue;
                    }
                    tuple_aux = parse(buffer);

                    if (tuple_aux.size() != visualAttributes.size()) {
                        throw new InvalidFileException("Invalid number of node properties.");
                    }

                    if (!this.attributes.get(0).containsKey(tuple_aux.get(0))) {
                        throw new InvalidFileException("Id " + tuple_aux.get(0) + " not declared.");
                    }

                    for (int i = 1; i < tuple_aux.size(); i++) {
                        Integer.valueOf(tuple_aux.get(i));
                    }

                    // Add the attributes to the visual matrix
                    this.visualMatrix.add(tuple_aux);

                    // Add each different value into a hashtable
                    for (int i = 0; i < tuple_aux.size(); i++) {
                        this.visualAttributes.get(i).putValue(tuple_aux.get(i));
                    }

                    // Read next line
                    buffer = this.file.readLine();
                }
            }

            if ((buffer != null) && (buffer.trim().equalsIgnoreCase("*tie data"))) {
                buffer = this.file.readLine();
                parseRelationships(buffer);

                if (!this.relationships.get(0).getName().equalsIgnoreCase("from")) {
                    throw new InvalidFileException("Tie data: first column must be from.");
                }

                if (!this.relationships.get(1).getName().equalsIgnoreCase("to")) {
                    throw new InvalidFileException("Tie data: second column must be to.");
                }

                // Read each line from file and add it to matrix
                buffer = this.file.readLine();
                while ((buffer != null)
                        && (!buffer.trim().equalsIgnoreCase("*node data"))
                        && (!buffer.trim().equalsIgnoreCase("*node properties"))
                        && (!buffer.trim().equalsIgnoreCase("*tie data"))) {
                    if (buffer.trim().equals("")) {
                        buffer = this.file.readLine();
                        continue;
                    }
                    tuple_aux = parse(buffer);
                    
                    // TEMPORARY, PLEASE DELETE ME!!!!!!!!!!!
//                    String from = tuple_aux.get(0);
//                    String to = tuple_aux.get(1);
//                    List<String> list = Collections.list(attributes.get(0).getValues().keys());
//                    if (!list.contains(from) || !list.contains(to)) {
//                        buffer = this.file.readLine();
//                        continue;
//                    }
                    

                    if (tuple_aux.size() != relationships.size()) {
                        throw new InvalidFileException("Invalid number of attributes: " + tuple_aux);
                    }

                    if (!this.attributes.get(0).containsKey(tuple_aux.get(0))) {                        
                        System.err.println("Id " + tuple_aux.get(0) + " not declared.");
                    } else if (!this.attributes.get(0).containsKey(tuple_aux.get(1))) {                        
                        System.err.println("Id " + tuple_aux.get(1) + " not declared.");
                    } else {
                        for (int i = 2; i < tuple_aux.size(); i++) {
                            Float.valueOf(tuple_aux.get(i));
                        }

                        // Add the attributes to the matrix
                        this.relationshipMatrix.add(tuple_aux);

                        count_rel_tuples++;

                        // Add each different value into a hashtable
                        for (int i = 0; i < tuple_aux.size(); i++) {
                            this.relationships.get(i).putValue(tuple_aux.get(i));
                        }

                    }

                    // Read next line
                    buffer = this.file.readLine();
                }
            }

            if (buffer != null) {
                throw new InvalidFileException("Invalid file.");
            }
        } catch (NumberFormatException e) {
            System.err.println("Visual attributes must be integers and relationships must be numerical (integer or float).");
        } catch (Exception e) {
            e.printStackTrace();
        }



//        System.out.println("executando a parte de connectivity e scalars");
        /* by Henry Heberle */

        Integer from;
        Integer to;
        Float w;
        if (connectivities == null) {
            connectivities = new Connectivities();
        }
        int count_connectivities = 0;
        ArrayList<Edge> edges = new ArrayList<Edge>();
        for (int i = 2; i < this.relationships.size(); i++) {
            edges.clear();

            for (int j = 0; j < this.relationshipMatrix.size(); j++) {
                tuple_aux = this.relationshipMatrix.get(j);

                //System.out.println("tuple: " + tuple);

                w = Float.parseFloat(tuple_aux.get(i));

                if (w != 0.0) {
                    from = this.attributes.get(0).getValues().get(tuple_aux.get(0));
                    to = this.attributes.get(0).getValues().get(tuple_aux.get(1));
                    edges.add(new Edge(from, to, w));
                    count_connectivities++;
                }
            }
            Connectivity conn = new Connectivity(this.relationships.get(i).getName(), edges);
            //System.out.println("edges: " + conn.getEdges());
            this.connectivities.add(conn);
//            System.out.println("n de edges lidas: " + count_rel_tuples);
//            System.out.println("n de connectivities: " + this.connectivities.size());
        }


        /* by Henry Heberle */
        String id;
        String valueInTuple;
        Integer value;
        ScalarsFromAttributes scalarsAttrb;

        Scalar scalar;
        for (int i = 0; i < this.attributes.size(); i++) {
            GraphData.Attribute att = this.attributes.get(i);
            //this.titles.add(att.getName());
            //int title = this.titles.indexOf(att.getName());
            
            scalar = new Scalar(att.getName());
            this.scalars.add(scalar);

            if (att.getType().equals("numerical")) {
                Float f = 0.0f, delta = 0.0f;
                // if this attribute has [NA], I want [NA] to be the only 0.0f,
                // avoiding conflicts with other possible real 0.0f's
                // So I'll push all values up 10%
                if (att.getValues().containsKey("[NA]")) {
                    delta = (att.getMax() - att.getMin()) / 2.0f;                    
                }
                for (int j = 0; j < this.matrix.size(); j++) {
                    if (ids.size() > j) {
                        tuple_aux = this.matrix.get(j);
                        valueInTuple = tuple_aux.get(scalars.indexOf(scalar));
                        //value = att.getValues().get(valueInTuple);

                        scalarsAttrb = this.scalarsTable.getScalarsByIndexTuple(j);
                        
                        try {
                            if (!valueInTuple.equals("[NA]"))
                                f = delta + new Float(valueInTuple);
                        } catch (NumberFormatException e) {
                            System.err.println("Found value \"" + valueInTuple + "\" in numerical attribute [" + att.getName() + "]."
                                    + " This is being considered as 0.");
                        } finally {
                            scalarsAttrb.setScalar(scalar, f);
                        }

                        //vertex.get(j).setTitle(title, ((GraphData) graph.getCorpus()).getMatrix().get(j).get(i));
                        //vertex.get(j).setScalar(GraphScalar, Float.parseFloat(((GraphData) graph.getCorpus()).getMatrix().get(j).get(i)));
                    }
                }
            } else if (att.getType().equals("date")) {
                for (int j = 0; j < this.matrix.size(); j++) {
                    if (ids.size() > j) {
                        //vertex.get(j).setTitle(title, ((GraphData) graph.getCorpus()).getMatrix().get(j).get(i));
                        try {
                            tuple_aux = this.matrix.get(j);
                            valueInTuple = tuple_aux.get(scalars.indexOf(scalar));
                            long tmp = att.getDateFormat().parse(valueInTuple).getTime();
                            //cria se não existir, seleciona se já existir                            
                            scalarsAttrb = this.scalarsTable.getScalarsByIndexTuple(j);
                            scalarsAttrb.setScalar(scalar, (float) tmp);

                        } catch (ParseException ex) {
                            Logger.getLogger(GraphModel.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            } else {
                for (int j = 0; j < this.matrix.size(); j++) {
                    if (ids.size() > j) {
                        tuple_aux = this.matrix.get(j);
                        valueInTuple = tuple_aux.get(scalars.indexOf(scalar));
                        value = att.getValues().get(valueInTuple);

                        scalarsAttrb = this.scalarsTable.getScalarsByIndexTuple(j);
                        scalarsAttrb.setScalar(scalar, new Float(value));

                        //vertex.get(j).setTitle(title, ((GraphData) graph.getCorpus()).getMatrix().get(j).get(i));
                        //vertex.get(j).setScalar(GraphScalar, att.getValues().get(((GraphData) graph.getCorpus()).getMatrix().get(j).get(i)));
                    }
                }
            }
        }


        //it's necessary set it to -1.
        //if not, reading a file again or a new file
        //will result in index error 
        //GraphScalarsFromAttributes.count = -1; -> transferido para os componentes
//        System.out.println("saiu de run");
    }

    public void addScalarFromAttributes(int vertex, String scalarName, int value) {
        boolean encontrou = false;
        Scalar scalar = null;
        for (Scalar s : this.scalars) {
            if (s.getName().equals(scalarName)) {
                encontrou = true;
                scalar = s;
                break;
            }
        }
        if (!encontrou) {
            scalar = new Scalar(scalarName);
            this.scalars.add(scalar);
        }
        ScalarsFromAttributes scalarsAttrb;
        scalarsAttrb = this.scalarsTable.getScalarsByIndexTuple(vertex);
        scalarsAttrb.setScalar(scalar, new Float(value));
    }

    private ArrayList<String> parse(String buffer) {
        // Parse the input string
        tuple.clear();

        int i = 0;
        while (i < buffer.length()) {
            if ((buffer.charAt(i) == ',') || (buffer.charAt(i) == '\t') || (buffer.charAt(i) == ' ')) {
                if (i > 0) {
                    tuple.add(XMLGraphWriter.encodeToValidChars(buffer.substring(0, i)));
                } else {
                    tuple.add("");
                }

                i++;
                if (i < buffer.length()) {
                    buffer = buffer.substring(i).trim();
                    i = 0;
                } else {
                    tuple.add("");
                }
            } else if (buffer.charAt(i) == '"') {
                i++;
                if (i < buffer.length()) {
                    buffer = buffer.substring(i);

                    if (buffer.indexOf('"') > 0) {
                        tuple.add(XMLGraphWriter.encodeToValidChars(buffer.substring(0, buffer.indexOf('"'))));
                    } else {
                        tuple.add("");
                    }

                    i = buffer.indexOf('"') + 1;
                    if (i < buffer.length()) {
                        i++;
                        if (i < buffer.length()) {
                            buffer = buffer.substring(i).trim();
                            i = 0;
                        } else {
                            tuple.add("");
                        }
                    }
                } else {
                    tuple.add("");
                }
            } else {
                i++;
                if (i >= buffer.length()) {
                    tuple.add(XMLGraphWriter.encodeToValidChars(buffer));
                }
            }
        }

        return (ArrayList<String>) tuple.clone();
    }

    private void parseAttributes(String buffer) {
        // Parse the input string
        tuple = parse(buffer);

        String[] attWeight;
        String[] attType;
        for (String att : tuple) {
            attWeight = att.split("#");
            attType = attWeight[0].split("@");

            this.attributes.add(new Attribute(attType[0]));
            this.titles.add(attType[0]);

            if (attWeight.length > 1) {
                try {
                    this.attributes.get(this.attributes.size() - 1).setWeight(Integer.parseInt(attWeight[1]));
                } catch (Exception e) {
                    this.attributes.get(this.attributes.size() - 1).setWeight(1);
                }
            }

            if (attType.length > 1) {

                try {
                    this.attributes.get(this.attributes.size() - 1).setType(Type.valueOf(attType[1].toUpperCase()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (attType.length > 2) {
                    this.attributes.get(this.attributes.size() - 1).setDateFormat(attType[2]);
                }
            }
        }
    }

    private void parseRelationships(String buffer) {
        // Parse the input string
        ArrayList<String> tuple = parse(buffer);

        String[] attWeight;
        for (String att : tuple) {
            attWeight = att.split("#");

            Attribute new_attribute = new Attribute(attWeight[0]);
            if ("from".equals(att) || "to".equals(att)) {
                new_attribute.setType(Type.STRING);
            }
            
            this.relationships.add(new_attribute);

            if (attWeight.length > 1) {
                try {
                    this.relationships.get(this.relationships.size() - 1).setWeight(Integer.parseInt(attWeight[1]));
                } catch (Exception e) {
                    e.printStackTrace();
                    this.relationships.get(this.relationships.size() - 1).setWeight(1);
                }
            }
        }
    }

    public void export(ArrayList<Integer> instancesIds, String path) throws FileNotFoundException {
        PrintWriter out = new PrintWriter(new File(path));
        out.println("*Node data");

        //export just individuals (and relationships) if your id is in ids
        //attributes
        for (int i = 0; i < this.matrix.get(0).size() - 1; i++) {
            Attribute att = this.attributes.get(i);
            if (att.getType().equals("date")) {
                out.print(att.getName() + "@DATE@" + att.getDateFormat().toPattern() + ",");
            } else if (att.getType().equals("string")) {
                out.print(att.getName() + "@STRING,");
            } else {
                out.print(att.getName() + ",");
            }
        }

        //IMPORTANT
        //in the scope of Motifs projection Layout, the number of attributes difers from matrix[i].size()
        //so we use matrix[i].size instead of attributes.size()

        if (this.attributes.get(this.matrix.get(0).size() - 1).getType().equals("date")) {
            out.print(this.attributes.get(this.matrix.get(0).size() - 1).getName() + "@DATE@d/M/y");
        } else {
            out.println(this.attributes.get(this.matrix.get(0).size() - 1).getName());
        }

        for (int i = 0; i < this.matrix.size(); i++) { //its the original matrix, it's not point matrix, so id is ok with instance.getId()
            if (instancesIds.contains(i)) {
                for (int j = 0; j < this.matrix.get(i).size() - 1; j++) {
                    if (this.attributes.get(j).getType().equals("string")) {
                        out.print("\"" + this.matrix.get(i).get(j) + "\",");
                    } else {
                        if (this.attributes.get(j).getType().equals("numerical")) {
                            out.print(this.matrix.get(i).get(j) + ",");
                        } else { //if == date
                            out.print(this.matrix.get(i).get(j) + ",");
                        }

                    }
                }
                out.println("\"" + this.matrix.get(i).get(this.matrix.get(0).size() - 1) + "\"");
            }
        }



        out.println("*Tie data");
        for (int i = 0; i < this.relationships.size() - 1; i++) {
            out.print(this.relationships.get(i).getName() + ",");
        }
        out.println(this.relationships.get(this.relationships.size() - 1).getName());
        //relationships

        for (int i = 0; i < this.relationshipMatrix.size(); i++) {
            if (instancesIds.contains(keyIndex.get(this.relationshipMatrix.get(i).get(0)))) {
                if (instancesIds.contains(keyIndex.get(this.relationshipMatrix.get(i).get(1)))) {
                    for (int j = 0; j < this.relationshipMatrix.get(0).size() - 1; j++) {
                        out.print("\"" + this.relationshipMatrix.get(i).get(j) + "\",");
                    }
                    out.println("\"" + this.relationshipMatrix.get(i).get(this.relationshipMatrix.get(0).size() - 1) + "\"");
                }
            }
        }
        out.close();
    }

    @Override
    public VNAGraphData clone() throws CloneNotSupportedException {
        VNAGraphData newgdata = null;
        try {
            newgdata = new VNAGraphData(location, false);
            newgdata.location = location;
            //newgdata.file = new BufferedReader(new FileReader(newgdata.location));
            newgdata.matrix = (ArrayList<ArrayList<String>>) matrix.clone();
            newgdata.attributes = (ArrayList<Attribute>) attributes.clone();
            newgdata.visualMatrix = (ArrayList<ArrayList<String>>) visualMatrix.clone();
            newgdata.visualAttributes = (ArrayList<Attribute>) visualAttributes.clone();
            newgdata.relationshipMatrix = (ArrayList<ArrayList<String>>) relationshipMatrix.clone();
            newgdata.relationships = (ArrayList<Attribute>) relationships.clone();
            newgdata.connectivities = ((Connectivities) connectivities).clone();
            newgdata.scalarsTable = (ScalarsFromAttributesTable) scalarsTable.clone();
            newgdata.titles = (ArrayList<String>) titles.clone();
            newgdata.scalars = (ArrayList<Scalar>) scalars.clone();
            newgdata.ids = (ArrayList<Integer>) ids.clone();
            newgdata.tuple = (ArrayList<String>) tuple.clone();
            newgdata.keyIndex = (LinkedHashMap<String, Integer>) keyIndex.clone();

        } catch (InvalidFileException ex) {
            Logger.getLogger(VNAGraphData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newgdata;
    }

    @Override
    public String getFilteredContent(int id) throws IOException {
        return getFullContent(id);
    }

    @Override
    public String getFullContent(int id) throws IOException {
        String ret = "";
        for (int i = 0; i < matrix.get(id).size(); i++) {
            ret += matrix.get(id).get(i) + " ";
        }
        return ret.trim();
    }

    @Override
    public ArrayList<Ngram> getNgrams(int id) throws IOException {
        ArrayList<Ngram> ret = ngrams.get(id);
        if (ret == null) {
            ret = getNgramsFromFile(getFullContent(id));
            ngrams.put(id, ret);
        }
        return ret;
    }

    @Override
    public String getSearchContent(int id) throws IOException {
        return getFullContent(id);
    }

    @Override
    public String getViewContent(int id) throws IOException {
        return getFullContent(id);
    }

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

    private ArrayList<Ngram> getNgramsFromFile(String filecontent) throws IOException {
        HashMap<String, Integer> ngramsTable = new HashMap<String, Integer>();

        Pattern pattern = Pattern.compile(TermExtractor.getRegularExpression());
//        String filecontent = corpus.getFullContent(id);

        if (filecontent != null) {
            Matcher matcher = pattern.matcher(filecontent);

            //create the firt ngram
            String[] ngram = new String[corpus.getNumberGrams()];
            int i = 0, count = 0;
            while (count < corpus.getNumberGrams() && matcher.find()) {
                String term = matcher.group();

                if (term.length() > 0) {
                    String word = term.toLowerCase();

                    if (word.trim().length() > 0) {
                        ngram[count] = word;
                        count++;
                    }
                }

                i++;
            }

            StringBuffer sb = new StringBuffer();
            for (int j = 0; j < ngram.length - 1; j++) {
                sb.append(ngram[j] + "<>");
            }

            sb.append(ngram[ngram.length - 1]);

            //adding to the frequencies table
            ngramsTable.put(sb.toString(), 1);

            //creating the remaining ngrams
            while (matcher.find()) {
                String term = matcher.group();

                if (term.trim().length() > 0) {
                    String word = term.toLowerCase();

                    if (word.trim().length() > 0) {
                        String ng = this.addNextWord(ngram, word);

                        //verify if the ngram already exist on the document
                        if (ngramsTable.containsKey(ng)) {
                            ngramsTable.put(ng, ngramsTable.get(ng) + 1);
                        } else {
                            ngramsTable.put(ng, 1);
                        }
                    }
                }

                i++;
            }
        }

        ArrayList<Ngram> ngrams = new ArrayList<Ngram>();

        for (String n : ngramsTable.keySet()) {
            ngrams.add(new Ngram(n, ngramsTable.get(n)));
        }

        return ngrams;
    }

    private String addNextWord(String[] ngram, String word) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < ngram.length - 1; i++) {
            ngram[i] = ngram[i + 1];
            sb.append(ngram[i] + "<>");
        }

        ngram[ngram.length - 1] = word;
        sb.append(word);
        return sb.toString();
    }
}
