/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network.input.bib;

import bibtex.dom.BibtexAbstractValue;
import bibtex.dom.BibtexEntry;
import bibtex.dom.BibtexFile;
import bibtex.parser.BibtexParser;
import bibtex.parser.ParseException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author Rafael
 */
public class BibGraphUtils {

    public static String createZipCorpus(File bibFile, boolean includeAuthors) {
        try {
            // The ZIP corpus will always be generated, because the options may
            // have changed; we have no way of knowing if the (possibly) already
            // existing ZIP was created with the same options. The same with the
            // .inv derived from it.
            
            String bibPath = bibFile.getAbsolutePath();
            String zipPath = bibPath.substring(0, bibPath.length() - 4) + ".zip";
            new File(bibPath.substring(0, bibPath.length() - 4) + ".inv").delete();
            
            BibtexFile bibFile2 = new BibtexFile();
            BibtexParser parser = new BibtexParser(true);
            InputStreamReader reader = new InputStreamReader(
                    new FileInputStream(bibFile), "ISO-8859-1");
            parser.parse(bibFile2, reader);            
            
            ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(zipPath));
            for (Object entry_obj : bibFile2.getEntries()) {
                if (entry_obj instanceof BibtexEntry) {
                    BibtexEntry entry = (BibtexEntry) entry_obj;
                    // A ZipCorpus auto-generates class data based on the first two letters of each
                    // filename; we want to this because most of the time it leads to random data.
                    String key = "00" + entry.getEntryKey();
                    String title = getFieldFrom("title", entry);
                    String abst = getFieldFrom("abstract", entry);
                    String authors = getFieldFrom("author", entry);

                    String text = title + "\n\n";
                    if (includeAuthors) {
                        text += authors + "\n";
                    }
                    if (!abst.isEmpty()) {
                        text += "\n" + abst + "\n";
                    }
                    
                    zout.putNextEntry(new ZipEntry(key));
                    zout.write(text.getBytes(), 0, text.length());
                }
            }
            zout.close();
            return zipPath;
        } catch (ParseException ex) {
            Logger.getLogger(BibGraphUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BibGraphUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String createAuthorsVNA(File bibFile) {
        try {
            BibtexFile bibFile2 = new BibtexFile();
            BibtexParser parser = new BibtexParser(true);
            InputStreamReader reader = new InputStreamReader(
                    new FileInputStream(bibFile), "ISO-8859-1");
            parser.parse(bibFile2, reader);
            String bibPath = bibFile.getAbsolutePath();
            String vnaPath = bibPath.substring(0, bibPath.length() - 4) + "-authors.vna";
            List<String> listaAutores = new ArrayList<String>();
            List<String[]> listaParcerias = new ArrayList<String[]>();
            final Map<String, Integer> pesos = new HashMap<String, Integer>();
            List<Paper> listaArtigos = new ArrayList<Paper>();
            Map<String, List<Paper>> artigosPorAutor = new HashMap<String, List<Paper>>();
            for (Object entry_obj : bibFile2.getEntries()) {
                if (entry_obj instanceof BibtexEntry) {
                    BibtexEntry entry = (BibtexEntry) entry_obj;
                    Paper paper = new Paper();

                    paper.key = entry.getEntryKey();

                    paper.titulo = getFieldFrom("title", entry);

                    paper.ano = getFieldFrom("year", entry);

                    paper.tipo = entry.getEntryType();
                    
                    paper.keywords = getFieldFrom("keywords", entry);

                    String autores1 = getFieldFrom("author", entry);

                    // adicionando autores
                    String[] autores2 = splitAuthors(autores1);                    
                                        
                    listaParcerias.add(autores2);
                    for (String autor : autores2) {
                        Integer rankAutor = pesos.get(autor);
                        if (rankAutor == null) {
                            pesos.put(autor, 1);
                        } else {
                            pesos.put(autor, rankAutor + 1);
                        }

                        if (!listaAutores.contains(autor)) {
                            listaAutores.add(autor);
                        }
                    }
                    paper.autores = autores2;
                    listaArtigos.add(paper);

                    for (String autor : paper.autores) {
                        List<Paper> artigosDoAutor = artigosPorAutor.get(autor);
                        if (artigosDoAutor == null) {
                            artigosDoAutor = new ArrayList<Paper>();
                            artigosPorAutor.put(autor, artigosDoAutor);
                        }
                        artigosDoAutor.add(paper);
                    }
                }
            }
            Collections.sort(listaAutores);
            Collections.sort(listaArtigos);

            // ranking por n�m. de artigos
            List<String> ranking = new ArrayList<String>();
            ranking.addAll(listaAutores);
            Collections.sort(ranking, new Comparator<String>() {

                @Override
                public int compare(String o1, String o2) {
                    int rank1 = pesos.get(o1);
                    int rank2 = pesos.get(o2);
                    // descendente
                    return rank2 - rank1;
                }
            });

            // matriz de autores (sim�trica)
            int[][] matrizAut = new int[listaAutores.size()][listaAutores.size()];
            for (int i = 0; i < matrizAut.length; i++) {
                Arrays.fill(matrizAut[i], 0);
            }
            for (String[] parceria : listaParcerias) {
                for (int i = 0; i < parceria.length - 1; i++) {
                    int id1 = listaAutores.indexOf(parceria[i]);
                    for (int j = i + 1; j < parceria.length; j++) {
                        int id2 = listaAutores.indexOf(parceria[j]);
                        matrizAut[id1][id2]++;
                        matrizAut[id2][id1]++;
                    }
                }
            }

            PrintWriter pwAut = new PrintWriter(new File(vnaPath));
            pwAut.println("*Node data");
            pwAut.println("id,name,weight,rank");
//            for (String autor : ranking) {
//                System.out.println(autor + "\t\t" + pesos.get(autor));
//            }

            for (int id = 0; id < listaAutores.size(); id++) {
                String autor = listaAutores.get(id);
                String s1 = ",\"" + autor + "\"," + pesos.get(autor) + ","
                        + ranking.indexOf(autor);
//                pwAut.println(id + s1);
                pwAut.println("\"" + autor + "\"" + s1);
//                String s2 = id + ",author" + s1 + ",[NA],[NA],[NA]";
            }

            // Co-autoria - VNA autores & misto
            pwAut.println("*Tie data");
            pwAut.println("from,to,co-authorship");
            for (int i = 0; i < matrizAut.length - 1; i++) {
                for (int j = i + 1; j < matrizAut[i].length; j++) {
                    int peso = matrizAut[i][j];
                    if (peso > 0) {
                        String nome1 = listaAutores.get(i);
                        String nome2 = listaAutores.get(j);
                        pwAut.println("\"" + nome1 + "\",\"" + nome2 + "\"," + peso);
                    }
                }
            }
            pwAut.close();

            return vnaPath;
        } catch (ParseException ex) {
            Logger.getLogger(BibGraphUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BibGraphUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String createPapersVNA(File bibFile) {
        try {
            BibtexFile bibFile2 = new BibtexFile();
            BibtexParser parser = new BibtexParser(true);
            InputStreamReader reader = new InputStreamReader(
                    new FileInputStream(bibFile), "ISO-8859-1");
            parser.parse(bibFile2, reader);
            String bibPath = bibFile.getAbsolutePath();
            String vnaPath = bibPath.substring(0, bibPath.length() - 4) + "-papers.vna";

            List<String> listaAutores = new ArrayList<String>();
            List<String[]> listaParcerias = new ArrayList<String[]>();
            final Map<String, Integer> pesos = new HashMap<String, Integer>();
            List<Paper> listaArtigos = new ArrayList<Paper>();
            Map<String, List<Paper>> artigosPorAutor = new HashMap<String, List<Paper>>();
            for (Object entry_obj : bibFile2.getEntries()) {
                if (entry_obj instanceof BibtexEntry) {
                    BibtexEntry entry = (BibtexEntry) entry_obj;
                    Paper paper = new Paper();

                    paper.key = entry.getEntryKey();

                    paper.titulo = getFieldFrom("title", entry);

                    paper.ano = getFieldFrom("year", entry);
                    
                    paper.keywords = getFieldFrom("keywords", entry);

                    paper.tipo = entry.getEntryType();

                    paper.fonte = "";
                    if (entry.getFieldValue("journal") != null) {
                        String fonte = entry.getFieldValue("journal").toString();
//                        paper.fonte = fonte.substring(1, fonte.length() - 2);
                        paper.fonte = cleanString(fonte);
                    } else if (entry.getFieldValue("booktitle") != null) {
                        String fonte = entry.getFieldValue("booktitle").toString();
//                        paper.fonte = fonte.substring(1, fonte.length() - 2);
                        paper.fonte = cleanString(fonte);
                    }

                    String autores1 = getFieldFrom("author", entry);

                    // adicionando autores
                    String[] autores2 = splitAuthors(autores1);
                    listaParcerias.add(autores2);
                    for (String autor : autores2) {
                        Integer rankAutor = pesos.get(autor);
                        if (rankAutor == null) {
                            pesos.put(autor, 1);
                        } else {
                            pesos.put(autor, rankAutor + 1);
                        }
//						System.out.println("'" + author + "'");
                        if (!listaAutores.contains(autor)) {
                            listaAutores.add(autor);
                        }
                    }
                    paper.autores = autores2;
                    listaArtigos.add(paper);

                    for (String autor : paper.autores) {
                        List<Paper> artigosDoAutor = artigosPorAutor.get(autor);
                        if (artigosDoAutor == null) {
                            artigosDoAutor = new ArrayList<Paper>();
                            artigosPorAutor.put(autor, artigosDoAutor);
                        }
                        artigosDoAutor.add(paper);
                    }
                }
            }
            Collections.sort(listaAutores);
            Collections.sort(listaArtigos);

            // ranking por n�m. de artigos
            List<String> ranking = new ArrayList<String>();
            ranking.addAll(listaAutores);
            Collections.sort(ranking, new Comparator<String>() {

                @Override
                public int compare(String o1, String o2) {
                    int rank1 = pesos.get(o1);
                    int rank2 = pesos.get(o2);
                    // descendente
                    return rank2 - rank1;
                }
            });

            int[][] matrizArt = new int[listaArtigos.size()][listaArtigos.size()];
            for (int i = 0; i < matrizArt.length; i++) {
                Arrays.fill(matrizArt[i], 0);
            }
            for (Paper artigo1 : listaArtigos) {
                int id1 = listaArtigos.indexOf(artigo1);
                for (String autor : artigo1.autores) {
                    List<Paper> artigosDoAutor = artigosPorAutor.get(autor);
                    for (Paper artigo2 : artigosDoAutor) {
                        if (!artigo1.equals(artigo2)) {
                            int id2 = listaArtigos.indexOf(artigo2);
                            matrizArt[id1][id2]++;
                            matrizArt[id2][id1]++;
                        }
                    }
                }
            }

            // VNA de artigos
            PrintWriter pwArt = new PrintWriter(new File(vnaPath));
            pwArt.println("*Node data");
            pwArt.println("id@STRING,authors@STRING,title@STRING,year@DATE@YYYY,type@STRING,"
                    + "source@STRING,keywords@STRING");

            // Artigos - VNA artigos & misto
            for (Paper artigo : listaArtigos) {
                String autores = Arrays.toString(artigo.autores);
                autores = autores.substring(1, autores.length() - 1);
                String s1 = ",\"" + autores + "\",\"" + artigo.titulo + "\","
                        + artigo.ano + "," + artigo.tipo + ",\"" + artigo.fonte 
                        + "\",\"" + artigo.keywords + "\"";
                pwArt.println(artigo.key + s1);
            }

            // Co-autores - VNA autores & misto
            pwArt.println("*Tie data");
            pwArt.println("from,to,co-authorship");
            for (int i = 0; i < matrizArt.length - 1; i++) {
                String key1 = listaArtigos.get(i).key;
                for (int j = i + 1; j < matrizArt[i].length; j++) {
                    String key2 = listaArtigos.get(j).key;
                    int peso = matrizArt[i][j] / 2; // problema na constru��o
                    if (peso > 0) {
                        pwArt.println(key1 + "," + key2 + "," + peso);
                    }
                }
            }
            pwArt.close();

            return vnaPath;
        } catch (ParseException ex) {
            Logger.getLogger(BibGraphUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BibGraphUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String createHeteroVNA(File bibFile) {
        try {
            BibtexFile bibFile2 = new BibtexFile();
            BibtexParser parser = new BibtexParser(true);
            InputStreamReader reader = new InputStreamReader(
                    new FileInputStream(bibFile), "ISO-8859-1");
            parser.parse(bibFile2, reader);
            String bibPath = bibFile.getAbsolutePath();
            String vnaPath = bibPath.substring(0, bibPath.length() - 4) + "-hetero.vna";

            List<String> listaAutores = new ArrayList<String>();
            List<String[]> listaParcerias = new ArrayList<String[]>();
            final Map<String, Integer> pesos = new HashMap<String, Integer>();
            List<Paper> listaArtigos = new ArrayList<Paper>();
            Map<String, List<Paper>> artigosPorAutor = new HashMap<String, List<Paper>>();
            for (Object entry_obj : bibFile2.getEntries()) {
                if (entry_obj instanceof BibtexEntry) {
                    BibtexEntry entry = (BibtexEntry) entry_obj;
                    Paper paper = new Paper();

                    paper.key = entry.getEntryKey();

                    paper.titulo = getFieldFrom("title", entry);

                    paper.ano = getFieldFrom("year", entry);

                    paper.tipo = entry.getEntryType();
                    
                    paper.keywords = getFieldFrom("keywords", entry);
                    
                    paper.abstr = cleanString(getFieldFrom("abstract", entry));

                    paper.fonte = "";
                    if (entry.getFieldValue("journal") != null) {
                        paper.fonte = entry.getFieldValue("journal").toString();
//                        paper.fonte = paper.fonte.substring(1, paper.fonte.length() - 2);
                        paper.fonte = cleanString(paper.fonte);
                    } else if (entry.getFieldValue("booktitle") != null) {
                        paper.fonte = entry.getFieldValue("booktitle").toString();
//                        paper.fonte = paper.fonte.substring(1, paper.fonte.length() - 2);
                        paper.fonte = cleanString(paper.fonte);
                    }

                    String autores1 = getFieldFrom("author", entry);
                    
//                    System.out.printf("autores1: %s\n", autores1);

                    // adicionando autores
                    String[] autores2 = splitAuthors(autores1);
                    
                    listaParcerias.add(autores2);
                    for (String autor : autores2) {
//                        System.out.printf("autor: %s\n", autor);
                        Integer rankAutor = pesos.get(autor);
                        if (rankAutor == null) {
                            pesos.put(autor, 1);
                        } else {
                            pesos.put(autor, rankAutor + 1);
                        }
//						System.out.println("'" + author + "'");
                        if (!listaAutores.contains(autor)) {
                            listaAutores.add(autor);
                        }
                    }
                    paper.autores = autores2;
                    listaArtigos.add(paper);

                    for (String autor : paper.autores) {
                        List<Paper> artigosDoAutor = artigosPorAutor.get(autor);
                        if (artigosDoAutor == null) {
                            artigosDoAutor = new ArrayList<Paper>();
                            artigosPorAutor.put(autor, artigosDoAutor);
                        }
                        artigosDoAutor.add(paper);
                    }
                }
            }
            Collections.sort(listaAutores);
            Collections.sort(listaArtigos);

            // ranking por n�m. de artigos
            List<String> ranking = new ArrayList<String>();
            ranking.addAll(listaAutores);
            Collections.sort(ranking, new Comparator<String>() {

                @Override
                public int compare(String o1, String o2) {
                    int rank1 = pesos.get(o1);
                    int rank2 = pesos.get(o2);
                    // descendente
                    return rank2 - rank1;
                }
            });

            int[][] matrizArt = new int[listaArtigos.size()][listaArtigos.size()];
            for (int i = 0; i < matrizArt.length; i++) {
                Arrays.fill(matrizArt[i], 0);
            }
            for (Paper artigo1 : listaArtigos) {
                int id1 = listaArtigos.indexOf(artigo1);
                for (String autor : artigo1.autores) {
                    List<Paper> artigosDoAutor = artigosPorAutor.get(autor);
                    for (Paper artigo2 : artigosDoAutor) {
                        if (!artigo1.equals(artigo2)) {
                            int id2 = listaArtigos.indexOf(artigo2);
                            matrizArt[id1][id2]++;
                            matrizArt[id2][id1]++;
                        }
                    }
                }
            }

            // matriz de autores (sim�trica)
            int[][] matrizAut = new int[listaAutores.size()][listaAutores.size()];
            for (int i = 0; i < matrizAut.length; i++) {
                Arrays.fill(matrizAut[i], 0);
            }
            for (String[] parceria : listaParcerias) {
                for (int i = 0; i < parceria.length - 1; i++) {
                    int id1 = listaAutores.indexOf(parceria[i]);
                    for (int j = i + 1; j < parceria.length; j++) {
                        int id2 = listaAutores.indexOf(parceria[j]);
                        matrizAut[id1][id2]++;
                        matrizAut[id2][id1]++;
                    }
                }
            }

            // VNA misto
            PrintWriter pwMis = new PrintWriter(vnaPath);
            pwMis.println("*Node data");
            pwMis.println("id,node_type,name,weight,rank,authors,year,paper_type,source,keywords,abstract");

            // Autores - VNA autores & misto
            for (int id = 0; id < listaAutores.size(); id++) {
                String autor = listaAutores.get(id);                
                String s1 = ",\"" + autor + "\"," + pesos.get(autor) + ","
                        + ranking.indexOf(autor);
                String sid = "\"" + autor + "\"";
//                String s2 = id + ",author" + s1 + ",[NA],[NA],[NA],[NA]";
                String s2 = sid + ",author" + s1 + ",[NA],[NA],[NA],[NA],[NA],[NA]";
                pwMis.println(s2);
            }

            // Artigos - VNA artigos & misto
            for (Paper artigo : listaArtigos) {
                String autores = Arrays.toString(artigo.autores);
                autores = autores.substring(1, autores.length() - 1);
//                String s1 = ",\"" + autores + "\",\"" + artigo.titulo + "\","
//                        + artigo.ano + "," + artigo.tipo;
                pwMis.println(artigo.key + ",paper,\"" + artigo.titulo + "\",[NA],[NA],\"" + autores + "\","
                        + artigo.ano + "," + artigo.tipo + ",\"" + artigo.fonte + "\",\"" + artigo.keywords + "\",\"" +
                        artigo.abstr + "\"");
            }

            pwMis.println("*Tie data");
            pwMis.println("from,to,authorship,co_authors,co_papers,all");

            // Co-autores - VNA autores & misto
            for (int i = 0; i < matrizAut.length - 1; i++) {
                for (int j = i + 1; j < matrizAut[i].length; j++) {
                    int peso = matrizAut[i][j];
                    if (peso > 0) {
                        String nome1 = listaAutores.get(i);
                        String nome2 = listaAutores.get(j);
                        pwMis.println("\"" + nome1 + "\",\"" + nome2 + "\"," + "0," + peso + ",0," + peso);
                    }
                }
            }

            // Co-artigos - VNA artigos & misto
            for (int i = 0; i < matrizArt.length - 1; i++) {
                String key1 = listaArtigos.get(i).key;
                for (int j = i + 1; j < matrizArt[i].length; j++) {
                    String key2 = listaArtigos.get(j).key;
                    int peso = matrizArt[i][j] / 2; // problema na constru��o
                    if (peso > 0) {
                        pwMis.println(key1 + "," + key2 + "," + "0,0," + peso + "," + peso);
                    }
                }
            }

            // autoria - VNA misto
            for (String autor : artigosPorAutor.keySet()) {
//                int id = listaAutores.indexOf(autor);
                for (Paper artigo : artigosPorAutor.get(autor)) {
                    pwMis.println("\"" + autor + "\"," + artigo.key + ",1,0,0,1");
                }
            }

            pwMis.close();

            return vnaPath;
        } catch (ParseException ex) {
            Logger.getLogger(BibGraphUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BibGraphUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String createCon(File bibFile) {
        // CON - artigos
        String conPath = null;
        try {
            String bibPath = bibFile.getAbsolutePath();
            conPath = bibPath.substring(0, bibPath.length() - 4) + ".con";
            BibtexFile bibFile2 = new BibtexFile();
            BibtexParser parser = new BibtexParser(true);
            InputStreamReader reader = new InputStreamReader(
                    new FileInputStream(bibFile), "ISO-8859-1");
            parser.parse(bibFile2, reader);
            PrintWriter pwCon = new PrintWriter(conPath);
            pwCon.println("artigos");

            List<String> listaAutores = new ArrayList<String>();
            List<String[]> listaParcerias = new ArrayList<String[]>();
            final Map<String, Integer> pesos = new HashMap<String, Integer>();
            List<Paper> listaArtigos = new ArrayList<Paper>();
            Map<String, List<Paper>> artigosPorAutor = new HashMap<String, List<Paper>>();
            for (Object entry_obj : bibFile2.getEntries()) {
                if (entry_obj instanceof BibtexEntry) {
                    BibtexEntry entry = (BibtexEntry) entry_obj;
                    Paper paper = new Paper();

                    paper.key = entry.getEntryKey();
//					System.out.println("[" + artigo.chave + "]");

                    paper.titulo = getFieldFrom("title", entry);
//					System.out.println("titulo = '" + artigo.titulo + "'");

                    paper.ano = getFieldFrom("year", entry);
//                    paper.ano = entry.getFieldValue("year").toString();
//					System.out.println("ano = '" + artigo.ano + "'");

                    paper.tipo = entry.getEntryType();
//					System.out.println("tipo = '" + artigo.tipo + "'");

                    String autores1 = getFieldFrom("author", entry);

                    // adicionando autores
                    String[] autores2 = splitAuthors(autores1);
                    listaParcerias.add(autores2);
                    for (String autor : autores2) {
                        Integer rankAutor = pesos.get(autor);
                        if (rankAutor == null) {
                            pesos.put(autor, 1);
                        } else {
                            pesos.put(autor, rankAutor + 1);
                        }
//						System.out.println("'" + author + "'");
                        if (!listaAutores.contains(autor)) {
                            listaAutores.add(autor);
                        }
                    }
                    paper.autores = autores2;
                    listaArtigos.add(paper);

                    for (String autor : paper.autores) {
                        List<Paper> artigosDoAutor = artigosPorAutor.get(autor);
                        if (artigosDoAutor == null) {
                            artigosDoAutor = new ArrayList<Paper>();
                            artigosPorAutor.put(autor, artigosDoAutor);
                        }
                        artigosDoAutor.add(paper);
                    }
                }
            }
            Collections.sort(listaAutores);
            Collections.sort(listaArtigos);
            for (Paper artigo : listaArtigos) {
                pwCon.println(artigo.key + ".txt;" + artigo.key + ";1");
            }
            pwCon.close();
        } catch (ParseException ex) {
            Logger.getLogger(BibGraphUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BibGraphUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conPath;
    }

    public static String getFieldFrom(String field, BibtexEntry entry) {
        BibtexAbstractValue value = entry.getFieldValue(field);
        String content = "";
        if (value != null) {
            content = value.toString();
            if (content.startsWith("{")) {
                content = content.substring(1, content.length() - 1);
            }
            content = content.replaceAll("\n", " ");
            content = content.replaceAll("\r", " ");
            content = content.replaceAll("\t", " ");
            content = content.replaceAll("\"", "'");
            // remover espa�os em branco extra
            String patternStr = "\\s+";
            String replaceStr = " ";
            Pattern pattern = Pattern.compile(patternStr);
            Matcher matcher = pattern.matcher(content);
            content = matcher.replaceAll(replaceStr);
        }
        return content;
    }

    public static File clean(File bibFile) {
        return clean(bibFile, false);
    }
    
    public static File clean(File bibFile, boolean force) {
        
        String basepath = bibFile.getAbsolutePath().substring(0, 
                bibFile.getAbsolutePath().length()-4);
        
        File cleanFile = new File(basepath + "-clean.bib");
        
        if (!force) {
            // Check if it is already the clean file...
            if (bibFile.getPath().endsWith("-clean.bib")) {
                return bibFile;
            }
            // ...or if the clean file exists and is up-to-date.
            if (cleanFile.exists() && cleanFile.lastModified() > bibFile.lastModified()) {
                return cleanFile;
            }
        }
        
//        logger.info("Formatting output...");
        BibtexFile formatBib = new BibtexFile();
        Set<String> authorNameList = new TreeSet<>();
        try {
            InputStreamReader bibReader = 
                    new InputStreamReader(new FileInputStream(bibFile), "ISO-8859-1");
            BibtexFile outputBib = new BibtexFile();
            BibtexParser parser = new BibtexParser(true);
            parser.parse(outputBib, bibReader);
            for (Object entry : outputBib.getEntries()) {
                if (entry instanceof BibtexEntry) {
                    BibtexEntry bibEntry = (BibtexEntry) entry;
                    // formatting authors
                    BibtexAbstractValue author = bibEntry.getFieldValue("author");
                    if (author != null) {
                        String authorString = author.toString();                        
                        authorString = cleanString(authorString);
                        
                        // dividir em autores individuais
                        String[] authorNames = splitAuthors(authorString);
                        
                        // formatar nomes individuais e adicionar � lista geral
                        String newAuthor = "";
                        for (int i = 0; i < authorNames.length; i++) {
                            String authorName = authorNames[i];
                            String newName = "";
                            // nomes podem estar em dois formatos, com v�rgula ou sem v�rgula
                            String[] nameParts = authorName.split(",");
                            // possibilidade 1: com v�rgula
                            if (nameParts.length > 1) {
                                // o sobrenome primeiro...
                                String lastname = nameParts[0];
                                newName = lastname + ", ";
                                // ... depois as iniciais de cada nome
                                nameParts = nameParts[1].split(" ");
                                for (int j = 0; j < nameParts.length; j++) {
                                    // ele pode retornar strings de tamanho zero
                                    if (nameParts[j].length() > 0) {
                                        // min�scula significa conectores, tipo "de", etc.
                                        if (!Character.isLowerCase(nameParts[j].charAt(0))) {
                                            newName += nameParts[j].charAt(0) + ".";
                                            if (j < nameParts.length - 1) {
                                                newName += " ";
                                            }
                                        }
                                    }
                                }
                            } // possibilidade 2: sem v�rgula
                            else {
                                nameParts = authorName.split(" ");
                                // alguns nomes terminam com ponto, sabe l� porque
                                if (nameParts[nameParts.length - 1].endsWith(".")) {
                                    nameParts[nameParts.length - 1] = nameParts[nameParts.length - 1].substring(0,
                                            nameParts[nameParts.length - 1].length() - 2);
                                }
                                // primeiro o sobrenome...
                                newName = nameParts[nameParts.length - 1] + ", ";
                                // ... depois as iniciais de cada nome
                                for (int j = 0; j < nameParts.length - 1; j++) {
                                    // acontece... não sei porque
                                    if (nameParts[j].isEmpty()) {
                                        continue;
                                    }
                                    // jr. deixa como t�
                                    if (nameParts[nameParts.length - 1].toLowerCase().equals("jr.")) {
                                        if (j == nameParts.length - 2) {
                                            newName += nameParts[j] + " ";
                                            continue;
                                        }
                                    }
                                    // quando a inicial � min�scula, deve ser um conector,
                                    // então remove
                                    char initial = nameParts[j].charAt(0);
                                    if (Character.isLowerCase(initial)) {
//                                        newName += nameParts[j] + " ";
                                        continue;
                                    }
                                    // a� sim
                                    nameParts[j] = nameParts[j].charAt(0) + ".";
                                    newName += nameParts[j] + " ";
                                }
                            }
                            newName = newName.trim();
                            authorNameList.add(newName);
                            newAuthor += newName;
                            if (i < authorNames.length - 1) {
                                newAuthor += " and ";
                            }
                        }                        
                        bibEntry.setField("author", formatBib.makeString(newAuthor));
                    }
                    // formatting titles
                    BibtexAbstractValue title = bibEntry.getFieldValue("title");
                    if (title != null) {
                        String titleString = title.toString();
                        titleString = cleanString(titleString);
                        bibEntry.setField("title", formatBib.makeString(titleString));
                    }
                    // limpar journal
                    BibtexAbstractValue journal = bibEntry.getFieldValue("journal");
                    if (journal != null) {
                        String journalString = journal.toString();
                        journalString = cleanString(journalString);
                        bibEntry.setField("journal", formatBib.makeString(journalString));
                    }
                    // limpar booktitle
                    BibtexAbstractValue booktitle = bibEntry.getFieldValue("booktitle");
                    if (booktitle != null) {
                        String booktitleString = booktitle.toString();
                        booktitleString = cleanString(booktitleString);
                        bibEntry.setField("booktitle", formatBib.makeString(booktitleString));
                    }
                    // formatar chave
                    String key = formatKey(bibEntry.getEntryKey());
                    bibEntry.setEntryKey(key);
//                    System.out.println("key: " + key);

                    formatBib.addEntry(bibEntry);
                }
            }
            
            try (PrintWriter writer = new PrintWriter(new OutputStreamWriter(
                    new FileOutputStream(cleanFile), "ISO-8859-1"))) {
                formatBib.printBibtex(writer);
                writer.flush();
//            logger.info("OK!");
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return cleanFile;
    }

    private static String cleanString(String input) {
        // remove beginning and end braces
        int pos = -1;
        while ((pos = input.indexOf('{')) > -1) {
            input = input.substring(0, pos) + input.substring(pos + 1);
        }
        while ((pos = input.indexOf('}')) > -1) {
            input = input.substring(0, pos) + input.substring(pos + 1);
        }
        // removing backslash for safety
        while ((pos = input.indexOf('\\')) > -1) {
            input = input.substring(0, pos) + input.substring(pos + 2);
        }
        // removing extra spaces like tabs, returns, etc.
        input = input.replace('\n', ' ');
        input = input.replace('\t', ' ');
        input = input.replace('\r', ' ');        
        String patternStr = "\\s+";
        String replaceStr = " ";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(input);
        input = matcher.replaceAll(replaceStr);
        // removing quotes for safety
        input = input.replaceAll("[\'\"]", "_"); 
        
        return input;
    }
    
    private static String[] splitAuthors(String authors) {
        
        // resposta padr�o: a pr�pria string (autor �nico)
        String[] ret = new String[1];
        ret[0] = authors;
        
        // tentando quebrar em autores
        if (authors.contains(" and ")) {
            ret = authors.split(" and ");
        } else if (authors.contains(";")) {
            ret = authors.split(";");
        } else if (authors.contains(",") && !authors.endsWith(".")) {
            // nesse caso pode tanto ser um autor único, no padrão "Sobrenome,
            // A. B." como podem ser vários nomes separados por vírgula...
            // difícil
            ret = authors.split(",");            
        }
        
        // autores vazios devem ser modificados
//        List<String> ret2 = new ArrayList<String>(Arrays.asList(ret));
//        for (int i = 0; i < ret2.size(); i++) {
//            if (ret2.get(i).trim().isEmpty()) {
//                ret2.remove(i);
//            }
//        }
//        ret = ret2.toArray(new String[0]);
        for (int i = 0; i < ret.length; i++) {
            if (ret[i].trim().isEmpty()) {
                ret[i] = "...";
            }
        }
        
        return ret;
    }
    
    public static String formatKey(String key) {
        String[] parts = key.split("/");
        key = parts[parts.length - 1];
        return key.replaceAll("[\\/:*\"<>|]", "_");
    }
    
    public static String printInfo(File bibFile) {
        // CON - artigos
        String infoPath = null;
        try {
            String bibPath = bibFile.getAbsolutePath();
            infoPath = bibPath.substring(0, bibPath.length() - 4) + "-info.txt";
            BibtexFile bibFile2 = new BibtexFile();
            BibtexParser parser = new BibtexParser(true);
            InputStreamReader reader = new InputStreamReader(
                    new FileInputStream(bibFile), "ISO-8859-1");
            parser.parse(bibFile2, reader);
            try (PrintWriter pwInfo = new PrintWriter(infoPath)) {
                List<String> listaAutores = new ArrayList<>();
                List<String[]> listaParcerias = new ArrayList<>();
                final Map<String, Integer> pesos = new HashMap<>();
                List<Paper> listaArtigos = new ArrayList<>();
                Map<String, List<Paper>> artigosPorAutor = new HashMap<>();
                for (Object entry_obj : bibFile2.getEntries()) {
                    if (entry_obj instanceof BibtexEntry) {
                        BibtexEntry entry = (BibtexEntry) entry_obj;
                        Paper paper = new Paper();
                        
                        paper.key = entry.getEntryKey();
//					System.out.println("[" + artigo.chave + "]");
                        
                        paper.titulo = getFieldFrom("title", entry);
//					System.out.println("titulo = '" + artigo.titulo + "'");
                        
                        paper.ano = getFieldFrom("year", entry);
//                    paper.ano = entry.getFieldValue("year").toString();
//					System.out.println("ano = '" + artigo.ano + "'");
                        
                        paper.tipo = entry.getEntryType();
//					System.out.println("tipo = '" + artigo.tipo + "'");
                        
                        String autores1 = getFieldFrom("author", entry);
                        
                        // adicionando autores
                        String[] autores2 = splitAuthors(autores1);
                        listaParcerias.add(autores2);
                        for (String autor : autores2) {
                            Integer rankAutor = pesos.get(autor);
                            if (rankAutor == null) {
                                pesos.put(autor, 1);
                            } else {
                                pesos.put(autor, rankAutor + 1);
                            }
//						System.out.println("'" + author + "'");
                            if (!listaAutores.contains(autor)) {
                                listaAutores.add(autor);
                            }
                        }
                        paper.autores = autores2;
                        listaArtigos.add(paper);
                        
                        for (String autor : paper.autores) {
                            List<Paper> artigosDoAutor = artigosPorAutor.get(autor);
                            if (artigosDoAutor == null) {
                                artigosDoAutor = new ArrayList<>();
                                artigosPorAutor.put(autor, artigosDoAutor);
                            }
                            artigosDoAutor.add(paper);
                        }
                    }
                }
                Collections.sort(listaAutores, new Comparator<String>() {
                    
                    @Override
                    public int compare(String o1, String o2) {
                        int peso1 = pesos.get(o1);
                        int peso2 = pesos.get(o2);
                        return peso2 - peso1;
                    }
                });
                Collections.sort(listaArtigos);
                
                pwInfo.println("Total publications: " + listaArtigos.size());
                pwInfo.println("Total authors: " + listaAutores.size());
                pwInfo.println("Ranking:");
                int rank = 1;
                for (String autor : listaAutores) {
                    pwInfo.println(rank++ + ". " + autor + " - " + pesos.get(autor));
                }
            }
        } catch (ParseException | IOException ex) {
            Logger.getLogger(BibGraphUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return infoPath;
    }
}
