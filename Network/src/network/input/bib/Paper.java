package network.input.bib;


public class Paper implements Comparable<Paper> {
	
	final String TIPO_REVISTA = "article";
	final String TIPO_EVENTO = "inproceedings";
	final String TIPO_TESE = "phdthesis";
	final String TIPO_DISSERT = "mastersthesis";
	final String TIPO_CAPITULO = "inbook";	
	final String TIPO_RELAT = "techreport";
	final String TIPO_OUTROS = "misc";
	
	String key;
	String[] autores;
	String titulo;	
	String ano;
	String tipo;
        String fonte;
        String keywords;
        String abstr;
	
	@Override
	public boolean equals(Object obj) {		
		if (obj instanceof Paper) {
			Paper a2 = (Paper) obj;
			return key.equals(a2.key);
		}
		return false;
	}
	
	@Override
	public int compareTo(Paper o) {
		return key.compareTo(o.key);
	}

}
