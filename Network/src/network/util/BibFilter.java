package network.util;

import visualizationbasics.util.filter.AbstractFilter;

/**
 * @author Henry Heberle
 */
public class BibFilter extends AbstractFilter{

    @Override
    public String getDescription() {
        return "BibTeX file (*.bib)";
    }

    @Override
    public String getProperty() {
        return "GraphData.DIR";
    }

    @Override
    public String getFileExtension() {
        return "bib";
    }
}
