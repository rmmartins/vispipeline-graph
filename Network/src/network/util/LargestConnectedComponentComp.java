/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network.util;

import network.input.VNAGraphData;
import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Set;
import matrix.AbstractMatrix;
import matrix.sparse.SparseMatrix;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author rmartins
 */
@VisComponent(
        hierarchy = "Network.Basics",
        name = "Largest Connected Component",
        description = "Returns the largest connected component as a new graph."
)
public class LargestConnectedComponentComp implements AbstractComponent {
    
    @Override
    public void execute() throws IOException {                
        File vna_file = new File(graph.getUrl());
        String basename = graph.getUrl().substring(0, graph.getUrl().lastIndexOf("."));
        
        // First check the cache
        String new_vna_filename = basename + "-lcc.vna";
        File new_vna_file = new File(new_vna_filename);
        if (!new_vna_file.exists() || new_vna_file.lastModified() < vna_file.lastModified()) {
            ArrayList<String> vertices = new ArrayList<>(graph.getKeys());
            ArrayList<ArrayList<String>> edges = graph.getRelationshipMatrix();

            float[][] adj_matrix = new float[vertices.size()][vertices.size()];
            for (int i = 0; i < adj_matrix.length; ++i) {
                for (int j = 0; j < adj_matrix.length; ++j) {
                    adj_matrix[i][j] = 0.0f;
                }
            }

            for (ArrayList<String> edge : edges) {
                int from = vertices.indexOf(edge.get(0));
                int to = vertices.indexOf(edge.get(1));
                adj_matrix[from][to] = Integer.valueOf(edge.get(2));
                adj_matrix[to][from] = Integer.valueOf(edge.get(2));
            }

            int[] vert_comp = new int[vertices.size()];
            for (int i = 0; i < vert_comp.length; ++i) {
                vert_comp[i] = -1;
            }

            int current_comp = 0;
            for (int i = 0; i < vert_comp.length; ++i) {
                if (vert_comp[i] == -1) {
                    Queue<Integer> queue = new ArrayDeque<>();
                    vert_comp[i] = current_comp;
                    queue.offer(i);
                    while (!queue.isEmpty()) {
                        int j = queue.poll();
                        for (int k = 0; k < adj_matrix.length; ++k) {
                            if (adj_matrix[j][k] > 0.0f && vert_comp[k] == -1) {
                                vert_comp[k] = current_comp;
                                queue.offer(k);
                            }
                        }
                    }
                    ++current_comp;
                }
            }

            int[] comp_count = new int[current_comp];
            for (int i = 0; i < vert_comp.length; ++i) {
                ++comp_count[vert_comp[i]];
            }

            int largest_comp = 0;
            for (int i = 1; i < comp_count.length; ++i) {
                if (comp_count[i] > comp_count[largest_comp]) {
                    largest_comp = i;
                }
            }

            ArrayList<Integer> largest_comp_ids = new ArrayList<>();
            for (int i = 0; i < vert_comp.length; ++i) {
                if (vert_comp[i] == largest_comp) {
                    largest_comp_ids.add(i);
                }
            }
            
            graph.export(largest_comp_ids, new_vna_filename);            
        }
        new_graph = new VNAGraphData(new_vna_filename, true);
        
        // matrix is optional
        if (matrix != null) {
            String new_data_filename = basename + "-lcc.data";
            File new_data_file = new File(new_data_filename);
            if (!new_data_file.exists() || new_data_file.lastModified() < new_vna_file.lastModified()) {
                Set<String> new_vertices = new_graph.getKeys();
                new_matrix = new SparseMatrix();
                new_matrix.setAttributes(matrix.getAttributes());
                for (int i = 0; i < matrix.getRowCount(); ++i) {
                    if (new_vertices.contains(matrix.getLabel(i))) {
                        new_matrix.addRow(matrix.getRow(i));
                        new_matrix.getLabels().add(matrix.getLabel(i));
                    }
                }
                new_matrix.save(basename + "-lcc.data");
            }
        }
        
    }
    
    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        graph = null;
    }
    
    public void input(@Param(name = "Original graph") VNAGraphData graph) {
        this.graph = graph;
    }
    
    public void attach(@Param(name = "Points") AbstractMatrix matrix) {
        this.matrix = matrix;
    }
    
    public VNAGraphData outputGraph() {
        return new_graph;
    }
    
    public AbstractMatrix outputMatrix() {
        return this.new_matrix;
    }
    
    private transient VNAGraphData graph;
    private transient AbstractMatrix matrix;
    private transient VNAGraphData new_graph;
    private transient AbstractMatrix new_matrix;
    
}
