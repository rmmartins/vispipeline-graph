package network.output;

import network.input.VNAGraphData;
import network.input.VNAReaderComp;
import graph.model.Connectivities;
import graph.model.Edge;
import graph.util.CentralityMeasures;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import projection.technique.lsp.LSPProjection2DComp;
import textprocessing.processing.PreprocessorComp;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Henry Heberle, Rafael Martins
 */
@VisComponent(hierarchy = "Network.Output",
name = "Graph Writer",
description = "Write VNA file.")
public class GraphDataWriterComp implements AbstractComponent {
    
    public static void main(String[] args) throws Exception {
        VNAReaderComp reader = new VNAReaderComp();
        reader.setVnaFilename("/dev/null");
        reader.execute();
        VNAGraphData gdata = reader.outputGraphData();
        
        PreprocessorComp ppc = new PreprocessorComp();
        ppc.execute();
        AbstractMatrix matrix = ppc.outputMatrix();
        
        LSPProjection2DComp lsp = new LSPProjection2DComp();
        lsp.input(matrix);
        lsp.execute();
        AbstractMatrix projection = lsp.output();
        
        GraphDataWriterComp writer = new GraphDataWriterComp();
        writer.setFilename("/home/rmartins/tmp/teste.vna");
        writer.setUseAsRank(1);
        writer.input(projection);
        writer.input(gdata);
        writer.execute();
    }

    @Override
    public void execute() throws IOException {
        try {                        
            // there must be an output file:
            if (filename == null || filename.isEmpty()) {
                throw new IOException("Please enter an output path (.vna).");
            }
            
            // Using centrality as "rank"
            int conn = gdata.getConnectivities().size() - 1;
            CentralityMeasures cm = new CentralityMeasures(gdata.getIds(), 
                    gdata.getConnectivities().get(conn));
            
            
            // main VNA file:
            PrintWriter pw = new PrintWriter(filename);
//            PrintStream pw = System.out;            
            pw.println("*node data");
            pw.println("id x y rank");
            for (int i = 0; i < projection.getRowCount(); i++) {
                AbstractVector row = projection.getRow(i);
                String label = projection.getLabel(i);                
                int id = gdata.getIds().get(i);
                double rank = 0.0;
                switch (useAsRank) {
                    case 0:
                        rank = cm.findBetweennessOf(id);
                        break;
                    case 1:
                        rank = cm.findClosenessOf(id);
                        break;
                    case 2:
                        rank = cm.findDegreeOf(id);
                        break;
                    case 3:
                        rank = cm.findClusteringOf(id);
                        break;
                }
                pw.printf("%s %f %f %f\n", label, row.getValue(0), row.getValue(1), rank);
            }
            pw.println("*tie data");
            pw.println("from to strength");
            
            for (Edge e : gdata.getConnectivities().get(conn).getEdges()) {                
                String idSource = gdata.getId(e.getSource());
                String idTarget = gdata.getId(e.getTarget());
                pw.printf("%s %s %f\n", idSource, idTarget, e.getWeight());
            }
            pw.close();            
        } catch (Exception ex) {
            Logger.getLogger(VNAReaderComp.class.getName()).log(Level.SEVERE, null, ex);
            throw new IOException(ex);
        }        
    }

    public void input(VNAGraphData gdata) throws CloneNotSupportedException {
        this.gdata = gdata;
    }
    
    public void input(AbstractMatrix matrix) throws CloneNotSupportedException {
        this.projection = matrix;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new GraphWriterParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        gdata = null;
        connectivities = null;
        projection = null;
    }

    /**
     * @return the vna_filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the vna_filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }    

    public int getUseAsRank() {
        return useAsRank;
    }

    public void setUseAsRank(int useAsRank) {
        this.useAsRank = useAsRank;
    }
    
    // non-transient:
    private String filename = "";
    private int useAsRank = 0;
    
    private transient VNAGraphData gdata;
    private transient AbstractMatrix projection;
    private transient GraphWriterParamView paramview;    
    private transient Connectivities connectivities;
    
    public static final long serialVersionUID = 1L;
}
