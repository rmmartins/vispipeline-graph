/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Roberto Pinho <robertopinho@yahoo.com.br>, 
 *                 Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package network.topics;


import graph.model.GraphInstance;
import graph.view.interaction.GraphAbstractSelection;
import network.model.LabeledNetGraphModel;
import network.view.LabeledNetGraphFrame;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import labeledgraph.model.LabeledGraphInstance;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

public class NetTopicSelection extends GraphAbstractSelection {

    public NetTopicSelection(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (selinst.size() > 0) {
            try {
                LabeledNetGraphModel model = (LabeledNetGraphModel) viewer.getModel();
                if (OpenDialog.checkCorpus(model, null)) {
                    AbstractTopicCreator creator = new NetCovarianceTopic(model);
                    ///
                    ArrayList<LabeledGraphInstance> selinst2 = new ArrayList<LabeledGraphInstance>();
                    for (AbstractInstance ai : selinst) {
                        selinst2.add((LabeledGraphInstance)ai);
                    }
                    ///
                    NetTopic topic = creator.createTopic(selinst2);
                    model.addTopic(topic);

                    ((LabeledNetGraphFrame) viewer).updateScalars(creator.getScalar());
                }
            } catch (IOException ex) {
                Logger.getLogger(NetTopicSelection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Information16.gif"));
    }

    @Override
    public String toString() {
        return "Create topic";
    }

//    private TopicFactory.TopicType topictype = TopicFactory.TopicType.COVARIANCE;

    @Override
    public void released(GraphInstance instance) {
//        JOptionPane.showMessageDialog(null, new UnsupportedOperationException(
//                "NetTopicSelection.released: Not supported yet."));
    }

    @Override
    public void selected(int x, int y) {
//        JOptionPane.showMessageDialog(null, new UnsupportedOperationException(
//                "NetTopicSelection.selected: Not supported yet."));
    }
}
