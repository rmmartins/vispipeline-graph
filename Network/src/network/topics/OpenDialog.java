/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package network.topics;

import network.input.VNAGraphData;
import network.input.bib.BibGraphUtils;
import java.awt.Component;
import java.io.File;
import java.io.IOException;
import javax.swing.JOptionPane;
import labeledgraph.model.LabeledGraphModel;
import textprocessing.corpus.zip.ZipCorpus;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class OpenDialog extends visualizationbasics.util.OpenDialog {

    public static boolean checkCorpus(LabeledGraphModel model,
            Component parent) throws IOException {
        if (OpenDialog.dialog == null) {
            OpenDialog.dialog = new javax.swing.JFileChooser();
        }

        _filename = null;

        if (model != null && model.getCorpus() == null) {
//            JOptionPane.showMessageDialog(null, "Some error occurred with the"
//                    + "GraphData; can't create the topics.");            
            VNAGraphData gdata = (VNAGraphData)model.getGraphData();
            String path = gdata.getLocation();
            File bibFile = new File(path);
            if (bibFile.exists()) {
                String zipPath = BibGraphUtils.createZipCorpus(bibFile, true);
                File zipFile = new File(zipPath);
                ZipCorpus corpus = new ZipCorpus(zipPath, 1);
                if (zipFile.exists() && corpus != null) {
                    model.setCorpus(corpus);                    
                } else {
                    JOptionPane.showMessageDialog(null, "Error while creating a "
                        + "Corpus to extract topics: couldn't create the ZIP.");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error while creating a "
                        + "Corpus to extract topics: the original BIB file"
                        + "doesn't exist.");
            }
        }
        return true;
    }

}
