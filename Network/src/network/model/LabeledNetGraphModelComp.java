/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2012-2016 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of VisPipeline-Graph (a branch of VisPipeline).
 *
 * How to cite this work:
 *  
@article{Martins2012,
  title={Multidimensional projections for visual analysis of social networks},
  author={Martins, Rafael Messias and Andery, Gabriel Faria and Heberle, Henry and Paulovich, 
        Fernando Vieira and de Andrade Lopes, Alneu and Pedrini, Helio and Minghim, Rosane},
  journal={Journal of Computer Science and Technology},
  volume={27},
  number={4},
  pages={791--810},
  year={2012},
  publisher={Springer US}
}
 *  
 * VisPipeline-Graph is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * VisPipeline-Graph is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Visualization, Imaging and Computer Graphcs Group 
 * (http://vicg.icmc.usp.br) at Instituto de Ciencias Matematicas e de Computacao - ICMC - 
 * (http://www.icmc.usp.br) of Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial 
 * developer of VisPipeline was Fernando Vieira Paulovich <fpaulovich@gmail.com>. VisPipeline-Graph
 * is a branch of VisPipeline developed initially by Gabriel Faria Andery <gfandery@gmail.com>,
 * Henry Heberle <henryhbrl@gmail.com> and Rafael Messias Martins <rmmartins@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with VisPipeline-Graph. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package network.model;

import network.input.VNAGraphData;
import graph.model.Connectivities;
import graph.model.Connectivity;
import graph.model.Edge;
import graph.model.GraphModelComp;
import graph.model.ScalarsFromAttributes;
import graph.model.ScalarsFromAttributesTable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import projection.model.Scalar;
import projection.util.ProjectionConstants;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;


/**
 *
 * @author Fernando Vieira Paulovich, Rafael Messias Martins
 */
@VisComponent(hierarchy = "Network.Basics",
name = "Network Model",
description = "Create a network model to be visualized.")
public class LabeledNetGraphModelComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        
        ScalarsFromAttributes scalarsFromTuple;
        if (placement != null && gdata != null) {
            model = new LabeledNetGraphModel();
            
            Scalar cdata = model.addScalar(ProjectionConstants.CDATA);
            Scalar dots = model.addScalar(ProjectionConstants.DOTS);  
            
            int nrows = placement.getRowCount();
            
            if (scalarAttributesTable == null) {
                scalarAttributesTable = gdata.getScalarsFromAttributesTable();
            }
            
            // IMPORTANT: BE CAREFUL!! Sequential id's from placement and gdata are not the same!!!
            // This is not - by far - the best way to do the matching, but it will do for now.
            for (int i = 0; i < gdata.getMatrix().size(); ++i) {
                String label1 = gdata.getMatrix().get(i).get(0);
//                System.out.print("gdata(" + i + "/" + label1 + "),");                
                for (int j = 0; j < placement.getRowCount(); ++j) {
                    String label2 = placement.getLabel(j);
                    if (label2.equals(label1)) {
//                        System.out.println("placement(" + j + "/" + label2 + ")");
                        AbstractVector row = placement.getRow(j);
                        LabeledNetGraphInstance pi = new LabeledNetGraphInstance(
                                (LabeledNetGraphModel) model,
                                placement.getLabel(j),
                                row.getId(),
                                row.getValue(0),
                                row.getValue(1));
                        pi.setScalarValue(cdata, row.getKlass());
                        pi.setScalarValue(dots, 0.0f);

                        scalarsFromTuple = this.scalarAttributesTable.getScalarsByIndexTuple(i);
                        if (scalarsFromTuple != null) {
                            for (Scalar scalar : scalarsFromTuple.getScalarList()) {
                                try {
                                    model.addScalar(scalar);
                                    pi.setScalarValue(scalar, scalarsFromTuple.getScalarValue(scalar));
                                } catch (Exception ex) {
                                    Logger.getLogger(GraphModelComp.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                        break;
                    }
                }
            }                

            // We need to copy the connectivities from the original graph, because the original
            // graph may also be used for other models
            Connectivities gdata_conns = new Connectivities();
            for (Connectivity conn_aux : gdata.getConnectivities()) {
                ArrayList<Edge> edges = new ArrayList<>();
                for (Edge edge : conn_aux.getEdges()) {
                    edges.add(new Edge(edge.getSource(), edge.getTarget(), edge.getWeight()));
                }
                Connectivity newconn = new Connectivity(conn_aux.getName(), edges);
                newconn.setShowArrow(conn_aux.isShowArrow());
                gdata_conns.add(newconn);
            }
//            if (conns != null) {
//                conns.addAll(gdata_conns);
//            } else {
                conns = gdata_conns;
//            }

            

            
            
            //adding the connectivities
            Connectivity dotsCon = new Connectivity(ProjectionConstants.DOTS, new ArrayList<Edge>());

            model.addConnectivity(dotsCon);

            if (conns != null) {
                for (int i = 0; i < conns.size(); i++) {                    
                    model.addConnectivity(conns.get(i));
                }
            }
            
//            if (this.conn != null) {
//                model.setSelectedConnectivity(this.conn);
//            }
            
            
            ((LabeledNetGraphModel) model).setGraphData(gdata); //setAttributes() is in setGraphData()
            ((LabeledNetGraphModel) model).setCorpus(((VNAGraphData)gdata).getCorpus());
        } else {
            throw new IOException("A 2D position should be provided.");
        }
    }

    public void input(@Param(name = "2D placement") AbstractMatrix placement,
            @Param(name = "layout graph data") VNAGraphData gdata) throws CloneNotSupportedException {
        this.placement = placement;
//        this.gdata = ((VNAGraphData)gdata).clone();
        this.gdata = gdata;
    }

//    public void attach(@Param(name = "connectivity") Connectivity conn) {
//        if (conns == null) {
//            conns = new Connectivities();
//        }
//        if (conn != null && !conns.contains(conn)) {
//            conns.add(conn);
//        }
//        this.conn = conn;
//    }
//
//    public void attach(@Param(name = "connectivities") Connectivities conn) {        
//        if (conns == null) {
//            this.conns = new Connectivities();
//        }
//        this.conns.addAll(conn);
//    }   

//    public void attach (@Param(name = "attributes and possible values") Attributes attributes){
//        this.attributes = attributes;
//    }
    public LabeledNetGraphModel output() {
        return model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        placement = null;
        model = null;
//        conns = null;
        scalarAttributesTable = null;
        gdata = null;
    }
    
    public static final long serialVersionUID = 1L;
    private transient LabeledNetGraphModel model;
    private transient AbstractMatrix placement;
    private transient Connectivities conns = new Connectivities();
//    private transient Connectivity conn = null;
    private transient ScalarsFromAttributesTable scalarAttributesTable;
//    private transient ArrayList<Attribute> attributes;
    private transient VNAGraphData gdata;
}
