/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network.model;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import labeledgraph.model.LabeledGraphInstance;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class LabeledNetGraphInstance extends LabeledGraphInstance {

    protected Shape drawAsShape; //0: dot, 1: circle, 2:image
    protected String title = Integer.toString(id);
    private int x_size = 100;
    private int y_size = 20;
//    private float xX = 0; //The expanded x-coodinate of the vertex
//    private float xY = 0; //The expanded y-coodinate of the vertex

    public LabeledNetGraphInstance(LabeledNetGraphModel model, String l, int id, float x, float y) {
        super(model, l, id, x, y);
        drawAsShape = Shape.CIRCLE;
        showlabel = false;
        showImage = false;
        sizefactor = 0;
    }

    public LabeledNetGraphInstance(LabeledNetGraphModel model, int id) {
        this(model, "", id, 0.0f, 0.0f);
    }

    @Override
    public void draw(BufferedImage image, boolean highquality) {
        if (this.isEnabled()) {
            Graphics2D g2 = (Graphics2D) image.getGraphics();
            LabeledNetGraphModel tmodel = (LabeledNetGraphModel) model;
            if (highquality) {
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            }
            int xaux = (((int) this.x) <= 0) ? 1 : (((int) this.x) < image.getWidth()) ? (int) this.x : image.getWidth() - 1;
            int yaux = (((int) this.y) <= 0) ? 1 : (((int) this.y) < image.getHeight()) ? (int) this.y : image.getHeight() - 1;
            int inssize = this.getSize();
            float alpha = tmodel.getAlpha();

            /*edited by Henry Heberle*/
            if ((tmodel.getSelectedInstances() == null) || (tmodel.getSelectedInstances().isEmpty())) {
                alpha = 1.0f;
            } else {
                if (this.isSelected()) {
                    alpha = 1.0f;
                } else {
                    alpha = 0.2f;
                }
            }

            if (((LabeledNetGraphModel)model).isDrawAsLabels()) {

                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, alpha));

                String label = this.toString().trim();
                if (this.toString().trim().length() > 20) {
                    label = this.toString().trim().substring(0, 17) + "...";
                }

                //g2.setFont(Vertex.font);
                java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());

                this.x_size = metrics.stringWidth(label) + 10;
                this.y_size = metrics.getAscent();
                /*
                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
                g2.setPaint(java.awt.Color.WHITE);
                g2.fill(new java.awt.Rectangle(((int) this.x) + this.getSize() + 5 - 2,
                ((int) this.y) - 1 - height, width + 4, height + 4));
                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                 */

                g2.setColor(this.color);
                g2.fillRect((int) this.getX() - (this.x_size / 2), (int) this.getY()
                        - this.getSize() - (this.y_size / 2), this.x_size, this.y_size + 2 * this.getSize());

                g2.setColor(Color.BLACK);
                g2.drawRect((int) this.getX() - (this.x_size / 2), (int) this.getY()
                        - this.getSize() - (this.y_size / 2), this.x_size, this.y_size + 2 * this.getSize());

                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));

                g2.setColor(java.awt.Color.DARK_GRAY);
                //g2.drawRect(((int) this.x) + this.getSize() + 5 - 2, ((int) this.y) - 1 - height,
                //        width + 4, height + 4);
                g2.drawString(label, (int) this.getX() - (this.x_size / 2) + 5, (int) this.getY() + (this.y_size / 2));

//                else { //not valid
//                if (Vertex.showNonValid) {
//                    int SIZE = 2;
//                    g2.setColor(Color.WHITE);
//                    g2.fillOval(((int) this.x) - SIZE, ((int) this.y) - SIZE, SIZE * 2, SIZE * 2);
//
//                    g2.setColor(Color.GRAY);
//                    g2.drawOval(((int) this.x) - SIZE, ((int) this.y) - SIZE, SIZE * 2, SIZE * 2);
//                }            
            } else {

                switch (drawAsShape) {
                    case DOT:
                        int rgb = color.getRGB();
                        inssize /= 2;
                        if (selected) {
                            alpha = 1.0f;
                            g2.setColor(Color.BLACK);
                            g2.drawRect(xaux - inssize - 1, yaux - inssize - 1, inssize * 2 + 2, inssize * 2 + 2);
                        }
                        for (int i = -inssize; i <= inssize; i++) {
                            for (int j = -inssize; j <= inssize; j++) {
                                simulateAlpha(image, alpha, xaux - i, yaux - j, rgb);
                            }
                        }
                        break;
                    case CIRCLE:
                        if (selected) {
                            alpha = 1.0f;
                        }
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, alpha));
                        g2.setColor(color);
                        g2.fillOval(xaux - inssize, yaux - inssize, inssize * 2, inssize * 2);
                        g2.setColor(Color.BLACK);
                        g2.drawOval(xaux - inssize, yaux - inssize, inssize * 2, inssize * 2);
                        g2.setStroke(new BasicStroke(1.0f));
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                        break;
                    case IMAGE:
                        if (this.image != null) {
                            Image img = this.image.getScaledInstance(this.getSize() * 5, this.getSize() * 5, 0);
                            int w = img.getWidth(null);
                            int h = img.getHeight(null);
                            g2.drawImage(img, xaux - (w / 2), yaux - (h / 2), null);
//                        int w = this.image.getWidth(null);
//                        int h = this.image.getHeight(null);
//                        g2.drawImage(this.image,xaux-(w/2),yaux-(h/2),null);
                            //Desenhando o contorno colorido para mostrar a classe...
                            if (drawFrame) {
                                g2.setStroke(new BasicStroke(3.0f));
                                g2.setColor(this.getColor());
                                g2.drawRect(xaux - (w / 2) - 2, yaux - (h / 2) - 2, w + 3, h + 3);
                                g2.setStroke(new BasicStroke(1.0f));
                            }
                            //Desenhando o contorno colorido para mostrar a classe...
                            if (drawFrame) {
                                g2.setStroke(new BasicStroke(3.0f));
                                g2.setColor(this.getColor());
                                g2.drawRect(xaux - w / 2 - 2, yaux - h / 2 - 2, w + 3, h + 3);
                                g2.setStroke(new BasicStroke(1.0f));
                            }
                            if (selected) {
                                g2.setStroke(new BasicStroke(2.0f));
                                g2.setColor(Color.RED);
                                g2.drawRect(((int) this.x) - w / 2, ((int) this.y) - h / 2, w, h);
                                g2.setStroke(new BasicStroke(1.0f));
                            }
                        }
                        break;

                    case SQUARE:
                        if (selected) {
                            alpha = 1.0f;
                        }
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, alpha));
                        g2.setColor(this.color);
                        g2.fillRect(xaux - inssize, yaux - inssize, 2 * inssize, 2 * inssize);
                        g2.setColor(Color.BLACK);
                        g2.drawRect(xaux - inssize, yaux - inssize, 2 * inssize, 2 * inssize);
                        g2.setStroke(new BasicStroke(1.0f));
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                        break;

                    case UPTRIANGLE:
                        if (selected) {
                            alpha = 1.0f;
                        }
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, alpha));
                        g2.setColor(this.color);
                        g2.fill(new java.awt.Polygon(new int[]{xaux - inssize, xaux + inssize, xaux},
                                new int[]{yaux + inssize, yaux + inssize, yaux - inssize}, 3));
                        g2.setColor(Color.BLACK);
                        g2.draw(new java.awt.Polygon(new int[]{xaux - inssize, xaux + inssize, xaux},
                                new int[]{yaux + inssize, yaux + inssize, yaux - inssize}, 3));
                        g2.setStroke(new BasicStroke(1.0f));
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                        break;

                    case DOWNTRIANGLE:
                        if (selected) {
                            alpha = 1.0f;
                        }
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, alpha));
                        g2.setColor(this.color);
                        g2.fill(new java.awt.Polygon(
                                new int[]{xaux - inssize, xaux + inssize, xaux},
                                new int[]{yaux - inssize, yaux - inssize, yaux + inssize}, 3));
                        g2.setColor(Color.BLACK);
                        g2.draw(new java.awt.Polygon(new int[]{xaux - inssize, xaux + inssize, xaux},
                                new int[]{yaux - inssize, yaux - inssize, yaux + inssize}, 3));
                        g2.setStroke(new BasicStroke(1.0f));
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                        break;

                    case DIAMOND:
                        if (selected) {
                            alpha = 1.0f;
                        }
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, alpha));
                        g2.setColor(this.color);
                        g2.fill(new java.awt.Polygon(
                                new int[]{xaux - inssize, xaux, xaux + inssize, xaux},
                                new int[]{yaux, (int) (this.y + 1.5 * inssize), yaux, (int) (this.y - 1.5 * inssize)}, 4));
                        g2.setColor(Color.BLACK);
                        g2.draw(new java.awt.Polygon(
                                new int[]{xaux - inssize, xaux, xaux + inssize, xaux},
                                new int[]{yaux, (int) (this.y + 1.5 * inssize), yaux, (int) (this.y - 1.5 * inssize)}, 4));
                        g2.setStroke(new BasicStroke(1.0f));
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                        break;

                    case BOX:
                        if (selected) {
                            alpha = 1.0f;
                        }
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, alpha));
                        g2.setColor(this.color);
                        g2.fillRect(xaux - inssize, yaux - inssize, 2 * inssize, 2 * inssize);
                        g2.setColor(Color.BLACK);
                        g2.drawRect(xaux - inssize, yaux - inssize, 2 * inssize, 2 * inssize);
                        g2.drawLine(xaux - inssize, yaux, xaux + inssize, yaux);
                        g2.drawLine(xaux, yaux - inssize, xaux, yaux + inssize);
                        g2.setStroke(new BasicStroke(1.0f));
                        g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                        break;
                }
                //show the label associated to this instance
                if (showlabel) {
                    //show the label associated to this instance
                    java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());

                    String label = this.toString().trim();
                    if (this.toString().trim().length() > 20) {
                        label = this.toString().trim().substring(0, 17) + "...";
                    }

                    int width = metrics.stringWidth(label) + 10;
                    int height = metrics.getAscent();

                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
                    g2.setPaint(java.awt.Color.WHITE);
                    g2.fillRect(xaux + 3, yaux - 1 - height, width + 4, height + 4);

                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                    g2.setColor(java.awt.Color.DARK_GRAY);
                    g2.drawRect(xaux + 3, yaux - 1 - height, width + 4, height + 4);

                    g2.drawString(label, xaux + 3, yaux);

                } else if (showImage) {
//            if (this.image != null)
//                g2.drawImage(this.image.getScaledInstance(100,100,0),xaux+(size/2),yaux+(size/2),null);
                }
            }
        }
    }

   

    public void setDrawAsShape(Shape d) {
        drawAsShape = d;
    }

    public Shape getDrawAsShape() {
        return drawAsShape;
    }

    public void setShape(Shape shape) {
        this.drawAsShape = shape;
    }

    public Shape getShape() {
        return drawAsShape;
    }

    @Override
    public void setX(float x) {
        this.x = x;
        
    }

    @Override
    public void setY(float y) {
        this.y = y;
        
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LabeledNetGraphInstance createClone(LabeledNetGraphModel newModel) {
        LabeledNetGraphInstance gi = new LabeledNetGraphInstance(newModel, label, id, x, y);
        gi.setColor(this.getColor());
        gi.setSelected(this.isSelected());
        gi.setShowLabel(this.isShowLabel());
        gi.setSizeFactor(this.getSizeFactor());
        gi.fdata = this.fdata;
        gi.scalars = new ArrayList<>();
        gi.setTitle(this.getTitle());
        gi.setShape(this.getShape());
        gi.setDrawAs(this.getDrawAs());

        for (int i = 0; i < this.scalars.size(); i++) {
            gi.scalars.add(this.scalars.get(i));
        }


        return gi;
    }



    public enum Shape {

        CIRCLE, SQUARE, UPTRIANGLE, DOWNTRIANGLE,  DIAMOND, DOT, BOX, IMAGE
    }
}
