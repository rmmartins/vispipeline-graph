/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2012-2016 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of VisPipeline-Graph (a branch of VisPipeline).
 *
 * How to cite this work:
 *  
@article{Martins2012,
  title={Multidimensional projections for visual analysis of social networks},
  author={Martins, Rafael Messias and Andery, Gabriel Faria and Heberle, Henry and Paulovich, 
        Fernando Vieira and de Andrade Lopes, Alneu and Pedrini, Helio and Minghim, Rosane},
  journal={Journal of Computer Science and Technology},
  volume={27},
  number={4},
  pages={791--810},
  year={2012},
  publisher={Springer US}
}
 *  
 * VisPipeline-Graph is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * VisPipeline-Graph is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Visualization, Imaging and Computer Graphcs Group 
 * (http://vicg.icmc.usp.br) at Instituto de Ciencias Matematicas e de Computacao - ICMC - 
 * (http://www.icmc.usp.br) of Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial 
 * developer of VisPipeline was Fernando Vieira Paulovich <fpaulovich@gmail.com>. VisPipeline-Graph
 * is a branch of VisPipeline developed initially by Gabriel Faria Andery <gfandery@gmail.com>,
 * Henry Heberle <henryhbrl@gmail.com> and Rafael Messias Martins <rmmartins@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with VisPipeline-Graph. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package network.model;

import graph.input.GraphData;
import graph.input.GraphData.Attribute;
import graph.model.Connectivity;
import network.TopicData;
import network.model.LabeledNetGraphInstance.Shape;
import network.topics.NetTopic;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import labeledgraph.model.LabeledGraphModel;
import visualizationbasics.model.AbstractInstance;


/**
 *
 * @author Fernando Vieira Paulovich, Rafael Messias Martins
 */
public class LabeledNetGraphModel extends LabeledGraphModel {

    public LabeledNetGraphModel() {
        this.connectivities = new ArrayList<>();
        this.selsconn = null;
        this.setInstanceSize(4);
        this.shapes = new HashMap<>();
    }

    @Override
    public String toString() {
        if (source.isEmpty()) {
            return "Labeled Graph Model";
        } else {
            return source;
        }
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<Attribute> attributes) {
        this.attributes = attributes;
    }

    /* by Henry Heberle*/
    @Override
    public GraphData getGraphData() {
        return gdata;
    }

    @Override
    public void setGraphData(GraphData gdata) {
        this.gdata = gdata;
        this.attributes = gdata.getAttributes();
    }

    public int getShapeAttribute() {
        return shapeAttribute;
    }

    public void setShapeAttribute(int shapeAttribute) {
        this.shapeAttribute = shapeAttribute;
    }

    public void changeTitles(String attr) {
        for (AbstractInstance gi : this.instances) {
            int id = gi.getId();
            if (id < 0 || id >= instances.size())
                id = instances.indexOf(gi);
            ((LabeledNetGraphInstance) gi).setTitle(this.getGraphData().getTitleOfATuple(attr, id));
        }
    }

    public void removeTitles(String attr) {
        for (AbstractInstance gi : this.instances) {
            int id = gi.getId();
            if (id < 0 || id >= instances.size())
                id = instances.indexOf(gi);
            ((LabeledNetGraphInstance) gi).setTitle(this.getGraphData().getTitleOfATuple(attr, id));
        }
    }
//    public void setSelectedInstance(GraphInstance selinst) {
//        if (selinst != null) {
//            cleanSelectedInstances();
//            if (selinst.getModel() == this) {
//                selinst.setSelected(true);
//                selinstances.add(selinst);
//            }
//            setChanged();
//        }
//    }

    public boolean isDrawAsLabels() {
        return this.drawAsLabels;
    }

    public void setDrawAsLabels(boolean drawAsLabels) {
        this.drawAsLabels = drawAsLabels;
    }

    public void setConnectivities(ArrayList<Connectivity> conns) {
        this.connectivities = conns;
        this.updateAllConnectivities();
        this.setChanged();
    }

    public TopicData getTopicData() {
        return tdata;
    }

    public void setTopicData(TopicData tdata) {
        this.tdata = tdata;
    }

    public boolean isShowTopics() {
        return showTopics;
    }

    public void setShowTopics(boolean showTopics) {
        this.showTopics = showTopics;
    }
    
    public void addTopic(NetTopic topic) {
        this.topics.add(topic);
        setChanged();
    }
    
    public List<NetTopic> getTopics() {
        return topics;
    }

    public HashMap<String, Shape> getShapes() {
        return shapes;
    }

    public void setShapes(HashMap<String, Shape> shapes) {
        this.shapes = shapes;
    }
        
    protected ArrayList<Attribute> attributes;
    protected ArrayList<String> titles;
    protected HashMap<String, Shape> shapes;
    protected GraphData gdata;
    protected int shapeAttribute = 0;
    protected boolean drawAsLabels = false;
    protected TopicData tdata = new TopicData(this);
    protected boolean showTopics = false;
    protected ArrayList<NetTopic> topics = new ArrayList<NetTopic>();
}
