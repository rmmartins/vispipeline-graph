/*
 *  Copyright (C) 2003  Jens Kanschik,
 * 	mail : jensKanschik@users.sourceforge.net
 *
 *  Part of <hypergraph>, an open source project at sourceforge.net
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package hypergraph.hyperbolic;

import hypergraph.graph.NodeImpl;
import hypergraph.graphApi.Element;
import hypergraph.graphApi.Node;
import hypergraph.graphApi.io.CSSColourParser;

import hypergraph.visualnet.GraphPanel;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import labeledprojection.util.ImageCollection;
import tree.model.TreeInstance;


/**
 * @author Jens Kanschik
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class DefaultTextRenderer extends JComponent implements TextRenderer {

    protected ModelPanel modelPanel;
    protected String text;
    protected JComponent stubRenderer = new EmptyComponent();
    protected boolean useStubRenderer;
    /** The maximum length for a displayed text. */
    private int maxTextLength = -1;
    private Color color;
    private Color background;
    private Font font1;
    private Font font2;
    private Font font3;
    private Font font4;
    private double scale1;
    private double scale2;
    private double scale3;
    private double scale4;
    private double optimalRatio = 8;
    private List lines;
//    private List vertexList;
    private int drawAs = 0; // 0 (text / dots) ; 1 (circle) ; 2 (image)
    private TreeInstance v;
    private double curScale;
    protected boolean globalsel = false;
     private Image toolTipImage;
    //Used to show the vertex tool tip
    private String toolTipLabel;

    public DefaultTextRenderer() {
        super();
        setOpaque(false);
        useStubRenderer = true;
        lines = new ArrayList();
//        vertexList = new ArrayList();
        curScale = 1;
    }

    public void configure(ModelPanel mp) {
        configure(mp, null, null);
    }

    /** Returns the maximal allowed length of the displayed text.
     * If it is less than 0, there are no restrictions,
     * if it is nonnegative and the text is longer than this number,
     * only the first maxTextLength -2 characters are shown plus two dots ("..").
     * <br />
     * The default value is -1, it can be overwritten by the property
     * "hypergraph.hyperbolic.text.maxLength" or by the method {@link setMaxTextLength(int)}.
     *
     * @return The maximal allowed text length.
     */
    public int getMaxTextLength() {
        return maxTextLength;
    }

    /** Sets the maximal allowed length of displayed text.
     * @see #getMaxTextLength()
     * @param mtl The maximal allowed length of displayed text.
     */
    public void setMaxTextLength(int mtl) {
        maxTextLength = mtl;
    }

    /** Returns the font that has a size that corresponds to the distortion
     * that is described by the parameter <code>scale</code>.
     * @param scale The distortion factor.
     * @return The font with the correct size.
     */
    public Font getScaledFont(Point2D scale) {
        double maxScale = Math.max(scale.getX(), scale.getY());
        Font font = null;
        if (maxScale > scale1) {
            font = font1;
        }
        if (maxScale <= scale1 && maxScale > scale2) {
            font = font2;
        }
        if (maxScale <= scale2 && maxScale > scale3) {
            font = font3;
        }
        if (maxScale <= scale3) {
            font = font4;
        }
        return font;
    }

    public double getScaledFactor(Point2D scale) {
        double maxScale = Math.max(scale.getX(), scale.getY());
        double returnScale = 1;
        if (maxScale > scale1) {
            returnScale = 2.0;
        }
        if (maxScale <= scale1 && maxScale > scale2) {
            returnScale = 1.0;
        }
        if (maxScale <= scale2 && maxScale > scale3) {
            returnScale = 0.5;
        }
        if (maxScale <= scale3) {
            returnScale = 0.1;
        }
        return returnScale;
    }

    protected boolean isDelimiter(char ch) {
        return ch == ' ' || ch == ',' || ch == ';' || ch == '-' || ch == '/' || ch == '\\';
    }

    public void configure(ModelPanel mp, ModelPoint at, Node node) {
        this.drawAs = node.getInstance().getDrawAs();
        this.v = node.getInstance();
        modelPanel = mp;
        setColor(null);
        String ltext = node.getLabel();
        if (at == null || ltext == null) {
            useStubRenderer = true;
            return;
        }
        String l = (String) mp.getPropertyManager().getProperty("hypergraph.hyperbolic.text.maxLength");
        if (maxTextLength == -1 && l != null) {
            maxTextLength = Integer.parseInt(l);
        }
        if (font1 == null) {
            float fontSize;
            String fontName;
            fontName = mp.getPropertyManager().getString("hypergraph.hyperbolic.text.fontName");
            if (fontName == null) {
                fontName = "Arial";
            }
            Font font = new Font(fontName, Font.PLAIN, 1);
            fontSize = (float) mp.getPropertyManager().getDouble("hypergraph.hyperbolic.text.size1", new Double(12)).doubleValue();
            font1 = font.deriveFont(fontSize);
            fontSize = (float) mp.getPropertyManager().getDouble("hypergraph.hyperbolic.text.size2", new Double(10)).doubleValue();
            font2 = font.deriveFont(fontSize);
            fontSize = (float) mp.getPropertyManager().getDouble("hypergraph.hyperbolic.text.size3", new Double(8)).doubleValue();
            font3 = font.deriveFont(fontSize);
            fontSize = (float) mp.getPropertyManager().getDouble("hypergraph.hyperbolic.text.size4", new Double(0)).doubleValue();
            font4 = font.deriveFont(fontSize);
            scale1 = mp.getPropertyManager().getDouble("hypergraph.hyperbolic.text.scale1", new Double(0.75)).doubleValue();
            scale2 = mp.getPropertyManager().getDouble("hypergraph.hyperbolic.text.scale2", new Double(0.5)).doubleValue();
            scale3 = mp.getPropertyManager().getDouble("hypergraph.hyperbolic.text.scale3", new Double(0.25)).doubleValue();
        }
        Point p = mp.project(at);
        Point2D scale = mp.getScale(at);
        Font font = getScaledFont(scale);
        curScale = getScaledFactor(scale);
        if (font == null) {
            useStubRenderer = true;
            return;
        }
        useStubRenderer = false;
        setFont(font);

        // restrict the text to a maximum if requested.
        ltext = ltext.trim();
        if (maxTextLength >= 2 && ltext.length() > maxTextLength) {
            ltext = ltext.substring(0, maxTextLength - 2) + "..";
        }
        this.text = ltext;

        FontMetrics fm = getFontMetrics(getFont().deriveFont(Font.BOLD));
        double textWidth = SwingUtilities.computeStringWidth(fm, ltext);
        double height = fm.getHeight();

        double width = 0;
        lines.clear();
        // check whether the text fits in one line
        if (textWidth / height < optimalRatio) {
            // ok, one line seems to be enough
            lines.add(0, ltext);
            width = textWidth;
        } else {
            double area = textWidth * height;
            textWidth = Math.sqrt(area * optimalRatio);
            int prevPos = 0;
            double runningWidth = 0;
            StringBuffer buffer = new StringBuffer();
            for (int pos = 0; pos < ltext.length(); pos++) {
                char ch = ltext.charAt(pos);
                if (isDelimiter(ch)) {
                    String word;
                    word = ltext.substring(prevPos, pos + 1);
                    prevPos = pos + 1;
                    double wordWidth = SwingUtilities.computeStringWidth(fm, word);
                    if (wordWidth + runningWidth > textWidth) {
                        // the word doesn't fit in the current line,
                        // therefore save the previous and start a new one.
                        lines.add(buffer.toString().trim());
                        buffer.setLength(0);
                        runningWidth = 0;
                    }
                    buffer.append(word);
                    runningWidth += wordWidth;
                    if (runningWidth > width) {
                        width = runningWidth;
                    }
                }
            }
            // if there hasn't been any delimiter, the buffer is empty and the text is shown in one line.
            // otherwise, we have to check, whether the last word fits in the last line or not.
            if (buffer.length() == 0) {
                buffer.append(ltext);
                width = SwingUtilities.computeStringWidth(fm, ltext);
            } else {
                String word;
                word = ltext.substring(prevPos, ltext.length());
                double wordWidth = SwingUtilities.computeStringWidth(fm, word);
                if (wordWidth + runningWidth > textWidth) {
                    // the word doesn't fit in the current line,
                    // therefore save the previous and start a new one.
                    lines.add(buffer.toString().trim());
                    buffer.setLength(0);
                    runningWidth = 0;
                }
                buffer.append(word);
                runningWidth += wordWidth;
                if (runningWidth > width) {
                    width = runningWidth;
                }
            }
            lines.add(buffer.toString().trim());
        }
        height = fm.getHeight() * lines.size();
        
        if (v.isValid()) {
            Element element = ((GraphPanel)mp).getHoverElement();
            if (element != null && element.getElementType() == Element.NODE_ELEMENT) {
                if (((Node)element).equals(node)) {
                    if (v.isShowLabel()) {
                        toolTipLabel = v.toString();
                        toolTipImage = null;
                    }else if (v.isShowImage()) {
                        toolTipLabel = "";
                        try {
                            ImageCollection collection = ((NodeImpl)element).getModel().getImageCollection();
                            if (collection != null) {
                                Image im = collection.getImage(v.toString());// getUrl());
                                if (im != null) {
                                    toolTipImage = im.getScaledInstance(100, 100, 0);
                                } else {
                                    toolTipImage = null;
                                }
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                }else {
                    toolTipLabel = "";
                    toolTipImage = null;
                }
            }else {
                toolTipLabel = "";
                toolTipImage = null;
            }
        }
        
        switch (drawAs) {
            case 0: //Text/Points
                setSize(new Dimension((int)Math.round(width),(int)Math.round(height)));
                p.translate(-getWidth()/2,-getHeight()/2);
                break;
            case 2: //Images
                if (!v.isValid()) {
                    setSize(new Dimension((int)Math.round(v.getSize()*1.6*curScale+1),(int)Math.round(v.getSize()*1.6*curScale+1)));
                    p.translate(-(int)((v.getSize()*1.6*curScale)/2),-(int)((v.getSize()*1.6*curScale)/2));
                }else if (v.getImage() != null) {
                    int w = (int)(v.getImage().getWidth(null)*curScale);
                    int h = (int)(v.getImage().getHeight(null)*curScale);
                    if ((this.toolTipLabel != null)&&(!this.toolTipLabel.isEmpty()))
                        setSize(new Dimension((int)Math.round(width+(w/2)),(int)Math.round(height+(h/2)+5)));
                    else if (this.toolTipImage != null)
                        setSize(new Dimension(100+(int)Math.round(w/2),100+(int)Math.round(h/2)));
                    else
                        setSize(new Dimension((int)Math.round(w),(int)Math.round(h)));
                    p.translate(-(int)Math.round(w/2),-(int)Math.round(h/2));
                }
                break;
            default: //circles
                if (!v.isValid()) {
                    setSize(new Dimension((int)Math.round(v.getSize()*1.6*curScale+1),(int)Math.round(v.getSize()*1.6*curScale+1)));
                    p.translate(-(int)((v.getSize()*1.6*curScale)/2),-(int)((v.getSize()*1.6*curScale)/2));
                }else {
                    if ((this.toolTipLabel != null)&&(!this.toolTipLabel.isEmpty()))
                        setSize(new Dimension((int)Math.round(width+(v.getSize()*2*curScale)),(int)Math.round(height+(v.getSize()*2*curScale)+5)));
                    else if (this.toolTipImage != null) {
                        if (v.isValid() && v.getImage() != null)
                            setSize(new Dimension(100+(int)Math.round(v.getSize()*2*curScale),100+(int)Math.round(v.getSize()*2*curScale)));
                    }else setSize(new Dimension((int)Math.round(v.getSize()*4*curScale+1),(int)Math.round(v.getSize()*4*curScale+1)));
                    p.translate(-(int)((v.getSize()*4*curScale)/2),-(int)((v.getSize()*4*curScale)/2));
                }
        }
        setLocation(p);
    }

    public Color getColor() {
        if (color != null) {
            return color;
        }
        String colorString = modelPanel.getPropertyManager().getString("hypergraph.hyperbolic.text.color");
        return CSSColourParser.stringToColor(colorString);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public Color getBackground() {
        if (background != null) {
            return background;
        }
        String colorString = modelPanel.getPropertyManager().getString("hypergraph.hyperbolic.text.backgroundColor");
        if (colorString == null) {
            return null;
        }
        return CSSColourParser.stringToColor(colorString);
    }

    @Override
    public void setBackground(Color color) {
        this.background = color;
    }

    @Override
    public void paintComponent(Graphics g) {

        FontMetrics fm = getFontMetrics(getFont());
        int y = 0;
        switch (drawAs) {
            case 0: //Text/Points
                g.setColor(getColor());
                if (getBackground() != null) {
                    ((Graphics2D) g).setBackground(getBackground());
                    g.clearRect(0,0,getWidth(),getHeight());
                }
                for (Iterator iter = lines.iterator(); iter.hasNext();) {
                    String textL = (String)iter.next();
                    int textWidth = SwingUtilities.computeStringWidth(fm,textL);
                    y += fm.getAscent();
                    g.drawString(textL,(getWidth()-textWidth)/2,y);
                }
                break;
            case 2: //Images
                if (v.isValid()) {
                    if (v.getImage() != null) {
                        int w = (int)(v.getImage().getWidth(null)*curScale);
                        int h = (int)(v.getImage().getHeight(null)*curScale);
                        
                        g.drawImage(v.getImage(),0,0,w,h,null);
                        if (v.isDrawFrame()) {
                            ((Graphics2D) g).setStroke(new BasicStroke(4.0f));
                            ((Graphics2D) g).setColor(v.getColor());
                            ((Graphics2D) g).drawRect(0,0,w,h);
                            ((Graphics2D) g).setStroke(new BasicStroke(1.0f));
                        }
                        if (v.isSelected()) {
                            ((Graphics2D) g).setStroke(new BasicStroke(2.0f));
                            ((Graphics2D) g).setColor(Color.RED);
                            ((Graphics2D) g).drawRect(1,1,w-1,h-1);
                            ((Graphics2D) g).setStroke(new BasicStroke(1.0f));
                        }
                        
                        if ((this.toolTipLabel != null)&&(!this.toolTipLabel.isEmpty())) {
                            //Desenhando o texto...
                            g.setFont(getFont().deriveFont(Font.BOLD));
                            if (getBackground() != null) {
                                ((Graphics2D) g).setBackground(getBackground());
                                g.clearRect(0, 0, getWidth(), getHeight());
                            }
                            for (Iterator iter = lines.iterator(); iter.hasNext();) {
                                String textL = (String) iter.next();
                                int textWidth = SwingUtilities.computeStringWidth(fm, textL);
                                int textHeight = fm.getHeight() * lines.size();
                                y += fm.getAscent();
                                g.setColor(new Color(1.0f,1.0f,1.0f,0.75f));
                                g.fillRect(((getWidth()-textWidth)/2),1,textWidth+1,textHeight+3);
                                g.setColor(getColor());
                                g.drawRect(((getWidth()-textWidth)/2),1,textWidth+1,textHeight+3);
                                g.setColor(java.awt.Color.DARK_GRAY);
                                g.drawString(textL,(getWidth() - textWidth)/2+2,textHeight+1);
                                g.setColor(getColor());
                            }
                        }else if (toolTipImage != null) {
                            //Desenhando a imagem...
                            g.drawImage(toolTipImage.getScaledInstance(100,100,0),w/2,h/2, null);
                        }
                    }
                } else {
//                    Color c = g.getColor();
//                    g.setColor(Color.BLACK);
//                    g.drawRect(0,0,(int)(v.getSize()*1.3*curScale),(int)(v.getSize()*1.3*curScale));
//                    g.setColor(c);
                    g.setColor(Color.WHITE);
                    g.fillOval((int)(v.getSize()*1.6*curScale/2)-(int)(v.getSize()*curScale/2),
                               (int)(v.getSize()*1.6*curScale/2)-(int)(v.getSize()*curScale/2),
                               (int)(v.getSize()*curScale),
                               (int)(v.getSize()*curScale));
                    g.setColor(Color.LIGHT_GRAY);
                    g.drawOval((int)(v.getSize()*1.6*curScale/2)-(int)(v.getSize()*curScale/2),
                               (int)(v.getSize()*1.6*curScale/2)-(int)(v.getSize()*curScale/2),
                               (int)(v.getSize()*curScale),
                               (int)(v.getSize()*curScale));
                }
                break;
            default: //circles
                if (v.isValid()) {
                    Color nodeInteriorColor, nodeStrokeColor = null;
                    if (!globalsel || v.isSelected()) {
                        nodeInteriorColor = new Color(v.getColor().getRed(),v.getColor().getGreen(),v.getColor().getBlue(),255);
                        nodeStrokeColor = new Color(0,0,0,255);
                    }else {
                        nodeInteriorColor = new Color(v.getColor().getRed(),v.getColor().getGreen(),v.getColor().getBlue(),51);
                        nodeStrokeColor = new Color(0,0,0,51);
                    }
                    g.setColor(nodeInteriorColor);
                    g.fillOval(0,0,(int)(v.getSize()*4*curScale),(int)(v.getSize()*4*curScale));
                    g.setColor(nodeStrokeColor);
                    g.drawOval(0,0,(int)(v.getSize()*4*curScale),(int)(v.getSize()*4*curScale));
                    if ((this.toolTipLabel != null)&&(!this.toolTipLabel.isEmpty())) {
                        //Desenhando o texto... 
                        g.setFont(getFont().deriveFont(Font.BOLD));
                        if (getBackground() != null) {
                            ((Graphics2D) g).setBackground(getBackground());
                            g.clearRect(0, 0, getWidth(), getHeight());
                        }
                        for (Iterator iter = lines.iterator(); iter.hasNext();) {
                            String textL = (String) iter.next();
                            int textWidth = SwingUtilities.computeStringWidth(fm, textL);
                            int textHeight = fm.getHeight() * lines.size();
                            y += fm.getAscent();
                            g.setColor(new Color(1.0f,1.0f,1.0f,0.75f));
                            g.fillRect(((getWidth()-textWidth)/2),1,textWidth+1,textHeight+3);
                            g.setColor(getColor());
                            g.drawRect(((getWidth()-textWidth)/2),1,textWidth+1,textHeight+3);
                            g.setColor(java.awt.Color.DARK_GRAY);
                            g.drawString(textL,(getWidth()-textWidth)/2+2,textHeight+1);
                            g.setColor(getColor());
                        }
                    }else if (toolTipImage != null) {
                        //Desenhando a imagem...
                        g.drawImage(toolTipImage.getScaledInstance(100,100,0),
                                    (int)Math.round(v.getSize()*2*curScale),(int)Math.round(v.getSize()*2*curScale),null);
                    }
                } else {
//                    Color c = g.getColor();
//                    g.setColor(Color.BLACK);
//                    g.drawRect(0,0,(int)(v.getSize()*1.3*curScale),(int)(v.getSize()*1.3*curScale));
//                    g.setColor(c);
                    g.setColor(Color.WHITE);
                    g.fillOval((int)(v.getSize()*1.6*curScale/2)-(int)(v.getSize()*curScale/2),
                               (int)(v.getSize()*1.6*curScale/2)-(int)(v.getSize()*curScale/2),
                               (int)(v.getSize()*curScale),
                               (int)(v.getSize()*curScale));
                    g.setColor(Color.LIGHT_GRAY);
                    g.drawOval((int)(v.getSize()*1.6*curScale/2)-(int)(v.getSize()*curScale/2),
                               (int)(v.getSize()*1.6*curScale/2)-(int)(v.getSize()*curScale/2),
                               (int)(v.getSize()*curScale),
                               (int)(v.getSize()*curScale));
                }
        }
    }

    public Component getComponent() {
        if (useStubRenderer) {
            return stubRenderer;
        }

        return this;
    }

    @Override
    public void setGlobalSel(boolean value) {
        this.globalsel = value;
    }

    @Override
    public boolean getGlobalSel() {
        return this.globalsel;
    }

    public double getCurScale() {
        return curScale;
    }
}
