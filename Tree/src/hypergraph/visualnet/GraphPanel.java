/*
 *  Copyright (C) 2003  Jens Kanschik,
 * 	mail : jensKanschik@users.sourceforge.net
 *
 *  Part of <hypergraph>, an open source project at sourceforge.net
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package hypergraph.visualnet;

import hypergraph.graph.GraphImpl;
import hypergraph.graph.NodeImpl;
import hypergraph.graphApi.AttributeManager;
import hypergraph.graphApi.Edge;
import hypergraph.graphApi.Element;
import hypergraph.graphApi.Graph;
import hypergraph.graphApi.GraphException;
import hypergraph.graphApi.Node;
import hypergraph.graphApi.io.CSSColourParser;
import hypergraph.hyperbolic.Complex;
import hypergraph.hyperbolic.DefaultTextRenderer;
import hypergraph.hyperbolic.ModelPanel;
import hypergraph.hyperbolic.ModelPoint;
import hypergraph.hyperbolic.TextRenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import tree.model.TreeModel;
import tree.model.TreeInstance;

/**
 * @author Jens Kanschik
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class GraphPanel extends ModelPanel implements MouseListener, GraphLayoutListener, GraphSelectionListener {

    /**
     * The attribute name for foreground colour of nodes.
     */
    public static final String NODE_FOREGROUND = "node.color";
    /**
     * The attribute name for background colour of nodes.
     */
    public static final String NODE_BACKGROUND = "node.bkcolor";
    /**
     * The attribute name for an icon of a node.
     */
    public static final String NODE_ICON = "node.icon";
    /**
     * The attribute name for foreground colour of edges.
     */
    public static final String EDGE_TEXTCOLOR = "edge.textcolor";
    public static final String EDGE_LINECOLOR = "edge.linecolor";
    public static final String EDGE_LINEWIDTH = "edge.linewidth";
    public static final String EDGE_STROKE = "edge.stroke";
    public static final String NODE_EXPANDED = "NODE_EXPANDED";
    public final ExpandAction expandAction = new ExpandAction(true);
    public final ExpandAction shrinkAction = new ExpandAction(false);
    Graph graph;
    ArrayList<GraphLayout> graphLayout = new ArrayList<GraphLayout>();
    GraphSelectionModel selectionModel;
    private Image logo;
    private Image smallLogo;
    private Element hoverElement;
    private Node lastMouseClickNode;
    private NodeRenderer nodeRenderer;
    private EdgeRenderer edgeRenderer;
    public java.awt.Polygon polygon;
    public java.awt.Color color = java.awt.Color.RED;

    public GraphPanel(Graph graph, double distanceHyper) {
        super();
        setGraph(graph);
//		initVisibleGraph();
        createGraphLayout(distanceHyper);
        createGraphSelectionModel();
        selectionModel.addSelectionEventListener(this);
        setNodeRenderer(new DefaultNodeRenderer());
        setEdgeRenderer(new DefaultEdgeRenderer());
        initDefaultAttributes();

        // read the logo for small and large applets :
        URL logoUrl = this.getClass().getResource("/hypergraph/visualnet/logo.png");
        if (logoUrl != null) {
            logo = Toolkit.getDefaultToolkit().createImage(logoUrl);
        }
        logoUrl = this.getClass().getResource("/hypergraph/visualnet/logo_small.png");
        if (logoUrl != null) {
            smallLogo = Toolkit.getDefaultToolkit().createImage(logoUrl);
        }
    }

    protected void initDefaultAttributes() {
        AttributeManager amgr = graph.getAttributeManager();

        String colorString = getPropertyManager().getString("hypergraph.hyperbolic.line.color");
        Color colorP = CSSColourParser.stringToColor(colorString);
        if (colorP == null || colorString == null) {
//            color = Color.LIGHT_GRAY;
            colorP = Color.BLACK;
        }
        amgr.setAttribute(EDGE_LINECOLOR, graph, colorP);
        amgr.setAttribute(EDGE_STROKE, graph, null);
        amgr.setAttribute(EDGE_LINEWIDTH, graph, new Float(1));
    }

    public boolean isSelected(Node node) {
        ArrayList<TreeInstance> selected = graph.getSelectedInstances();
        if ((selected != null)&&(selected.size() > 0)) {
            for (int i=0;i<selected.size();i++) {
                if (selected.get(i).getId() == node.getInstance().getId())
                    return true;
            }
            return false;
        } else return false; //else return true; //ATENCAO
    }
    
    public void createGraphLayout(double distanceHyper) {
        ArrayList<GraphLayout> layout = new ArrayList<GraphLayout>();
        GraphLayout lyt;
        try {
            Class layoutClass = getPropertyManager().getClass("hypergraph.visualnet.layout.class");
            lyt = (GraphLayout) layoutClass.newInstance();
            layout.add(lyt);
        } catch (Exception e) {
            //ALTERADO POR JOSE GUSTAVO
            ArrayList<Graph> spts = getGraph().getSpanningTree();
            if (spts != null) {
                Graph gr = (Graph)((GraphImpl)getGraph());
                if (gr.getNodes() != null) {
                    Iterator itr = gr.getNodes().iterator();
                    NodeImpl n = (NodeImpl) itr.next();
                    if (n != null) {
                        TreeModel tm = n.getModel();
                        if (tm != null) {
                                for (int i=0;i<spts.size();i++) {
                                //Adjusting layouts according to the corresponding spanning trees...
                                Graph g = (Graph)((GraphImpl)getGraph()).clone(spts.get(i),tm);
                                Iterator it = null;
                                for (int k=0;k<g.getEdges().size();k++) {
                                    Edge edge = (Edge)g.getEdges().toArray()[k];
                                    if (!spts.get(i).getEdges().contains(edge)) {
                                        g.removeElement(edge);
                                        k--;
                                    }
                                }
                                for (int k=0;k<g.getNodes().size();k++) {
                                    Node node = (Node)g.getNodes().toArray()[k];
                                    if (!spts.get(i).getNodes().contains(node)) {
                                        g.removeElement(node);
                                        k--;
                                    }
                                }
                                lyt = new TreeLayout(g, getModel(), i, getPropertyManager(), distanceHyper);
                                layout.add(lyt);
                            }

                        }
                    }
                }
            }
            //FIM DA ALTERACAO
        }
        setGraphLayout(layout);
    }

    @Override
    public void loadProperties(InputStream is) throws IOException {
        super.loadProperties(is);
        createGraphLayout(0.3); //VALOR FIXO
        initDefaultAttributes();
    }

    public void setGraphLayout(ArrayList<GraphLayout> layout) {
        if (graphLayout != null) {
            for (int i=0;i<graphLayout.size();i++)
                graphLayout.get(i).getGraphLayoutModel().removeLayoutEventListener(this);
        }

        graphLayout = layout;
    }

    public ArrayList<GraphLayout> getGraphLayout() {
        return graphLayout;
    }

    void createGraphSelectionModel() {
        setGraphSelectionModel(new DefaultGraphSelectionModel(getGraph()));
    }

    public void setGraphSelectionModel(GraphSelectionModel gsm) {
        selectionModel = gsm;
    }

    public GraphSelectionModel getSelectionModel() {
        return selectionModel;
    }

    public void setGraph(Graph g) {
        graph = g;
        if (graphLayout != null) {
            for (int i=0;i<graphLayout.size();i++)
                graphLayout.get(i).setGraph(graph);
        }
    }

    public Graph getGraph() {
        return graph;
    }

    @Override
    public void valueChanged(GraphLayoutEvent e) {
        repaint();
    }

    protected void checkLayout() {
        for (int i=0;i<graphLayout.size();i++)
            if (!graphLayout.get(i).isValid()) {
//                if (c == null) {
//                    Node root = ((GraphImpl)graphLayout.get(i).getGraph().getSpanningTree().get(0)).getMinInDegree();
//                    if (root != null) {
//                        c = new Complex(root.getVertex().getX(),root.getVertex().getY());
//                    }
//                }
//                if (c != null) graphLayout.get(i).layout(c);
                graphLayout.get(i).layout();
            }
    }

    public Iterator getVisibleNodeIterator(GraphLayout gl) {
        if (gl != null)
            return gl.getGraph().getNodes().iterator();
        else return null;
    }

    public Iterator getVisibleEdgeIterator(GraphLayout gl) {
        if (gl != null)
            return gl.getGraph().getEdges().iterator();
        else return null;
    }

    @Override
    public void paintComponent(Graphics g) {

        synchronized (graph) {
            checkLayout();
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setColor(Color.WHITE); // vamos pintar o fundo de branco
            g2.fillRect(0, 0, getWidth(), getHeight()); // desenhamos o ret ngulo

            if (getUI().isDraft()) {
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_OFF);
                g2.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_SPEED);
                g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
            } else {
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
            }

            super.paintComponent(g2);
            
            GraphLayoutModel glm;
            ArrayList<GraphLayout> lm = getGraphLayout();

            if (lm != null) {
                for (int k=0;k<lm.size();k++) {
                    glm = lm.get(k).getGraphLayoutModel();
                    for (Iterator i = getVisibleEdgeIterator(lm.get(k)); i.hasNext();) {
                        Edge edge = (Edge) i.next();
                        if (edge != hoverElement) {
                            edgeRenderer.configure(this, edge);
                            paintRenderer(g2, edgeRenderer);
                        }
                    }

                    if (hoverElement != null) {
                        if (hoverElement.getElementType() == Element.EDGE_ELEMENT) {
                            edgeRenderer.configure(this, (Edge) hoverElement);
                            paintRenderer(g2, edgeRenderer);
                        }
                        if (hoverElement.getElementType() == Element.NODE_ELEMENT) {
                            GraphLayout gl = getGraphLayout((Node)hoverElement);
                            ModelPoint mp = gl.getGraphLayoutModel().getNodePosition((Node) hoverElement);
                            nodeRenderer.configure(this, mp, (Node) hoverElement);
                            paintRenderer(g2, nodeRenderer);
                        }
                    }

                    nodeRenderer.setGlobalSel(false);
                    edgeRenderer.setGlobalSel(false);
                    for (Iterator i = getVisibleNodeIterator(lm.get(k)); i.hasNext();) {
                        Node node = (Node) i.next();
                        if (isSelected(node)) {
                            nodeRenderer.setGlobalSel(true);
                            edgeRenderer.setGlobalSel(true);
                        }
                    }

                    for (Iterator i = getVisibleNodeIterator(lm.get(k)); i.hasNext();) {
                        Node node = (Node) i.next();
                        if (node != hoverElement) {
                            ModelPoint mp = glm.getNodePosition(node);
                            nodeRenderer.configure(this, mp, node);
                            //ALTERADO POR JOSE GUSTAVO - 04/03/2010
//                            if (!isSelected(node))
//                                node.getVertex().setSelected(false);
//                            else
//                                node.getVertex().setSelected(true);
                            paintRenderer(g2, nodeRenderer);
                            //FIM DA ALTERACAO
                        }
                    }

                    if (hoverElement != null) {
//                        if (hoverElement.getElementType() == Element.EDGE_ELEMENT) {
//                            edgeRenderer.configure(this, (Edge) hoverElement);
//                            paintRenderer(g2, edgeRenderer);
//                        }
                        if (hoverElement.getElementType() == Element.NODE_ELEMENT) {
                            GraphLayout gl = getGraphLayout((Node)hoverElement);
                            ModelPoint mp = gl.getGraphLayoutModel().getNodePosition((Node) hoverElement);
                            nodeRenderer.configure(this, mp, (Node) hoverElement);
                            paintRenderer(g2, nodeRenderer);
                        }
                    }
                }
            }
            if (this.polygon != null) {
                g2.setColor(this.color);
                g2.drawPolygon(this.polygon);
                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.45f));
                g2.setPaint(this.color);
                g2.fillPolygon(this.polygon);
            }
            g2.dispose();
        }
        if (getWidth() > 300 && getHeight() > 300) {
            if (logo != null) {
                g.drawImage(logo, getWidth() - logo.getWidth(this),
                        getHeight() - logo.getHeight(this), this);
            }
        } else {
            if (smallLogo != null) {
                g.drawImage(smallLogo, getWidth() - smallLogo.getWidth(this),
                        getHeight() - smallLogo.getHeight(this), this);
            }
        }


    }

    /**
     * @return
     */
    public Element getHoverElement() {
        return hoverElement;
    }

    protected void setHoverElement(Element element, boolean repaint) {
        hoverElement = element;
        if (repaint) {
            repaint();
        }
    }

    /** Sets the standard version of the logo.
     * @param l The logo.
     */
    public void setLogo(Image l) {
        logo = l;
    }

    /** Sets a small version of the logo.
     * This is shown if the graph panel is so small
     * that the default logo would overlap with the data that is shown.
     * @param l The small logo.
     */
    public void setSmallLogo(Image l) {
        smallLogo = l;
    }

    /** Returns <code>true</code> if the point is contained in the logo.
     * @param p A location within the graph panel,
     * the coordinates are with respect to the graph panel.
     * @return <code>True</code> if the specified point on in the logo.*/
    protected boolean isOnLogo(Point p) {
        if (logo != null) {
            return p.getX() > getWidth() - logo.getWidth(this) &&
                    p.getY() > getHeight() - logo.getHeight(this);
        } else {
            return false;
        }
    }

    /** This method is called when the user clicked on the logo.
     * @param event The mouse event that repesents the mouse click on the logo. */
    protected void logoClicked(MouseEvent event) {
    }

    protected GraphLayout getGraphLayout(Node node) {
        ArrayList<GraphLayout> layouts = getGraphLayout();
        GraphLayout lt = null;
        if (layouts != null) {
            for (int i=0;i<layouts.size();i++) {
                Iterator it = layouts.get(i).getGraph().getNodes().iterator();
                while (it.hasNext()) {
                    if (((Node)it.next()).equals(node)) {
                        lt = layouts.get(i);
                        break;
                    }
                }
                if (lt != null) break;
            }
        }
        return lt;
    }

    public boolean hasExpander(Node node) {
        GraphLayout lt = getGraphLayout(node);
        if (lt != null) {
            if (!lt.isExpandingEnabled()) return false;
            AttributeManager amgr = lt.getGraph().getAttributeManager();
            ExpandAction action = (ExpandAction) amgr.getAttribute(NODE_EXPANDED, node);
            return action != null;
        }else return false;
    }

    public boolean isExpanded(Node node) {
        GraphLayout lt = getGraphLayout(node);
        if (lt != null) {
            AttributeManager amgr = lt.getGraph().getAttributeManager();
            ExpandAction action = (ExpandAction) amgr.getAttribute(NODE_EXPANDED, node);
            if (action == null) {
                return false;
            }
            return action == shrinkAction;
        }else return false;
    }

    public void expandNode(Node node) {
        GraphLayout lt = getGraphLayout(node);
        if (lt != null) {
            AttributeManager amgr = lt.getGraph().getAttributeManager();
            // get all outgoing edges in the original (!) graph.
            Iterator iter = graph.getEdges(node).iterator();
            while (iter.hasNext()) {
                Edge edge = (Edge) iter.next();
                if (edge.getSource() != node) {
                    continue; // only outgoing edges
                }
                try {
                    lt.getGraph().addElement(edge);
                } catch (GraphException ge) {
                }
                Node target = edge.getOtherNode(node);
                if (graph.getEdges(target).size() != 0) {
                    amgr.setAttribute(NODE_EXPANDED, target, expandAction);
                }
            }
            amgr.setAttribute(NODE_EXPANDED, node, shrinkAction);
        }
    }

    public void shrinkNode(Node node) {
        GraphLayout lt = getGraphLayout(node);
        if (lt != null) {
            AttributeManager amgr = lt.getGraph().getAttributeManager();
            // get all outgoing edges in the original (!) graph.
            Iterator iter = graph.getEdges(node).iterator();
            while (iter.hasNext()) {
                Edge edge = (Edge) iter.next();
                if (edge.getSource() != node) {
                    continue; // only outgoing edges
                }
                Node target = edge.getOtherNode(node);
                lt.getGraph().removeElement(target);
            }
            amgr.setAttribute(NODE_EXPANDED, node, expandAction);
        }
    }

    /** Centers the given node.
     * @param node The node that has to be moved to the center.
     */
    public void centerNode(Node node) {
        GraphLayout lt = getGraphLayout(node);
        if (lt != null) {
            GraphLayoutModel glm = lt.getGraphLayoutModel();
            getUI().center(glm.getNodePosition(node), this);
        }
    }

    /** Returns the element at the position <code>point</code>.
     * This can be either a node or an edge.
     * If a node and an edge are at the same position, the node is returned.
     *
     * @param point The position of the element
     * @return The element that is located at the position <code>point</code>.
     */
    public Element getElement(Point point) {
        ArrayList<GraphLayout> layouts = getGraphLayout();
        if (layouts != null) {
            //ALTERADO POR JOSE GUSTAVO
            for (int k=0;k<layouts.size();k++) {
                GraphLayoutModel glm = layouts.get(k).getGraphLayoutModel();
                // make sure that neither the graph nor the graph layout are changed while getting the nearest node.
                synchronized (graph) {
                    synchronized (glm) {
                        // check nodes first
                        NodeRenderer nr = getNodeRenderer();
                        Point p = new Point();
                        for (Iterator i = getVisibleNodeIterator(layouts.get(k)); i.hasNext();) {
                            Node node = (Node) i.next();
                            nr.configure(this, glm.getNodePosition(node), node);
                            Component c = nr.getComponent();

                            if (nr instanceof DefaultNodeRenderer) {
                                TextRenderer tr = ((DefaultNodeRenderer)nr).getTextRenderer();
                                if ((tr != null)&&(tr instanceof DefaultTextRenderer)) {
                                    int radius = (int)(node.getInstance().getSize() * 2 * ((DefaultTextRenderer) tr).getCurScale());
                                    if (isInside((int)point.getX(),(int)point.getY(),c,radius)) {
                                        return node;
                                    }
                                }else {
                                    p.setLocation(point.getX() - c.getX(), point.getY() - c.getY());
                                    if (c.contains(p)) {
                                        return node;
                                    }
                                }
                            }else {
                                p.setLocation(point.getX() - c.getX(), point.getY() - c.getY());
                                if (c.contains(p)) {
                                    return node;
                                }
                            }

                        }
                        // now check edges
                        for (Iterator i = getVisibleEdgeIterator(layouts.get(k)); i.hasNext();) {
                            Edge edge = (Edge) i.next();
                            ModelPoint mp1 = glm.getNodePosition(edge.getSource());
                            ModelPoint mp2 = glm.getNodePosition(edge.getTarget());
                            //ALTERADO POR JOSE GUSTAVO - 10/03/2010
                            if ((mp1 != null)&&(mp2 != null)) {
                                ModelPoint mp3 = unProject(point);
                                if (mp3 != null && getModel().getDistance(mp3, mp1, mp2, true, true) < 0.025) {
                                    return edge;
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public boolean isInside(int x, int y, Component c, int radius) {
        return (Math.sqrt(Math.pow(x - (c.getX()+radius), 2) + Math.pow(y - (c.getY()+radius), 2)) <= radius);
    }

    /** Is called when the user clicked on a node.
     *  The default implementation centers the node if it is clicked once. */
    public void nodeClicked(int clickCount, Node node) {
        if (clickCount == 1) {
            lastMouseClickNode = node;
            if (lastMouseClickNode != null) {
                centerNode(lastMouseClickNode);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
       if (e.getButton() == java.awt.event.MouseEvent.BUTTON1) {
           if (isOnLogo(e.getPoint())) {
                logoClicked(e);
                return;
            }
            setHoverElement(null, false);
            Element element = getElement(e.getPoint());
            if (element != null && element.getElementType() == Element.NODE_ELEMENT) {
                if (e.getClickCount() == 1) {
                    nodeClicked(1, (Node) element);
                    return;
                }
                if (e.getClickCount() == 2 && lastMouseClickNode != null) {
                    nodeClicked(2, lastMouseClickNode);
                    return;
                }
                NodeRenderer nr = getNodeRenderer();
                GraphLayout lt = getGraphLayout((Node)element);
                if (lt != null) {
                    GraphLayoutModel glm = lt.getGraphLayoutModel();
                    nr.configure(this,glm.getNodePosition((Node)element),(Node)element);
                }
            }
       //}else if (e.getButton() == java.awt.event.MouseEvent.BUTTON3) {
            if (element != null) super.mouseClicked(e);
       }
       //if (element != null)
       //    super.mouseClicked(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Element element = getElement(e.getPoint());
        setHoverElement(element, true);
    }

    @Override
    public void valueChanged(GraphSelectionEvent e) {
        Iterator i = getSelectionModel().getSelectionElementIterator();
        repaint();
    }

    public class ExpandAction implements ActionListener {

        private boolean expand;

        public ExpandAction(boolean expand) {
            this.expand = expand;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Node node = (Node) e.getSource();
            if (expand) {
                expandNode(node);
            } else {
                shrinkNode(node);
            }
            repaint();
        }
    }

    /**
     * @return
     */
    public EdgeRenderer getEdgeRenderer() {
        return edgeRenderer;
    }

    /**
     * @param renderer
     */
    public void setEdgeRenderer(EdgeRenderer renderer) {
        edgeRenderer = renderer;
    }

    public void setNodeRenderer(NodeRenderer nodeRenderer) {
        this.nodeRenderer = nodeRenderer;
    }

    public NodeRenderer getNodeRenderer() {
        return nodeRenderer;
    }
}
