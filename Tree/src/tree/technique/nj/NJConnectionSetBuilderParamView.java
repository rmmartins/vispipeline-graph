/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.technique.nj;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import textprocessing.util.filter.ZIPFilter;
import tree.basics.TreeConstants;
import visualizationbasics.util.OpenDialog;
import visualizationbasics.util.PropertiesManager;

/**
 *
 * @author Jose Gustavo
 */
public class NJConnectionSetBuilderParamView extends NJConnectionParamView {

    public NJConnectionSetBuilderParamView(NJConnectionComp comp) {
        super(comp);
        initComponents();
        reset();
    }

    private void initComponents() {

        //hiding Dissimilarity combobox...
        dissimilarityPanel.setVisible(false);

        java.awt.GridBagConstraints gridBagConstraints;

        collectionPanel = new javax.swing.JPanel();
        matrixLabel = new javax.swing.JLabel();
        collectionTextField = new javax.swing.JTextField();
        collectionButton = new javax.swing.JButton();

        //setBorder(javax.swing.BorderFactory.createTitledBorder("Neighbor Joining Connection Parameters"));
        //setLayout(new java.awt.GridBagLayout());

        collectionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Collection"));
        collectionPanel.setLayout(new java.awt.GridBagLayout());

        matrixLabel.setText("File name");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        collectionPanel.add(matrixLabel, gridBagConstraints);

        collectionTextField.setColumns(35);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        collectionPanel.add(collectionTextField, gridBagConstraints);

        collectionButton.setText("Search...");
        collectionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                collectionButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        collectionPanel.add(collectionButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(collectionPanel, gridBagConstraints);
    }

    private void collectionButtonActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            PropertiesManager spm = PropertiesManager.getInstance(TreeConstants.PROPFILENAME);
            int result = OpenDialog.showOpenDialog(spm, new ZIPFilter(), this);
            if (result == JFileChooser.APPROVE_OPTION) {
                String filename = OpenDialog.getFilename();
                collectionTextField.setText(filename);
            }
        } catch (IOException ex) {
            Logger.getLogger(NJConnectionSetBuilderParamView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void reset() {
        super.reset();
        if (((NJConnectionSetBuilderComp)comp).getCollection() != null)
            collectionTextField.setText(((NJConnectionSetBuilderComp)comp).getCollection());
    }

    @Override
    public void finished() throws IOException {
        super.finished();
        if (!collectionTextField.getText().isEmpty())
            ((NJConnectionSetBuilderComp)comp).setCollection(collectionTextField.getText());
    }

    //private NJConnectionSetBuilderComp comp;
    private javax.swing.JButton collectionButton;
    private javax.swing.JPanel collectionPanel;
    private javax.swing.JTextField collectionTextField;
    private javax.swing.JLabel matrixLabel;

}
