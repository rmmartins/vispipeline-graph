package tree.technique.nj;

import distance.DetailedDistanceMatrix;
import distance.DistanceMatrix;
import java.io.IOException;
import distance.dissimilarity.AbstractDissimilarity;
import tree.model.TreeConnectivity;
import graph.technique.connection.GraphConnection;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import tree.basics.TreeConstants;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class NJConnection extends GraphConnection {

    public NJConnection(Boolean pnj) {
        super();
        this.pnj = pnj;
    }

    public TreeConnectivity execute(AbstractMatrix matrix, AbstractDissimilarity diss) throws IOException {
        DistanceMatrix dmat = new DetailedDistanceMatrix(matrix, diss);
        return execute(dmat);
    }

    @Override
    public TreeConnectivity execute(DistanceMatrix dmat) throws IOException {

        this.neighbor = new NeighborJoining(dmat);
        this.neighbor.createTree(pnj);
        this.neighbor.getTree().generateEdges();
        ArrayList<String> labels = dmat.getLabels();
        if (!labels.isEmpty()) assignLabels(labels);

        TreeConnectivity conn = new TreeConnectivity(TreeConstants.NJ, this.getNeighbor().getTree().getEdges());
        return conn;
    }

    public NeighborJoining getNeighbor() {
        return neighbor;
    }

    public void assignLabels(ArrayList<String> labels) {
        if (labels.size() <= neighbor.getTree().getSize()) {
            for (int i=0;i<labels.size();i++) {
                neighbor.getTree().getNode(i).setLabel(labels.get(i));
            }
        }
    }

    private NeighborJoining neighbor;
    private Boolean pnj;


}
