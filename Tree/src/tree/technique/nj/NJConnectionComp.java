package tree.technique.nj;

import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import tree.model.TreeConnectivity;
import java.io.IOException;
import matrix.AbstractMatrix;
import tree.basics.Tree;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.Technique",
name = "Neighbor-Joinning connection",
description = "Project points from a multidimensional space to the plane " +
"preserving the neighborhood relations.")
public class NJConnectionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        //projecting
        NJConnection nj = new NJConnection(pnj);
        if (matrix != null) { //using a points matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            conn = nj.execute(matrix, diss);
            tree = nj.getNeighbor().getTree();
        } else if (dmat != null) { //using a distance matrix
            //System.out.println("ORIGINAL");
            //dmat.printIDLabels();
            conn = nj.execute(dmat);
            tree = nj.getNeighbor().getTree();
        } else {
            throw new IOException("A distance matrix or a points matrix should " +
                    "be provided.");
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "distance matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public Tree outputTree() {
        return tree;
    }

    public TreeConnectivity output() {
        return conn;
    }
    
    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new NJConnectionParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        conn = null;
        matrix = null;
        dmat = null;
    }

    /**
     * @return the disstype
     */
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    /**
     * @param disstype the disstype to set
     */
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public boolean isDistanceMatrixProvided() {
        return (dmat != null);
    }

    public Boolean isPnj() {
        if (pnj != null) return pnj;
        else return false;
    }

    public void setPnj(Boolean pnj) {
        this.pnj = pnj;
    }

    public static final long serialVersionUID = 1L;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private Boolean pnj = false;
    private transient NJConnectionParamView paramview;
    private transient Tree tree;
    private transient TreeConnectivity conn;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
}
