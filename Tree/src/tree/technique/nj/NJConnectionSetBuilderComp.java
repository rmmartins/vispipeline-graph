package tree.technique.nj;

import distance.DistanceMatrix;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import tree.layout.radial.RadialLayoutComp;
import tree.model.TreeModel;
import tree.model.TreeModelComp;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.Technique",
name = "NJ Connection Set Builder",
description = "Builds several NJs, for each distance matrix provided.")
public class NJConnectionSetBuilderComp extends NJConnectionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {

        if ((dmats == null)||(dmats.isEmpty()))
                throw new IOException("A distance matrix set should be provided.");

        //Initializing components that will execute the process..
        NJConnectionComp njc = new NJConnectionComp();
        njc.setPnj(this.isPnj());
        TreeModelComp tmc = new TreeModelComp();
        tmc.setCollection(collection);
        RadialLayoutComp radialc = new RadialLayoutComp();

        //Building the models...
        models = new ArrayList<TreeModel>();
        for (int i=0;i<dmats.size();i++) models.add(null);
        
        Iterator it = dmats.entrySet().iterator();
        String chave = "";
        DistanceMatrix dm = null;
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            chave = (String) e.getKey();
            dm = (DistanceMatrix)e.getValue();
            //executing nj component with each distance matrix...
            njc.input(dm);
            njc.execute();
            //creating tree model with tree produced by nj connection component...
            tmc.input(njc.outputTree());
            tmc.execute();
            //applying radial layout to produced models...
            radialc.input(tmc.output());
            radialc.execute();
            TreeModel m = radialc.output();
            m.setSource(chave.substring(chave.indexOf("-")+1).trim());
            int pos = Integer.parseInt(chave.substring(0,chave.indexOf("-")).trim());
            models.set(pos, m);
        }
    }

    public void input(@Param(name = "distance matrices") HashMap<String,DistanceMatrix> dmats) {
        this.dmats = dmats;
    }

    public void attach(@Param(name = "distance matrix") DistanceMatrix dmat) {
        if (dmats == null)
            dmats = new HashMap<String,DistanceMatrix>();
        if (dmats != null) {
            dmats.put(Integer.toString(dmats.size())+"-Dmat "+Integer.toString(dmats.size()),dmat);
        }
    }

    public ArrayList<TreeModel> outputModels() {
        return models;
    }
    
    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new NJConnectionSetBuilderParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        dmats = null;
    }

    public void setCollection(String c) {
        this.collection = c;
    }

    public String getCollection() {
        return collection;
    }

    public static final long serialVersionUID = 1L;
    private transient NJConnectionSetBuilderParamView paramview;
    private transient ArrayList<TreeModel> models;
    private transient HashMap<String,DistanceMatrix> dmats;
    private transient String collection;
}
