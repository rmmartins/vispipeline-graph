package tree.technique.nj;

import distance.DetailedDistanceMatrix;
import tree.basics.ContentTree;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import distance.DistanceMatrix;
import tree.basics.Tree;
import tree.basics.TreeConstants;

public class NeighborJoining {

    DistanceMatrix originalDmat,dmat;
    private int min_x,min_y;
    private Tree tree;
    private float divergence[];
    private float [][] newDmat;
    private int maxId;
    private int numberClusters;
    private long init, end;
    
    public void load() {
        numberClusters = originalDmat.getElementCount();
        tree = new Tree();
        tree.setType(TreeConstants.NJ);
        try {
            dmat = (DistanceMatrix) originalDmat.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(NeighborJoining.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList ids = dmat.getIds();
        if (ids != null)
            maxId = (Integer)ids.get(0);
        else
            maxId = 0;
        float[] cdatas = dmat.getClassData();
        for (int i=0;i<dmat.getElementCount();i++) {
            tree.addNode(new ContentTree((Integer)ids.get(i),Integer.toString(i),cdatas[i]));
            if ((Integer)ids.get(i) > maxId)
                maxId = (Integer)ids.get(i);
        }
        this.numberClusters = dmat.getElementCount();
    }
  
    public NeighborJoining(DistanceMatrix dmat) {
        originalDmat = dmat;
        load();
    }

    public NeighborJoining(String filename) throws IOException {
        originalDmat = new DetailedDistanceMatrix(filename);
        load();
    }

    //Construtor utilizado para a SNJ, para finalizar os nos da arvore sem nos virtuais.
    public NeighborJoining(Tree tree, DistanceMatrix dmat) throws IOException {
        this.tree = tree;
        this.dmat = dmat;
        numberClusters = this.dmat.getElementCount();
    }

    public void createTree(Boolean pnj) {

        ContentTree junctionNode;
        float[][] newDmatTemp;

        init = System.currentTimeMillis();

        do {
            //System.out.println("Calculando divergencias - iteracao "+numberClusters+"...");
            calculateDivergence();
            //System.out.println("Calculando newDistanceMatrix - iteracao "+numberClusters+"...");
            calculateNewDistanceMatrix();
            //System.out.println("Calculando smallestValue - iteracao "+numberClusters+"...");
            calculateSmallestValue(newDmat);
            //System.out.println("Setando JunctionNode - iteracao "+numberClusters+"...");
            junctionNode = createJunctionNode(this.dmat,this.divergence);
            tree.addNode(junctionNode);
            //Alterando a matrix de distancias considerando o no com o agrupamento...
            //System.out.println("Recalculando DistanceMatrix - iteracao "+numberClusters+"...");
            newDmatTemp = recalculateDistanceMatrix();
            this.dmat.setDistmatrix(newDmatTemp);
            ArrayList<Integer> ids = this.dmat.getIds();
            if (min_y > min_x) { //Devo retirar sempre o maior indice primeiro, porque o metodo faz um shift dos elementos subsequentes, o que atrapalhara a retirada do proximo elemento.
                ids.remove(min_y);
                ids.remove(min_x);
            }else {
                ids.remove(min_x);
                ids.remove(min_y);
            }
            if ((this.min_x == this.dmat.getElementCount()-1)||(this.min_y == this.dmat.getElementCount()-1))//Neste caso especial, um dos itens escolhidos para ser agrupado eh o ultimo item da lista.
                ids.add(junctionNode.getId());
            else
                ids.add(0,junctionNode.getId());
            this.dmat.setIds(ids);
            this.dmat.setElementCount(this.numberClusters-1);
            numberClusters--;
        } while (this.numberClusters > 2);

        //Ajustando os ultimos nos da matriz de distancia.
        //System.out.println("Acabou iteracoes, ajustando Root...");
        adjustRoot();

        //Realizando promoção de folhas, de acordo com opcao no pipeline...
        if ((pnj != null)&&(pnj)) promoteLeafs(tree);

        end = System.currentTimeMillis();
        long diff = end - init;
        System.out.println("Time spent (NJ Connection creation) -> " + (diff/1000.0f) + " seconds");
        
    }

    public ArrayList generateHierarchy() {
        ArrayList nodes = new ArrayList();
        for (int i=0;i<this.tree.getSize();i++) nodes.add(0);
        for (int i = 0;i < this.tree.getSize(); i++) {
            ContentTree node = this.tree.getNode(i);
            if (node.getParent() != null)
                nodes.add(node.getParent().getId(),node.getId());
        }
        return nodes;
    }

    private void calculateDivergence() {

        this.divergence = new float[this.dmat.getElementCount()];
        int i=0,j;
        for (j=0;j<this.divergence.length;j++) this.divergence[j] = 0.0f;
        while (i<this.dmat.getElementCount()) {
            j = i+1;
            while (j<this.dmat.getElementCount()) {
                this.divergence[i] += this.dmat.getDistance(i,j);
                this.divergence[j] += this.dmat.getDistance(i,j);
                j++;
            }
            i++;
        }
    }

    private void calculateNewDistanceMatrix() {

        newDmat = new float[this.dmat.getElementCount() - 1][];
        for (int i=0;i<newDmat.length;i++) {
            newDmat[i] = new float[i+1];
            for (int j=0;j<newDmat[i].length;j++) {
                float distance = this.dmat.getDistance(i+1,j) - ((this.divergence[i+1] + this.divergence[j])/(this.dmat.getElementCount()-2));
                newDmat[i][j] = distance;
            }
        }
    }

    private void calculateSmallestValue(float [][] newDmat) {

        min_x = 0;
        min_y = 0;
        float min_value = Float.MAX_VALUE;

        for (int i=0;i<newDmat.length;i++) {
            for (int j=0;j<newDmat[i].length;j++) {
                if (newDmat[i][j] < min_value) {
                    min_x = i;
                    min_y = j; 
                    min_value = newDmat[i][j];
                }
            }
        }
        if (min_x > min_y) //Somo com um para coincidir com o indice na matriz de distancias. Foi feito assim para preservar a estrutura original da matriz.
            min_x++;
        else
            min_y++;
    }

    private ContentTree createJunctionNode(DistanceMatrix dmat, float divergence[]) {

        //Junction Node properties
        ContentTree junctionNode, son1, son2;
        junctionNode = new ContentTree(++maxId,Integer.toString(tree.getSize()));
        son1 = tree.getNodeById(this.dmat.getIds().get(min_x));
        son2 = tree.getNodeById(this.dmat.getIds().get(min_y));

        ArrayList<ContentTree> children = new ArrayList<ContentTree>();
        children.add(son1);
        children.add(son2);
        junctionNode.setChildren(children);

        //o nivel do no de juncao sera um a mais do maior nivel entre seus filhos...
        if (son1.getNivelNo() > son2.getNivelNo())
            junctionNode.setNivelNo(son1.getNivelNo()+1);
        else junctionNode.setNivelNo(son2.getNivelNo()+1);
        
        //d(AB) / 2 + [r(A)-r(B)] / 2(N-2)
        float distSon1, distSon2;
        distSon1 = (dmat.getDistance(min_x, min_y)/2) + (divergence[min_x]-divergence[min_y])/(2*(dmat.getElementCount()-2));
        distSon2 = dmat.getDistance(min_x, min_y) - distSon1;
        
        ArrayList<Float> distChildren = new ArrayList<Float>();
        distChildren.add(distSon1);
        distChildren.add(distSon1);
        junctionNode.setDistChildren(distChildren);

        junctionNode.setValid(false);

        //adjusting sons properties
        son1.setParent(junctionNode);
        son2.setParent(junctionNode);

        son1.setDistParent(distSon1);
        son2.setDistParent(distSon2);

        son1.setRoot(junctionNode.getRoot());
        son2.setRoot(junctionNode.getRoot());
        
        return junctionNode;
    }

    private float[][] recalculateDistanceMatrix() {

        float[][] dmatTemp = new float[this.dmat.getElementCount() - 2][];
        //Criando a nova matriz...
        for (int i=0;i<dmatTemp.length;i++) dmatTemp[i] = new float[i+1];

        float distanceNode;

        float distanceJoin = this.dmat.getDistance(min_x, min_y); //Distancia entre os nos sendo juntados, para usar na formula.
        int l, c;  //linha e coluna da nova matriz de distancia, com a nova juncao.
        int linMatrix, colMatrix; //linha e coluna na matriz de distancia atual, para retornar as distancias.

        if ((this.min_x == this.dmat.getElementCount()-1)||(this.min_y == this.dmat.getElementCount()-1)) { //Neste caso especial, um dos itens escolhidos para ser agrupado � o �ltimo item da lista.
            //d(iU) = [d(Ai) + d(Bi) - d(AB)] / 2 = 3 - Com A e B filhos de U
            //Calculando as distancias novas, resultantes da adicao do no de juncao.
            colMatrix = 0;
            for (int i=0;i<dmatTemp[dmatTemp.length-1].length;i++) {
                while ((colMatrix==(this.min_x))||(colMatrix==(this.min_y))) colMatrix++; //Pulando os nos que foram agrupados, eles nao serao colocados na nova matriz.
                distanceNode = (this.dmat.getDistance(min_x,colMatrix)+this.dmat.getDistance(min_y,colMatrix)-distanceJoin)/2;
                colMatrix++;
                dmatTemp[dmatTemp.length-1][i] = distanceNode;
            }
            //Transferindo as distancias antigas...
            l = c = 0; //linha e coluna da nova matriz de distancia, com a nova juncao.
            linMatrix = colMatrix = 0; //linha e coluna na matriz de distancia atual, para retornar as distancias.

            if (dmatTemp.length > 1) { //Este teste � feito porque, como o contador de colunas abaixo vai at� a penultima linha, quando a matriz criada tem apenas uma linha, nao existe penultima coluna. Isto acontece na ultima iteracao, e n�o eh necessario fazer nada neste caso.
                while (c < dmatTemp[dmatTemp.length-2].length) { //Este contador vai ate a ultima coluna da penultima linha da nova matriz. Isso acontece porque eu ja preenchi a ultima linha da matriz, que eh aquela com o no de juncao.
                    while (l < dmatTemp.length-1) { //Este contador controla as linhas da nova matriz. Tambem vai ate a penultima linha, porque a ultima eu ja preenchie com os dados da nova juncao.
                        while ((linMatrix == (this.min_y-1))||(linMatrix == (this.min_x-1))) linMatrix++; //Isto serve para pular as linhas que foram unidas (nao eh preciso armazenar suas distancias mais). O -1 aparece porque os indices aqui estao uma unidade avancados, por causa do posicionamento da coluna.
                        if (linMatrix < this.dmat.getDistmatrix().length) {
                            while ((colMatrix == (this.min_x))||(colMatrix == (this.min_y))) colMatrix++; //Isto serve para pular as colunas que foram unidas (nao eh preciso armazenas suas distancias mais).
                            if ((colMatrix < this.dmat.getDistmatrix()[linMatrix].length)&&(c < dmatTemp[l].length)) { //A segunda condicao evita que o contador de colunas exceda determinada linha.
                                dmatTemp[l][c] = this.dmat.getDistance(linMatrix+1,colMatrix); //Armazeno a distancia, obtida da matriz atual. o +1 eh devido ao posicionamento das linhas, sempre uma a mais que as colunas.
                                l++;
                            }
                            linMatrix++;
                        }
                    }
                    colMatrix++;
                    c++; //A matriz sempre comeca na diagonal principal, por isso essa conta eh feita.
                    l = c;
                    linMatrix = 0;
                }
            }
        }else {
            linMatrix = 0;
            //d(iU) = [d(Ai) + d(Bi) - d(AB)] / 2 = 3 - Com A e B filhos de U
            //Calculando as distancias novas, resultantes da adicao do no de juncao.
            for (int i=0;i<dmatTemp.length;i++) {
                while ((linMatrix==(this.min_x))||(linMatrix==(this.min_y))) linMatrix++; //Pulando os nos que foram agrupados, eles nao serao colocados na nova matriz.
                distanceNode = (this.dmat.getDistance(min_x,linMatrix)+this.dmat.getDistance(min_y,linMatrix)-distanceJoin)/2;
                linMatrix++;
                dmatTemp[i][0] = distanceNode;
            }
            //Transferindo as distancias antigas...
            l = c = 1; //linha e coluna da nova matriz de distancia, com a nova juncao.
            linMatrix = 0;
            colMatrix = 0; //linha e coluna na matriz de distancia atual, para retornar as distancias.

            while (c < dmatTemp[dmatTemp.length-1].length) { //Este contador vai ate a ultima coluna da ultima linha da nova matriz, que eh a maior e a final.
                while (l < dmatTemp.length) { //Este contador controla as linhas da nova matriz.
                    while ((linMatrix == (this.min_y-1))||(linMatrix == (this.min_x-1))) linMatrix++; //Isto serve para pular as linhas que foram unidas (nao eh preciso armazenar suas distancias mais). O -1 aparece porque os indices aqui estao uma unidade avancados, por causa do posicionamento da coluna.
                    if (linMatrix < this.dmat.getDistmatrix().length) {
                        while ((colMatrix == (this.min_x))||(colMatrix == (this.min_y))) colMatrix++; //Isto serve para pular as colunas que foram unidas (nao eh preciso armazenas suas distancias mais).
                        if ((colMatrix < this.dmat.getDistmatrix()[linMatrix].length)&&(c < dmatTemp[l].length)) { //A segunda condicao evita que o contador de colunas exceda determinada linha.
                            dmatTemp[l][c] = this.dmat.getDistance(linMatrix+1,colMatrix); //Armazeno a distancia, obtida da matriz atual. o +1 eh devido ao posicionamento das linhas, sempre uma a mais que as colunas.
                            l++;
                        }
                        linMatrix++;
                    }
                }
                colMatrix++;
                c++; //A matriz sempre comeca na diagonal principal, por isso essa conta eh feita.
                l = c;
                linMatrix = 0;
            }
        }
        return dmatTemp;
    }

    private void adjustRoot() {
        //Vou ligar os dois ultimos nos que sobraram na matriz de distancia...
        ArrayList<Integer> ids = this.dmat.getIds();

        //Os dois ultimos nos terao seus niveis setados como os mais altos da arvore (serao as raizes)
        //Assim, pego o maior dos niveis e seto os dois com esse valor mais um
        int maxNivel;
        if (this.tree.getNodeById(ids.get(0)).getNivelNo() > this.tree.getNodeById(ids.get(1)).getNivelNo())
            maxNivel = this.tree.getNodeById(ids.get(0)).getNivelNo();
        else
            maxNivel = this.tree.getNodeById(ids.get(1)).getNivelNo();
        this.tree.getNodeById(ids.get(0)).setNivelNo(maxNivel+1);
        this.tree.getNodeById(ids.get(1)).setNivelNo(maxNivel+1);

        this.tree.getNodeById(ids.get(0)).setParent(this.tree.getNodeById(ids.get(1)));
        this.tree.getNodeById(ids.get(0)).setDistParent(this.dmat.getDistmatrix()[0][0]);
        this.tree.getNodeById(ids.get(1)).setParent(this.tree.getNodeById(ids.get(0)));
        this.tree.getNodeById(ids.get(1)).setDistParent(this.dmat.getDistmatrix()[0][0]);
    }

    public int getSize() {
        return tree.getSize();
    }

    public Tree getTree() {
        return this.tree;
    }

    public static void promoteLeafs(Tree t) {

        long linit, lend, diff;

        ContentTree virtualNode = null; //no virtual em analise
        ContentTree parentVirtualNode = null; //pai do no virtual em analise
        ArrayList<ContentTree> children = null;
        
        //t.setRoot(t.getNode(t.getSize()-1).getId());

        linit = System.currentTimeMillis();

        for (int i=0;i<t.getSize();i++) {
            if ((!t.getNode(i).isValid())&&(!t.getNode(i).isRoot())) { //analiso apenas nos virtuais, e nao analiso os nos virtuais que estao no ultimo nivel ("raizes")
                virtualNode = t.getNode(i);
                parentVirtualNode = virtualNode.getParent();
                if (parentVirtualNode != null) {
                    children = parentVirtualNode.getChildren();
                    if (children != null) {
                        for (int j=0;j<children.size();j++) {
                            //pegando o outro filho...
                            if (!children.get(j).equals(virtualNode)) {
                                if ((children.get(j).isValid())&&((children.get(j).getChildren()==null)||(children.get(j).getChildren().isEmpty()))) {
                                    //Fazendo a troca...
                                   if (virtualNode.getChildren() != null) {
                                       if (children.get(j).getChildren() == null) children.get(j).setChildren(new ArrayList<ContentTree>());
                                       if (children.get(j).getDistChildren() == null) children.get(j).setDistChildren(new ArrayList<Float>());
                                       for (int k=0;k<virtualNode.getChildren().size();k++) {
                                           virtualNode.getChildren().get(k).setParent(children.get(j));
                                           children.get(j).getChildren().add(virtualNode.getChildren().get(k));
                                           //children.get(j).getDistChildren().add(virtualNode.getDistChildren().get(k));
                                           float d = virtualNode.getDistChildren().get(k) +
                                                    (virtualNode.getDistParent()/2) +
                                                    (parentVirtualNode.getDistChildren().get(j)/4);
                                           children.get(j).getDistChildren().add(d);
                                       }
                                   }
                                   //Setando o pai do no promovido...
                                   children.get(j).setParent(parentVirtualNode.getParent());
                                   float dp = parentVirtualNode.getDistParent()+(virtualNode.getDistParent()/2)+(children.get(j).getDistParent()/2);
                                   //setando a distancia entre o no promovido e seu pai...
                                   children.get(j).setDistParent(dp);
                                   if (children.get(j).getParent() != null) {
                                       int indexSon = children.get(j).getParent().getChildren().indexOf(children.get(j));
                                       //setando a distancia entre o pai do no promovido e o no promovido...
                                       if (indexSon != -1) children.get(j).getParent().getDistChildren().set(indexSon,dp);
                                   }else {
                                       System.out.println(" ");
                                   }
                                   children.get(j).setNivelNo(parentVirtualNode.getNivelNo());
                                   if ((parentVirtualNode.getParent() != null)&&(parentVirtualNode.getParent().getChildren() != null)) {
                                       //Neste caso, um no nao aparece como filho do outro, e vice-versa, e por isso nao se remove um da
                                       //lista de filhos do outro, e nem se adiciona o outro na lista de filhos do um.
                                       if ((children.get(j).getParent().getParent() != null)&&(children.get(j).getParent().getParent().equals(parentVirtualNode))) {
                                           children.get(j).getParent().setParent(children.get(j));
                                           //Como neste caso os nos sao os ultimos, ditos raizes, devemos igualar seus niveis
                                           //int maxNivel;
                                           if (children.get(j).getParent().getNivelNo() > children.get(j).getNivelNo()) {
                                               //maxNivel = children.get(j).getParent().getNivelNo();
                                               //children.get(j).setNivelNo(maxNivel);
                                               children.get(j).setNivelNo(children.get(j).getParent().getNivelNo());
                                           } else {
                                               //maxNivel = children.get(j).getNivelNo();
                                               //children.get(j).getParent().setNivelNo(maxNivel);
                                               children.get(j).getParent().setNivelNo(children.get(j).getNivelNo());
                                           }
                                           //children.get(j).getParent().setNivelNo(maxNivel);
                                           //children.get(j).setNivelNo(maxNivel);
                                           //t.setRoot(children.get(j).getId());
                                       }else {
                                           //dp = parentVirtualNode.getDistParent()+(virtualNode.getDistParent()/2)+(children.get(j).getDistParent()/2);
                                           int index = parentVirtualNode.getParent().getChildren().indexOf(parentVirtualNode);
                                           //parentVirtualNode.getParent().getChildren().remove(parentVirtualNode);
                                           if (index != -1) {
                                               parentVirtualNode.getParent().getChildren().set(index,children.get(j));
                                               parentVirtualNode.getParent().getDistChildren().set(index,dp);
                                           } else {
                                               parentVirtualNode.getParent().getChildren().add(children.get(j));
                                               parentVirtualNode.getParent().getDistChildren().add(dp);
                                           }
                                       }
                                   }
                                   parentVirtualNode.setParent(null);
                                   virtualNode.setParent(null);
                                   //Se o parentVirtualNode tiver 3 filhos, entao ele eh a raiz da arvore. Neste caso, como vou exclui-lo,
                                   //sobrarao dois nos, e eles deverao ser unidos, formando a nova arvore
                                   if (children.size() ==3) {
                                       for (int x=0;x<children.size();x++) {
                                           if (!children.get(x).equals(children.get(j))&&!children.get(x).equals(virtualNode)) {
                                               children.get(j).setParent(children.get(x));
                                               children.get(j).setDistParent(children.get(x).getDistParent());
                                               children.get(x).setParent(children.get(j));
                                               //Como nesse caso, o nivel do parentvirtualNode era o mais alto, vou setar os seus filhos com esse nivel,
                                               //pois eles serao as novas raizes
                                               children.get(x).setNivelNo(parentVirtualNode.getNivelNo());
                                               children.get(j).setNivelNo(parentVirtualNode.getNivelNo());
                                           }
                                       }
                                   }
                                   t.removeNode(parentVirtualNode);
                                   t.removeNode(virtualNode);
                                   virtualNode = null;
                                   i--;
                                   //Em todos os casos, um no tem apenas dois filhos, e esse break seria inutil, pois ele so entraria aqui uma vez.
                                   //No entanto, no nivel mais alto da arvore, o no raiz tem 3 filhos, e pode acontecer de entrar aqui antes de ser
                                   //o ultimo filho. Assim, eu forco ele a sair do for, pois a troca ja foi feita, e nao eh necessario iterar mais
                                   //sobre os filhos.
                                   break;
                                }
                            }
                        }
                    }
                }
            }
        }
        lend = System.currentTimeMillis();
        diff = lend - linit;
        System.out.println("Time spent (Leaf promotion) -> " + (diff/1000.0f) + " seconds");
    }

}
