package tree.technique.njprojection;

import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import projection.model.Scalar;
import tree.basics.Tree;
import tree.layout.radial.RadialLayoutComp;
import tree.model.TreeInstance;
import tree.model.TreeModel;
import tree.model.TreeModelComp;
import tree.technique.packagenj.PackageNJ;
import tree.technique.packagenj.PackageNJConnectionComp;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Projection.Technique",
name = "Neighbor-Joinning based Projection",
description = "Project points from a multidimensional space to the plane based on a Neighbor-Joining tree structure.")
public class NJProjectionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (matrix != null) {//using a points matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            DistanceMatrix ldmat = new DistanceMatrix(matrix,diss);
            createTreeProjection(ldmat); //execute nj
        } else if (dmat != null) {//using a distance matrix
            createTreeProjection(dmat); //execute nj
        } else {
            throw new IOException("A distance matrix or a points matrix should be provided.");
        }
    }

    private void createTreeProjection(DistanceMatrix dmat) throws IOException {
        PackageNJConnectionComp packageNJC = new PackageNJConnectionComp();
        packageNJC.input(dmat);
        packageNJC.setDissimilarityType(disstype);
        packageNJC.setNjAlgorithmType(njAlgorithmType);
        packageNJC.setPnj(pnj);
        packageNJC.execute();
        Tree tree = packageNJC.output();
        if (tree != null) {
            TreeModelComp tmc = new TreeModelComp();
            RadialLayoutComp rc = new RadialLayoutComp();
            tmc.input(tree);
            tmc.execute();
            rc.input(tmc.output());
            rc.execute();
            TreeModel m = rc.output();
            ArrayList<String> labels = new ArrayList<String>();
            if (m != null) {
                //Excluding virtual nodes...
                projection = new DenseMatrix();
                Scalar scalar = m.getScalarByName("cdata");
                for (int i=0;i<m.getInstances().size();i++) {
                    TreeInstance ti = (TreeInstance)m.getInstances().get(i);
                    if (ti.isValid()) {
                        float[] vector = new float[2];
                        vector[0] = ti.getX();
                        vector[1] = ti.getY();
                        DenseVector row = new DenseVector(vector,ti.getId(),ti.getScalarValue(scalar));
                        projection.addRow(row);
                        labels.add(ti.toString());
                    }
                }
                projection.setLabels(labels);
                ArrayList<String> attributes = new ArrayList<String>();
                attributes.add("x");
                attributes.add("y");
                projection.setAttributes(attributes);
            }
        } else {
            throw new IOException("Problems in generating tree.");
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "distance matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public AbstractMatrix output() {
        return projection;
    }
    
    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new NJProjectionParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        projection = null;
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public boolean isDistanceMatrixProvided() {
        return (dmat != null);
    }

    public Boolean isPnj() {
        if (pnj != null) return pnj;
        else return false;
    }

    public void setPnj(Boolean pnj) {
        this.pnj = pnj;
    }

    public PackageNJ.NJAlgorithmType getNjAlgorithmType() {
        return njAlgorithmType;
    }

    public void setNjAlgorithmType(PackageNJ.NJAlgorithmType njAlgorithmType) {
        this.njAlgorithmType = njAlgorithmType;
    }

    public static final long serialVersionUID = 1L;
    private transient NJProjectionParamView paramview;
    //private transient Tree tree;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
    private transient AbstractMatrix projection;
    private PackageNJ.NJAlgorithmType njAlgorithmType;
    private Boolean pnj = false;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
}
