package tree.technique.mst;

import tree.basics.ContentTree;
import java.io.IOException;
import java.util.ArrayList;
import distance.DistanceMatrix;
import graph.model.Edge;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import tree.basics.Tree;

public class MinimumSpanningTree {

    DistanceMatrix dmat;
    private Tree tree;
    private long init, end;
    private int junctionNodeId;
    private Edge smallestEdge;
    HashMap<Integer,TreeMap> distances;
    
    public void load() {

        tree = new Tree();
        ArrayList ids = dmat.getIds();
        float[] cdatas = dmat.getClassData();

        distances = new HashMap();

        for (int i=0;i<dmat.getElementCount();i++) {
            distances.put(dmat.getIds().get(i),new TreeMap(new Comparator() {
                                    public int compare(Object o1, Object o2) {
                                        Edge p1 = (Edge) o1;
                                        Edge p2 = (Edge) o2;
                                        if (p1.getWeight() < p2.getWeight()) return -1;
                                        else if (p1.getWeight() > p2.getWeight()) return +1;
                                        else return -1;
                                    }
                                }));
        }

        TreeMap t = null;
        for (int i=0;i<dmat.getElementCount();i++) {
            tree.addNode(new ContentTree((Integer)ids.get(i),Integer.toString(i),cdatas[i]));
            for (int j=i+1;j<dmat.getElementCount();j++) {
                t = distances.get(dmat.getIds().get(i));
                t.put(new Edge(dmat.getIds().get(i),dmat.getIds().get(j),dmat.getDistance(i,j)),dmat.getDistance(i,j));
                t = distances.get(dmat.getIds().get(j));
                t.put(new Edge(dmat.getIds().get(j),dmat.getIds().get(i),dmat.getDistance(i,j)),dmat.getDistance(i,j));
            }
        }

        tree.setRoot(tree.getNode(0).getId());

    }
  
    public MinimumSpanningTree(DistanceMatrix dmat) {
        this.dmat = dmat;
        load();
    }

    public void createTree() {
        
        init = System.currentTimeMillis();

        Tree finalTree = new Tree();
        finalTree.addNode(tree.getNode(0));
        finalTree.setRoot(tree.getNode(0).getId());

        while (finalTree.getSize() < tree.getSize()) {
            getSmallestEdge(finalTree);
            createJunction(finalTree);
        }

        tree = finalTree;
        end = System.currentTimeMillis();
        long diff = end - init;
        System.out.println("Tempo gasto -> " + (diff/1000.0f) + " segundos");
        
    }

    public void getSmallestEdge(Tree tree) {
        Edge edge;
        smallestEdge = null;
        TreeMap t = null;
        for (int k=0;k<tree.getSize();k++) {
            t = distances.get(tree.getNode(k).getId());
            if ((t != null)&&(!t.isEmpty())) {
                edge = (Edge) t.firstKey();
                if ((smallestEdge == null)||(smallestEdge.getWeight() > edge.getWeight())) {
                    smallestEdge = edge;
                    junctionNodeId = tree.getNode(k).getId();
                }
            }
        }
    }

    public void createJunction(Tree tree) {

        ContentTree parent = tree.getNodeById(junctionNodeId);
        Edge edge = null;
        if (parent != null) {
            if (smallestEdge != null) {
                ContentTree son = null;
                if (smallestEdge.getSource() == junctionNodeId) son = this.tree.getNodeById(smallestEdge.getTarget());
                else son = this.tree.getNodeById(smallestEdge.getSource());
                if (son != null) {
                    if (parent.getChildren() != null) {
                        parent.getChildren().add(son);
                        parent.getDistChildren().add(smallestEdge.getWeight());
                    }
                    son.setParent(parent);
                    son.setDistParent(smallestEdge.getWeight());
                    son.setRoot(parent.getRoot());
                    tree.addNode(son);
                    if (tree.getEdges() == null) tree.setEdges(new ArrayList<Edge>());
                    edge = new Edge(parent.getId(),son.getId(),smallestEdge.getWeight());
                    tree.getEdges().add(edge);

                    updateEdges(tree, son.getId());
                    
                }
            }
        }
    }

    public void updateEdges(Tree tree, int node) {

        Edge edge = null;
        TreeMap distsNode = null;
        int id;

        //tirando os outros nós da lista do nó em questao
        distsNode = distances.get(node);
        for (int i=0;i<tree.getSize();i++) {
            id = tree.getNode(i).getId();
            if (id != node) {
                Iterator it = distsNode.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry e = (Map.Entry) it.next();
                    edge = (Edge)e.getKey();
                    if (((edge.getSource() == id)||(edge.getTarget() == id))) {
                        it.remove();
                    }
                }
            }
        }

        //tirando o nó em questao das listas dos outros nós
        for (int i=0;i<tree.getSize();i++) {
            id = tree.getNode(i).getId();
            if (id != node) {
                distsNode = distances.get(id);
                //remover os edges que ligam este no aos nos que estao na nova arvore.
                Iterator it = distsNode.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry e = (Map.Entry) it.next();
                    edge = (Edge)e.getKey();
                    if ((edge.getTarget() == node)||(edge.getSource() == node)) {
                        it.remove();
                    }
                }
            }
        }
    }

    public static void main(String a[]) throws IOException {

        DistanceMatrix dt = new DistanceMatrix("D:\\Tutorial.dmat");

        MinimumSpanningTree mst = new MinimumSpanningTree(dt);

        mst.createTree();

        
    }

    public ArrayList generateHierarchy() {
        ArrayList nodes = new ArrayList();
        for (int i=0;i<this.tree.getSize();i++) nodes.add(0);
        for (int i = 0;i < this.tree.getSize(); i++) {
            ContentTree node = this.tree.getNode(i);
            if (node.getParent() != null)
                nodes.add(node.getParent().getId(),node.getId());
        }
        return nodes;
    }

    private boolean existEdge(ArrayList<Edge> edges, int sour, int targ) {
        if (edges == null) return false;
        for (int i=0;i<edges.size();i++) {
            if (((edges.get(i).getSource()==sour)&&(edges.get(i).getTarget()==targ))||
                ((edges.get(i).getSource()==targ)&&(edges.get(i).getTarget()==sour)))
                return true;
        }
        return false;
    }

    public void generateEdges() {

        ArrayList<Edge> edges = new ArrayList<Edge>();
        int sour, targ;
        float dis;

        //generating the connectivity
        for (int i = 0; i < this.getTree().getSize(); i++) {
            ContentTree son = this.getTree().getNode(i);
            if (son != null) {
                dis = son.getDistParent();
                ContentTree parent = son.getParent();
                if (parent != null) {
                    sour = parent.getId();
                    targ = son.getId();
                    if (!existEdge(edges, sour, targ)) {
                        edges.add(new Edge(sour, targ, dis));
                    }
                }
            }
        }

        this.getTree().setEdges(edges);
    }

    public int getSize() {
        return tree.getSize();
    }

    public Tree getTree() {
        return this.tree;
    }

}