package tree.technique.mst;

import distance.DistanceMatrix;
import java.io.IOException;
import distance.dissimilarity.AbstractDissimilarity;
import tree.model.TreeConnectivity;
import graph.technique.connection.GraphConnection;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import tree.basics.Tree;
import tree.basics.TreeConstants;
import tree.technique.mst.algorithms.MSTAlgorithmFactory;
import tree.technique.mst.algorithms.MSTAlgorithmFactory.Algorithm;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class MSTConnection extends GraphConnection {

    public MSTConnection(Algorithm alg) {
        if (alg == null) this.alg = Algorithm.PRIM;
        else this.alg = alg;
    }

    public TreeConnectivity execute(AbstractMatrix matrix, AbstractDissimilarity diss) throws IOException {
        DistanceMatrix dmat = new DistanceMatrix(matrix, diss);
        return execute(dmat);
    }

    @Override
    public TreeConnectivity execute(DistanceMatrix dmat) throws IOException {

        //this.mst = new MinimumSpanningTree(dmat);
        //this.getMST().createTree();
        //this.getMST().generateEdges();
        tree = MSTAlgorithmFactory.getInstance(this.alg).execute(dmat);
        TreeConnectivity conn = new TreeConnectivity(TreeConstants.MST,tree.getEdges());
        return conn;
    }

    public Tree getTree() {
        return tree;
    }


//    public MinimumSpanningTree getMST() {
//        return mst;
//    }

    public void assignScalars(ArrayList<String> scalars) {
        if (scalars.size() <= tree.getSize()) {
            for (int i=0;i<scalars.size();i++) {
                tree.getNode(i).setLabel(scalars.get(i));
            }
        }
    }

    //private MinimumSpanningTree mst;
    private Algorithm alg;
    private Tree tree;

}
