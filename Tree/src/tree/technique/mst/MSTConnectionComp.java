package tree.technique.mst;

import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import tree.model.TreeConnectivity;
import java.io.IOException;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import tree.basics.Tree;
import tree.technique.mst.algorithms.MSTAlgorithmFactory.Algorithm;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.Technique",
name = "Minimum Spanning Tree",
description = "Project points from a multidimensional space to the plane " +
"preserving the neighborhood relations.")
public class MSTConnectionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        //projecting
        MSTConnection mst = new MSTConnection(alg);
        if (matrix != null) { //using a points matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            conn = mst.execute(matrix, diss);
            if (scalars != null) mst.assignScalars(scalars);
            tree = mst.getTree();
        } else if (dmat != null) { //using a distance matrix
            conn = mst.execute(dmat);
            if (scalars != null) mst.assignScalars(scalars);
            tree = mst.getTree();
        } else {
            throw new IOException("A distance matrix or a points matrix should " + "be provided.");
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "distance matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public void input(@Param(name = "scalars set") ArrayList<String> scalars) {
        this.scalars = scalars;
    }

    /**
     * @return the disstype
     */
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    /**
     * @param disstype the disstype to set
     */
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public TreeConnectivity output() {
        return conn;
    }

    public Tree outputTree() {
        return tree;
    }
    
    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new MSTConnectionParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        conn = null;
        matrix = null;
        dmat = null;
        scalars = null;
    }

    public boolean isDistanceMatrixProvided() {
        return (dmat != null);
    }

    public Algorithm getAlgorithm() {
        return alg;
    }

    public void setAlgorithm(Algorithm alg) {
        this.alg = alg;
    }

    public static final long serialVersionUID = 1L;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private Algorithm alg = Algorithm.PRIM;
    private transient MSTConnectionParamView paramview;
    private transient TreeConnectivity conn;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
    private transient Tree tree;
    private transient ArrayList<String> scalars;

}
