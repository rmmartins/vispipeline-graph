/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.technique.mst.algorithms;

import distance.DistanceMatrix;
import tree.basics.Tree;

/**
 *
 * @author Jose Gustavo
 */
public abstract class AbstractMSTAlgorithm {

    public abstract Tree execute(DistanceMatrix dmat);

}
