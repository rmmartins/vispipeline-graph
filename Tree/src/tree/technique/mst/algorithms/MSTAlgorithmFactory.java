package tree.technique.mst.algorithms;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class MSTAlgorithmFactory {

    public enum Algorithm {
        PRIM,
        KRUSKA
    }

    public static AbstractMSTAlgorithm getInstance(Algorithm alg) {

        if (alg.equals(Algorithm.PRIM)) {
            return new Prim();
        } else if (alg.equals(Algorithm.KRUSKA)) {
            //return new Kruska();
        }

        return null;
    }

}
