/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx), based on the code presented 
 * in:
 * 
 * http://snippets.dzone.com/user/scvalex/tag/prim
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package tree.technique.mst.algorithms;

import distance.DistanceMatrix;
import graph.model.Edge;
import java.util.ArrayList;
import tree.basics.ContentTree;
import tree.basics.Tree;
import tree.basics.TreeConstants;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class Prim extends AbstractMSTAlgorithm {

    public void load(DistanceMatrix dmat) {

        this.n = dmat.getElementCount();
        this.weight = new float[this.n][];
        this.inTree = new boolean[this.n];
        this.d = new float[this.n];
        this.whoTo = new int[this.n];

        //creating the edges
        for (int i = 0; i < dmat.getElementCount(); i++) {
            this.weight[i] = new float[this.n];
            for (int j = 0; j < dmat.getElementCount(); j++) {
                float dist = dmat.getDistance(i, j);
                if (dist < 0.0f) {
                    this.weight[i][j] = 0.0f;
                } else {
                    this.weight[i][j] = dist;
                }
            }
        }

        //creating the tree
        ArrayList ids = dmat.getIds();
        float[] cdatas = dmat.getClassData();
        tree = new Tree();
        tree.setType(TreeConstants.MST);
        for (int i=0;i<dmat.getElementCount();i++) {
            tree.addNode(new ContentTree((Integer)ids.get(i),Integer.toString(i),cdatas[i]));
        }
        tree.setRoot(tree.getNode(0).getId());

    }

    public Tree execute(DistanceMatrix dmat) {
        load(dmat);
        long init = System.currentTimeMillis();

        ArrayList<Edge> edges = new ArrayList<Edge>();

        /* Initialise d with infinity */
        for (int i = 0; i < n; ++i) {
            d[i] = Float.MAX_VALUE;
        }

        /* Mark all nodes as NOT beeing in the minimum spanning tree */
        for (int i = 0; i < n; ++i) {
            inTree[i] = false;
        }

        /* Add the first node to the tree */
        inTree[0] = true;
        this.updateDistances(0);

        for (int treeSize = 1; treeSize < n; ++treeSize) {
            /* Find the node with the smallest distance to the tree */
            int min = -1;
            for (int i = 0; i < n; ++i) {
                if (!inTree[i]) {
                    if ((min == -1) || (d[min] > d[i])) {
                        min = i;
                    }
                }
            }

            /* And add it */
            edges.add(new Edge(whoTo[min], min, d[min]));
            inTree[min] = true;

            this.updateDistances(min);
        }

        long end = System.currentTimeMillis();
        long diff = end - init;
        System.out.println("Time spent (MST Creation) -> " + (diff/1000.0f) + " seconds");
        generateTree(edges);
        return tree;
    }

    /* updateDistances(int target)
    should be called immediately after target is added to the tree;
    updates d so that the values are correct (goes through target's
    neighbours making sure that the distances between them and the tree
    are indeed minimum)
     */
    private void updateDistances(int target) {
        for (int i = 0; i < n; ++i) {
            if ((weight[target][i] != 0.0f) && (d[i] > weight[target][i])) {
                d[i] = weight[target][i];
                whoTo[i] = target;
            }
        }
    }

    private void generateTree(ArrayList<Edge> edges) {

        Edge e = null;
        ArrayList<Edge> newEdges = new ArrayList<Edge>();

        for (int i=0;i<edges.size();i++) {
            e = edges.get(i);
            ContentTree parent = tree.getNode(e.getSource());
            if (parent != null) {
                ContentTree son = tree.getNode(e.getTarget());
                if (son != null) {
                    son.setParent(parent);
                    son.setRoot(parent.getRoot());
                    son.setDistParent(e.getWeight());
                    if (parent.getChildren() == null) parent.setChildren(new ArrayList<ContentTree>());
                    parent.getChildren().add(son);
                    if (parent.getDistChildren() == null) parent.setDistChildren(new ArrayList<Float>());
                    parent.getDistChildren().add(e.getWeight());
                    newEdges.add(new Edge(parent.getId(),son.getId(),e.getWeight()));
                }
            }
        }
        tree.setEdges(newEdges);

    }

    private Tree tree;

    private int n; /* The number of nodes in the graph */

    private float weight[][]; /* weight[i][j] is the distance between node i and node j;
    if there is no path between i and j, weight[i][j] should
    be 0 */

    private boolean inTree[]; /* inTree[i] is 1 if the node i is already in the minimum
    spanning tree; 0 otherwise*/

    private float d[]; /* d[i] is the distance between node i and the minimum spanning
    tree; this is initially infinity (100000); if i is already in
    the tree, then d[i] is undefined;
    this is just a temporary variable. It's not necessary but speeds
    up execution considerably (by a factor of n) */

    private int whoTo[]; /* whoTo[i] holds the index of the node i would have to be
    linked to in order to get a distance of d[i] */

}
