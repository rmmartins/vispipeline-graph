package tree.technique.snj.heuristic;

import java.util.ArrayList;
import java.util.Collections;
import tree.technique.snj.SlimNeighborJoining;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public abstract class AbstractHeuristic {

//  public abstract int calculate(int minX, int minY, Tree tree, DistanceMatrix dmat);
    public abstract int calculate(SlimNeighborJoining snj);

    public class Distances {

        public ArrayList<Distance> distances;

        public Distances() {
            distances = new ArrayList<Distance>();
        }

        public void add(Distance o) {
            distances.add(o);
        }

        public Distance getbyIndex(int index) {
            for (int i=0;i<distances.size();i++)
                if (distances.get(i).index == index) return distances.get(i);
            return null;
        }

        public void sort() {
            Collections.sort(distances);
        }

        public int size() {
            return distances.size();
        }

    }

    public class Distance implements Comparable {

        public int index;
        public float distance;

        public Distance(int index, float distance) {
            this.index = index;
            this.distance = distance;
        }

        @Override
        public int compareTo(Object o) {
            Distance temp = (Distance)o;
            if (this.distance > temp.distance) return 1;
            else if (this.distance < temp.distance) return -1;
            else return 0;
        }

    }

}

