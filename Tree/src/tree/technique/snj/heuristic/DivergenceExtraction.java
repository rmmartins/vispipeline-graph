/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.technique.snj.heuristic;

import tree.basics.ContentTree;
import tree.technique.snj.SlimNeighborJoining;

/**
 *
 * @author Jose Gustavo
 */
public class DivergenceExtraction extends AbstractHeuristic {

    @Override
    public int calculate(SlimNeighborJoining snj) {

        Distances distX = new Distances();
        int minX = snj.getMinX();
        int minY = snj.getMinY();
        float dif;

        for (int i=0; i<snj.getTree().getSize();i++) {
            ContentTree node = snj.getTree().getNode(i);
            //if ((i != minX)&&(i != minY)&&(node.getParent()==null)&&(node.getSon1()==null)&&(node.getSon2()==null)) {
            if ((i != minX)&&(i != minY)&&(node.getParent()==null)&&((node.getChildren()==null)||(node.getChildren().size()==0))) {
                dif = (snj.getDistance(snj.getDmatDivergence(),minX,i)+
                       snj.getDistance(snj.getDmatDivergence(),minY,i)
                       )-snj.getDistance(snj.getDmatDivergence(),minX,minY);
                            distX.add(new Distance(i,dif));
            }
        }
        if (distX.size() > 0) {
            distX.sort();
            return distX.distances.get(0).index;
        }else {
            return -1;
        }
    }

}
