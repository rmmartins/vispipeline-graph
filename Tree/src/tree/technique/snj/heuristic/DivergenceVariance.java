/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.technique.snj.heuristic;

import tree.basics.ContentTree;
import tree.technique.snj.SlimNeighborJoining;

/**
 *
 * @author Jose Gustavo
 */
public class DivergenceVariance extends AbstractHeuristic {

    @Override
    public int calculate(SlimNeighborJoining snj) {

        Distances distX = new Distances();
        int minX = snj.getMinX();
        int minY = snj.getMinY();

        for (int i=0; i<snj.getTree().getSize();i++) {
            ContentTree node = snj.getTree().getNode(i);
            //if ((i != minX)&&(i != minY)&&(node.getParent()==null)&&(node.getSon1()==null)&&(node.getSon2()==null)) {
            if ((i != minX)&&(i != minY)&&(node.getParent()==null)&&((node.getChildren()==null)||(node.getChildren().size()==0))) {
                distX.add(new Distance(i,
                (snj.getDistance(snj.getDmatDivergence(),minX,i)-snj.getDistance(snj.getDmatDivergence(),minY,i))));
            }
        }
        if (distX.size() > 0) {
            distX.sort();
            return distX.distances.get(0).index;
        }else {
            return -1;
        }
    }

}
