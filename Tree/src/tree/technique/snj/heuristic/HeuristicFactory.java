package tree.technique.snj.heuristic;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class HeuristicFactory {

    public enum HeuristicType {

        SIMULTANEOUS_DISTANCE,
        AVERAGE_DISTANCE,
        SIMULTANEOUS_DIVERGENCE,
        AVERAGE_DIVERGENCE,
        DIVERGENCE_VARIANCE,
        DIVERGENCE_EXTRACTION
    }

    public static AbstractHeuristic getInstance(HeuristicType type) {

        if (type.equals(HeuristicType.SIMULTANEOUS_DISTANCE)) {
            return new SimultaneousDistance();
        } else if (type.equals(HeuristicType.AVERAGE_DISTANCE)) {
            return new AverageDistance();
        } else if (type.equals(HeuristicType.SIMULTANEOUS_DIVERGENCE)) {
            return new SimultaneousDivergence();
        } else if (type.equals(HeuristicType.AVERAGE_DIVERGENCE)) {
            return new AverageDivergence();
        } else if (type.equals(HeuristicType.DIVERGENCE_VARIANCE)) {
            return new DivergenceVariance();
        } else if (type.equals(HeuristicType.DIVERGENCE_EXTRACTION)) {
            return new DivergenceExtraction();
        }

        return null;
    }

}
