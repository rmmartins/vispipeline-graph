/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.technique.snj.heuristic;

import distance.DistanceMatrix;
import tree.basics.ContentTree;
import tree.basics.Tree;
import tree.technique.snj.SlimNeighborJoining;

/**
 *
 * @author Jose Gustavo
 */
public class SimultaneousDistance extends AbstractHeuristic {

    public int calculate(SlimNeighborJoining snj) {

        Distances distX = new Distances();
        Distances distY = new Distances();
        int minX = snj.getMinX();
        int minY = snj.getMinY();

        for (int i=0; i<snj.getTree().getSize();i++) {
            ContentTree node = snj.getTree().getNode(i);
            //if ((i != minX)&&(i != minY)&&(node.getParent()==null)&&(node.getSon1()==null)&&(node.getSon2()==null)) {
            if ((i != minX)&&(i != minY)&&(node.getParent()==null)&&((node.getChildren()==null)||(node.getChildren().size()==0))) {
                distX.add(new Distance(node.getId(),snj.getDistanceMatrix().getDistance(minX,i)));
                distY.add(new Distance(node.getId(),snj.getDistanceMatrix().getDistance(minY,i)));
            }
        }

        distX.sort();
        distY.sort();

        Distances distT = new Distances();
        //Como distX e distY tem sempre o mesmo tamanho, posso usar qualquer um para processar.
        float dist;

        if (distX.size() > 0) {
            for (int i=0;i<distX.size();i++) {
                dist = Math.abs(i - distY.distances.indexOf(distY.getbyIndex(distX.distances.get(i).index))) //Diferenca entre as ordens das distancias
                       + (i*distX.distances.get(i).distance) //indice da distancia multiplicado pela distancia
                       + (distY.distances.indexOf(distY.getbyIndex(distX.distances.get(i).index))*distY.getbyIndex(distX.distances.get(i).index).distance); //indice da distancia em relacao ao outro no multiplicado pela distancia deste outro no.
                distT.add(new Distance(distX.distances.get(i).index,dist));
            }
            distT.sort();
            return distT.distances.get(0).index;
        }else {
            return -1;
        }
    }

}
