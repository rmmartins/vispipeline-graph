package tree.technique.snj;

import distance.DistanceMatrix;
import java.io.IOException;
import distance.dissimilarity.AbstractDissimilarity;
import java.util.logging.Level;
import java.util.logging.Logger;
import tree.model.TreeConnectivity;
import graph.technique.connection.GraphConnection;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import tree.basics.TreeConstants;
import tree.technique.snj.heuristic.HeuristicFactory.HeuristicType;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class SNJConnection extends GraphConnection {

   public TreeConnectivity execute(AbstractMatrix matrix, AbstractDissimilarity diss, HeuristicType htype) throws IOException {
       this.htype = htype;
       return execute(matrix,diss);
   }

   public TreeConnectivity execute(DistanceMatrix dmat, HeuristicType htype) throws IOException {
       this.htype = htype;
       return execute(dmat);
   }

    public TreeConnectivity execute(AbstractMatrix matrix, AbstractDissimilarity diss) throws IOException {
        DistanceMatrix dmat = new DistanceMatrix(matrix, diss);
        return execute(dmat);
    }

    @Override
    public TreeConnectivity execute(DistanceMatrix dmat) throws IOException {

        this.neighbor = new SlimNeighborJoining(dmat);
        try {
            this.getNeighbor().createTree(htype);
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(SNJConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.getNeighbor().generateEdges();
        ArrayList<String> labels = dmat.getLabels();
        if (!labels.isEmpty()) assignScalars(labels);

        TreeConnectivity conn = new TreeConnectivity(TreeConstants.SNJ, this.getNeighbor().getTree().getEdges());
        return conn;
    }

    public SlimNeighborJoining getNeighbor() {
        return neighbor;
    }

    public void assignScalars(ArrayList<String> scalars) {
        if (scalars.size() <= neighbor.getTree().getSize()) {
            for (int i=0;i<scalars.size();i++) {
                neighbor.getTree().getNode(i).setLabel(scalars.get(i));
            }
        }
    }

    private SlimNeighborJoining neighbor;
    private HeuristicType htype;


}
