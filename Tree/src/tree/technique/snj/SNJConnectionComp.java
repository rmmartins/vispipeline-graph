package tree.technique.snj;

import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import tree.model.TreeConnectivity;
import java.io.IOException;
import matrix.AbstractMatrix;
import tree.basics.Tree;
import tree.technique.snj.heuristic.HeuristicFactory.HeuristicType;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
//@VisComponent(hierarchy = "Tree.Connection.Technique",
//name = "Slim Neighbor-Joinning connection",
//description = "Project points from a multidimensional space to the plane " +
//"preserving the neighborhood relations.")
public class SNJConnectionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        //projecting
        SNJConnection snj = new SNJConnection();
        if (matrix != null) { //using a points matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            conn = snj.execute(matrix, diss, htype);
            tree = snj.getNeighbor().getTree();
        } else if (dmat != null) { //using a distance matrix
            conn = snj.execute(dmat,htype);
            tree = snj.getNeighbor().getTree();
        } else {
            throw new IOException("A distance matrix or a points matrix should " +
                    "be provided.");
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "distance matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public TreeConnectivity output() {
        return conn;
    }

    public Tree outputTree() {
        return tree;
    }
    
    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new SNJConnectionParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        conn = null;
        matrix = null;
        dmat = null;
    }
    
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public HeuristicType getHeuristicType() {
        return htype;
    }

    public void setHeuristicType(HeuristicType h) {
        this.htype = h;
    }

    public boolean isDistanceMatrixProvided() {
        return (dmat != null);
    }

    public static final long serialVersionUID = 1L;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private HeuristicType htype = HeuristicType.SIMULTANEOUS_DISTANCE;
    private transient SNJConnectionParamView paramview;
    private transient TreeConnectivity conn;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
    private transient Tree tree;
}
