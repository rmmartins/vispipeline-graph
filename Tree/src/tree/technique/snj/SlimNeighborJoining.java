package tree.technique.snj;

import distance.DistanceMatrix;
import graph.model.Edge;
import java.io.IOException;
import java.util.ArrayList;
import tree.basics.ContentTree;
import tree.basics.Tree;
import tree.basics.TreeConstants;
import tree.technique.nj.NeighborJoining;
import tree.technique.snj.heuristic.HeuristicFactory;
import tree.technique.snj.heuristic.HeuristicFactory.HeuristicType;

public class SlimNeighborJoining {
    //private DistanceMatrix originalDmat;
    private DistanceMatrix dmat;
    float[][] dmatDivergence;
    private int min_x,min_y;
    private Tree tree;
    private int numberClusters,it=0;
    private long init,end,maxId;

    public void load() {
        //numberClusters = originalDmat.getElementCount();
        numberClusters = this.dmat.getElementCount();
        tree = new Tree();
        tree.setType(TreeConstants.SNJ);
//        try {
//            dmat = (DistanceMatrix) originalDmat.clone();
//        } catch (CloneNotSupportedException ex) {
//            Logger.getLogger(SlimNeighborJoining.class.getName()).log(Level.SEVERE, null, ex);
//        }

        ArrayList ids = this.dmat.getIds();
        if (ids != null)
            maxId = (Integer)ids.get(0);
        else
            maxId = 0;
        float[] cdatas = this.dmat.getClassData();
        for (int i=0;i<this.dmat.getElementCount();i++) {
            tree.addNode(new ContentTree((Integer)ids.get(i),Integer.toString(i),cdatas[i]));
            if ((Integer)ids.get(i) > maxId)
                maxId = (Integer)ids.get(i);
        }
        this.numberClusters = this.dmat.getElementCount();
    }

    public SlimNeighborJoining(DistanceMatrix dmat) {
        //originalDmat = dmat;
        this.dmat = dmat;
        load();
    }

    public SlimNeighborJoining(String filename) throws IOException {
        //originalDmat = new DistanceMatrix(filename);
        dmat = new DistanceMatrix(filename);
        load();
    }

    public void createTree(HeuristicType htype) throws CloneNotSupportedException, IOException {

        //float[][] newDmat;
        float[] divergence;

        init = System.currentTimeMillis();

        System.out.println("Iniciando...");

        boolean flag = false;

        do {
            System.out.println("Calculando divergencias - iteracao "+numberClusters+"...");
            divergence = calculateDivergence();
            System.out.println("Calculando newDistanceMatrix - iteracao "+numberClusters+"...");
            //newDmat = calculateNewDistanceMatrix(divergence);
            calculateNewDistanceMatrix(divergence);
            System.out.println("Calculando smallestValue - iteracao "+numberClusters+"...");
            //calculateSmallestValue(newDmat);
            calculateSmallestValue();

            //Calculando o id do no de juncao
            //Verificando se existem nos que ainda podem ser pais. Se idJunctionNode eh igual a -1, nao ha mais,
            //e neste caso terminarei o processo usando a NJ, apenas para conectar os nos restantes.
            int idJunctionNode = HeuristicFactory.getInstance(htype).calculate(this);

            if (idJunctionNode != -1) {
                System.out.println("Setando JunctionNode - iteracao "+numberClusters+"...");
                setJunctionNode(idJunctionNode);
                numberClusters-=2;
                it++;
            }else {
                flag = true;
                DistanceMatrix dmatNJ = calculateValidDistanceMatrix();
                NeighborJoining nj = new NeighborJoining(tree, dmatNJ);
                nj.createTree(false);
                tree = nj.getTree();
            }
        } while ((numberClusters > 2)&&(!flag));

        if (!flag) {
            //Ajustando os dois ultimos nos da matriz de distancia.
            System.out.println("Acabou iteracoes, ajustando Root...");
            adjustRoot();
        }

        end = System.currentTimeMillis();
        long diff = end - init;
        System.out.println("- Tempo gasto -> " + (diff/1000.0f) + " segundos");
        
    }

    private float[] calculateDivergence() {

        float[] divergence = new float[this.dmat.getElementCount()];
        int i = 0,j;

        while (i < tree.getSize()) {
            //if (tree.getNode(i).isValid()) {
            if (tree.getNode(i).getParent()==null){
                j = i+1;
                while (j < tree.getSize()) {
                    //if ((tree.getNode(j).isValid())&&(i!=j)) {
                    if ((tree.getNode(j).getParent()==null)&&(i!=j)){
                        divergence[i] += this.dmat.getDistance(i,j);
                        divergence[j] += this.dmat.getDistance(i,j);
                    }
                    j++;
                }
            }
            i++;
        }
        return divergence;
        
    }

    private void calculateNewDistanceMatrix(float[] divergence) {

        float[][] newDmat;

        newDmat = new float[this.dmat.getElementCount() - 1][];
        for (int i=0;i<newDmat.length;i++) {
            newDmat[i] = new float[i+1];
            if (tree.getNode(i+1).getParent()==null){
                for (int j=0;j<newDmat[i].length;j++) {                    
                    if (tree.getNode(j).getParent()==null){
                        float distance = this.dmat.getDistance(i+1,j) - ((divergence[i+1] + divergence[j])/(this.dmat.getElementCount()-2));
                        newDmat[i][j] = distance;
                    }else
                        newDmat[i][j] = Float.MAX_VALUE;
                }
            }else
                for (int j=0;j<newDmat[i].length;j++) newDmat[i][j] = Float.MAX_VALUE;
        }
        dmatDivergence = newDmat;
    }

    private void calculateSmallestValue() {
    //private void calculateSmallestValue(float[][] newDmat) {
        min_x = 0;
        min_y = 0;
        float min_value = Float.MAX_VALUE;

//        for (int i=0;i<newDmat.length;i++) {
//            for (int j=0;j<newDmat[i].length;j++) {
//                if (newDmat[i][j] < min_value) {
//                    min_x = i;
//                    min_y = j;
//                    min_value = newDmat[i][j];
//                }
//            }
//        }
        for (int i=0;i<dmatDivergence.length;i++) {
            for (int j=0;j<dmatDivergence[i].length;j++) {
                if (dmatDivergence[i][j] < min_value) {
                    min_x = i;
                    min_y = j;
                    min_value = dmatDivergence[i][j];
                }
            }
        }
        
        //Vamos setar a coordenada encontrada com o maior valor real, para impedir que ela seja analisada na proxima iteracao.
        //newDmat[min_x][min_y] = Float.MAX_VALUE;
        dmatDivergence[min_x][min_y] = Float.MAX_VALUE;

        if (min_x > min_y) //Somo com um para coincidir com o indice na matriz de distancias. Foi feito assim para preservar a estrutura original da matriz.
            min_x++;
        else
            min_y++;
    }

    public float getDistance(float[][] matrix, int indexA, int indexB) {

        if (indexA == indexB)
            return 0.0f;
        else
            if (indexA < indexB) return matrix[indexB - 1][indexA];
            else return matrix[indexA - 1][indexB];

    }

    private void setJunctionNode(int id) {

        //Junction Node properties
        ContentTree junctionNode, son1, son2;
        junctionNode = tree.getNodeById(id);
        son1 = tree.getNode(min_x);
        son2 = tree.getNode(min_y);

        //junctionNode.setSon1(son1);
        //junctionNode.setSon2(son2);

        ArrayList<ContentTree> children = new ArrayList<ContentTree>();
        children.add(son1);
        children.add(son2);
        junctionNode.setChildren(children);

        //o nivel do no de juncao sera um a mais do maior nivel entre seus filhos...
        if (son1.getNivelNo() > son2.getNivelNo())
            junctionNode.setNivelNo(son1.getNivelNo()+1);
        else junctionNode.setNivelNo(son2.getNivelNo()+1);
        //d(AB) / 2 + [r(A)-r(B)] / 2(N-2)
        float distSon1, distSon2;
        distSon1 = dmat.getDistance(tree.getNodePosition(junctionNode),min_x);
        distSon2 = dmat.getDistance(tree.getNodePosition(junctionNode),min_y);

        //junctionNode.setDistSon1(distSon1);
        //junctionNode.setDistSon2(distSon2);

        ArrayList<Float> distChildren = new ArrayList<Float>();
        distChildren.add(distSon1);
        distChildren.add(distSon1);
        junctionNode.setDistChildren(distChildren);

        //adjusting sons properties
        son1.setParent(junctionNode);
        son2.setParent(junctionNode);

        son1.setDistParent(distSon1);
        son2.setDistParent(distSon2);

        System.out.println("Setando raiz filho 1");
        son1.setRoot(junctionNode.getRoot());
        System.out.println("Setando raiz filho 2");
        son2.setRoot(junctionNode.getRoot());
        System.out.println("OK");
        
    }

    private DistanceMatrix calculateValidDistanceMatrix() throws CloneNotSupportedException {

        DistanceMatrix d = (DistanceMatrix) this.dmat.clone();
        ArrayList ids = d.getIds();
        //Retirando os indices nao validos...
        for (int i=0;i<tree.getSize();i++)
            //if (!tree.getNode(i).isValid())
            if (tree.getNode(i).getParent()!=null)
                ids.remove(ids.indexOf(tree.getNode(i).getId()));
        
        int numValids = ids.size();
        float [][] tempDmat = d.getDistmatrix();
        float[][] newDmat = new float[numValids - 1][];
        for (int i=0;i<newDmat.length;i++) newDmat[i] = new float[i+1];

        int l=0,c=0;
        float minDistance=Float.MAX_VALUE,maxDistance=Float.MIN_VALUE;

        for (int i=0;i<tempDmat.length;i++)
            for (int j=0;j<tempDmat[i].length;j++)
                //if ((tree.getNode(i+1).isValid())&&(tree.getNode(j).isValid())) {
                if ((tree.getNode(i+1).getParent()==null)&&(tree.getNode(j).getParent()==null)){
                    newDmat[l][c] = tempDmat[i][j];
                    if (minDistance > tempDmat[i][j]) minDistance = tempDmat[i][j];
                    if (maxDistance < tempDmat[i][j]) maxDistance = tempDmat[i][j];
                    c++;
                    if (c >= newDmat[l].length) {
                        l++;
                        c=0;
                    }
                }
        d.setIds(ids);
        d.setElementCount(ids.size());
        d.setDistmatrix(newDmat);
        d.setMinDistance(minDistance);
        d.setMaxDistance(maxDistance);
        return(d);
    }

    private int calculateNumberValids() {
        int ret=0,indice=0;
        for (int i=0;i<tree.getSize();i++)
            //if (tree.getNode(i).isValid()) {
            if (tree.getNode(i).getParent()==null) {
                indice = i;
                ret++;
            }
        if (ret == 1) //Caso haja apenas um no valido, significa que todos os outros nos estao pendurados nele, entao eh dele que eu vou processar as arestas. Por isso o atributo m_PosJun eh setado com ele. Nos outros casos, nao preciso setar o atributo, ele eh setado no metodo ADJUSTROOT.
            tree.setRoot(tree.getNode(indice).getId());
        return ret;
    }

    private void adjustRoot() {
        //Vou ligar os dois ultimos nos que sobraram na matriz de distancia...
        ContentTree node1 = null, node2 = null;

        if (calculateNumberValids() != 1) { //Caso so haja um no valido, nao ha como junta-lo a outro no, e a arvore simplesmente esta pronta.
            int i = 0,maiorNivel = 0;
            while (i < tree.getSize()) {
                //if (tree.getNode(i).isValid()) {
                if (tree.getNode(i).getParent()==null) {
                    if (node1 == null) {
                        node1 = tree.getNode(i);
                        maiorNivel = i;
                    }
                    else {
                        node2 = tree.getNode(i);
                        if (node2.getNivelNo() > node1.getNivelNo()) maiorNivel = i;
                        i = tree.getSize();
                    }
                }
                i++;
            }
            //float dist = this.dmat.getDistance(node1.getId(),node2.getId());
            float dist = this.dmat.getDistance(tree.getNodePosition(node1),tree.getNodePosition(node2));
            node1.setParent(node2);
            node1.setDistParent(dist);
            node2.setParent(node1);
            node2.setDistParent(dist);
            tree.setRoot(tree.getNode(maiorNivel).getId());
        }
    }

    public Tree getTree() {
        return tree;
    }

    private boolean existEdge(ArrayList<Edge> edges, int sour, int targ) {

        for (int i=0;i<edges.size();i++) {
            if (((edges.get(i).getSource()==sour)&&(edges.get(i).getTarget()==targ))||
                ((edges.get(i).getSource()==targ)&&(edges.get(i).getTarget()==sour)))
                return true;
        }
        return false;
    }

    public void generateEdges() {

        ArrayList<Edge> edges = new ArrayList<Edge>();
        int sour, targ;
        float dis;

        //generating the connectivity
        for (int i = 0; i < this.getTree().getSize(); i++) {
            ContentTree son = this.getTree().getNode(i);
            if (son != null) {
                dis = son.getDistParent();
                ContentTree parent = son.getParent();
                if (parent != null) {
                    sour = parent.getId();
                    targ = son.getId();
                    if (!existEdge(edges, sour, targ)) {
                        edges.add(new Edge(sour, targ, dis));
                    }
                }
            }
        }

        this.getTree().setEdges(edges);
    }

    public float[][] getDmatDivergence() {
        return dmatDivergence;
    }

    public void setDmatDivergence(float[][] dmatD) {
        dmatDivergence = dmatD;
    }

//    public DistanceMatrix getOriginalDmat() {
//        return originalDmat;
//    }
//
//    public void setOriginalDmat(DistanceMatrix originalDmat) {
//        this.originalDmat = originalDmat;
//    }

    public int getMinX() {
        return min_x;
    }

    public int getMinY() {
        return min_y;
    }

    public DistanceMatrix getDistanceMatrix() {
        return dmat;
    }
}
