package tree.technique.packagenj;

import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import tree.basics.Tree;
import tree.technique.packagenj.PackageNJ.NJAlgorithmType;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.Technique",
name = "Neighbor-Joinning Algorithms",
description = "Project points from a multidimensional space to the plane " +
"preserving the neighborhood relations.",
howtocite = "ORIGINAL NJ: N. Saitou and M. Nei. "
+ "The Neighbor-Joining Method: A New Method for Reconstructing Phylogenetic Trees. "
+ "Molecular Biology and Evolution, 4(4):406–425, 1987 \n"
+ "RAPID NJ: M. Simonsen, T. Mailund, and C. N. Pedersen. "
+ "Rapid Neighbour-Joining. In Proceedings of WABI 2008, pages 113–122, Karlsruhe, Germany, September 2008 \n"
+ "FAST NJ: I. Elias and J. Lagergren. "
+ "Fast Neighbor Joining. In Proceedings of the 32nd International Colloquium on Automata, "
+ "Languages and Programming (ICALP’05), volume 3580, pages 1263–1274, 2005"
)
public class PackageNJConnectionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        PackageNJ nj = new PackageNJ(pnj,njAlgorithmType);
        if (dmatFile != null && !dmatFile.trim().isEmpty()) {
            matrix = null;
            dmat = null;
            try {
                tree = nj.execute(dmatFile);
            } catch (ParseException ex) {
                Logger.getLogger(PackageNJConnectionComp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else {
            if (matrix != null) {// && matrix instanceof DetailedDenseMatrix) { //using a points matrix
                AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
                //DetailedDistanceMatrix ldmat = new DetailedDistanceMatrix(matrix,diss);
                //DistanceMatrix ldmat = new DistanceMatrix(matrix,diss);
                //tree = nj.execute(ldmat); //execute nj
                tree = nj.execute(matrix, diss);
            } else if (dmat != null) {// && dmat instanceof DetailedDistanceMatrix) { //using a distance matrix
                tree = nj.execute(dmat); //execute nj
            } else {
                throw new IOException("A distance matrix or a points matrix should be provided.");
            }

        }

    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "distance matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public Tree output() {
        return tree;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new PackageNJConnectionParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        matrix = null;
        dmat = null;
    }
   
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }
    
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public boolean isDistanceMatrixProvided() {
        return (dmat != null);
    }

    public Boolean isPnj() {
        if (pnj != null) return pnj;
        else return false;
    }

    public void setPnj(Boolean pnj) {
        this.pnj = pnj;
    }

    public PackageNJ.NJAlgorithmType getNjAlgorithmType() {
        return njAlgorithmType;
    }

    public void setNjAlgorithmType(PackageNJ.NJAlgorithmType njAlgorithmType) {
        this.njAlgorithmType = njAlgorithmType;
    }

    public static final long serialVersionUID = 1L;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private Boolean pnj = false;
    private transient PackageNJConnectionParamView paramview;
    private transient Tree tree;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
    private PackageNJ.NJAlgorithmType njAlgorithmType = PackageNJ.NJAlgorithmType.RAPID;
    private String dmatFile;

    String getDmatFile() {
        return dmatFile;
    }

    void setDmatFile(String text) {
        dmatFile = text;
    }
}
