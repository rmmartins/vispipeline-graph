package tree.technique.packagenj;

import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import javax.swing.JOptionPane;
import matrix.AbstractMatrix;
import tree.basics.ContentTree;
import tree.basics.Tree;
import tree.technique.nj.NeighborJoining;


/**
 * Implementations of NJ.
 * 
 * Every implementation takes a lower triangular matrix. For every i and j such that 0<=j<=i<n, Dij 
 * should be the distance between objects i an j.  It is unimportant whether or not Dii 
 * equals zero for every i<n.  
 * 
 * For instace, for n=4 a valid matrix is:
 * 
 * {{ 0 },
 *  { 7, 0 },
 *  { 8, 5, 0 },
 *  {11, 8, 5, 0 }}
 *  
 * @author g.p.telles.
*/
public class PackageNJ {

  public enum NJAlgorithmType {

        ORIGINAL("Original Neighbor-Joining"),
        FAST("Fast Neighbor-Joining"),
        RAPID("Rapid Neighbor-Joining");

        private NJAlgorithmType(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }

        private final String name;
    }

  private boolean promotion = false;
  private NJAlgorithmType type;

  public PackageNJ(boolean p, NJAlgorithmType type) {
      this.promotion = p;
      this.type = type;
  }

  public static HashMap<String,Node> originalNJ(PexMatrix p) {

    double Sum = 0;  // The distances sum.
    double[] sum = new double[p.n];  // The distances sum for node i:
    double[] joined = new double[p.n]; // Half of the distance between two nodes joined into a new node.
    String[] newick = new String[p.n];
    HashMap<String,Node> nodes = new HashMap<String,Node>();
    int nextId = p.n;

    for (int k=0; k<p.n; k++) {
      //newick[k] = p.labels == null ? Integer.toString(k) : p.labels[k];
      newick[k] = p.labels == null ? Integer.toString(k) : Integer.toString(p.ids[k]);
      sum[k] = 0;
      nodes.put(newick[k],new Node(newick[k]));

      for (int x=0; x<k; x++) {
        sum[k] += p.M[k][x];
        Sum += p.M[k][x];
      }
      for (int x=k+1; x<p.n; x++) {
        sum[k] += p.M[x][k];
        Sum += p.M[x][k];
      }
    }

    while (p.n > 2) {
      // Evals S[i][j] and gets minimum:
      double smin = Double.MAX_VALUE;
      int imin=0, jmin=0;

      for (int i=1; i<p.n; i++) {
        for (int j=0; j<i; j++) {

          double s = (sum[i] - p.M[i][j] + sum[j] - p.M[i][j])/(2*(p.n-2)) +
                     p.M[i][j]/2 + (Sum - sum[i] - sum[j] + p.M[i][j])/(p.n-2);
          if (s<smin) {
            smin = s;
            imin = i;
            jmin = j;
          }
        }
      }

      // Stores data on new node (imin,jmin) at jmin:
      double dikmin = (sum[imin] - p.M[imin][jmin])/(p.n-2);
      double djkmin = (sum[jmin] - p.M[imin][jmin])/(p.n-2);

      String oldvalue = newick[jmin];

      newick[jmin] = "(" +
                     newick[imin] + ":" +
                     new Formatter().format(Locale.US,"%1.2f",(p.M[imin][jmin]+dikmin-djkmin)/2-joined[imin]).toString() +
                     "," +
                     newick[jmin] + ":" +
                     new Formatter().format(Locale.US,"%1.2f",(p.M[imin][jmin]+djkmin-dikmin)/2-joined[jmin]).toString() +
                     ")" + Integer.toString(nextId);//"A" + Integer.toString(D.length-n);

      String id = Integer.toString(nextId);//"A" + Integer.toString(D.length-n);
      nextId++;
      Node node = new Node(id);
      node.distance.add((p.M[imin][jmin]+dikmin-djkmin)/2-joined[imin]);
      node.distance.add((p.M[imin][jmin]+djkmin-dikmin)/2-joined[jmin]);
      if (newick[imin].lastIndexOf(")") != -1)
          node.children.add(nodes.get(newick[imin].substring(newick[imin].lastIndexOf(")")+1)));
      else
          node.children.add(nodes.get(newick[imin]));
      if (oldvalue.lastIndexOf(")") != -1)
          node.children.add(nodes.get(oldvalue.substring(oldvalue.lastIndexOf(")")+1)));
      else
          node.children.add(nodes.get(oldvalue));
      nodes.put(id,node);

      joined[jmin] = p.M[imin][jmin]/2;

      sum[jmin] = 0;
      for (int k=0; k<jmin; k++)
        if (k != imin) {
          Sum -= p.M[jmin][k];
          sum[k] -= p.M[jmin][k];
          p.M[jmin][k] = (p.M[jmin][k] + (k<imin ? p.M[imin][k] : p.M[k][imin])) / 2;
          Sum += p.M[jmin][k];
          sum[k] += p.M[jmin][k];
          sum[jmin] += p.M[jmin][k];
        }

      for (int k=jmin+1; k<p.n; k++)
        if (k != imin) {
          Sum -= p.M[k][jmin];
          sum[k] -= p.M[k][jmin];
          p.M[k][jmin] = (p.M[k][jmin] + (k<imin ? p.M[imin][k] : p.M[k][imin])) / 2;
          Sum += p.M[k][jmin];
          sum[k] += p.M[k][jmin];
          sum[jmin] += p.M[k][jmin];
        }

      sum[jmin] += p.M[imin][jmin];

      // Moves n-1 to imin:
      for (int k=0; k<imin; k++) {
        Sum -= p.M[imin][k];
        sum[k] -= p.M[imin][k];
        p.M[imin][k] = p.M[p.n-1][k];
      }

      for (int k=imin+1; k<p.n-1; k++) {
        Sum -= p.M[k][imin];
        sum[k] -= p.M[k][imin];
        p.M[k][imin] = p.M[p.n-1][k];
      }

      Sum -= p.M[p.n-1][imin];

      newick[imin] = newick[p.n-1];
      joined[imin] = joined[p.n-1];
      sum[imin] = sum[p.n-1] - p.M[p.n-1][imin];
      p.n--;
    }

    // 3 points:
    double x = (p.M[1][0]+p.M[2][0]-p.M[2][1])/2 - joined[0];
    double y = (p.M[1][0]+p.M[2][1]-p.M[2][0])/2 - joined[1];
    double z = (p.M[2][0]+p.M[2][1]-p.M[1][0])/2 - joined[2];

    String id = Integer.toString(nextId);//"A" + Integer.toString(D.length-n);
    Node node = new Node(id);
    node.distance.add(x);
    node.distance.add(y);
    node.distance.add(z);
    if (newick[0].lastIndexOf(")") != -1)
        node.children.add(nodes.get(newick[0].substring(newick[0].lastIndexOf(")")+1)));
    else
        node.children.add(nodes.get(newick[0]));
    if (newick[1].lastIndexOf(")") != -1)
        node.children.add(nodes.get(newick[1].substring(newick[1].lastIndexOf(")")+1)));
    else
        node.children.add(nodes.get(newick[1]));
    if (newick[2].lastIndexOf(")") != -1)
        node.children.add(nodes.get(newick[2].substring(newick[2].lastIndexOf(")")+1)));
    else
        node.children.add(nodes.get(newick[2]));
    nodes.put(id,node);

    return nodes;

    //printNodes(nodes);

//    return "(" +
//           newick[0] + ":" + new Formatter().format(Locale.US,"%1.2f",x) + "," +
//           newick[1] + ":" + new Formatter().format(Locale.US,"%1.2f",y) + "," +
//           newick[2] + ":" + new Formatter().format(Locale.US,"%1.2f",z) +
//           ")" + "A" + Integer.toString(D.length-n) + ":0.0";
  }

  public static HashMap<String,Node> fastNJ(PexMatrix p) {

    class closestPair {
      public int j;
      public double d;
    }

    int nextId = p.n;
    String[] newick = new String[p.n];
    HashMap<String,Node> nodes = new HashMap<String,Node>();
    for (int i=0; i<p.n; i++) {
      //newick[i] = p.labels == null ? Integer.toString(i) : p.labels[i];
        newick[i] = (p.labels == null ? Integer.toString(i) : Integer.toString(p.ids[i]));
      nodes.put(newick[i],new Node(newick[i]));
    }

    // Evals the distances sum for node k:
    double[] sum = new double[p.n];
    for (int k=0; k<p.n; k++) {
      sum[k] = 0;
      for (int x=0; x<k; x++)
        sum[k] += p.M[k][x];
      for (int x=k+1; x<p.n; x++)
        sum[k] += p.M[x][k];
    }

    closestPair[] C = new closestPair[p.n+1];
    int c = p.n;
    // Builds the set of closest pairs:
    for (int i=0; i<p.n; i++) {

      C[i] = new closestPair();

      C[i].d = Double.MAX_VALUE;

      for (int j=0; j<i; j++) {
        double s = p.M[i][j] - (sum[i]+sum[j]) / (p.n-2);
        if (s<C[i].d) {
          C[i].d = s;
          C[i].j = j;
        }
      }

      for (int j=i+1; j<p.n; j++) {
        double s = p.M[j][i] - (sum[i]+sum[j]) / (p.n-2);
        if (s<C[i].d) {
          C[i].d = s;
          C[i].j = j;
        }
      }
    }

    while (p.n > 3) {

      // Gets minimum:
      int cmin = 0;
      for (int i=1; i<c; i++)
        if (C[i].d < C[cmin].d)
          cmin = i;

      int imin, jmin;

      if (cmin > C[cmin].j) {
       imin = cmin;
       jmin = C[cmin].j;
      }
      else {
         imin = C[cmin].j;
         jmin = cmin;
      }

      // Data on new node (imin,jmin) will be stored at jmin.
      // Evals branch lengths Lik and Ljk:
      double lik = 0.5 * (p.M[imin][jmin] + ((sum[imin]-sum[jmin])/(p.n-2)));
      double ljk = p.M[imin][jmin] - lik;

      // Updates tree:
      String oldvalue = newick[jmin];

      newick[jmin] = "(" +
        newick[imin] + ":" +
        new Formatter().format(Locale.US,"%.2f",lik).toString() +
        "," +
        newick[jmin] + ":" +
        new Formatter().format(Locale.US,"%.2f",ljk).toString() +
        ")" + Integer.toString(nextId);//"A" + Integer.toString(D.length-n);

      String id = Integer.toString(nextId);//"A" + Integer.toString(D.length-n);
      nextId++;
      Node node = new Node(id);
      node.distance.add(Math.abs(lik));
      node.distance.add(Math.abs(ljk));
      if (newick[imin].lastIndexOf(")") != -1)
          node.children.add(nodes.get(newick[imin].substring(newick[imin].lastIndexOf(")")+1)));
      else
          node.children.add(nodes.get(newick[imin]));
      if (oldvalue.lastIndexOf(")") != -1)
          node.children.add(nodes.get(oldvalue.substring(oldvalue.lastIndexOf(")")+1)));
      else
          node.children.add(nodes.get(oldvalue));
      nodes.put(id,node);

      // Updates D and sum:
      sum[jmin] = 0;
      for (int k=0; k<jmin; k++)
        if (k != imin) {
          sum[k] -= p.M[jmin][k];
          p.M[jmin][k] = (p.M[jmin][k] + (k<imin ? p.M[imin][k] : p.M[k][imin]) - p.M[imin][jmin]) / 2;
          sum[k] += p.M[jmin][k];
          sum[jmin] += p.M[jmin][k];
        }

      for (int k=jmin+1; k<p.n; k++)
        if (k != imin) {
          sum[k] -= p.M[k][jmin];
          p.M[k][jmin] = (p.M[k][jmin] + (k<imin ? p.M[imin][k] : p.M[k][imin]) - p.M[imin][jmin]) / 2;
          sum[k] += p.M[k][jmin];
          sum[jmin] += p.M[k][jmin];
        }

      sum[jmin] += p.M[imin][jmin];

      // Moves n-1 onto imin:
      
      for (int k=0; k<imin; k++) {
        sum[k] -= p.M[imin][k];
        p.M[imin][k] = p.M[p.n-1][k];
      }

      for (int k=imin+1; k<p.n-1; k++) {
        sum[k] -= p.M[k][imin];
        p.M[k][imin] = p.M[p.n-1][k];
      }

      newick[imin] = newick[p.n-1];
      sum[imin] = sum[p.n-1] - p.M[p.n-1][imin];

      p.n--;

      // Updates the closest pairs:

      // Moves n-1 onto imin:
      c--;
      closestPair a = C[imin];
      C[imin] = C[c];
      C[c] = a;

      for (int i=0; i<c; i++) {

        // The closest pairs for the new node, having j=imin or j=jmin must be rebuilt:
        if (i == jmin || C[i].j == imin || C[i].j == jmin) {

          double s;
          C[i].d = Double.MAX_VALUE;

          for (int j=0; j<i; j++) {
            s = p.M[i][j] - (sum[i]+sum[j]) / (p.n-2);
            if (s<C[i].d) {
              C[i].d = s;
              C[i].j = j;
            }
          }

          for (int j=i+1; j<p.n; j++) {
            s = p.M[j][i] - (sum[i]+sum[j]) / (p.n-2);
            if (s<C[i].d) {
              C[i].d = s;
              C[i].j = j;
            }
          }
        }

        // Any other closest pair must be updated:
        else {
          if(C[i].j == p.n)
            C[i].j = imin;
        }

      }

    }

    // 3 points:
    double x = (p.M[1][0]+p.M[2][0]-p.M[2][1])/2;
    double y = (p.M[1][0]+p.M[2][1]-p.M[2][0])/2;
    double z = (p.M[2][0]+p.M[2][1]-p.M[1][0])/2;

    String id = Integer.toString(nextId);//"A" + Integer.toString(D.length-n);
    Node node = new Node(id);
    node.distance.add(x);
    node.distance.add(y);
    node.distance.add(z);
    if (newick[0].lastIndexOf(")") != -1)
        node.children.add(nodes.get(newick[0].substring(newick[0].lastIndexOf(")")+1)));
    else
        node.children.add(nodes.get(newick[0]));
    if (newick[1].lastIndexOf(")") != -1)
        node.children.add(nodes.get(newick[1].substring(newick[1].lastIndexOf(")")+1)));
    else
        node.children.add(nodes.get(newick[1]));
    if (newick[2].lastIndexOf(")") != -1)
        node.children.add(nodes.get(newick[2].substring(newick[2].lastIndexOf(")")+1)));
    else
        node.children.add(nodes.get(newick[2]));
    nodes.put(id,node);

    return nodes;
  }

  public static HashMap<String,Node> rapidNJ(PexMatrix p) {

    HashMap<String,Node> nodes = new HashMap<String,Node>();
    int nextId = p.n;

    // Makes room for a larger matrix:

    double[][] E = new double[2*p.n-3][];
    for (int i=0; i<p.n; i++)
      E[i] = p.M[i];
    p.M = E;

    // Labels:
    String[] newick = new String[2*p.n-3];
    for (int i=0; i<p.n; i++) {
      //newick[i] = (p.labels == null ? Integer.toString(i) : p.labels[i]);
        newick[i] = (p.labels == null ? Integer.toString(i) : Integer.toString(p.ids[i]));
      nodes.put(newick[i],new Node(newick[i]));
    }

    // The sorted matrix S and the indices I:
    double S[][] = new double[2*p.n-3][];
    int I[][] = new int[2*p.n-3][];   // I[i][j] has the position of S[i][j] in D[i].

    for (int i=0; i<p.n; i++) {
      S[i] = new double[i+1];
      I[i] = new int[i+1];

      for (int j=0; j<i; j++)
        S[i][j] = p.M[i][j];

      Sort.qsort(S[i], I[i], 0, i);
    }

    // Evals the distances sum for every node k and records the maximum sum:
    double[] sum = new double[2*p.n-3];
    double smax = Double.MIN_VALUE;

    for (int k=0; k<p.n; k++) {
      sum[k] = 0;
      for (int x=0; x<k; x++)
        sum[k] += p.M[k][x];
      for (int x=k+1; x<p.n; x++)
        sum[k] += p.M[x][k];

      if (sum[k]>smax)
        smax = sum[k];
    }

    // The last row in the matrix is l, the number of active rows is n:
    int l = p.n;

    while (p.n > 3) {

      // Gets minimum:
      double s, smin = Double.MAX_VALUE;
      int imin=0, jmin=0;

      for (int i=l-1; i>=0; i--)
        if (p.M[i] != null)
          for (int j=0; j<i; j++)
            if (p.M[I[i][j]] != null)
              if (S[i][j] < Double.MAX_VALUE && S[i][j] - (sum[i]+smax) / (p.n-2) < smin) {
                s = S[i][j] - (sum[i]+sum[I[i][j]]) / (p.n-2);
                if (s < smin) {
                  smin = s;
                  imin = i;
                  jmin = I[i][j];
                }
              }
              else
                break;

      // Data on new node (imin,jmin) will be stored at l.
      // Evals branch lengths Lik and Ljk:
      double lik = 0.5 * (p.M[imin][jmin] + ((sum[imin]-sum[jmin])/(p.n-2)));
      double ljk = p.M[imin][jmin] - lik;

      // Updates tree:
      //String oldvalue = newick[l];
      newick[l] = "(" +
        newick[imin] + ":" +
        new Formatter().format(Locale.US,"%.2f",lik).toString() +
        "," +
        newick[jmin] + ":" +
        new Formatter().format(Locale.US,"%.2f",ljk).toString() +
        ")" + Integer.toString(nextId);//"A" + Integer.toString((l-n)/2);

      String id = Integer.toString(nextId);//"A" + Integer.toString(D.length-n);
      nextId++;
      Node node = new Node(id);
      node.distance.add(lik);
      node.distance.add(ljk);
      if (newick[imin].lastIndexOf(")") != -1)
          node.children.add(nodes.get(newick[imin].substring(newick[imin].lastIndexOf(")")+1)));
      else
          node.children.add(nodes.get(newick[imin]));
      if (newick[jmin].lastIndexOf(")") != -1)
          node.children.add(nodes.get(newick[jmin].substring(newick[jmin].lastIndexOf(")")+1)));
      else
          node.children.add(nodes.get(newick[jmin]));
      nodes.put(id,node);

      newick[imin] = newick[jmin] = null;

      // Updates D, S, I, sum:
      sum[l] = 0;
      p.M[l] = new double[l+1];
      S[l] = new double[l+1];
      I[l] = new int[l+1];

      for (int k=0; k<jmin; k++)
        if (k != imin && p.M[k] != null) {
          sum[k] -= p.M[jmin][k];
          p.M[l][k] = (p.M[jmin][k] + (k<imin ? p.M[imin][k] : p.M[k][imin]) - p.M[imin][jmin]) / 2;
          S[l][k] = p.M[l][k];
          sum[l] += p.M[l][k];
          sum[k] += p.M[l][k];
        }
        else  // Positions already removed at row l will be "sorted-out"
          S[l][k] = Double.MAX_VALUE;

      for (int k=jmin+1; k<l; k++)
        if (k != imin && p.M[k] != null) {
          sum[k] -= p.M[k][jmin];
          p.M[l][k] = (p.M[k][jmin] + (k<imin ? p.M[imin][k] : p.M[k][imin]) - p.M[imin][jmin]) / 2;
          S[l][k] = p.M[l][k];
          sum[l] += p.M[l][k];
          sum[k] += p.M[l][k];
        }
        else  // Positions already removed at row l will be "sorted-out"
          S[l][k] = Double.MAX_VALUE;

      for (int k=0; k<imin; k++)
        if (k != jmin && p.M[k] != null)
          sum[k] -= p.M[imin][k];

      for (int k=imin+1; k<l; k++)
        if (k != jmin && p.M[k] != null)
          sum[k] -= p.M[k][imin];

      Sort.qsort(S[l],I[l],0,l);

      p.M[imin] = p.M[jmin] = null;
      S[imin] = S[jmin] = null;
      I[imin] = I[jmin] = null;


      // Updates the maximum sum:
      smax = Double.MIN_VALUE;
      for (int k=0; k<l; k++)
        if (p.M[k] != null && sum[k] > smax)
          smax = sum[k];

      p.n--;
      l++;
    }

    // 3 points:
    // Finds the points out:
    int i,j,k;

    int a=0;
    for ( ; ; a++)
      if (p.M[a] != null) {
        i = a++;
        break;
      }

    for ( ; ; a++)
      if (p.M[a] != null) {
        j = a++;
        break;
      }

    for (; ; a++)
      if (p.M[a] != null) {
        k = a;
        break;
      }

    double x = (p.M[j][i]+p.M[k][i]-p.M[k][j])/2;
    double y = (p.M[j][i]+p.M[k][j]-p.M[k][i])/2;
    double z = (p.M[k][i]+p.M[k][j]-p.M[j][i])/2;

    String id = Integer.toString(nextId);//"A" + Integer.toString(D.length-n);
    Node node = new Node(id);
    node.distance.add(x);
    node.distance.add(y);
    node.distance.add(z);
    if (newick[i].lastIndexOf(")") != -1)
        node.children.add(nodes.get(newick[i].substring(newick[i].lastIndexOf(")")+1)));
    else
        node.children.add(nodes.get(newick[i]));
    if (newick[j].lastIndexOf(")") != -1)
        node.children.add(nodes.get(newick[j].substring(newick[j].lastIndexOf(")")+1)));
    else
        node.children.add(nodes.get(newick[j]));
    if (newick[k].lastIndexOf(")") != -1)
        node.children.add(nodes.get(newick[k].substring(newick[k].lastIndexOf(")")+1)));
    else
        node.children.add(nodes.get(newick[k]));
    nodes.put(id,node);

    return nodes;

  }

//  public static void printNodes(HashMap<String,Node> nodes) {
//      Iterator it = nodes.entrySet().iterator();
//      Node valor;
//      while (it.hasNext()) {
//          Map.Entry e = (Map.Entry) it.next();
//          valor = (Node)e.getValue();
//          System.out.println(valor.toString());
//      }
//  }

  public Tree createTree(HashMap<String,Node> nodes, int n, int[] ids, String[] labels, float[] cdatas) {

      Tree tree = new Tree();
      tree.setType(this.type.toString());

      int maxId = Integer.MIN_VALUE;

      //Inserindo os nos reais, ja presentes na matriz...
      for (int i=0;i<n;i++) {
          tree.addNode(new ContentTree(ids[i],labels[i],cdatas[i]));
//          if (ids[i] > maxId) maxId = ids[i];
      }

      ids = null;
      labels = null;
      cdatas = null;

      ArrayList<Integer> virtualLabels = new ArrayList<Integer>();

      Iterator it = nodes.entrySet().iterator();
      while (it.hasNext()) {
          Map.Entry e = (Map.Entry) it.next();
          String id = (String)e.getKey();
          Node node = (Node)e.getValue();
          if (!node.children.isEmpty()) {//apenas nos virtuais possuem filhos, e essa eh a condicao usada para adiciona-los
              virtualLabels.add(Integer.parseInt(id));
              if (Integer.parseInt(id) > maxId) maxId = Integer.parseInt(id);
          }
      }
      //Ordenando as chaves virtuais, para que o acesso seja feito na ordem de criação...
      Collections.sort(virtualLabels);
      //labelsV.addAll(labelsV2);

      //Inserindo os nos virtuais
      for (int i=0;i<virtualLabels.size();i++) {
          Node node = nodes.get(virtualLabels.get(i).toString());
          if (node != null) {
              //ContentTree ct = new ContentTree(++maxId,virtualLabels.get(i).toString());
              ContentTree ct = new ContentTree(virtualLabels.get(i),virtualLabels.get(i).toString());
              ct.setValid(false);
              tree.addNode(ct);
          }
      }

      virtualLabels = null;
      System.gc();

      for (int i=0;i<tree.getSize();i++) {
          //Node node = nodes.get(tree.getNode(i).getLabel());
          Node node = nodes.get(Integer.toString(tree.getNode(i).getId()));
          if (node != null) {
              int maxNivel = -1;
              //Setando os filhos e as distancias...
//              if (node.children.size() == 3) { //No raiz. Vou criar mais um no virtual com dois filhos, e depois juntar o terceiro em outro nó, que será a raiz.
//                  ContentTree son;
//                  for (int ii=0;ii<2;ii++) {
//                      son = tree.getNodeById(Integer.parseInt(node.children.get(ii).id));
//                      if (son != null) {
//                          son.setParent(tree.getNode(i));
//                          son.setDistParent(node.distance.get(ii).floatValue());
//                          son.setRoot(tree.getNode(i).getRoot());
//                          if (son.getNivelNo() > maxNivel) maxNivel = son.getNivelNo();
//                          tree.getNode(i).getChildren().add(son);
//                          tree.getNode(i).getDistChildren().add(node.distance.get(ii).floatValue());
//                      }
//                  }
//                  ContentTree ct = new ContentTree(maxId+1,Integer.toString(maxId+1));
//                  ct.setValid(false);
//                  tree.addNode(ct);
//                  ct.getChildren().add(tree.getNode(i));
//                  ct.getDistChildren().add(0.0f);
//                  tree.getNode(i).setParent(ct);
//                  tree.getNode(i).setDistParent(0.0f);
//                  if (tree.getNode(i).getNivelNo() > maxNivel) maxNivel = tree.getNode(i).getNivelNo();
//                  son = tree.getNodeById(Integer.parseInt(node.children.get(2).id));
//                  if (son != null) {
//                      ct.getChildren().add(son);
//                      ct.getDistChildren().add(0.0f);
//                      son.setParent(ct);
//                      son.setDistParent(0.0f);
//                  }
//                  if (son.getNivelNo() > maxNivel) maxNivel = son.getNivelNo();
//                  ct.setNivelNo(maxNivel+1);
//                  ct.setRoot(ct);
//                  tree.setRoot(ct.getId());
//              }else {
                  for (int j=0;j<node.children.size();j++) {
                      ContentTree son = tree.getNodeById(Integer.parseInt(node.children.get(j).id));
                      if (son != null) {
                          son.setParent(tree.getNode(i));
                          son.setDistParent(node.distance.get(j).floatValue());
                          son.setRoot(tree.getNode(i).getRoot());
                          if (son.getNivelNo() > maxNivel) maxNivel = son.getNivelNo();
                          tree.getNode(i).getChildren().add(son);
                          tree.getNode(i).getDistChildren().add(node.distance.get(j).floatValue());
                      }
                  }
//              }
              if (maxNivel != -1)
                  tree.getNode(i).setNivelNo(maxNivel+1);
          }
      }
      nodes = null;

      if (promotion) NeighborJoining.promoteLeafs(tree);
      //tree.printNodes();
      tree.generateEdges();
      tree.updateDeepChildCount();
      //tree.printEdges();
      return tree;
  }

  public Tree execute(DistanceMatrix dmat) {

//      String[] labels = new String[dmat.getElementCount()];
//      if (!dmat.getIds().isEmpty())
//          for (int i=0;i<dmat.getIds().size();i++) labels[i] = Integer.toString(dmat.getIds().get(i));
      PexMatrix pexMatrix = DistanceMatrixReader.loadPex(dmat);
      dmat = null;
      if (pexMatrix != null)
          return constructTree(pexMatrix);
      else return null;
            
  }

  public Tree execute(AbstractMatrix matrix, AbstractDissimilarity diss) {

//      String[] labels = new String[matrix.getIds().size()];
//      for (int i=0;i<matrix.getIds().size();i++) labels[i] = Integer.toString(matrix.getIds().get(i));
      PexMatrix pexMatrix = DistanceMatrixReader.loadPex(matrix,diss);
      matrix = null;
      if (pexMatrix != null)
          return constructTree(pexMatrix);
      else return null;

  }

  public Tree execute(String dmatFile) throws IOException, ParseException {

      //String[] labels = new String[matrix.getIds().size()];
      //for (int i=0;i<matrix.getIds().size();i++) labels[i] = Integer.toString(matrix.getIds().get(i));
      PexMatrix pexMatrix = DistanceMatrixReader.loadPex(dmatFile);
      dmatFile = null;
      if (pexMatrix != null)
          return constructTree(pexMatrix);
      else return null;

  }

  private Tree constructTree(PexMatrix pexMatrix) {

      Tree tree = null;
      HashMap<String, Node> nodes = null;

      long linit, lend, diff, total;
      linit = System.currentTimeMillis();
      int n = pexMatrix.n;
      try {
          if (type.equals(NJAlgorithmType.ORIGINAL)) {
              nodes = originalNJ(pexMatrix);//.M,pexMatrix.n,pexMatrix.labels);
          } else if (type.equals(NJAlgorithmType.FAST)) {
              nodes = fastNJ(pexMatrix);
          } else if (type.equals(NJAlgorithmType.RAPID)) {
              nodes = rapidNJ(pexMatrix);
          }
          if (nodes != null) {
              lend = System.currentTimeMillis();
              diff = lend - linit;
              total = diff;
              System.out.println("Time spent calculating ("+type+") -> " + (diff/1000.0f) + " seconds.");
              linit = System.currentTimeMillis();
              pexMatrix.M = null;
              tree = createTree(nodes,n,pexMatrix.ids,pexMatrix.labels,pexMatrix.classes);
              pexMatrix = null;
              lend = System.currentTimeMillis();
              diff = lend - linit;
              total += diff;
              System.out.println("- Time spent (VisPipeline Tree Creation) -> " + (diff/1000.0f) + " seconds.");
              System.out.println("TOTAL TIME PACKAGE NJ -> " + (total/1000.0f) + " seconds.");
          } else
              JOptionPane.showMessageDialog(null,"Error collecting tree nodes!");
      } catch (Exception e) {
          e.printStackTrace();
      } finally {
          return tree;
      }

  }

  //---------------- METODOS ORIGINAIS ---------------------

  /**
   * NJ as proposed by Saitou and Nei.
   * 
   * @param D A lower triangular distance matrix.  D is destroyed during processing.
   * @param n The number of objects.  The lower triangular submatrix whose indices 
   * are in the range [0..n) will be used to build the tree.
   * @param labels An optional list of labels for each object.
   * 
   * @return The Newick representation for the tree built using neighbor-joining.
   */
  public static String original(double[][] D, int n, String[] labels) {
  
    double Sum = 0;  // The distances sum.
    double[] sum = new double[n];  // The distances sum for node i: 
    double[] joined = new double[n]; // Half of the distance between two nodes joined into a new node.
    String[] newick = new String[n];
      
    for (int k=0; k<n; k++) {
      newick[k] = labels == null ? Integer.toString(k) : labels[k];
      sum[k] = 0;
            
      for (int x=0; x<k; x++) {
        sum[k] += D[k][x];
        Sum += D[k][x];
      }
      for (int x=k+1; x<n; x++) {
        sum[k] += D[x][k];
        Sum += D[x][k];
      }
    }
  
    while (n > 3) {
      // Evals S[i][j] and gets minimum:
      double smin = Double.MAX_VALUE;
      int imin=0, jmin=0;
        
      for (int i=1; i<n; i++) {
        for (int j=0; j<i; j++) {
          
          double s = (sum[i] - D[i][j] + sum[j] - D[i][j])/(2*(n-2)) + 
                     D[i][j]/2 + (Sum - sum[i] - sum[j] + D[i][j])/(n-2);
          if (s<smin) {
            smin = s;
            imin = i;
            jmin = j;
          }
        }
      }
  
      // Stores data on new node (imin,jmin) at jmin:
      double dikmin = (sum[imin] - D[imin][jmin])/(n-2);
      double djkmin = (sum[jmin] - D[imin][jmin])/(n-2);

      newick[jmin] = "(" +
                     newick[imin] + ":" +
                     new Formatter().format(Locale.US,"%1.2f",(D[imin][jmin]+dikmin-djkmin)/2-joined[imin]).toString() +
                     "," +
                     newick[jmin] + ":" +
                     new Formatter().format(Locale.US,"%1.2f",(D[imin][jmin]+djkmin-dikmin)/2-joined[jmin]).toString() +
                     ")" + "A" + Integer.toString(D.length-n);

      joined[jmin] = D[imin][jmin]/2;

      sum[jmin] = 0;
      for (int k=0; k<jmin; k++) 
        if (k != imin) { 
          Sum -= D[jmin][k];
          sum[k] -= D[jmin][k];
          D[jmin][k] = (D[jmin][k] + (k<imin ? D[imin][k] : D[k][imin])) / 2;
          Sum += D[jmin][k];          
          sum[k] += D[jmin][k];
          sum[jmin] += D[jmin][k];
        }
  
      for (int k=jmin+1; k<n; k++) 
        if (k != imin) { 
          Sum -= D[k][jmin];
          sum[k] -= D[k][jmin];
          D[k][jmin] = (D[k][jmin] + (k<imin ? D[imin][k] : D[k][imin])) / 2;
          Sum += D[k][jmin];
          sum[k] += D[k][jmin];
          sum[jmin] += D[k][jmin];
        }
      
      sum[jmin] += D[imin][jmin];
      
      // Moves n-1 to imin:
      for (int k=0; k<imin; k++) {
        Sum -= D[imin][k];
        sum[k] -= D[imin][k];
        D[imin][k] = D[n-1][k];
      }
        
      for (int k=imin+1; k<n-1; k++) {
        Sum -= D[k][imin];
        sum[k] -= D[k][imin];
        D[k][imin] = D[n-1][k];
      }
      
      Sum -= D[n-1][imin];

      newick[imin] = newick[n-1];
      joined[imin] = joined[n-1];
      sum[imin] = sum[n-1] - D[n-1][imin];
      n--;
    }
  
    // 3 points:
    double x = (D[1][0]+D[2][0]-D[2][1])/2 - joined[0];
    double y = (D[1][0]+D[2][1]-D[2][0])/2 - joined[1];
    double z = (D[2][0]+D[2][1]-D[1][0])/2 - joined[2];

    return "(" +
           newick[0] + ":" + new Formatter().format(Locale.US,"%1.2f",x) + "," +
           newick[1] + ":" + new Formatter().format(Locale.US,"%1.2f",y) + "," +
           newick[2] + ":" + new Formatter().format(Locale.US,"%1.2f",z) +
           ")" + "A" + Integer.toString(D.length-n) + ":0.0";
  }

  /**
   * NJ implementing the arithmetic proposed by Studier and Keppler.
   * 
   * J.A. Studier, K.J. Keppler. A note on the Neighbor-Joining algorithm of Saitou and Nei. 
   * Mol Biol. Evol. v.5, 1988.
   * 
   * @param D A lower triangular distance matrix.  D is destroyed during processing.
   * @param n The number of objects.  The lower triangular submatrix whose indices 
   * are in the range [0..n) will be used to build the tree.
   * @param labels An optional list of labels for each object.
   * 
   * @return The Newick representation for the tree built using neighbor-joining.
   */
  public static String nj(double[][] D, int n, String[] labels) {
    
    String[] newick = new String[n];
    for (int i=0; i<n; i++) 
      newick[i] = labels == null ? Integer.toString(i) : labels[i];
  
    // Evals the distances sum for node k: 
    double[] sum = new double[n];
    for (int k=0; k<n; k++) {
      sum[k] = 0;
      for (int x=0; x<k; x++)
        sum[k] += D[k][x];
      for (int x=k+1; x<n; x++)
        sum[k] += D[x][k];
    }
  
    long ct = 0;
    
    while (n > 3) {
      
      // Gets minimum:
      double s, smin = Double.MAX_VALUE;
      int imin=0, jmin=0;
        
      for (int i=1; i<n; i++) {
        for (int j=0; j<i; j++) {
          ct++;
          s = D[i][j] - (sum[i]+sum[j]) / (n-2); 
          if (s<smin) {
            smin = s;
            imin = i;
            jmin = j;
          }
        }
      }
  
      // Data on new node (imin,jmin) will be stored at jmin.
      // Evals branch lengths Lik and Ljk:
      double lik = 0.5 * (D[imin][jmin] + ((sum[imin]-sum[jmin])/(n-2)));
      double ljk = D[imin][jmin] - lik;

      // Updates tree:
      newick[jmin] = "(" +
        newick[imin] + ":" +
        new Formatter().format(Locale.US,"%.2f",lik).toString() +
        "," +
        newick[jmin] + ":" +
        new Formatter().format(Locale.US,"%.2f",ljk).toString() + 
        ")" + "A" + Integer.toString(D.length-n);

      // Updates D and sum:  
      sum[jmin] = 0;
      for (int k=0; k<jmin; k++) 
        if (k != imin) { 
          sum[k] -= D[jmin][k];
          D[jmin][k] = (D[jmin][k] + (k<imin ? D[imin][k] : D[k][imin]) - D[imin][jmin]) / 2;
          sum[k] += D[jmin][k];
          sum[jmin] += D[jmin][k];
        }
  
      for (int k=jmin+1; k<n; k++) 
        if (k != imin) {
          sum[k] -= D[k][jmin];
          D[k][jmin] = (D[k][jmin] + (k<imin ? D[imin][k] : D[k][imin]) - D[imin][jmin]) / 2;
          sum[k] += D[k][jmin];
          sum[jmin] += D[k][jmin];
        }
      
      sum[jmin] += D[imin][jmin];

      // Moves n-1 onto imin:
      for (int k=0; k<imin; k++) {
        sum[k] -= D[imin][k];
        D[imin][k] = D[n-1][k];
      }
        
      for (int k=imin+1; k<n-1; k++) {
        sum[k] -= D[k][imin];
        D[k][imin] = D[n-1][k];
      }
      
      newick[imin] = newick[n-1];
      newick[n-1] = null;
      sum[imin] = sum[n-1] - D[n-1][imin];
      n--;
    }
    
    
    System.out.printf("ct %d",ct);
    
    // 3 points:
    double x = (D[1][0]+D[2][0]-D[2][1])/2;
    double y = (D[1][0]+D[2][1]-D[2][0])/2;
    double z = (D[2][0]+D[2][1]-D[1][0])/2;
    
    return "(" +
           newick[0] + ":" + new Formatter().format(Locale.US,"%.2f",x) + "," +
           newick[1] + ":" + new Formatter().format(Locale.US,"%.2f",y) + "," +
           newick[2] + ":" + new Formatter().format(Locale.US,"%.2f",z) +
           ")" + "A" + Integer.toString(D.length-n) + ":0.0";
  }

  /**
  * An implementation of Fast NJ.
  *
  * I. Elias, J. Lagergren. Fast Neighbor Joining. Proc. of ICALP 2005.
  *
  * @param D A lower triangular distance matrix.  D is destroyed during processing.
  * @param n The number of objects.  The lower triangular submatrix whose indices
  * are in the range [0..n) will be used to build the tree.
  * @param labels An optional list of labels for each object.
  *
  * @return The Newick representation for the tree built using neighbor-joining.
  */
  public static String fast(double[][] D, int n, String[] labels) {

    class closestPair {
      public int j;
      public double d;
    }

    String[] newick = new String[n];
    for (int i=0; i<n; i++) {
      newick[i] = labels == null ? Integer.toString(i) : labels[i];
    }

    // Evals the distances sum for node k:
    double[] sum = new double[n];
    for (int k=0; k<n; k++) {
      sum[k] = 0;
      for (int x=0; x<k; x++)
        sum[k] += D[k][x];
      for (int x=k+1; x<n; x++)
        sum[k] += D[x][k];
    }

    closestPair[] C = new closestPair[n+1];
    int c = n;
    // Builds the set of closest pairs:
    for (int i=0; i<n; i++) {

      C[i] = new closestPair();

      C[i].d = Double.MAX_VALUE;

      //System.out.printf("i %d :",i);

      for (int j=0; j<i; j++) {

        //System.out.printf("%d ",j);

        double s = D[i][j] - (sum[i]+sum[j]) / (n-2);
        if (s<C[i].d) {
          C[i].d = s;
          C[i].j = j;
        }
      }

      for (int j=i+1; j<n; j++) {

        //System.out.printf("%d ",j);

        double s = D[j][i] - (sum[i]+sum[j]) / (n-2);
        if (s<C[i].d) {
          C[i].d = s;
          C[i].j = j;
        }
      }
      //System.out.println();
    }


    while (n > 3) {
      /*
      for (int i = 0; i<n; i++) {
        for (int j = 0; j<=i; j++) {
          System.out.printf("%.3f ",D[i][j]);
        }
        System.out.println();
      }
      */

      // Gets minimum:
      int cmin = 0;
      for (int i=1; i<c; i++)
        if (C[i].d < C[cmin].d)
          cmin = i;

      int imin, jmin;


      //for (int i=0; i<c; i++) {
        //System.out.printf("i %d j %d d %.3f\n",i,C[i].j,C[i].d);
      //}



      if (cmin > C[cmin].j) {
       imin = cmin;
       jmin = C[cmin].j;
      }
      else {
         imin = C[cmin].j;
         jmin = cmin;
      }

      //System.out.printf("imin %d jmin %d cmin %d\n",imin,jmin,cmin);

      // Data on new node (imin,jmin) will be stored at jmin.
      // Evals branch lengths Lik and Ljk:
      double lik = 0.5 * (D[imin][jmin] + ((sum[imin]-sum[jmin])/(n-2)));
      double ljk = D[imin][jmin] - lik;

      // Updates tree:
      String oldvalue = newick[jmin];

      newick[jmin] = "(" +
        newick[imin] + ":" +
        new Formatter().format(Locale.US,"%.2f",lik).toString() +
        "," +
        newick[jmin] + ":" +
        new Formatter().format(Locale.US,"%.2f",ljk).toString() +
        ")" + "A" + Integer.toString(D.length-n);

      // Updates D and sum:
      sum[jmin] = 0;
      for (int k=0; k<jmin; k++)
        if (k != imin) {
          sum[k] -= D[jmin][k];
          D[jmin][k] = (D[jmin][k] + (k<imin ? D[imin][k] : D[k][imin]) - D[imin][jmin]) / 2;
          sum[k] += D[jmin][k];
          sum[jmin] += D[jmin][k];
        }

      for (int k=jmin+1; k<n; k++)
        if (k != imin) {
          sum[k] -= D[k][jmin];
          D[k][jmin] = (D[k][jmin] + (k<imin ? D[imin][k] : D[k][imin]) - D[imin][jmin]) / 2;
          sum[k] += D[k][jmin];
          sum[jmin] += D[k][jmin];
        }

      sum[jmin] += D[imin][jmin];

      // Moves n-1 onto imin:
      /*
      System.out.printf("imin %d jmin %d n %d\n",imin,jmin,n);

      if (n==391){
        for (int i=0; i<c; i++) {
          System.out.printf("i %d j %d d %.3f\n",C[i].i,C[i].j,C[i].d);
        }
      }
      */

      for (int k=0; k<imin; k++) {
        sum[k] -= D[imin][k];
        D[imin][k] = D[n-1][k];
      }

      for (int k=imin+1; k<n-1; k++) {
        sum[k] -= D[k][imin];
        D[k][imin] = D[n-1][k];
      }

      newick[imin] = newick[n-1];
      sum[imin] = sum[n-1] - D[n-1][imin];

      n--;

      //System.out.printf("imin %d jmin %d cmin %d\n",imin,jmin,cmin);

      // Updates the closest pairs:

      // Moves n-1 onto imin:
      c--;
      closestPair a = C[imin];
      C[imin] = C[c];
      C[c] = a;

      for (int i=0; i<c; i++) {

        // The closest pairs for the new node, having j=imin or j=jmin must be rebuilt:
        if (i == jmin || C[i].j == imin || C[i].j == jmin) {

          double s;
          C[i].d = Double.MAX_VALUE;

          for (int j=0; j<i; j++) {
            s = D[i][j] - (sum[i]+sum[j]) / (n-2);
            if (s<C[i].d) {
              C[i].d = s;
              C[i].j = j;
            }
          }

          for (int j=i+1; j<n; j++) {
            s = D[j][i] - (sum[i]+sum[j]) / (n-2);
            if (s<C[i].d) {
              C[i].d = s;
              C[i].j = j;
            }
          }
        }

        // Any other closest pair must be updated:
        else {
          if(C[i].j == n)
            C[i].j = imin;

          /*
          int ai = i, aj = C[i].j;
          if (i < C[i].j) {
            ai = C[i].j;
            aj = i;
          }

          C[i].d = D[ai][aj] - (sum[ai] + sum[aj]) / (n-2);
          */
        }

      }

    }

    // 3 points:
    double x = (D[1][0]+D[2][0]-D[2][1])/2;
    double y = (D[1][0]+D[2][1]-D[2][0])/2;
    double z = (D[2][0]+D[2][1]-D[1][0])/2;

    //printNodes(nodes);

    return "(" +
           newick[0] + ":" + new Formatter().format(Locale.US,"%.2f",x) + "," +
           newick[1] + ":" + new Formatter().format(Locale.US,"%.2f",y) + "," +
           newick[2] + ":" + new Formatter().format(Locale.US,"%.2f",z) +
           ")" + "A" + Integer.toString(D.length-n) + ":0.0";
  }

  /**
   * An implementation of Rapid NJ.
   * 
   * M. Simonsen, T. Mailund, C.N.S. Pedersen. Rapid Neighbour-Joining. 
   * Proc. of WABI 2008.
   * 
   * @param D A lower triangular distance matrix.  D is destroyed during processing.
   * @param n The number of objects.  The lower triangular submatrix whose indices 
   * are in the range [0..n) will be used to build the tree.
   * @param labels A list of labels for each object or null.
   * 
   * @return The Newick representation for the tree built using neighbor-joining.
   */
  public static String rapid(double[][] D, int n, String[] labels) {
  
    // Makes room for a larger matrix:
    double[][] E = new double[2*n-3][];
    for (int i=0; i<n; i++) 
      E[i] = D[i];
    D = E;
    
    // Labels:
    String[] newick = new String[2*n-3];
    for (int i=0; i<n; i++) 
      newick[i] = (labels == null ? Integer.toString(i) : labels[i]);
        
    // The sorted matrix S and the indices I:
    double S[][] = new double[2*n-3][];
    int I[][] = new int[2*n-3][];   // I[i][j] has the position of S[i][j] in D[i].

    for (int i=0; i<n; i++) {
      S[i] = new double[i+1];
      I[i] = new int[i+1];
      
      for (int j=0; j<i; j++) 
        S[i][j] = D[i][j];
      
      Sort.qsort(S[i], I[i], 0, i);
    }

    // Evals the distances sum for every node k and records the maximum sum:
    double[] sum = new double[2*n-3];
    double smax = Double.MIN_VALUE;
    
    for (int k=0; k<n; k++) {
      sum[k] = 0;
      for (int x=0; x<k; x++)
        sum[k] += D[k][x];
      for (int x=k+1; x<n; x++)
        sum[k] += D[x][k];
      
      if (sum[k]>smax) 
        smax = sum[k];
    }

    // The last row in the matrix is l, the number of active rows is n:
    int l = n;
    
    while (n > 3) {
      
      // Gets minimum:
      double s, smin = Double.MAX_VALUE;
      int imin=0, jmin=0;

      for (int i=l-1; i>=0; i--) 
        if (D[i] != null) 
          for (int j=0; j<i; j++) 
            if (D[I[i][j]] != null) 
              if (S[i][j] < Double.MAX_VALUE && S[i][j] - (sum[i]+smax) / (n-2) < smin) {
                s = S[i][j] - (sum[i]+sum[I[i][j]]) / (n-2); 
                if (s < smin) {
                  smin = s;
                  imin = i;
                  jmin = I[i][j];
                }
              }
              else 
                break;
              
      
      
      // Data on new node (imin,jmin) will be stored at l.
      // Evals branch lengths Lik and Ljk:
      double lik = 0.5 * (D[imin][jmin] + ((sum[imin]-sum[jmin])/(n-2)));
      double ljk = D[imin][jmin] - lik;
  
      // Updates tree:
      newick[l] = "(" +
        newick[imin] + ":" +
        new Formatter().format(Locale.US,"%.2f",lik).toString() +
        "," +
        newick[jmin] + ":" +
        new Formatter().format(Locale.US,"%.2f",ljk).toString() + 
        ")" + "A" + Integer.toString((l-n)/2);

      newick[imin] = newick[jmin] = null;

      
      // Updates D, S, I, sum:
      sum[l] = 0;
      D[l] = new double[l+1];
      S[l] = new double[l+1];
      I[l] = new int[l+1];
      
      for (int k=0; k<jmin; k++) 
        if (k != imin && D[k] != null) { 
          sum[k] -= D[jmin][k];
          D[l][k] = (D[jmin][k] + (k<imin ? D[imin][k] : D[k][imin]) - D[imin][jmin]) / 2;
          S[l][k] = D[l][k];
          sum[l] += D[l][k];
          sum[k] += D[l][k];
        }
        else  // Positions already removed at row l will be "sorted-out" 
          S[l][k] = Double.MAX_VALUE;
            
      for (int k=jmin+1; k<l; k++) 
        if (k != imin && D[k] != null) {
          sum[k] -= D[k][jmin];
          D[l][k] = (D[k][jmin] + (k<imin ? D[imin][k] : D[k][imin]) - D[imin][jmin]) / 2;
          S[l][k] = D[l][k];
          sum[l] += D[l][k];
          sum[k] += D[l][k];
        }
        else  // Positions already removed at row l will be "sorted-out" 
          S[l][k] = Double.MAX_VALUE;
      
      for (int k=0; k<imin; k++) 
        if (k != jmin && D[k] != null) 
          sum[k] -= D[imin][k];
        
      for (int k=imin+1; k<l; k++)
        if (k != jmin && D[k] != null)
          sum[k] -= D[k][imin];

      Sort.qsort(S[l],I[l],0,l);

      D[imin] = D[jmin] = null;
      S[imin] = S[jmin] = null;
      I[imin] = I[jmin] = null;
      
      
      // Updates the maximum sum:
      smax = Double.MIN_VALUE;
      for (int k=0; k<l; k++)
        if (D[k] != null && sum[k] > smax)
          smax = sum[k];
          
      n--;
      l++;
    }
  
    
    // 3 points:
    // Finds the points out:
    int i,j,k;
    
    int a=0;
    for ( ; ; a++) 
      if (D[a] != null) {
        i = a++;
        break;
      }
        
    for ( ; ; a++) 
      if (D[a] != null) {
        j = a++;
        break;
      }
  
    for (; ; a++) 
      if (D[a] != null) {
        k = a;
        break;
      }
      
    double x = (D[j][i]+D[k][i]-D[k][j])/2;
    double y = (D[j][i]+D[k][j]-D[k][i])/2;
    double z = (D[k][i]+D[k][j]-D[j][i])/2;
    
    return "(" +
           newick[i] + ":" + new Formatter().format(Locale.US,"%.2f",x) + "," +
           newick[j] + ":" + new Formatter().format(Locale.US,"%.2f",y) + "," +
           newick[k] + ":" + new Formatter().format(Locale.US,"%.2f",z) +
           ")" + "A" + Integer.toString((l-n)/2) + ":0.0";
  }

  /**
   * NJ implementing the arithmetic proposed by Studier and Keppler, WITHOUT MATRIX ROWS REUSE.
   * 
   * Experiments have shown that the number of accesses to the matrix cells is substantially larger.
   * 
   * Used as a base for building rapid.
   * 
   * J.A. Studier, K.J. Keppler. A note on the Neighbor-Joining algorithm of Saitou and Nei. 
   * Mol Biol. Evol. v.5, 1988.
   * 
   * @param D A lower triangular distance matrix.  D is destroyed during processing.
   * @param n The number of objects.  The lower triangular submatrix whose indices 
   * are in the range [0..n) will be used to build the tree.
   * @param labels An optional list of labels for each object.
   * @return The Newick representation for the tree built using neighbor-joining.
   */
  public static String nj2(double[][] D, int n, String[] labels) {
  
    // Makes room for a larger matrix:
    double[][] E = new double[2*n-3][];
    for (int i=0; i<n; i++) 
      E[i] = D[i];
    D = E;
    
    // Labels:
    String[] newick = new String[2*n-3];
    for (int i=0; i<n; i++) 
      newick[i] = labels == null ? Integer.toString(i) : labels[i];
        
    // Evals the distances sum for every node k: 
    double[] sum = new double[2*n-3];
    for (int k=0; k<n; k++) {
      sum[k] = 0;
      for (int x=0; x<k; x++)
        sum[k] += D[k][x];
      for (int x=k+1; x<n; x++)
        sum[k] += D[x][k];
    }
  
    int l = n;

    long ct = 0;

    
    while (n > 3) {
      
      /*
      
      for (int i = 0; i<l; i++) {
        if (D[i] == null)
          System.out.print("  d ");
        else
          System.out.printf("%4.1f ",sum[i]);
      }
      System.out.println();
      
      
      
            
      for (int i = 0; i<l; i++) {
        if (D[i] == null)
          System.out.print("null");
        else
        for (int j = 0; j<=i; j++) {
          
          if (D[j] == null)
            System.out.printf("%4c ",'d');
          else
          System.out.printf("%4.1f ",D[i][j]);
        }
        System.out.println();
      }
      
      System.out.println();
      */
      
      // Gets minimum:
      double s, smin = Double.MAX_VALUE;
      int imin=0, jmin=0;
        
      for (int i=1; i<l; i++) {
        if (D[i] != null) {
          for (int j=0; j<i; j++) {
            ct++;
            if (D[j] != null) {
            s = D[i][j] - (sum[i]+sum[j]) / (n-2); 
            if (s<smin) {
              smin = s;
              imin = i;
              jmin = j;
            }
            }
          }
        }
      }
  
      // Data on new node (imin,jmin) will be stored at l.
      // Evals branch lengths Lik and Ljk:
      double lik = 0.5 * (D[imin][jmin] + ((sum[imin]-sum[jmin])/(n-2)));
      double ljk = D[imin][jmin] - lik;
  
      // Updates tree:
      newick[l] = "(" +
        newick[imin] + ":" +
        new Formatter().format(Locale.US,"%.2f",lik).toString() +
        "," +
        newick[jmin] + ":" +
        new Formatter().format(Locale.US,"%.2f",ljk).toString() + 
        ")" + "A" + Integer.toString((l-n)/2);
  
      
      
      // Updates D and sum:
      sum[l] = 0;
      D[l] = new double[l+1];
      for (int k=0; k<jmin; k++) 
        if (k != imin && D[k] != null) { 
          sum[k] -= D[jmin][k];
          D[l][k] = (D[jmin][k] + (k<imin ? D[imin][k] : D[k][imin]) - D[imin][jmin]) / 2;
          sum[l] += D[l][k];
          sum[k] += D[l][k];
        }
  
      for (int k=jmin+1; k<l; k++) 
        if (k != imin && D[k] != null) {
          sum[k] -= D[k][jmin];
          D[l][k] = (D[k][jmin] + (k<imin ? D[imin][k] : D[k][imin]) - D[imin][jmin]) / 2;
          sum[l] += D[l][k];
          sum[k] += D[l][k];
        }
      
      //sum[l] += D[imin][jmin];
      
     
      for (int k=0; k<imin; k++) {
        if (k != jmin && D[k] != null) 
        sum[k] -= D[imin][k];
      }
        
      for (int k=imin+1; k<l; k++) {
        if (k != jmin && D[k] != null)
        sum[k] -= D[k][imin];
      }
  
      D[imin] = D[jmin] = null;
      newick[imin] = newick[jmin] = null;
      
      n--;
      l++;
    }
  
    // 3 points:
    
    // Finds the points out:
    int i,j,k;
    
    int a=0;
    for ( ; ; a++) 
      if (D[a] != null) {
        i = a++;
        break;
      }
        
    for ( ; ; a++) 
      if (D[a] != null) {
        j = a++;
        break;
      }
  
    for (; ; a++) 
      if (D[a] != null) {
        k = a;
        break;
      }
  
    System.out.printf("ct %d",ct);
      
    
    double x = (D[j][i]+D[k][i]-D[k][j])/2;
    double y = (D[j][i]+D[k][j]-D[k][i])/2;
    double z = (D[k][i]+D[k][j]-D[j][i])/2;
    
    return "(" +
           newick[i] + ":" + new Formatter().format(Locale.US,"%.2f",x) + "," +
           newick[j] + ":" + new Formatter().format(Locale.US,"%.2f",y) + "," +
           newick[k] + ":" + new Formatter().format(Locale.US,"%.2f",z) +
           ")" + "A" + Integer.toString((l-n)/2) + ":0.0";
  }

  /**
     Returns the Newick representation for the tree built using Jurassic
     Neighbor-joining.  D is destroyed.
   */
  public static String jnj(double[][] D, int n, String[] labels) {
  
    double dsum = 0;
    double[] djoined = new double[n]; // distance between two nodes joined into a new node.
    String[] newick = new String[n];
  
    for (int i=0; i<n; i++) {
      newick[i] = labels == null ? Integer.toString(i) : labels[i];
      for (int j=0; j<i; j++)
        dsum += D[i][j];
    }
  
    while (n > 3) {

      // Evals d(k,x) for every k and x!=k:
      double[] ds = new double[n];
      
      for (int k=0; k<n; k++) {
        for (int x=0; x<k; x++)
          ds[k] += D[k][x];
        for (int x=k+1; x<n; x++)
          ds[k] += D[x][k];
      }

      
      // Evals S[i][j] and gets minimum:
      double smin = Double.MAX_VALUE;
      int ismin=0, jsmin=0;
        
      for (int i=1; i<n; i++) {
        for (int j=0; j<i; j++) {
  
          double s = (ds[i] - D[i][j] + ds[j] - D[i][j])/(2*(n-2)) + 
                     D[i][j]/2 + (dsum - ds[i] - ds[j] + D[i][j])/(n-2);

          if (s<smin) {
            smin = s;
            ismin = i;
            jsmin = j;
          }
        }
      }
  
      // Evals T[i][j][k] and gets minimum:
      double tmin = Double.MAX_VALUE;
      int itmin=0, jtmin=0, ktmin=0;
  
      for (int i=1; i<n; i++) {
        for (int j=0; j<i; j++) {
          for (int k=0; k<n; k++) {
  
            if (k==i || k==j)
              continue;

            // An internal node is not taken as a parent again (relax this?):
            if (djoined[k] > 0)
              continue;

            double dik = i>k ? D[i][k] : D[k][i];
            double djk = j>k ? D[j][k] : D[k][j];
            double t = dik + djk + (dsum - D[i][j])/(n-2);
  
            if (t<tmin) {
              itmin = i;
              jtmin = j;
              ktmin = k;
              tmin = t;
            }
          }
        }
      }
  
      if (smin < tmin*0.9) {
        double dikmin = (ds[ismin] - D[ismin][jsmin])/(n-2);
        double djkmin = (ds[jsmin] - D[ismin][jsmin])/(n-2);
        
        double lix = (D[ismin][jsmin]+dikmin-djkmin)/2-djoined[ismin];
        double ljx = (D[ismin][jsmin]+djkmin-dikmin)/2-djoined[jsmin];
        
        if (lix<0) lix = 0;
        if (ljx<0) ljx = 0;
        
        newick[jsmin] = "(" + newick[ismin] + ":" +
                       new Formatter().format(Locale.US,"%1.2f",lix) +
                       "," + newick[jsmin] + ":" +
                       new Formatter().format(Locale.US,"%1.2f",ljx) +
                       ")" + "A" + Integer.toString(D.length-n);
    
        djoined[jsmin] = D[ismin][jsmin]/2;

        ds[jsmin] = 0;
        for (int k=0; k<jsmin; k++) 
          if (k != ismin) { 
            dsum -= D[jsmin][k];
            ds[k] -= D[jsmin][k];
            D[jsmin][k] = (D[jsmin][k] + (k<ismin ? D[ismin][k] : D[k][ismin])) / 2;
            dsum += D[jsmin][k];          
            ds[k] += D[jsmin][k];
            ds[jsmin] += D[jsmin][k];
          }
    
        for (int k=jsmin+1; k<n; k++) 
          if (k != ismin) { 
            dsum -= D[k][jsmin];
            ds[k] -= D[k][jsmin];
            D[k][jsmin] = (D[k][jsmin] + (k<ismin ? D[ismin][k] : D[k][ismin])) / 2;
            dsum += D[k][jsmin];
            ds[k] += D[k][jsmin];
            ds[jsmin] += D[k][jsmin];
          }
        
        ds[jsmin] += D[ismin][jsmin];
        
        // Moves n-1 to imin, updating dsum and ds:
        for (int k=0; k<ismin; k++) {
          dsum -= D[ismin][k];
          ds[k] -= D[ismin][k];
          D[ismin][k] = D[n-1][k];
        }
          
        for (int k=ismin+1; k<n-1; k++) {
          dsum -= D[k][ismin];
          ds[k] -= D[k][ismin];
          D[k][ismin] = D[n-1][k];
        }
        
        dsum -= D[n-1][ismin];

        newick[ismin] = newick[n-1];
        djoined[ismin] = djoined[n-1];
        ds[ismin] = ds[n-1] - D[n-1][ismin];
        
        n--;

      }
      else {
  
        djoined[ktmin] = ((itmin>ktmin?D[itmin][ktmin]:D[ktmin][itmin]) +
        		(jtmin>ktmin?D[jtmin][ktmin]:D[ktmin][jtmin]))/2;
        
        double lik = (itmin>ktmin ? D[itmin][ktmin] : D[ktmin][itmin]) - djoined[itmin]; 
        double ljk = (jtmin>ktmin ? D[jtmin][ktmin] : D[ktmin][jtmin]) - djoined[jtmin];
        
        if (lik<0) lik = 0;
        if (ljk<0) ljk = 0;

        newick[ktmin] = "(" + newick[itmin] + ":" + 
                        new Formatter().format(Locale.US,"%.2f",lik) +
                        "," + newick[jtmin] + ":" +
                        new Formatter().format(Locale.US,"%.2f",ljk) +
                        ")" + newick[ktmin];
  
        // Moves n-1 to jtmin:
        for (int x=0; x<itmin; x++) {
          dsum -= D[itmin][x];
          D[itmin][x] = D[n-1][x];
          dsum += D[itmin][x];
        }
  
        for (int x=itmin+1; x<n-1; x++) {
          dsum -= D[x][itmin];
          D[x][itmin] = D[n-1][x];
          dsum += D[x][itmin];
        }
  
        dsum -= D[n-1][itmin];
        newick[itmin] = newick[n-1];
        djoined[itmin] = djoined[n-1];
  
  
        // Moves n-2 to itmin:
        for (int s=0; s<jtmin; s++) {
          dsum -= D[jtmin][s];
          D[jtmin][s] = D[n-2][s];
          dsum += D[jtmin][s];
        }
  
        for (int s=jtmin+1; s<n-2; s++) {
          dsum -= D[s][jtmin];
          D[s][jtmin] = D[n-2][s];
          dsum += D[s][jtmin];
        }
  
        dsum -= D[n-2][jtmin];
        newick[jtmin] = newick[n-2];
        djoined[jtmin] = djoined[n-2];
  
        n -= 2;
      }
  
    }
  
  
    // 3 points:
    double x = (D[1][0]+D[2][0]-D[2][1])/2 - djoined[0]/2;
    double y = (D[1][0]+D[2][1]-D[2][0])/2 - djoined[1]/2;
    double z = (D[2][0]+D[2][1]-D[1][0])/2 - djoined[2]/2;
  
    return "(" +
    newick[0] + ":" + new Formatter().format(Locale.US,"%.2f",x) + "," +
    newick[1] + ":" + new Formatter().format(Locale.US,"%.2f",y) + "," +
    newick[2] + ":" + new Formatter().format(Locale.US,"%.2f",z) +
    ")" + "A" + Integer.toString(D.length-n) + ":0.0";
  }

  public static void main(String[] args) throws IOException, ParseException {


    double[][] D0 = {{ 0 },
                     { 5, 0 },
                     { 4, 7, 0 },
                     { 7,10, 7, 0 },
                     { 6, 9, 6, 5, 0 },
                     { 8,11, 8, 9, 8, 0 }};

    float[][] D1 = {{ 0 },
                     { 5, 0 },
                     { 4, 7, 0 },
                     { 7,10, 7, 0 },
                     { 6, 9, 6, 5, 0 },
                     { 8,11, 8, 9, 8, 0 }};


    double[][] E0 = {{ 0 },
                     { 7, 0 },
                     { 8, 5, 0 },
                     {11, 8, 5, 0 },
                     {13,10, 7, 8, 0 },
                     {16,13,10,11, 5, 0 },
                     {13,10, 7, 8, 6, 9, 0 },
                     {17,14,11,12,10,13, 8, 0 }};

    double[][] E1 = {{ 0 },
                     { 7, 0 },
                     { 8, 5, 0 },
                     {11, 8, 5, 0 },
                     {13,10, 7, 8, 0 },
                     {16,13,10,11, 5, 0 },
                     {13,10, 7, 8, 6, 9, 0 },
                     {17,14,11,12,10,13, 8, 0 }};


    //System.out.printf("%s\n",original(E0,E0.length,null));
    //System.out.printf("%s\n",nj(D0,D0.length,null));
    PexMatrix m = DistanceMatrixReader.loadPex("D:\\Doutorado\\My Dropbox\\MaterialTestes\\Imagem\\Chars.dmat");
    System.out.printf("%s\n",original(m.M,m.n,null));
    System.out.printf("%s\n",fast(m.M,m.n,null));
    //System.out.printf("%s\n",NJ.nj(E0,E0.length,null));


//    String dmat = "/home/gpt/1000.mat";
//    PexMatrix E = DistanceMatrixReader.loadPex(dmat);
//    String t = nj(E.M,E.n,null);
//    System.out.printf("%s\n",t);

  }

  //--------------------------------------------------------

}