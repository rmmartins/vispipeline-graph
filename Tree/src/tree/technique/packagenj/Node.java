/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.technique.packagenj;

import java.util.ArrayList;

/**
 *
 * @author Jose Gustavo
 */
public class Node {

    public String id;
    public ArrayList<Node> children = new ArrayList<Node>();
    public ArrayList<Double> distance = new ArrayList<Double>();

    public Node(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        String ret = id;
        if (!children.isEmpty()) {
            for (int i=0;i<children.size();i++)
                ret += "\n - "+children.get(i).id+":"+distance.get(i);
        }
        return ret;
    }

}
