package tree.technique.packagenj;

import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;
import matrix.AbstractMatrix;


public class DistanceMatrixReader {

  /**
   * Loads a lower triangular distance matrix.  For a n x n matrix the 
   * file format omits the main diagonal and is as follows.
   * 
   *  n
   *  M[1][0]
   *  M[2][0];M[2][1]
   *  M[3][0];M[3][1];M[3][2]
   *  ...
   *  M[n-1][0];M[n-1][1];M[n-1][2];...;M[n-1][n-2]
   *  
   * Optionally, a semicolon may occur at the end of lines and spaces 
   * may surround semicolons.
   *  
   * @param file The file.
   * @return A lower triangular matrix with an all zeros main diagonal.
   * @throws ParseException If file is not properly formatted.
   * @throws IOException If an IO error occurs. 
   */
  public static double[][] loadLower(String file) throws IOException, ParseException {

    try {
      Scanner s = new Scanner(new BufferedReader(new FileReader(file)));
      int n = s.nextInt();

      s.useDelimiter("\\s*;\\s*|;?\\s*\\n");

      double[][] M = new double[n][];
      M[0] = new double[1];
      for (int i=1; i<n; i++) {
        M[i] = new double[i+1];
        for (int j=0; j<i; j++)
          M[i][j] = s.nextDouble();
        M[i][i] = 0;
      }

      s.close();

      return M;
    }
    catch (NoSuchElementException e) {
      throw new ParseException("Format mismatch in "+file,0);
    }
  }

  /**
   * Reads a n x m matrix of integers.  File format is: the first line holds n and m, 
   * and the next n lines hold m integers each.  Entries may be delimited by semicolon
   * or by spaces. 
   * 
   * @param file The file.
   * @return An int[n][m] matrix.
   * @throws ParseException If file is not properly formatted.
   * @throws IOException If an IO error occurs. 
   */
  public static int[][] loadInt(String file) throws ParseException, IOException {

    try {
      Scanner s = new Scanner(new BufferedReader(new FileReader(file)));
      s.useDelimiter("\\s*;\\s*|;?\\s*\\n|\\s+|\\n");

      int n = s.nextInt();
      int m = s.nextInt();

      int[][] M = new int[n][m];
      for (int i=0; i<n; i++) 
        for (int j=0; j<m; j++)
          M[i][j] = s.nextInt();

      s.close();
      return M;
    }
    catch (NoSuchElementException e) {
      throw new ParseException("Misformatted file: "+file,0);
    }
  }

  /**
   * Loads a PEx distance matrix file.  For a n x n matrix the 
   * file format is as follows.
   * 
   *  n
   *  L[0];L[1];...;L[n-1]
   *  C[0];C[1];...;C[n-1]
   *  M[1][0]
   *  M[2][0];M[2][1]
   *  M[3][0];M[3][1];M[3][2]
   *  ...
   *  M[n-1][0];M[n-1][1];M[n-1][2];...;M[n-1][n-2]
   * 
   * L[.] are object labels and C[.] are object classes. 
   * 
   * @param file The file.
   * @return a PexMatrix record, where the matrix itself is a 
   * lower triangular matrix with an all zeros main diagonal
   * @throws ParseException If file is not properly formatted.
   * @throws IOException If an IO error occurs. 
   */
  public static PexMatrix loadPex(String file) throws IOException, ParseException {

      String st = "";
    try {
      Scanner s = new Scanner(new BufferedReader(new FileReader(file)));
      PexMatrix D = new PexMatrix();
  
      D.n = s.nextInt();
      s.nextLine();
      D.labels = s.nextLine().split("\\s*;\\s*");

      String[] cdata = s.nextLine().split(";");
      D.classes = new float[D.n];
      for (int i = 0; i < D.n; i++) {
        //Double n = Double.parseDouble(cdata[i]);
        Float n = Float.parseFloat(cdata[i]);
        D.classes[i] = n;//n.intValue();
      }

      s.useDelimiter("\\s*;\\s*|;?\\s*\\n");
      //s.useDelimiter("[;\\n\\s]*");
  
//      D.classes = new int[D.n];
//      for (int i = 0; i < D.n; i++)
//        D.classes[i] = (int) s.nextDouble();

      D.M = new double[D.n][];
      D.M[0] = new double[1];
      for (int i=1; i<D.n; i++) {
        D.M[i] = new double[i+1];
  
        for (int j=0; j<i; j++) {
          st = s.next();
          //D.M[i][j] = s.nextDouble();
          D.M[i][j] = Double.parseDouble(st);
          //System.out.println("OK : "+st);
        }
  
        D.M[i][i] = 0;
      }
      s.close();
  
      return D;
    }
    catch (NoSuchElementException e) {
        //System.out.println("Erro : "+st);
      throw new ParseException("Format mismatch in "+file,0);
    }
  }

  public static PexMatrix loadPex(DistanceMatrix dmat) {

      PexMatrix dm = new PexMatrix();

      dm.n = dmat.getElementCount();
      dm.ids = new int[dm.n];
      dm.labels = new String[dm.n];
      dm.classes = new float[dm.n];

      //populating ids data
      if (!dmat.getIds().isEmpty())
          for (int i=0;i<dm.n;i++)
              dm.ids[i] = dmat.getIds().get(i);

      //populating class data
      if (dmat.getClassData().length > 0)
          for (int i=0;i<dm.n;i++)
              dm.classes[i] = new Float(dmat.getClassData()[i]);//new Float(dmat.getClassData()[i]).intValue();

      //populating label data
      if (!dmat.getLabels().isEmpty()) {
          for (int i=0;i<dm.n;i++)
              dm.labels[i] = dmat.getLabels().get(i);
      }else {
          //In this case, matrix elements do not have labels. Creating labels based on its ids...
          if (!dmat.getIds().isEmpty()) {
              for (int i=0;i<dm.n;i++) {
                  dm.labels[i] = dmat.getIds().get(i).toString();
              }
          } else {
              //int this very rare case, matrix elements do not have ids. Creating ids...
              for (int i=0;i<dm.n;i++) {
                  dm.labels[i] = Integer.toString(i);
              }
          }
      }
      
      dm.M = new double[dm.n][];
      dm.M[0] = new double[1];
      dm.M[0][0] = 0.0;
      try {
          for (int i=0;i<dmat.getDistmatrix().length;i++) {
              //System.out.println(i);
              dm.M[i+1] = new double[dmat.getDistmatrix()[i].length+1];
              for (int j=0;j<dmat.getDistmatrix()[i].length;j++) {
                  dm.M[i+1][j] = dmat.getDistmatrix()[i][j];
              }
              dm.M[i+1][dmat.getDistmatrix()[i].length] = 0.0;
          }
          //printMatrix(dm);
      }catch (Exception e) {
          System.out.println(e.getMessage());
      }
      return dm;
    
  }

  public static PexMatrix loadPex(AbstractMatrix matrix, AbstractDissimilarity diss) {

      PexMatrix dm = new PexMatrix();

      dm.n = matrix.getRowCount();
      dm.ids = new int[dm.n];
      dm.labels = new String[dm.n];
      dm.classes = new float[dm.n];

      //populating ids data
      if (!matrix.getIds().isEmpty())
          for (int i=0;i<dm.n;i++)
              dm.ids[i] = matrix.getIds().get(i);

      //populating class data
      if (matrix.getClassData().length > 0)
          for (int i=0;i<dm.n;i++)
              dm.classes[i] = new Float(matrix.getClassData()[i]);

      //populating label data
      if (!matrix.getLabels().isEmpty()) {
          for (int i=0;i<dm.n;i++)
              dm.labels[i] = matrix.getLabels().get(i);
      }else {
          //In this case, matrix elements do not have labels. Creating labels based on its ids...
          if (!matrix.getIds().isEmpty()) {
              for (int i=0;i<dm.n;i++) {
                  dm.labels[i] = matrix.getIds().get(i).toString();
              }
          } else {
              //int this very rare case, matrix elements do not have ids. Creating ids...
              for (int i=0;i<dm.n;i++) {
                  dm.labels[i] = Integer.toString(i);
              }
          }
      }

      dm.M = new double[dm.n][];
      //dm.M[0] = new double[1];
      //dm.M[0][0] = 0.0;
      try {
          for (int i=0;i<matrix.getRowCount();i++) {
              //System.out.println(i);
              dm.M[i] = new double[i+1];
              for (int j=0;j<=i;j++) {
                  if (i == j)
                      dm.M[i][j] = 0.0;
                  else
                      dm.M[i][j] = diss.calculate(matrix.getRow(i), matrix.getRow(j));
              }
          }
          //printMatrix(dm);
      }catch (Exception e) {
          System.out.println(e.getMessage());
      }
      return dm;
  }

  private static void printMatrix(PexMatrix m) {

      System.out.println("*****************************");

      for (int i=0;i<m.M.length;i++) {
          for (int j=0;j<m.M[i].length;j++)
              System.out.print(m.M[i][j]+" ");
          System.out.println(" ");
      }

      System.out.println("*****************************");

  }

  /**
   * Reads a TSPLIB matrix.
   * 
   * @param file The file.
   * @return An int[n][n] matrix.
   * @throws IOException If an IO error occurs. 
   * 
   */
  public static int[][] loadTspLib(String file) throws IOException {

    Scanner s = new Scanner(new BufferedReader(new FileReader(file)));
    int A[][] = null, n = 0;

    while (s.hasNext()) {
      String str = s.next();

      if (str.equals("DIMENSION:")) {
        n = s.nextInt();
      }

      if (str.equals("EDGE_WEIGHT_SECTION")) {
        int i = 0, j = 0;
        A = new int[n][n];
        while (s.hasNextInt()) {
          A[i][j] = s.nextInt();
          j = (j + 1) % n;
          i += j == 0 ? 1 : 0;
        }
      }
    }
    s.close();

    // for (int i=0; i<n; i++) {
    // for (int j=0; j<n; j++) {
    // System.out.printf("%d ",A[i][j]);
    // }
    // System.out.printf("\n");
    // }

    return A;
  }

  /**
   * Writes a lower triangular distance matrix with a main diagonal.  See loadLower 
   * for the file format.
   * 
   * @param M A lower triangular matrix with a main diagonal.
   * @param file The file.
   * @throws IOException If an IO error occurs. 
   */
  static void writeLower(double[][] M, String file) throws IOException {

    FileWriter f = new FileWriter(file);
    int n = M.length;

    f.write(String.format("%d\n",n));

    int i, j;
    for (i=1; i<n; i++) {
      for (j=0; j<i-1; j++) 
        f.write(String.format("%.15f;",M[i][j]));
      f.write(String.format("%.15f\n",M[i][j]));
    }

    f.close();
  }

  /**
   * Writes a lower triangular matrix with a main diagonal as a PEx distance matrix file.
   *
   * @param M A lower triangular matrix with a main diagonal.
   * @param file The file.
   * @throws IOException If an IO error occurs. 
  */
  static void writePEX(double[][] M, String file) throws IOException {

    FileWriter f = new FileWriter(file);
    int n = M.length;
    int i, j;

    f.write(String.format("%d\n",n));

    for (i=0; i<n-1; i++)
      f.write(String.format("%d;",i));
    f.write(String.format("%d\n",i));

    for (i=0; i<n-1; i++)
      f.write("0;");
    f.write("0\n");

    for (i=1; i<n; i++) {
      for (j=0; j<i-1; j++) 
        f.write(String.format("%.15f;",M[i][j]));
      f.write(String.format("%.15f\n",M[i][j]));
    }

    f.close();
  }

  /**
   * Generates an n x n lower triangular matrix, whose distances are
   * the Euclidian distances among n points in a d-dimensional space defined
   * by integers in [0,m( in every dimension.  The matrix has an all zeros 
   * main diagonal.
   */
  static double[][] genLowerEuclidian(int n, int d, int m) {

    int[] pts = new int[d*n];

    Random r = new Random(1);

    for (int i=n*d-1; i>=0; i--) 
      pts[i] = r.nextInt(m);

    double[][] M = new double[n][];
    M[0] = new double[1];

    for (int i=1; i<n; i++) {
      M[i] = new double[i+1];

      for (int j=0; j<i; j++) {
        int diff;
        int sum = 0;
        for (int k=0; k<d; k++) {
          diff = pts[i*d+k] - pts[j*d+k];
          sum += (diff*diff);  
        }
        M[i][j] = Math.sqrt(sum);
      }
      M[i][i] = 0;
    }

    return M;
  }

  public static void main(String[] args) throws IOException, ParseException {

    /*
    PexMatrix D = Loader.loadPexMatrix("/home/gpt/Desktop/gpt/Desktop/info-comp.dmat");

    for (int i = 0; i < D.n; i++)
      System.out.println(D.labels[i]);

    for (int i = 0; i < D.n; i++)
      System.out.println(D.classes[i]);

    for (int i = 0; i < D.n; i++) {
      for (int j = 0; j <= i; j++) {
        System.out.printf("%.3f ",D.M[i][j]);
      }
      System.out.println();
    }
     */

    int[][] M = loadInt("/home/gpt/Desktop/gpt/Desktop/x.mat");

    int n = M.length;
    int m = M[0].length;

    for (int i=0; i<n; i++) {
      for (int j=0; j<m; j++) {
        System.out.printf("%d ",M[i][j]);
      }
      System.out.println();
    }

    /*
    for (int n=9000; n<30000; n+=1000) {
      System.out.println("n = "+n);
      double[][] M = genLowerEuclidian(n,20,10);

      //for (int i = 0; i < n; i++) {
      //  for (int j = 0; j <= i; j++) {
      //    System.out.printf("%.3f ",M[i][j]);
      //  }
      //  System.out.println();
      //}

      writePEX(M,n+".mat");
     */
  }
}



