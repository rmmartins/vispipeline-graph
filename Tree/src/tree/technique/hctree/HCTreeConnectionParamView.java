package tree.technique.hctree;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import tree.technique.hctree.HierarchicalClusteringTree.HierarchicalClusteringType;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class HCTreeConnectionParamView extends AbstractParametersView {

    /** Creates new form NJConnectionParamView */
    public HCTreeConnectionParamView(HCTreeConnectionComp comp) {
        initComponents();

        this.comp = comp;

        for (DissimilarityType disstype : DissimilarityType.values()) {
            this.dissimilarityComboBox.addItem(disstype);
        }

        for (HierarchicalClusteringType hctype : HierarchicalClusteringType.values()) {
            this.clusteringTypeComboBox.addItem(hctype);
        }

        this.divergenceCheckBox.setVisible(false);

        reset();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        dissimilarityPanel = new javax.swing.JPanel();
        dissimilarityComboBox = new javax.swing.JComboBox();
        pnjCheckBox = new javax.swing.JCheckBox();
        clusteringTypePanel = new javax.swing.JPanel();
        clusteringTypeComboBox = new javax.swing.JComboBox();
        divergenceCheckBox = new javax.swing.JCheckBox();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Neighbor Joining Connection Parameters"));
        setLayout(new java.awt.GridBagLayout());

        dissimilarityPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Dissimilarity"));
        dissimilarityPanel.setLayout(new java.awt.BorderLayout());
        dissimilarityPanel.add(dissimilarityComboBox, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(dissimilarityPanel, gridBagConstraints);

        pnjCheckBox.setText("Leaf Promotion");
        pnjCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        add(pnjCheckBox, gridBagConstraints);

        clusteringTypePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Clustering Type"));
        clusteringTypePanel.setLayout(new java.awt.BorderLayout());
        clusteringTypePanel.add(clusteringTypeComboBox, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(clusteringTypePanel, gridBagConstraints);

        divergenceCheckBox.setText("Use divergence");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        add(divergenceCheckBox, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void reset() {
        if (comp.isDistanceMatrixProvided()) {
            dissimilarityComboBox.setSelectedItem(comp.getDissimilarityType());
            dissimilarityComboBox.setEnabled(false);
        } else {
            dissimilarityComboBox.setEnabled(true);
            dissimilarityComboBox.setSelectedItem(comp.getDissimilarityType());
            pnjCheckBox.setSelected(comp.isPnj());
        }
        clusteringTypeComboBox.setSelectedItem(comp.getClusteringType());
        divergenceCheckBox.setSelected(comp.isDivergence());
    }

    @Override
    public void finished() throws IOException {
        if (!comp.isDistanceMatrixProvided()) {
            comp.setDissimilarityType((DissimilarityType)dissimilarityComboBox.getSelectedItem());
        }
        comp.setClusteringType((HierarchicalClusteringType)clusteringTypeComboBox.getSelectedItem());
        comp.setPnj(pnjCheckBox.isSelected());
        comp.setDivergence(divergenceCheckBox.isSelected());
    }

    protected HCTreeConnectionComp comp;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox clusteringTypeComboBox;
    protected javax.swing.JPanel clusteringTypePanel;
    private javax.swing.JComboBox dissimilarityComboBox;
    protected javax.swing.JPanel dissimilarityPanel;
    private javax.swing.JCheckBox divergenceCheckBox;
    private javax.swing.JCheckBox pnjCheckBox;
    // End of variables declaration//GEN-END:variables
}
