package tree.technique.hctree;

import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import tree.model.TreeConnectivity;
import java.io.IOException;
import javax.swing.JOptionPane;
import matrix.AbstractMatrix;
import tree.basics.Tree;
import tree.technique.hctree.HierarchicalClusteringTree.HierarchicalClusteringType;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.Technique",
name = "Hierarchical Clustering Tree",
description = "Project points from a multidimensional space to the plane " +
"preserving the neighborhood relations.")
public class HCTreeConnectionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        conn = null;
        if (matrix != null) { //using a points matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            if (hctype == HierarchicalClusteringType.WARD) {
                HierarchicalClusteringTreeWard htcw = new HierarchicalClusteringTreeWard(matrix,pnj);
                tree = htcw.createTree();
            }else {
                HierarchicalClusteringTree htc = new HierarchicalClusteringTree(matrix,diss,hctype,pnj,divergence);
                tree = htc.createTree();
            }
        } else if (dmat != null) { //using a distance matrix
            if (hctype == HierarchicalClusteringType.WARD) {
                JOptionPane.showMessageDialog(null,"Cannot generate Ward Hierarquical Clustering Tree with Distance Matrix.");
                tree = null;
                throw new IOException("Cannot generate Ward Hierarquical Clustering Tree with Distance Matrix.");
            }else {
                HierarchicalClusteringTree htc = new HierarchicalClusteringTree(dmat,hctype,pnj,divergence);
                tree = htc.createTree();
            }
        } else {
            throw new IOException("A distance matrix or a points matrix should be provided.");
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "distance matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public Tree outputTree() {
        return tree;
    }

    public TreeConnectivity output() {
        return conn;
    }
    
    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new HCTreeConnectionParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        conn = null;
        matrix = null;
        dmat = null;
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public boolean isDistanceMatrixProvided() {
        return (dmat != null);
    }

    public Boolean isPnj() {
        if (pnj != null) return pnj;
        else return false;
    }

    public void setPnj(Boolean pnj) {
        this.pnj = pnj;
    }

    public Boolean isDivergence() {
        if (divergence != null) return divergence;
        else return false;
    }

    public void setDivergence(Boolean div) {
        divergence = div;
    }

    public HierarchicalClusteringType getClusteringType() {
        return hctype;
    }

    public void setClusteringType(HierarchicalClusteringType hctype) {
        this.hctype = hctype;
    }

    public static final long serialVersionUID = 1L;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private Boolean pnj = false;
    private Boolean divergence = false;
    private transient HCTreeConnectionParamView paramview;
    private transient Tree tree;
    private transient TreeConnectivity conn;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
    private HierarchicalClusteringType hctype = HierarchicalClusteringType.CLINK;
}
