package tree.technique.hctree;

import java.io.IOException;
import java.util.ArrayList;
import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import matrix.AbstractMatrix;
import tree.basics.ContentTree;
import tree.basics.Tree;
import tree.basics.TreeConstants;
import tree.technique.nj.NeighborJoining;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class HierarchicalClusteringTree {

    public HierarchicalClusteringTree(DistanceMatrix dmat, HierarchicalClusteringType type, boolean pnj, boolean divergence) {
        originalDmat = dmat;
        load(type,pnj,divergence);
    }

    public HierarchicalClusteringTree(AbstractMatrix matrix, AbstractDissimilarity dissType, HierarchicalClusteringType type, boolean pnj, boolean divergence) throws IOException {
        originalDmat = new DistanceMatrix(matrix, dissType);
        load(type,pnj,divergence);
    }

    private void load(HierarchicalClusteringType type, boolean pnj, boolean divergence) {
        this.type = type;
        this.pnj = pnj;
        this.divergence = divergence;
        tree = new Tree();
        tree.setType(TreeConstants.HCT);
        for (int i=0;i<originalDmat.getElementCount();i++) {
            if (originalDmat.getLabels() != null && !originalDmat.getLabels().isEmpty())
                tree.addNode(new ContentTree(originalDmat.getIds().get(i),originalDmat.getLabels().get(i),originalDmat.getClassData()[i]));
            else
                tree.addNode(new ContentTree(originalDmat.getIds().get(i),Integer.toString(i),originalDmat.getClassData()[i]));
            if (originalDmat.getIds().get(i) > maxId)
                maxId = originalDmat.getIds().get(i);
        }
    }

    private void init(DistanceMatrix dmat) throws IOException {
        //Cria a matriz de distancias, no formato n x n, com a diagonal principal igual a 0.
        for (int i = 0; i < dmat.getElementCount(); i++) {
            ArrayList<Float> lin = new ArrayList<Float>();
            //this.divergences.add(new ArrayList<Float>());
            for (int j = 0; j < dmat.getElementCount() - 1; j++) {
                lin.add(0.0f);
                //this.divergences.get(i).add(0.0f);
            }
            this.distances.add(lin);
        }

        //Calcula e preenche a matriz com as distancias
        for (int i = 0; i < dmat.getElementCount(); i++) {
            for (int j = 0; j < dmat.getElementCount(); j++) {
                if (i == j) {
                    this.distances.get(i).add(j, 0.0f);
                } else {
                    float distance = dmat.getDistance(i, j);
                    this.distances.get(i).set(j, distance);
                    this.distances.get(j).set(i, distance);
                }
            }
        }

        //Calculando divergencias
        float[] divs = new float[this.distances.size()];
        for (int i=0;i<this.distances.size();i++) {
            float d = 0;
            for (int j=0;j<this.distances.get(i).size();j++) {
                d += this.distances.get(i).get(j);
            }
            divs[i] = d;
        }

//        //Montando nova matrix
//        for (int i = 0; i < dmat.getElementCount(); i++) {
//            for (int j = 0; j < dmat.getElementCount(); j++) {
//                if (i == j) {
//                    this.divergences.get(i).add(j, 0.0f);
//                } else {
//                    float distance = this.distances.get(i).get(j) - ((divs[i] + divs[j])/(this.divergences.size()-2));
//                    this.divergences.get(i).set(j, distance);
//                    this.divergences.get(j).set(i, distance);
//                }
//            }
//        }
    }

    public Tree createTree() throws IOException {

        long init, end;

        init(originalDmat);

        //Inicialmente, crio clusters unitarios
        ArrayList<ArrayList<Integer>> clusters = new ArrayList<ArrayList<Integer>>();
        //HashMap<Integer,ArrayList<Integer>> clusters = new HashMap<Integer,ArrayList<Integer>>();
        
        //for (int i = 0; i < originalDmat.getElementCount(); i++) {
        for (int i = 0; i < distances.size(); i++) {
            //clusters.put(originalDmat.getIds().get(i),new ArrayList<Integer>());
            clusters.add(new ArrayList<Integer>());
            clusters.get(i).add(i);
            //mapIdIndex.put(i,originalDmat.getIds().get(i));
            mapIdIndex.add(originalDmat.getIds().get(i));
        }

        init = System.currentTimeMillis();

        //Em cada iteracao, eu pego os dois clusters mais proximos. eles serao conectados a um no virtual.
        //Vou fazendo isso ate que sobre apenas um cluster, que sera a raiz da arvore.
        while (clusters.size() > 1) {
            int[] cmin = null;
            if (!divergence)
                cmin = findNearestClustersByDistance(clusters);
//            else
//                cmin = findNearestClustersByDivergence(clusters);

            //Juntando os clusters (o conteudo do cmin[1] vai para cmin[0]
            for (int i = 0; i < clusters.get(cmin[1]).size(); i++) {
                clusters.get(cmin[0]).add(clusters.get(cmin[1]).get(i));
            }

            int newId = createJunctionNode(cmin);

            //excluo o cmin[1]
            clusters.remove(cmin[1]);

            //Atualizo o indice do cmin[0] para ser o indice do novo no criado...
            //mapIdIndex.put(cmin[0],newId);
            mapIdIndex.set(cmin[0],newId);

            //Retiro o cmin[1] do mapeamento id index...
            mapIdIndex.remove(cmin[1]);

            //Faco o update da matriz de distancias
            this.updateDistanceMatrix(cmin);
        }

        //Realizando promoção de folhas, de acordo com opcao no pipeline...
        if (pnj) NeighborJoining.promoteLeafs(tree);

        end = System.currentTimeMillis();
        long diff = end - init;
        System.out.println("Time spent (HCT Connection creation) -> " + (diff/1000.0f) + " seconds");

        tree.generateEdges();

        return tree;

    }

    private int[] findNearestClustersByDistance(ArrayList<ArrayList<Integer>> clusters) {
        float minDistance = distances.get(0).get(1);
        int[] cmin = new int[2];
        cmin[0] = 0;
        cmin[1] = 1;

        //Procurar os clusters mais proximos c1 e c2
        for (int c1 = 0; c1 < clusters.size(); c1++) {
            for (int c2 = c1 + 1; c2 < clusters.size(); c2++) {
                if (minDistance > distances.get(c1).get(c2)) {
                    minDistance = distances.get(c1).get(c2);
                    cmin[0] = c1;
                    cmin[1] = c2;
                }
            }
        }

        return cmin;
    }

//    private int[] findNearestClustersByDivergence(ArrayList<ArrayList<Integer>> clusters) {
//        float minDistance = divergences.get(0).get(1);
//        int[] cmin = new int[2];
//        cmin[0] = 0;
//        cmin[1] = 1;
//
//        //Procurar os clusters mais proximos c1 e c2
//        for (int c1 = 0; c1 < clusters.size(); c1++) {
//            for (int c2 = c1 + 1; c2 < clusters.size(); c2++) {
//                if (minDistance > divergences.get(c1).get(c2)) {
//                    minDistance = divergences.get(c1).get(c2);
//                    cmin[0] = c1;
//                    cmin[1] = c2;
//                }
//            }
//        }
//
//        return cmin;
//    }

    private int createJunctionNode(int[] nearestClusters) {

        //Junction Node properties
        ContentTree junctionNode, son1, son2;
        junctionNode = new ContentTree(++maxId,Integer.toString(tree.getSize()));
        son1 = tree.getNodeById(mapIdIndex.get(nearestClusters[0]));
        son2 = tree.getNodeById(mapIdIndex.get(nearestClusters[1]));

        ArrayList<ContentTree> children = new ArrayList<ContentTree>();
        children.add(son1);
        children.add(son2);
        junctionNode.setChildren(children);

        //o nivel do no de juncao sera um a mais do maior nivel entre seus filhos...
        if (son1.getNivelNo() > son2.getNivelNo())
            junctionNode.setNivelNo(son1.getNivelNo()+1);
        else junctionNode.setNivelNo(son2.getNivelNo()+1);

        float distSon1, distSon2;
        distSon1 = this.distances.get(nearestClusters[0]).get(nearestClusters[1])/2.0f;
        distSon2 = distSon1;

        ArrayList<Float> distChildren = new ArrayList<Float>();
        distChildren.add(distSon1);
        distChildren.add(distSon1);
        junctionNode.setDistChildren(distChildren);

        junctionNode.setValid(false);

        //adjusting sons properties
        son1.setParent(junctionNode);
        son2.setParent(junctionNode);

        son1.setDistParent(distSon1);
        son2.setDistParent(distSon2);

        son1.setRoot(junctionNode.getRoot());
        son2.setRoot(junctionNode.getRoot());

        tree.addNode(junctionNode);

        return junctionNode.getId();
    }

    private void updateDistanceMatrix(int[] cmin) {
        if (this.type.equals(HierarchicalClusteringType.SLINK)) {
            //Linha: copia a menor distancia entre os clusters c1min e c2min para o cluster c1min
            for (int i = 0; i < distances.get(0).size(); i++) {
                if (distances.get(cmin[0]).get(i) < distances.get(cmin[1]).get(i))
                    distances.get(cmin[0]).set(i, distances.get(cmin[0]).get(i));
                else
                    distances.get(cmin[0]).set(i, distances.get(cmin[1]).get(i));
                
                if (distances.get(i).get(cmin[0]) < distances.get(i).get(cmin[1]))
                    distances.get(i).set(cmin[0], distances.get(i).get(cmin[0]));
                else
                    distances.get(i).set(cmin[0], distances.get(i).get(cmin[1]));
            }
        } else if (this.type.equals(HierarchicalClusteringType.CLINK)) {
            //Linha: copia a maior distancia entre os clusters c1min e c2min para o cluster c1min
            for (int i = 0; i < distances.get(0).size(); i++) {
                if (distances.get(cmin[0]).get(i) > distances.get(cmin[1]).get(i))
                    distances.get(cmin[0]).set(i, distances.get(cmin[0]).get(i));
                else
                    distances.get(cmin[0]).set(i, distances.get(cmin[1]).get(i));
                
                if (distances.get(i).get(cmin[0]) > distances.get(i).get(cmin[1]))
                    distances.get(i).set(cmin[0], distances.get(i).get(cmin[0]));
                else
                    distances.get(i).set(cmin[0], distances.get(i).get(cmin[1]));
            }
        } else if (this.type.equals(HierarchicalClusteringType.ALINK)) {
            //Linha: copia a distancia media entre os clusters c1min e c2min para o cluster c1min
            for (int i = 0; i < distances.get(0).size(); i++) {
                distances.get(cmin[0]).set(i, (distances.get(cmin[0]).get(i) + distances.get(cmin[1]).get(i)) / 2);
                distances.get(i).set(cmin[0], (distances.get(i).get(cmin[0]) + distances.get(i).get(cmin[1])) / 2);
            }
        }

        //Apago a linha das dist�ncias do cluster c2min
        distances.remove(cmin[1]);

        //Apago a coluna das dist�ncias do cluster c2min
        for (int k = 0; k < distances.size(); k++) {
            distances.get(k).remove(cmin[1]);
        }

        //updateDivergenceMatrix();

    }

//    private void updateDivergenceMatrix() {
//
//        divergences = new ArrayList<ArrayList<Float>>();
//
//        for (int i = 0; i < this.distances.size(); i++) {
//            ArrayList<Float> lin = new ArrayList<Float>();
//            for (int j = 0; j < this.distances.size() - 1; j++) {
//                lin.add(0.0f);
//            }
//            this.divergences.add(lin);
//        }
//
//        //Calculando divergencias
//        float[] divs = new float[this.distances.size()];
//        for (int i=0;i<this.distances.size();i++) {
//            float d = 0;
//            for (int j=0;j<this.distances.get(i).size();j++) {
//                d += this.distances.get(i).get(j);
//            }
//            divs[i] = d;
//        }
//
//        //Montando nova matrix
//        for (int i=0; i < this.divergences.size(); i++) {
//            for (int j=0; j < this.divergences.size(); j++) {
//                if (i == j) {
//                    this.divergences.get(i).add(j, 0.0f);
//                } else {
//                    float distance = this.distances.get(i).get(j) - ((divs[i] + divs[j])/(this.divergences.size()-2));
//                    this.divergences.get(i).set(j, distance);
//                    this.divergences.get(j).set(i, distance);
//                }
//            }
//        }
//
//    }

    public enum HierarchicalClusteringType {

        SLINK("Single link"),
        CLINK("Complete link"),
        ALINK("Average link"),
        WARD("Ward Method");

        private HierarchicalClusteringType(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }

        private final String name;
    }

    private DistanceMatrix originalDmat;
    private boolean pnj, divergence;
    private Tree tree;
    private int maxId;
    private HierarchicalClusteringType type = HierarchicalClusteringType.CLINK;
    private ArrayList<ArrayList<Float>> distances = new ArrayList<ArrayList<Float>>();
    //private ArrayList<ArrayList<Float>> divergences = new ArrayList<ArrayList<Float>>();
    //private HashMap<Integer,Integer> mapIdIndex = new HashMap<Integer,Integer>();
    private ArrayList<Integer> mapIdIndex = new ArrayList<Integer>();
    
}
