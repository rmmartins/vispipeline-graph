package tree.technique.hctree;

import java.io.IOException;
import java.util.ArrayList;
import distance.dissimilarity.AbstractDissimilarity;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import tree.basics.ContentTree;
import tree.basics.Tree;
import tree.basics.TreeConstants;
import tree.technique.nj.NeighborJoining;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class HierarchicalClusteringTreeWard {

    public HierarchicalClusteringTreeWard(AbstractMatrix matrix,boolean pnj) {
        originalMatrix = matrix;
        load(pnj);
    }

    private void load(boolean pnj) {
        this.pnj = pnj;
        tree = new Tree();
        tree.setType(TreeConstants.HCT);
        for (int i=0;i<originalMatrix.getRowCount();i++) {
            if (originalMatrix.getLabels() != null && !originalMatrix.getLabels().isEmpty())
                tree.addNode(new ContentTree(originalMatrix.getIds().get(i),originalMatrix.getLabels().get(i),originalMatrix.getClassData()[i]));
            else
                tree.addNode(new ContentTree(originalMatrix.getIds().get(i),Integer.toString(i),originalMatrix.getClassData()[i]));
            if (originalMatrix.getIds().get(i) > maxId)
                maxId = originalMatrix.getIds().get(i);
        }
    }

    private AbstractVector generateCentroid(int c1, int tamc1, int c2, int tamc2) {
        try {
            AbstractVector v1 = this.centroids.get(c1);
            AbstractVector v2 = this.centroids.get(c2);
            AbstractVector centroid = null;
            if (v1.size() > v2.size())
                centroid = (AbstractVector) v1.clone();
            else
                centroid = (AbstractVector) v2.clone();
            for (int i = 0; i < centroid.size(); i++) {
                float value = (centroid.getValue(i) * tamc1 + v2.getValue(i) * tamc2) / (tamc1 + tamc2);
                centroid.setValue(i, value);
            }
            return centroid;
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(HierarchicalClusteringTreeWard.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    private float calculateError(int c1, int tamc1, int c2, int tamc2) {

        AbstractVector centroidC1 = this.centroids.get(c1);
        AbstractVector centroidC2 = this.centroids.get(c2);

        //System.out.println("-- Calculando erro entre "+c1+" e "+c2+". Dimensoes: "+centroidC1.size()+","+centroidC2.size());

        float erroTotal = 0;
        for (int i=0;i<centroidC1.size();i++) {
            //System.out.println("--- "+i+" processado");
            erroTotal += (centroidC1.getValue(i)-centroidC2.getValue(i))*(centroidC1.getValue(i)-centroidC2.getValue(i));
        }

        erroTotal *= (float)(tamc1*tamc2)/(float)(tamc1+tamc2);

        return erroTotal;

    }

    private void initErrors() throws IOException {

        //System.out.println("- Criando matriz de erros, n x n");
        //Cria a matriz de erros, no formato n x n, com a diagonal principal igual a 0.
        for (int i=0;i<originalMatrix.getRowCount();i++) {
            ArrayList<Float> lin = new ArrayList<Float>();
            for (int j=0;j<originalMatrix.getRowCount()-1;j++)
                lin.add(0.0f);
            this.errors.add(lin);
            //No inicio, cada centroide eh a propria instancia, pois os grupos sao unitarios...
            this.centroids.add(originalMatrix.getRow(i));
        }

        //System.out.println("- Preenchendo matriz de erros");
        //Calcula e preenche a matriz com os erros previstos na juncao de cada par de instancias
        for (int i=0;i<originalMatrix.getRowCount();i++) {
            for (int j=0;j<originalMatrix.getRowCount();j++) {
                if (i == j) this.errors.get(i).add(j, 0.0f);
                else {
                    float error = calculateError(i,1,j,1);
                    this.errors.get(i).set(j,error);
                    this.errors.get(j).set(i,error);
                }
            }
        }
//        for (int i=0;i<originalMatrix.getRowCount();i++) {
//            for (int j=i;j<originalMatrix.getRowCount();j++) {
//                if (i == j) this.errors.get(i).add(j, 0.0f);
//                else {
//                    float error = calculateError(i,1,j,1);
//                    this.errors.get(i).set(j,error);
//                    this.errors.get(j).set(i,error);
//                }
//            }
//        }
        //printErrors("D:\\errors_"+clusters.size()+".txt");

    }

    public Tree createTree() throws IOException {

        long init, end;

        //Inicialmente, crio clusters unitarios
        for (int i=0;i<originalMatrix.getRowCount();i++) {
            clusters.add(new ArrayList<Integer>());
            clusters.get(i).add(i);
            mapIdIndex.add(originalMatrix.getIds().get(i));
        }

        init = System.currentTimeMillis();

        //System.out.println("Iniciando dados...");

        initErrors();

        //Em cada iteracao, eu pego os dois clusters mais proximos. eles serao conectados a um no virtual.
        //Vou fazendo isso ate que sobre apenas um cluster, que sera a raiz da arvore.
        while (clusters.size() > 1) {
            //System.out.println("Comecando iteracao - "+clusters.size()+" clusters...");
            int[] cmin = null;
            //System.out.println("- Encontrando clusters mais proximos...");
            cmin = findNearestClusters(clusters);

            //System.out.println("-- "+cmin[0]+","+cmin[1]);

            //gero o centroide do novo grupo...
            //System.out.println("- Gerando centroide do novo cluster...");
            centroids.set(cmin[0],generateCentroid(cmin[0],clusters.get(cmin[0]).size(),cmin[1],clusters.get(cmin[1]).size()));

            //System.out.println("- Juntando os clusters mais proximos...");
            //Juntando os clusters (o conteudo do cmin[1] vai para cmin[0]
            for (int i = 0; i < clusters.get(cmin[1]).size(); i++) {
                clusters.get(cmin[0]).add(clusters.get(cmin[1]).get(i));
            }

            //System.out.println("- Criando um novo no virtual na arvore...");
            //crio o novo no e adiciono na arvore...
            int newId = createJunctionNode(cmin);

            //excluo o cmin[1]
            clusters.remove(cmin[1]);
            centroids.remove(cmin[1]);

            //Atualizo o indice do cmin[0] para ser o indice do novo no criado...
            mapIdIndex.set(cmin[0],newId);

            //Retiro o cmin[1] do mapeamento id index...
            mapIdIndex.remove(cmin[1]);

            //Faco o update da matriz de distancias
            //System.out.println("- Atualizando matriz de erros...");
            updateErrorMatrix(cmin,clusters);

            //printErrors("D:\\errors_"+clusters.size()+".txt");

        }

        //Realizando promoção de folhas, de acordo com opcao no pipeline...
        if (pnj) NeighborJoining.promoteLeafs(tree);

        end = System.currentTimeMillis();
        long diff = end - init;
        System.out.println("Time spent (HCT Connection creation) -> " + (diff/1000.0f) + " seconds");

        tree.generateEdges();

        return tree;

    }

    private int[] findNearestClusters(ArrayList<ArrayList<Integer>> clusters) {

        Float minError = new Float(errors.get(0).get(1));
        int[] cmin = new int[2];
        cmin[0] = 0;
        cmin[1] = 1;

        //Procurar os clusters mais proximos c1 e c2
        for (int c1=0;c1<clusters.size();c1++) {
            for (int c2=c1 + 1;c2<clusters.size();c2++) {
                int i2 = minError.compareTo(errors.get(c1).get(c2));
                if (i2 > 0) {
                //if (minError > errors.get(c1).get(c2)) {
                    minError = errors.get(c1).get(c2);
                    cmin[0] = c1;
                    cmin[1] = c2;
                }
            }
        }

        return cmin;
    }

    private int createJunctionNode(int[] nearestClusters) {

        //Junction Node properties
        ContentTree junctionNode, son1, son2;
        junctionNode = new ContentTree(++maxId,Integer.toString(tree.getSize()));
        son1 = tree.getNodeById(mapIdIndex.get(nearestClusters[0]));
        son2 = tree.getNodeById(mapIdIndex.get(nearestClusters[1]));

        ArrayList<ContentTree> children = new ArrayList<ContentTree>();
        children.add(son1);
        children.add(son2);
        junctionNode.setChildren(children);

        //o nivel do no de juncao sera um a mais do maior nivel entre seus filhos...
        if (son1.getNivelNo() > son2.getNivelNo())
            junctionNode.setNivelNo(son1.getNivelNo()+1);
        else junctionNode.setNivelNo(son2.getNivelNo()+1);

        float distSon1, distSon2;
        distSon1 = this.errors.get(nearestClusters[0]).get(nearestClusters[1])/2.0f;
        distSon2 = distSon1;

        ArrayList<Float> distChildren = new ArrayList<Float>();
        distChildren.add(distSon1);
        distChildren.add(distSon1);
        junctionNode.setDistChildren(distChildren);

        junctionNode.setValid(false);

        //adjusting sons properties
        son1.setParent(junctionNode);
        son2.setParent(junctionNode);

        son1.setDistParent(distSon1);
        son2.setDistParent(distSon2);

        son1.setRoot(junctionNode.getRoot());
        son2.setRoot(junctionNode.getRoot());

        tree.addNode(junctionNode);

        return junctionNode.getId();
    }

    private void updateErrorMatrix(int[] cmin, ArrayList<ArrayList<Integer>> clusters) {

        //Apago a linha das dist�ncias do cluster c2min
        errors.remove(cmin[1]);

        //Apago a coluna das dist�ncias do cluster c2min
        for (int k = 0; k < errors.size(); k++) {
            errors.get(k).remove(cmin[1]);
        }

        for (int i=0;i<errors.get(0).size();i++) {
            if (i != cmin[0]) {
                float newError = calculateError(cmin[0],clusters.get(cmin[0]).size(),i,clusters.get(i).size());
                errors.get(cmin[0]).set(i,newError);
                errors.get(i).set(cmin[0],newError);
            }
        }
        
    }

    public String truncateFloat(Float num, int numLength) {

        String stringDouble = String.valueOf(num);
        int iPoint = stringDouble.indexOf(".");
        int iMore = stringDouble.length() - (iPoint + numLength + 1);
        if( iMore < 0 )
            for(int i=0;i<(-iMore);i++)
                stringDouble = stringDouble + "0";
        else
            stringDouble=stringDouble.substring(0 , iPoint + numLength + 1 );
        return stringDouble;

    }

    private void printErrors(String arq_result) {

        String resultado = "";
        String eol = System.getProperty("line.separator");
        DecimalFormat df = new DecimalFormat("0.00");
        for (int i=0;i<errors.size();i++) {
            for (int j=0;j<errors.get(i).size();j++) {
                resultado += df.format(errors.get(i).get(j))+" ";
            }
            resultado += eol;
        }
        //Gerando arquivo com resultado...
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(arq_result)));
            bw.write(resultado);
            bw.flush();
            bw.close();
        } catch(IOException e){
            e.printStackTrace();
        }

    }

    private AbstractMatrix originalMatrix;
    private boolean pnj;
    private Tree tree;
    private int maxId;
    private ArrayList<ArrayList<Float>> errors = new ArrayList<ArrayList<Float>>();
    private ArrayList<Integer> mapIdIndex = new ArrayList<Integer>();
    private ArrayList<AbstractVector> centroids = new ArrayList<AbstractVector>();
    private ArrayList<ArrayList<Integer>> clusters = new ArrayList<ArrayList<Integer>>();
    
}
