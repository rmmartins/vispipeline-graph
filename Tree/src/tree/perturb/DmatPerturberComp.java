/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.perturb;

import distance.DistanceMatrix;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import projection.util.ProjectionConstants;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import visualizationbasics.util.PropertiesManager;

/**
 *
 * @author Jose Gustavo
 */
@VisComponent(hierarchy = "Distance.Perturb",
name = "Distance Matrix Perturber",
description = "Creates several perturbing distance matrices.")
public class DmatPerturberComp implements AbstractComponent {

    private DistanceMatrix applyCoef(DistanceMatrix dismat, double a, double d) throws CloneNotSupportedException {

        DistanceMatrix ndistmat = (DistanceMatrix) dismat.clone();
        float classes[] = ndistmat.getClassData();

        for (int i=0;i<classes.length;i++)
            for (int j=i+1;j<classes.length;j++)
                if (classes[i]==classes[j])
                    ndistmat.setDistance(i,j,ndistmat.getDistance(i,j)*(float)(1.0-(a/100.0)));
                else
                    ndistmat.setDistance(i,j,ndistmat.getDistance(i,j)*(float)(1.0+(d/100.0)));

        return ndistmat;

    }

    public void execute() throws IOException {

        PropertiesManager pm = null;
        results = new HashMap<String,DistanceMatrix>();
        if ((dmatsaveDirectory == null)||(dmatsaveDirectory.isEmpty())) {
            try {
                pm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
                if (pm != null) {
                    dmatsaveDirectory = pm.getProperty("UNZIP.DIR");
                    if (dmatsaveDirectory.isEmpty())
                        dmatsaveDirectory = System.getProperty("user.dir") + "\\tempDir\\";
                }
            } catch (IOException ex) {
                Logger.getLogger(DmatPerturberCompParamView.class.getName()).log(Level.SEVERE, null, ex);
                dmatsaveDirectory = System.getProperty("user.dir") + "\\tempDir\\";
            }
        }

        if (matrix != null) dmat = new DistanceMatrix(matrix,DissimilarityFactory.getInstance(disstype));
        else if (dmat == null) System.out.println("A matrix file or a distance matrix file should be provided!");

        if ((this.aproxCoef <= 0.0)||(this.aproxCoef > 100.0)) return;
        if ((this.distCoef <= 0.0)||(this.distCoef > 100.0)) return;
        if (this.numberMatrices <= 0) return;

        DistanceMatrix m = null;
        results.put("0-Original",dmat);
        double tempac = 0,tempdc = 0;
        //String arqText = "";
        for (int i=0;i<this.numberMatrices-1;i++) {
            tempac = (i+1)*this.aproxCoef;
            tempdc = (i+1)*this.distCoef;
            try {
                m = applyCoef(dmat,tempac,tempdc);
                results.put((i+1)+"-"+tempac+"_"+tempdc,m);
                if (saveDmats && !dmatsaveDirectory.isEmpty()) {
                    //saving distance matrix
                    System.out.println("Saving distance matrix "+i+"...");
                    m.save(dmatsaveDirectory+"\\"+dmatPref+"_"+tempac+"_"+tempdc+".dmat");
                }//else arqText += tempac+"_"+tempdc+";";
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(DmatPerturberComp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
//        if (!saveDmats) {
//            //Saving the text file containing the coefficients of the generated matrices...
//            try{
//                BufferedWriter bw = new BufferedWriter(new FileWriter(new File(dmatsaveDirectory+"dmatFiles")));
//                bw.write(arqText);
//                bw.flush();
//                bw.close();
//            } catch(IOException e){
//                e.printStackTrace();
//            }
//        }

    }

    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new DmatPerturberCompParamView(this);
        }
        return paramview;
    }

    public void reset() {
        matrix = null;
        dmat = null;
    }
    
    public void input(@Param(name = "Point matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "Distance matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public HashMap<String,DistanceMatrix> output() {
        return results;
    }

    public String getDmatSaveDirectory() {
        return dmatsaveDirectory;
    }

    public void setDmatSaveDirectory(String ts) {
        dmatsaveDirectory = ts;
    }
    
    public boolean isSaveDmats() {
        return saveDmats;
    }

    public void setSaveDmats(boolean saveDmats) {
        this.saveDmats = saveDmats;
    }

    public double getAproxCoef() {
        return aproxCoef;
    }

    public void setAproxCoef(double aproxCoef) {
        this.aproxCoef = aproxCoef;
    }

    public double getDistCoef() {
        return distCoef;
    }

    public void setDistCoef(double distCoef) {
        this.distCoef = distCoef;
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public int getNumberMatrices() {
        return numberMatrices;
    }

    public void setNumberMatrices(int i) {
        numberMatrices = i;
    }

    public String getDmatPref() {
        return dmatPref;
    }

    public void setDmatPref(String dmatPref) {
        this.dmatPref = dmatPref;
    }

    public static final long serialVersionUID = 1L;
    private transient DmatPerturberCompParamView paramview;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
    private transient String dmatsaveDirectory = "", dmatPref = "";
    private transient boolean saveDmats = false;
    private double aproxCoef, distCoef;
    private transient HashMap<String,DistanceMatrix> results;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private int numberMatrices;
    
}
