/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.view.perturb;

import org.xml.sax.helpers.DefaultHandler;
import projection.model.ProjectionModel;

/**
 *
 * @author Jose Gustavo
 */
public class Serie extends DefaultHandler {

        public Serie(String name, String filename) {
            this.name = name;
            this.filename = filename;
        }

        public Serie(ProjectionModel model) {
            this.model = model;
            this.name = model.toString();
        }

        public String name;
        public String filename;
        public ProjectionModel model;
    }
