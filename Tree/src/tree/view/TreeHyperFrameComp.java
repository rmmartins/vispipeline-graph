package tree.view;

import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import tree.model.TreeModel;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.View",
name = "Hyperbolic Tree View Frame",
description = "Display a tree model in hyperbolic view.")
public class TreeHyperFrameComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (model != null) {
            TreeModel nmodel = model.clone();
            System.out.println("HYPERFRAME");
            HyperFrame frame = new HyperFrame(nmodel);
            frame.setSize(600, 600);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
            frame.setTitle(title);
            //frame.setModel(model.clone());
            if (coordinators != null) {
                for (int i = 0; i < coordinators.size(); i++) {
                    frame.addCoordinator(coordinators.get(i));
                }
            }

        } else {
            throw new IOException("A tree model should be provided.");
        }
    }

    public void input(@Param(name = "tree model") TreeModel model) {
        this.model = model;
    }

    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
          if (paramview == null) {
           // paramview = new TreeFrameParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        model = null;
        coordinators = null;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle () {
        return title;
    }

    public static final long serialVersionUID = 1L;
    private String title = "";    
    private transient TreeModel model;
    private transient TreeFrameParamView paramview;
    private transient ArrayList<AbstractCoordinator> coordinators;
}
