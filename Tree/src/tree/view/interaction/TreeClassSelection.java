/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.view.interaction;

import labeledgraph.model.LabeledGraphInstance;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;
import projection.model.Scalar;
import tree.model.TreeInstance;
import tree.view.TreeFrame;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class TreeClassSelection extends TreeClassAbstractSelection {

    private Scalar ncdata = null;
    private float indexCdata = 0.0f;
    private int indexScalar;

    public TreeClassSelection(ModelViewer viewer) {
        super(viewer);
    }

    public boolean isElement(ArrayList<Scalar> scalars,String name) {
        if (scalars == null || scalars.isEmpty()) return false;
        for (int i=0;i<scalars.size();i++)
            if (scalars.get(i).getName().equalsIgnoreCase(name))
                return true;
        return false;
    }

    protected void setClass(ArrayList<AbstractInstance> selinst) {
        //Verifying if the scalar was deleted...
            if (((ProjectionModel)viewer.getModel()).getScalars().indexOf(ncdata)==-1) ncdata = null;

            if (ncdata == null) {
                while (isElement(((ProjectionModel)viewer.getModel()).getScalars(),"New Class "+indexScalar))
                    indexScalar++;
                ncdata = ((ProjectionModel)viewer.getModel()).addScalar("New Class "+indexScalar);
                indexScalar++;
                indexCdata = 0;
                //Setting all the instances with class 0 of this scalar...
                for (int i=0;i<viewer.getModel().getInstances().size();i++) {
                    TreeInstance pi = (TreeInstance) viewer.getModel().getInstances().get(i);
                    pi.setScalarValue(ncdata,indexCdata);
                }
                indexCdata++;
            }
            //Setting just the selected instances with the scalar...
            for (int i=0;i<selinst.size();i++) {
                ProjectionInstance pi = (ProjectionInstance) selinst.get(i);
                pi.setScalarValue(ncdata,indexCdata);
            }
            indexCdata++;
    }

//    protected void setClass(ArrayList<AbstractInstance> selinst) {
//        //Verifying if the scalar was deleted...
//            if (((TreeModel)viewer.getModel()).getScalars().indexOf(ncdata)==-1) ncdata = null;
//
//            if (ncdata == null) {
//                while (isElement(((TreeModel)viewer.getModel()).getScalars(),"New Class "+indexScalar))
//                    indexScalar++;
//                ncdata = ((TreeModel)viewer.getModel()).addScalar("New Class "+indexScalar);
//                indexScalar++;
//                indexCdata = 0;
//                //Setting all the instances with class 0 of this scalar...
//                for (int i=0;i<viewer.getModel().getInstances().size();i++) {
//                    TreeInstance pi = (TreeInstance) viewer.getModel().getInstances().get(i);
//                    pi.setScalarValue(ncdata,indexCdata);
//                }
//                indexCdata++;
//            }
//            //Setting just the selected instances with the scalar...
//            for (int i=0;i<selinst.size();i++) {
//                ProjectionInstance pi = (ProjectionInstance) selinst.get(i);
//                pi.setScalarValue(ncdata,indexCdata);
//            }
//            indexCdata++;
//    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (viewer.getModel() != null) {
            ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
            if (selinst.size() == 1) {
                instances = selectHierarchy((TreeInstance)selinst.get(0));
            }else {
                instances.addAll(selinst);
            }
            setClass(instances);
            ((TreeFrame)viewer).updateScalars(ncdata);
            viewer.getModel().setSelectedInstances(instances);
            viewer.getModel().notifyObservers();
        }
    }

    public void setNcdata(Scalar c) {
        ncdata = c;
    }

    public Scalar getNcdata() {
        return ncdata;
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/AlignCenter16.gif"));
    }

    @Override
    public String toString() {
        return "Class Selection";
    }

    @Override
    public void reset() {
        ncdata = null;
        indexScalar = 1;
    }

    @Override
    public void selected(int x, int y) {
    }

    @Override
    public void released(LabeledGraphInstance instance) {
    }

}
