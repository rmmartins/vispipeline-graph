/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tree.view.interaction;

import java.util.*;
import javax.swing.ImageIcon;
import labeledgraph.model.LabeledGraphInstance;
import tree.model.TreeInstance;
import tree.model.TreeModel;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 * Aims to select and show only a subset of the NJ Tree, to improve the speed and
 * interaction of massive data tree visualizations.
 * 
 * @author Renato Rodrigues Oliveira da Silva
 */
public class TreeMultiScaleSelection extends TreeAbstractSelection {

    
    private int maxNodesSelection = 1;
    private HashSet<TreeInstance> neighborsSet;
    private HashSet<TreeInstance> borderSet;

    
    public TreeMultiScaleSelection(ModelViewer viewer) {
        super(viewer);
    }    
    
    private List<TreeInstance> getNodeParents(TreeInstance node, int levels) {
        List<TreeInstance> parentList = new LinkedList<TreeInstance>();
        
        for (int i = 0; i < levels; i++) {
            TreeInstance parent = node.getParent();
            if (parent != null) {
                parentList.add(parent);
                node = parent;
            }
            else {
                break;
            }
        }
        
        if (node.getParent() != null)
            borderSet.add(node.getParent());
        
        return parentList;
    }
    
    private ArrayList<TreeInstance> getNodeSiblings(TreeInstance node) {
        if (node == null || node.getParent() == null)
            return null;
        
        ArrayList<TreeInstance> nodeSiblings = new ArrayList<TreeInstance>();
        nodeSiblings.addAll(node.getParent().getChildren());
        nodeSiblings.remove(node);
        
        return nodeSiblings;
    }
    
    private ArrayList<TreeInstance> getNodeChildren(TreeInstance node, int levels) {
        ArrayList<TreeInstance> childList = new ArrayList<TreeInstance>();
        
        if ((node.getChildren() == null) || (levels < 0))
            return null;
        
        // If levels > 0, keep running throughout the hierarchy of the tree
        if (levels > 0) {
            for (TreeInstance child: node.getChildren()) {
                // Don't scan the hierarchy of a node already scanned
                if (!this.neighborsSet.contains(child)) {
                    // 'Mark' the node to prevent scan it's hierarchy again
                    this.neighborsSet.add(child);
                    
                    childList.add(child);
                    childList.addAll(getNodeChildren(child, levels - 1));
                }
            }
        }
        // If levels = 0, mark the current children as the 'border' nodes 
        // of the new multiscale visualization
        if (levels == 0) {
            borderSet.addAll(node.getChildren());
        }
     
        return childList;
    }
    
    private ArrayList<AbstractInstance> getNodeNeighbors(TreeInstance node, int levels) {
        this.neighborsSet = new HashSet<TreeInstance>();
        this.borderSet = new HashSet<TreeInstance>();
        
        // 'Mark' the current node to prevent multiple scannings of it's hierarchy
        this.neighborsSet.add(node);
        
        List<TreeInstance> parentList = getNodeParents(node, levels);
        
        // Fist must scan the hierarchy of the parent nodes
        int currentLevel = levels - 1;
        for (TreeInstance parent: parentList) {
            neighborsSet.add(parent);
            neighborsSet.addAll(getNodeChildren(parent, currentLevel--));
        }
        
        // Then scan the hierarchy of the children
        neighborsSet.addAll(getNodeChildren(node, levels));
        
        return new ArrayList<AbstractInstance>(neighborsSet);
    }
    
    @Override
    public void selected(int x, int y) {
        System.out.println("Selected: X = " + x + ", Y = " + y);
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (selinst.size() == 1) {
            TreeModel treeModel = (TreeModel)viewer.getModel();
            
            ArrayList<AbstractInstance> nodeNeighbors = getNodeNeighbors((TreeInstance)selinst.get(0),
                    treeModel.getMultiScaleLevels());
            
            treeModel.setMultiScaleDraw(true);
            treeModel.setMultiScaleVisibleNodes(nodeNeighbors);
            treeModel.setMultiScaleBorderNodes(new ArrayList(borderSet));
            treeModel.setSelectedInstances(nodeNeighbors);
            
            for (int i=0;i<viewer.getCoordinators().size();i++) {
                viewer.getCoordinators().get(i).coordinate(selinst,null);
            }
            viewer.getModel().notifyObservers();
        }
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/AlignRight16.gif"));
    }

    @Override
    public String toString() {
        return "Tree Multi Scale Selection";
    }

    @Override
    public void released(LabeledGraphInstance instance) {
        
    }
    
}
