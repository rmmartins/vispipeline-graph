/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.view.interaction;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Jose Gustavo
 */
public abstract class TreeClassAbstractSelection extends TreeAbstractSelection {

    public TreeClassAbstractSelection(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public abstract void selected(ArrayList<AbstractInstance> selinst);

    @Override
    public abstract ImageIcon getIcon();

    @Override
    public abstract String toString();

    public abstract void reset();


}
