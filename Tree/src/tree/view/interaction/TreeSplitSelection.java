package tree.view.interaction;

import graph.model.Connectivity;
import graph.model.Edge;
import labeledgraph.model.LabeledGraphInstance;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import projection.model.Scalar;
import tree.model.TreeConnectivity;
import tree.model.TreeInstance;
import tree.model.TreeModel;
import tree.view.HyperFrame;
import tree.view.TreeFrame;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class TreeSplitSelection extends TreeAbstractSelection {

    public TreeSplitSelection(ModelViewer viewer) {
        super(viewer);
    }

    protected boolean isInInstances(ArrayList<AbstractInstance> instances, int id) {
        //System.out.println("Tentando "+id+"...");
        for (int i=0;i<instances.size();i++) {
            if (instances.get(i).getId() == id) {
                //System.out.println("- Encontrou!");
                return true;
            }
        }
        //System.out.println("- Não Encontrou!");
        return false;
    }

    @Override
    public void selected(int x, int y) {
    }

    private TreeModel getNewModel(TreeModel model,ArrayList<AbstractInstance> instances) {

        TreeModel newModel = new TreeModel();

        Scalar scalar;
        ArrayList<Scalar> sc = new ArrayList<Scalar>();
        for (int i=0;i<((TreeModel)model).getScalars().size();i++) {
            scalar = new Scalar(((TreeModel)model).getScalars().get(i).getName());
            scalar.store(((TreeModel)model).getScalars().get(i).getMin());
            scalar.store(((TreeModel)model).getScalars().get(i).getMax());
            sc.add(scalar);
        }
        newModel.setScalars(sc);

        int pos = ((TreeModel)model).getScalars().indexOf(((TreeModel)model).getSelectedScalar());

        if (pos == -1) {
            scalar = new Scalar(model.getSelectedScalar().getName());
            scalar.store(model.getSelectedScalar().getMin());
            scalar.store(model.getSelectedScalar().getMax());
            newModel.setSelectedScalar(scalar);
        }else {
            newModel.setSelectedScalar(sc.get(pos));
        }

        newModel.setImageCollection(model.getImageCollection());

        ArrayList<AbstractInstance> inst = new ArrayList<AbstractInstance>();
        for (int i=0;i<instances.size();i++) {
            TreeInstance ti = ((TreeInstance)instances.get(i)).createClone(newModel);
            ti.setModel(newModel);
            inst.add(ti);
        }

        //Adjusting new Treeinstances children...
        for (int i=0;i<inst.size();i++) {
            TreeInstance nti = (TreeInstance)inst.get(i);
            TreeInstance ti = model.getInstanceById(nti.getId());
            if (ti != null) {
                for (int j=0;j<ti.getChildren().size();j++) {
                    TreeInstance tic = ti.getChildren().get(j);
                    TreeInstance ntic = newModel.getInstanceById(tic.getId());
                    if (ntic != null) {
                        if (nti.getChildren() == null) nti.setChildren(new ArrayList<TreeInstance>());
                        nti.getChildren().add(ntic);
                    }
                }
            }
        }

        newModel.setInstances(inst);
        
        newModel.setAlpha(model.getAlpha());

        newModel.setColortable(model.getColorTable());

        newModel.setType(model.getType());

        TreeConnectivity conn = model.getSelectedConnectivity();
        ArrayList<Edge> newEdges = new ArrayList<Edge>();

        if (conn != null) {
            ArrayList<Edge> edges = conn.getEdges();
            if (edges != null) {
                for (int i=0;i<edges.size();i++) {
                    if ((isInInstances(instances,edges.get(i).getSource()))&&
                        (isInInstances(instances,edges.get(i).getTarget()))) {
                        newEdges.add(new Edge(edges.get(i).getSource(),edges.get(i).getTarget(),edges.get(i).getWeight()));
                    }
                }
            }
        }

        TreeConnectivity newCon = new TreeConnectivity(conn.getName(),newEdges);
        
        ArrayList<Connectivity> conns = newModel.getConnectivities();
        Connectivity c;
        ArrayList<Edge> edges;
        pos = -1;
        for (int i=0;i<model.getConnectivities().size();i++) {
            if (!model.getConnectivities().get(i).equals(conn)) {
                edges = new ArrayList<Edge>();
                    for (int j=0;j<model.getConnectivities().get(i).getEdges().size();j++) {
                        edges.add(new Edge(model.getConnectivities().get(i).getEdges().get(i).getSource(),
                                           model.getConnectivities().get(i).getEdges().get(i).getTarget()));
                    }
                    c = new TreeConnectivity(model.getConnectivities().get(i).getName(), edges);
                    conns.add(c);
            }else pos = i;
        }
        if (pos != -1)
            if (pos == conns.size()) conns.add(newCon);
            else conns.add(pos,newCon);
        //newModel.setConnectivities(conns);
        newModel.setSelectedConnectivity(newCon);
        newModel.setInstanceSize(model.getInstanceSize());

        return newModel;
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {

        if (viewer.getModel() != null) {

            ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
            if (selinst.size() == 1) {
                instances = selectHierarchy((TreeInstance)selinst.get(0));
            }else {
                instances.addAll(selinst);
            }

            //Criando um novo modelo, apenas com os vertices selecionados...
            TreeModel newModel = getNewModel((TreeModel)viewer.getModel(),instances);

            //Criando um novo frame com o novo modelo...
            if (viewer instanceof TreeFrame) {
                TreeFrame treeFrame = new TreeFrame();
                treeFrame.setSize(600, 600);
                treeFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                treeFrame.setVisible(true);
                treeFrame.setTitle(((TreeFrame)viewer).getTitle()+" (split)");
                treeFrame.setModel(newModel);
                treeFrame.setShowInstances(((TreeFrame)viewer).isShowInstanceLabel(),((TreeFrame)viewer).isShowInstanceImage());
                treeFrame.setDrawAs(((TreeFrame)viewer).getDrawAs());
                treeFrame.setLabelSize(((TreeFrame)viewer).getLabelSize());
                treeFrame.setLabelBold(((TreeFrame)viewer).getLabelBold());
                treeFrame.setDrawFrame(((TreeFrame)viewer).isDrawFrame());

//                int option = 0;
//                if (((TreeFrame)viewer).isShowInstanceLabel())
//                    option = 1;
//                else if (((TreeFrame)viewer).isShowInstanceImage())
//                    option = 2;

                //treeFrame.setlabelType(option);
                
                if (viewer.getCoordinators() != null) {
                    for (int i = 0; i < viewer.getCoordinators().size(); i++) {
                        treeFrame.addCoordinator(viewer.getCoordinators().get(i));
                    }
                }
            }
            else if (viewer instanceof HyperFrame) {
                HyperFrame hyperFrame = new HyperFrame(newModel);
                hyperFrame.setSize(600, 600);
                hyperFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                hyperFrame.setVisible(true);
                hyperFrame.setTitle(((TreeFrame)viewer).getTitle()+" (split)");
                //hyperFrame.setModel(newModel);
            }
            selinst.clear();
            //}
        }
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Cut16.gif"));
    }

    @Override
    public String toString() {
        return "Split Tree";
    }

    @Override
    public void released(LabeledGraphInstance instance) {
    }

}
