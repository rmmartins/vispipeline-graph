package tree.view.interaction;

import graph.model.Edge;
import labeledgraph.model.LabeledGraphInstance;
import labeledgraph.view.LabeledGraphFrame;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import tree.basics.TreeConstants;
import tree.model.TreeConnectivity;
import tree.model.TreeInstance;
import tree.model.TreeModel;
import tree.view.TreeFrame;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class TreeRearrangeSelection extends TreeAbstractSelection {

    public TreeRearrangeSelection(ModelViewer viewer) {
        super(viewer);
    }

    public TreeInstance getInstanceSamePosition(ArrayList<AbstractInstance> instances, TreeInstance inst) {
        int x = Math.round(inst.getX());
        int y = Math.round(inst.getY());
        for (AbstractInstance ti : instances) {
            if ((((TreeInstance)ti).isInside(x, y))&&(!ti.equals(inst))) {
                return (TreeInstance)ti;
            }
        }
        return null;
    }

    @Override
    public void released(LabeledGraphInstance selinst) {
        if (viewer.getModel() != null) {
            ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
            instances = selectHierarchy((TreeInstance)selinst);

            TreeInstance son = null, parent = null;

            //Tratando no filho
            son = (TreeInstance)selinst;
            if (son != null) {
                parent = getInstanceSamePosition(viewer.getModel().getInstances(),son);
                if (parent != null) {
                    //Deslocando o no inserido e sua hierarquia...
                    float dx,dy;
                    dx = dy = 30.0f; //Parametro fixo, definido arbitrariamente. Pode ser parametrizado posteriormente.

                    for (AbstractInstance v : instances) {
                        ((TreeInstance)v).setX(dx + ((TreeInstance)v).getX());
                        ((TreeInstance)v).setY(dy + ((TreeInstance)v).getY());
                    }
                    if (son.getParent() == null) {
                        if (!parent.isRelative(son)) { //Teste para evitar que se feche um ciclo na arvore
                            TreeConnectivity conn = ((TreeModel)viewer.getModel()).getSelectedConnectivity();
                            if (conn != null) {
                                ArrayList edges = conn.getEdges();
                                if (edges != null) {
                                    boolean cond = false;
                                    if (((TreeModel)viewer.getModel()).getType().equalsIgnoreCase(TreeConstants.NJ)) {
                                        if ((!parent.isValid())&&((parent.getChildren() == null)||(parent.getChildren().size() < 2))) cond = true;
                                    }else if (((TreeModel)viewer.getModel()).getType().equalsIgnoreCase(TreeConstants.SNJ)) {
                                        if ((parent.getChildren() == null)||(parent.getChildren().size() < 2)) cond = true;
                                    }else if (((TreeModel)viewer.getModel()).getType().equalsIgnoreCase(TreeConstants.MST)) {
                                        cond = true;
                                    }
                                    //Executando a conexao propriamente dita
                                    if (cond) {
                                        if (parent.getChildren() == null) parent.setChildren(new ArrayList<TreeInstance>());
                                        parent.getChildren().add(son);
                                        son.setParent(parent);
                                        //Criando uma aresta entre os nos
                                        Edge ed = new Edge(parent.getId(),son.getId());
                                        edges.add(ed);
//                                        //Deslocando o no inserido e sua hierarquia...
//                                        float dx,dy;
//                                        dx = dy = 30.0f; //Parametro fixo, definido arbitrariamente. Pode ser parametrizado posteriormente.
//
//                                        son.setX(son.getX()+dx);
//                                        son.setY(son.getY()+dy);
//
//                                        for (AbstractInstance v : instances) {
//                                            ((TreeInstance)v).setX(dx + ((TreeInstance)v).getX());
//                                            ((TreeInstance)v).setY(dy + ((TreeInstance)v).getY());
//                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (this.viewer.getModel() instanceof TreeModel) {
                ((TreeModel)this.viewer.getModel()).setNoAlpha(false);
                viewer.getModel().cleanSelectedInstances();
                viewer.getModel().setSelectedInstance(null);
            }
            if (this.viewer instanceof TreeFrame) {
                if (!((javax.swing.JToggleButton)((LabeledGraphFrame)this.viewer).getMoveInstancesToggleButton()).isSelected())
                    ((LabeledGraphFrame)this.viewer).setMoveInstance(false);
            }
            viewer.getModel().notifyObservers();
        }
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Refresh16.gif"));
    }

    @Override
    public String toString() {
        return "Rearrange Tree";
    }

    @Override
    public void selected(int x, int y) {
        
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        TreeSelection t = new TreeSelection(viewer);
        t.selected(selinst);
        if (this.viewer.getModel() instanceof TreeModel) {
            ((TreeModel)this.viewer.getModel()).setNoAlpha(true);
        }
        if (this.viewer instanceof TreeFrame) {
            ((TreeFrame)this.viewer).setMoveInstance(true);
        }
    }

}
