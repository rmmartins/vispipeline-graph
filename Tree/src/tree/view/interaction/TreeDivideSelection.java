package tree.view.interaction;

import graph.model.Edge;
import labeledgraph.view.interaction.LabeledGraphAbstractSelection;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import labeledgraph.model.LabeledGraphInstance;
import tree.model.TreeConnectivity;
import tree.model.TreeInstance;
import tree.model.TreeModel;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class TreeDivideSelection extends LabeledGraphAbstractSelection {

    public TreeDivideSelection(ModelViewer viewer) {
        super(viewer);
    }

    public Edge getEdgeByPosition(TreeConnectivity con, int x, int y) {

        ArrayList<Edge> edges = new ArrayList<Edge>();
        
        if (con != null) {
            edges = con.getEdges();
            if (edges != null) {
                for (Edge e : edges) {
                    if (isInsideEdge(e,x,y)) {
                        return e;
                    }
                }
            }
        }
        return null;
    }

    public boolean isInsideEdge(Edge e, int x, int y) {
        float xs,ys,xt,yt;
        TreeInstance ti;
        TreeModel tm = (TreeModel) this.viewer.getModel();
        ti = (TreeInstance) tm.getInstanceById(e.getSource());
        if (ti != null) {
            xs = ti.getX();
            ys = ti.getY();
        }else return false;
        ti = (TreeInstance) tm.getInstanceById(e.getTarget());
        if (ti != null) {
            xt = ti.getX();
            yt = ti.getY();
        }else return false;

        if (xs > xt) {
            if ((x < (int)xt)||(x > (int)xs)) return false;
        }else {
            if ((x < (int)xs)||(x > (int)xt)) return false;
        }

        if (ys > yt) {
            if ((y < (int)yt)||(y > (int)ys)) return false;
        }else {
            if ((y < (int)ys)||(y > (int)yt)) return false;
        }

        float a = ((yt-ys)/(xt-xs));
        float b = ys - (a*xs);
        float yr = ((a*x)+b);
        //System.out.println("y: "+y+", r: "+((a*x)+b)+", yy: "+(int)(yr)+".");
        return (Math.abs(y-(int)yr)<=3);

        //return ((y == Math.round(yr))||(y == (int)yr));
    }

    @Override
    public void selected(int x, int y) {
        TreeConnectivity con = ((TreeModel)this.viewer.getModel()).getSelectedConnectivity();
        if (con != null) {
            Edge edge = getEdgeByPosition(con, x, y);
            if (edge != null) {
                con.getEdges().remove(edge);
                //Desfazendo a relacao hierarquica entre os dois nos da edge
                TreeInstance source,target,parent;
                ArrayList<TreeInstance> children;
                source = (TreeInstance) ((TreeModel)this.viewer.getModel()).getInstanceById(edge.getSource());
                target = (TreeInstance) ((TreeModel)this.viewer.getModel()).getInstanceById(edge.getTarget());
                if (source != null) {
                    parent = source.getParent();
                    if ((parent != null)&&(parent.equals(target))) source.setParent(null);
                    children = source.getChildren();
                    if (children != null) {
                        for (int i=0;i<children.size();i++) {
                            if ((children.get(i) != null)&&(children.get(i).equals(target))) {
                                children.remove(i);
                                i--;
                            }
                        }
                    }
                }
                if (target != null) {
                    parent = target.getParent();
                    if ((parent != null)&&(parent.equals(source))) target.setParent(null);
                    children = target.getChildren();
                    if (children != null) {
                        for (int i=0;i<children.size();i++) {
                            if ((children.get(i) != null)&&(children.get(i).equals(source))) {
                                children.remove(i);
                                i--;
                            }
                        }
                    }
                 }
                 this.viewer.getModel().setChanged();
             }
         }
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Export16.gif"));
    }

    @Override
    public String toString() {
        return "Divide Tree";
    }

    @Override
    public void released(LabeledGraphInstance instance) {
    }

}
