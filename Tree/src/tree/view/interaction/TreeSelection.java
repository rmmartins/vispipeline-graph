package tree.view.interaction;

import labeledgraph.model.LabeledGraphInstance;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import tree.model.TreeInstance;
import tree.view.HyperFrame;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class TreeSelection extends TreeAbstractSelection {

    public TreeSelection(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(int x, int y) {
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (viewer.getModel() != null) {
            if (selinst.size() == 1) {
                selinst = selectHierarchy((TreeInstance)selinst.get(0));
            }
            viewer.getModel().setSelectedInstances(selinst);
            if (viewer instanceof HyperFrame) {
                if (selinst != null && !selinst.isEmpty()) {
                    ArrayList<TreeInstance> treeinsts = new ArrayList<TreeInstance>();
                    for (int i=0;i<selinst.size();i++) treeinsts.add((TreeInstance)selinst.get(i));
                    ((HyperFrame)viewer).getView().graphPanel.getGraph().setSelectedInstances(treeinsts);
                    TreeInstance inst = treeinsts.get(0);
                    ((HyperFrame)viewer).getView().centerVertex(inst);
                }
            }
            for (int i=0;i<viewer.getCoordinators().size();i++) {
                viewer.getCoordinators().get(i).coordinate(selinst,null);
            }
            viewer.getModel().notifyObservers();
        }
    }

    @Override
    public void released(LabeledGraphInstance instance) {
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/AlignRight16.gif"));
    }

    @Override
    public String toString() {
        return "Select Tree";
    }

}
