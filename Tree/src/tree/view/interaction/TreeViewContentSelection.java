/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.view.interaction;

import javax.swing.JFrame;
import labeledgraph.model.LabeledGraphInstance;
import labeledgraph.view.LabeledGraphReportView;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import labeledprojection.util.ImageCollection;
import labeledprojection.util.ProjectionOpenDialog;
import projection.util.ProjectionConstants;
import textprocessing.corpus.Corpus;
import textprocessing.processing.view.MultipleFileView;
import tree.model.TreeInstance;
import tree.model.TreeModel;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.filter.ZIPFilter;
import visualizationbasics.view.ModelViewer;
import textprocessing.processing.view.TextOpenDialog;
import tree.view.MultipleImageView;
import tree.view.TreeFrame;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class TreeViewContentSelection extends TreeAbstractSelection {

    public TreeViewContentSelection(ModelViewer viewer) {
        super(viewer);
    }

    public ArrayList<AbstractInstance> excludeVirtualNodes(ArrayList<AbstractInstance> instances) {
        ArrayList<AbstractInstance> result = new ArrayList<AbstractInstance>();
        result.addAll(instances);
        for (int i=0;i<result.size();i++) {
               AbstractInstance ins = result.get(i);
               if (ins.toString().trim().isEmpty()) {
                   result.remove(ins);
                   i--;
               }
            }
        return result;
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if ((selinst == null)||(selinst.isEmpty())) return;
        ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
        if (selinst.size() == 1) {
            instances = selectHierarchy((TreeInstance)selinst.get(0));
            //if (viewer.getModel() != null) viewer.getModel().setSelectedInstances(instances);
            instances = excludeVirtualNodes(instances);
        }else instances = excludeVirtualNodes(selinst);
        if (viewer.getModel() != null) {
            viewer.getModel().setSelectedInstances(instances);
            boolean isText = ((TreeInstance)instances.get(0)).toString().contains(".txt");
            ImageCollection im = null;
            Corpus c = null;
            if (!isText) im = ((TreeModel)viewer.getModel()).getImageCollection();
            else c = ((TreeModel)viewer.getModel()).getCorpus();

            if (!isText) {
                if ((im != null)&&(ProjectionOpenDialog.checkImages(im,viewer.getModel()))) {
                    MultipleImageView.getInstance((JFrame) viewer.getContainer()).display(im,instances);
                }else {
                    try {
                        PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
                        im = ProjectionOpenDialog.openImages(spm,new ZIPFilter(),viewer.getContainer());
                        if (ProjectionOpenDialog.checkImages(im,viewer.getModel())) {
                            ((TreeModel)viewer.getModel()).setImageCollection(im);
                            ((LabeledGraphReportView)(((TreeFrame)viewer).getReportPanel())).setDataSource(im.getFilename());
                            MultipleImageView.getInstance((JFrame) viewer.getContainer()).display(im,instances);
                        }else {
                            String message = "There are non-correponding files on the chosen images file. Would like to proceed?";
                            int answer = JOptionPane.showOptionDialog(viewer.getContainer(),message,"Openning Warning",
                                                                      JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE,null,null,null);
                            if (answer == JOptionPane.YES_OPTION) {
                                MultipleImageView.getInstance((JFrame) viewer.getContainer()).display(im,instances);
                            }
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(TreeViewContentSelection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else {
                if ((c != null)&&(TextOpenDialog.checkCorpus(c,viewer.getModel()))) {
                   MultipleFileView.getInstance((JFrame) viewer.getContainer()).display(c,instances);
                   // NewMultipleFileView.getInstance((JFrame) viewer.getContainer()).display(c,instances);
                }else {
                    try {
                        PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
                        c = TextOpenDialog.openCorpus(spm,new ZIPFilter(),viewer.getContainer());
                        if (TextOpenDialog.checkCorpus(c,viewer.getModel())) {
                            ((TreeModel)viewer.getModel()).setCorpus(c);
                            MultipleFileView.getInstance((JFrame) viewer.getContainer()).display(c,instances);
                            //NewMultipleFileView.getInstance((JFrame) viewer.getContainer()).display(c,instances);
                        }else {
                            String message = "There are non-correponding files on the chosen file. Would like to proceed?";
                            int answer = JOptionPane.showOptionDialog(viewer.getContainer(),message,"Openning Warning",
                                                                      JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE,null,null,null);
                            if (answer == JOptionPane.YES_OPTION) {
                                MultipleFileView.getInstance((JFrame) viewer.getContainer()).display(c,instances);
                                //NewMultipleFileView.getInstance((JFrame) viewer.getContainer()).display(c,instances);
                            }
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(TreeViewContentSelection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
//            if ((im == null)||(!OpenDialog.checkImages(im,viewer.getModel()))) {
//                try {
//                    PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
//                    im = OpenDialog.openImages(spm,new ZIPFilter(), viewer);
//                    if (OpenDialog.checkImages(im,viewer.getModel())) {
//                        ((TreeModel)viewer.getModel()).setImageCollection(im);
//                        MultipleImageView.getInstance(viewer).display(im,instances);
//                    }else {
//                        String message = "There are non-correponding files on the chosen images file. Would like to proceed?";
//                        int answer = JOptionPane.showOptionDialog(viewer,message,"Openning Warning",
//                                                                  JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE,null,null,null);
//                        if (answer == JOptionPane.YES_OPTION) {
//                            MultipleImageView.getInstance(viewer).display(im,instances);
//                        }
//                    }
//                } catch (IOException ex) {
//                    Logger.getLogger(TreeViewContentSelection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }else {
//                MultipleImageView.getInstance(viewer).display(im,instances);
//            }
            viewer.getModel().notifyObservers();
        }
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Copy16.gif"));
    }

    @Override
    public String toString() {
        return "View Content";
    }

    @Override
    public void selected(int x, int y) {
    }

    @Override
    public void released(LabeledGraphInstance instance) {
    }

}
