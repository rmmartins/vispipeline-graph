package tree.view;

import distance.DetailedDistanceMatrix;
import distance.DistanceMatrix;
import labeledgraph.coordination.GraphClassMatchCoordination;
import labeledgraph.model.LabeledGraphInstance;
import labeledgraph.view.LabeledGraphReportView;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import matrix.AbstractMatrix;
import detailedMatrix.dense.DetailedDenseMatrix;
import labeledprojection.util.ImageCollection;
import projection.model.Scalar;
import textprocessing.corpus.Corpus;
import tree.model.TreeModel;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.View",
name = "Tree View Frame",
description = "Display a tree model.")
public class TreeFrameComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (model != null) {
            TreeFrame frame = new TreeFrame();
            frame.setSize(600, 600);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
            frame.setTitle(title);
            frame.setModel(model);
            if (coordinators != null) {
                for (int i = 0; i < coordinators.size(); i++) {
                    frame.addCoordinator(coordinators.get(i));
                    if (coordinators.get(i) instanceof GraphClassMatchCoordination) {
                        Scalar sc = ((GraphClassMatchCoordination)coordinators.get(i)).match();
                    }
                }
            }
            //Setting report data...
            ((LabeledGraphReportView)frame.getReportPanel()).clean();
            boolean isText = ((LabeledGraphInstance)model.getInstances().get(0)).toString().contains(".txt");
            ImageCollection im = null;
            Corpus c = null;
            if (!isText) im = ((TreeModel)model).getImageCollection();
            else c = ((TreeModel)model).getCorpus();
            if (im != null) ((LabeledGraphReportView)frame.getReportPanel()).setDataSource(im.getFilename());
            else if (c != null) ((LabeledGraphReportView)frame.getReportPanel()).setDataSource(c.getUrl());
            ((LabeledGraphReportView)frame.getReportPanel()).setObjects(model.getValidInstances().size());
            ((LabeledGraphReportView)frame.getReportPanel()).setVirtualObjects(model.getInstances().size() - model.getValidInstances().size());
            ((LabeledGraphReportView)frame.getReportPanel()).setType(model.getType());

            if (matrix != null && matrix instanceof DetailedDenseMatrix) {
                if (matrix.getRowCount() == ((TreeModel)model).getValidInstances().size()) {
                    ((LabeledGraphReportView)frame.getReportPanel()).setSource(((DetailedDenseMatrix)matrix).getSource());
                    ((LabeledGraphReportView)frame.getReportPanel()).setDimensions(matrix.getDimensions());
                }
            }else if (dmat != null && dmat instanceof DetailedDistanceMatrix) {
                if (dmat.getElementCount() == ((TreeModel)model).getValidInstances().size()) {
                    ((LabeledGraphReportView)frame.getReportPanel()).setSource(((DetailedDistanceMatrix)dmat).getSource());
                }
            }
        } else {
            throw new IOException("A tree model should be provided.");
        }
    }

    public void input(@Param(name = "Original Distance Matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public void input(@Param(name = "Original Data Matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "tree model") TreeModel model) {
        this.model = model;
    }

    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
          if (paramview == null) {
            paramview = new TreeFrameParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        model = null;
        coordinators = null;
        matrix = null;
        dmat = null;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle () {
        return title;
    }

    public static final long serialVersionUID = 1L;
    private String title = "";    
    private transient TreeModel model;
    private transient TreeFrameParamView paramview;
    private transient ArrayList<AbstractCoordinator> coordinators;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
}
