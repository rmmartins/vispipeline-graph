package tree.view;

import labeledgraph.view.LabeledGraphFrameOptions;
import labeledgraph.view.LabeledGraphReportView;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import labeledprojection.util.ImageCollection;
import labeledprojection.util.ProjectionOpenDialog;
import projection.util.ProjectionConstants;
import tree.model.TreeInstance;
import tree.model.TreeModel;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.filter.ZIPFilter;

public class TreeFrameOptions extends LabeledGraphFrameOptions {

    private TreeFrameOptions(java.awt.Frame parent) {
        super(parent);
        viewer = (TreeFrame) parent;

        String[] items = {"none","file name","image"};
        this.labelsComboBox.setModel(new DefaultComboBoxModel(items));

        if (viewer.isShowInstanceLabel()) this.labelsComboBox.setSelectedIndex(1);
        else if(viewer.isShowInstanceImage()) this.labelsComboBox.setSelectedIndex(2);
        else this.labelsComboBox.setSelectedIndex(0);

        this.showImageFrameCheckBox.setSelected(viewer.isDrawFrame());

    }

    public static TreeFrameOptions getInstance(javax.swing.JFrame parent) {
        return new TreeFrameOptions(parent);
    }

    @Override
    protected void imagesRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {

        int oldDrawAs = 1;
       
        if ((viewer.getModel().getInstances() != null)&&(!viewer.getModel().getInstances().isEmpty()))
            oldDrawAs = ((TreeInstance)viewer.getModel().getInstances().get(0)).getDrawAs();
        ImageCollection im = ((TreeModel)viewer.getModel()).getImageCollection();
        if ((im == null)||(!ProjectionOpenDialog.checkImages(im,viewer.getModel()))) {
           try {
               PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
               im = ProjectionOpenDialog.openImages(spm,new ZIPFilter(), viewer);
               if (im == null) {
                   switch (oldDrawAs) {
                       case 0:
                           this.dotsRadioButton.setSelected(true);
                           break;
                       case 1:
                           this.circlesRadioButton.setSelected(true);
                           break;
                   }
               }else {
                   if (ProjectionOpenDialog.checkImages(im,viewer.getModel())) {
                       ((TreeModel)viewer.getModel()).setImageCollection(im);
                       ((LabeledGraphReportView)(viewer.getReportPanel())).setDataSource(im.getFilename());
                       //Populando imagens nas instancias...
                       populateImageInstances(im,viewer.getModel().getInstances());
                       viewer.setDrawAs(2);
                   }else {
                       JOptionPane.showMessageDialog(viewer,
                                            "The image collection file do not correspond to the projected image collection!",
                                            "Error",
                                            JOptionPane.ERROR_MESSAGE,null);
                       switch (oldDrawAs) {
                           case 0:
                               this.dotsRadioButton.setSelected(true);
                               break;
                           case 1:
                               this.circlesRadioButton.setSelected(true);
                               break;
                       }
                   }
               }
           } catch (IOException ex) {
               Logger.getLogger(TreeFrameOptions.class.getName()).log(Level.SEVERE, null, ex);
               viewer.setDrawAs(oldDrawAs);
           }
        }else {
            populateImageInstances(im,viewer.getModel().getInstances());
            viewer.setDrawAs(2);
        }
      
}

    @Override
    protected void labelsComboBoxActionPerformed(java.awt.event.ActionEvent evt) {
        int option = (int) ((JComboBox) evt.getSource()).getSelectedIndex();
        int noption = viewer.setlabelType(option);
        if (noption != option)
            this.labelsComboBox.setSelectedIndex(noption);
    }

    @Override
    protected void showImageFrameCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {
        viewer.setDrawFrame(this.showImageFrameCheckBox.isSelected());
    }

    @Override
    protected void imageSizeComboBoxActionPerformed(java.awt.event.ActionEvent evt) {
        String value = (String)this.imageSizeComboBox.getSelectedItem();
        boolean validNumber = true;
        for(int i=0;i<value.length();i++){
            Character caractere = value.charAt(i);
            if(!Character.isDigit(caractere)) {
                validNumber = false;
                break;
            }
        }

        if (validNumber && Integer.parseInt(value) > 0) {
            if (this.viewer != null && ((TreeModel)viewer.getModel()).getImageCollection() != null) {
                changeInstancesImageSize(((TreeModel)viewer.getModel()).getImageCollection(),viewer.getModel().getInstances(),Integer.parseInt(value));
                viewer.getView().adjustPanel();
                this.viewer.update(null,null);
                //this.viewer.setImageSize(Integer.parseInt((String)this.imageSizeComboBox.getSelectedItem()));
            }
        }else {
            JOptionPane.showMessageDialog(imageSizeComboBox,"Invalid number!");
            this.imageSizeComboBox.setSelectedItem("4");
        }
    }

    private void changeInstancesImageSize(ImageCollection imc, ArrayList instances, int size) {
        for (int i=0;i<instances.size();i++)
            ((TreeInstance)instances.get(i)).setImageSize(size);
        populateImageInstances(imc,instances);
    }

    private void populateImageInstances(ImageCollection imc, ArrayList instances) {
        for (int i=0;i<instances.size();i++) {
            TreeInstance ti = (TreeInstance) instances.get(i);
            if (!ti.toString().trim().isEmpty()) {
                try {
                    Image im = ti.getImage(imc);
                    if (im != null) {
                        ti.setImage(im.getScaledInstance(ti.getImageSize()*5,ti.getImageSize()*5, 0));
                    }
//                    if (lpi.getImage(imc) == null) {
//                        Image im = imc.getImage(lpi.toString());
//                        lpi.setImage(im.getScaledInstance(lpi.getSize()*5,lpi.getSize()*5, 0));
//                    }
                } catch (IOException ex) {
                }
            }
        }
    }

    private TreeFrame viewer;

}
