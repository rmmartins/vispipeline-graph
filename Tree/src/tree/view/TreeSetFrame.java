package tree.view;

import graph.model.Connectivity;
import labeledgraph.model.LabeledGraphInstance;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import labeledprojection.util.ImageCollection;
import labeledprojection.util.ProjectionSaveDialog;
import projection.model.Scalar;
import textprocessing.corpus.Corpus;
import tree.model.TreeModel;
import tree.model.xml.XMLTreeModelWriterComp;
import tree.view.perturb.*;
import visualizationbasics.model.AbstractModel;
import visualizationbasics.util.SaveDialog;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class TreeSetFrame extends TreeFrame {

    /** Creates new form ProjectionFrame */
    public TreeSetFrame() {
        super();
    }

    public TreeSetFrame(int maxSlider) {
        this();
        initComponents();
        this.treeSlider.setMaximum(maxSlider-1);
    }

    public void loadModel(AbstractModel model) {
        if (model instanceof TreeModel) {
            if (model != null) {

                //Dimension size = getSize();
                //size.height = (int) (size.height * 0.75f);
                //size.width = (int) (size.width * 0.95f);
                //((TreeModel) model).fitToSize(size);

                super.setModel(model);

                Scalar scalar = ((TreeModel) model).getSelectedScalar();

                if (scalar != null) {
                    updateScalars(scalar);
                } else {
                    updateScalars(((TreeModel) model).getScalars().get(0));
                }

                Connectivity selectedCon = ((TreeModel) model).getSelectedConnectivity();

                if (selectedCon != null) {
                    updateEdges(selectedCon);
                }else {
                    updateEdges(((TreeModel) model).getConnectivities().get(0));
                }

                view.setModel((TreeModel) model);

                //Setting report data...
                //((DTreeFrameReportView)this.getReportPanel()).clean();
                if (((TreeModel)model).getSource().contains("_-_"))
                    ((TreeModel)model).setSource(((TreeModel)model).getSource().substring(((TreeModel)model).getSource().indexOf("_-_")+3));


                if (((TreeModel)model).getSource().contains("_")) {
                    String[] values = ((TreeModel)model).getSource().split("_");
                    ((TreeSetFrameReportView)getReportPanel()).setApproximatingFactor(values[0]);
                    ((TreeSetFrameReportView)getReportPanel()).setDistancingFactor(values[1]);
                }else {
                    ((TreeSetFrameReportView)getReportPanel()).setApproximatingFactor("0.0");
                    ((TreeSetFrameReportView)getReportPanel()).setDistancingFactor("0.0");
                }
                
                boolean isText = ((LabeledGraphInstance)model.getInstances().get(0)).toString().contains(".txt");
                ImageCollection im = null;
                Corpus c = null;
                if (!isText) im = ((TreeModel)model).getImageCollection();
                else c = ((TreeModel)model).getCorpus();
                if (im != null) ((TreeSetFrameReportView)this.getReportPanel()).setDataSource(im.getFilename());
                else if (c != null) ((TreeSetFrameReportView)this.getReportPanel()).setDataSource(c.getUrl());
                ((TreeSetFrameReportView)this.getReportPanel()).setObjects(((TreeModel)model).getValidInstances().size());

                ((TreeSetFrameReportView)this.getReportPanel()).setType(((TreeModel)model).getType());

            }
        }
    }

    public void setModels(ArrayList<TreeModel> models) {

        this.models = models;
        for (int i=0;i<models.size();i++) setModel(models.get(i));
        loadModel(models.get(selectedModel));

    }

    private void treeSliderStateChanged(javax.swing.event.ChangeEvent evt) {
        JSlider comp = (JSlider) evt.getSource();

        if(!comp.getValueIsAdjusting()) {
            if (selectedModel != comp.getValue()) {
                //System.out.println(comp.getValue());
                selectedModel = comp.getValue();
                loadModel(models.get(selectedModel));
                updateImage();
            }
        }

    }

    private void nhitButtonActionPerformed(java.awt.event.ActionEvent evt) {

        ArrayList<Serie> series = new ArrayList<Serie>();
        for (int i=0;i<models.size();i++) {
            Serie serie = new Serie(models.get(i));
            series.add(serie);
        }
        NeighborhoodHit.getInstance(null).display(series,30,false);

    }

    private void openTreeSetMenuItemActionPerformed(ActionEvent evt) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void saveTreeSetMenuItemActionPerformed(ActionEvent evt) {
        
        int result = ProjectionSaveDialog.showDirSaveDialog(this,"");
        if (result == JFileChooser.APPROVE_OPTION) {
            if (!SaveDialog.getFilename().trim().isEmpty()) {
                String s = "";
                do {
                    s = (String)JOptionPane.showInputDialog(this,"Inform a preffix name for generated trees: ");
                    if (s == null || s.isEmpty()) {
                        int n = JOptionPane.showConfirmDialog(this,"Default name will be used. Confirm?","Warning",JOptionPane.YES_NO_OPTION);
                        if (n == JOptionPane.YES_OPTION) s = "TreeModel";
                    }
                }while (s == null || s.isEmpty());
                for (int i=0;i<models.size();i++) {
                    try {
                        XMLTreeModelWriterComp xmlw = new XMLTreeModelWriterComp();
                        xmlw.input(models.get(i));
                        xmlw.setFilename(SaveDialog.getFilename()+"\\"+s+"_-_"+((TreeModel)models.get(i)).getSource()+".xml");
                        xmlw.execute();
                    } catch (IOException ex) {
                        Logger.getLogger(TreeSetFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                JOptionPane.showMessageDialog(null,"Tree set saved.");
            }
        }
    }

    //@Override
    protected void initComponents() {
        //super.initComponents();
        DTreeReportPanel = new TreeSetFrameReportView();
        graphTabbedPane.remove(1);
        graphTabbedPane.addTab("Report",DTreeReportPanel);

        treeSlider = new javax.swing.JSlider();
        treeSlider.setMajorTickSpacing(1);
        treeSlider.setMaximum(3);
        treeSlider.setMinorTickSpacing(1);
        treeSlider.setPaintTicks(true);
        treeSlider.setSnapToTicks(true);
        treeSlider.setValue(0);
        treeSlider.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        treeSlider.setPreferredSize(new java.awt.Dimension(100, 31));
        treeSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                treeSliderStateChanged(evt);
            }
        });
        toolBar.add(treeSlider);

        nhitButton = new javax.swing.JButton();
        nhitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/History16.gif")));
        nhitButton.setToolTipText("Generate Neighborhood Hit");
        nhitButton.setFocusable(false);
        nhitButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        nhitButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        nhitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nhitButtonActionPerformed(evt);
            }
        });
        fixedToolBar.add(nhitButton);

        treeSetMenu = new javax.swing.JMenu();
        openTreeSetMenuItem = new javax.swing.JMenuItem();
        saveTreeSetMenuItem = new javax.swing.JMenuItem();

        treeSetMenu.setText("Tree Set");
        openTreeSetMenuItem.setText("Open Tree Set");
        openTreeSetMenuItem.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                        openTreeSetMenuItemActionPerformed(evt);
                }
        });
        treeSetMenu.add(openTreeSetMenuItem);
        saveTreeSetMenuItem.setText("Save Tree Set");
        saveTreeSetMenuItem.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                        saveTreeSetMenuItemActionPerformed(evt);
                }
        });
        treeSetMenu.add(saveTreeSetMenuItem);

        menuFile.add(treeSetMenu);

        pack();

    }

    @Override
    public javax.swing.JPanel getReportPanel() {
        return DTreeReportPanel;
    }

    private ArrayList<TreeModel> models;
    private int selectedModel = 0;
    private javax.swing.JSlider treeSlider;
    protected javax.swing.JButton nhitButton;
    private javax.swing.JPanel DTreeReportPanel;
    private javax.swing.JMenu treeSetMenu;
    private javax.swing.JMenuItem openTreeSetMenuItem;
    private javax.swing.JMenuItem saveTreeSetMenuItem;

}
