package tree.view.tools;

import java.io.IOException;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class TreeModelSilhouetteCoefficientParamView extends AbstractParametersView {

    public TreeModelSilhouetteCoefficientParamView(TreeModelSilhouetteCoefficientModelComp comp) {
        initComponents();
        this.comp = comp;
        reset();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        chooseDistanceTypePanel2 = new javax.swing.JPanel();
        useWeightsCheckBox = new javax.swing.JCheckBox();
        titlePanel = new javax.swing.JPanel();
        titleLabel = new javax.swing.JLabel();
        titleTextField = new javax.swing.JTextField();

        setLayout(new java.awt.GridBagLayout());

        chooseDistanceTypePanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        chooseDistanceTypePanel2.setLayout(new java.awt.GridBagLayout());

        useWeightsCheckBox.setText("Use Edges Weights");
        useWeightsCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        chooseDistanceTypePanel2.add(useWeightsCheckBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(chooseDistanceTypePanel2, gridBagConstraints);

        titleLabel.setText("Title : ");
        titlePanel.add(titleLabel);

        titleTextField.setColumns(15);
        titlePanel.add(titleTextField);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(titlePanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void finished() throws IOException {
       comp.setUseWeight(this.useWeightsCheckBox.isSelected());
       comp.setTitle(titleTextField.getText().trim());
    }
    
    @Override
    public void reset() {
        this.useWeightsCheckBox.setSelected(comp.isUseWeight());
        titleTextField.setText(comp.getTitle());
    }

    private TreeModelSilhouetteCoefficientModelComp comp;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel chooseDistanceTypePanel2;
    private javax.swing.JLabel titleLabel;
    private javax.swing.JPanel titlePanel;
    private javax.swing.JTextField titleTextField;
    private javax.swing.JCheckBox useWeightsCheckBox;
    // End of variables declaration//GEN-END:variables

}
