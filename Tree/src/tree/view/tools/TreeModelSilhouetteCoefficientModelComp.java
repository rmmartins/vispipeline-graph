package tree.view.tools;

import distance.DistanceMatrix;
import graph.model.Edge;
import java.io.IOException;
import java.util.ArrayList;
import labeledgraph.model.LabeledGraphInstance;
import labeledgraph.model.LabeledGraphModel;
import labeledprojection.view.tools.silhouette.DataSilhouetteCoefficient;
import projection.model.Scalar;
import tree.model.TreeModel;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Data Analysis.Silhouette",
name = "Tree Model Silhouette Coefficient",
description = "Display the silhouette coefficient of a clustered dataset.")
public class TreeModelSilhouetteCoefficientModelComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        DataSilhouetteCoefficient sc = new DataSilhouetteCoefficient();
        DistanceMatrix dmat = null;
        float[] silhouette = null;
        
        if (model != null) {
            Scalar scalar = model.getScalarByName("cdata");
            if (scalar == null)
                if (model.getScalars() != null && !model.getScalars().isEmpty())
                    scalar = model.getScalars().get(0);


            dmat = createDistanceMatrix(model,scalar,isUseWeight());
            silhouette = sc.execute(dmat);

            String result = "";
            if (silhouette != null) {
                //showing the average silhouette on console
                float average = sc.average(silhouette);
                result = "Silhouette coefficient:\r\n";
                result += "Original Clustering: " + average + "\r\n";
                result += "---\r\n\n";
            }else {
                result += "Error generating Silhouette coefficient.";
            }
            TreeModelSilhouetteCoefficientConsole.getInstance(null).display(title,result);
        }
    }

    public void input(@Param(name = "Tree Model") TreeModel model) {
        this.model = model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
          if (paramview == null) {
            paramview = new TreeModelSilhouetteCoefficientParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        
    }

    private DistanceMatrix createDistanceMatrix(LabeledGraphModel model, Scalar scalar, boolean useWeights) {

        DistanceMatrix dm = new DistanceMatrix();

        float[][] dmat = new float[model.getInstances().size()][model.getInstances().size()];
        for (int i=0;i<dmat.length;i++)
            for (int j=0;j<dmat[i].length;j++)
                if (i == j) dmat[i][j] = 0;
                else dmat[i][j] = Float.MAX_VALUE;

        ArrayList<Edge> edges = null;

        if (model.getSelectedConnectivity() != null)
                edges = model.getSelectedConnectivity().getEdges();
            else if ((model.getConnectivities() != null)&&(model.getConnectivities().size() > 1))
                    edges = model.getConnectivities().get(1).getEdges();

        if (edges == null) return null;

        for (int k=0;k<edges.size();k++) {
            Edge ed = edges.get(k);
            int x = model.getInstances().indexOf(model.getInstanceById(ed.getSource()));
            int y = model.getInstances().indexOf(model.getInstanceById(ed.getTarget()));
            if (useWeights) dmat[x][y] = dmat[y][x] = ed.getWeight();
            else dmat[x][y] = dmat[y][x] = 1.0f;
        }

        //Calculating the shortest path, in the tree, among all nodes (including virtual nodes)
        //Floyd Warshall algorithm.
        int n = dmat.length;
        for (int k=0; k<n; k++)
            for (int i=0; i<n; i++)
                for (int j=0; j<n; j++) {
                    float dd = dmat[i][k] + dmat[k][j];
                    if (dmat[i][j] > dd) dmat[i][j] = dd;
                }

        int k = -1;
        ArrayList<Integer> ids = new ArrayList<Integer>();
        ArrayList<ArrayList<Float>> ndmat = new ArrayList<ArrayList<Float>>();
        for (int i=0;i<dmat.length;i++) {
            if (!model.getInstances().get(i).toString().isEmpty()) {
                ids.add(model.getInstances().get(i).getId());
                k++;
                ndmat.add(new ArrayList<Float>());
                for (int j=0;j<dmat[i].length;j++) {
                    if (!model.getInstances().get(j).toString().isEmpty()) {
                        ndmat.get(k).add(dmat[i][j]);
                    }
                }
            }
        }

        //Create and fill the distance distmatrix
        dm.setElementCount(ndmat.size());

        float maxDistance = Float.NEGATIVE_INFINITY;
        float minDistance = Float.POSITIVE_INFINITY;

        float[][] distmat = new float[ndmat.size() - 1][];
        for (int i=0; i<ndmat.size()-1; i++) {
            distmat[i] = new float[i + 1];
            for (int j=0;j<distmat[i].length; j++) {
                float distance = ndmat.get(i+1).get(j);
                if (distance < minDistance) minDistance = distance;
                if (distance > maxDistance) maxDistance = distance;
                if ((i+1)!=j) {
                    if ((i+1) < j) distmat[j - 1][(i+1)] = distance;
                    else distmat[(i+1) - 1][j] = distance;
                }
            }
        }
        dm.setMinDistance(minDistance);
        dm.setMaxDistance(maxDistance);
        dm.setDistmatrix(distmat);

        //class data and labels...
        ArrayList<String> labels = new ArrayList<String>();
        float[] classdata = new float[ids.size()];
        for (int i=0;i<ids.size();i++) {
            LabeledGraphInstance gi = (LabeledGraphInstance) model.getInstanceById(ids.get(i));
            if (gi != null) {
                labels.add(gi.getLabel());
                classdata[i] = gi.getScalarValue(scalar);
            }
        }

        dm.setClassData(classdata);
        dm.setIds(ids);
        dm.setLabels(labels);

        return dm;

    }
    
    public boolean isUseWeight() {
        return useWeight;
    }

    public void setUseWeight(boolean useWeigth) {
        this.useWeight = useWeigth;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String t) {
        title = t;
    }

    public static final long serialVersionUID = 1L;
    private String title = "";
    private transient LabeledGraphModel model;
    private transient TreeModelSilhouetteCoefficientParamView paramview;
    private boolean useWeight;
    
}
