package tree.view.tools.correlation;

import distance.DetailedDistanceMatrix;
import distance.DistanceMatrix;
import java.io.IOException;
import java.util.ArrayList;
import tree.model.TreeInstance;
import tree.model.TreeModel;

/**
 *
 * @author Jose Gustavo
 */
public class TreeCorrelationCoefficient {

    public String execute(DistanceMatrix dmat, ArrayList<TreeModel> models, ArrayList<DetailedDistanceMatrix> projectionDmats) throws IOException {

        String result = "";
        ArrayList<float[]> distanceVectorsO = new ArrayList<float[]>();
        ArrayList<float[]> distanceVectorsP = new ArrayList<float[]>();

        for (int i=0;i<dmat.getElementCount();i++) {
            distanceVectorsO.add(new float[dmat.getElementCount()]);
            for (int k=0;k<dmat.getElementCount();k++) {
                distanceVectorsO.get(i)[k] = dmat.getDistance(i,k);
            }
        }

        //Calculating correlation coefficients for models provided...
        if (models != null && !models.isEmpty()) {
            for (int i=0;i<models.size();i++) {
                TreeModel model = models.get(i);
                distanceVectorsP = new ArrayList<float[]>();
                for (int j=0;j<model.getValidInstances().size();j++) {
                    distanceVectorsP.add(new float[model.getValidInstances().size()]);
                    float[] c1 = new float[2];
                    c1[0] = ((TreeInstance)model.getValidInstances().get(j)).getX();
                    c1[1] = ((TreeInstance)model.getValidInstances().get(j)).getY();
                    for (int k=0;k<model.getValidInstances().size();k++) {
                        float[] c2 = new float[2];
                        c2[0] = ((TreeInstance)model.getValidInstances().get(k)).getX();
                        c2[1] = ((TreeInstance)model.getValidInstances().get(k)).getY();
                        float dist = (float)Math.sqrt(Math.pow(c1[0]-c2[0],2)+Math.pow(c1[1]-c2[1],2));
                        distanceVectorsP.get(j)[k] = dist;
                    }
                }
                float correlation = getCorrelationCoefficient(distanceVectorsO,distanceVectorsP);
                result += model.toString()+" correlation coefficient: "+correlation+"\r\n";
            }
        }

        //Calculating correlation coefficients from datasets (points matrix or distance matrix) provided...
        if (projectionDmats != null && !projectionDmats.isEmpty()) {
            for (int i=0;i<projectionDmats.size();i++) {
                distanceVectorsP = new ArrayList<float[]>();
                for (int j=0;j<projectionDmats.get(i).getElementCount();j++) {
                    distanceVectorsP.add(new float[projectionDmats.get(i).getElementCount()]);
                    for (int k=0;k<projectionDmats.get(i).getElementCount();k++) {
                        distanceVectorsP.get(j)[k] = projectionDmats.get(i).getDistance(j,k);
                    }
                }
                for (int ii=0;ii<distanceVectorsP.size();ii++) {
                    System.out.println();
                    for (int jj=0;jj<distanceVectorsP.get(ii).length;jj++)
                        System.out.print(distanceVectorsP.get(ii)[jj]+" ");
                }
                
                float correlation = getCorrelationCoefficient(distanceVectorsO,distanceVectorsP);
                if (projectionDmats.get(i) instanceof DetailedDistanceMatrix)
                    result += ((DetailedDistanceMatrix)projectionDmats.get(i)).getName()+" correlation coefficient: "+correlation+"\r\n";
                else
                    result += "Distance Matrix "+i+" correlation coefficient: "+correlation+"\r\n";
            }
        }
        return result;

    }

    private float getMean(float[] vector) {
        if (vector.length < 1) return Float.NaN;
        float sum = 0.0f;
        for (int i=0;i<vector.length;i++)
            sum += vector[i];
        return sum/vector.length;
    }

    private float getStdDev(float[] vector) {
        float mean = getMean(vector);
        float sum = 0;
        for (int i=0;i<vector.length;i++)
            sum += Math.pow((vector[i]-mean),2);
        return (float)Math.sqrt(sum/(vector.length-1));
    }

    private float[] product(float[] vec1, float[] vec2) {
        float[] ret = new float[vec1.length];
        for (int i=0;i<vec1.length;i++)
            ret[i] = vec1[i]*vec2[i];
        return ret;
    }

    private float getCorrelationCoefficient(ArrayList<float[]> distanceVectorsO, ArrayList<float[]> distanceVectorsP) {

        if (distanceVectorsO.size() != distanceVectorsP.size()) return Float.NaN;

        float meanProduct, meanO, meanP, stDevO, stDevP;
        float correlation = 0;

        for (int i=0;i<distanceVectorsO.size();i++) {
            meanProduct = getMean(product(distanceVectorsO.get(i),distanceVectorsP.get(i)));
            meanO = getMean(distanceVectorsO.get(i));
            meanP = getMean(distanceVectorsP.get(i));
            stDevO = getStdDev(distanceVectorsO.get(i));
            stDevP = getStdDev(distanceVectorsP.get(i));
            correlation += (meanProduct-(meanO*meanP))/(stDevO*stDevP);
        }
        return correlation/distanceVectorsO.size();

    }

}
