/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.view;

import java.io.IOException;
import java.util.ArrayList;
import projection.model.ProjectionModel;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Coordination",
name = "Class Match Coordination tree",
description = "Create an class match coordination object.")
public class ClassMatchComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        coord = new ClassMatch();
    }

    public AbstractCoordinator output() {
        return coord;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        coord = null;
    }

    public static final long serialVersionUID = 1L;
    private transient ClassMatch coord;
    private transient ArrayList<ProjectionModel> models;
}
