/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.view;

import labeledprojection.model.LabeledProjectionModel;
import labeledprojection.model.LabeledProjectionInstance;
import projection.model.Scalar;
import visualizationbasics.coordination.IdentityCoordinator;
import visualizationbasics.model.AbstractModel;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class ClassMatch extends IdentityCoordinator {

    public void match() {
        Scalar sc1,sc2,cdata1,cdata2;
        LabeledProjectionInstance pi1,pi2;
        if (models.size() >= 2) {
            for (int i=0;i<2;i++) { //Por enquanto, só para dois modelos....
                cdata1 = ((LabeledProjectionModel)models.get(i)).getScalarByName("cdata");
                if (cdata1 != null) {
                    sc1 = ((LabeledProjectionModel)models.get(i)).getScalarByName("ClassMatch");
                    if (sc1 == null) sc1 = ((LabeledProjectionModel)models.get(i)).addScalar("ClassMatch");
                    for (int j=0;j<models.get(i).getInstances().size();j++) {
                        pi1 = (LabeledProjectionInstance)models.get(i).getInstances().get(i);
                        for (int k=0;k<2;k++) {
                            if (k != i) {
                                cdata2 = ((LabeledProjectionModel)models.get(k)).getScalarByName("cdata");
                                if (cdata2 != null) {
                                    sc2 = ((LabeledProjectionModel)models.get(k)).getScalarByName("ClassMatch");
                                    if (sc2 == null) sc2 = ((LabeledProjectionModel)models.get(k)).addScalar("ClassMatch");
                                    pi2 = ((LabeledProjectionModel)models.get(k)).getInstanceByLabel(pi1.getLabel());
                                    if (pi2 != null) {
                                        if (pi1.getScalarValue(cdata1) == pi2.getScalarValue(cdata2)) {//class match...
                                            pi2.setScalarValue(sc1,1);
                                            pi1.setScalarValue(sc2,1);
                                        }else { //class does not match...
                                            pi2.setScalarValue(sc1,2);
                                            pi1.setScalarValue(sc2,2);
                                        }
                                    }else { //instance does not exist on the other model...
                                        pi1.setScalarValue(sc1,0);
                                    }
                                }
                            }
                        }
                    }
                }
                models.get(i).notifyObservers();
            }
        }
    }

    @Override
    public synchronized void addModel(AbstractModel model) {
        super.addModel(model);
        //match();
    }

}
