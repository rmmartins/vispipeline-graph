package tree.view;

import datamining.clustering.HierarchicalClustering;
import datamining.clustering.HierarchicalClustering.HierarchicalClusteringType;
import datamining.neighbors.Pair;
import distance.dissimilarity.Euclidean;
import graph.model.Edge;
import graph.util.Delaunay;
import graph.util.GraphConstants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import labeledgraph.view.LabeledGraphFrame;
import labeledgraph.view.LabeledGraphReportView;
import labeledgraph.view.tools.GraphCorrelationCoefficientView;
import labeledprojection.model.LabeledProjectionModelComp;
import labeledprojection.util.ImageCollection;
import labeledprojection.util.ProjectionOpenDialog;
import labeledprojection.view.LabeledProjectionFrameComp;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import projection.model.Scalar;
import projection.util.ProjectionConstants;
import projection.util.filter.XMLFilter;
import tree.basics.TreeConstants;
import tree.model.TreeConnectivity;
import tree.model.TreeInstance;
import tree.model.TreeModel;
import tree.model.xml.XMLTreeModelReaderComp;
import tree.model.xml.XMLTreeModelWriterComp;
import tree.view.forms.SaveSampleDialog;
import tree.view.interaction.*;
import tree.view.tools.JDropDownToggleButton;
import tree.view.tools.TreeMultiscaleParamView;
import tree.view.tools.correlation.TreeCorrelationCoefficientView;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.model.AbstractModel;
import visualizationbasics.util.OpenDialog;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.SaveDialog;
import visualizationbasics.util.filter.ZIPFilter;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class TreeFrame extends LabeledGraphFrame {

    /** Creates new form ProjectionFrame */
    public TreeFrame() {
        super();
        TreeSelection t = new TreeSelection(this);
        addSelection(t,true);
        this.view.setSelection(t);
        addSelection(new TreeViewContentSelection(this),false);
        addSelection(new TreeDivideSelection(this),false);
        addSelection(new TreeSplitSelection(this),false);
        addSelection(new TreeRearrangeSelection(this),false);
        addSelection(new TreeClassSelection(this),false);
        
        JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem menuItem = new JMenuItem();
        menuItem.setText("Select parameters...");
        menuItem.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    TreeMultiscaleParamView paramView = new TreeMultiscaleParamView(TreeFrame.this, true);
                    paramView.show(((TreeModel)model));
                }
        });
        popupMenu.add(menuItem);
        
        addSelection(new TreeMultiScaleSelection(this), false, popupMenu);
    }

    public void addSelection(final TreeClassAbstractSelection selection, boolean state) {
        if (selection != null) {
            JDropDownToggleButton button = new JDropDownToggleButton();
            selectionButtonGroup.add(button);
            button.setIcon(selection.getIcon());
            button.setSelected(false);
            button.setToolTipText(selection.toString());
            button.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (getView() != null) {
                        getView().setSelection(selection);
                        selection.reset();
                    }
                }
            });
            button.setSelected(state);
            selectionToolBar.add(button);
        }
    }
    
    public void addSelection(final TreeAbstractSelection selection, boolean state, JPopupMenu buttonMenu) {
        if (selection != null) {
            JDropDownToggleButton button = new JDropDownToggleButton(selection.getIcon(), buttonMenu);
            selectionButtonGroup.add(button);
            button.setSelected(false);
            button.setToolTipText(selection.toString());
            button.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (getView() != null) {
                        getView().setSelection(selection);
                    }
                }
            });
            button.addChangeListener(new javax.swing.event.ChangeListener() {
                public void stateChanged(javax.swing.event.ChangeEvent changeEvent) {
                    
                    AbstractButton abstractButton = (AbstractButton) changeEvent.getSource();
                    ButtonModel buttonModel = abstractButton.getModel();
                    if (!buttonModel.isSelected()) {
                        ((TreeModel)model).setMultiScaleDraw(false);
                        model.setChanged();
                        model.notifyObservers();
                    }
                }
            });
            button.setSelected(state);
            selectionToolBar.add(button);
        }
    }
    
    @Override
    protected void toolOptionsActionPerformed(java.awt.event.ActionEvent evt) {
        TreeFrameOptions.getInstance(this).display(this);
    }

    @Override
    protected void toolButtonActionPerformed(java.awt.event.ActionEvent evt) {
        toolOptionsActionPerformed(evt);
    }

    @Override
    protected void delaunayMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        ((TreeModel)model).perturb();

        //Creating new Delaunay triangulation
        float[][] points = new float[model.getInstances().size()][];
        for (int i = 0; i < points.length; i++) {
            points[i] = new float[2];
            points[i][0] = ((TreeInstance)model.getInstances().get(i)).getX();
            points[i][1] = ((TreeInstance)model.getInstances().get(i)).getY();
        }

        Delaunay d = new Delaunay();
        Pair[][] neighborhood = d.execute(points);

        HashMap<Integer, TreeInstance> index = new HashMap<Integer, TreeInstance>();
        for (int i=0;i<model.getInstances().size();i++) {
            index.put(i,(TreeInstance)model.getInstances().get(i));
        }

        ArrayList<Edge> edges = new ArrayList<Edge>();

        for (int i = 0; i < neighborhood.length; i++) {
            for (int j = 0; j < neighborhood[i].length; j++) {
                edges.add(new Edge(index.get(i).getId(),index.get(neighborhood[i][j].index).getId(),neighborhood[i][j].value));
            }
        }

        TreeConnectivity delaunayCon = new TreeConnectivity("Delaunay",edges);
        ((TreeModel)model).addConnectivity(delaunayCon);

        //Connectivity dotsCon = new Connectivity(ProjectionConstants.DOTS, new ArrayList<Edge>());

//        edgeComboModel.removeAllElements();
//        edgeComboModel.addElement(dotsCon);
//        edgeComboModel.addElement(delaunayCon);
//        edgeComboModel.setSelectedItem(delaunayCon);
//
//        edgeLabel.setVisible(true);
//        edgeCombo.setVisible(true);

        updateEdges(delaunayCon);

    }

    @Override
    public void setModel(AbstractModel model) {
        super.setModel(model);
        if (model instanceof TreeModel) {
            view.setModel((TreeModel) model);
        }
    }

    @Override
    protected void createNJProjectionMenuItemActionPerformed(java.awt.event.ActionEvent evt) {

        AbstractMatrix projection = null;
        ArrayList<String> labels = new ArrayList<String>();
        if (model != null) {
            try {
                //Excluding virtual nodes...
                projection = new DenseMatrix();
                Scalar scalar = ((TreeModel) model).getSelectedScalar();
                if (scalar == null) {
                    ((TreeModel) model).getScalarByName("cdata");
                }
                for (int i = 0; i < model.getInstances().size(); i++) {
                    TreeInstance ti = (TreeInstance) model.getInstances().get(i);
                    if (ti.isValid()) {
                        float[] vector = new float[2];
                        vector[0] = ti.getX();
                        vector[1] = ti.getY();
                        DenseVector row = new DenseVector(vector, ti.getId(), ti.getScalarValue(scalar));
                        projection.addRow(row);
                        labels.add(ti.toString());
                    }
                }
                projection.setLabels(labels);
                ArrayList<String> attributes = new ArrayList<String>();
                attributes.add("x");
                attributes.add("y");
                projection.setAttributes(attributes);
                LabeledProjectionModelComp lcp = new LabeledProjectionModelComp();
                lcp.input(projection);
                lcp.execute();
                LabeledProjectionFrameComp lpf = new LabeledProjectionFrameComp();
                lpf.input(lcp.output());
                lpf.execute();
            } catch (IOException ex) {
                Logger.getLogger(TreeFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    @Override
    protected void sLinkMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        createHCScalar(HierarchicalClusteringType.SLINK);
    }

    @Override
    protected void cLinkMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        createHCScalar(HierarchicalClusteringType.CLINK);
    }

    @Override
    protected void aLinkMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        createHCScalar(HierarchicalClusteringType.ALINK);
    }

    @Override
    protected void saveSamplesButtonActionPerformed(java.awt.event.ActionEvent evt) {

        SaveSampleDialog.getInstance(this,(TreeModel)model,"").display();

    }

    public int setlabelType(int option) {
        ArrayList<AbstractInstance> instances = getModel().getInstances();
        if ((instances != null)&&(!instances.isEmpty())) {
            switch (option) {
                case 0:
//                    for (int i=0;i<instances.size();i++) {
//                        if (instances.get(i) instanceof TreeInstance) {
//                            ((TreeInstance)instances.get(i)).setShowLabel(false);
//                            ((TreeInstance)instances.get(i)).setShowImage(false);
//                            this.showinstancelabel = false;
//                            this.showinstanceImage = false;
//                        }
//                    }
                    if (this.isShowInstanceImage() || this.isShowInstanceLabel()) {
                        for (int i=0;i<instances.size();i++) {
                            ((TreeInstance)instances.get(i)).setShowLabel(false);
                            ((TreeInstance)instances.get(i)).setShowImage(false);
                        }
                        this.setShowInstances(false,false);
                    }
                    return 0;
                case 1:
//                    for (int i=0;i<instances.size();i++) {
//                        if (instances.get(i) instanceof TreeInstance) {
//                            ((TreeInstance)instances.get(i)).setShowLabel(true);
//                            ((TreeInstance)instances.get(i)).setShowImage(false);
//                            this.setShowInstanceLabel(true);
//                        }
//                    }
                    if (!this.isShowInstanceLabel()) {
                        this.setShowInstanceLabel(true);
                        for (int i=0;i<instances.size();i++) {
                            ((TreeInstance)instances.get(i)).setShowLabel(true);
                            ((TreeInstance)instances.get(i)).setShowImage(false);
                        }
                    }
                    return 1;
                case 2:
//                    int oldIndex = 0;
//                    if (isShowInstanceLabel()) oldIndex = 1;
//                    else if (isShowInstanceImage()) oldIndex = 2;
//                    ImageCollection im = ((TreeModel)model).getImageCollection();
//                    if ((im == null)||(!ImageOpenDialog.checkImages(im,model))) {
//                        try {
//                            PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
//                            im = ImageOpenDialog.openImages(spm,new ZIPFilter(),this);
//                            if (im == null) {
//                                //Usuario desistiu de aplicar as imagens, voltar opcao antiga
//                                return oldIndex;
//                            }else {
//                                if (ImageOpenDialog.checkImages(im,model)) {
//                                   ((TreeModel)model).setImageCollection(im);
//                                   ((GraphReportView)(getReportPanel())).setDataSource(im.getFilename());
//                                   this.setShowInstanceImage(true);
//                                   //Populando imagem na instancia...
//                                   for (int i=0;i<instances.size();i++) {
//                                       if (instances.get(i) instanceof TreeInstance) {
//                                           Image image = im.getImage(((TreeInstance)instances.get(i)).toString());
//                                           if (image != null) {
////                                               ((TreeInstance)instances.get(i)).setImage(
////                                                   image.getScaledInstance(((TreeInstance)instances.get(i)).getSize()*5,
////                                                   ((TreeInstance)instances.get(i)).getSize()*5,0));
//                                               ((TreeInstance)instances.get(i)).setImage(image);
//                                               ((TreeInstance)instances.get(i)).setShowLabel(false);
//                                               ((TreeInstance)instances.get(i)).setShowImage(true);
//                                           }
//                                       }
//                                   }
//                               }else {
//                                   JOptionPane.showMessageDialog(this,
//                                        "The image collection file do not correspond to the projected image collection!",
//                                        "Error",
//                                        JOptionPane.ERROR_MESSAGE,null);
//                                   //colecao escolhida nao correspondeo a colecao projetada, voltar opcao antiga
//                                   return oldIndex;
//                               }
//                            }
//                        } catch (IOException ex) {
//                            Logger.getLogger(TreeFrameOptions.class.getName()).log(Level.SEVERE, null, ex);
//                            //voltar opcao antiga
//                            return oldIndex;
//                        }
//                    }else {
//                        for (int i=0;i<instances.size();i++) {
//                            if (instances.get(i) instanceof TreeInstance) {
//                                try {
//                                    Image image = im.getImage(((TreeInstance)instances.get(i)).toString());
//                                    if (image != null) {
////                                        ((TreeInstance)instances.get(i)).setImage(
////                                            image.getScaledInstance(((TreeInstance)instances.get(i)).getSize()*5,
////                                            ((TreeInstance)instances.get(i)).getSize()*5,0));
//                                        ((TreeInstance)instances.get(i)).setImage(image);
//                                        ((TreeInstance)instances.get(i)).setShowLabel(false);
//                                        ((TreeInstance)instances.get(i)).setShowImage(true);
//                                    }
//                                } catch (IOException ex) {
//                                   Logger.getLogger(TreeFrameOptions.class.getName()).log(Level.SEVERE, null, ex);
//                                   //voltar opcao antiga
//                                   return oldIndex;
//                                }
//                            }
//                            this.setShowInstanceImage(true);
//                        }
//                    }
//                    return 2;
                    if (!this.isShowInstanceImage()) {
                        int oldIndex = 0;
                        if (isShowInstanceLabel()) oldIndex = 1;
                        else if (isShowInstanceImage()) oldIndex = 2;
                        ImageCollection im = ((TreeModel)model).getImageCollection();
                        if ((im == null)||(!ProjectionOpenDialog.checkImages(im,model))) {
                            try {
                                PropertiesManager spm = PropertiesManager.getInstance(TreeConstants.PROPFILENAME);
                                im = ProjectionOpenDialog.openImages(spm,new ZIPFilter(),this);
                                if (im == null) {
                                    //Usuario desistiu de aplicar as imagens, voltar opcao antiga
                                    return oldIndex;
                                }else {
                                    if (ProjectionOpenDialog.checkImages(im,model)) {
                                       ((TreeModel)model).setImageCollection(im);
                                       ((LabeledGraphReportView)(getReportPanel())).setDataSource(im.getFilename());
                                       this.setShowInstanceImage(true);
                                       //Populando imagem na instancia...
                                       for (int i=0;i<instances.size();i++) {
                                           ((TreeInstance)instances.get(i)).setShowLabel(false);
                                           ((TreeInstance)instances.get(i)).setShowImage(true);
                                       }
                                   }else {
                                       JOptionPane.showMessageDialog(this,
                                            "The image collection file do not correspond to the projected image collection!",
                                            "Error",
                                            JOptionPane.ERROR_MESSAGE,null);
                                       //colecao escolhida nao correspondeo a colecao projetada, voltar opcao antiga
                                       return oldIndex;
                                   }
                                }
                            } catch (IOException ex) {
                                Logger.getLogger(TreeFrameOptions.class.getName()).log(Level.SEVERE, null, ex);
                                //voltar opcao antiga
                                return oldIndex;
                            }
                        }else {
                            this.setShowInstanceImage(true);
                            for (int i=0;i<instances.size();i++) {
                                ((TreeInstance)instances.get(i)).setShowLabel(false);
                                ((TreeInstance)instances.get(i)).setShowImage(true);
                            }
                        }
                    }
                    return 2;
            }
            view.cleanImage();
            view.repaint();
        }
        return option;
    }

    @Override
    protected void fileSaveActionPerformed(java.awt.event.ActionEvent evt) {
        if (model != null) {
            try {
                PropertiesManager spm = PropertiesManager.getInstance(GraphConstants.PROPFILENAME);
                int result = SaveDialog.showSaveDialog(spm, new XMLFilter(), this, "TreeModel.xml");

                if (result == JFileChooser.APPROVE_OPTION) {
                    String filename = SaveDialog.getFilename();
                    XMLTreeModelWriterComp xmlComp = new XMLTreeModelWriterComp();
                    xmlComp.input((TreeModel) model);
                    xmlComp.setFilename(filename);
                    xmlComp.execute();
                }
            } catch (IOException ex) {
                Logger.getLogger(TreeFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    protected void fileOpenActionPerformed(java.awt.event.ActionEvent evt) {

        try {
            PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
            int result = OpenDialog.showOpenDialog(spm, new XMLFilter(), this);

            if (result == JFileChooser.APPROVE_OPTION) {
                String filename = OpenDialog.getFilename();

                XMLTreeModelReaderComp xmlcomp = new XMLTreeModelReaderComp();
                xmlcomp.setFilename(filename);
                xmlcomp.execute();
                setModel(xmlcomp.output());

            }
        } catch (IOException ex) {
            Logger.getLogger(TreeFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void correlationMenuItemActionPerformed(java.awt.event.ActionEvent evt) {                                                    
        if (model != null) {
            try {
                TreeCorrelationCoefficientView.getInstance(this).display((TreeModel) model);
                //GraphCorrelationCoefficientView.getInstance(this).display((TreeModel) model);
            } catch (IOException ex) {
                Logger.getLogger(TreeFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void createHCScalar(HierarchicalClusteringType hierarchicalClusteringType) {
        if (model != null) {
            try {
                javax.swing.JOptionPane.showMessageDialog(this,
                        "The Hierachical Clustering is a very expensive process." +
                        "\nIt can take several minutes!",
                        "WARNING", javax.swing.JOptionPane.WARNING_MESSAGE);

                float[][] projection = new float[((TreeModel)model).getValidInstances().size()][];
                for (int i = 0; i < ((TreeModel)model).getValidInstances().size(); i++) {
                    projection[i] = new float[2];
                    projection[i][0] = ((TreeInstance)((TreeModel)model).getValidInstances().get(i)).getX();
                    projection[i][1] = ((TreeInstance)((TreeModel)model).getValidInstances().get(i)).getY();
                }

                DenseMatrix dproj = new DenseMatrix();
                for (int i = 0; i < projection.length; i++) {
                    dproj.addRow(new DenseVector(projection[i]));
                }

                HierarchicalClustering hc = new HierarchicalClustering(hierarchicalClusteringType);
                float[] hcScalars = hc.getPointsHeight(dproj, new Euclidean());

                String scalarname = "hc-slink";
                if (hierarchicalClusteringType == HierarchicalClusteringType.ALINK) scalarname = "hc-alink";
                else if (hierarchicalClusteringType == HierarchicalClusteringType.CLINK) scalarname = "hc-clink";

                Scalar scalar = ((TreeModel)model).addScalar(scalarname);

                for (int i=0;i<((TreeModel)model).getValidInstances().size();i++)
                    ((TreeInstance)((TreeModel)model).getValidInstances().get(i)).setScalarValue(scalar, hcScalars[i]);

                this.updateScalars(scalar);
                model.notifyObservers();
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
