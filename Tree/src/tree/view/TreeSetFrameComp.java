package tree.view;

import distance.DetailedDistanceMatrix;
import distance.DistanceMatrix;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import matrix.AbstractMatrix;
import detailedMatrix.dense.DetailedDenseMatrix;
import tree.model.TreeModel;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.View",
name = "Tree Set View Frame",
description = "Display a set of tree models.")
public class TreeSetFrameComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if ((models != null)&&(!models.isEmpty())) {
            TreeSetFrame frame = new TreeSetFrame(models.size());
            frame.setSize(600, 600);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
            frame.setTitle(title);
            //frame.setModel(models.get(i));
            frame.setModels(models);
            if (coordinators != null) {
                for (int k=0;k<coordinators.size();k++) {
                    frame.addCoordinator(coordinators.get(k));
                }
            }
            //Setting report data...
//            ((GraphReportView)frame.getReportPanel()).clean();
//            boolean isText = ((LabeledGraphInstance)models.get(0).getInstances().get(0)).toString().contains(".txt");
//            ImageCollection im = null;
//            Corpus c = null;
//            if (!isText) im = ((TreeModel)models.get(0)).getImageCollection();
//            else c = ((TreeModel)models.get(0)).getCorpus();
//            if (im != null) ((GraphReportView)frame.getReportPanel()).setDataSource(im.getFilename());
//            else if (c != null) ((GraphReportView)frame.getReportPanel()).setDataSource(c.getUrl());
//            ((GraphReportView)frame.getReportPanel()).setObjects(models.get(0).getValidInstances().size());
//
//            ((GraphReportView)frame.getReportPanel()).setType(models.get(0).getType());

            if (originalMatrix != null && originalMatrix instanceof DetailedDenseMatrix) {
                if (originalMatrix.getRowCount() == ((TreeModel)models.get(0)).getValidInstances().size()) {
                    ((TreeSetFrameReportView)frame.getReportPanel()).setSource(((DetailedDenseMatrix)originalMatrix).getSource());
                    ((TreeSetFrameReportView)frame.getReportPanel()).setDimensions(originalMatrix.getDimensions());
                }
            }else if (originalDmat != null && originalDmat instanceof DetailedDistanceMatrix) {
                if (originalDmat.getElementCount() == ((TreeModel)models.get(0)).getValidInstances().size()) {
                    ((TreeSetFrameReportView)frame.getReportPanel()).setSource(((DetailedDistanceMatrix)originalDmat).getSource());
                }
            }
        } else {
            throw new IOException("A tree model set should be provided.");
        }
    }

    public void input(@Param(name = "Original Distance Matrix") DistanceMatrix dmat) {
        this.originalDmat = dmat;
    }

    public void input(@Param(name = "Original Data Matrix") AbstractMatrix matrix) {
        this.originalMatrix = matrix;
    }

    public void input(@Param(name = "tree models set") ArrayList<TreeModel> models) {
        this.models = models;
    }

    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
          if (paramview == null) {
            ///paramview = new TreeFrameParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        models = null;
        coordinators = null;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle () {
        return title;
    }

    public static final long serialVersionUID = 1L;
    private String title = "";    
    private transient ArrayList<TreeModel> models;
    private transient TreeFrameParamView paramview;
    private transient ArrayList<AbstractCoordinator> coordinators;
    private transient AbstractMatrix originalMatrix;
    private transient DistanceMatrix originalDmat;
}
