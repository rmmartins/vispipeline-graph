/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.model.xml;

import java.io.IOException;
import tree.model.TreeModel;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.Input",
name = "Tree model XML reader",
description = "Read a tree model from an XML file.")
public class XMLTreeModelReaderComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (filename.trim().length() > 0) {
            XMLTreeModelReader xmr = new XMLTreeModelReader();

            model = new TreeModel();
            xmr.read(model, filename);
        } else {
            throw new IOException("A tree file name must be provided to write.");
        }
    }

    public TreeModel output() {
        return model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new XMLTreeModelReaderParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        model = null;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public static final long serialVersionUID = 1L;
    protected String filename = "";
    protected transient TreeModel model;
    protected transient XMLTreeModelReaderParamView paramview;
}
