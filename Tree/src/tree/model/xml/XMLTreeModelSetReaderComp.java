/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.model.xml;

import java.io.IOException;
import java.util.ArrayList;
import tree.model.TreeModel;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.Input",
name = "Tree Set Reader",
description = "Read a set of tree models.")
public class XMLTreeModelSetReaderComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (filenames.trim().length() > 0) {
            XMLTreeModelReaderComp xmc = new XMLTreeModelReaderComp();
            String[] files = filenames.split(";");
            if (files.length > 0) {
                for (int i=0;i<files.length;i++) {
                    xmc.setFilename(files[i]);
                    xmc.execute();
                    models.add(xmc.output());
                }
            }
        } else {
            throw new IOException("A tree set file name must be provided to write.");
        }
    }

    public ArrayList<TreeModel> output() {
        return models;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new XMLTreeModelSetReaderParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        models = new ArrayList<TreeModel>();
    }

    /**
     * @return the filename
     */
    public String getFilesname() {
        return filenames;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilesname(String filename) {
        this.filenames = filename;
    }

    public static final long serialVersionUID = 1L;
    protected String filenames = "";
    protected transient ArrayList<TreeModel> models;
    protected transient XMLTreeModelSetReaderParamView paramview;
}
