/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tree.model.xml;

import graph.model.Connectivity;
import graph.model.Edge;
import labeledgraph.model.xml.XMLLabeledGraphModelReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import projection.model.Scalar;
import tree.model.TreeConnectivity;
import tree.model.TreeInstance;
import tree.model.TreeModel;
import visualizationbasics.util.Util;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class XMLTreeModelReader extends XMLLabeledGraphModelReader {

    public void read(TreeModel model, String filename) throws IOException {
        this.model = model;
        this.model.setSource(filename.substring(filename.lastIndexOf("\\")+1,filename.lastIndexOf(".")));
        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {
            InputSource in = new InputSource(new InputStreamReader(new FileInputStream(filename), "ISO-8859-1"));
            SAXParser sp = spf.newSAXParser();
            sp.parse(in, this);
        } catch (SAXException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        
        if (qName.equalsIgnoreCase(SCALAR)) {
            String name = attributes.getValue(NAME);
            String value = attributes.getValue(VALUE);

            if (name != null && value != null) {
                Scalar s = model.addScalar(name);
                tmpinstance.setScalarValue(s, Float.parseFloat(value));
            }
        } else if (qName.equalsIgnoreCase(INSTANCE)) {
            String id = attributes.getValue(ID);
            if (Util.isParsableToInt(id)) {
                tmpinstance = new TreeInstance(model, Integer.parseInt(id));
            } else {
                tmpinstance = new TreeInstance(model, Util.convertToInt(id));
            }
        } else if (qName.equalsIgnoreCase(X_COORDINATE)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                tmpinstance.setX(Float.parseFloat(value));
            }
        } else if (qName.equalsIgnoreCase(Y_COORDINATE)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                tmpinstance.setY(Float.parseFloat(value));
            }
        } else if (qName.equalsIgnoreCase(LABEL)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                tmpinstance.setLabel(value);
            }
        } else if (qName.equalsIgnoreCase(ROOT)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                //((TreeModel)model).setRoot(Integer.parseInt(value));
                model.setRoot(Integer.parseInt(value));
            }
        } else if (qName.equalsIgnoreCase(TYPE)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                model.setType(value);
            }
        } else if (qName.equalsIgnoreCase(NOALPHA)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                model.setNoAlpha(Boolean.parseBoolean(value));
            }
        } else if (qName.equalsIgnoreCase(ALPHA)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                model.setAlpha(Float.parseFloat(value));
            }
        } else if (qName.equalsIgnoreCase(EDGES)) {
            edges = new ArrayList<Edge>();
            con = new TreeConnectivity(attributes.getValue(NAME),edges);
            model.addConnectivity(con);
            if ((selconnValue != null)&&(con.getName().equalsIgnoreCase(selconnValue)))
               model.setSelectedConnectivity(con);
        } else if (qName.equalsIgnoreCase(EDGE)) {
            int v1 = Integer.parseInt(attributes.getValue(SOURCE));
            int v2 = Integer.parseInt(attributes.getValue(TARGET));
            float length = Edge.NO_SIZE;
            String aux_len = attributes.getValue(LENGTH);
            if (aux_len != null) {
                length = Float.parseFloat(aux_len);
            }
            Edge ed = new Edge(v1,v2,length);
            edges.add(ed);
            //Adjusting children...
            TreeInstance parent = model.getInstanceById(v1);
            TreeInstance son = model.getInstanceById(v2);
            if ((parent != null)&&(son != null)) {
                if (parent.getChildren() == null) parent.setChildren(new ArrayList<TreeInstance>());
                parent.getChildren().add(son);
            }
        }  else if (qName.equalsIgnoreCase(VALID)) {
            String value = attributes.getValue(VALUE);
            if (value != null) {
                //tmpinstance.setValid(Boolean.getBoolean(value));
                tmpinstance.setValid(Boolean.parseBoolean(value));
            }
        }  else if (qName.equalsIgnoreCase(SELCONN)) {
            selconnValue = attributes.getValue(VALUE);
            if (selconnValue != null) {
                if (model.getConnectivities() != null) {
                    for (int i=0;i<model.getConnectivities().size();i++) {
                        if (model.getConnectivities().get(i).getName().equalsIgnoreCase(selconnValue))
                            model.setSelectedConnectivity(model.getConnectivities().get(i));
                    }
                }
            }
        }
    }

    private Connectivity con;
    ArrayList<Edge> edges;
    private TreeInstance tmpinstance;
    private TreeModel model;
    private String selconnValue;
    private static final String TREE = "tree";
    private static final String LABEL = "label";
    private static final String EDGE = "edge";
    private static final String SELCONN = "selconn";
    private static final String COLLECTION = "collection";
    private static final String ROOT = "root";
    private static final String TYPE = "type";
    private static final String NOALPHA = "noalpha";
    private static final String ALPHA = "alpha";
    private static final String EDGES = "edges";
    private static final String SOURCE = "source";
    private static final String TARGET = "target";
    private static final String LENGTH = "length";
    private static final String VALID = "valid";
    
}
