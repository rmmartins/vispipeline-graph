/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.model.xml;

import graph.model.Connectivity;
import graph.model.Edge;
import labeledgraph.model.xml.XMLLabeledGraphModelWriter;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import projection.model.Scalar;
import tree.model.TreeInstance;
import tree.model.TreeModel;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author Jose Gustavo
 */
public class XMLTreeModelWriter extends XMLLabeledGraphModelWriter {

    public void write(TreeModel model, String filename) throws IOException {
        BufferedWriter out = null;

        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "ISO-8859-1"));
            //writting the header
            out.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\r\n");
            
            //out.write("<tree type=\""+model.getClass().getSimpleName()+"\">\r\n");
            out.write("<tree>\r\n");

            if (model.getImageCollection() != null) {
                out.write("<collection value=\"");
                out.write(model.getImageCollection().getFilename());
                out.write("\"/>\r\n");
            }

            out.write("<root value=\"");
            out.write(Integer.toString(model.getRoot()));
            out.write("\"/>\r\n");

            out.write("<type value=\"");
            out.write(model.getType());
            out.write("\"/>\r\n");

            out.write("<noalpha value=\"");
            out.write(Boolean.toString(model.isNoAlpha()));
            out.write("\"/>\r\n");

            out.write("<alpha value=\"");
            out.write(Float.toString(model.getAlpha()));
            out.write("\"/>\r\n");
            
            //writting the instances
            out.write("<!--   instances   -->\r\n");

            ArrayList<AbstractInstance> instances = model.getInstances();

            for (AbstractInstance ai : instances) {
                TreeInstance pi = (TreeInstance) ai;

                out.write("<instance id=\"");
                out.write(Integer.toString(pi.getId()));
                out.write("\">\r\n");

                out.write("<valid value=\"");
                out.write(Boolean.toString(pi.isValid()));
                out.write("\"/>\r\n");

                out.write("<x-coordinate value=\"");
                out.write(Float.toString(pi.getX()));
                out.write("\"/>\r\n");

                out.write("<y-coordinate value=\"");
                out.write(Float.toString(pi.getY()));
                out.write("\"/>\r\n");

                out.write("<scalars>\r\n");
                for (Scalar s : model.getScalars()) {
                    out.write("<scalar name=\"");
                    out.write(s.getName().replaceAll("\"", "'"));
                    out.write("\" value=\"");
                    out.write(Float.toString(pi.getScalarValue(s)));
                    out.write("\"/>\r\n");
                }
                out.write("</scalars>\r\n");

                out.write("<label value=\"");
                out.write(pi.getLabel());
                out.write("\"/>\r\n");

                out.write("</instance>\r\n");
            }

            out.write("<selconn value=\"");
            out.write(model.getSelectedConnectivity().getName());
            out.write("\"/>\r\n");

            //writting the edges
            out.write("<!--   edges   -->\r\n");
            ArrayList<Connectivity> cons = model.getConnectivities();
            for (int i=0;i<cons.size();i++) {
                out.write("<edges name=\"");
                out.write(cons.get(i).getName());
                out.write("\">\r\n");
                for (Edge e : cons.get(i).getEdges()) {
                    out.write("<edge source=\"");
                    out.write(Integer.toString(e.getSource()));
                    out.write("\" target=\"");
                    out.write(Long.toString(e.getTarget()));
                    out.write("\" length=\"");
                    out.write(Float.toString(e.getWeight()));
                    out.write("\"/>\r\n");
                }
                out.write("</edges>\r\n");
            }
            out.write("</tree>\r\n");

        } catch (FileNotFoundException e) {
            throw new IOException("File \"" + filename + "\" was not found!");
        } catch (IOException e) {
            throw new IOException("Problems reading the file \"" + filename + "\"");
        } finally { 
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    Logger.getLogger(XMLTreeModelWriter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
