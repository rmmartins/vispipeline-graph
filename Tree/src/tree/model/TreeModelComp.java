/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.model;

import java.io.IOException;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import tree.basics.Tree;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent; 

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.Basics",
name = "Tree Model",
description = "Create a tree model to be visualized.")
public class TreeModelComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if ((placement != null)||(tree != null)) {
            model = new TreeModel();
            if (tree != null) { //Busco a hierarquia a partir da arvore...
                //tree.printEdges();
                model = new TreeModel(tree,collection);
            }else { //Como nao foi passada a tree, vou monta-la a partir dos pontos em placement
                model = new TreeModel(placement, conns, collection);
            }
            model.setSource(source);
            //model.print();
        } else {
            throw new IOException("A 2D position and a tree structure should be provided.");
        }
    }

    public void input(@Param(name = "Tree Data") Tree tree) {
        this.tree = tree;
    }

    public TreeModel output() {
        return model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new TreeModelParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        placement = null;
        model = null;
        conns = null;
        tree = null;
    }

    public void setCollection(String c) {
        this.collection = c;
    }

    public String getCollection() {
        return collection;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String s) {
        this.source = s;
    }

    public static final long serialVersionUID = 1L;
    private String collection;
    private transient TreeModelParamView paramview;
    private transient TreeModel model;
    private transient AbstractMatrix placement;
    private transient ArrayList<TreeConnectivity> conns;
    private transient Tree tree;
    private String source;
}
