package tree.model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import labeledgraph.model.LabeledGraphInstance;
import hypergraph.hyperbolic.Complex;
import java.awt.BasicStroke;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import labeledprojection.util.ImageCollection;
import labeledprojection.util.ProjectionOpenDialog;
import projection.model.Scalar;
import projection.util.ProjectionConstants;
import textprocessing.util.filter.ZIPFilter;
import visualizationbasics.util.PropertiesManager;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class TreeInstance extends LabeledGraphInstance {

//    public TreeInstance getChild(ArrayList<TreeInstance> children, int id) {
//        for (int i=0;i<children.size();i++)
//            if (children.get(id).getId() == id) return children.get(id);
//        return null;
//    }

    public TreeInstance createClone(TreeModel model) {
                
        Scalar selscalar = model.getSelectedScalar();
        TreeInstance ins = new TreeInstance(model,id,label,isValid());

        ins.setColor(((TreeInstance)this).getColor());
        ins.setParent(((TreeInstance)this).getParent());
        ins.setScalarValue(selscalar,((TreeInstance)this).getScalarValue(selscalar));
        ins.setSelected(((TreeInstance)this).isSelected());
        ins.setShowLabel(((TreeInstance)this).isShowLabel());
        ins.setShowImage(((TreeInstance)this).isShowImage());
        ins.setSizeFactor(((TreeInstance)this).getSizeFactor());
        ins.setX(((TreeInstance)this).getX());
        ins.setY(((TreeInstance)this).getY());
        ins.setDrawAs(((TreeInstance)this).getDrawAs());
        if (ins.getDrawAs() == 2)//Load associated image...
            ins.setImage(((TreeInstance)this).getImage());
            //populateInstanceImage(model.getImageCollection());
        ins.setImageSize(((TreeInstance)this).getImageSize());
        ins.fdata = this.fdata;
        ins.scalars = new ArrayList<Float>();
        for (int i=0;i<this.scalars.size();i++) {
            ins.scalars.add(this.scalars.get(i));
        }

        ins.children = new ArrayList<TreeInstance>();
        //Os filhos nao sao gerados aqui, pois podem nao existir ainda no momento da criacao deste clone. No momento, os filhos devem ser setados
        //separadamente, analisar alterar isso depois.
//        for (int i=0;i<this.children.size();i++) {
//            TreeInstance t = getChild(ins.children,this.children.get(i).getId());
//            if (t == null)
//                ins.children.add(this.children.get(i).createClone(model));
//            else ins.children.add(ins);
//        }

        return ins;

    }

    public TreeInstance(TreeModel model, int id, String label, float x, float y, boolean valid) {
        super(model, label, id, x, y);
        setValid(valid);
        this.children = new ArrayList<TreeInstance>();
        this.drawAs = 1;
    }

    public TreeInstance(TreeModel model, int id, String label, boolean valid) {
        this(model, id, label, 0.0f, 0.0f, valid);
    }

    public TreeInstance(TreeModel model, int id) {
        this(model, id, "", 0.0f, 0.0f, false);
    }

    @Override
    public String toString() {
        if (isValid()) return label;
        return "";
    }

    @Override
    public void draw(BufferedImage image, boolean highquality) {
        Graphics2D g2 = (Graphics2D) image.getGraphics();
        TreeModel tmodel = (TreeModel) model;
        if (highquality) g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int xaux = (((int) this.x) <= 0) ? 1 : (((int) this.x) < image.getWidth()) ? (int) this.x : image.getWidth() - 1;
        int yaux = (((int) this.y) <= 0) ? 1 : (((int) this.y) < image.getHeight()) ? (int) this.y : image.getHeight() - 1;
        int inssize = tmodel.getInstanceSize();
        float alpha = tmodel.getAlpha();
        if ((tmodel.getSelectedInstances() == null)||(tmodel.getSelectedInstances().isEmpty())||(tmodel.isNoAlpha())) alpha = 1.0f;
        switch (drawAs) {
            case 0:
                int rgb = color.getRGB();
                inssize /= 2;
                if ((selected)||(!isValid())) {
                    alpha = 1.0f;
                    g2.setColor(Color.BLACK);
                    if (!isValid()) {
                        inssize /= 2;
                        g2.setColor(Color.GRAY);
                        rgb = Color.WHITE.getRGB();
                    }
                    g2.drawRect(xaux-inssize-1,yaux-inssize-1,inssize*2+2,inssize*2+2);
                }
                for (int i=-inssize;i<=inssize;i++)
                    for (int j=-inssize;j<=inssize;j++)
                        simulateAlpha(image,alpha,xaux-i,yaux-j,rgb);
                break;
            case 1:
               if ((selected)||(!isValid())) {
                    alpha = 1.0f;
                    //if (valid) g2.setStroke(new BasicStroke(1.5f));
                }
                if (isValid()) {
                    g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, alpha));
                    g2.setColor(color);
                    g2.fillOval(xaux-inssize,yaux-inssize,inssize*2,inssize*2);
                    g2.setColor(Color.BLACK);
                    g2.drawOval(xaux-inssize,yaux-inssize,inssize*2,inssize*2);
                }else {
                    inssize /= 2;
                    g2.setColor(Color.WHITE);
                    g2.fillOval(xaux-inssize,yaux-inssize,inssize*2,inssize*2);
                    g2.setColor(Color.GRAY);
                    g2.drawOval(xaux-inssize,yaux-inssize,inssize*2,inssize*2);
                }
                g2.setStroke(new BasicStroke(1.0f));
                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                break;
            case 2:
                if (this.image != null) {
                    Image img = this.image.getScaledInstance(this.getImageSize()*5,this.getImageSize()*5,0);
                    //Image img = this.image.getScaledInstance(this.getSize()*5,this.getSize()*5,0);
                    int w = img.getWidth(null);
                    int h = img.getHeight(null);
                    g2.drawImage(img,xaux-(w/2),yaux-(h/2),null);
                    //Desenhando o contorno colorido para mostrar a classe...
                    if (drawFrame) {
                        g2.setStroke(new BasicStroke(3.0f));
                        g2.setColor(this.getColor());
                        g2.drawRect(xaux-(w/2)-2,yaux-(h/2)-2,w+3,h+3);
                        g2.setStroke(new BasicStroke(1.0f));
                    }
                    if (selected) {
                        g2.setStroke(new BasicStroke(2.0f));
                        g2.setColor(Color.RED);
                        g2.drawRect(((int) this.x) - w / 2, ((int) this.y) - h / 2, w, h);
                        g2.setStroke(new BasicStroke(1.0f));
                    }
                }
                break;
        }
        //show the label associated to this instance
        if (showlabel) {
            //if (valid) {
//                java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());
//                int width = metrics.stringWidth(toString().trim());
//                int height = metrics.getAscent();
//                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
//                g2.setPaint(java.awt.Color.WHITE);
//                g2.fillRect(((int) x) + 3, ((int) y) - 1 - height, width + 4, height + 4);
//                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
//                g2.setColor(java.awt.Color.DARK_GRAY);
//                g2.drawRect(((int) x) + 3, ((int) y) - 1 - height, width + 4, height + 4);
//                g2.drawString(toString().trim(), ((int) x) + 3, ((int) y));
            //}
        }
    }

    public TreeInstance getParent() {
        return parent;
    }
    
    public void setParent(TreeInstance parent) {
        this.parent = parent;
    }

    public ArrayList<TreeInstance> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<TreeInstance> children) {
        this.children = children;
    }

    public boolean isRelative(TreeInstance node) {

        TreeInstance son;
        boolean result = false;
        ArrayList<TreeInstance> childrenSet = node.getChildren();
        if (childrenSet != null) {
            for (int i=0;i<childrenSet.size();i++) {
                son = childrenSet.get(i);
                if (son.equals(node)) return true;
                else result = isRelative(son);
            }
        }
        return result;
    }

//    public boolean isValid() {
//        return valid;
//    }
//
//    public void setValid(boolean v) {
//        valid = v;
//    }

    public void setPositionHyperbolic(Complex c) {
        this.positionHyperbolic = c;
    }

    public Complex getPositionHyperbolic() {
        return this.positionHyperbolic;
    }

//    public float getCdata() {
//        return cdata;
//    }
//
//    public void setCdata(float cdata) {
//        this.cdata = cdata;
//    }

    //private String label;
    private TreeInstance parent;
    private ArrayList<TreeInstance> children;
    //private boolean valid;
    private Complex positionHyperbolic;
    private int deepChildrenCount; // Count the total number of children (including the whole tree hierarchy)
    private int treeLevel = - 1;
    
    private void populateInstanceImage(ImageCollection im) {

        if ((im == null)||(!ProjectionOpenDialog.checkImages(im,this.getModel()))) {
           try {
               PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
               im = ProjectionOpenDialog.openImages(spm,new ZIPFilter(), null);
               if (im != null) {
                   if (ProjectionOpenDialog.checkImages(im,this.getModel())) {
                       ((TreeModel)this.getModel()).setImageCollection(im);
                       //Populando imagens nas instancias...
                       Image img = getImage(im);
                       if (img != null) {
                           setImage(img.getScaledInstance(getImageSize()*5,getImageSize()*5, 0));
                       }
                   }else {
                       JOptionPane.showMessageDialog(null,
                                            "The image collection file do not correspond to the projected image collection!",
                                            "Error",
                                            JOptionPane.ERROR_MESSAGE,null);
                   }
               }
           } catch (IOException ex) {
               Logger.getLogger(TreeInstance.class.getName()).log(Level.SEVERE, null, ex);
           }
        }else {
            try {
                Image img = getImage(im);
                if (img != null) {
                    setImage(img.getScaledInstance(getImageSize()*5,getImageSize()*5, 0));
                }
            } catch (IOException ex) {
                Logger.getLogger(TreeInstance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    //private float cdata;

    /**
     * @return the deepChildrenCount
     */
    public int getDeepChildrenCount() {
        return deepChildrenCount;
    }

    /**
     * @param deepChildrenCount the deepChildrenCount to set
     */
    public void setDeepChildrenCount(int deepChildrenCount) {
        this.deepChildrenCount = deepChildrenCount;
    }

    /**
     * @return the treeLevel
     */
    public int getTreeLevel() {
        return treeLevel;
    }

    /**
     * @param treeLevel the treeLevel to set
     */
    public void setTreeLevel(int treeLevel) {
        this.treeLevel = treeLevel;
    }

}
