package tree.model;

import graph.model.Connectivity;
import graph.model.Edge;
import labeledgraph.model.LabeledGraphModel;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import labeledprojection.util.ImageCollection;
import labeledprojection.util.PseudoClass;
import labeledprojection.util.TablePScalar;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import projection.model.Scalar;
import projection.util.ProjectionConstants;
import textprocessing.corpus.Corpus;
import textprocessing.corpus.CorpusFactory;
import tree.basics.ContentTree;
import tree.basics.Tree;
import tree.basics.TreeConstants;
import visualizationbasics.color.ColorTable;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class TreeModel extends LabeledGraphModel {

    public TreeModel() {
        super();
        alpha = 0.2f;
        noAlpha = false;
    }

    public TreeModel(Tree tree, String collection) {
        this();
        Scalar cdata = addScalar(ProjectionConstants.CDATA);
        Scalar dots = addScalar(ProjectionConstants.DOTS);
        HashMap ids = new HashMap();
        if (tree != null) { //Busco a hierarquia a partir da arvore...
            for (int i = 0; i < tree.getSize(); i++) {
                ContentTree node = tree.getNode(i);
                if (node != null) {
                    TreeInstance ti = new TreeInstance(this,node.getId(),node.getLabel(),0.0f,0.0f,node.isValid());
                    ti.setDeepChildrenCount(node.getDeepChildCount());
                    ti.setTreeLevel(node.getNivelNo());
                    ids.put(ti.getId(),getInstances().indexOf(ti));
                    ti.setScalarValue(cdata,node.getCdata());
                    ti.setScalarValue(dots,0.0f);
                }
            }
        }

        //generating the hierarchy (2 children!)
        if (ids.size() > 0) {
            for (int i=0;i<instances.size();i++) {
                ContentTree node = tree.getNodeById(instances.get(i).getId());
                if (node != null) {
                    ContentTree node2 = null;
                    TreeInstance node3 = null;
                    //setting the parent
                    node2 = node.getParent();
                    if (node2 != null) {
                        Integer id = (Integer)ids.get(node2.getId());
                        if (id != null) {
                            node3 = (TreeInstance)instances.get(id);
                            if (node3 != null) {
                                ((TreeInstance)instances.get(i)).setParent(node3);
                            }
                        }
                    }

                    //setting the children
                    ArrayList<TreeInstance> linstances = new ArrayList<TreeInstance>();
                    if (node.getChildren() != null) {
                        for (int j=0;j<node.getChildren().size();j++) {
                            node2 = node.getChildren().get(j);
                            if (node2 != null) {
                                Integer id = (Integer)ids.get(node2.getId());
                                if (id != null) {
                                    node3 = (TreeInstance)instances.get(id);
                                    if (node3 != null) {
                                        linstances.add(node3);
                                    }
                                }
                            }
                        }
                    }
                    ((TreeInstance)instances.get(i)).setChildren(linstances);
                }
            }
        }

        root = tree.getRootId();

        //adding the connectivities
        TreeConnectivity dotsCon = new TreeConnectivity(ProjectionConstants.DOTS, new ArrayList<Edge>());
        addConnectivity(dotsCon);

        ArrayList<Edge> edges = tree.getEdges();
        if (edges != null) {
            TreeConnectivity nj = new TreeConnectivity(TreeConstants.NJ,edges);
            addConnectivity(nj);
        }

        if (getConnectivities().size() > 1) {
            setSelectedConnectivity(getConnectivities().get(1));
        }

        //setting the collection...
        if ((collection != null)&&(!collection.isEmpty())) {
            if (instances != null) {
                if (((TreeInstance)instances.get(0)).toString().endsWith(".txt")) {
                    Corpus c = CorpusFactory.getInstance(collection,1); //Este 1 esta fixo, estudar parametrizacao (ngrams)
                    corpus = c;
                } else {
                    ImageCollection im = new ImageCollection(collection);
                    imageCollection = im;
                }
            }
        }

        type = tree.getType();
        if (type == null) type = TreeConstants.NJ;

    }

    public TreeModel(AbstractMatrix placement, ArrayList<TreeConnectivity> conns, String collection) {
        this();
        Scalar cdata = addScalar(ProjectionConstants.CDATA);
        Scalar dots = addScalar(ProjectionConstants.DOTS);
        HashMap ids = new HashMap();
        int nrows = placement.getRowCount();
        for (int i = 0; i < nrows; i++) {
            AbstractVector row = placement.getRow(i);
            String label = "";
            TreeInstance pi = new TreeInstance(this, row.getId(), label, row.getValue(0), row.getValue(1), true);
            ids.put(pi.getId(),instances.indexOf(pi));
            pi.setScalarValue(cdata, row.getKlass());
            pi.setScalarValue(dots, 0.0f);
        }

        //adding the connectivities
        TreeConnectivity dotsCon = new TreeConnectivity(ProjectionConstants.DOTS, new ArrayList<Edge>());
        addConnectivity(dotsCon);

        if (conns != null) {
            for (int i = 0; i < conns.size(); i++) {
                addConnectivity(conns.get(i));
            }
        }

        if (getConnectivities().size() > 1) {
            setSelectedConnectivity(getConnectivities().get(1));
        }

        //setting the collection...
        if ((collection != null)&&(!collection.isEmpty())) {
            if (instances != null) {
                if (((TreeInstance)instances.get(0)).toString().endsWith(".txt")) {
                    Corpus c = CorpusFactory.getInstance(collection,1); //Este 1 esta fixo, estudar parametrizacao (ngrams)
                    corpus = c;
                } else {
                    ImageCollection im = new ImageCollection(collection);
                    imageCollection = im;
                }
            }
        }
        if (getSelectedConnectivity() != null) type = getSelectedConnectivity().getName();
        else type = TreeConstants.NJ;
    }

    public TreeInstance getInstance(ArrayList<AbstractInstance> instances, int id) {
        for (int i=0;i<instances.size();i++)
            if (instances.get(i).getId() == id) return (TreeInstance)instances.get(i);
        return null;
    }

    @Override
    public TreeModel clone() {

        TreeModel newModel = new TreeModel();

        newModel.instances = new ArrayList<AbstractInstance>();
        for (int i=0;i<this.instances.size();i++) {
            ((TreeInstance)this.instances.get(i)).createClone(newModel);
        }

        for (int i=0;i<newModel.instances.size();i++) {
            TreeInstance t = getInstance(this.instances,newModel.instances.get(i).getId());
            if (t != null) {
                for (int j=0;j<t.getChildren().size();j++) {
                    TreeInstance child = getInstance(newModel.instances,t.getChildren().get(j).getId());
                    if (child != null) {
                        ((TreeInstance)newModel.instances.get(i)).getChildren().add(child);
                    }
                }
            }
        }

        newModel.connectivities = new ArrayList<Connectivity>();
        TreeConnectivity conn;
        ArrayList<Edge> edges;

        for (int i=0;i<this.connectivities.size();i++) {
            edges = new ArrayList<Edge>();
            for (int j=0;j<this.connectivities.get(i).getEdges().size();j++) {
                edges.add(new Edge(this.connectivities.get(i).getEdges().get(j).getSource(),
                                   this.connectivities.get(i).getEdges().get(j).getTarget()));
            }
            conn = new TreeConnectivity(this.connectivities.get(i).getName(), edges);
            newModel.connectivities.add(conn);
        }

        newModel.scalars = new ArrayList<Scalar>();
        for (int i=0;i<this.scalars.size();i++) {
            Scalar scalar = new Scalar(this.scalars.get(i).getName());
            scalar.store(this.scalars.get(i).getMin());
            scalar.store(this.scalars.get(i).getMax());
            newModel.scalars.add(scalar);
        }

        int i = 0;
        if (this.selsconn != null) {
           i = this.connectivities.indexOf(this.selsconn);
           if (i != -1)
               newModel.selsconn = (TreeConnectivity)newModel.connectivities.get(i);
        }else newModel.selsconn = null;

        newModel.setInstanceSize(this.getInstanceSize());

        newModel.colortable = new ColorTable(this.colortable.getColorScaleType());

        newModel.alpha = this.alpha;
        newModel.root = this.root;
        newModel.type = this.type;

        newModel.source = this.source;
        newModel.setDrawAs(this.getDrawAs());
        newModel.selscalar = this.selscalar;

        newModel.imageCollection = this.imageCollection;
        newModel.corpus = this.corpus;
        newModel.disstype = this.disstype;
        newModel.setInstanceSize(this.getInstanceSize());

        return newModel;

    }

    private void drawMultiScale(BufferedImage image, boolean highquality) {
        
        //Drawing the edges, according to the selected connectivity...
        if (selsconn != null) {
            ((TreeConnectivity)selsconn).draw(this, image, highquality, multiScaleVisibleNodes);
        }
        
        //first draw the non-valid instances
        for (int i = 0; i < multiScaleVisibleNodes.size(); i++) {
            TreeInstance gi = (TreeInstance) multiScaleVisibleNodes.get(i);
            if (!gi.isValid()) {
                gi.draw(image, highquality);
            }
        }
      
        //then draw the non-selected instances
        for (int i = 0; i < multiScaleBorderNodes.size(); i++) {
            TreeInstance gi = (TreeInstance) multiScaleBorderNodes.get(i);
            //if (gi.isValid() && !gi.isSelected()) {
                gi.draw(image, highquality);
            //}
        }
        
        //finally, the selected instances
        for (int i = 0; i < multiScaleVisibleNodes.size(); i++) {
            TreeInstance gi = (TreeInstance) multiScaleVisibleNodes.get(i);
            if (gi.isValid() && gi.isSelected()) {
                gi.draw(image, highquality);
            }
        }        
    }
    
    @Override
    public void draw(BufferedImage image, boolean highquality) {
        if (image == null)
            return;
        
        if (isMultiScaleDraw()) {
            drawMultiScale(image, highquality);
        }
        else {    

            //Drawing the edges, according to the selected connectivity...
            if (selsconn != null) {
                ((TreeConnectivity)selsconn).draw(this, image, highquality);
            }

            //first draw the non-valid instances
            for (int i = 0; i < instances.size(); i++) {
                TreeInstance gi = (TreeInstance) instances.get(i);
                if (!gi.isValid()) {
                    gi.draw(image, highquality);
                }
            }

            //first draw the non-selected instances
            for (int i = 0; i < instances.size(); i++) {
                TreeInstance gi = (TreeInstance) instances.get(i);
                if (gi.isValid() && !gi.isSelected()) {
                    gi.draw(image, highquality);
                }
            }

            //then the selected instances
            for (int i = 0; i < instances.size(); i++) {
                TreeInstance gi = (TreeInstance) instances.get(i);
                if (gi.isValid() && gi.isSelected()) {
                    gi.draw(image, highquality);
                }
            }
            
        }
    }

    @Override
    public TreeConnectivity getSelectedConnectivity() {
        return (TreeConnectivity)this.selsconn;
    }

    public void setSelectedInstance(TreeInstance selinst) {
        if (selinst != null) {
            cleanSelectedInstances();
            if (selinst.getModel() == this) {
                selinst.setSelected(true);
                selinstances.add(selinst);
                if (selinst.getChildren() != null) {
                    for (int i=0;i<selinst.getChildren().size();i++) {
                        setSelectedInstance(selinst.getChildren().get(i));
                    }
                }
            }
            setChanged();
        }
    }

    public void setInstances(ArrayList<AbstractInstance> instances) {
        this.instances = instances;
    }

    public void setScalars(ArrayList<Scalar> scalars) {
        this.scalars = scalars;
    }

    /***Add by Laura Florian***/
    public Scalar getScalarByName(String name) {
        for (Scalar s : this.scalars)
            if (s.getName().equals(name)) return s;
        return null;
    }

    public void setTablePScalar(TablePScalar table) {
        this.tablePScalar = table;
    }

    public TablePScalar getTablePScalar() {
        return this.tablePScalar;
    }

    public void updateTablePScalar(TablePScalar tablemodel) {
        // tiene todas las clases con sus colores de todos los vertices
        String filename;
        int indexTablePScalar, indexTableModel;
        Float scalarValue = null;
        for (int j=0;j<instances.size();j++) {
            TreeInstance tins = (TreeInstance)instances.get(j);
            if (tablemodel==null) indexTableModel=-1;
            else indexTableModel= tablemodel.getNumRowGiveName(tins.getLabel());
            if(indexTableModel >= 0) {
                filename = (String)tablemodel.getValueAt(indexTableModel,1);
                scalarValue = Float.valueOf(tablemodel.getValueAt(indexTableModel,3).toString().trim()).floatValue();
                //actualizar tablepc
                indexTablePScalar = tablePScalar.getNumRowGiveName(tins.getLabel());
                if(indexTablePScalar >= 0) {
                    this.tablePScalar.setValueAt(filename,indexTablePScalar,1);
                    this.tablePScalar.setValueAt(scalarValue,indexTablePScalar,3);
                }
            }
        }
    }

    public void updateTablePScalar(Scalar s) {
        // tiene todas las clases con sus colores de todos los vertices
        String label;
        int indexTablePScalar;
        PseudoClass pseudoClass = new PseudoClass();
        ArrayList<PseudoClass> pClasses = new ArrayList<PseudoClass>();
        Float scalarValue= new Float(0);
        for (int j=0;j<instances.size();j++) {
            TreeInstance v = (TreeInstance)instances.get(j);
            label = v.getLabel();
            if(this.tablePScalar == null) indexTablePScalar = -1;
            else indexTablePScalar= this.tablePScalar.getNumRowGiveName(label);

            if(indexTablePScalar < 0) {
                scalarValue = v.getScalarValue(s);
                label = "class"+  scalarValue.intValue();
            } else{
                label = (String)this.tablePScalar.getValueAt(indexTablePScalar, 1);
                scalarValue = Float.valueOf(this.tablePScalar.getValueAt(indexTablePScalar,3).toString().trim()).floatValue();
            }
            pseudoClass = new PseudoClass(v.getId(),v.getLabel(),label,true,scalarValue);
            pClasses.add(pseudoClass);
        }
        try {
            this.tablePScalar = new TablePScalar(pClasses,null);
        }catch(Exception ex) {
            System.err.println("Caught: " + ex);
        }
    }

    protected TablePScalar tablePScalar;

    /***by Laura Florian***/

    public void setColortable(ColorTable colorTable) {
        this.colortable = colorTable;
    }

    public void setConnectivities(ArrayList<Connectivity> conns) {
        this.connectivities = conns;
    }
    
    @Override
    public TreeInstance getInstanceById(int id) {
        for (int i=0;i<this.instances.size();i++) {
            if (this.instances.get(i).getId() == id) {
                return (TreeInstance)this.instances.get(i);
            }
        }
        return null;
    }

    public int getRoot() {
        return root;
    }

    public void setRoot(int root) {
        this.root = root;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isNoAlpha() {
        return noAlpha;
    }

    public void setNoAlpha(boolean noAlpha) {
        this.noAlpha = noAlpha;
    }

    @Override
    public String toString() {
        if (source != null) {
            if (source.isEmpty()) return "Tree Model";
            else return source;
        }else return "Tree Model";
    }

//    public void print() {
//
//        System.out.println("-- INSTANCES --");
//
//        for (int i=0;i<getInstances().size();i++) {
//            System.out.print("ID: "+((TreeInstance)getInstances().get(i)).getId()+", LABEL: "+((TreeInstance)getInstances().get(i)).getLabel());
//            System.out.println(", X: "+((TreeInstance)getInstances().get(i)).getX()+", Y: "+((TreeInstance)getInstances().get(i)).getY());
//        }
//
//        System.out.println("-- EDGES --");
//
//        for (int i=0;i<getSelectedConnectivity().getEdges().size();i++) {
//            System.out.print("SOURCE: "+getSelectedConnectivity().getEdges().get(i).getSource());
//            System.out.print(", TARGET: "+getSelectedConnectivity().getEdges().get(i).getTarget());
//            System.out.println("WEIGHT: "+getSelectedConnectivity().getEdges().get(i).getWeight());
//        }
//
//    }

    @Override
    public void removeSelectedInstances() {
        //Removing deleted instances from connectivities...
        ArrayList<Connectivity> conns = this.getConnectivities();

        if (conns != null) {
            for (int i=0;i<conns.size();i++) {
                Connectivity conn = conns.get(i);
                ArrayList<Edge> edges = conn.getEdges();
                if (edges != null) {
                    for (int j=0;j<edges.size();j++) {
                        if (containsInstances(edges.get(j))) {
                            edges.remove(j);
                            j--;
                        }
                    }
                }
            }
        }
        super.removeSelectedInstances();
    }

    private boolean containsInstances(Edge e) {

        for (int i=0;i<selinstances.size();i++)
            if ((e.getSource() == selinstances.get(i).getId())||(e.getTarget() == selinstances.get(i).getId()))
                return true;
        return false;

    }

    /** Tell if only some nodes of tree should be drawn. If true, only the 
     * multiScaleVisibleNodes and the multiScaleBorderNodes will be visible.
     * @return the multiScaleDraw
     */
    public boolean isMultiScaleDraw() {
        return multiScaleDraw;
    }

    /** Tell if only some nodes of tree should be drawn. If true, only the 
     * multiScaleVisibleNodes and the multiScaleBorderNodes will be visible.
     * @param multiScaleDraw the multiScaleDraw to set
     */
    public void setMultiScaleDraw(boolean multiScaleDraw) {
        this.multiScaleDraw = multiScaleDraw;
    }

    /**
     * @param multiScaleVisibleNodes the multiScaleVisibleNodes to set
     */
    public void setMultiScaleVisibleNodes(ArrayList<AbstractInstance> multiScaleVisibleNodes) {
        this.multiScaleVisibleNodes = multiScaleVisibleNodes;
    }

    /**
     * @param multiScaleBorderNodes the multiScaleBorderNodes to set
     */
    public void setMultiScaleBorderNodes(ArrayList<AbstractInstance> multiScaleBorderNodes) {
        this.multiScaleBorderNodes = multiScaleBorderNodes;
    }    

    /**
     * @return the multiScaleLevels
     */
    public int getMultiScaleLevels() {
        return multiScaleLevels;
    }

    /**
     * @param multiScaleLevels the multiScaleLevels to set
     */
    public void setMultiScaleLevels(int multiScaleLevels) {
        this.multiScaleLevels = multiScaleLevels;
    }    
    
    private int root;
    private String type;
    private boolean noAlpha;
    
    private boolean multiScaleDraw = false;
    private ArrayList<AbstractInstance> multiScaleVisibleNodes;
    private ArrayList<AbstractInstance> multiScaleBorderNodes;
    private int multiScaleLevels = 5;
}
