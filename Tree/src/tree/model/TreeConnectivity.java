package tree.model;

import graph.model.Connectivity;
import graph.model.Edge;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import visualizationbasics.model.AbstractInstance;

/**
 * This class represents the tree connectivity.
 * 
 * @author Jose Gustavo de Souza Paiva
 */
public class TreeConnectivity extends Connectivity {
   
    public TreeConnectivity(String name, ArrayList<Edge> edges) {
        super(name,edges);
    }

    public void draw(TreeModel model, BufferedImage image, boolean highquality,
            ArrayList<AbstractInstance> visibleNodes) {
        Graphics2D g2 = (Graphics2D) image.getGraphics();

        if (highquality) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }

        ArrayList<Edge> edges = getEdges();
        int size = edges.size();
        ArrayList<AbstractInstance> selectedInstances = ((TreeModel) model).getSelectedInstances();
        for (int i = 0; i < size; i++) {

            TreeInstance source = (TreeInstance) model.getInstanceById(edges.get(i).getSource());
            TreeInstance target = (TreeInstance) model.getInstanceById(edges.get(i).getTarget());

            if (visibleNodes.contains(source) && visibleNodes.contains(target)) {
                //Combines the color of the two vertex to paint the edge
                if (!source.isValid() && !target.isValid()) {
                    g2.setColor(Color.BLACK);
                } else {
                    float alpha = model.getAlpha();
                    if ((selectedInstances == null)||(selectedInstances.size() == 0)) alpha = 1.0f;
                    if ((selectedInstances == null)||(selectedInstances.size() == 0)||(model.isNoAlpha())) alpha = 1.0f;

                    for (int j=0;j<selectedInstances.size();j++) {
                        if ((source.equals(selectedInstances.get(j)))||(target.equals(selectedInstances.get(j)))) {
                            alpha = 1.0f;
                            break;
                        }
                    }

                    g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
                    g2.setColor(new Color((source.getColor().getRed() + target.getColor().getRed()) / 2,
                            (source.getColor().getGreen() + target.getColor().getGreen()) / 2,
                            (source.getColor().getBlue() + target.getColor().getBlue()) / 2));
                }

                g2.setStroke(new BasicStroke(1.3f));
                g2.drawLine(((int) source.getX()), ((int) source.getY()),
                        ((int) target.getX()), ((int) target.getY()));
                g2.setStroke(new BasicStroke(1.0f));

                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));

                if (this.isShowWeight()) {
                    String label = Float.toString(edges.get(i).getWeight());

                    float x = 5 + (float) Math.abs(source.getX() - target.getX()) / 2 +
                            Math.min(source.getX(), target.getX());
                    float y = (float) Math.abs(source.getY() - target.getY()) / 2 +
                            Math.min(source.getY(), target.getY());

                    //Getting the font information
                    FontMetrics metrics = g2.getFontMetrics(g2.getFont());

                    //Getting the label size
                    int width = metrics.stringWidth(label);
                    int height = metrics.getAscent();

                    g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.75f));
                    g2.setPaint(Color.WHITE);
                    g2.fill(new Rectangle((int) x - 2, (int) y - height, width + 4, height + 4));
                    g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));

                    g2.setColor(Color.BLACK);
                    g2.drawRect((int) x - 2, (int) y - height, width + 4, height + 4);

                    g2.drawString(label, x, y);
                }

            }else {
            }
        }        
    }
    
    public void draw(TreeModel model, BufferedImage image, boolean highquality) {
        Graphics2D g2 = (Graphics2D) image.getGraphics();

        if (highquality) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }

        ArrayList<AbstractInstance> instances = model.getInstances();

        ArrayList<Edge> edges = getEdges();
        int size = edges.size();
        ArrayList<AbstractInstance> selectedInstances = ((TreeModel) model).getSelectedInstances();
        for (int i = 0; i < size; i++) {
            //TreeInstance source = (TreeInstance) instances.get(edges.get(i).getSource());
            //TreeInstance target = (TreeInstance) instances.get(edges.get(i).getTarget());
            TreeInstance source = (TreeInstance) model.getInstanceById(edges.get(i).getSource());
            TreeInstance target = (TreeInstance) model.getInstanceById(edges.get(i).getTarget());

            if (source != null && target != null) {
                //Combines the color of the two vertex to paint the edge
                if (!source.isValid() && !target.isValid()) {
                    g2.setColor(Color.BLACK);
                } else {
                    float alpha = model.getAlpha();
                    if ((selectedInstances == null)||(selectedInstances.size() == 0)) alpha = 1.0f;
                    if ((selectedInstances == null)||(selectedInstances.size() == 0)||(model.isNoAlpha())) alpha = 1.0f;

                    for (int j=0;j<selectedInstances.size();j++) {
                        if ((source.equals(selectedInstances.get(j)))||(target.equals(selectedInstances.get(j)))) {
                            alpha = 1.0f;
                            break;
                        }
                    }

                    g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
                    g2.setColor(new Color((source.getColor().getRed() + target.getColor().getRed()) / 2,
                            (source.getColor().getGreen() + target.getColor().getGreen()) / 2,
                            (source.getColor().getBlue() + target.getColor().getBlue()) / 2));
                }

                g2.setStroke(new BasicStroke(1.3f));
                g2.drawLine(((int) source.getX()), ((int) source.getY()),
                        ((int) target.getX()), ((int) target.getY()));
                g2.setStroke(new BasicStroke(1.0f));

                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));

                if (this.isShowWeight()) {
                    String label = Float.toString(edges.get(i).getWeight());

                    float x = 5 + (float) Math.abs(source.getX() - target.getX()) / 2 +
                            Math.min(source.getX(), target.getX());
                    float y = (float) Math.abs(source.getY() - target.getY()) / 2 +
                            Math.min(source.getY(), target.getY());

                    //Getting the font information
                    FontMetrics metrics = g2.getFontMetrics(g2.getFont());

                    //Getting the label size
                    int width = metrics.stringWidth(label);
                    int height = metrics.getAscent();

                    g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.75f));
                    g2.setPaint(Color.WHITE);
                    g2.fill(new Rectangle((int) x - 2, (int) y - height, width + 4, height + 4));
                    g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));

                    g2.setColor(Color.BLACK);
                    g2.drawRect((int) x - 2, (int) y - height, width + 4, height + 4);

                    g2.drawString(label, x, y);
                }

            }else {
                System.out.println("null");
            }
        }
    }

}
