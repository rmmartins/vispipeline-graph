/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.stress;

import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractComponent;
import labeledgraph.stress.LabeledGraphStressCurveComp;
import tree.model.TreeModel;

/**
 *
 * @author Jose Gustavo
 */
@VisComponent(hierarchy = "Data Analysis",
name = "Tree Stress Curve",
description = "Calculate how the distances relations of a tree differ from the original distances relations, for each instance.")
public class TreeStressCurveComp extends LabeledGraphStressCurveComp implements AbstractComponent {
    
    public void input(@Param(name = "Tree Model") TreeModel model) {
        this.model = model;
    }

    public static final long serialVersionUID = 1L;

}
