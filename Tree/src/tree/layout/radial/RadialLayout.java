package tree.layout.radial;

import graph.model.Edge;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import tree.basics.ContentTree;
import tree.basics.Tree;
import tree.layout.*;
import tree.model.TreeInstance;
import tree.model.TreeModel;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class RadialLayout implements TreeLayout {

    public AbstractMatrix execute(Tree tree) {
        //initializing the tree
        init(tree);

        for (int i=0;i<nodes.size();i++)
            nodes.get(i).dist = 1.0f/(1.0f+nodes.get(i).dist);

        //creating the layout
        postorderTraversal(root);

        root.x = 0.0f;
        root.y = 0.0f;
        root.w = (float) (2 * Math.PI);
        root.t = 0;

        preorderTraversal(root);

        //creating the placement
        DenseMatrix position = new DenseMatrix();

        int size = nodes.size();

        for (int i = 0; i < size; i++) {
            float[] vect = new float[2];
            vect[0] = nodes.get(i).x;
            vect[1] = nodes.get(i).y;

            ContentTree node = tree.getNode(i);
            DenseVector vector = new DenseVector(vect, node.getId(), node.getCdata());
            position.addRow(vector);
        }

        return position;
    }

    public TreeModel execute(TreeModel tree) {
        //initializing the tree
        if (init(tree)) {

            for (int i=0;i<nodes.size();i++) 
                nodes.get(i).dist = 1.0f/(1.0f+nodes.get(i).dist);
            
            //creating the layout
            postorderTraversal(root);

            root.x = 0.0f;
            root.y = 0.0f;
            root.w = (float) (2 * Math.PI);
            root.t = 0;

            preorderTraversal(root);

            //Modifying the model, with new positions...
            for (int i=0;i<nodes.size();i++) {
                TreeInstance ti = (TreeInstance) tree.getInstanceById(nodes.get(i).id);
                if (ti != null) {
                    ti.setX(nodes.get(i).x);
                    ti.setY(nodes.get(i).y);
                }
            }
        }

        return tree;
    }

    private void init(Tree tree) {
        
        ArrayList<Edge> edges = tree.getEdges();
        int nrinstances = tree.getSize();

        //creating the nodes
        nodes = new ArrayList<Node>();
        for (int i = 0; i < nrinstances; i++) {
            int id = tree.getNode(i).getId();
            Node node = new Node(id);
            nodes.add(node);
            if (tree.getRootId() == id)
                root = node;
        }

//        //setting the root
//        if (tree.getRoot()==-1) {
//            root = nodes.get(nrinstances-1);
//        }
        
        for (int i = 0;i < edges.size();i++) {
            Edge edge = edges.get(i);
            Node parent = getNode(edge.getSource());
            Node ch = getNode(edge.getTarget());
            ch.parent = parent;
            ch.dist = edge.getWeight();
            parent.children.add(ch);
        }
    }

    private boolean init(TreeModel tree) {

        if (tree.getSelectedConnectivity() == null) return false;

        ArrayList<Edge> edges = tree.getSelectedConnectivity().getEdges();
        ArrayList<AbstractInstance> instances = tree.getInstances();

        if (instances != null) {

            //creating the nodes
            nodes = new ArrayList<Node>();
            for (int i=0; i < instances.size(); i++) {
                int id = instances.get(i).getId();
                Node node = new Node(id);
                nodes.add(node);
                if (tree.getRoot() == id)
                    root = node;
            }

            //setting the root
            if (tree.getRoot()==-1) {
                root = nodes.get(instances.size()-1);
            }

            for (int i = 0;i < edges.size();i++) {
                Edge edge = edges.get(i);
                Node parent = getNode(edge.getSource());
                Node ch = getNode(edge.getTarget());
                ch.parent = parent;
                ch.dist = edge.getWeight();
                parent.children.add(ch);
            }
        }else return false;

        return true;

    }

    private void postorderTraversal(RadialLayout.Node v) {
        if (v.children.size() == 0) {
            v.l = 1;
        } else {
            v.l = 0;

            for (RadialLayout.Node w : v.children) {
                postorderTraversal(w);
                v.l = v.l + w.l;
            }
        }
    }

    private void preorderTraversal(RadialLayout.Node v) {
        if (v != root) {
            Node u = v.parent;
            v.x = u.x + v.dist * (float) Math.cos(v.t + v.w / 2);
            v.y = u.y + v.dist * (float) Math.sin(v.t + v.w / 2);
        }

        float n = v.t;

        for (RadialLayout.Node w : v.children) {
            w.w = w.l / root.l * (float) (2 * Math.PI);
            w.t = n;
            n = n + w.w;
            preorderTraversal(w);
        }
    }

    private Node getNode(int id) {
        for (int i=0;i<this.nodes.size();i++) {
            if (this.nodes.get(i).id == id) return this.nodes.get(i);
        }
        return null;
    }

    public class Node {

        public Node(int id) {
            this.id = id;
        }

        public int id;
        public float x;
        public float y;
        public float w;
        public float t;
        public float l;
        public Node parent;
        public float dist; //distance to parent
        public ArrayList<Node> children = new ArrayList<Node>();
    }

    private ArrayList<Node> nodes;
    private RadialLayout.Node root;
}
