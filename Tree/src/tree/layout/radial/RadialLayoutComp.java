/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tree.layout.radial;

import java.io.IOException;
import matrix.AbstractMatrix;
import tree.basics.Tree;
import tree.model.TreeModel;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Tree.Layout",
name = "Radial Layout",
description = "Draw to the plane using the radial approach.",
howtocite = "M. Cuadros, F. V. Paulovich, R. Minghim, and G. P. Telles. "
+ "Point Placement by Phylogenetic Trees and its Application for Visual Analysis of Document Collections. "
+ "In Proceedings of IEEE Symposium on Visual Analytics Science and Technology (VAST.2007), "
+ "pages 99.106, Sacramento, CA, USA, 2007"
)
public class RadialLayoutComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
//        if (tree != null) {
//            RadialLayout radial = new RadialLayout();
//            position = radial.execute(tree);
//        } else {
//            throw new IOException("A tree data should be provided.");
//        }
        if (model != null) {
            RadialLayout radial = new RadialLayout();
            model = radial.execute(model);
        } else {
            throw new IOException("A tree data should be provided.");
        }
    }

//    public void input(@Param(name = "tree data") Tree tree) {
//        this.tree = tree;
//    }

    public void input(@Param(name = "tree model") TreeModel model) {
        this.model = model;
    }

//    public AbstractMatrix output() {
//        return position;
//    }

    public TreeModel output() {
        return model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        tree = null;
        position = null;
        model = null;
    }

    public static final long serialVersionUID = 1L;
    private transient Tree tree;
    private transient AbstractMatrix position;
    private transient TreeModel model;

}
