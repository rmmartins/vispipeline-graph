package tree.layout;

import matrix.AbstractMatrix;
import tree.basics.Tree;
import tree.model.TreeModel;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public interface TreeLayout {

    public AbstractMatrix execute(Tree tree);

    public TreeModel execute(TreeModel tree);

}
