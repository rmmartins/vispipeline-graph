/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tree.basics;

import graph.util.GraphConstants;

/**
 *
 * @author Jose Gustavo
 */
public interface TreeConstants extends GraphConstants {

    public static final String NJ = "Neighbor-Joinning";
    public static final String SNJ = "Neighbor-Joinning";
    public static final String MST = "Minimum Spanning Tree";
    public static final String HCT = "Hiearchical Cluster Tree";

}
