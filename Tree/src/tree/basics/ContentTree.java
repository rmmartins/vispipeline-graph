package tree.basics;

import java.util.ArrayList;

public class ContentTree {

    private int id;
    private String label;
    private float cdata;
    private boolean valid;
    private int nivelNo; //0 no folha, > 0, niveis maiores na arvore
    private ContentTree root; //Raiz local da arvore onde este no esta. Poderia usar o parent para chegar a essa informacao, esta sendo feito assim para deixar o processo mais rapido.
    private ArrayList<ContentTree> children;
    private ContentTree parent;
    private ArrayList<Float> distChildren;
    private float distParent;
    private int deepChildCount; //Informa quantos descendentes na arvore existem a partir deste no, considerando toda a hierarquia da arvore.
    
    public ContentTree(int id, String label) {
        this.id = id;
        this.label = label;
        this.cdata = 0.0f;
        this.valid = true;
        this.root = this;
        this.children = new ArrayList<ContentTree>();
        this.parent = null;
        this.nivelNo = 0;
        this.distChildren = new ArrayList<Float>();
        this.distParent = 0;
    }

    public ContentTree(int id, String label, float cdata) {
        this(id,label);
        this.cdata = cdata;
    }

    @Override
    public ContentTree clone() {
        ContentTree t = new ContentTree(this.id,this.label);
        t.valid = this.valid;
        t.root = this.root;
        t.children = new ArrayList<ContentTree>();
        for (int i=0;i<this.children.size();i++) t.children.add(this.children.get(i));
        t.parent = this.parent;
        t.nivelNo = this.nivelNo;
        for (int i=0;i<this.distChildren.size();i++) t.distChildren.add(this.distChildren.get(i));
        t.distParent = this.distParent;
        return t;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public ArrayList<ContentTree> getChildren() {
        return children;
    }

    public ContentTree getParent() {
        return parent;
    }

    public int getNivelNo() {
        return nivelNo;
    }

    public ArrayList<Float> getDistChildren() {
        return distChildren;
    }

    public boolean isValid() {
        return valid;
    }

    public void setChildren(ArrayList<ContentTree> c) {
        this.children = c;
    }

    public void setParent(ContentTree parent) {
        this.parent = parent;
    }

    public void setNivelNo(int nivelNo) {
        this.nivelNo = nivelNo;
        if (this.getParent() != null && this.getParent().getParent() != null && !this.getParent().getParent().equals(this))
            if (this.getParent().getNivelNo() <= nivelNo)
                this.getParent().setNivelNo(nivelNo+1);
    }

    public void setDistChildren(ArrayList<Float> c) {
        this.distChildren = c;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getDistParent() {
        return distParent;
    }

    public void setDistParent(float distParent) {
        this.distParent = distParent;
    }

    public float getCdata() {
        return cdata;
    }

    public void setCdata(float cdata) {
        this.cdata = cdata;
    }

    public boolean isRelative(ContentTree node) {

        ContentTree son;
        boolean result = false;
        for (int i=0;i<node.children.size();i++) {
            son = node.children.get(i);
            if (son != null) {
                if (son.equals(node)) return true;
                else return isRelative(son);
            }
        }
        return result;
    }

    public ContentTree getRoot() {
        return root;
    }

    public void setRoot(ContentTree root) {
        
        this.root = root;
        if (this.nivelNo != 0) {
            for (int i=0;i<children.size();i++) {
                if (this.children.get(i) != null) this.children.get(i).setRoot(root);
            }
        }
    }

    public boolean isRoot() {
        //O pai de um no tem como pai ele mesmo. Isso acontece no nivel mais alto da arvore,
        //pois ja que nao ha raiz, os dois ultimos nos da matriz sao unidos um ao outro.
        return ((this.getParent() != null)&&((this.getParent().getParent() != null)&&(this.getParent().getParent().equals(this))));
    }

    @Override
    public String toString() {
        return id+":"+label+"("+(valid ? "valid":"non-valid")+")";
    }

    /**
     * @return the deepChildCount
     */
    public int getDeepChildCount() {
        return deepChildCount;
    }

    /**
     * @param deepChildCount the deepChildCount to set
     */
    public void setDeepChildCount(int deepChildCount) {
        this.deepChildCount = deepChildCount;
    }

}
