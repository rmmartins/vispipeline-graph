package tree.basics;

import graph.model.Edge;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class Tree {

    private String type;
    private ArrayList<ContentTree> nodes;
    private ArrayList<Edge> edges;
    private int root;

    public Tree() {
        nodes = new ArrayList<ContentTree>();
        edges = new ArrayList<Edge>();
        root = -1;
    }

    @Override
    public Tree clone() {
        Tree ntree = new Tree();
        ntree.type = this.type;
        ntree.root = this.root;

        ntree.nodes = new ArrayList<ContentTree>();
        HashMap<ContentTree,ContentTree> t = new HashMap<ContentTree,ContentTree>();

        for (int i=0;i<this.nodes.size();i++) {
            ContentTree n = (ContentTree)this.nodes.get(i);
            ContentTree p = n.clone();
            ntree.nodes.add(p);
            t.put(n, p);
        }

        for (int i=0;i<ntree.nodes.size();i++) {
            ContentTree n = (ContentTree)ntree.nodes.get(i);

            if (n.getChildren() != null) {
                for (int j=0;j<n.getChildren().size();j++) {
                    n.getChildren().set(j,(ContentTree)t.get(n.getChildren().get(j)));
                }
            }
            if (n.getParent()!= null)
                n.setParent((ContentTree)t.get(n.getParent()));
        }

        ntree.edges = new ArrayList<Edge>();
        ntree.edges.addAll(this.edges);

        return ntree;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void addNode(ContentTree node) {
        nodes.add(node);
    }

    public ArrayList<ContentTree> getNodes() {
        return nodes;
    }

    public int getNumberRealNodes() {
        int result = 0;
        for (int i=0;i<nodes.size();i++) {
            if (nodes.get(i).isValid())
                result++;
        }
        return result;
    }

    public boolean isNode(ContentTree node) {
        for (int i=0;i<nodes.size();i++)
            if (nodes.get(i).equals(node)) return true;
        return false;
    }

    public ContentTree getNode(int index) {
        return nodes.get(index);
    }

    public int getNodePosition(ContentTree node) {
        return nodes.indexOf(node);
    }

    public ContentTree getNodeByLabel(String label) {
        for (int i=0;i<nodes.size();i++) {
            if (nodes.get(i).getLabel().equalsIgnoreCase(label)) return nodes.get(i);
        }
        return null;
    }
   
    public ContentTree getNodeById(Integer id) {
        for (int i=0;i<nodes.size();i++) {
            if (nodes.get(i).getId() == id) return nodes.get(i);
        }
        return null;
    }

    public void removeNode(ContentTree node) {
        if (node != null) {
            //Verificando se ele eh filho de alguem. Se for, passaremos seus filhos para este pai.
            ContentTree parent = node.getParent();
            if (parent != null) {
                int id = parent.getChildren().indexOf(node);
                if (id != -1) {
                    parent.getChildren().remove(id);
                    parent.getDistChildren().remove(id);
                    if ((node.getChildren() != null)&&(node.getChildren().size() > 0))
                        parent.getChildren().addAll(node.getChildren());
                    if ((node.getDistChildren() != null)&&(node.getDistChildren().size() > 0))
                        parent.getDistChildren().addAll(node.getDistChildren());
                    for (int i=0;i<node.getChildren().size();i++) {
                        node.getChildren().get(i).setParent(parent);
                    }
                }
            }else {
                //O no era uma raiz. Por enquanto nao farei nada. TODO.
            }
        }
        int indexNode = nodes.indexOf(node);
        nodes.remove(node);
        //Reajustando os indices dos nos subsequentes...
        for(int i=indexNode;i<nodes.size();i++)
            //nodes.get(i).setId(nodes.get(i).getId()-1);
            nodes.get(i).setLabel(new Integer(Integer.parseInt(nodes.get(i).getLabel())-1).toString());
    }

    public int getMaxNivel() {
        int maxNivel = nodes.get(0).getNivelNo();
        for (int i=1;i<nodes.size();i++)
            if (nodes.get(i).getNivelNo() > maxNivel)
                maxNivel = nodes.get(i).getNivelNo();
        return maxNivel;
    }

    public int getSize() {
        return nodes.size();
    }

    public ArrayList<Edge> getEdges() {
        return edges;
    }

    public void setEdges(ArrayList<Edge> edges) {
        this.edges = edges;
    }
    
//    public int getRoot() {
//        return root;
//    }

    public int getRootId() {
        int id = 0;
        int maxNivel = nodes.get(0).getNivelNo();
        for (int i=1;i<nodes.size();i++)
            if (nodes.get(i).getNivelNo() >= maxNivel) {
                maxNivel = nodes.get(i).getNivelNo();
                id = nodes.get(i).getId();
            }
        return id;
    }

    public void setRoot(int root) {
        this.root = root;
    }

    private boolean existEdge(int sour, int targ) {
        for (int i=0;i<edges.size();i++) {
            if (((edges.get(i).getSource()==sour)&&(edges.get(i).getTarget()==targ))||
                ((edges.get(i).getSource()==targ)&&(edges.get(i).getTarget()==sour)))
                return true;
        }
        return false;
    }

    public void generateEdges() {
        //ArrayList<Edge> ledges = new ArrayList<Edge>();
        int sour, targ;
        float dis;
        //generating the connectivity
        for (int i=0;i<this.getSize();i++) {
            ContentTree son = this.getNode(i);
            if (son != null) {
                dis = son.getDistParent();
                ContentTree parent = son.getParent();
                if (parent != null) {
                    sour = parent.getId();
                    targ = son.getId();
                    int ind = parent.getChildren().indexOf(son);
                    if (ind != -1)
                        dis = parent.getDistChildren().get(ind);
                    else
                        dis = 1000; //This distance is just a dummy distance, for the layout algorithm procedure, and is not related to any distance.
                    if (!existEdge(sour,targ)) {
                        edges.add(new Edge(sour, targ, dis));
                    }
                }
            }
        }
        //this.setEdges(edges);
    }

    public void printNodes() {
        System.out.println("\n\n--- NODES ---");
        for (int i=0;i<nodes.size();i++) {
            System.out.print("ID:"+nodes.get(i).getId()+":"+
                             "CLASS:"+nodes.get(i).getCdata()+":"+
                             "NIVEL:"+nodes.get(i).getNivelNo()+":");
            if (nodes.get(i).getParent() == null)
                System.out.print("PARENT: -1:");
            else
                System.out.print("PARENT:" + nodes.get(i).getParent().getId() + ":");
            System.out.print(" | CHILDREN:");
            if (!nodes.get(i).getChildren().isEmpty())
                for (int j=0;j<nodes.get(i).getChildren().size();j++)
                    System.out.print(" "+nodes.get(i).getChildren().get(j).getId()+"("+nodes.get(i).getDistChildren().get(j)+")");
            else
                System.out.print("NONE");
            System.out.println(" ");
        }
        System.out.println("-------------\n\n");
    }

    public void printEdges() {
        System.out.println("\n\n--- EDGES ---");
        for (int i=0;i<edges.size();i++) {
            ContentTree source = this.getNodeById(edges.get(i).getSource());
            ContentTree target = this.getNodeById(edges.get(i).getTarget());
            System.out.println("SOURCE : "+source.getLabel()+" | TARGET : "+target.getLabel()+ " ("+edges.get(i).getWeight()+")");
            //System.out.println("SOURCE : "+source.getId()+" | TARGET : "+target.getId()+ " ("+edges.get(i).getWeight()+")");
        }
        System.out.println("-------------\n\n");

    }
    
    public void updateDeepChildCount() {
        ArrayList<ContentTree> nodesAux = new ArrayList<ContentTree>();
        for (ContentTree node: nodes) {
            nodesAux.add(node);
        }
        
        Collections.sort(nodesAux, new Comparator() {
            public int compare(Object o1, Object o2) {  
                ContentTree node1 = (ContentTree) o1;  
                ContentTree node2 = (ContentTree) o2;
                return node1.getNivelNo() - node2.getNivelNo();
            }
        });
        
        for (ContentTree node: nodesAux) {
            if (node.getParent() != null) {
                int parentDeepChildCount = node.getParent().getDeepChildCount();
                int nodeDeepChildCount = node.getDeepChildCount();
                node.getParent().setDeepChildCount(parentDeepChildCount + nodeDeepChildCount + 1);
            }
        }
    }    
    

}
