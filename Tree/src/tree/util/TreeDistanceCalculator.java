package tree.util;

import graph.model.Edge;
import java.util.ArrayList;

/**
 *
 * @author Jose Gustavo
 */
public class TreeDistanceCalculator {
    
    //Methods to get distance on a tree.
    public static float getDistance(ArrayList<Edge> edges, int node1, int node2) {
        
        int nodeLevel1 = getNodeHeight(edges,node1);
        int nodeLevel2 = getNodeHeight(edges,node2);

        float dist1 = 0, dist2 = 0;
        
        while (nodeLevel1 > 0 && nodeLevel2 > 0) {
            if (nodeLevel1 > nodeLevel2) {
                dist1 += getDistParent(edges,node1);
                node1 = getParent(edges,node1);
                nodeLevel1--;
            }
            else if (nodeLevel2 > nodeLevel1) {
                dist2 += getDistParent(edges,node2);
                node2 = getParent(edges,node2);
                nodeLevel2--;
            }
            else {
                if (node1 == node2)
                    return dist1+dist2;
                dist1 += getDistParent(edges,node1);
                node1 = getParent(edges,node1);
                dist2 += getDistParent(edges,node2);
                node2 = getParent(edges,node2);
                nodeLevel1--;
                nodeLevel2--;
            }
        }
        if (node1 == node2)
            return dist1+dist2;
        return -1;
    }
    
    private static Edge getTargetEdge(ArrayList<Edge> edges, int target) {
        
        for (int i=0;i<edges.size();i++)
            if (edges.get(i).getTarget() == target)
                return edges.get(i);
        
        return null;
        
    }
    
    private static int getParent(ArrayList<Edge> edges, int target) {
        
        Edge e = getTargetEdge(edges,target);
        if (e != null)
            return e.getSource();
        return -1;
        
    }
    
    private static float getDistParent(ArrayList<Edge> edges, int target) {
        
        Edge e = getTargetEdge(edges,target);
        if (e != null)
            return e.getWeight();
        return -1;
        
    }
    
    private static int getNodeHeight(ArrayList<Edge> edges, int node) {
        int level = 0;
        Edge edge = getTargetEdge(edges,node);
        while (edge != null) {
            level++;
            edge = getTargetEdge(edges,edge.getSource());
        }
        return level;
    }
    //End of methods
    
}
