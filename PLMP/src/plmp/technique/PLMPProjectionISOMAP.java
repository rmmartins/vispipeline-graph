/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package plmp.technique;

import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.reader.MatrixReaderComp;
import datamining.sampling.Sampling.SampleType;
import projection.model.ProjectionModelComp;
import projection.technique.isomap.ISOMAPProjection;
import projection.view.ProjectionFrameComp;

/**
 *
 * @author paulovich
 */
public class PLMPProjectionISOMAP extends PLMPProjection {

    @Override
     protected AbstractMatrix projectRepresentatives(AbstractMatrix sampledata,
            float fracdelta, int nriteractions, AbstractDissimilarity diss) throws IOException {
        ISOMAPProjection isomap = new ISOMAPProjection();
        isomap.setNumberNeighbors(8);
        return isomap.project(sampledata, diss);
    }

        public static void main(String[] args) {
        try {
            MatrixReaderComp reader = new MatrixReaderComp();
            reader.setFilename("/home/paulovich/Dropbox/dados/swissroll10000.data");
            reader.execute();
            AbstractMatrix matrix = reader.output();

            PLMPProjectionISOMAP plmp = new PLMPProjectionISOMAP();
            plmp.setFractionDelta(8.0f);
            plmp.setNumberIterations(100);
            plmp.setSampleType(SampleType.RANDOM);
            plmp.setSampleSize(matrix.getRowCount());
            AbstractMatrix projection = plmp.project(matrix, new Euclidean());

            ProjectionModelComp model = new ProjectionModelComp();
            model.input(projection);
            model.execute();

            ProjectionFrameComp frame = new ProjectionFrameComp();
            frame.setTitle("Complete Projection");
            frame.input(model.output());
            frame.execute();
        } catch (IOException ex) {
            Logger.getLogger(PLMPProjection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
