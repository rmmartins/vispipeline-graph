/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package plmp.technique;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;
import matrix.dense.DenseVector;

/**
 *
 * @author PC
 */
public class CheckConsistency {

    public CheckConsistency() {
    }

    public AbstractMatrix removeNullColumns(AbstractMatrix matrix) throws IOException {
        //discovering if there are null columns
        nullcols = new boolean[matrix.getDimensions()];
        Arrays.fill(nullcols, true);

        boolean allnotnull = true; //indicates if all values of one row are not null

        int size = matrix.getRowCount();
        for (int i = 0; i < size; i++) {
            float[] array = matrix.getRow(i).toArray();

            for (int j = 0; j < array.length; j++) {
                if (Math.abs(array[j]) > EPSILON) {
                    nullcols[j] = false;
                } else {
                    allnotnull = false;
                }
            }

            if (allnotnull) {
                break;
            }

            allnotnull = true;
        }

        //calculate the number of not null columns
        int nrnotnullcols = 0;
        for (int i = 0; i < nullcols.length; i++) {
            if (!nullcols[i]) {
                nrnotnullcols++;
            }
        }

        //removing the null columns
        AbstractMatrix newmatrix = MatrixFactory.getInstance(matrix.getClass());

        for (int i = 0; i < size; i++) {
            AbstractVector row = matrix.getRow(i);
            float[] array = row.toArray();

            float[] vector = new float[nrnotnullcols];
            for (int j = 0, k = 0; j < array.length; j++) {
                if (!nullcols[j]) {
                    vector[k++] = array[j];
                }
            }

            newmatrix.addRow(new DenseVector(vector, row.getId(), row.getKlass()));
        }

        Logger.getLogger(getClass().getName()).log(Level.INFO,
                "Number of original columns: {0}\nNumber of null columns: {1}",
                new Object[]{matrix.getDimensions(), matrix.getDimensions() - nrnotnullcols});

        return newmatrix;
    }

    public float[] returnTheRemoved(float[] vector) {
        if (nullcols != null) {
            float[] newvector = new float[nullcols.length];

            for (int j = 0, k = 0; j < newvector.length; j++) {
                if (!nullcols[j]) {
                    newvector[j] = vector[k++];
                } else {
                    newvector[j] = 0;
                }
            }
            return newvector;
        }

        return vector;
    }

    public static void main(String[] args) throws IOException {
        String filename = "/home/paulovich/Dropbox/dados/multifield.0099-normcols.bin-200000.bin";
        AbstractMatrix instance = MatrixFactory.getInstance(filename);

        CheckConsistency cc = new CheckConsistency();
        AbstractMatrix removeNullColumns = cc.removeNullColumns(instance);
        removeNullColumns.save(filename + "-notnull.data");
        System.out.println(instance.getDimensions() + " => " + removeNullColumns.getDimensions());
    }
    private boolean[] nullcols;
    private static final float EPSILON = 0.0000001f;
}
