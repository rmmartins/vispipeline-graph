/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * AttributePanel.java
 *
 * Created on 03/03/2010, 14:31:00
 */
package plmp.technique.streaming;

import java.awt.GridBagConstraints;
import javax.swing.BorderFactory;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import prefuse.util.ui.JRangeSlider;

/**
 *
 * @author PC
 */
public class AttributePanel extends javax.swing.JPanel {

    /** Creates new form AttributePanel */
    public AttributePanel(String name) {
        initComponents();
        setBorder(BorderFactory.createTitledBorder(name));

        rangeslider = new JRangeSlider(0, 100, 0, 0, JRangeSlider.HORIZONTAL);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        add(rangeslider, gbc);

        rangeslider.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                maxTextField.setText(Float.toString(getMax()));
                minTextField.setText(Float.toString(getMin()));
            }

        });        
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        maxTextField = new javax.swing.JTextField();
        minTextField = new javax.swing.JTextField();

        setLayout(new java.awt.GridBagLayout());

        maxTextField.setColumns(3);
        maxTextField.setEditable(false);
        maxTextField.setText("0.0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(maxTextField, gridBagConstraints);

        minTextField.setColumns(3);
        minTextField.setEditable(false);
        minTextField.setText("0.0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(minTextField, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    public float getMax() {
        return rangeslider.getHighValue() / 100.0f;
    }

    public float getMin() {
        return rangeslider.getLowValue() / 100.0f;
    }

    private JRangeSlider rangeslider;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField maxTextField;
    private javax.swing.JTextField minTextField;
    // End of variables declaration//GEN-END:variables
}
