/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plmp.util;

import datamining.sampling.Sampling;
import distance.dissimilarity.DissimilarityFactory;
import java.io.File;
import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.reader.MatrixReaderComp;
import plmp.technique.PLMPProjectionComp;
import projection.util.ProjectionRunner;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractRunner.Param;

/**
 *
 * @author Rafael M. Martins <rafael.martins@lnu.se>
 */
public class PlmpRunner extends ProjectionRunner {

    @Param(desc = "Fraction Delta", pos = 0)
    public float fractionDelta = 8.0f;

    @Param(desc = "Number of Iterations", pos = 1)
    public int numberOfIterations = 100;

    @Param(desc = "Sample Type", pos = 2)
    public Sampling.SampleType sampleType = Sampling.SampleType.RANDOM;

    @Param(desc = "Dissimilarity Type", pos = 3)
    public DissimilarityFactory.DissimilarityType dissType = DissimilarityFactory.DissimilarityType.EUCLIDEAN;

    @Override
    protected AbstractMatrix runProjection(File f) throws IOException {

        MatrixReaderComp reader = new MatrixReaderComp();
        reader.setFilename(f.getAbsolutePath());
        reader.execute();
        AbstractMatrix matrix = reader.output();

        PLMPProjectionComp mmds = new PLMPProjectionComp();
        mmds.setDissimilarityType(dissType);
        mmds.setFractionDelta(fractionDelta);
        mmds.setNumberIterations(numberOfIterations);
        mmds.setSampleType(sampleType);
        //mmds.setSampleSize((int)Math.pow(matrix.getRowCount(), 0.75f));
        mmds.input(matrix);
        mmds.execute();
        return mmds.output();

//            ProjectionModelComp model = new ProjectionModelComp();
//            model.input(projection);
//            model.execute();
//
//            ProjectionFrameComp frame = new ProjectionFrameComp();
//            frame.setTitle("Complete Projection");
//            frame.input(model.output());
//            frame.execute();
    }

    @Override
    public String getName() {
        return PLMPProjectionComp.class.getAnnotation(VisComponent.class).name();
    }

    @Override
    public String getShortName() {
        return "plmp";
    }

}
