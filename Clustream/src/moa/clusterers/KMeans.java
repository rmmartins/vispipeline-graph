/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package moa.clusterers;

import distance.dissimilarity.AbstractDissimilarity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import matrix.AbstractVector;
import matrix.dense.DenseVector;

import moa.cluster.CFCluster;
import moa.cluster.Cluster;
import moa.cluster.Clustering;
import moa.cluster.SphereCluster;
import moa.clusterers.streamkm.MTRandom;

/**
 *
 * @author jansen
 */
public class KMeans {
    
    public static int melhorK;
    public static int verdadeiroK;
	
    public static Clustering kMeans(Cluster[] centers, List<? extends Cluster> data) {
        
        int k = centers.length;

	int dimensions = centers[0].getCenter().length;

	ArrayList<ArrayList<Cluster>> clustering = new ArrayList<ArrayList<Cluster>>();
	for (int i=0;i<k;i++)
            clustering.add(new ArrayList<Cluster>());

	int repetitions = 100;
        
	while (repetitions-- >= 0) {
	    // Assign points to clusters
	    for (Cluster point : data) {
		double minDistance = distance(dimensions,point.getCenter(),centers[0].getCenter());
		int closestCluster = 0;
		for (int i=1;i<k;i++) {
		    double distance = distance(dimensions,point.getCenter(),centers[i].getCenter());
		    if (distance < minDistance) {
			closestCluster = i;
			minDistance = distance;
		    }
		}
		clustering.get(closestCluster).add(point);
	    }

	    // Calculate new centers and clear clustering lists
	    SphereCluster[] newCenters = new SphereCluster[centers.length];
	    for (int i=0;i<k;i++) {
		newCenters[i] = calculateCenter(clustering.get(i),dimensions);
		clustering.get(i).clear();
	    }
	    centers = newCenters;
	}

	return new Clustering( centers );
    }

    private static double distance(int dimension, double[] pointA, double [] pointB){
        double distance = 0.0;
        for (int i = 0; i < dimension; i++) {
            double d = pointA[i] - pointB[i];
            distance += d * d;
        }
        return Math.sqrt(distance);
    }

    public static double visPipelineDistance(double[] pointA, double[] pointB, AbstractDissimilarity diss) {
        
        float[] values = pointA.length < pointB.length ? new float[pointA.length] : new float[pointB.length];
        for (int i=0;i<values.length;i++)
            values[i] = (float)pointA[i];
        AbstractVector vA = new DenseVector(values);
        values = new float[values.length];
        for (int i=0;i<values.length;i++)
            values[i] = (float)pointB[i];
        AbstractVector vB = new DenseVector(values);
        return diss.calculate(vA,vB);
        
    }
    
    private static SphereCluster calculateCenter(ArrayList<Cluster> cluster, int dimensions) {
        
	double[] res = new double[dimensions];
	for (int i=0;i<res.length;i++) {
	    res[i] = 0.0;
	}

	if (cluster.size() == 0) {
	    return new SphereCluster(res,0.0);
	}

	for (Cluster point : cluster) {
            double [] center = point.getCenter();
            for (int i=0;i<res.length;i++) {
               res[i] += center[i];
            }
	}

	// Normalize
	for (int i=0;i<res.length;i++) {
	    res[i] /= cluster.size();
	}

	// Calculate radius
	double radius = 0.0;
	for (Cluster point : cluster) {
	    double dist = distance(res.length,res,point.getCenter());
	    if (dist > radius) {
		radius = dist;
	    }
	}

	return new SphereCluster(res,radius);
    }

    public static Clustering gaussianMeansOriginal(Clustering gtClustering, Clustering clustering, ArrayList<ArrayList<Integer>> grupo) {
        ArrayList<CFCluster> microclusters = new ArrayList<CFCluster>();
        ArrayList<ArrayList<Integer>> grupoAux = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < clustering.size(); i++) {
            if (clustering.get(i) instanceof CFCluster) {
                microclusters.add((CFCluster)clustering.get(i));
            }
            else{
                System.out.println("Unsupported Cluster Type:"+clustering.get(i).getClass()
                        +". Cluster needs to extend moa.cluster.CFCluster");
            }
        }
        Cluster[] centers = new Cluster[gtClustering.size()];
        for (int i = 0; i < centers.length; i++) {
            centers[i] = gtClustering.get(i);

        }
                  
        int k = centers.length;
	if ( microclusters.size() < k ) {
	    return new Clustering( new Cluster[0]);
	}
        
        //feito por Elaine
        for (int i = 0; i < k; i++)
            grupoAux.add(new ArrayList<Integer>());

	Clustering kMeansResult = kMeans(centers,microclusters);

	k = kMeansResult.size();
	CFCluster[] res = new CFCluster[ k ];

        int contador = 0;
	for ( CFCluster microcluster : microclusters) {
	    // Find closest kMeans cluster
	    double minDistance = Double.MAX_VALUE;
	    int closestCluster = 0;
	    for ( int i = 0; i < k; i++ ) {
		double distance = distance(kMeansResult.get(i).getCenter().length, kMeansResult.get(i).getCenter(), microcluster.getCenter() );
		if ( distance < minDistance ) {
		    closestCluster = i;
		    minDistance = distance;
		}
	    }

	    // Add to cluster
	    if ( res[closestCluster] == null ) {
		res[closestCluster] = (CFCluster)microcluster.copy();
	    } else {
		res[closestCluster].add( microcluster );
	    }
            grupoAux.get(closestCluster).add((int)contador);
            contador++;
	}

	// Clean up res
	int count = 0;
	for ( int i = 0; i < res.length; i++ ) {
	    if ( res[i] != null )
		++count;
	}

	CFCluster[] cleaned = new CFCluster[count];
	count = 0;
	for ( int i = 0; i < res.length; i++ ) {
	    if ( res[i] != null )
		cleaned[count++] = res[i];
            grupo.add(grupoAux.get(i));
	}

	return new Clustering( cleaned );
    }

    public static Clustering gaussianMeans(int nroGrupos, Clustering clustering, int algEscolhaCentros, AbstractDissimilarity diss, ArrayList<ArrayList<Integer>> grupo) {
        
        ArrayList<CFCluster> microclusters = new ArrayList<CFCluster>();
        ArrayList<ArrayList<Integer>>grupoAux = new ArrayList<ArrayList<Integer>>();
        
        for (int i = 0; i < clustering.size(); i++)
            if (clustering.get(i) instanceof CFCluster)
                microclusters.add((CFCluster)clustering.get(i));
            else
                System.out.println("Unsupported Cluster Type:"+clustering.get(i).getClass()+". Cluster needs to extend moa.cluster.CFCluster");
        
        Cluster[] centers = new Cluster[nroGrupos];
        Cluster[] melhoresCentros = null;

        //feito por Elaine
        for (int i = 0; i < nroGrupos; i++)
            grupoAux.add(new ArrayList<Integer>());
        
        switch (algEscolhaCentros){
            case 0: melhoresCentros = centrosKMeansMais(nroGrupos, microclusters, diss); break;
            case 1: melhoresCentros = centrosJG(microclusters,nroGrupos); break;
            case 2: melhoresCentros = centrosSeq(microclusters, nroGrupos); break;
            case 3: melhoresCentros = centrosClustream(microclusters, nroGrupos); break;
            case 4: melhoresCentros = centrosAleatorios(microclusters, nroGrupos); break;
        }
        
        int k = centers.length;
        
	Clustering kMeansResult = moa.clusterers.clustream.Clustream.kMeans(k,melhoresCentros,microclusters,null,diss);

	k = kMeansResult.size();
	CFCluster[] res = new CFCluster[k];

	int contador = 0;
	for (CFCluster microcluster : microclusters) {
	    // Find closest kMeans cluster
	    double minDistance = Double.MAX_VALUE;
	    int closestCluster = 0;
	    for (int i=0;i<k;i++) {
                //double distance = distance(kMeansResult.get(i).getCenter().length, kMeansResult.get(i).getCenter(),microcluster.getCenter());
                double distance = moa.clusterers.KMeans.visPipelineDistance(kMeansResult.get(i).getCenter(),microcluster.getCenter(),diss);
                if (distance < minDistance) {
                    closestCluster = i;
                    minDistance = distance;
                }
	    }

	    // Add to cluster
	    if (res[closestCluster] == null) {
                res[closestCluster] = (CFCluster)microcluster.copy();
            } else {
                res[closestCluster].add(microcluster);
            }
	    grupoAux.get(closestCluster).add((int)contador);
	    contador++;
	}

	// Clean up res
	int count = 0;
	for (int i=0;i<res.length;i++) {
            if (res[i] != null)
	    	++count;
	}

	CFCluster[] cleaned = new CFCluster[count];
	count = 0;
	for (int i=0;i<res.length;i++ ) {
	    if (res[i] != null)
                cleaned[count++] = res[i];
	    grupo.add(grupoAux.get(i));
	}
        
	return new Clustering(cleaned);
    }
    
    public static Cluster[] centrosMOA(ArrayList<CFCluster> clustering, int k, Clustering clusterTrue){
    	
    	Cluster[] centers = new Cluster[k];
    	for (int i=0; i<centers.length; i++)
    		centers[i] = clusterTrue.get(i);
    	return centers;
    }
    
    //get the centers according to the original clustream algorithm, according to the size of each microcluster of the list.
    private static Cluster[] centrosClustream(ArrayList<CFCluster> microclusters, int tam) {
        
        //Modo1: centros com probabilidade proporcional ao tamanho dos grupos
        Cluster[] centers = new Cluster[tam];
        //ordena os microclusters em ordem decrescente de tamanho
        Collections.sort (microclusters, new Comparator() {
            public int compare(Object o1, Object o2) {
                CFCluster p1 = (CFCluster) o1;
                CFCluster p2 = (CFCluster) o2;
                return p1.getN() > p2.getN() ? -1 : (p1.getN() < p2.getN() ? +1 : 0);
            }
        });
        int tamanhoTotal=0;
         //calculo do tamanho total de elementos nos microclusters
         for (int i=0; i<microclusters.size(); i++)
             tamanhoTotal+= microclusters.get(i).getN();
         
         Random m = new Random(1);//System.currentTimeMillis());
         //MTRandom m = new MTRandom(1);
         int nroaleatorio=0, nromicro=0;
         int nrocentros = centers.length;
         ArrayList<Integer>NroSorteados = new ArrayList<Integer>();
         for (int i = 0; i < nrocentros; i++) {
            nroaleatorio = m.nextInt(tamanhoTotal);
            //busca em qual microclusters esta o nro aleatorio
            int soma = 0;
            for (int j=0; j<microclusters.size(); j++) {
                if ((nroaleatorio >= soma) && (nroaleatorio < microclusters.get(j).getN()+soma)) {
                    if (!(NroSorteados.contains(j))) {
                        nromicro = j;
                        NroSorteados.add(nromicro);
                        break;
                    }
                    else{
                        i--;
                        break;
                    }
                }
                soma+= (int) microclusters.get(j).getN();
            }        	
            centers[i] = microclusters.get(nromicro);
         }
         return centers;
    }

    //Get k elementos of the microcluster list, according to Kmeans++ distribution.
    private static Cluster[] centrosKMeansMais(int k, ArrayList<CFCluster> points, AbstractDissimilarity diss) {
        
        //array to store the choosen centres
    	Cluster[] centres = new Cluster[k]; 
    	MTRandom clustererRandom;
    	clustererRandom = new MTRandom(System.currentTimeMillis());
    	int[] centreIndex = new int[points.size()];
    	double[] curCost = new double[points.size()];
        
        //choose the first centre (each point has the same probability of being choosen)
        int i = 0;
	int next = 0;
	int j = 0;
        
        next = clustererRandom.nextInt(points.size()-1);
        
        //set j to next unchoosen point
        j = next;
        
        //copy the choosen point to the array
        centres[i] = points.get(j);
			
	//set the current centre for all points to the choosen centre
	for(i=0;i<points.size();i++) {
            centreIndex[i] = 0;
            curCost[i] = visPipelineDistance(points.get(i).getCenter(),centres[0].getCenter(),diss);
        }
        
        //choose centre 1 to k-1 with the kMeans++ distribution
        for(i=1;i<k;i++) {
            double cost = 0.0;
            for(j=0;j<points.size();j++)
                cost += curCost[j];
			
            double random = 0;
            double sum = 0.0;
            int pos = -1;
            random = clustererRandom.nextDouble();
            sum = 0.0;
            pos = -1;

            for(j=0;j<points.size();j++){
                sum = sum + curCost[j];
                if(random <= sum/cost) {
                    pos = j;
                    break;
                }
            }

            //copy the choosen centre
            centres[i] = points.get(pos);
            //check which points are closest to the new centre
            for(j=0;j<points.size();j++) {
                double newCost = visPipelineDistance(points.get(j).getCenter(),centres[i].getCenter(),diss);
                if(curCost[j] > newCost) {
                    curCost[j] = newCost;
                    centreIndex[j] = i;
                }
            }
        }
        return centres;
    }

    //Get k randomly choosen elements of the microclusters list.
    public static Cluster[] centrosAleatorios(ArrayList<CFCluster> clustering, int k) {
        
        Random obj = new Random();
        obj.setSeed(10);
    	Cluster[] centers = new Cluster[k];
    	HashSet<Integer> valores = new HashSet<Integer>();
        while (valores.size() < centers.length) {
            int x = (int) (obj.nextInt(clustering.size()));
            valores.add(new Integer(x));
        }
        int cont = 0;
        for (Integer i : valores) {
            centers[cont] = clustering.get(i);
            cont++;
        }
        return centers;
        
    }

    //Divide the microclusters list in k equal parts, and get the first element of each part.
    public static Cluster[] centrosJG(ArrayList<CFCluster> clustering, int k) {
        int slop = (clustering.size())/k;
        Cluster[] centers = new Cluster[k];
        for (int i=0;i<k;i++) {
            centers[i] = clustering.get(i*slop);
        }
       return centers;
    }
    
    //Get the first k elements of the microclusters list
    public static Cluster[] centrosSeq(ArrayList<CFCluster> clustering, int k) {
        Cluster[] centers = new Cluster[k];
        for (int i = 0; i < centers.length; i++)
            centers[i] = clustering.get(i);
        return centers;
    }
    
    public static double silhueta(int k, Clustering clusters, int ResGrupos[],ArrayList<CFCluster> microclusters ){
	    int numFCluster = k;
	    double silhueta=0;
	    double silhuetaPonto = 0;
	
	    for (int p=0; p<ResGrupos.length;p++){
	    	Integer ownClusters=ResGrupos[p];
	    	    		
	       	double[] distanceByClusters = new double[numFCluster-1];
	       	//laco para procurar a distancia de cada elemento a todos os grupos
	       	int pos=0;
	       	for (int fc = 0; fc < numFCluster; fc++) {
	       		if (fc!=ownClusters){
	       			double distance = distance(clusters.get(fc).getCenter().length, microclusters.get(p).getCenter(), clusters.get(fc).getCenter());
	       			distanceByClusters[pos] = distance;
	       			pos++;
	       		}
	       	}
	       	Arrays.sort(distanceByClusters);
	       	double distPontoSeuCluster = distance(clusters.get(ownClusters).getCenter().length,microclusters.get(p).getCenter(),clusters.get(ownClusters).getCenter());
	       	double distPontoClusterMaisProx = distanceByClusters[0];
	
	       	silhuetaPonto += (distPontoClusterMaisProx - distPontoSeuCluster)/Math.max(distPontoClusterMaisProx, distPontoSeuCluster);
	    }
	    silhueta = silhuetaPonto / microclusters.size(); 
	    return silhueta;
    }  	

    
}
