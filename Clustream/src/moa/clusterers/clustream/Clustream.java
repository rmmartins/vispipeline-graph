package moa.clusterers.clustream;

import distance.dissimilarity.AbstractDissimilarity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import moa.cluster.Cluster;
import moa.cluster.Clustering;
import moa.cluster.SphereCluster;
import moa.clusterers.AbstractClusterer;
import moa.core.Measurement;
import moa.options.IntOption;
import weka.core.DenseInstance;
import weka.core.Instance;


public class Clustream extends AbstractClusterer{
    public IntOption timeWindowOption = new IntOption("timeWindow",
			't', "Rang of the window.", 1000);

    public IntOption maxNumKernelsOption = new IntOption(
			"maxNumKernels", 'k',
			"Maximum number of micro kernels to use.", 100);

    public static final int m = 50;
    public static final int t = 2;
    private int timeWindow;
    private long timestamp = -1;
    private ClustreamKernel[] kernels;
    private boolean initialized;
    private List<ClustreamKernel> buffer; // Buffer for initialization with kNN
    private int bufferSize;
    private ArrayList<Integer> grupo[];
    public long init;

    public Clustream() {
    }


    @Override
    public void resetLearningImpl() {
    	//mexi
    	//this.bufferSize = maxNumKernelsOption.getValue();
    	//this.bufferSize = 20;
    	
    	//mexi
    	//this.kernels = new ClustreamKernel[maxNumKernelsOption.getValue()];
        //this.kernels = new ClustreamKernel[this.bufferSize];
	this.timeWindow = timeWindowOption.getValue();
	this.initialized = false;
	this.buffer = new LinkedList<ClustreamKernel>();
	
	//mexi - adicionei
	//grupo = (ArrayList<Integer>[])new ArrayList[this.bufferSize];
    }

    public ArrayList<Integer>[] retornaGrupo(){
    	return grupo;
    }
    public void InicializaBufferSize(int valor){
    	this.bufferSize = valor;
    	this.kernels = new ClustreamKernel[this.bufferSize];
    	grupo = (ArrayList<Integer>[])new ArrayList[this.bufferSize];
    }
    
    @Override
    public void trainOnInstanceImpl(Instance instance) {
        int dim = instance.numValues();
        if (timestamp==-1){
        	for (int i=0; i< this.grupo.length;i++)
    			grupo[i] = new ArrayList<Integer>();
        }
        
	timestamp++;
        // 0. Initialize
	if ( !initialized ) {
	    if ( buffer.size() < bufferSize*10 ) {
		buffer.add( new ClustreamKernel(instance,dim, timestamp) );
		//grupo[(int)timestamp].add((int)timestamp);
		return;
	    }

	    int k = kernels.length;
	    assert (k < bufferSize);

	    
        int nroaleatorio;
        List<Integer> numeros = new ArrayList<Integer>();  
        for (int cont = 0; cont < buffer.size(); cont++) { 
            numeros.add(cont);  
        }  
        Collections.shuffle(numeros); 
	    
	    
	    ClustreamKernel[] centers = new ClustreamKernel[k];
	    for ( int i = 0; i < k; i++ ) {
		centers[i] = buffer.get( i ); // TODO: make random!
	    //	nroaleatorio = numeros.get(i).intValue();
	    //	centers[i] = buffer.get(nroaleatorio);
	    	//grupo[i].add((int)nroaleatorio);
	    }
	    Clustering kmeans_clustering = kMeans(k, centers, buffer,grupo);

	    for ( int i = 0; i < kmeans_clustering.size(); i++ ) {
		kernels[i] = new ClustreamKernel( new DenseInstance(1.0,centers[i].getCenter()), dim, timestamp );
	    }

	    buffer.clear();
	    initialized = true;
	    //mexi --> acho q nao pode ter esse return, senao 1 instancia fica sem ir para os micro-clusters
	    //no meu caso, a instancia de nro 11
	    //return;
	    
	}


	// 1. Determine closest kernel
	ClustreamKernel closestKernel = null;
	double minDistance = Double.MAX_VALUE;
	int kernelProximo = 0;

	for ( int i = 0; i < kernels.length; i++ ) {
            //System.out.println(i+" "+kernels[i].getWeight()+" "+kernels[i].getDeviation());
	    double distance = distance(instance.toDoubleArray(), kernels[i].getCenter() );
	    if ( distance < minDistance ) {
		closestKernel = kernels[i];
		kernelProximo=i;
		minDistance = distance;
	    }
	}
	// 2. Check whether instance fits into closestKernel
	double radius = 0.0;
	if ( closestKernel.getWeight() == 1 ) {
	    // Special case: estimate radius by determining the distance to the
	    // next closest cluster
	    radius = Double.MAX_VALUE;
	    double[] center = closestKernel.getCenter();
	    for ( int i = 0; i < kernels.length; i++ ) {
		if ( kernels[i] == closestKernel ) {
		    continue;
		}

		double distance = distance(kernels[i].getCenter(), center );
		radius = Math.min( distance, radius );
	    }
	} else {
	    radius = closestKernel.getRadius();
	}

	if ( minDistance < radius ) {
	    // Date fits, put into kernel and be happy
	    closestKernel.insert( instance, timestamp );
	    grupo[kernelProximo].add((int)timestamp);
            return;
	}

	// 3. Date does not fit, we need to free
	// some space to insert a new kernel
	long threshold = timestamp - timeWindow; // Kernels before this can be forgotten

	// 3.1 Try to forget old kernels
	/*for ( int i = 0; i < kernels.length; i++ ) {
	    if ( kernels[i].getRelevanceStamp() < threshold ) {
		kernels[i] = new ClustreamKernel( instance, dim, timestamp );
		System.out.println("\n*****aqui");
		return;
	    }
	}*/

	// 3.2 Merge closest two kernels
	int closestA = 0;
	int closestB = 0;
	minDistance = Double.MAX_VALUE;
	for ( int i = 0; i < kernels.length; i++ ) {
	    double[] centerA = kernels[i].getCenter();
	    for ( int j = i + 1; j < kernels.length; j++ ) {
		double dist = distance( centerA, kernels[j].getCenter() );
		if ( dist < minDistance ) {
		    minDistance = dist;
		    closestA = i;
		    closestB = j;
		}
	    }
	}
	assert (closestA != closestB);

	kernels[closestA].add( kernels[closestB] );
	kernels[closestB] = new ClustreamKernel( instance, dim, timestamp );
	for (int i=0;i<grupo[closestB].size();i++){
		grupo[closestA].add(grupo[closestB].get(i));
	}
	grupo[closestB]= new ArrayList<Integer>();
	grupo[closestB].add((int)timestamp);
		
    }

//    public void trainOnInstance2(Instance instance, AbstractDissimilarity diss) {
//        int dim = instance.numValues();
//        if (timestamp==-1){
//        	for (int i=0; i< this.grupo.length;i++)
//    			grupo[i] = new ArrayList<Integer>();
//        }
//        
//	timestamp++;
//        // 0. Initialize
//	if ( !initialized ) {
//	    if ( buffer.size() < bufferSize ) {
//		buffer.add( new ClustreamKernel(instance,dim, timestamp) );
//		//grupo[(int)timestamp].add((int)timestamp);
//		return;
//	    }
//
//	    int k = kernels.length;
//	    assert (k < bufferSize);
//
//	    
//        int nroaleatorio;
//        List<Integer> numeros = new ArrayList<Integer>();  
//        for (int cont = 0; cont < buffer.size(); cont++) { 
//            numeros.add(cont);  
//        }  
//        Collections.shuffle(numeros); 
//	    
//	    
//	    ClustreamKernel[] centers = new ClustreamKernel[k];
//	    for ( int i = 0; i < k; i++ ) {
//		centers[i] = buffer.get( i ); // TODO: make random!
//	    //	nroaleatorio = numeros.get(i).intValue();
//	    //	centers[i] = buffer.get(nroaleatorio);
//	    	//grupo[i].add((int)nroaleatorio);
//	    }
//	    //Clustering kmeans_clustering = kMeans(k, centers, buffer,grupo);
//            Clustering kmeans_clustering = kMeans(k,centers,buffer,grupo,diss);
//
//	    for ( int i = 0; i < kmeans_clustering.size(); i++ ) {
//		kernels[i] = new ClustreamKernel( new DenseInstance(1.0,centers[i].getCenter()), dim, timestamp );
//	    }
//
//	    buffer.clear();
//	    initialized = true;
//	    //mexi --> acho q nao pode ter esse return, senao 1 instancia fica sem ir para os micro-clusters
//	    //no meu caso, a instancia de nro 11
//	    //return;
//	    
//	}
//
//
//	// 1. Determine closest kernel
//	ClustreamKernel closestKernel = null;
//	double minDistance = Double.MAX_VALUE;
//	int kernelProximo = 0;
//
//	for ( int i = 0; i < kernels.length; i++ ) {
//            //System.out.println(i+" "+kernels[i].getWeight()+" "+kernels[i].getDeviation());
//	    //double distance = distance(instance.toDoubleArray(), kernels[i].getCenter() );
//            double distance = moa.clusterers.KMeans.visPipelineDistance(instance.toDoubleArray(), kernels[i].getCenter(),diss);
//	    if ( distance < minDistance ) {
//		closestKernel = kernels[i];
//		kernelProximo=i;
//		minDistance = distance;
//	    }
//	}
//	// 2. Check whether instance fits into closestKernel
//	double radius = 0.0;
//	if ( closestKernel.getWeight() == 1 ) {
//	    // Special case: estimate radius by determining the distance to the
//	    // next closest cluster
//	    radius = Double.MAX_VALUE;
//	    double[] center = closestKernel.getCenter();
//	    for ( int i = 0; i < kernels.length; i++ ) {
//		if ( kernels[i] == closestKernel ) {
//		    continue;
//		}
//
//		//double distance = distance(kernels[i].getCenter(), center );
//                double distance = moa.clusterers.KMeans.visPipelineDistance(kernels[i].getCenter(), center,diss );
//		radius = Math.min( distance, radius );
//	    }
//	} else {
//	    radius = closestKernel.getRadius();
//	}
//
//	if ( minDistance < radius ) {
//	    // Date fits, put into kernel and be happy
//	    closestKernel.insert( instance, timestamp );
//	    grupo[kernelProximo].add((int)timestamp);
//            return;
//	}
//
//	// 3. Date does not fit, we need to free
//	// some space to insert a new kernel
//	long threshold = timestamp - timeWindow; // Kernels before this can be forgotten
//
//	// 3.1 Try to forget old kernels
//	/*for ( int i = 0; i < kernels.length; i++ ) {
//	    if ( kernels[i].getRelevanceStamp() < threshold ) {
//		kernels[i] = new ClustreamKernel( instance, dim, timestamp );
//		System.out.println("\n*****aqui");
//		return;
//	    }
//	}*/
//
//	// 3.2 Merge closest two kernels
//	int closestA = 0;
//	int closestB = 0;
//	minDistance = Double.MAX_VALUE;
//	for ( int i = 0; i < kernels.length; i++ ) {
//	    double[] centerA = kernels[i].getCenter();
//	    for ( int j = i + 1; j < kernels.length; j++ ) {
//		//double dist = distance( centerA, kernels[j].getCenter() );
//                double dist = moa.clusterers.KMeans.visPipelineDistance(centerA, kernels[j].getCenter(),diss);
//		if ( dist < minDistance ) {
//		    minDistance = dist;
//		    closestA = i;
//		    closestB = j;
//		}
//	    }
//	}
//	assert (closestA != closestB);
//
//	kernels[closestA].add( kernels[closestB] );
//	kernels[closestB] = new ClustreamKernel( instance, dim, timestamp );
//	for (int i=0;i<grupo[closestB].size();i++){
//		grupo[closestA].add(grupo[closestB].get(i));
//	}
//	grupo[closestB]= new ArrayList<Integer>();
//	grupo[closestB].add((int)timestamp);
//		
//    }
    
    public void trainOnInstance(Instance instance, AbstractDissimilarity diss) {
        
//        System.out.println("------------------------");
//        System.out.println("--"+timestamp+"--");
        
        int dim = instance.numValues();
        if (timestamp==-1) {
            for (int i=0; i< this.grupo.length;i++)
                grupo[i] = new ArrayList<Integer>();
            init = System.currentTimeMillis();
        }
        
	timestamp++;
        // 0. Initialize
	if (!initialized) {
	    if (buffer.size() < bufferSize) {
		buffer.add(new ClustreamKernel(instance,dim,timestamp));
		return;
	    }

	    int k = kernels.length;
	    assert (k < bufferSize);

            List<Integer> numeros = new ArrayList<Integer>();  
            for (int cont = 0; cont < buffer.size(); cont++) { 
                numeros.add(cont);  
            }  
            Collections.shuffle(numeros); 

            int numeroAleatorio = 0;
            ClustreamKernel[] centers = new ClustreamKernel[k];
            for (int i=0;i<k;i++) {
                //centers[i] = buffer.get(i);
                numeroAleatorio = numeros.get(i).intValue();
                centers[i] = buffer.get(numeroAleatorio);
            }

            Clustering kmeans_clustering = kMeans(k,centers,buffer,grupo,diss);
            
            long end = System.currentTimeMillis();
            long diff = end - init;
            //System.out.println("Time spent (0 to Kmeans execution) -> " + (diff/1000.0f) + " seconds.");

            for (int i=0;i<kmeans_clustering.size();i++)
                kernels[i] = new ClustreamKernel(new DenseInstance(1.0,centers[i].getCenter()),dim,timestamp);

            buffer.clear();
            initialized = true;
	    
	}
        init = System.currentTimeMillis();
       

	// 1. Determine closest kernel
	ClustreamKernel closestKernel = null;
	double minDistance = Double.MAX_VALUE;
	int kernelProximo = 0;

	for ( int i=0;i<kernels.length;i++) {
	    double distance = moa.clusterers.KMeans.visPipelineDistance(instance.toDoubleArray(),kernels[i].getCenter(),diss);
            //double distance = distance(instance.toDoubleArray(),kernels[i].getCenter());
	    if (distance < minDistance) {
		closestKernel = kernels[i];
		kernelProximo=i;
		minDistance = distance;
	    }
	}
        
	// 2. Check whether instance fits into closestKernel
	double radius = 0.0;
	if (closestKernel.getWeight() == 1) {
            //System.out.println("1");
	    // Special case: estimate radius by determining the distance to the
	    // next closest cluster
	    radius = Double.MAX_VALUE;
	    double[] center = closestKernel.getCenter();
	    for (int i=0;i<kernels.length;i++) {
		if (kernels[i] == closestKernel)
		    continue;
		double distance = moa.clusterers.KMeans.visPipelineDistance(kernels[i].getCenter(),center,diss);
                //double distance = distance(kernels[i].getCenter(),center);
		radius = Math.min( distance, radius );
	    }
	} else {
            //System.out.println("2");
	    radius = closestKernel.getRadius();
	}

	if (minDistance < radius) {
	    // Date fits, put into kernel and be happy
	    closestKernel.insert(instance, timestamp);
            //int totalOld = grupo[kernelProximo].size();
	    grupo[kernelProximo].add((int)timestamp);
            //System.out.println("Adding instance on group :"+kernelProximo+". It had "+totalOld+", and now it has "+grupo[kernelProximo].size());
            long end = System.currentTimeMillis();
            long diff = end - init;
            //System.out.println("Time spent (Timestamp "+timestamp+" - Fits closest kernel) -> " + (diff/1000.0f) + " seconds.");
            return;
	}

	// 3. Date does not fit, we need to free
	// some space to insert a new kernel
	//long threshold = timestamp - timeWindow; // Kernels before this can be forgotten

	// 3.1 Try to forget old kernels
	/*for ( int i = 0; i < kernels.length; i++ ) {
	    if ( kernels[i].getRelevanceStamp() < threshold ) {
		kernels[i] = new ClustreamKernel( instance, dim, timestamp );
		System.out.println("\n*****aqui");
		return;
	    }
	}*/

	// 3.2 Merge closest two kernels
	int closestA = 0;
	int closestB = 0;
	minDistance = Double.MAX_VALUE;
	for ( int i = 0; i < kernels.length; i++ ) {
	    double[] centerA = kernels[i].getCenter();
	    for ( int j = i + 1; j < kernels.length; j++ ) {
		double dist = moa.clusterers.KMeans.visPipelineDistance(centerA,kernels[j].getCenter(),diss);
                //double dist = distance(centerA,kernels[j].getCenter());
		if ( dist < minDistance ) {
		    minDistance = dist;
		    closestA = i;
		    closestB = j;
		}
	    }
	}
	assert (closestA != closestB);

        //System.out.print("Adding B:"+grupo[closestB].size()+" to A: "+grupo[closestA].size());
	kernels[closestA].add( kernels[closestB] );
	kernels[closestB] = new ClustreamKernel( instance, dim, timestamp );
	for (int i=0;i<grupo[closestB].size();i++){
		grupo[closestA].add(grupo[closestB].get(i));
	}
        //System.out.println(" - Total:"+grupo[closestA].size());
	grupo[closestB]= new ArrayList<Integer>();
	grupo[closestB].add((int)timestamp);
        
        long end = System.currentTimeMillis();
            long diff = end - init;
            //System.out.println("Time spent (Timestamp "+timestamp+" - Merge kernels) -> " + (diff/1000.0f) + " seconds.");
        
        
//        int total = 0;
//        for (int i=0;i<grupo.length;i++) {
//            //System.out.println(grupo[i].size());
//            total += grupo[i].size();
//        }
//        System.out.println("Total: "+total);
                
    }
    
    @Override
    public Clustering getMicroClusteringResult() {
	if ( !initialized ) {
	    return new Clustering( new Cluster[0] );
	}


	ClustreamKernel[] res = new ClustreamKernel[kernels.length];
        int total = 0;
	for ( int i = 0; i < res.length; i++ ) {
	    res[i] = new ClustreamKernel( kernels[i] );
            //System.out.println(res[i].getWeight());
            total += res[i].getWeight();
	}
        //System.out.println(total);

	return new Clustering( res );
    }

    @Override
    public boolean implementsMicroClusterer() {
        return true;
    }

    @Override
    public Clustering getClusteringResult() {
        return null;
    }

    public String getName() {
        return "Clustream " + timeWindow;
    }

    private static double distance(double[] pointA, double [] pointB){
        double distance = 0.0;
        for (int i = 0; i < pointA.length; i++) {
            double d = pointA[i] - pointB[i];
            distance += d * d;
        }
        return Math.sqrt(distance);
    }

    public static Clustering kMeans( int k, Cluster[] centers, List<? extends Cluster> data, ArrayList<Integer>[] grupo) {
	
        assert (centers.length == k);
	assert (k > 0);

	int dimensions = centers[0].getCenter().length;

	ArrayList<ArrayList<Cluster>> clustering = new ArrayList<ArrayList<Cluster>>();
	for ( int i = 0; i < k; i++ ) {
	    clustering.add( new ArrayList<Cluster>() );
	}
	
	int repetitions = 100;
	int m;
	while ( repetitions-- >= 0 ) {
	    // Assign points to clusters
            m=0;
	    for ( Cluster point : data ) {
                double minDistance = distance( point.getCenter(), centers[0].getCenter() );
                int closestCluster = 0;
                for ( int i = 1; i < k; i++ ) {
                    double distance = distance( point.getCenter(), centers[i].getCenter() );
                    if ( distance < minDistance ) {
                        closestCluster = i;
                        minDistance = distance;
                    }
                }
                clustering.get( closestCluster ).add( point );
                if (repetitions == 0)
                        grupo[closestCluster].add(m);
                m++;
	    }

	    // Calculate new centers and clear clustering lists
	    SphereCluster[] newCenters = new SphereCluster[centers.length];
	    for ( int i = 0; i < k; i++ ) {
		newCenters[i] = calculateCenter( clustering.get( i ), dimensions );
		clustering.get( i ).clear();
	    }
	    centers = newCenters;
	}
	return new Clustering( centers );
    }
    
    public static Clustering kMeans( int k, Cluster[] centers, List<? extends Cluster> data, ArrayList<Integer>[] grupo, AbstractDissimilarity diss ) {
	
        assert (centers.length == k);
	assert (k > 0);

	int dimensions = centers[0].getCenter().length;

	ArrayList<ArrayList<Cluster>> clustering = new ArrayList<ArrayList<Cluster>>();
	for ( int i = 0; i < k; i++ ) {
	    clustering.add( new ArrayList<Cluster>() );
	}
	
	int repetitions = 100;
	int m;
	while ( repetitions-- >= 0 ) {
	    // Assign points to clusters
            m=0;
	    for ( Cluster point : data ) {
                double minDistance = moa.clusterers.KMeans.visPipelineDistance(point.getCenter(),centers[0].getCenter(),diss);
                int closestCluster = 0;
                for ( int i = 1; i < k; i++ ) {
                    double distance = moa.clusterers.KMeans.visPipelineDistance(point.getCenter(),centers[i].getCenter(),diss);
                    if ( distance < minDistance ) {
                        closestCluster = i;
                        minDistance = distance;
                    }
                }
                clustering.get( closestCluster ).add( point );
                if (repetitions == 0)
                    if (grupo != null)
                        grupo[closestCluster].add(m);
                m++;
	    }

	    // Calculate new centers and clear clustering lists
	    SphereCluster[] newCenters = new SphereCluster[centers.length];
	    for ( int i = 0; i < k; i++ ) {
		newCenters[i] = calculateCenter( clustering.get( i ), dimensions, diss );
		clustering.get( i ).clear();
	    }
	    centers = newCenters;
	}
	return new Clustering( centers );
    }

    //Original method, just for checks...
    private static SphereCluster calculateCenter( ArrayList<Cluster> cluster, int dimensions ) {
	double[] res = new double[dimensions];
	for ( int i = 0; i < res.length; i++ ) {
	    res[i] = 0.0;
	}

	if ( cluster.size() == 0 ) {
	    return new SphereCluster( res, 0.0 );
	}

	for ( Cluster point : cluster ) {
            double [] center = point.getCenter();
            for (int i = 0; i < res.length; i++) {
               res[i] += center[i];
            }
	}

	// Normalize
	for ( int i = 0; i < res.length; i++ ) {
	    res[i] /= cluster.size();
	}

	// Calculate radius
	double radius = 0.0;
	for ( Cluster point : cluster ) {
	    double dist = distance( res, point.getCenter() );
	    if ( dist > radius ) {
		radius = dist;
	    }
	}
        SphereCluster sc = new SphereCluster( res, radius );
        sc.setWeight(cluster.size());
	return sc;
    }
    
    private static SphereCluster calculateCenter( ArrayList<Cluster> cluster, int dimensions, AbstractDissimilarity diss ) {
	double[] res = new double[dimensions];
	for ( int i = 0; i < res.length; i++ ) {
	    res[i] = 0.0;
	}

	if ( cluster.size() == 0 ) {
	    return new SphereCluster( res, 0.0 );
	}

	for ( Cluster point : cluster ) {
            double [] center = point.getCenter();
            for (int i = 0; i < res.length; i++) {
               res[i] += center[i];
            }
	}

	// Normalize
	for ( int i = 0; i < res.length; i++ ) {
	    res[i] /= cluster.size();
	}

	// Calculate radius
	double radius = 0.0;
	for ( Cluster point : cluster ) {
	    double dist = moa.clusterers.KMeans.visPipelineDistance(res,point.getCenter(),diss);
	    if ( dist > radius ) {
		radius = dist;
	    }
	}
        SphereCluster sc = new SphereCluster( res, radius );
        sc.setWeight(cluster.size());
	return sc;
    }

    @Override
    protected Measurement[] getModelMeasurementsImpl() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void getModelDescription(StringBuilder out, int indent) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean isRandomizable() {
        return false;
    }

    public double[] getVotesForInstance(Instance inst) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setInitialized(boolean i) {
        initialized = i;
    }
    
    

}

