/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package moa.components;

import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import moa.OfflineClustream;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
@VisComponent(hierarchy = "Data Mining",
name = "Clustream Clustering",
description = "Generate groups using Clustream algorithm on the original attribute space.")
public class ClustreamClusteringComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {

        try {
            if (nroClusters <= 0) {
                System.out.println("Error, number of clusters invalid!");
                return;
            }
            
            OfflineClustream ocs = new OfflineClustream(microClusters,nroClusters,centroidSelectionTechnique);
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            ocs.doCluster(inMatrix,diss);
            int[] mappingInstancesCluster = ocs.getMappingInstancesGroups();
            
            outMatrix = (AbstractMatrix) inMatrix.clone();
            for (int i=0;i<outMatrix.getRowCount();i++) {
                AbstractVector row = outMatrix.getRow(i);
                row.setKlass(mappingInstancesCluster[i]);
            }

        } catch (Exception ex) {
            Logger.getLogger(ClustreamClusteringComp.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        
    }

    public void input(@Param(name = "Points Matrix") AbstractMatrix m) {
        inMatrix = m;
    }

    public AbstractMatrix output() {
        return outMatrix;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new ClustreamClusteringParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        inMatrix = outMatrix = null;
    }

    public DissimilarityType getDisstype() {
        return disstype;
    }

    public void setDisstype(DissimilarityType disstype) {
        this.disstype = disstype;
    }

    public int getNroClusters() {
        return nroClusters;
    }

    public void setNroClusters(int numberCluster) {
        nroClusters = numberCluster;
    }
    
    public int getNromicroClusters() {
        return microClusters;
    }

    public void setNroMicroClusters(int numberCluster) {
        microClusters = numberCluster;
    }
    
    Object getCentroidSelectionTechnique() {
        return centroidSelectionTechnique;
    }

    void setCentroidSelectionTechnique(Integer selectedItem) {
        centroidSelectionTechnique = selectedItem;
    }

    public static final long serialVersionUID = 1L;
    private transient ClustreamClusteringParamView paramview;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private int nroClusters = 0, microClusters = 100;
    private transient AbstractMatrix inMatrix, outMatrix;
    private int centroidSelectionTechnique;
    
}
