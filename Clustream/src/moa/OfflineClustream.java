package moa;
import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.MatrixFactory;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import matrix.sparse.SparseMatrix;
import matrix.sparse.SparseVector;

import moa.cluster.Clustering;
import moa.clusterers.AbstractClusterer;
import moa.clusterers.Clusterer;
import moa.clusterers.clustream.Clustream;
import moa.clusterers.clustream.ClustreamKernel;
import moa.gui.clustertab.ClusteringSetupTab;
import moa.gui.visualization.DataPoint;
import weka.core.*;

//Created by Elaine Ribeiro de Faria, as an adaptation of MOA Framework


public class OfflineClustream extends datamining.clustering.Clustering {
    
    private int nroMicroClusters;
    //private int nrclusters;
    private double[] tamanho;
    private double[] raio;
    private ArrayList<Integer> grupo[];
    private ArrayList<Integer> grupoFinal[];
    Clustering macroClusters;
    //private String Classe;
    //private String Caminho;
    private double distMedia[]; 
    private double withinss[];
    private AbstractMatrix centroids;
    private int centroidSelectionType;
    
    public OfflineClustream(int numMicroClusters, int numMacroClusters, int centroidSelectionType) {
        super(numMacroClusters);
        nroMicroClusters = numMicroClusters;
        this.centroidSelectionType =  centroidSelectionType;
    }
    
    @Override
    public ArrayList<ArrayList<Integer>> execute(AbstractDissimilarity diss, AbstractMatrix matrix) throws IOException {
        
        doCluster(matrix,diss);
        int[] mappingInstancesCluster = getMappingInstancesGroups();
        
        ArrayList<ArrayList<Integer>> clusters = new ArrayList<ArrayList<Integer>>();
        
        for (int i=0;i<nrclusters;i++)
            clusters.add(new ArrayList<Integer>());
        
        for (int i=0;i<matrix.getRowCount();i++)
            //clusters.get(mappingInstancesCluster[i]-1).add(matrix.getRow(i).getId());
            clusters.get(mappingInstancesCluster[i]-1).add(i);
        
        return clusters;
    }

    @Override
    public ArrayList<ArrayList<Integer>> execute(DistanceMatrix dmat) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    private HashMap<Integer,double[]> loadDenseMatrix(String filename) throws IOException {
        
        HashMap<Integer,double[]> rows = new HashMap<Integer,double[]>();
        BufferedReader in = null;

        try {
            in = new BufferedReader(new FileReader(filename));

            //read the header
            char[] header = in.readLine().trim().toCharArray();

            //checking
            if (header.length != 2) {
                throw new IOException("Wrong format of header information.");
            }

            //checking
            if (header[0] != 'D') {
                throw new IOException("Wrong matrix format. It is not the dense format.");
            }

            //read the number of objects
            int nrobjs = Integer.parseInt(in.readLine());

            //read the number of dimensions
            int nrdims = Integer.parseInt(in.readLine());

            //read the attributes
            String line = in.readLine();
            
            //read the vectors
            while ((line = in.readLine()) != null && line.trim().length() > 0) {
                StringTokenizer t2 = new StringTokenizer(line, ";");

                //read the id
                String id = t2.nextToken().trim();

                //class data
                float klass = 0.0f;

                //the vector
                double[] vector = new double[nrdims];

                int index = 0;
                while (t2.hasMoreTokens()) {
                    String token = t2.nextToken();
                    float value = Float.parseFloat(token.trim());
                    if (header[1] == 'Y') {
                        if (t2.hasMoreTokens()) {
                            if (index < nrdims) {
                                vector[index] = value;
                                index++;
                            } else {
                                throw new IOException("Vector with wrong number of "
                                        + "dimensions!");
                            }
                        } else {
                            klass = value;
                        }
                    } else if (header[1] == 'N') {
                        if (index < nrdims) {
                            vector[index] = value;
                            index++;
                        } else {
                            throw new IOException("Vector with wrong number of "
                                    + "dimensions!");
                        }
                    } else {
                        throw new IOException("Unknown class data option");
                    }
                }
                try {
                    rows.put(Integer.parseInt(id),vector);
                }catch (NumberFormatException e) {
                    rows.put(id.toLowerCase().hashCode(),vector);
                }
            }

            //checking
            if (rows.size() != nrobjs) {
                throw new IOException("The number of vectors does not match "
                        + "with the matrix size (" + rows.size() + " - " + nrobjs + ").");
            }
            return rows;

        } catch (FileNotFoundException e) {
            throw new IOException("File " + filename + " does not exist!");
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(OfflineClustream.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    private HashMap<Integer,double[]> loadSparseMatrix(String filename) throws IOException {
        
        HashMap<Integer,double[]> rows = new HashMap<Integer,double[]>();
        BufferedReader in = null;
        
        try {
            in = new BufferedReader(new FileReader(filename));

            //read the header
            char[] header = in.readLine().trim().toCharArray();

            //checking
            if (header.length != 2) {
                throw new IOException("Wrong format of header information.");
            }

            //checking
            if (header[0] != 'S') {
                throw new IOException("Wrong matrix format. It is not the sparse format.");
            }

            //read the number of objects
            int nrobjs = Integer.parseInt(in.readLine());

            //read the number of dimensions
            int nrdims = Integer.parseInt(in.readLine());

            //read the attributes
            String line = in.readLine();            

            //read the vectors
            while ((line = in.readLine()) != null && line.trim().length() > 0) {
                StringTokenizer t2 = new StringTokenizer(line, ";");

                //read the id
                String id = t2.nextToken().trim();

                //class data
                float klass = 0.0f;

                //the vector
                ArrayList<Element> values = new ArrayList<Element>();

                while (t2.hasMoreTokens()) {
                    String token = t2.nextToken();

                    if (header[1] == 'Y') {
                        if (t2.hasMoreTokens()) {
                            StringTokenizer t3 = new StringTokenizer(token, ":");

                            int index = Integer.parseInt(t3.nextToken().trim());
                            float value = Float.parseFloat(t3.nextToken().trim());

                            values.add(new Element(index, value));
                        } else {
                            klass = Float.parseFloat(token.trim());
                        }
                    } else if (header[1] == 'N') {
                        StringTokenizer t3 = new StringTokenizer(token, ":");

                        int index = Integer.parseInt(t3.nextToken().trim());
                        float value = Float.parseFloat(t3.nextToken().trim());

                        values.add(new Element(index, value));
                    } else {
                        throw new IOException("Unknown class data option");
                    }
                }
                
                double[] vector = new double[nrdims];
                
                for (int i=0;i<values.size();i++) {
                    vector[values.get(i).index] = values.get(i).value;
                }

                try {
                    rows.put(Integer.parseInt(id),vector);
                }catch (NumberFormatException e) {
                    rows.put(id.toLowerCase().hashCode(),vector);
                }
            }

            //checking
            if (rows.size() != nrobjs) {
                throw new IOException("The number of vectors does not match "
                        + "with the matrix size (" + rows.size() + " - " + nrobjs + ").");
            }
            return rows;

        } catch (FileNotFoundException e) {
            throw new IOException("File " + filename + " does not exist!");
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(OfflineClustream.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public HashMap<Integer,double[]> loadMatrix(String filename) throws FileNotFoundException, IOException {
        
        if (filename.endsWith("data")) {
            BufferedReader in = new BufferedReader(new java.io.FileReader(filename));

            //read the header
            char[] header = in.readLine().trim().toCharArray();

            //checking
            if (header.length != 2 && header.length != 3) {
                throw new IOException("Wrong format of header information.");
            }

            in.close();

            if (header[0] == 'D') {
                return loadDenseMatrix(filename);
            } else if (header[0] == 'S') {
                return loadSparseMatrix(filename);
            } else {
                throw new IOException("Unknow file format!");
            }

        }
        
        return null;
    }

    public double truncateDouble(double number, int numDigits) {
        double result = number;
        String arg = "" + number;
        int idx = arg.indexOf('.');
        if (idx!=-1) {
            if (arg.length() > idx+numDigits) {
                arg = arg.substring(0,idx+numDigits+1);
                result  = Double.parseDouble(arg);
            }
        }
        return result;
    }

    public double[] distanciaMaxima() throws NumberFormatException, IOException {
        
        int nroColunas = ((ClustreamKernel)macroClusters.getClustering().get(0)).getLS().length;
        double LS[] = new double[nroColunas];
        double centroGrupo[][] = new double[nrclusters][nroColunas];
        BufferedReader arquivo = null;	
        double distMax[] = new double[nrclusters];
        withinss = new double[nrclusters];
        distMedia = new double[nrclusters];

        //obtendo o centro de cada grupo
        for (int i=0; i<nrclusters; i++){			
            LS= ((ClustreamKernel)macroClusters.getClustering().get(i)).getLS();
            for (int j=0; j<nroColunas; j++){
                centroGrupo[i][j] = LS[j]/tamanho[i];
            }
            distMax[i] = 0;
            withinss[i] = 0;
        }

        //lendo os dados do arquivo
        //arquivo = new BufferedReader(new FileReader(new File(Caminho+Classe)));
        String linha;
        double linhaarq[]=null;
        int tam;
        int cont = 0;
        double distancia=0;
        int pos=0;

        while ((linha = arquivo.readLine()) != null) {
            String linhaarqString []= linha.split(",");
            tam = linhaarqString.length-1;
            linhaarq = new double[tam];
            for (int i=0; i<tam; i++){
                linhaarq[i] = Double.parseDouble(linhaarqString[i]);
            }

            //procura elemento lido em um dos macro grupos
            for (int i=0; i<nrclusters; i++){
                if (grupoFinal[i].contains(cont)){
                    pos = i;
                    break;
                }   					
            }
            //calcula distancia do elemento ao centro do seu grupo
            double soma=0;
            for (int i=0; i< tam; i++){
                soma = soma + Math.pow(linhaarq[i] - centroGrupo[pos][i], 2);
            }
            distancia = Math.sqrt(soma);
            distMedia[pos] =+ distancia;
            withinss[pos] =  withinss[pos] + (distancia * distancia);
            //se a distancia encontrada for maior q a maior distancia
            if (distancia > distMax[pos]) {
                distMax[pos] = distancia;
            }
            cont++;
        }	
        for (int i=0; i< distMax.length; i++) {
            if (distMax[i] == 0)
                distMax[i] = 0.0001;
            if (withinss[i] == 0)
                withinss[i] = 0.0001;
        }  		
        return distMax;
    }

    public double[] distanciaMedia(){
        double res[] = new double[nrclusters];
        for (int i=0; i<nrclusters;i++){
            res[i]= distMedia[i]/tamanho[i];
        }
        return res;
    }

    public double[] getTamanho(){
        return tamanho;
    }

    public double[] getWithinss(){
        return withinss;
    }
    
    //Returns a vector of centroids, one per group...
    public double[] doCluster(String file) throws IOException {
        
        AbstractClusterer Cluster1;
        ClusteringSetupTab clusteringSetupTab = new ClusteringSetupTab();

        macroClusters = null;
        Clustering micro0 = null;

        Cluster1 = clusteringSetupTab.getClusterer0();
        Cluster1.prepareForUse();
        ((Clustream)Cluster1).InicializaBufferSize(nroMicroClusters);
        
        HashMap<Integer,double[]> dataset = loadMatrix(file);
        
        int tam = ((double[])dataset.values().toArray()[0]).length;
        
        for (int i=0;i<dataset.size();i++) {
            Instance inst = new DenseInstance(1,dataset.get(dataset.keySet().toArray()[i]));
            Cluster1.trainOnInstanceImpl(inst);
        }

        //quando ler todos as linha e obtiver os microgrupos, criar os macro     
        micro0 = Cluster1.getMicroClusteringResult();	
        ArrayList<ArrayList<Integer>> grupoAux = new ArrayList<ArrayList<Integer>>();
        macroClusters = moa.clusterers.KMeans.gaussianMeans(nrclusters,micro0,3,DissimilarityFactory.getInstance(DissimilarityType.EUCLIDEAN),grupoAux);
        grupo = (ArrayList<Integer>[])new ArrayList[nroMicroClusters];
        grupo = ((Clustream)Cluster1).retornaGrupo();

        nrclusters = grupoAux.size();
        tamanho=new double[nrclusters];
        raio = new double[nrclusters];
        grupoFinal = (ArrayList<Integer>[])new ArrayList[nrclusters];
        
        for (int i=0; i<grupoFinal.length;i++) {
            grupoFinal[i] = new ArrayList<Integer>();
        }

        for (int i=0; i<grupoAux.size();i++){
            for (int j=0; j<grupoAux.get(i).size();j++) {
                int pos = grupoAux.get(i).get(j);
                for (int k=0; k< grupo[pos].size();k++) {
                    grupoFinal[i].add((int)grupo[pos].get(k));
                }
            }
        }

        double centroid[] = null;
        double resultado[] = new double[nrclusters*tam];
        centroids = new DenseMatrix();
        double LS[] = new double[tam];
        double SS[] = new double[tam];
        System.out.println("nroMacro: " + nrclusters);
        System.out.println("tamanho do vetor Macro" + macroClusters.size());
        for (int k=0;k<nrclusters;k++) {
            //obtendo o tamanho de cada cluster
            LS = ((ClustreamKernel)macroClusters.getClustering().get(k)).getLS();
            SS = ((ClustreamKernel)macroClusters.getClustering().get(k)).getSS();
            tamanho[k]= macroClusters.getClustering().get(k).getWeight();
            centroid = macroClusters.getClustering().get(k).getCenter();
            float[] floatCentroids = new float[centroid.length];
            for (int c=0;c<centroid.length;c++)
                floatCentroids[c] = (float) centroid[c];
            centroids.addRow(new DenseVector(floatCentroids));
            for (int j=0;j<tam;j++) {
                resultado[nrclusters*j+k] = centroid[j];
                raio[k] = raio[k]+ SS[j] - (LS[j]*LS[j]/tamanho[k]);
            }
        }
        return resultado;
        
    }
    
    public double[] doCluster(AbstractMatrix data, AbstractDissimilarity diss) throws IOException {
        
        long init = System.currentTimeMillis();
        
        AbstractClusterer Cluster1;
        ClusteringSetupTab clusteringSetupTab = new ClusteringSetupTab();
        
        macroClusters = null;
        Clustering microClusters = null;

        Cluster1 = clusteringSetupTab.getClusterer0();
        Cluster1.prepareForUse();
        ((Clustream)Cluster1).InicializaBufferSize(nroMicroClusters);
        
        //Creating microclusters...
        for (int i=0;i<data.getRowCount();i++) {
            double[] values = new double[data.getDimensions()]; //Considering just the attribute values
            if (data instanceof DenseMatrix) {
                for (int j=0;j<data.getRow(i).getValues().length;j++)
                    values[j] = data.getRow(i).getValues()[j];
            }else if (data instanceof SparseMatrix) {
                for (int j=0;j<((SparseVector)data.getRow(i)).getIndex().length;j++)
                    values[((SparseVector)data.getRow(i)).getIndex()[j]] = data.getRow(i).getValues()[j];
            }
            
            Instance inst= new DenseInstance(1, values);
            ((Clustream)Cluster1).trainOnInstance(inst,diss);
        }
        
        int tam = data.getDimensions();
        
        long end = System.currentTimeMillis();
        long diff = end - init;
        long total = diff;
        System.out.println("Time spent (MicroClusters creation) -> " + (diff/1000.0f) + " seconds.");
        init = System.currentTimeMillis();
        
        //Creating macroclusters from microclusters...    
        microClusters = Cluster1.getMicroClusteringResult();
        
        ArrayList<ArrayList<Integer>> grupoAux = new ArrayList<ArrayList<Integer>>();
        macroClusters = moa.clusterers.KMeans.gaussianMeans(nrclusters,microClusters,centroidSelectionType,diss,grupoAux);
        grupo = (ArrayList<Integer>[])new ArrayList[nroMicroClusters];
        grupo = ((Clustream)Cluster1).retornaGrupo();
        
        nrclusters = grupoAux.size();
        tamanho = new double[nrclusters];
        raio = new double[nrclusters];
        grupoFinal = (ArrayList<Integer>[])new ArrayList[nrclusters];
        
        for (int i=0; i<grupoFinal.length;i++) {
            grupoFinal[i] = new ArrayList<Integer>();
        }

        for (int i=0; i<grupoAux.size();i++){
            for (int j=0; j<grupoAux.get(i).size();j++) {
                int pos = grupoAux.get(i).get(j);
                for (int k=0; k< grupo[pos].size();k++) {
                    grupoFinal[i].add((int)grupo[pos].get(k));
                }
            }
        }

        double centroid[] = null;
        double resultado[] = new double[nrclusters*tam];
        centroids = new DenseMatrix();
        double LS[] = new double[tam];
        double SS[] = new double[tam];
        
        for (int k=0;k<nrclusters;k++) {
            //obtendo o tamanho de cada cluster
            LS = ((ClustreamKernel)macroClusters.getClustering().get(k)).getLS();
            SS = ((ClustreamKernel)macroClusters.getClustering().get(k)).getSS();
            tamanho[k]= macroClusters.getClustering().get(k).getWeight();
            centroid = macroClusters.getClustering().get(k).getCenter();
            float[] floatCentroids = new float[centroid.length];
            for (int c=0;c<centroid.length;c++)
                floatCentroids[c] = (float) centroid[c];
            centroids.addRow(new DenseVector(floatCentroids));
            for (int j=0;j<tam;j++) {
                resultado[nrclusters*j+k] = centroid[j];
                raio[k] = raio[k]+ SS[j] - (LS[j]*LS[j]/tamanho[k]);
            }
        }
        
        end = System.currentTimeMillis();
        diff = end - init;
        total += diff;
        System.out.println("Time spent (MacroClusters creation) -> " + (diff/1000.0f) + " seconds.");
        System.out.println("Time spent (Clustream Multidimensional Clustering) -> " + (total/1000.0f) + " seconds.");
        return resultado;
        
    }
    
    public double[] getRaio(){
        return raio;
    }

    public int getNroMacroClusters(){
        return nrclusters;
    }

    public String[] Classe(){
        String vet [] = new String[nrclusters];
        for (int i=0; i<nrclusters; i++)
            vet[i] = "";//Classe;
        return vet;
    }

    //Returns a vector of the size of the number of instances. Each elements corresponds
    //to the group the correspondent data set instance belongs.
    public int[] getMappingInstancesGroups() {
        int resultado[];
        int nroelem = 0;
        for (int i=0; i<nrclusters;i++){
            nroelem += grupoFinal[i].size();
        }
        resultado = new int[nroelem];

        for (int i=0; i<nrclusters;i++){
            for (int j=0;j<grupoFinal[i].size();j++){
                resultado[grupoFinal[i].get(j)] = i+1;
            }
        }
        return resultado;
    }

    public AbstractMatrix getCentroids() {
        return centroids;
    }
    
    public static void main(String[] args) throws IOException {
//        OfflineClustream t = new OfflineClustream();
//        System.out.println("a");
//        //t.CluStream("Iris-setosa","C:\\Users\\Elaine\\Documents\\data_s_n_f_s\\iris_nor1_fold6_ini",20,30);		
//        //t.CluStream("","C:\\Users\\Elaine\\Documents\\data_shuf\\temp.csv",3);
//        t.getTamanho();
//        t.getMappingInstancesGroups();
//        t.distanciaMaxima();
//        t.distanciaMedia();
//        t.getWithinss();
        
        String filename = "D:\\Doutorado\\My Dropbox\\MaterialTestes\\Imagem\\Iris.data";
        
        OfflineClustream ocs = new OfflineClustream(100,3,3);
        
        double[] centroids = ocs.doCluster(filename);
        
        int[] mappingInstancesCluster = ocs.getMappingInstancesGroups();
        
        AbstractMatrix m = MatrixFactory.getInstance(filename);
        
        for (int i=0;i<m.getRowCount();i++) {
            AbstractVector row = m.getRow(i);
            row.setKlass(mappingInstancesCluster[i]);
        }
        
        m.save("E:\\Iris_Clustream.data");
        
    }
    
}

class Element {

    public Element(int index, float value) {
        this.index = index;
        this.value = value;
    }

    public int index;
    public float value;
}