/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.repositioning.image;

import datamining.sampling.Sampling.SampleType;
import datamining.sampling.SamplingComp;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import distance.dissimilarity.Euclidean;
import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import lamp.repositioning.IterativeProjectionFrame;
import lamp.technique.LAMPNeighborhoodR2Projection;
import lamp.util.LampConstants;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.reader.MatrixReaderComp;
import matrix.util.MatrixUtils;
import projection.model.ProjectionModel;
import projection.model.Scalar;
import projection.technique.idmap.IDMAPProjection.InitializationType;
import projection.technique.idmap.IDMAPProjectionComp;
import projection.util.ProjectionConstants;
import projection.util.ProjectionUtil;
import projection.view.ProjectionFrameComp;
import visualizationbasics.color.ColorScaleFactory.ColorScaleType;
import visualizationbasics.util.OpenDialog;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.filter.DATAandBINMultipleFilter;
import visualizationbasics.util.filter.ZIPFilter;

/**
 *
 * @author paulovich
 */
public class IterativeProjectionFrameImage extends IterativeProjectionFrame {

    public IterativeProjectionFrameImage() {
//        buttonPanel.remove(classInfoCheckBox);
//        buttonPanel.remove(weightTextField);
//        buttonPanel.revalidate();
//        buttonPanel.repaint();
    }

    @Override
    protected void init() {
        try {
            PropertiesManager spm = PropertiesManager.getInstance(LampConstants.PROPFILENAME);

            //opening the images dataset
            int resultim = OpenDialog.showOpenDialog(spm, new ZIPFilter(), this);

            if (resultim == JFileChooser.APPROVE_OPTION) {
                String filenameim = OpenDialog.getFilename();
                imcol = new ImageCollection(filenameim);

                //opening the data
                int resultdata = OpenDialog.showOpenDialog(spm, new DATAandBINMultipleFilter(), this);

                if (resultdata == JFileChooser.APPROVE_OPTION) {
                    String filenamedata = OpenDialog.getFilename();

                    MatrixReaderComp reader = new MatrixReaderComp();
                    reader.setFilename(filenamedata);
                    reader.execute();
                    matrix = reader.output();

                    SamplingComp sampling = new SamplingComp();
                    sampling.setDissimilarityType(DissimilarityType.EUCLIDEAN);
                    sampling.setSampleSize((int) Math.sqrt(matrix.getRowCount()));
                    sampling.setSampleType(SampleType.RANDOM);
                    sampling.input(matrix);
                    sampling.execute();
                    samplematrix = sampling.output();

                    //////////////
                    //adding the class information
                    AbstractMatrix samplematrix_aux = samplematrix;
                    if (classInfoCheckBox.isSelected()) {
                        samplematrix_aux = addCdata(samplematrix, Float.parseFloat(weightTextField.getText()));
                    }
                    //////////////

                    IDMAPProjectionComp idmap = new IDMAPProjectionComp();
                    idmap.setDissimilarityType(DissimilarityType.EUCLIDEAN);
                    idmap.setFractionDelta(8.0f);
                    idmap.setInitialization(InitializationType.FASTMAP);
                    idmap.setNumberIterations(100);
                    idmap.input(samplematrix_aux);
                    idmap.execute();
                    sampleproj = idmap.output();

                    //creating the model
                    model = new ProjectionModel();
                    ((ProjectionModel) model).changeColorScaleType(ColorScaleType.CATEGORY_SCALE);
                    Scalar cdata = ((ProjectionModel) model).addScalar(ProjectionConstants.CDATA);
                    int nrows = sampleproj.getRowCount();

                    for (int i = 0; i < nrows; i++) {
                        AbstractVector row = sampleproj.getRow(i);
                        ImageProjectionInstance impi = new ImageProjectionInstance(row.getId(),
                                row.getValue(0), row.getValue(1));
                        model.addInstance(impi);
                        impi.setImage(imcol.getImage(samplematrix.getLabel(i)));
                        impi.setReducedImage(imcol.getReducedImage(samplematrix.getLabel(i)));
                        impi.setScalarValue(cdata, row.getKlass());
                    }

                    setModel(model);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(IterativeProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void finish() {
        if (model != null) {
            try {
                AbstractMatrix cpproj_screen = getRepositionedProjection();

                //re-scaling the screen coordinates of the control points
                //to the original space
                ArrayList<ArrayList<Float>> minmax_screen = MatrixUtils.getMinMax(cpproj_screen);
                ArrayList<ArrayList<Float>> minmax_original = MatrixUtils.getMinMax(sampleproj);
                for (int i = 0; i < sampleproj.getRowCount(); i++) {
                    float[] values = cpproj_screen.getRow(i).getValues();

                    values[0] = (values[0] - minmax_screen.get(0).get(0))
                            / (minmax_screen.get(1).get(0) - minmax_screen.get(0).get(0))
                            * (minmax_original.get(1).get(0) - minmax_original.get(0).get(0))
                            + minmax_original.get(0).get(0);

                    values[1] = (values[1] - minmax_screen.get(0).get(1))
                            / (minmax_screen.get(1).get(1) - minmax_screen.get(0).get(1))
                            * (minmax_original.get(1).get(1) - minmax_original.get(0).get(1))
                            + minmax_original.get(0).get(1);
                }

                LAMPNeighborhoodR2Projection lamp = new LAMPNeighborhoodR2Projection();
                lamp.setPercentageLocalNeighrbors(Float.parseFloat(percneighTextField.getText()));
                lamp.setSampleMatrix(samplematrix);
                lamp.setSampleProjection(cpproj_screen);
                AbstractMatrix projection = lamp.project(matrix, new Euclidean());

                //creating the model
                ProjectionModel finalmodel = new ProjectionModel();
                ((ProjectionModel) finalmodel).changeColorScaleType(ColorScaleType.CATEGORY_SCALE);
                Scalar cdata = ((ProjectionModel) finalmodel).addScalar(ProjectionConstants.CDATA);
                Scalar dots = ((ProjectionModel) finalmodel).addScalar(ProjectionConstants.DOTS);
                int nrows = projection.getRowCount();

                for (int i = 0; i < nrows; i++) {
                    AbstractVector row = projection.getRow(i);
                    ImageProjectionInstance impi = new ImageProjectionInstance(row.getId(),
                            row.getValue(0), row.getValue(1));
                    finalmodel.addInstance(impi);

                    if (imcol != null) {
                        impi.setImage(imcol.getImage(matrix.getLabel(i)));
                        impi.setReducedImage(imcol.getReducedImage(matrix.getLabel(i)));
                    }

                    impi.setScalarValue(cdata, row.getKlass());
                    impi.setScalarValue(dots, 0);
                }

                finalmodel.changeColorScaleType(((ProjectionModel) model).getColorTable().getColorScaleType());

                ProjectionFrameComp frame = new ProjectionFrameComp();
                frame.setTitle("Complete Projection");
                frame.setSize(new Dimension(800, 800));
                frame.input(finalmodel);
                frame.execute();
            } catch (IOException ex) {
                Logger.getLogger(IterativeProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        ProjectionUtil.log(false, false);
        IterativeProjectionFrameImage frame = new IterativeProjectionFrameImage();
        frame.setSize(800, 800);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }
    
    private ImageCollection imcol;
}
