/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.repositioning.image;

/**
 *
 * @author PC
 */
public class ImageInstance implements Comparable<ImageInstance> {

    public ImageInstance(String url) {
        this.url = url;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @return the reduced
     */
    public java.awt.Image getReducedImage() {
        return reduced;
    }

    /**
     * @param reduced the reduced to set
     */
    public void setImage(java.awt.Image image) {
        this.original = image;
        this.reduced = image.getScaledInstance(35, 35, 0);

        //small trick to speed-up future render of the images :-)
        this.original.getHeight(null);
        this.reduced.getHeight(null);
        /////
    }

    /**
     * @return the original
     */
    public java.awt.Image getOriginalImage() {
        return original;
    }

    @Override
    public String toString() {
        return getUrl();
    }

    @Override
    public int compareTo(ImageInstance o) {
        return o.getUrl().compareTo(getUrl());
    }
    
    private String url;
    private java.awt.Image reduced;
    private java.awt.Image original;
}
