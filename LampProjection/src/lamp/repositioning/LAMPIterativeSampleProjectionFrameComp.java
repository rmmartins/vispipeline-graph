/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lamp.repositioning;

import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import projection.model.CircledProjectionInstance;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;
import projection.model.Scalar;
import projection.util.ProjectionConstants;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import visualizationbasics.coordination.AbstractCoordinator;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author DaniloEler
 */
@VisComponent(hierarchy = "Projection.Technique.LAMP",
name = "LAMP Iterative Projection View Frame",
description = "Display control points projection model before final LAMP projection.")
public class LAMPIterativeSampleProjectionFrameComp implements AbstractComponent{

@Override
    public void execute() throws IOException {
        if (sampleprojection != null) {


            LAMPIterativeSampleProjectionFrame frame = new LAMPIterativeSampleProjectionFrame();
            frame.setSize(size);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);            
            frame.setTitle(title);
            frame.setVisible(true);

            //creating the model with CircledProjectionInstance
            model = new ProjectionModel();
            Scalar cdata = model.addScalar(ProjectionConstants.CDATA);
            Scalar dots = model.addScalar(ProjectionConstants.DOTS);

            int nrows = sampleprojection.getRowCount();

            for (int i = 0; i < nrows; i++) {
                AbstractVector row = sampleprojection.getRow(i);
                ProjectionInstance pi = new CircledProjectionInstance(row.getId(),
                        row.getValue(0), row.getValue(1));
                model.addInstance(pi);
                pi.setScalarValue(cdata, row.getKlass());
                pi.setScalarValue(dots, 0.0f);                
            }


            frame.setModel(model);
            frame.setMatrix(matrix);
            frame.setSamplematrix(samplematrix);
            frame.setSampleproj(sampleprojection);

            Scalar sNN = ((ProjectionModel) frame.getModel()).addScalar("ScalarNN");
            Scalar dColor = ((ProjectionModel) frame.getModel()).addScalar(ProjectionConstants.DYNAMIC_COLOR_SCALAR);
//
//
            for (AbstractInstance ins:frame.getModel().getInstances()){
//                float cdataValue = ((ProjectionInstance) ins).getScalarValue(cdata);
                ((ProjectionInstance) ins).setScalarValue(sNN, 0.0f);
                ((ProjectionInstance) ins).setScalarValue(dColor, 0.0f);
            }

            //System.out.println(((ProjectionModel) frame.getModel()).getScalars());
            frame.updateScalars(null);

            if (coordinators != null) {
                for (int i = 0; i < coordinators.size(); i++) {
                    frame.addCoordinator(coordinators.get(i));
                }
            }
        } else {
            throw new IOException("A sample projection should be provided.");
        }
    }


    public void input(@Param(name = "points matrix") AbstractMatrix matrix,
            @Param(name = "sample matrix") AbstractMatrix samplematrix,
            @Param(name = "sample projection") AbstractMatrix sampleprojection) {        
        this.matrix = matrix;
        this.samplematrix = samplematrix;
        this.sampleprojection = sampleprojection;
    }

    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
          if (paramview == null) {
            paramview = new LAMPIterativeSampleProjectionFrameParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        model = null;
        coordinators = null;
        matrix = null;
        samplematrix = null;
        sampleprojection = null;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle () {
        return title;
    }

    public void setSize(Dimension size) {
        this.size = size;
    }

    public Dimension getSize() {
        return size;
    }

    public static final long serialVersionUID = 1L;
    private String title = "";
    private Dimension size = new Dimension(600, 600);
    private transient ProjectionModel model;
    private transient AbstractMatrix matrix;
    private transient AbstractMatrix samplematrix;
    private transient AbstractMatrix sampleprojection;
    private transient LAMPIterativeSampleProjectionFrameParamView paramview;
    private transient ArrayList<AbstractCoordinator> coordinators;

}
