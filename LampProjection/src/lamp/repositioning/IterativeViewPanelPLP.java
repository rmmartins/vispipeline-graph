package lamp.repositioning;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;
import projection.view.selection.InstanceSelection;
import visualizationbasics.color.ColorScalePanel;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.model.AbstractModel;
import visualizationbasics.view.selection.AbstractSelection;

public class IterativeViewPanelPLP extends JPanel {

    public IterativeViewPanelPLP(IterativeProjectionFramePLP frame) {
        this.selcolor = java.awt.Color.RED;
        this.setBackground(java.awt.Color.WHITE);

        this.addMouseMotionListener(new MouseMotionAdapter() {

            @Override
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                super.mouseMoved(evt);
                IterativeViewPanelPLP.this.mouseMoved(evt);
            }

            @Override
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                super.mouseDragged(evt);
                IterativeViewPanelPLP.this.mouseDragged(evt);
            }
        });

        this.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                super.mouseClicked(evt);
                IterativeViewPanelPLP.this.mouseClicked(evt);
            }

            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                super.mousePressed(evt);
                IterativeViewPanelPLP.this.mousePressed(evt);
            }

            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                super.mouseReleased(evt);
                IterativeViewPanelPLP.this.mouseReleased(evt);
            }
        });

        this.setLayout(new FlowLayout(FlowLayout.LEFT));
        this.selection = new InstanceSelection(frame);
    }

    @Override
    public void paintComponent(java.awt.Graphics g) {
        super.paintComponent(g);

        java.awt.Graphics2D g2 = (java.awt.Graphics2D) g;

        if (model != null && image == null) {
            Dimension size = ((ProjectionModel) model).getSize();
            image = new BufferedImage(size.width + 10, size.height + 10,
                    BufferedImage.TYPE_INT_RGB);

            java.awt.Graphics2D g2Buffer = image.createGraphics();
            g2Buffer.setColor(this.getBackground());
            g2Buffer.fillRect(0, 0, size.width + 10, size.height + 10);

            g2Buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);

            ((ProjectionModel) model).draw(image, true);

            g2Buffer.dispose();
        }

        if (image != null) {
            g2.drawImage(image, 0, 0, null);
        }

        //Draw he rectangle to select the instances
        if (selsource != null && seltarget != null) {
            int x = selsource.x;
            int width = width = seltarget.x - selsource.x;

            int y = selsource.y;
            int height = seltarget.y - selsource.y;

            if (selsource.x > seltarget.x) {
                x = seltarget.x;
                width = selsource.x - seltarget.x;
            }

            if (selsource.y > seltarget.y) {
                y = seltarget.y;
                height = selsource.y - seltarget.y;
            }
            g2.setColor(selcolor);
            g2.drawRect(x, y, width, height);

            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.45f));
            g2.fillRect(x, y, width, height);
        } else { //Draw the instance label
            if (labelins != null && labelpos != null && !labelins.isSelected()) {
                labelins.drawLabel(g2, labelpos.x, labelpos.y);
            }
        }

        //drawn the selection polygon
        if (selpolygon != null) {
            g2.setColor(selcolor);
            g2.drawPolygon(selpolygon);

            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.45f));
            g2.fillPolygon(selpolygon);
        }
    }

    public void cleanImage() {
        image = null;
    }

    public void saveToPngImageFile(String filename) throws IOException {
        try {
            Dimension size = ((ProjectionModel) model).getSize();
            BufferedImage buffer = new BufferedImage(size.width + 10, size.height + 10,
                    BufferedImage.TYPE_INT_RGB);
            paint(buffer.getGraphics());
            ImageIO.write(buffer, "png", new File(filename));
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setModel(ProjectionModel model) {
        this.model = model;

        colorscale = new ColorScalePanel(null);
        colorscale.setColorTable(model.getColorTable());
        colorscale.setPreferredSize(new Dimension(200, 12));
        removeAll();
        add(colorscale);

        colorscale.setBackground(getBackground());

        Dimension size = model.getSize();
        setPreferredSize(new Dimension(size.width * 2, size.height * 2));
        setSize(new Dimension(size.width * 2, size.height * 2));

        cleanImage();
        repaint();
    }

    public void setSelection(AbstractSelection selection) {
        this.selection = selection;
    }

    public void zoomIn() {
        if (model != null) {
            ((ProjectionModel) model).zoom(1.1f);

            //Change the size of the panel according to the projection
            Dimension size = ((ProjectionModel) model).getSize();
            setPreferredSize(new Dimension(size.width * 2, size.height * 2));
            setSize(new Dimension(size.width * 2, size.height * 2));

            model.notifyObservers();
        }
    }

    public void zoomOut() {
        if (model != null) {
            ((ProjectionModel) model).zoom(0.9091f);

            //Change the size of the panel according to the projection
            Dimension size = ((ProjectionModel) model).getSize();
            setPreferredSize(new Dimension(size.width * 2, size.height * 2));
            setSize(new Dimension(size.width * 2, size.height * 2));

            model.notifyObservers();
        }
    }

    public void adjustPanel() {
        float iniX = ((ProjectionInstance) model.getInstances().get(0)).getX();
        float iniY = ((ProjectionInstance) model.getInstances().get(0)).getY();
        float max_x = iniX, max_y = iniX;
        float min_x = iniY, min_y = iniY;
        int zero = 30;

        for (int i = 1; i < model.getInstances().size(); i++) {
            float x = ((ProjectionInstance) model.getInstances().get(i)).getX();
            if (max_x < x) {
                max_x = x;
            } else if (min_x > x) {
                min_x = x;
            }

            float y = ((ProjectionInstance) model.getInstances().get(i)).getY();
            if (max_y < y) {
                max_y = y;
            } else if (min_y > y) {
                min_y = y;
            }
        }

        for (AbstractInstance ai : model.getInstances()) {
            ProjectionInstance pi = (ProjectionInstance) ai;
            pi.setX(pi.getX() + zero - min_x);
            pi.setY(pi.getY() + zero - min_y);
        }

        Dimension d = this.getSize();
        d.width = (int) max_x + zero;
        d.height = (int) max_y + zero;
        setSize(d);
        setPreferredSize(d);

        model.notifyObservers();
    }

    public ArrayList<ProjectionInstance> getSelectedInstances(Polygon polygon) {
        ArrayList<ProjectionInstance> selected = new ArrayList<ProjectionInstance>();

        if (model != null) {
            selected = ((ProjectionModel) model).getInstancesByPosition(polygon);
        }

        return selected;
    }

    public ArrayList<ProjectionInstance> getSelectedInstances(Point source, Point target) {
        ArrayList<ProjectionInstance> selinstances = new ArrayList<ProjectionInstance>();

        if (model != null) {
            int x = Math.min(source.x, target.x);
            int width = Math.abs(source.x - target.x);

            int y = Math.min(source.y, target.y);
            int height = Math.abs(source.y - target.y);

            Rectangle rect = new Rectangle(x, y, width, height);
            selinstances = ((ProjectionModel) model).getInstancesByPosition(rect);
        }

        return selinstances;
    }

    @Override
    public final void setBackground(Color bg) {
        super.setBackground(bg);

        if (this.colorscale != null) {
            this.colorscale.setBackground(bg);
        }
    }

    ///////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    //mouse motion methods
    protected void mouseMoved(java.awt.event.MouseEvent evt) {
        if (model != null) {
            labelins = ((ProjectionModel) model).getInstanceByPosition(evt.getPoint());

            if (labelins != null) {
                labelpos = evt.getPoint();
                repaint();
            } else {
                if (labelpos != null) {
                    labelpos = null;
                    repaint();
                }
            }
        }
    }

    protected void mouseDragged(java.awt.event.MouseEvent evt) {
        if (selinstance != null) {
            if (model.hasSelectedInstances()) {
                float x = evt.getX() - selinstance.getX();
                float y = evt.getY() - selinstance.getY();

                for (AbstractInstance ai : model.getSelectedInstances()) {
                    ProjectionInstance pi = (ProjectionInstance) ai;
                    pi.setX(x + pi.getX());
                    pi.setY(y + pi.getY());
                }

                adjustPanel();
            }
        } else if (selsource != null) {
            seltarget = evt.getPoint();
        } else if (selpolygon != null) {
            selpolygon.addPoint(evt.getX(), evt.getY());
        }

        repaint();
    }

    ///////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    //mouse  methods
    protected void mouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
            if (model != null) {
                ProjectionInstance instance = ((ProjectionModel) model).getInstanceByPosition(evt.getPoint());
                if (instance != null) {
                    model.setSelectedInstance(instance);
                    model.notifyObservers();
                }
            }
        }
    }

    public void mousePressed(java.awt.event.MouseEvent evt) {
        if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
            if (model != null) {
                ProjectionInstance instance = ((ProjectionModel) model).getInstanceByPosition(evt.getPoint());

                if (instance != null) {
                    if (model.getSelectedInstances().contains(instance)) {
                        selinstance = instance;
                    }
                } else {
                    selsource = evt.getPoint();
                }
            }
        } else if (evt.getButton() == java.awt.event.MouseEvent.BUTTON3) {
            selpolygon = new java.awt.Polygon();
            selpolygon.addPoint(evt.getX(), evt.getY());
        }
    }

    public void mouseReleased(java.awt.event.MouseEvent evt) {
        if (model != null) {
            if ((selsource != null && seltarget != null) || selpolygon != null) {
                ArrayList<ProjectionInstance> instances = null;

                if (selpolygon != null) {
                    instances = getSelectedInstances(selpolygon);
                } else {
                    instances = getSelectedInstances(selsource, seltarget);
                }

                if (instances != null) {



                    if (selection != null) {
                        selection.selected(new ArrayList<AbstractInstance>(instances));
                    }
                }
            }
        }

        selpolygon = null;
        selinstance = null;
        selsource = null;
        seltarget = null;
    }
    
    protected AbstractModel model;
    protected ProjectionInstance selinstance;
    protected Polygon selpolygon;
    protected Point selsource;
    protected Point seltarget;
    protected Color selcolor;
    protected ProjectionInstance labelins;
    protected Point labelpos;
    protected BufferedImage image;
    protected ColorScalePanel colorscale;
    protected AbstractSelection selection;
}
