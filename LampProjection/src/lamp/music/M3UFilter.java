/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lamp.music;

import visualizationbasics.util.filter.AbstractFilter;

/**
 *
 * @author PC
 */
public class M3UFilter  extends AbstractFilter {

    @Override
    public String getDescription() {
        return "M3U playlist file (*.m3u)";
    }

    @Override
    public String getProperty() {
        return "M3U.DIR";
    }

    @Override
    public String getFileExtension() {
        return "m3u";
    }

}
