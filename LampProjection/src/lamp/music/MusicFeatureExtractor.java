/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.music;

import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import visualizationbasics.util.PropertiesManager;

/**
 *
 * @author paulovich
 */
public class MusicFeatureExtractor {

    public AbstractMatrix execute(MusicCollection collection) {
        DenseMatrix matrix = new DenseMatrix();

        for (int i = 0; i < collection.size(); i++) {
            int id = collection.getMusicByIndex(i).getId();
            float[] vector = new float[10];
            for (int j = 0; j < vector.length; j++) {
                vector[j] = (float) Math.random();
            }
            matrix.addRow(new DenseVector(vector, id));
        }

        return matrix;
    }

    public static void main(String[] args) throws IOException {
        PropertiesManager spm = PropertiesManager.getInstance(MusicBrowserConstants.PROP_FILE);
        MusicCollection collection = MusicCollection.readMusicCollectionFromXML(spm.getProperty(MusicBrowserConstants.XML_FILE));
        MusicFeatureExtractor extract = new MusicFeatureExtractor();
        AbstractMatrix matrix = extract.execute(collection);
        matrix.save(spm.getProperty(MusicBrowserConstants.FEATURE_MATRIX_FILE));
    }
}
