/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.music;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.AudioDevice;
import javazoom.jl.player.FactoryRegistry;
import javazoom.jl.player.Player;

/**
 *
 * @author PC
 */
public class MusicPlayer {

    private MusicPlayer() {
    }

    public static MusicPlayer getInstance() {
        if (instance == null) {
            instance = new MusicPlayer();
        }
        return instance;
    }

    public void play(PlayList playlist) throws JavaLayerException {
        stop();
        player = new MusicPlayerThread(playlist);
        player.start();
    }

    public void play(Music musics) throws JavaLayerException {
        play(musics, 0);
    }

    public void play(Music music, long delaymillis) throws JavaLayerException {
        stop();
        player = new MusicPlayerThread(music, delaymillis);
        player.start();
    }

    public void stop() {
        if (player != null) {
            player.stopPlayer();
        }
        player = null;
    }

    
    class MusicPlayerThread extends Thread {

        public MusicPlayerThread(Music music) {
            this(music, 0);
        }

        public MusicPlayerThread(Music music, long delaymillis) {
            this.delaymillis = delaymillis;
            this.playlist = new ArrayList<Music>();
            this.playlist.add(music);
        }

        public MusicPlayerThread(PlayList playlist) {
            this.delaymillis = 0;
            this.playlist = new ArrayList<Music>();

            ArrayList<MusicProjectionInstance> instances = playlist.getInstances();
            for (int i = 0; i < instances.size(); i++) {
                Music music = instances.get(i).getMusic();
                this.playlist.add(music);
            }
        }

        @Override
        public void run() {
            FileInputStream fin = null;
            BufferedInputStream bin = null;

            try {
                Thread.sleep(delaymillis);
                for (int i = 0; i < playlist.size(); i++) {
                    fin = new FileInputStream(playlist.get(i).getFilename());
                    bin = new BufferedInputStream(fin);
                    AudioDevice dev = FactoryRegistry.systemRegistry().createAudioDevice();
                    player = new Player(bin, dev);
                    player.play();
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MusicPlayer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JavaLayerException ex) {
                Logger.getLogger(MusicPlayer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                //Logger.getLogger(MusicPlayer.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (bin != null) {
                        bin.close();
                    }
                    if (fin != null) {
                        fin.close();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(MusicPlayer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        public void stopPlayer() {
            playlist.clear();
            interrupt();
            if (player != null) {
                player.close();
            }
        }

        private long delaymillis;
        private Player player;
        private ArrayList<Music> playlist;
    }

    public static void main(String[] args) throws JavaLayerException {
        Music m = new Music(0);
        m.setFilename("/home/paulovich/Desktop/MP3_complete_collection/AC_DC_-_30_best_songs_-_01_-_Jailbreak.mp3");
        MusicPlayer.getInstance().play(m);
    }
    
    private static MusicPlayer instance;
    private MusicPlayerThread player;
}
