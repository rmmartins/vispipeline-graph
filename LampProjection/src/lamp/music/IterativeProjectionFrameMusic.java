/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.music;

import java.io.IOException;
import javax.swing.JFrame;
import lamp.repositioning.IterativeProjectionFrame;
import projection.util.ProjectionUtil;

/**
 *
 * @author paulovich
 */
public class IterativeProjectionFrameMusic extends IterativeProjectionFrame {

    public IterativeProjectionFrameMusic() {
        this.view = new IterativeViewPanelMusic(this);
        this.scrollPanel.setViewportView(view);
    }

    @Override
    protected void init() {
//        try {
//            MusicCollection collection = MusicCollection.readMusicCollectionFromXML("/home/paulovich/musics.xml");
//
//            MatrixReaderComp reader = new MatrixReaderComp();
//            reader.setFilename("/home/paulovich/musics.data");
//            reader.execute();
//            matrix = reader.output();
//
//            int nrclusters = (int) Math.sqrt(matrix.getRowCount());
//            BKmeans bkmeans = new BKmeans(nrclusters);
//            clusters = bkmeans.execute(new Euclidean(), matrix);
//
//            PLSPProjection2D plsp = new PLSPProjection2D();
//            cpoints = plsp.getControlPoints(clusters, matrix, new Euclidean(), PLSPProjection2D.SampleType.CLUSTERING);
//
//            //creating the matrix with all control points
//            AbstractMatrix cpmatrix = MatrixFactory.getInstance(matrix.getClass());
//            for (int i = 0; i < cpoints.size(); i++) {
//                for (int j = 0; j < cpoints.get(i).size(); j++) {
//                    cpmatrix.addRow(matrix.getRow(cpoints.get(i).get(j)), matrix.getLabel(cpoints.get(i).get(j)));
//                }
//            }
//
//            //////////////
//            //adding the class information
//            if (classInfoCheckBox.isSelected()) {
//                cpmatrix = addCdata(cpmatrix, Float.parseFloat(weightTextField.getText()));
//            }
//            //////////////
//
//            //initial projection of the control points
//            IDMAPProjectionComp idmap = new IDMAPProjectionComp();
//            idmap.setDissimilarityType(DissimilarityType.EUCLIDEAN);
//            idmap.setFractionDelta(8.0f);
//            idmap.setInitialization(InitializationType.FASTMAP);
//            idmap.setNumberIterations(100);
//            idmap.input(cpmatrix);
//            idmap.execute();
//            cpprojection = idmap.output();
//
//            //creating the model
//            model = new MusicProjectionModel();
//            ((ProjectionModel) model).changeColorScaleType(ColorScaleType.CATEGORY_SCALE);
//            Scalar cdata = ((ProjectionModel) model).addScalar(ProjectionConstants.CDATA);
//            int nrows = cpprojection.getRowCount();
//
//            for (int i = 0; i < nrows; i++) {
//                AbstractVector row = cpprojection.getRow(i);
//                MusicProjectionInstance mpi = new MusicProjectionInstance(((ProjectionModel) model), row.getId(),
//                        row.getValue(0), row.getValue(1));
//                mpi.setScalarValue(cdata, row.getKlass());
//                mpi.setMusic(collection.getMusicById(row.getId()));
//            }
//
//            setModel(model);
//
//        } catch (IOException ex) {
//            Logger.getLogger(IterativeProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    @Override
    protected void finish() {
//        if (model != null) {
//            try {
//                ////////////
//                RedistributeControlPoints2D red = new RedistributeControlPoints2D();
//                ArrayList<ArrayList<Integer>> clusters_aux = red.execute(matrix,
//                        getRepositionedProjection(), cpoints, (int) Math.sqrt(matrix.getRowCount()));
//                ArrayList<ArrayList<Integer>> cpoints_aux = red.getNewControlPoints();
//                AbstractMatrix newproj = red.getNewControlPointsProjection();
//                ////////////
//
//                PLSPProjection2D plsp = new PLSPProjection2D();
//                plsp.setClusters(clusters_aux);
//                plsp.setControlPoints(cpoints_aux);
//                plsp.setControlPointsProjection(newproj);
//                AbstractMatrix projection = plsp.project(matrix, new Euclidean());
//
//                //creating the model
//                MusicProjectionModel finalmodel = new MusicProjectionModel();
//                ((ProjectionModel) finalmodel).changeColorScaleType(ColorScaleType.CATEGORY_SCALE);
//                Scalar cdata = ((ProjectionModel) finalmodel).addScalar(ProjectionConstants.CDATA);
//                Scalar dots = ((ProjectionModel) finalmodel).addScalar(ProjectionConstants.DOTS);
//                int nrows = projection.getRowCount();
//
//                for (int i = 0; i < nrows; i++) {
//                    AbstractVector row = projection.getRow(i);
//                    MusicProjectionInstance mpi = new MusicProjectionInstance(((ProjectionModel) finalmodel), row.getId(),
//                            row.getValue(0), row.getValue(1));
//                    mpi.setScalarValue(cdata, row.getKlass());
//                    mpi.setScalarValue(dots, 0);
//                }
//
//                finalmodel.changeColorScaleType(((ProjectionModel) model).getColorTable().getColorScaleType());
//
//                ProjectionFrameComp frame = new ProjectionFrameComp();
//                frame.setTitle("Complete Projection");
//                frame.setSize(new Dimension(800, 800));
//                frame.input(finalmodel);
//                frame.execute();
//            } catch (IOException ex) {
//                Logger.getLogger(IterativeProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
    }

    public static void main(String[] args) throws IOException {
        ProjectionUtil.log(false, false);
        IterativeProjectionFrameMusic frame = new IterativeProjectionFrameMusic();
        frame.setSize(800, 800);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);

    }
}
