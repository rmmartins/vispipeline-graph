/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.music;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


//The panel that will show the CHull class in action
/**
 *
 * @author paulovich
 */
public class ConvexHull {

    public ArrayList<Point> execute(ArrayList<Point> array) {
        Collections.sort(array, new Comparator<Point>() {

            @Override
            public int compare(Point pt1, Point pt2) {
                int r = pt1.x - pt2.x;
                if (r != 0) {
                    return r;
                } else {
                    return pt1.y - pt2.y;
                }
            }
        });

        return cHull(array);
    }
    
    private ArrayList<Point> cHull(ArrayList<Point> array) {
        int size = array.size();
        if (size < 2) {
            return null;
        }
        Point l = array.get(0);
        Point r = array.get(size - 1);
        ArrayList<Point> path = new ArrayList<Point>();
        path.add(l);
        cHull(array, l, r, path);
        path.add(r);
        cHull(array, r, l, path);
        return path;
    }

    //Returns the determinant of the point matrix
    //This determinant tells how far p3 is from vector p1p2 and on which side it is
    private int distance(Point p1, Point p2, Point p3) {
        int x1 = p1.x;
        int x2 = p2.x;
        int x3 = p3.x;
        int y1 = p1.y;
        int y2 = p2.y;
        int y3 = p3.y;
        return x1 * y2 + x3 * y1 + x2 * y3 - x3 * y2 - x2 * y1 - x1 * y3;
    }

    private void cHull(ArrayList<Point> points, Point l, Point r, ArrayList<Point> path) {
        if (points.size() < 3) {
            return;
        }
        int maxDist = Integer.MIN_VALUE;
        int tmp;
        Point p = null;
        for (Point pt : points) {
            if (pt != l && pt != r) {
                tmp = distance(l, r, pt);
                if (tmp > maxDist) {
                    maxDist = tmp;
                    p = pt;
                }
            }
        }
        ArrayList<Point> left = new ArrayList<Point>();
        ArrayList<Point> right = new ArrayList<Point>();
        left.add(l);
        right.add(p);
        for (Point pt : points) {
            if (distance(l, p, pt) > 0) {
                left.add(pt);
            } else if (distance(p, r, pt) > 0) {
                right.add(pt);
            }
        }
        left.add(p);
        right.add(r);
        cHull(left, l, p, path);
        path.add(p);
        cHull(right, p, r, path);
    }
}