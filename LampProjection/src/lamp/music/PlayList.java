/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.music;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import projection.model.XMLModelWriter;

/**
 *
 * @author paulovich
 */
public class PlayList {

    public PlayList(String name) {
        this.name = name;
        this.instances = new ArrayList<MusicProjectionInstance>();
        this.color = Color.BLUE;
    }

    public void draw(BufferedImage image, boolean highquality) {
        createConvexHullPolygon();

        if (hull != null) {
            BufferedImage hullimage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2hull = (Graphics2D) hullimage.getGraphics();
            Graphics2D g2image = (Graphics2D) image.getGraphics();

            if (highquality) {
                g2hull.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                g2image.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            }

            //create the image of the hull
            g2hull.setColor(color);
            g2hull.fillPolygon(hull);
            g2hull.setStroke(new BasicStroke(40, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
            g2hull.drawPolygon(hull);
            g2hull.dispose();

            //draw the hull image on the main image
            g2image.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.35f));
            g2image.drawImage(hullimage, null, 0, 0);
            g2image.dispose();
        }
    }

    public void addInstances(ArrayList<MusicProjectionInstance> newins) {
        instances.addAll(newins);
        hull = null;
    }

    public void addInstance(MusicProjectionInstance ins) {
        instances.add(ins);
        hull = null;
    }

    public String getName() {
        return name;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public ArrayList<MusicProjectionInstance> getInstances() {
        return instances;
    }

    public void saveM3U(String filename) throws IOException {
        BufferedWriter out = null;

        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "ISO-8859-1"));

            out.write("#EXTM3U\r\n");

            for (int i = 0; i < instances.size(); i++) {
                Music music = instances.get(i).getMusic();
                out.write("#EXTINF:123," + music.getArtist() + " - " + music.getTitle() + "\r\n");
                out.write(music.getFilename() + "\r\n");
            }
        } catch (IOException e) {
            throw new IOException("Problems writiing \"" + filename + "\"");
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    Logger.getLogger(XMLModelWriter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public String toString() {
        return name;
    }

    private void createConvexHullPolygon() {
        ArrayList<Point> points = new ArrayList<Point>();
        for (int i = 0; i < instances.size(); i++) {
            points.add(new Point((int) instances.get(i).getX(), (int) instances.get(i).getY()));
        }

        if (points.size() >= 3) {
            hull = new Polygon();
            ConvexHull chull = new ConvexHull();
            ArrayList<Point> hullpoints = chull.execute(points);


            for (int i = 0; i < hullpoints.size(); i++) {
                hull.addPoint(hullpoints.get(i).x, hullpoints.get(i).y);
            }
        }
    }
    private Polygon hull;
    private Color color;
    private String name;
    private ArrayList<MusicProjectionInstance> instances;
}
