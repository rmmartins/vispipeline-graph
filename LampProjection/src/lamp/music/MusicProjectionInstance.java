/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.music;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import javax.swing.ImageIcon;
import projection.model.ProjectionInstance;

/**
 *
 * @author paulovich
 */
public class MusicProjectionInstance extends ProjectionInstance {

    static {
        URL urlnote = MusicProjectionInstance.class.getResource("/lamp/images/notasmall6.png");
        MusicProjectionInstance.imagenote = (new ImageIcon(urlnote)).getImage();

        URL urlplay = MusicProjectionInstance.class.getResource("/lamp/images/play2.png");
        MusicProjectionInstance.imageplay = (new ImageIcon(urlplay)).getImage();
    }

    public MusicProjectionInstance(int id, float x, float y) {
        super(id, x, y);
    }

    public void setMusic(Music music) {
        this.music = music;
    }

    public Music getMusic() {
        return this.music;
    }

    @Override
    public void draw(BufferedImage image, boolean highquality) {
        Graphics2D g2 = (Graphics2D) image.getGraphics();

        if (highquality) {
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        }

        if (((MusicProjectionModel) model).isDrawAsNote()) {
            if (imagenote != null) {
                int w = imagenote.getWidth(null);
                int h = imagenote.getHeight(null);
                g2.drawImage(imagenote, ((int) x) - w / 2, ((int) y) - h / 2, null);

                if (selected) {
                    g2.setStroke(new BasicStroke(2.0f));
                    g2.setColor(Color.RED);
                    int radius = 18;
                    g2.drawOval(((int) x) - radius / 2, ((int) y) - radius / 2, radius, radius);
                }
            }

            if (showlabel) {
                drawLabel(g2, (int) x, (int) y);
            }
        } else {
            if (music != null) {
                String artist = music.getArtist().trim();
                if (artist.length() > 30) {
                    artist = artist.substring(0, 26) + "...";
                }

                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
                g2.setColor(new Color(200, 200, 255));
                g2.fillOval((int) this.x - 7, (int) this.y - 7, 14, 14);
                g2.setColor(new Color(100, 100, 255));
                g2.drawOval((int) this.x - 7, (int) this.y - 7, 14, 14);

                if (selected) {
                    g2.setStroke(new BasicStroke(2.0f));
                    g2.setColor(Color.RED);
                    g2.drawOval((int) this.x - 7, (int) this.y - 7, 14, 14);
                }

                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
                g2.setColor(Color.BLACK);
                g2.drawString(artist, (int) this.x, (int) this.y);
            }
        }

        g2.dispose();

    }

    @Override
    public void drawLabel(Graphics2D g2, int x, int y) {
        if (music != null) {
            String title = music.getTitle().trim();
            if (title.length() == 0) {
                title = (new File(music.getFilename())).getName();
            }

            if (title.length() > 30) {
                title = title.substring(0, 26) + "...";
            }

            String artist = music.getArtist().trim();
            if (artist.length() > 30) {
                artist = artist.substring(0, 26) + "...";
            }

            String album = music.getAlbum().trim();
            if (album.length() > 30) {
                album = album.substring(0, 26) + "...";
            }

            String larger = (title.length() > artist.length())
                    ? ((title.length() > album.length()) ? title : album)
                    : ((artist.length() > album.length()) ? artist : album);

            java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());
            int width = metrics.stringWidth(larger);
            int height = metrics.getAscent();

            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
            g2.setPaint(java.awt.Color.WHITE);
            g2.fillRect((int) this.x + 20, (int) this.y - 1 - height - 20, width + 15, 3 * (height + 1));
            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
            g2.setColor(java.awt.Color.DARK_GRAY);
            g2.drawRect((int) this.x + 20, (int) this.y - 1 - height - 20, width + 15, 3 * (height + 1));
            g2.drawString(title, (int) this.x + 23, (int) this.y - 20);
            g2.drawString(artist, (int) this.x + 23, (int) this.y + height - 20);
            g2.drawString(album, (int) this.x + 23, (int) this.y + 2 * height - 20);
        }

        int h = imageplay.getHeight(null);
        g2.drawImage(imageplay, (int) (this.x), (int) (this.y - h), null);
    }

    @Override
    public boolean isInside(int x, int y) {
        int w = imagenote.getWidth(null) / 2;
        int h = imagenote.getHeight(null) / 2;
        return (Math.abs((this.x - x)) <= w && Math.abs((this.y - y)) <= h);
    }

    @Override
    public boolean isInside(Rectangle rect) {
        return ((x >= rect.x) && (x <= rect.x + rect.width))
                && ((y >= rect.y) && (y <= rect.y + rect.height));
    }
    
    private static Image imagenote;
    private static Image imageplay;
    private Music music;
}
