/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.music;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PC
 */
public class MP3FeaturesDataset {

    public void load(String xmlFilename) {
        try {
            MP3FeatureParser reader = new MP3FeatureParser();
            matrix = reader.read(xmlFilename);
        } catch (IOException ex) {
            Logger.getLogger(MP3FeaturesDataset.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public float[] getFeatures(String filename) {
        assert (matrix != null) : "features nor readed";
        return matrix.get(filename);
    }
    
    HashMap<String, float[]> matrix = null;
}
