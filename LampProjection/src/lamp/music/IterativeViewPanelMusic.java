package lamp.music;

import java.util.logging.Level;
import java.util.logging.Logger;
import javazoom.jl.decoder.JavaLayerException;
import lamp.repositioning.IterativeViewPanel;
import projection.model.ProjectionModel;

public class IterativeViewPanelMusic extends IterativeViewPanel {

    public IterativeViewPanelMusic(IterativeProjectionFrameMusic frame) {
        super(frame);
    }

    @Override
    protected void mouseMoved(java.awt.event.MouseEvent evt) {
        if (model != null) {
            MusicProjectionInstance ins = (MusicProjectionInstance) ((ProjectionModel) model).getInstanceByPosition(evt.getPoint());

            if (ins != null) {
                if (ins != labelins) {
                    MusicPlayer.getInstance().stop();
                    
                    Music music = ins.getMusic();
                    if (music != null) {
                        try {
                            MusicPlayer.getInstance().play(music, 1000);
                        } catch (JavaLayerException ex) {
                            Logger.getLogger(IterativeViewPanelMusic.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

                    labelins = ins;
                    labelpos = evt.getPoint();
                    repaint();
                }                
            } else {
                MusicPlayer.getInstance().stop();

                if (labelins != null) {
                    labelpos = null;
                    labelins = null;
                    repaint();
                }
            }
        }
    }
}
