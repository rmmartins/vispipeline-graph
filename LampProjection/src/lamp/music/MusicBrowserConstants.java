/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lamp.music;

/**
 *
 * @author PC
 */
public interface MusicBrowserConstants {

    public static final String PROP_FILE = "musicbrowser.properties";
    public static final String XML_FILE = "XML.FILE";
    public static final String FEATURE_FILE = "FEATURE.FILE";
    public static final String FEATURE_MATRIX_FILE = "FEATURE.MATRIX.FILE";

}
