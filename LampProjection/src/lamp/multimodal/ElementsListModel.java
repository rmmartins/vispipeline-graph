/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lamp.multimodal;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import lamp.music.MusicProjectionInstance;
import lamp.music.MusicProjectionModel;
import lamp.repositioning.image.ImageProjectionInstance;
import projection.model.ProjectionInstance;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.model.AbstractModel;

/**
 *
 * @author paulovich
 */
public class ElementsListModel extends AbstractModel {

    public ElementsListModel() {
        lists = new ArrayList<ElementsList>();
    }

    public void addInstancesList(ElementsList list) {
        lists.add(list);
        setChanged();
    }

    public void clear() {
        lists.clear();
        setChanged();
    }

    public ArrayList<ElementsList> getElementsLists() {
        return lists;
    }

    public void draw(BufferedImage image, boolean highquality) {
        if (image != null) {
            for (ElementsList p : lists) {
                p.draw(image, highquality);
            }
        }
    }

    public void generateElementsLists(ElementsListModel samplemodel,
            MusicProjectionModel musicmodel, ImageProjectionModel imagemodel) {
        ArrayList<ElementsList> samplelists = samplemodel.getElementsLists();

        for (int i = 0; i < samplelists.size(); i++) {
            ElementsList list = samplelists.get(i);

            ArrayList<ImageProjectionInstance> insimages = list.getImageInstances();
            ArrayList<MusicProjectionInstance> insmusics = list.getMusicInstances();

            //calculating the max distance between all points in a list
            ArrayList<ProjectionInstance> sampleinstances = new ArrayList<ProjectionInstance>();
            sampleinstances.addAll(insimages);
            sampleinstances.addAll(insmusics);

            float max_dist = Float.NEGATIVE_INFINITY;
            for (int m = 0; m < sampleinstances.size(); m++) {
                float x1 = sampleinstances.get(m).getX();
                float y1 = sampleinstances.get(m).getY();

                for (int n = m + 1; n < sampleinstances.size(); n++) {
                    float x2 = sampleinstances.get(n).getX();
                    float y2 = sampleinstances.get(n).getY();

                    float dist = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

                    if (max_dist < dist) {
                        max_dist = dist;
                    }
                }
            }

            //the threshold used to add the musics+images to the final list
            float threshold = max_dist * 0.25f;

            //creating the new list
            ElementsList newlist = new ElementsList(list.getName());
            newlist.setColor(list.getColor());

            //adding musics
            if (musicmodel != null) {
                ArrayList<AbstractInstance> musicmodelins = musicmodel.getInstances();
                ArrayList<MusicProjectionInstance> newinsmusics = new ArrayList<MusicProjectionInstance>();
                HashSet<Integer> unique_ids = new HashSet<Integer>();

                for (int m = 0; m < musicmodelins.size(); m++) {
                    float x1 = ((ProjectionInstance) musicmodelins.get(m)).getX();
                    float y1 = ((ProjectionInstance) musicmodelins.get(m)).getY();

                    for (int n = 0; n < sampleinstances.size(); n++) {
                        float x2 = sampleinstances.get(n).getX();
                        float y2 = sampleinstances.get(n).getY();

                        float dist = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

                        if (dist < threshold) {
                            if (!unique_ids.contains(musicmodelins.get(m).getId())) {
                                newinsmusics.add((MusicProjectionInstance) musicmodelins.get(m));
                                unique_ids.add(musicmodelins.get(m).getId());
                            }
                        }
                    }
                }
                newlist.addMusicInstances(newinsmusics);
            }

            //adding images
            if (imagemodel != null) {
                ArrayList<AbstractInstance> imagemodelins = imagemodel.getInstances();
                ArrayList<ImageProjectionInstance> newinsimages = new ArrayList<ImageProjectionInstance>();
                HashSet<Integer> unique_ids = new HashSet<Integer>();
                for (int m = 0; m < imagemodelins.size(); m++) {
                    float x1 = ((ProjectionInstance) imagemodelins.get(m)).getX();
                    float y1 = ((ProjectionInstance) imagemodelins.get(m)).getY();

                    for (int n = 0; n < sampleinstances.size(); n++) {
                        float x2 = sampleinstances.get(n).getX();
                        float y2 = sampleinstances.get(n).getY();

                        float dist = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

                        if (dist < threshold) {
                            if (!unique_ids.contains(imagemodelins.get(m).getId())) {
                                newinsimages.add((ImageProjectionInstance) imagemodelins.get(m));
                                unique_ids.add(imagemodelins.get(m).getId());
                            }
                        }
                    }
                }
                newlist.addImageInstances(newinsimages);
            }

            this.addInstancesList(newlist);
        }
    }

    private ArrayList<ElementsList> lists;
}
