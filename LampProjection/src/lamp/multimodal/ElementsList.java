/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lamp.multimodal;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import lamp.music.ConvexHull;
import lamp.music.MusicProjectionInstance;
import lamp.repositioning.image.ImageProjectionInstance;

/**
 *
 * @author paulovich
 */
public class ElementsList {

     public ElementsList(String name) {
        this.name = name;
        this.insmusic = new ArrayList<MusicProjectionInstance>();
        this.insimage = new ArrayList<ImageProjectionInstance>();
        this.color = Color.BLUE;
    }

    public void draw(BufferedImage image, boolean highquality) {
        createConvexHullPolygon();

        if (hull != null) {
            BufferedImage hullimage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2hull = (Graphics2D) hullimage.getGraphics();
            Graphics2D g2image = (Graphics2D) image.getGraphics();

            if (highquality) {
                g2hull.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                g2image.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            }

            //create the image of the hull
            g2hull.setColor(color);
            g2hull.fillPolygon(hull);
            g2hull.setStroke(new BasicStroke(40, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
            g2hull.drawPolygon(hull);
            g2hull.dispose();

            //draw the hull image on the main image
            g2image.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.35f));
            g2image.drawImage(hullimage, null, 0, 0);
            g2image.dispose();
        }
    }

    public void addMusicInstances(ArrayList<MusicProjectionInstance> newins) {
        insmusic.addAll(newins);
        hull = null;
    }
    
    public void addImageInstances(ArrayList<ImageProjectionInstance> newins) {
        insimage.addAll(newins);
        hull = null;
    }

    public String getName() {
        return name;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public ArrayList<MusicProjectionInstance> getMusicInstances() {
        return insmusic;
    }

    public ArrayList<ImageProjectionInstance> getImageInstances() {
        return insimage;
    }
    
    @Override
    public String toString() {
        return name;
    }

    private void createConvexHullPolygon() {
        ArrayList<Point> points = new ArrayList<Point>();
        for (int i = 0; i < insmusic.size(); i++) {
            points.add(new Point((int) insmusic.get(i).getX(), (int) insmusic.get(i).getY()));
        }

        for (int i = 0; i < insimage.size(); i++) {
            points.add(new Point((int) insimage.get(i).getX(), (int) insimage.get(i).getY()));
        }

        if (points.size() >= 3) {
            hull = new Polygon();
            ConvexHull chull = new ConvexHull();
            ArrayList<Point> hullpoints = chull.execute(points);

            for (int i = 0; i < hullpoints.size(); i++) {
                hull.addPoint(hullpoints.get(i).x, hullpoints.get(i).y);
            }
        }
    }

    private Polygon hull;
    private Color color;
    private String name;
    private ArrayList<MusicProjectionInstance> insmusic;
    private ArrayList<ImageProjectionInstance> insimage;
}
