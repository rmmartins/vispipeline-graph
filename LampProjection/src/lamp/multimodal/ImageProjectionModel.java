/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lamp.multimodal;

import java.awt.Dimension;
import lamp.util.LampConstants;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;

/**
 *
 * @author paulovich
 */
public class ImageProjectionModel extends ProjectionModel  {

    @Override
    public void fitToSize(Dimension size) {
        float maxx = Float.NEGATIVE_INFINITY;
        float minx = Float.POSITIVE_INFINITY;
        float maxy = Float.NEGATIVE_INFINITY;
        float miny = Float.POSITIVE_INFINITY;

        for (int i = 0; i < instances.size(); i++) {
            ProjectionInstance pi = (ProjectionInstance) instances.get(i);

            if (maxx < pi.getX()) {
                maxx = pi.getX();
            }

            if (minx > pi.getX()) {
                minx = pi.getX();
            }

            if (maxy < pi.getY()) {
                maxy = pi.getY();
            }

            if (miny > pi.getY()) {
                miny = pi.getY();
            }
        }

        float begin = LampConstants.min;
        float endy = 0.0f;
        float endx = 0.0f;

        if (maxy > maxx) {
            endy = Math.min(size.width, size.height) - begin;

            if (maxy != miny) {
                endx = ((maxx - minx) * endy) / (maxy - miny);
            } else {
                endx = ((maxx - minx) * endy);
            }
        } else {
            endx = Math.min(size.width, size.height) - begin;

            if (maxx != minx) {
                endy = ((maxy - miny) * endx) / (maxx - minx);
            } else {
                endy = ((maxy - miny) * endx);
            }
        }

        for (int i = 0; i < instances.size(); i++) {
            ProjectionInstance pi = (ProjectionInstance) instances.get(i);

            if (maxx != minx) {
                pi.setX((((pi.getX() - minx) / (maxx - minx)) *
                        (endx - begin)) + begin);
            } else {
                pi.setX(begin);
            }

            if (maxy != miny) {
                pi.setY(((((pi.getY() - miny) / (maxy - miny)) *
                        (endy - begin)) + begin));
            } else {
                pi.setY(begin);
            }
        }

        setChanged();
    }

}
