
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.multimodal;

import datamining.clustering.BKmeans;
import distance.dissimilarity.AbstractDissimilarity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import lamp.music.Music;
import lamp.repositioning.image.ImageInstance;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;

/**
 *
 * @author PC
 */
public class ControlPoints {

    public static ArrayList<Integer> createMusicControlpoints(AbstractMatrix matrix,
            AbstractDissimilarity diss, ArrayList<Music> initialseeds, int nrneigh) throws IOException {
        ArrayList<Integer> seeds_aux = new ArrayList<Integer>();
        HashSet<Integer> seeds_aux_unique = new HashSet<Integer>();
        int nrinitialseeds = initialseeds.size();

        //adding the initial seeds to the set
        for (int i = 0; i < nrinitialseeds; i++) {
            int id = initialseeds.get(i).getId();

            for (int j = 0; j < matrix.getRowCount(); j++) {
                if (matrix.getRow(j).getId() == id) {
                    seeds_aux.add(j);
                    seeds_aux_unique.add(j);
                    break;
                }
            }
        }

        //adding the most similar other seeds
        for (int i = 0; i < nrinitialseeds; i++) {
            int seedindex = seeds_aux.get(i);
            float[] dists = new float[nrneigh - 1];
            int[] ids = new int[nrneigh - 1];

            Arrays.fill(dists, Float.POSITIVE_INFINITY);
            Arrays.fill(ids, -1);

            for (int j = 0; j < matrix.getRowCount(); j++) {
                if (!seeds_aux_unique.contains(j)) {
                    float dist = diss.calculate(matrix.getRow(seedindex), matrix.getRow(j));
                    int index = 0;

                    while (index < dists.length) {
                        if (dist < dists[index]) {
                            break;
                        }
                        index++;
                    }

                    if (index < dists.length) {
                        for (int k = dists.length - 1; k > index; k--) {
                            dists[k] = dists[k - 1];
                            ids[k] = ids[k - 1];
                        }

                        dists[index] = dist;
                        ids[index] = j;
                    }
                }
            }

            for (int j = 0; j < ids.length; j++) {
                seeds_aux_unique.add(ids[j]);
            }
        }

        //cluster the remaning points and get the medoids
        AbstractMatrix partialmatrix = MatrixFactory.getInstance(matrix.getClass());
        HashMap<Integer, Integer> mapping = new HashMap<Integer, Integer>();
        for (int i = 0; i < matrix.getRowCount(); i++) {
            if (!seeds_aux_unique.contains(i)) {
                mapping.put(partialmatrix.getRowCount(), i);
                partialmatrix.addRow(matrix.getRow(i));
            }
        }

        int nrclusters = (nrneigh * nrinitialseeds);//Math.max(25, nrneigh * nrinitialseeds);
        BKmeans kmeans = new BKmeans(nrclusters);
        kmeans.execute(diss, partialmatrix);
        int[] medoids = kmeans.getMedoids(partialmatrix);
        for (int j = 0; j < medoids.length; j++) {
            seeds_aux_unique.add(mapping.get(medoids[j]));
        }

        //creating the seeds data
        seeds_aux = new ArrayList<Integer>();
        Iterator<Integer> it = seeds_aux_unique.iterator();
        while (it.hasNext()) {
            int id = it.next();
            seeds_aux.add(id);
        }

        return seeds_aux;
    }

    public static ArrayList<Integer> createImageControlpoints(AbstractMatrix matrix,
            AbstractDissimilarity diss, ArrayList<ImageInstance> initialseeds, int nrneigh) throws IOException {

        ////////////////////////////////////////////////
        //getting the control points using clustering
        int nr_control_points = (int) Math.sqrt(matrix.getRowCount());
        BKmeans bkmeans = new BKmeans(nr_control_points);
        bkmeans.execute(diss, matrix);
        int[] medoids = bkmeans.getMedoids(matrix);

        ArrayList<Integer> controlpoints = new ArrayList<Integer>();
        for (int i = 0; i < medoids.length; i++) {
            controlpoints.add(medoids[i]);
        }

        return controlpoints;
        ////////////////////////////////////////////////

//                    ////////////////////////////////////////////////
//                    int nrel_perclass = 3;
//                    cpmatriximage = MatrixFactory.getInstance(matriximage.getClass());
//                    HashMap<Float, Integer> klass = new HashMap<Float, Integer>();
//                    for (int i = 0; i < matriximage.getRowCount(); i++) {
//                        AbstractVector row = matriximage.getRow(i);
//
//                        if (!klass.containsKey(row.getKlass())) {
//                            klass.put(row.getKlass(), 0);
//                        } else {
//                            if (klass.get(row.getKlass()) < nrel_perclass) {
//                                klass.put(row.getKlass(), klass.get(row.getKlass()) + 1);
//                                cpmatriximage.addRow(row, matriximage.getLabel(i));
//                            }
//                        }
//                    }
//                    ////////////////////////////////////////////////


//        ////////////////////////////////////////////////
//        ArrayList<Integer> seeds_aux = new ArrayList<Integer>();
//        HashSet<Integer> seeds_aux_unique = new HashSet<Integer>();
//        int nrinitialseeds = initialseeds.size();
//
//        //adding the initial seeds to the set
//        for (int i = 0; i < nrinitialseeds; i++) {
//            String url = initialseeds.get(i).getUrl();
//
//            for (int j = 0; j < matrix.getRowCount(); j++) {
//                if (matrix.getLabel(j).equalsIgnoreCase(url)) {
//                    seeds_aux.add(j);
//                    seeds_aux_unique.add(j);
//                    break;
//                }
//            }
//        }
//
//        //adding the most similar other seeds
//        for (int i = 0; i < nrinitialseeds; i++) {
//            int seedindex = seeds_aux.get(i);
//            float[] dists = new float[nrneigh - 1];
//            int[] ids = new int[nrneigh - 1];
//
//            Arrays.fill(dists, Float.POSITIVE_INFINITY);
//            Arrays.fill(ids, -1);
//
//            for (int j = 0; j < matrix.getRowCount(); j++) {
//                if (!seeds_aux_unique.contains(j)) {
//                    float dist = diss.calculate(matrix.getRow(seedindex), matrix.getRow(j));
//                    int index = 0;
//
//                    while (index < dists.length) {
//                        if (dist < dists[index]) {
//                            break;
//                        }
//                        index++;
//                    }
//
//                    if (index < dists.length) {
//                        for (int k = dists.length - 1; k > index; k--) {
//                            dists[k] = dists[k - 1];
//                            ids[k] = ids[k - 1];
//                        }
//
//                        dists[index] = dist;
//                        ids[index] = j;
//                    }
//                }
//            }
//
//            for (int j = 0; j < ids.length; j++) {
//                seeds_aux_unique.add(ids[j]);
//            }
//        }
//
//        //cluster the remaning points and get the medoids
//        AbstractMatrix partialmatrix = MatrixFactory.getInstance(matrix.getClass());
//        HashMap<Integer, Integer> mapping = new HashMap<Integer, Integer>();
//        for (int i = 0; i < matrix.getRowCount(); i++) {
//            if (!seeds_aux_unique.contains(i)) {
//                mapping.put(partialmatrix.getRowCount(), i);
//                partialmatrix.addRow(matrix.getRow(i));
//            }
//        }
//
//        int nrclusters = nrneigh * nrinitialseeds;//Math.max(25, nrneigh * nrinitialseeds);
//        BKmeans kmeans = new BKmeans(nrclusters);
//        kmeans.execute(diss, partialmatrix);
//        int[] medoids = kmeans.getMedoids(partialmatrix);
//        for (int j = 0; j < medoids.length; j++) {
//            seeds_aux_unique.add(mapping.get(medoids[j]));
//        }
//
//        //creating the seeds data
//        seeds_aux = new ArrayList<Integer>();
//        Iterator<Integer> it = seeds_aux_unique.iterator();
//        while (it.hasNext()) {
//            int id = it.next();
//            seeds_aux.add(id);
//        }
//
//        return seeds_aux;
    }
}
