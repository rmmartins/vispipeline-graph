
package lamp.multimodal;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import lamp.music.MusicPlayer;
import lamp.music.MusicProjectionInstance;
import lamp.music.MusicProjectionModel;
import lamp.repositioning.image.ImageProjectionInstance;
import lamp.util.LampConstants;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.selection.AbstractSelection;

public class ViewPanelMultimodal extends JPanel implements Observer {

    public ViewPanelMultimodal(MultimodalBrowser browser) {
        this.browser = browser;
        this.selcolor = java.awt.Color.RED;
        this.setBackground(java.awt.Color.WHITE);

        this.modellist = new ElementsListModel();
        this.modellist.addObserver(this);
        this.modellist.addObserver(browser);

        this.addMouseMotionListener(new MouseMotionAdapter() {

            @Override
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                super.mouseMoved(evt);
                ViewPanelMultimodal.this.mouseMoved(evt);
            }

            @Override
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                super.mouseDragged(evt);
                ViewPanelMultimodal.this.mouseDragged(evt);
            }

        });

        this.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                super.mouseClicked(evt);
                ViewPanelMultimodal.this.mouseClicked(evt);
            }

            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                super.mousePressed(evt);
                ViewPanelMultimodal.this.mousePressed(evt);
            }

            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                super.mouseReleased(evt);
                ViewPanelMultimodal.this.mouseReleased(evt);
            }

        });
    }

    @Override
    public void paintComponent(java.awt.Graphics g) {
        super.paintComponent(g);

        java.awt.Graphics2D g2 = (java.awt.Graphics2D) g;

        if (image == null) {
            Dimension size = calculateMaxSize();

            image = new BufferedImage(size.width + LampConstants.min * 5,
                    size.height + LampConstants.min * 5,
                    BufferedImage.TYPE_INT_RGB);

            java.awt.Graphics2D g2Buffer = image.createGraphics();
            g2Buffer.setColor(this.getBackground());
            g2Buffer.fillRect(0, 0,
                    size.width + LampConstants.min * 5,
                    size.height + LampConstants.min * 5);

            g2Buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);

            //draw the musics
            if (modelmusic != null) {
                modelmusic.draw(image, true);
            }

            //draw the images
            if (modelimage != null) {
                modelimage.draw(image, true);
            }

            if (modellist != null) {
                modellist.draw(image, true);
            }

            g2Buffer.dispose();
        }

        if (image != null) {
            g2.drawImage(image, 0, 0, null);
        }

        //Draw he rectangle to select the instances
        if (selsource != null && seltarget != null) {
            int x = selsource.x;
            int width = width = seltarget.x - selsource.x;

            int y = selsource.y;
            int height = seltarget.y - selsource.y;

            if (selsource.x > seltarget.x) {
                x = seltarget.x;
                width = selsource.x - seltarget.x;
            }

            if (selsource.y > seltarget.y) {
                y = seltarget.y;
                height = selsource.y - seltarget.y;
            }

            g2.setColor(selcolor);
            g2.drawRect(x, y, width, height);

            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.45f));
            g2.fillRect(x, y, width, height);
        } else { //Draw the instance label
            if (labelinsmusic != null && labelposmusic != null && !labelinsmusic.isSelected()) {
                labelinsmusic.drawLabel(g2, labelposmusic.x, labelposmusic.y);
            }

            if (labelinsimage != null && labelposimage != null && !labelinsimage.isSelected()) {
                labelinsimage.drawLabel(g2, labelposimage.x, labelposimage.y);
            }
        }

        //drawn the selection polygon
        if (selpolygon != null) {
            g2.setColor(selcolor);
            g2.drawPolygon(selpolygon);

            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.45f));
            g2.fillPolygon(selpolygon);
        }
    }

    public void setMusicProjectionModel(MusicProjectionModel model) {
        if (this.modelmusic != null) {
            this.modelmusic.deleteObserver(this);
        }

//        this.modellist.clear();
        this.modelmusic = model;

        if (model != null) {
            model.addObserver(this);
            model.addObserver(browser);
        }

        Dimension size = calculateMaxSize();
        setPreferredSize(new Dimension(size.width * 2, size.height * 2));
        setSize(new Dimension(size.width * 2, size.height * 2));

        cleanImage();
        repaint();
    }

    public void setImageProjectionModel(ImageProjectionModel model) {
        if (this.modelimage != null) {
            this.modelimage.deleteObserver(this);
        }

//        this.modellist.clear();
        this.modelimage = model;

        if (model != null) {
            model.addObserver(this);
            model.addObserver(browser);
        }

        Dimension size = calculateMaxSize();
        setPreferredSize(new Dimension(size.width * 2, size.height * 2));
        setSize(new Dimension(size.width * 2, size.height * 2));

        cleanImage();
        repaint();
    }

    public ElementsListModel getElementsListModel() {
        return modellist;
    }

    public ImageProjectionModel getImageProjectionModel() {
        return modelimage;
    }

    public MusicProjectionModel getMusicProjectionModel() {
        return modelmusic;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (modelmusic != null || modelimage != null) {
            cleanImage();
            repaint();
        }
    }

    public AbstractMatrix getMusicProjection() {
        DenseMatrix projection = new DenseMatrix();

        if (modelmusic != null) {
            ArrayList<AbstractInstance> instances = modelmusic.getInstances();

            for (int i = 0; i < instances.size(); i++) {
                ProjectionInstance pi = (ProjectionInstance) instances.get(i);

                float[] vector = new float[2];
                vector[0] = pi.getX();
                vector[1] = pi.getY();

                projection.addRow(new DenseVector(vector, pi.getId(), 0));
            }
        }

        return projection;
    }

    public AbstractMatrix getImageProjection() {
        DenseMatrix projection = new DenseMatrix();

        if (modelimage != null) {
            ArrayList<AbstractInstance> instances = modelimage.getInstances();

            for (int i = 0; i < instances.size(); i++) {
                ProjectionInstance pi = (ProjectionInstance) instances.get(i);

                float[] vector = new float[2];
                vector[0] = pi.getX();
                vector[1] = pi.getY();

                projection.addRow(new DenseVector(vector, pi.getId(), 0));
            }
        }

        return projection;
    }

    public void cleanImage() {
        image = null;
    }

    public void fitScreen() {
        float minx = Float.POSITIVE_INFINITY;
        float miny = Float.POSITIVE_INFINITY;

        if (modelimage != null) {
            ArrayList<AbstractInstance> instances = modelimage.getInstances();

            for (int i = 0; i < instances.size(); i++) {
                ProjectionInstance ins = (ProjectionInstance) instances.get(i);

                if (minx > ins.getX()) {
                    minx = ins.getX();
                }

                if (miny > ins.getY()) {
                    miny = ins.getY();
                }
            }
        }

        if (modelmusic != null) {
            ArrayList<AbstractInstance> instances = modelmusic.getInstances();

            for (int i = 0; i < instances.size(); i++) {
                ProjectionInstance ins = (ProjectionInstance) instances.get(i);

                if (minx > ins.getX()) {
                    minx = ins.getX();
                }

                if (miny > ins.getY()) {
                    miny = ins.getY();
                }
            }
        }

        if (modelimage != null) {
            ArrayList<AbstractInstance> instances = modelimage.getInstances();

            for (int i = 0; i < instances.size(); i++) {
                ProjectionInstance ins = (ProjectionInstance) instances.get(i);
                ins.setX(ins.getX() - minx + LampConstants.min);
                ins.setY(ins.getY() - miny + LampConstants.min);
            }
        }

        if (modelmusic != null) {
            ArrayList<AbstractInstance> instances = modelmusic.getInstances();

            for (int i = 0; i < instances.size(); i++) {
                ProjectionInstance ins = (ProjectionInstance) instances.get(i);
                ins.setX(ins.getX() - minx + LampConstants.min);
                ins.setY(ins.getY() - miny + LampConstants.min);
            }
        }
    }

    public void saveToPngImageFile(String filename) throws IOException {
        try {
            Dimension size = calculateMaxSize();
            BufferedImage buffer = new BufferedImage(size.width + LampConstants.min * 5,
                    size.height + LampConstants.min * 5,
                    BufferedImage.TYPE_INT_RGB);
            paint(buffer.getGraphics());
            ImageIO.write(buffer, "png", new File(filename));
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void zoomIn() {
        if (modelmusic != null) {
            modelmusic.zoom(1.1f);
        }

        if (modelimage != null) {
            modelimage.zoom(1.1f);
        }

        if (modelmusic != null || modelimage != null) {
            Dimension size = calculateMaxSize();
            setPreferredSize(new Dimension(size.width * 2, size.height * 2));
            setSize(new Dimension(size.width * 2, size.height * 2));

            if (modelmusic != null) {
                modelmusic.notifyObservers();
            }

            if (modelimage != null) {
                modelimage.notifyObservers();
            }
        }
    }

    public void zoomOut() {
        if (modelmusic != null) {
            modelmusic.zoom(0.9091f);
        }

        if (modelimage != null) {
            modelimage.zoom(0.9091f);
        }

        if (modelmusic != null || modelimage != null) {
            Dimension size = calculateMaxSize();
            setPreferredSize(new Dimension(size.width * 2, size.height * 2));
            setSize(new Dimension(size.width * 2, size.height * 2));

            if (modelmusic != null) {
                modelmusic.notifyObservers();
            }

            if (modelimage != null) {
                modelimage.notifyObservers();
            }
        }
    }

    public void adjustPanel() {
        float max_x = Float.NEGATIVE_INFINITY, max_y = Float.NEGATIVE_INFINITY;
        float min_x = Float.POSITIVE_INFINITY, min_y = Float.POSITIVE_INFINITY;
        int zero = LampConstants.min;

        //getting the max/min of music instances
        if (modelmusic != null) {
            for (int i = 0; i < modelmusic.getInstances().size(); i++) {
                float x = ((ProjectionInstance) modelmusic.getInstances().get(i)).getX();
                if (max_x < x) {
                    max_x = x;
                }

                if (min_x > x) {
                    min_x = x;
                }

                float y = ((ProjectionInstance) modelmusic.getInstances().get(i)).getY();
                if (max_y < y) {
                    max_y = y;
                }

                if (min_y > y) {
                    min_y = y;
                }
            }
        }

        //getting the max/min of music instances
        if (modelimage != null) {
            for (int i = 0; i < modelimage.getInstances().size(); i++) {
                float x = ((ProjectionInstance) modelimage.getInstances().get(i)).getX();
                if (max_x < x) {
                    max_x = x;
                }

                if (min_x > x) {
                    min_x = x;
                }

                float y = ((ProjectionInstance) modelimage.getInstances().get(i)).getY();
                if (max_y < y) {
                    max_y = y;
                }

                if (min_y > y) {
                    min_y = y;
                }
            }
        }

        if (modelmusic != null) {
            for (AbstractInstance ai : modelmusic.getInstances()) {
                ProjectionInstance pi = (ProjectionInstance) ai;
                pi.setX(pi.getX() + zero - min_x);
                pi.setY(pi.getY() + zero - min_y);
            }
        }

        if (modelimage != null) {
            for (AbstractInstance ai : modelimage.getInstances()) {
                ProjectionInstance pi = (ProjectionInstance) ai;
                pi.setX(pi.getX() + zero - min_x);
                pi.setY(pi.getY() + zero - min_y);
            }
        }

        Dimension d = this.getSize();
        d.width = (int) max_x + zero;
        d.height = (int) max_y + zero;
        this.setSize(d);
        this.setPreferredSize(d);

        if (modelmusic != null) {
            modelmusic.notifyObservers();
        }

        if (modelimage != null) {
            modelimage.notifyObservers();
        }
    }

    public ArrayList<ProjectionInstance> getSelectedMusicInstances(Polygon polygon) {
        ArrayList<ProjectionInstance> selected = new ArrayList<ProjectionInstance>();

        if (modelmusic != null) {
            selected = ((ProjectionModel) modelmusic).getInstancesByPosition(polygon);
        }

        return selected;
    }

    public ArrayList<ProjectionInstance> getSelectedImageInstances(Polygon polygon) {
        ArrayList<ProjectionInstance> selected = new ArrayList<ProjectionInstance>();

        if (modelimage != null) {
            selected = ((ProjectionModel) modelimage).getInstancesByPosition(polygon);
        }

        return selected;
    }

    public ArrayList<ProjectionInstance> getSelectedMusicInstances(Point source, Point target) {
        ArrayList<ProjectionInstance> selinstances = new ArrayList<ProjectionInstance>();

        if (modelmusic != null) {
            int x = Math.min(source.x, target.x);
            int width = Math.abs(source.x - target.x);

            int y = Math.min(source.y, target.y);
            int height = Math.abs(source.y - target.y);

            Rectangle rect = new Rectangle(x, y, width, height);
            selinstances = ((ProjectionModel) modelmusic).getInstancesByPosition(rect);
        }

        return selinstances;
    }

    public ArrayList<ProjectionInstance> getSelectedImageInstances(Point source, Point target) {
        ArrayList<ProjectionInstance> selinstances = new ArrayList<ProjectionInstance>();

        if (modelimage != null) {
            int x = Math.min(source.x, target.x);
            int width = Math.abs(source.x - target.x);

            int y = Math.min(source.y, target.y);
            int height = Math.abs(source.y - target.y);

            Rectangle rect = new Rectangle(x, y, width, height);
            selinstances = ((ProjectionModel) modelimage).getInstancesByPosition(rect);
        }

        return selinstances;
    }

    ///////////////////////////////////////////////////////////
    /*Music model mouse methods*/
    ///////////////////////////////////////////////////////////
    ///////////////////////
    //mouse motion methods
    protected void mouseMoved(java.awt.event.MouseEvent evt) {
        if (MultimodalOptions.isMoveMusics() && modelmusic != null) {
            MusicProjectionInstance insmusic = (MusicProjectionInstance) ((ProjectionModel) modelmusic).getInstanceByPosition(evt.getPoint());

            if (insmusic != null) {
                if (insmusic != labelinsmusic) {
                    MusicPlayer.getInstance().stop();

//                    Music music = insmusic.getMusic();
//                    if (music != null) {
//                        try {
//                            MultimodalBrowser.setStatus("Playing: " + music.getTitle() + " (" + music.getArtist() + ")");
//                            MusicPlayer.getInstance().play(music, 500);
//                        } catch (JavaLayerException ex) {
//                            Logger.getLogger(ViewPanelMultimodal.class.getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }

                    labelinsmusic = insmusic;
                    labelposmusic = evt.getPoint();
                    repaint();
                }
            } else {
                MultimodalBrowser.setStatus("Stopped");
                MusicPlayer.getInstance().stop();

                if (labelinsmusic != null) {
                    labelposmusic = null;
                    labelinsmusic = null;
                    repaint();
                }
            }
        }

        if (MultimodalOptions.isMoveImages() && modelimage != null) {
            labelinsimage = ((ProjectionModel) modelimage).getInstanceByPosition(evt.getPoint());

            if (labelinsimage != null) {
                labelposimage = evt.getPoint();
                repaint();
            } else {
                if (labelposimage != null) {
                    labelposimage = null;
                    repaint();
                }
            }
        }
    }

    protected void mouseDragged(java.awt.event.MouseEvent evt) {
        if (selinstance != null) {
            float x = evt.getX() - selinstance.getX();
            float y = evt.getY() - selinstance.getY();

            if (MultimodalOptions.isMoveMusics() && modelmusic != null) {
                if (modelmusic.hasSelectedInstances()) {
                    for (AbstractInstance ai : modelmusic.getSelectedInstances()) {
                        ProjectionInstance pi = (ProjectionInstance) ai;
                        pi.setX(x + pi.getX());
                        pi.setY(y + pi.getY());
                    }
                }
            }

            if (MultimodalOptions.isMoveImages() && modelimage != null) {
                if (modelimage.hasSelectedInstances()) {
                    for (AbstractInstance ai : modelimage.getSelectedInstances()) {
                        ProjectionInstance pi = (ProjectionInstance) ai;
                        pi.setX(x + pi.getX());
                        pi.setY(y + pi.getY());
                    }
                }
            }

            adjustPanel();
        } else if (selsource != null) {
            seltarget = evt.getPoint();
        } else if (selpolygon != null) {
            selpolygon.addPoint(evt.getX(), evt.getY());
        }

        repaint();
    }

    //////////////////
    //mouse  methods
    protected void mouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
            if (MultimodalOptions.isMoveMusics() && modelmusic != null) {
                ProjectionInstance instance = ((ProjectionModel) modelmusic).getInstanceByPosition(evt.getPoint());
                if (instance != null) {
                    modelmusic.setSelectedInstance(instance);
                    modelmusic.notifyObservers();
                }
            }

            if (MultimodalOptions.isMoveImages() && modelimage != null) {
                ProjectionInstance instance = ((ProjectionModel) modelimage).getInstanceByPosition(evt.getPoint());
                if (instance != null) {
                    modelimage.setSelectedInstance(instance);
                    modelimage.notifyObservers();
                }
            }
        }
    }

    public void mousePressed(java.awt.event.MouseEvent evt) {
        if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
            ProjectionInstance insmusic = null;
            ProjectionInstance insimage = null;

            if (MultimodalOptions.isMoveMusics() && modelmusic != null) {
                insmusic = ((ProjectionModel) modelmusic).getInstanceByPosition(evt.getPoint());
            }

            if (MultimodalOptions.isMoveImages() && modelimage != null) {
                insimage = ((ProjectionModel) modelimage).getInstanceByPosition(evt.getPoint());
            }

            if (insmusic != null) {
                if (modelmusic.getSelectedInstances().contains(insmusic)) {
                    selinstance = insmusic;
                }
            } else if (insimage != null) {
                if (modelimage.getSelectedInstances().contains(insimage)) {
                    selinstance = insimage;
                }
            } else {
                selsource = evt.getPoint();
            }
        } else if (evt.getButton() == java.awt.event.MouseEvent.BUTTON3) {
            selpolygon = new java.awt.Polygon();
            selpolygon.addPoint(evt.getX(), evt.getY());
        }
    }

    public void mouseReleased(java.awt.event.MouseEvent evt) {
        if ((selsource != null && seltarget != null) || selpolygon != null) {
            ArrayList<ProjectionInstance> insmusic = null;
            ArrayList<ProjectionInstance> insimage = null;

            if (MultimodalOptions.isMoveMusics() && modelmusic != null) {
                if (selpolygon != null) {
                    insmusic = getSelectedMusicInstances(selpolygon);
                } else {
                    insmusic = getSelectedMusicInstances(selsource, seltarget);
                }

                if (insmusic != null) {
                    if (!evt.isControlDown()) {
                        modelmusic.setSelectedInstances(new ArrayList<AbstractInstance>(insmusic));
                        modelmusic.notifyObservers();
                    }
                }
            }

            if (MultimodalOptions.isMoveImages() && modelimage != null) {
                if (selpolygon != null) {
                    insimage = getSelectedImageInstances(selpolygon);
                } else {
                    insimage = getSelectedImageInstances(selsource, seltarget);
                }

                if (insimage != null) {
                    if (!evt.isControlDown()) {
                        modelimage.setSelectedInstances(new ArrayList<AbstractInstance>(insimage));
                        modelimage.notifyObservers();
                    }
                }
            }

            if (evt.isControlDown()) {
                int size = modellist.getElementsLists().size();
                Color c = colors[size % colors.length];

                String name = JOptionPane.showInputDialog(browser, "Enter the playlist name",
                        "Playlist name", JOptionPane.QUESTION_MESSAGE);
                ElementsList list = new ElementsList(name);
                modellist.addInstancesList(list);
                list.setColor(c);

                if (insmusic != null) {
                    ArrayList<MusicProjectionInstance> aux = new ArrayList<MusicProjectionInstance>();
                    for (int i = 0; i < insmusic.size(); i++) {
                        MusicProjectionInstance ins = (MusicProjectionInstance) insmusic.get(i);
                        aux.add(ins);
                    }
                    list.addMusicInstances(aux);
                }

                if (insimage != null) {
                    ArrayList<ImageProjectionInstance> aux = new ArrayList<ImageProjectionInstance>();
                    for (int i = 0; i < insimage.size(); i++) {
                        ImageProjectionInstance ins = (ImageProjectionInstance) insimage.get(i);
                        aux.add(ins);
                    }
                    list.addImageInstances(aux);
                }

                modellist.notifyObservers();
            }
        }

        selinstance = null;
        selpolygon = null;
        selsource = null;
        seltarget = null;
    }

    private Dimension calculateMaxSize() {
        Dimension size = new Dimension(0, 0);

        if (modelmusic != null) {
            Dimension sizetmp = modelmusic.getSize();
            size = new Dimension(Math.max(size.width, sizetmp.width),
                    Math.max(size.height, sizetmp.height));
        }

        if (modelimage != null) {
            Dimension sizetmp = modelimage.getSize();
            size = new Dimension(Math.max(size.width, sizetmp.width),
                    Math.max(size.height, sizetmp.height));
        }

        return size;
    }

    private static final Color[] colors = {Color.MAGENTA, /*Color.YELLOW,*/
        Color.GREEN, Color.RED, Color.BLUE, Color.CYAN, Color.DARK_GRAY
    };
    
    protected MultimodalBrowser browser;
    protected MusicProjectionModel modelmusic;
    protected ImageProjectionModel modelimage;
    protected ElementsListModel modellist;
    protected ProjectionInstance selinstance;
    protected Polygon selpolygon;
    protected Point selsource;
    protected Point seltarget;
    protected Color selcolor;
    protected ProjectionInstance labelinsmusic;
    protected Point labelposmusic;
    protected ProjectionInstance labelinsimage;
    protected Point labelposimage;
    protected BufferedImage image;
    protected AbstractSelection selection;
}
