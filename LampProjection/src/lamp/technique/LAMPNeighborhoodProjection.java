/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.technique;

import Jama.Matrix;
import Jama.SingularValueDecomposition;
import datamining.sampling.Sampling;
import datamining.sampling.Sampling.SampleType;
import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import projection.model.ProjectionModelComp;
import projection.technique.Projection;
import projection.technique.idmap.IDMAPProjection;
import projection.technique.idmap.IDMAPProjection.InitializationType;
import projection.view.ProjectionFrameComp;

/**
 *
 * @author PC
 */
public class LAMPNeighborhoodProjection implements Projection {

    public enum SolverType {

        PARALLEL("Parallel solver"),
        SEQUENTIAL("Sequential solver");

        private SolverType(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
        private final String name;
    }
    //
    // Constants
    //
    private final double epsilon = 1.0e-4;
    //
    // Instance Variables
    //
    private float[][] sampledata = null;
    private float[][] sampleproj = null;
    private float fracdelta = 8.0f;
    private int nriteractions = 100;
    private int samplesize = 0;
    private int nrthreads = 8;
    private SampleType sampletype = SampleType.CLUSTERING_MEDOID;
    private SolverType solvertype = SolverType.PARALLEL;
    private float percentage = 1.0f;
    //
    // Constructor
    //

    public LAMPNeighborhoodProjection() {
    }

    //
    // Properties
    //
    public float getFractionDelta() {
        return fracdelta;
    }

    public void setFractionDelta(float fracdelta) {
        this.fracdelta = fracdelta;
    }

    public int getNumberIteractions() {
        return nriteractions;
    }

    public void setNumberIterations(int nriteractions) {
        this.nriteractions = nriteractions;
    }

    public SampleType getSampleType() {
        return sampletype;
    }

    public void setSampleType(SampleType sampletype) {
        this.sampletype = sampletype;
    }

    public int getSampleSize() {
        return samplesize;
    }

    public void setSampleSize(int samplesize) {
        this.samplesize = samplesize;
    }

    public void setPercentageLocalNeighrbors(float percentage) {
        this.percentage = percentage;
    }

    public void setSampleProjection(AbstractMatrix sampleproj) {
        this.sampleproj = sampleproj.toMatrix();
    }

    public void setSampleMatrix(AbstractMatrix samplematrix) {
        this.sampledata = samplematrix.toMatrix();
    }

    private AbstractMatrix getSampleData(AbstractMatrix matrix,
            AbstractDissimilarity diss, int samplesize) throws IOException {
        //getting the sample
        Sampling sampling = new Sampling(sampletype, samplesize);
        AbstractMatrix sampledata_aux = sampling.execute(matrix, diss);
        return sampledata_aux;
    }

    //
    // Projection
    //
    @Override
    public AbstractMatrix project(AbstractMatrix matrix, AbstractDissimilarity diss) throws IOException {
        long start = System.currentTimeMillis();

        //project the sample using IDMAP
        if (sampledata == null) {
            //define the sample
            if (samplesize == 0) {
                samplesize = (int) Math.sqrt(matrix.getRowCount());
            }

            //create the sample matrix data
            AbstractMatrix sampledata_aux = getSampleData(matrix, diss, samplesize);

            //projecting the sample
            IDMAPProjection idmap = new IDMAPProjection();
            idmap.setFractionDelta(fracdelta);
            idmap.setInitialization(InitializationType.FASTMAP);
            idmap.setNumberIterations(nriteractions);
            AbstractMatrix sampleproj_aux = idmap.project(sampledata_aux, diss);

            sampledata = sampledata_aux.toMatrix();
            sampleproj = sampleproj_aux.toMatrix();
        } else if (sampleproj == null) {
            IDMAPProjection idmap = new IDMAPProjection();
            idmap.setFractionDelta(fracdelta);
            idmap.setInitialization(InitializationType.FASTMAP);
            idmap.setNumberIterations(nriteractions);

            DenseMatrix sampledata_aux = new DenseMatrix();
            for (int i = 0; i < sampledata.length; i++) {
                sampledata_aux.addRow(new DenseVector(sampledata[i]));
            }

            AbstractMatrix sampleproj_aux = idmap.project(sampledata_aux, diss);
            sampleproj = sampleproj_aux.toMatrix();
            samplesize = sampleproj.length;
        } else {
            samplesize = sampleproj.length;
        }

        float[][] proj_aux = new float[matrix.getRowCount()][];
        for (int i = 0; i < proj_aux.length; i++) {
            proj_aux[i] = new float[2];
        }

        if (solvertype == SolverType.PARALLEL) {
            int nrpartitions = nrthreads * 4; //number os pieces to split the data
            int step = proj_aux.length / nrpartitions;
            int begin = 0, end = 0;
            ArrayList<ParallelSolver> threads = new ArrayList<ParallelSolver>();
            for (int i = 0; i < nrpartitions; i++) {
                end += step;
                end = (end > proj_aux.length - 1) ? proj_aux.length - 1 : end;

                ParallelSolver ps = new ParallelSolver(proj_aux, matrix, begin, end);
                threads.add(ps);

                begin = end + 1;
            }

            if (end < proj_aux.length - 1) {
                ParallelSolver ps = new ParallelSolver(proj_aux, matrix, end + 1, proj_aux.length - 1);
                threads.add(ps);
            }

            try {
                ExecutorService executor = Executors.newFixedThreadPool(nrthreads);
                executor.invokeAll(threads);
                executor.shutdown();
            } catch (InterruptedException ex) {
                Logger.getLogger(LAMPNeighborhoodProjection.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            createTransformation(proj_aux, matrix, 0, matrix.getRowCount() - 1);
        }

        // Elapsed time
        long finish = System.currentTimeMillis();
        Logger.getLogger(this.getClass().getName()).log(Level.INFO,
                "Local Affine Multidimentional Projection (neighborhood) (LAMP-n) time: {0}s",
                (finish - start) / 1000.0f);

        // result projection
        DenseMatrix projection = new DenseMatrix();

        // labels
        float[] cdata = matrix.getClassData();
        ArrayList<Integer> ids = matrix.getIds();
        ArrayList<String> labels = matrix.getLabels();

        for (int i = 0; i < proj_aux.length; i++) {
            if (labels.size() > 0) {
                projection.addRow(new DenseVector(proj_aux[i], ids.get(i), cdata[i]), labels.get(i));
            } else {
                projection.addRow(new DenseVector(proj_aux[i], ids.get(i), cdata[i]));
            }
        }

        return projection;
    }

    @Override  // not used
    public AbstractMatrix project(DistanceMatrix dmat) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void createTransformation(float[][] projection, AbstractMatrix matrix,
            int begin, int end) {
        // dimensions
        int d = matrix.getDimensions();      // origin space: dimension
        int k = sampledata.length;    // sampling:  instances
        int r = sampleproj[0].length;  // projected: dimension
        int n = Math.max((int) (sampledata.length * percentage), 1); //number neighbors

        // scalars
        float Wsum, aij, w, wsqrt;
        float v00, v10, v01, v11;
        float uj0, uj1, x, y, diff;

        // arrays 1d
        float[] X;
        float[] P, Psum, Pstar;
        float[] Q, Qsum, Qstar;

        Pstar = new float[d];
        Qstar = new float[r];

        //used for limiting the weights
        int[] neighbors_index;
        float[] local_W;

        neighbors_index = new int[n];
        local_W = new float[n];

        // arrays 2d
        Matrix AtB = new Matrix(d, r);

        // Starting calcs
        int p, i, j, m;
        for (p = begin; p <= end; p++) {
            // point to be projected
            X = matrix.getRow(p).toArray();

            //==============================================================
            // STEP 1: Obtain W, Pstar and Qstar
            //==============================================================
            Psum = new float[d];
            Qsum = new float[r];
            Wsum = 0;
            boolean jump = false;

            //obtaining local W
            Arrays.fill(local_W, Float.POSITIVE_INFINITY);

            for (i = 0; i < k; i++) {
                P = sampledata[i];
                Q = sampleproj[i];

                w = 0;
                for (j = 0; j < d; j++) {
                    w += (X[j] - P[j]) * (X[j] - P[j]);
                }

                // coincident points
                if (w < epsilon) {
                    projection[p][0] = Q[0];
                    projection[p][1] = Q[1];
                    jump = true;
                    break;
                }

                if (w < local_W[n - 1]) {
                    for (j = 0; j < n; j++) {
                        if (local_W[j] > w) {
                            for (m = n - 1; m > j; m--) {
                                local_W[m] = local_W[m - 1];
                                neighbors_index[m] = neighbors_index[m - 1];
                            }

                            local_W[j] = w;
                            neighbors_index[j] = i;
                            break;
                        }
                    }
                }
            }

            if (jump) {
                continue;
            }

            for (i = 0; i < n; i++) {
                P = sampledata[neighbors_index[i]];
                Q = sampleproj[neighbors_index[i]];

                local_W[i] = 1 / local_W[i];

                for (j = 0; j < d; j++) {
                    Psum[j] = Psum[j] + P[j] * local_W[i];
                }

                Qsum[0] = Qsum[0] + Q[0] * local_W[i];
                Qsum[1] = Qsum[1] + Q[1] * local_W[i];

                Wsum = Wsum + local_W[i];
            }

            for (j = 0; j < d; j++) {
                Pstar[j] = Psum[j] / Wsum;
            }

            Qstar[0] = Qsum[0] / Wsum;
            Qstar[1] = Qsum[1] / Wsum;

            //==============================================================
            // STEP 2: Obtain Phat, Qhat, A and B
            //==============================================================
            //calculating AtB
            for (i = 0; i < d; i++) {
                x = 0;
                y = 0;

                for (j = 0; j < n; j++) {
                    P = sampledata[neighbors_index[j]];
                    Q = sampleproj[neighbors_index[j]];

                    wsqrt = (float) Math.sqrt(local_W[j]);

                    aij = (P[i] - Pstar[i]) * wsqrt;

                    x = x + (aij * ((Q[0] - Qstar[0]) * wsqrt));
                    y = y + (aij * ((Q[1] - Qstar[1]) * wsqrt));
                }

                AtB.set(i, 0, x);
                AtB.set(i, 1, y);
            }

            //==============================================================
            // STEP 3: Projection
            //==============================================================

            // SVD Computation            
            SingularValueDecomposition svdcalc = new SingularValueDecomposition(AtB); //USV'
            Matrix V = svdcalc.getV();
            Matrix U = svdcalc.getU();

            v00 = (float) V.get(0, 0);
            v01 = (float) V.get(0, 1);
            v10 = (float) V.get(1, 0);
            v11 = (float) V.get(1, 1);

            x = 0;
            y = 0;
            for (j = 0; j < d; j++) {
                diff = (X[j] - Pstar[j]);
                uj0 = (float) U.get(j, 0);
                uj1 = (float) U.get(j, 1);

                x += diff * (uj0 * v00 + uj1 * v01);
                y += diff * (uj0 * v10 + uj1 * v11);
            }

            x = x + Qstar[0];
            y = y + Qstar[1];

            // Add point in the projection
            projection[p][0] = x;
            projection[p][1] = y;
        }
    }

    class ParallelSolver implements Callable<Integer> {

        public ParallelSolver(float[][] projection,
                AbstractMatrix matrix, int begin, int end) {
            this.projection = projection;
            this.matrix = matrix;
            this.begin = begin;
            this.end = end;
        }

        @Override
        public Integer call() throws Exception {
            createTransformation(projection, matrix, begin, end);
            return 0;
        }
        private AbstractMatrix matrix;
        private float[][] projection;
        private int begin;
        private int end;
    }

    public static void main(String[] args) throws IOException {
        String filename = "/home/paulovich/Dropbox/dados/"
                //+ "mammals-10000-normcols.bin";
                //                + "segmentation-normcols.data-notnull.data";
                //+ "multifield.0099-normcols.bin-30000.bin";
                //+ "shuttle_trn_corr-normcols.data";
                //+ "fiber_5_beginEnd_All-normrows.data";
                + "alldocs3-5_30000.data";
//        String filename = "test.data";

        AbstractMatrix matrix = MatrixFactory.getInstance(filename);
//        AbstractMatrix sample = MatrixFactory.getInstance("test_sample.data");


//        CheckConsistency cc = new CheckConsistency();
//        matrix = cc.removeNullColumns(matrix);
//        matrix = cc.shuffle(matrix);

        LAMPNeighborhoodProjection jp = new LAMPNeighborhoodProjection();
        jp.setFractionDelta(8.0f);
        jp.setNumberIterations(100);
        jp.setSampleType(SampleType.RANDOM);
        jp.setPercentageLocalNeighrbors(0.075f);
//        jp.setSampleMatrix(sample);

        AbstractMatrix projection = jp.project(matrix, new Euclidean());

        //original: 18.804s
        //paralelo: 11.233s
        //modif: 8.165s
        //modif: 5.358s
        //modif: 4.523s
        //modif: 4.148s
        //modif: 2.314s

        ProjectionModelComp model = new ProjectionModelComp();
        model.input(projection);
        model.execute();

        ProjectionFrameComp frame = new ProjectionFrameComp();
        frame.input(model.output());
        frame.execute();

//        StressComp stress = new StressComp();
//        stress.setDissimilarityType(DissimilarityType.EUCLIDEAN);
//        stress.setStressType(StressType.NORMALIZED_KRUSKAL);
//        stress.input(projection, matrix);
//        stress.execute();
//        System.out.println("stress: " + stress.output());
    }
}
