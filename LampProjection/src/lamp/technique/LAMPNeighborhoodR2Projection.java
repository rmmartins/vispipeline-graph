/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.technique;

import Jama.Matrix;
import Jama.SingularValueDecomposition;
import datamining.neighbors.KNN;
import datamining.neighbors.Pair;
import datamining.sampling.Sampling;
import datamining.sampling.Sampling.SampleType;
import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import projection.model.ProjectionModelComp;
import projection.technique.Projection;
import projection.technique.idmap.IDMAPProjection;
import projection.technique.idmap.IDMAPProjection.InitializationType;
import projection.view.ProjectionFrameComp;

/**
 *
 * @author PC
 */
public class LAMPNeighborhoodR2Projection implements Projection {

    public enum SolverType {

        PARALLEL("Parallel solver"),
        SEQUENTIAL("Sequential solver");

        private SolverType(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
        private final String name;
    }
    //
    // Constants
    //
    private final double epsilon = 1.0e-4;
    //
    // Instance Variables
    //
    private float[][] sampledata = null;
    private float[][] sampleproj = null;
    private float fracdelta = 8.0f;
    private int nriteractions = 100;
    private int samplesize = 0;
    private int nrthreads = 1;
    private SampleType sampletype = SampleType.CLUSTERING_MEDOID;
    private SolverType solvertype = SolverType.PARALLEL;
    private float percentage = 1.0f;
    //
    // Constructor
    //

    public LAMPNeighborhoodR2Projection() {
    }

    //
    // Properties
    //
    public float getFractionDelta() {
        return fracdelta;
    }

    public void setFractionDelta(float fracdelta) {
        this.fracdelta = fracdelta;
    }

    public int getNumberIteractions() {
        return nriteractions;
    }

    public void setNumberIterations(int nriteractions) {
        this.nriteractions = nriteractions;
    }

    public SampleType getSampleType() {
        return sampletype;
    }

    public void setSampleType(SampleType sampletype) {
        this.sampletype = sampletype;
    }

    public int getSampleSize() {
        return samplesize;
    }

    public void setSampleSize(int samplesize) {
        this.samplesize = samplesize;
    }

    public void setPercentageLocalNeighrbors(float percentage) {
        this.percentage = percentage;
    }

    public void setSampleProjection(AbstractMatrix sampleproj) {
        this.sampleproj = sampleproj.toMatrix();
    }

    public void setSampleMatrix(AbstractMatrix samplematrix) {
        this.sampledata = samplematrix.toMatrix();
    }

    private AbstractMatrix getSampleData(AbstractMatrix matrix,
            AbstractDissimilarity diss, int samplesize) throws IOException {
        //getting the sample
        Sampling sampling = new Sampling(sampletype, samplesize);
        AbstractMatrix sampledata_aux = sampling.execute(matrix, diss);
        return sampledata_aux;
    }

    //
    // Projection
    //
    @Override
    public AbstractMatrix project(AbstractMatrix matrix, AbstractDissimilarity diss) throws IOException {
        long start = System.currentTimeMillis();

        //project the sample using IDMAP
        if (sampledata == null) {
            //define the sample
            if (samplesize == 0) {
                samplesize = (int) Math.sqrt(matrix.getRowCount());
            }

            //create the sample matrix data
            AbstractMatrix sampledata_aux = getSampleData(matrix, diss, samplesize);

            //projecting the sample
            IDMAPProjection idmap = new IDMAPProjection();
            idmap.setFractionDelta(fracdelta);
            idmap.setInitialization(InitializationType.FASTMAP);
            idmap.setNumberIterations(nriteractions);
            AbstractMatrix sampleproj_aux = idmap.project(sampledata_aux, diss);

            sampledata = sampledata_aux.toMatrix();
            sampleproj = sampleproj_aux.toMatrix();
        } else if (sampleproj == null) {
            IDMAPProjection idmap = new IDMAPProjection();
            idmap.setFractionDelta(fracdelta);
            idmap.setInitialization(InitializationType.FASTMAP);
            idmap.setNumberIterations(nriteractions);

            DenseMatrix sampledata_aux = new DenseMatrix();
            for (int i = 0; i < sampledata.length; i++) {
                sampledata_aux.addRow(new DenseVector(sampledata[i]));
            }

            AbstractMatrix sampleproj_aux = idmap.project(sampledata_aux, diss);
            sampleproj = sampleproj_aux.toMatrix();
            samplesize = sampleproj.length;
        } else {
            samplesize = sampleproj.length;
        }

        //calculating the neighborhood in R2
        int n = Math.max((int) (sampleproj.length * percentage), 1); //number neighbors
        Pair[][] neighborhood = null;

        if (n > 1) {
            DenseMatrix sampleproj_aux = new DenseMatrix();
            for (int i = 0; i < sampleproj.length; i++) {
                sampleproj_aux.addRow(new DenseVector(sampleproj[i]));
            }

            KNN knn = new KNN(n - 1);
            neighborhood = knn.execute(sampleproj_aux, new Euclidean());

            //complementing the neighborhood to add itself as a neighbor
            for (int i = 0; i < sampleproj.length; i++) {
                Pair[] new_neigh = new Pair[n];

                new_neigh[0] = new Pair(i, 0);
                System.arraycopy(neighborhood[i], 0, new_neigh, 1, neighborhood[i].length);

                neighborhood[i] = new_neigh;
            }
        } else {
            //only one neighbor, put it self as closest neighbors
            neighborhood = new Pair[sampleproj.length][1];

            for (int i = 0; i < sampleproj.length; i++) {
                neighborhood[i] = new Pair[1];
                neighborhood[i][0] = new Pair(i, 0);
            }
        }

        //creating the auxiliary to project
        float[][] proj_aux = new float[matrix.getRowCount()][];
        for (int i = 0; i < proj_aux.length; i++) {
            proj_aux[i] = new float[2];
        }

        if (solvertype == SolverType.PARALLEL) {
            int nrpartitions = nrthreads * 4; //number os pieces to split the data
            int step = proj_aux.length / nrpartitions;
            int begin = 0, end = 0;
            ArrayList<ParallelSolver> threads = new ArrayList<ParallelSolver>();
            for (int i = 0; i < nrpartitions; i++) {
                end += step;
                end = (end > proj_aux.length - 1) ? proj_aux.length - 1 : end;

                ParallelSolver ps = new ParallelSolver(proj_aux, matrix, neighborhood, begin, end);
                threads.add(ps);

                begin = end + 1;
            }

            if (end < proj_aux.length - 1) {
                ParallelSolver ps = new ParallelSolver(proj_aux, matrix, neighborhood, end + 1, proj_aux.length - 1);
                threads.add(ps);
            }

            try {
                ExecutorService executor = Executors.newFixedThreadPool(nrthreads);
                executor.invokeAll(threads);
                executor.shutdown();
            } catch (InterruptedException ex) {
                Logger.getLogger(LAMPNeighborhoodR2Projection.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            createTransformation(proj_aux, matrix, neighborhood, 0, matrix.getRowCount() - 1);
        }

        // Elapsed time
        long finish = System.currentTimeMillis();
        Logger.getLogger(this.getClass().getName()).log(Level.INFO,
                "Local Affine Multidimentional Projection (neighborhood) (LAMP-n) time: {0}s",
                (finish - start) / 1000.0f);

        // result projection
        DenseMatrix projection = new DenseMatrix();

        // labels
        float[] cdata = matrix.getClassData();
        ArrayList<Integer> ids = matrix.getIds();
        ArrayList<String> labels = matrix.getLabels();

        for (int i = 0; i < proj_aux.length; i++) {
            if (labels.size() > 0) {
                projection.addRow(new DenseVector(proj_aux[i], ids.get(i), cdata[i]), labels.get(i));
            } else {
                projection.addRow(new DenseVector(proj_aux[i], ids.get(i), cdata[i]));
            }
        }

        return projection;
    }

    @Override  // not used
    public AbstractMatrix project(DistanceMatrix dmat) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void createTransformation(float[][] projection, AbstractMatrix matrix,
            Pair[][] neighborhood, int begin, int end) {
        // dimensions
        int d = matrix.getDimensions();      // origin space: dimension
        int k = sampledata.length;    // sampling:  instances
        int r = sampleproj[0].length;  // projected: dimension
        int n = neighborhood[0].length; //number neighbors

        // scalars
        float Wsum, aij, wsqrt;
        float v00, v10, v01, v11;
        float uj0, uj1, x, y, diff;

        // arrays 1d
        float[] X, W;
        float[] P, Psum, Pstar;
        float[] Q, Qsum, Qstar;

        Pstar = new float[d];
        Qstar = new float[r];

        // arrays 2d
        Matrix AtB = new Matrix(d, r);

        // Starting calcs
        int p, i, j;
        for (p = begin; p <= end; p++) {
            // point to be projected
            X = matrix.getRow(p).toArray();

            //==============================================================
            // STEP 1: Obtain W, Pstar and Qstar
            //==============================================================
            Psum = new float[d];
            Qsum = new float[r];
            Wsum = 0;
            boolean jump = false;

            //finding the closest control point to X
            float min_dist = Float.POSITIVE_INFINITY;
            int closest = 0;

            for (i = 0; i < k; i++) {
                P = sampledata[i];

                float dist = 0;
                for (j = 0; j < d; j++) {
                    dist += (X[j] - P[j]) * (X[j] - P[j]);
                }

                if (min_dist > dist) {
                    min_dist = dist;
                    closest = i;
                }
            }

            //obtaining W
            W = new float[n];

            for (i = 0; i < n; i++) {
                P = sampledata[neighborhood[closest][i].index];
                Q = sampleproj[neighborhood[closest][i].index];

                W[i] = 0;
                for (j = 0; j < d; j++) {
                    W[i] += (X[j] - P[j]) * (X[j] - P[j]);
                }

                // coincident points
                if (W[i] < epsilon) {
                    projection[p][0] = Q[0];
                    projection[p][1] = Q[1];
                    jump = true;
                    break;
                }

                W[i] = 1 / W[i];

                for (j = 0; j < d; j++) {
                    Psum[j] = Psum[j] + P[j] * W[i];
                }

                Qsum[0] = Qsum[0] + Q[0] * W[i];
                Qsum[1] = Qsum[1] + Q[1] * W[i];

                Wsum = Wsum + W[i];
            }

            if (jump) {
                continue;
            }

            for (j = 0; j < d; j++) {
                Pstar[j] = Psum[j] / Wsum;
            }

            Qstar[0] = Qsum[0] / Wsum;
            Qstar[1] = Qsum[1] / Wsum;

            //==============================================================
            // STEP 2: Obtain Phat, Qhat, A and B
            //==============================================================
            //calculating AtB
            for (i = 0; i < d; i++) {
                x = 0;
                y = 0;

                for (j = 0; j < n; j++) {
                    P = sampledata[neighborhood[closest][j].index];
                    Q = sampleproj[neighborhood[closest][j].index];

                    wsqrt = (float) Math.sqrt(W[j]);

                    aij = (P[i] - Pstar[i]) * wsqrt;

                    x = x + (aij * ((Q[0] - Qstar[0]) * wsqrt));
                    y = y + (aij * ((Q[1] - Qstar[1]) * wsqrt));
                }

                AtB.set(i, 0, x);
                AtB.set(i, 1, y);
            }

            //==============================================================
            // STEP 3: Projection
            //==============================================================

            // SVD Computation            
            SingularValueDecomposition svdcalc = new SingularValueDecomposition(AtB); //USV'
            Matrix V = svdcalc.getV();
            Matrix U = svdcalc.getU();

            v00 = (float) V.get(0, 0);
            v01 = (float) V.get(0, 1);
            v10 = (float) V.get(1, 0);
            v11 = (float) V.get(1, 1);

            x = 0;
            y = 0;
            for (j = 0; j < d; j++) {
                diff = (X[j] - Pstar[j]);
                uj0 = (float) U.get(j, 0);
                uj1 = (float) U.get(j, 1);

                x += diff * (uj0 * v00 + uj1 * v01);
                y += diff * (uj0 * v10 + uj1 * v11);
            }

            x = x + Qstar[0];
            y = y + Qstar[1];

            // Add point in the projection
            projection[p][0] = x;
            projection[p][1] = y;
        }
    }

    class ParallelSolver implements Callable<Integer> {

        public ParallelSolver(float[][] projection,
                AbstractMatrix matrix, Pair[][] neighborhood, int begin, int end) {
            this.projection = projection;
            this.matrix = matrix;
            this.neighborhood = neighborhood;
            this.begin = begin;
            this.end = end;
        }

        @Override
        public Integer call() throws Exception {
            createTransformation(projection, matrix, neighborhood, begin, end);
            return 0;
        }
        private Pair[][] neighborhood;
        private AbstractMatrix matrix;
        private float[][] projection;
        private int begin;
        private int end;
    }

    public static void main(String[] args) throws IOException {
        String filename = "D:\\My Dropbox\\dados\\"
                //+ "mammals-10000-normcols.bin";
                + "segmentation-normcols.data-notnull.data";
        //+ "multifield.0099-normcols.bin-30000.bin";
        //+ "shuttle_trn_corr-normcols.data";
        //+ "fiber_5_beginEnd_All-normrows.data";
        //+ "alldocs3-5_30000.data";

        AbstractMatrix matrix = MatrixFactory.getInstance(filename);

        Sampling smp = new Sampling(SampleType.RANDOM, (int) Math.sqrt(matrix.getRowCount()));
        AbstractMatrix sample = smp.execute(matrix, new Euclidean());

        IDMAPProjection idmap = new IDMAPProjection();
        idmap.setFractionDelta(8.0f);
        idmap.setInitialization(InitializationType.FASTMAP);
        idmap.setNumberIterations(100);
        AbstractMatrix sampleproj = idmap.project(sample, new Euclidean());


        LAMPNeighborhoodR2Projection lamp1 = new LAMPNeighborhoodR2Projection();
        lamp1.setFractionDelta(8.0f);
        lamp1.setNumberIterations(100);
        lamp1.setSampleMatrix(sample);
        lamp1.setSampleProjection(sampleproj);
        //lamp1.setSampleType(SampleType.RANDOM);
        //lamp1.setSampleSize(9);
        //lamp1.setPercentageLocalNeighrbors(0.1f);
        AbstractMatrix projection1 = lamp1.project(matrix, new Euclidean());

        ProjectionModelComp model1 = new ProjectionModelComp();
        model1.input(projection1);
        model1.execute();

        ProjectionFrameComp frame1 = new ProjectionFrameComp();
        frame1.setTitle("LAMP R2");
        frame1.input(model1.output());
        frame1.execute();

        ///////////////////
        LAMPNeighborhoodProjection lamp2 = new LAMPNeighborhoodProjection();
        lamp2.setFractionDelta(8.0f);
        lamp2.setNumberIterations(100);
        lamp2.setSampleMatrix(sample);
        lamp2.setSampleProjection(sampleproj);
        //lamp2.setSampleType(SampleType.RANDOM);
        //lamp1.setSampleSize(9);
        //lamp1.setPercentageLocalNeighrbors(0.1f);
        AbstractMatrix projection2 = lamp2.project(matrix, new Euclidean());

        ProjectionModelComp model2 = new ProjectionModelComp();
        model2.input(projection2);
        model2.execute();

        ProjectionFrameComp frame2 = new ProjectionFrameComp();
        frame2.setTitle("LAMP Rn");
        frame2.input(model2.output());
        frame2.execute();
    }
}
