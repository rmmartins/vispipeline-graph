/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.technique;

import distance.dissimilarity.Euclidean;
import lamp.repositioning.*;
import java.io.IOException;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import matrix.util.MatrixUtils;
import projection.model.ProjectionModel;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Danilo Medeiros Eler
 */
@VisComponent(hierarchy = "Projection.Technique.LAMP",
name = "LAMP Projection",
description = "Create a final LAMP projection.")
public class LAMPProjectionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (matrix != null) {
            LAMPNeighborhoodProjection lamp = new LAMPNeighborhoodProjection();
            lamp.setFractionDelta(8.0f);
            lamp.setNumberIterations(100);
            lamp.setPercentageLocalNeighrbors(percneigh);
            if (samplematrix != null) {
                lamp.setSampleMatrix(samplematrix);
            }
            if (sampleprojection != null) {
                lamp.setSampleProjection(sampleprojection);
            }
            projection = lamp.project(matrix, new Euclidean());
        } else {
            throw new IOException("A matrix, sample matrix and sample projection"
                    + "should be provided.");
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix,
            @Param(name = "sample matrix") AbstractMatrix samplematrix,
            @Param(name = "sample projection") AbstractMatrix sampleprojection) {
        this.matrix = matrix;
        this.samplematrix = samplematrix;
        this.sampleprojection = sampleprojection;
    }
    
    public void input(@Param(name = "points matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public AbstractMatrix output() {
        return projection;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null){
            paramview = new LAMPProjectionParamView( this );
        }
        return paramview;
    }

    @Override
    public void reset() {
        model = null;
        coordinators = null;
        matrix = null;
        samplematrix = null;
        sampleprojection = null;
        projection = null;
    }

    public float getPercneigh() {
        return percneigh;
    }

    public void setPercneigh(float percneigh) {
        this.percneigh = percneigh;
    }


    public static final long serialVersionUID = 1L;
    private float percneigh = 1.0f;
    private transient ProjectionModel model;
    private transient AbstractMatrix matrix;
    private transient AbstractMatrix samplematrix;
    private transient AbstractMatrix sampleprojection;
    private transient AbstractMatrix projection;
    private transient LAMPProjectionParamView paramview;
    private transient ArrayList<AbstractCoordinator> coordinators;
}
