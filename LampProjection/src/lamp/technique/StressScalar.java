/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lamp.technique;

import distance.LightWeightDistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.Euclidean;
import matrix.AbstractMatrix;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;
import projection.model.Scalar;

/**
 *
 * @author paulovich
 */
public class StressScalar {

    public static void execute(ProjectionModel model, AbstractMatrix matrix,
            AbstractMatrix projection, AbstractDissimilarity diss) {

        LightWeightDistanceMatrix dmat = new LightWeightDistanceMatrix(matrix, diss);
        LightWeightDistanceMatrix dmatprj = new LightWeightDistanceMatrix(projection, new Euclidean());

        double maxrn = Double.NEGATIVE_INFINITY;
        double maxr2 = Double.NEGATIVE_INFINITY;

        for (int i = 0; i < dmat.getElementCount(); i++) {
            for (int j = i + 1; j < dmat.getElementCount(); j++) {
                double valuern = dmat.getDistance(i, j);
                double valuer2 = dmatprj.getDistance(i, j);

                if (valuern > maxrn) {
                    maxrn = valuern;
                }

                if (valuer2 > maxr2) {
                    maxr2 = valuer2;
                }
            }
        }

        Scalar stress = model.addScalar("stress");

        for (int i = 0; i < dmat.getElementCount(); i++) {
            double num = 0.0f;

            for (int j = i + 1; j < dmat.getElementCount(); j++) {
                double distrn = dmat.getDistance(i, j) / maxrn;
                double distr2 = dmatprj.getDistance(i, j) / maxr2;
                num += (distrn - distr2) * (distrn - distr2);
            }

            ProjectionInstance pi = (ProjectionInstance) model.getInstances().get(i);
            pi.setScalarValue(stress, (float) (num));

//            System.out.println(pi.getId() + " " + num);
        }
    }

}
