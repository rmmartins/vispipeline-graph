/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.technique;

import distance.dissimilarity.Euclidean;
import lamp.repositioning.*;
import java.io.IOException;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import matrix.util.MatrixUtils;
import projection.model.ProjectionModel;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import visualizationbasics.coordination.AbstractCoordinator;

/**
 *
 * @author Danilo Medeiros Eler
 */
@VisComponent(hierarchy = "Projection.Technique.LAMP",
name = "LAMP R2 Projection",
description = "Create a final LAMP projection based on a sample repositioned projection.")
public class LAMPR2ProjectionComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (sampleprojection != null && matrix != null
                && samplematrix != null) {
                LAMPNeighborhoodR2Projection lamp = new LAMPNeighborhoodR2Projection();
                lamp.setFractionDelta(8.0f);
                lamp.setNumberIterations(100);
                lamp.setPercentageLocalNeighrbors(percneigh);
                lamp.setSampleMatrix(samplematrix);
                lamp.setSampleProjection(sampleprojection);
                projection = lamp.project(matrix, new Euclidean());
        } else {
            throw new IOException("A matrix, sample matrix and sample projection"
                    + "should be provided.");
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix,
            @Param(name = "sample matrix") AbstractMatrix samplematrix,
            @Param(name = "sample projection") AbstractMatrix sampleprojection) {
        this.matrix = matrix;
        this.samplematrix = samplematrix;
        this.sampleprojection = sampleprojection;        
    }

    public AbstractMatrix output() {
        return projection;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null){
            paramview = new LAMPR2ProjectionParamView( this );
        }
        return paramview;
    }

    @Override
    public void reset() {
        model = null;
        coordinators = null;
        matrix = null;
        samplematrix = null;
        sampleprojection = null;        
        projection = null;
    }

    public float getPercneigh() {
        return percneigh;
    }

    public void setPercneigh(float percneigh) {
        this.percneigh = percneigh;
    }

    
    public static final long serialVersionUID = 1L;
    private float percneigh = 1.0f;
    private transient ProjectionModel model;
    private transient AbstractMatrix matrix;
    private transient AbstractMatrix samplematrix;
    private transient AbstractMatrix sampleprojection;    
    private transient AbstractMatrix projection;
    private transient LAMPR2ProjectionParamView paramview;
    private transient ArrayList<AbstractCoordinator> coordinators;
}
