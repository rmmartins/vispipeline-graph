/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.util;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author PC
 */
public class Util {

    public static void main(String[] args) throws IOException {
        File dir = new File("D:\\dados\\LAMP\\imagens_reduzidas");
        ArrayList<File> images = new ArrayList<File>();
        Util.loadImages(images, dir);

        System.out.println(images.size());

        for (int i = 0; i < images.size(); i++) {
            Image image = (new ImageIcon(images.get(i).getPath())).getImage();
            Image scaled = (new ImageIcon(image.getScaledInstance(360, 240, 0))).getImage();

            BufferedImage buffer = new BufferedImage(360, 240, BufferedImage.TYPE_INT_RGB);
            buffer.getGraphics().drawImage(scaled, 0, 0, null);
            ImageIO.write(buffer, "jpg", images.get(i));
        }
    }

    public static void loadImages(ArrayList<File> images, File dir) {
        File[] listFiles = dir.listFiles();

        if (listFiles != null && listFiles.length > 0) {
            for (File f : listFiles) {
                if (f.getName().endsWith(".jpg")) {
                    images.add(f);
                } else if (f.isDirectory()) {
                    loadImages(images, f);
                }
            }
        }
    }
}
