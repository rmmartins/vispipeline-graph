/*
 * Copyright 2018 Rafael M. Martins <rafael.martins@lnu.se>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 */
package lamp.util;

import datamining.sampling.Sampling;
import distance.dissimilarity.Euclidean;
import java.io.File;
import java.io.IOException;
import lamp.technique.LAMPProjection;
import lamp.technique.LAMPProjectionComp;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import projection.util.ProjectionRunner;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractRunner.Param;

/**
 *
 * @author Rafael M. Martins <rafael.martins@lnu.se>
 */
public class LampRunner extends ProjectionRunner {

    @Param(desc = "Fraction Delta", pos = 0)
    public float fractionDelta = 8.0f;

    @Param(desc = "Number of Iterations", pos = 1)
    public int numberOfIterations = 100;

    @Param(desc = "Sample Type", pos = 2)
    public Sampling.SampleType sampleType = Sampling.SampleType.CLUSTERING_MEDOID;

    @Override
    protected AbstractMatrix runProjection(File f) throws IOException {

        AbstractMatrix matrix = MatrixFactory.getInstance(f.getPath());

        LAMPProjection lamp = new LAMPProjection();
        lamp.setFractionDelta(fractionDelta);
        lamp.setNumberIterations(numberOfIterations);
        lamp.setSampleType(sampleType);
        return lamp.project(matrix, new Euclidean());        

//            ProjectionModelComp model = new ProjectionModelComp();
//            model.input(projection);
//            model.execute();
//
//            ProjectionFrameComp frame = new ProjectionFrameComp();
//            frame.input(model.output());
//            frame.execute();
    }
    
    @Override
    public String getName() {
        return LAMPProjectionComp.class.getAnnotation(VisComponent.class).name();
    }

    @Override
    public String getShortName() {
        return "lamp";
    }

}
