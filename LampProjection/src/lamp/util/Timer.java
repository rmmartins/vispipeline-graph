/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lamp.util;

/**
 *
 * @author paulovich
 */
public class Timer extends Thread {

    public Timer(TimerListener listener, long millis) {
        this.listener = listener;
        this.millis = millis;
        this.pause = false;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (!pause) {
                    listener.update(this);
                }

                Thread.sleep(millis);
            } catch (InterruptedException ex) {
            }
        }
    }

    public void pause() {
        pause = !pause;
    }
    
    private boolean pause;
    private long millis;
    private TimerListener listener;
}
