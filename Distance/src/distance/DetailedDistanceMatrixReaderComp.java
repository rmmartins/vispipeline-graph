package distance;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

@VisComponent(hierarchy = "Distance.Input",
name = "Detailed Distance Matrix reader",
description = "Read a Detailed Distance Matrix from a file.")
public class DetailedDistanceMatrixReaderComp extends DistanceMatrixReaderComp implements AbstractComponent {

    @Override
    public void execute() {
        try {
            dmat = new DetailedDistanceMatrix(filename);
        } catch (IOException ex) {
            Logger.getLogger(DetailedDistanceMatrixReaderComp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public DistanceMatrix output() {
        return dmat;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new DistanceMatrixReaderParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        dmat = null;
    }

    /**
     * @return the filename
     */
    @Override
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    @Override
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public static final long serialVersionUID = 1L;
    private String filename = "";
    private transient DistanceMatrixReaderParamView paramview;
    private transient DetailedDistanceMatrix dmat;
}
