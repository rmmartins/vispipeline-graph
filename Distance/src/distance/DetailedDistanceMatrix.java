package distance;

import distance.dissimilarity.AbstractDissimilarity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import matrix.AbstractMatrix;
import detailedMatrix.dense.DetailedDenseMatrix;

public class DetailedDistanceMatrix extends DistanceMatrix {

    public DetailedDistanceMatrix(String filename) throws IOException {
        this.load(filename);
        this.source = filename;
        int begin = source.lastIndexOf("\\");
        int end = source.lastIndexOf(".");
        if (begin == -1) begin = 0;
        else if ((begin+1)<=end)
                begin = begin+1;
        if (end == -1) end = source.length();
        this.name = source.substring(begin, end);
        //Verifying if the ids are all the same (problems caused by empty labels on dmat file)
        if (!this.ids.isEmpty()) {
            int id = this.ids.get(0);
            boolean changed = false;
            for (int i=0;i<this.ids.size();i++)
                if (ids.get(i) != id) {
                    changed = true;
                    break;
                }
            if (!changed) {
                for (int i=0;i<this.ids.size();i++)
                    this.ids.set(i,i);
            }
        }
    }

    public DetailedDistanceMatrix(int nrElements) {
       super(nrElements);
    }

    /**
     * This constructor create a distance distmatrix with distances for
     * one to all other points passed as argument.
     * @param matrix
     * @param diss
     * @throws java.io.IOException 
     */
    public DetailedDistanceMatrix(AbstractMatrix matrix, AbstractDissimilarity diss) throws IOException {
        super(matrix,diss);
        if (matrix instanceof DetailedDenseMatrix)
            if (((DetailedDenseMatrix)matrix).getSource() != null)
                this.source = ((DetailedDenseMatrix)matrix).getSource();
            else
                this.source = matrix.toString();
        int begin = source.lastIndexOf("\\");
        int end = source.lastIndexOf(".");
        if (begin == -1) begin = 0;
        else if ((begin+1)<=end)
                begin = begin+1;
        if (end == -1) end = source.length();
        this.name = source.substring(begin, end);
        this.diss = diss;
    }

    public DetailedDistanceMatrix() {
        super();
    }
 
    @Override
    public Object clone() throws CloneNotSupportedException {
        DetailedDistanceMatrix clonedmat = new DetailedDistanceMatrix(this.nrElements);
        clonedmat.maxDistance = this.maxDistance;
        clonedmat.minDistance = this.minDistance;
        for (int i = 0; i < this.distmatrix.length; i++) {
            for (int j = 0; j < this.distmatrix[i].length; j++) {
                clonedmat.distmatrix[i][j] = this.distmatrix[i][j];
            }
        }
        clonedmat.ids = new ArrayList<Integer>();
        clonedmat.ids.addAll(ids);
        clonedmat.cdata = Arrays.copyOf(cdata, cdata.length);
        clonedmat.labels = new ArrayList<String>();
        clonedmat.labels.addAll(labels);
        clonedmat.source = this.source;
        clonedmat.name = this.name;
        return clonedmat;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AbstractDissimilarity getDiss() {
        return diss;
    }

    public void setDiss(AbstractDissimilarity diss) {
        this.diss = diss;
    }

    private String source = "";
    private String name = "";
    private AbstractDissimilarity diss;
}
