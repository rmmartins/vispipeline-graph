/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package textprocessing.graph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import textprocessing.processing.Ngram;
import textprocessing.processing.Stopword;
import textprocessing.processing.TermExtractor;
import textprocessing.processing.stemmer.Stemmer;
import textprocessing.processing.stemmer.StemmerFactory;

/**
 *
 * @author PC
 */
public class WordGraph {

    public void execute(String text) throws IOException {        
        HashMap<String, Integer> freqwords = getMostFrequentWords(text, StemmerFactory.getInstance(StemmerFactory.StemmerType.ENGLISH), 10);

        int[][] graph = createGraph(text, StemmerFactory.getInstance(StemmerFactory.StemmerType.ENGLISH), freqwords);
        for (int i = 0; i < graph.length; i++) {
            for (int j = 0; j < graph[i].length; j++) {
                System.out.print(graph[i][j] + " ");
            }
            System.out.println();
        }

//        //for each sentence
//        StringTokenizer senttok = new StringTokenizer(text, ".!?");
//        while (senttok.hasMoreTokens()) {
////            String sentence = filter(senttok.nextToken(), StemmerFactory.getInstance(StemmerFactory.StemmerType.ENGLISH));
////            System.out.println(sentence);
//
//            System.out.println(filter(senttok.nextToken(), words));
//
//
//
//
//        }
    }

    private int[][] createGraph(String text, Stemmer stemmer, HashMap<String, Integer> freqwords) throws IOException {
        int graph[][] = new int[freqwords.size()][freqwords.size()];
        for (int i = 0; i < graph.length; i++) {
            Arrays.fill(graph[i], 0);
        }

        //for each sentence
        StringTokenizer texttok = new StringTokenizer(text, ".!?");
        while (texttok.hasMoreTokens()) {
            String sent = texttok.nextToken();
            String filsent = filter(sent, stemmer, freqwords);

            StringTokenizer senttok = new StringTokenizer(filsent, " ");
            if (senttok.hasMoreTokens()) {
                int size = senttok.countTokens();
                int first = freqwords.get(senttok.nextToken().trim());
                graph[first][first]++;

                for (int i = 0; i < size-1; i++) {
                    int second = freqwords.get(senttok.nextToken().trim());

                    graph[first][second]++;
                    graph[second][first]++;

                    first = second;
                }
            }
        }

        return graph;
    }


    /*
     * Filtra um texto para que sobre somente as palavras mais frequentes.
     */
    private String filter(String sentence, Stemmer stemmer, HashMap<String, Integer> freqwords) throws IOException {
        StringBuilder sb = new StringBuilder();

        StringTokenizer token = new StringTokenizer(filter(sentence, stemmer), " \n");
        while (token.hasMoreTokens()) {
            String word = token.nextToken();
            if (freqwords.containsKey(word)) {
                sb.append(word).append(" ");
            }
        }

        return sb.toString();
    }

    /*
     * Pega as palavras mais frequentes em um texto.
     */
    private HashMap<String, Integer> getMostFrequentWords(String text, Stemmer stemmer, int nrwords) throws IOException {
        HashMap<String, Integer> wordfreq = new HashMap<String, Integer>();

        StringTokenizer token = new StringTokenizer(filter(text, stemmer), " \n");
        while (token.hasMoreTokens()) {
            String word = token.nextToken();
            if (wordfreq.containsKey(word)) {
                wordfreq.put(word, wordfreq.get(word) + 1);
            } else {
                wordfreq.put(word, 1);
            }
        }

        ArrayList<Ngram> ngrams = new ArrayList<Ngram>();
        for (String word : wordfreq.keySet()) {
            ngrams.add(new Ngram(word, wordfreq.get(word)));
        }
        Collections.sort(ngrams);

        HashMap<String, Integer> words_aux = new HashMap<String, Integer>();
        int size = Math.min(nrwords, ngrams.size());
        for (int i = 0; i < size; i++) {
            System.out.println(ngrams.get(i).ngram + "<>" + ngrams.get(i).frequency);
            words_aux.put(ngrams.get(i).ngram, i);
        }

        return words_aux;
    }

    /*
     * Recebe uma sentença de entrada e retorna a sentença filtrada, restando somente
     * as palavras representativas (não stopwords) reduzidas a seu radical (stemming)
     */
    private String filter(String sentence, Stemmer stemmer) throws IOException {
        Pattern pattern = Pattern.compile(TermExtractor.getRegularExpression());
        StringBuilder sb = new StringBuilder();
        Stopword stp = Stopword.getInstance();

        if (sentence != null) {
            Matcher matcher = pattern.matcher(sentence);

            while (matcher.find()) {
                String term = matcher.group();

                if (term.trim().length() > 0) {
                    String word = term.toLowerCase();

                    if (word.trim().length() > 0) {
                        word = stemmer.stem(word);//aplica o stemmer

                        if (!stp.isStopWord(word)) {
                            sb.append(word).append(" ");
                        }
                    }
                }
            }
        }

        return sb.toString();

    }

    public static void main(String[] args) throws IOException {
        String text = ReadText.read("D:\\CBR-837Aam274-288.txt");

        WordGraph wg = new WordGraph();
        wg.execute(text);


    }

}
