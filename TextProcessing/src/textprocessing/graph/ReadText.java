/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package textprocessing.graph;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PC
 */
public class ReadText {

  public static String read(String filename) throws IOException {
    BufferedReader in = null;
    StringBuilder sb = new StringBuilder();

    try {
      in = new BufferedReader(new java.io.FileReader(filename));
      String line = null;
      while ((line = in.readLine()) != null) {
        sb.append(line).append(" ");
      }
    } catch (IOException e) {
      throw new IOException(e.getMessage());
    } finally {
      if (in != null) {
        try {
          in.close();
        } catch (IOException ex) {
          Logger.getLogger(ReadText.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return sb.toString();
  }

}
