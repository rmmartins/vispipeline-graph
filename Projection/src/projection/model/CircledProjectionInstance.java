/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projection.model;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

/**
 *
 * @author paulovich
 */
public class CircledProjectionInstance extends NewProjectionInstance {

    public CircledProjectionInstance(String label, int id, float x, float y) {
        super(label, id, x, y);
    }

    public CircledProjectionInstance(int id, float x, float y) {
        super(null, id, x, y);
    }

    public CircledProjectionInstance( int id) {
        this(id, 0, 0);
    }

    public CircledProjectionInstance(String label, int id) {
        this(label, id, 0, 0);
    }

    @Override
    public void draw(BufferedImage image, boolean highquality) {
        Graphics2D g2 = (Graphics2D) image.getGraphics();

        if (highquality) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }

        if (selected) {
            int inssize = size;

            g2.setStroke(new BasicStroke(4.0f));
            inssize *= 1.5;

            g2.setColor(getColor());
            g2.fillOval(((int) x) - inssize, ((int) y) - inssize, inssize * 2, inssize * 2);

            g2.setColor(Color.DARK_GRAY);
            g2.drawOval(((int) x) - inssize, ((int) y) - inssize, inssize * 2, inssize * 2);

            g2.setStroke(new BasicStroke(1.0f));
        } else {
            int inssize = size;

            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, ((ProjectionModel) model).getAlpha()));

            g2.setColor(getColor());
            g2.fillOval(((int) x) - inssize, ((int) y) - inssize, inssize * 2, inssize * 2);

            g2.setColor(Color.BLACK);
            g2.drawOval(((int) x) - inssize, ((int) y) - inssize, inssize * 2, inssize * 2);

            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
        }

        //show the label associated to this instance
        if (showlabel) {
            drawLabel(g2, (int) x, (int) y);
        }
    }

    @Override
    public boolean isInside(int x, int y) {
        return (Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2)) <= size);
    }
}
