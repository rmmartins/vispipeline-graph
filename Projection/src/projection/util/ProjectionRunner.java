/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projection.util;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import matrix.AbstractMatrix;
import vispipelinebasics.interfaces.AbstractRunner;

/**
 *
 * @author Rafael M. Martins <rafael.martins@lnu.se>
 */
public abstract class ProjectionRunner extends AbstractRunner {
    
    protected abstract AbstractMatrix runProjection(File f) throws IOException;

    @Override
    public void run(String[] args) throws IOException {
        super.run(args);
        for (File f : files) {
            System.out.println("Processing: " + f.getPath());
            
            AbstractMatrix projection = runProjection(f);
            
            if (projection.getAttributes().isEmpty()) {
                projection.setAttributes(Arrays.asList(new String[]{"x", "y"}));
            }
            
            String baseName = f.getName().substring(0, f.getName().indexOf("."));
            projection.save(dir + "//" + baseName + "-" + getShortName() + ".prj");
        }
    }
    
}
