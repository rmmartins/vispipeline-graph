/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projection.technique.projclus;

import distance.dissimilarity.DissimilarityFactory;
import java.io.File;
import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import projection.technique.idmap.IDMAPProjection;
import projection.util.ProjectionRunner;
import vispipelinebasics.annotations.VisComponent;

/**
 *
 * @author Rafael M. Martins <rafael.martins@lnu.se>
 */
public class ProjClusRunner extends ProjectionRunner {
        
    @Param(desc = "Dissimilarity Type", pos = 0)
    public DissimilarityFactory.DissimilarityType dissType = DissimilarityFactory.DissimilarityType.EUCLIDEAN;
    
    @Param(desc = "Cluster Factor", pos = 1)
    public float clusterFactor = 4.5f;
    
    @Param(desc = "Fraction Delta", pos = 2)
    public float fractionDelta = 8.0f;
    
    @Param(desc = "Number of Iterations", pos = 3)
    public int numberOfIterations = 50;
    
    @Param(desc = "Initialization Type", pos = 4)
    public IDMAPProjection.InitializationType initType = IDMAPProjection.InitializationType.FASTMAP;

    @Override
    public AbstractMatrix runProjection(File f) throws IOException {        
        AbstractMatrix matrix = MatrixFactory.getInstance(f.getAbsolutePath());

        //ProjectionUtil.log(false, false);

        ProjClusProjection pp = new ProjClusProjection();
        
        pp.setClusterFactor(clusterFactor);
        pp.setFractionDelta(fractionDelta);
        pp.setInitialization(initType);
        pp.setNumberIterations(numberOfIterations);
        
        AbstractMatrix projection = pp.project(matrix, 
                DissimilarityFactory.getInstance(dissType));

        return projection;
    }
    
    @Override
    public String getName() {
        return ProjClusProjectionComp.class.getAnnotation(VisComponent.class).name();
    }

    @Override
    public String getShortName() {
        return "projclus";
    }
    
}
