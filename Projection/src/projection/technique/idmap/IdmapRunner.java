/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projection.technique.idmap;

import distance.dissimilarity.DissimilarityFactory;
import java.io.File;
import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import projection.util.ProjectionRunner;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractRunner;

/**
 *
 * @author Rafael M. Martins <rafael.martins@lnu.se>
 */
public class IdmapRunner extends ProjectionRunner {
    
    @AbstractRunner.Param(desc = "Fraction Delta", pos = 0)
    public float fractionDelta = 8.0f;

    @AbstractRunner.Param(desc = "Number of Iterations", pos = 1)
    public int numberOfIterations = 100;

    @AbstractRunner.Param(desc = "Initialization Type", pos = 2)
    public IDMAPProjection.InitializationType initType = IDMAPProjection.InitializationType.FASTMAP;
    
    @Param(desc = "Dissimilarity Type", pos = 3)
    public DissimilarityFactory.DissimilarityType dissType = DissimilarityFactory.DissimilarityType.EUCLIDEAN;

    @Override
    protected AbstractMatrix runProjection(File f) throws IOException {

        AbstractMatrix matrix = MatrixFactory.getInstance(f.getPath());

        IDMAPProjection idmap = new IDMAPProjection();
        
        idmap.setInitialization(initType);
        idmap.setFractionDelta(fractionDelta);
        idmap.setNumberIterations(numberOfIterations);

        AbstractMatrix projection = idmap.project(matrix, DissimilarityFactory.getInstance(dissType));
        

//            ProjectionModelComp model = new ProjectionModelComp();
//            model.input(projection);
//            model.execute();
//
//            ProjectionFrameComp frame = new ProjectionFrameComp();
//            frame.input(model.output());
//            frame.execute();

        return projection;
    }
    
    @Override
    public String getName() {
        return IDMAPProjectionComp.class.getAnnotation(VisComponent.class).name();
    }

    @Override
    public String getShortName() {
        return "idmap";
    }
    
}
