/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SammonMappingProjectionParamView.java
 *
 * Created on 19/06/2009, 14:21:42
 */
package projection.technique.sammon;

import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class SammonMappingProjectionParamView extends AbstractParametersView {

    /** Creates new form SammonMappingProjectionParamView */
    public SammonMappingProjectionParamView(SammonMappingProjectionComp comp) {
        initComponents();

        this.comp = comp;

        for (DissimilarityType disstype : DissimilarityType.values()) {
            this.dissimilarityComboBox.addItem(disstype);
        }

        reset();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        improvementPanel = new javax.swing.JPanel();
        nIterationsLabel = new javax.swing.JLabel();
        magicFactorLabel = new javax.swing.JLabel();
        magicFactorTextField = new javax.swing.JTextField();
        nIterationsTextField = new javax.swing.JTextField();
        dissimilarityPanel = new javax.swing.JPanel();
        dissimilarityComboBox = new javax.swing.JComboBox();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Sammon's Mapping Parameters"));
        setLayout(new java.awt.GridBagLayout());

        improvementPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Projection Improvement (Force)"));
        improvementPanel.setLayout(new java.awt.GridBagLayout());

        nIterationsLabel.setText("Number of iterations");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        improvementPanel.add(nIterationsLabel, gridBagConstraints);

        magicFactorLabel.setText("Magic Factor");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        improvementPanel.add(magicFactorLabel, gridBagConstraints);

        magicFactorTextField.setColumns(5);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        improvementPanel.add(magicFactorTextField, gridBagConstraints);

        nIterationsTextField.setColumns(5);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        improvementPanel.add(nIterationsTextField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(improvementPanel, gridBagConstraints);

        dissimilarityPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Dissimilarity"));
        dissimilarityPanel.setLayout(new java.awt.BorderLayout());
        dissimilarityPanel.add(dissimilarityComboBox, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(dissimilarityPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void reset() {
        comp.setNumberIterations(Math.max(comp.getNumberInstances(), comp.getNumberIterations()));

        this.magicFactorTextField.setText(Float.toString(comp.getMagicFactor()));
        this.nIterationsTextField.setText(Integer.toString(comp.getNumberIterations()));

        if (comp.isDistanceMatrixProvided()) {
            this.dissimilarityComboBox.setEnabled(false);
        } else {
            this.dissimilarityComboBox.setEnabled(true);
            this.dissimilarityComboBox.setSelectedItem(comp.getDissimilarityType());
        }
    }

    @Override
    public void finished() throws IOException {
        if (magicFactorTextField.getText().trim().length() > 0) {
            float mfactor = Float.parseFloat(magicFactorTextField.getText());
            if (mfactor > 0) {
                comp.setMagicFactor(mfactor);
            } else {
                throw new IOException("The magic fact shouldor be positive.");
            }
        } else {
            throw new IOException("The magic factor should be provided.");
        }

        if (nIterationsTextField.getText().trim().length() > 0) {
            int nit = Integer.parseInt(nIterationsTextField.getText());
            if (nit > -1) {
                comp.setNumberIterations(nit);
            } else {
                throw new IOException("The number of iterations should be positive.");
            }
        } else {
            throw new IOException("The number of iterations should be provided.");
        }

        if (!comp.isDistanceMatrixProvided()) {
            comp.setDissimilarityType((DissimilarityType) dissimilarityComboBox.getSelectedItem());
        }
    }

    private SammonMappingProjectionComp comp;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox dissimilarityComboBox;
    private javax.swing.JPanel dissimilarityPanel;
    private javax.swing.JPanel improvementPanel;
    private javax.swing.JLabel magicFactorLabel;
    private javax.swing.JTextField magicFactorTextField;
    private javax.swing.JLabel nIterationsLabel;
    private javax.swing.JTextField nIterationsTextField;
    // End of variables declaration//GEN-END:variables
}
