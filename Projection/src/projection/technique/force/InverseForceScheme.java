/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.technique.force;

import distance.DistanceMatrix;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;

/**
 *
 * @author paulovich
 */
public class InverseForceScheme {

    public AbstractMatrix execute(AbstractMatrix projection, AbstractMatrix matrix) throws IOException {
        DistanceMatrix dmatr2 = new DistanceMatrix(projection, new Euclidean());

        //Create the indexes and shuffle them
        ArrayList<Integer> index_aux = new ArrayList<Integer>();
        for (int i = 0; i < dmatr2.getElementCount(); i++) {
            index_aux.add(i);
        }

        this.index = new int[dmatr2.getElementCount()];
        for (int ind = 0, j = 0; j < this.index.length; ind += index_aux.size() / 10, j++) {
            if (ind >= index_aux.size()) {
                ind = 0;
            }

            this.index[j] = index_aux.get(ind);
            index_aux.remove(ind);
        }

        float[][] aux_matrix = matrix.toMatrix();

        for (int i = 0; i < nriterations; i++) {
            iteration(dmatr2, aux_matrix);
        }

        DenseMatrix newmatrix = new DenseMatrix();
        newmatrix.setAttributes(matrix.getAttributes());

        for (int i = 0; i < matrix.getRowCount(); i++) {
            newmatrix.addRow(new DenseVector(aux_matrix[i], matrix.getRow(i).getId(),
                    matrix.getRow(i).getKlass()), matrix.getLabel(i));
        }

        return newmatrix;
    }

    private void iteration(DistanceMatrix dmatr2, float[][] aux_matrix) {
        double error = 0;

        //for each instance
        for (int ins1 = 0; ins1 < aux_matrix.length; ins1++) {
            int instance1 = this.index[ins1];

            //for each other instance
            for (int ins2 = 0; ins2 < aux_matrix.length; ins2++) {
                int instance2 = this.index[ins2];

                if (instance1 == instance2) {
                    continue;
                }

                //distance between projected instances
                double diffvect[] = new double[aux_matrix[0].length];
                double drn = 0;
                for (int k = 0; k < diffvect.length; k++) {
                    diffvect[k] = aux_matrix[instance2][k] - aux_matrix[instance1][k];
                    drn += diffvect[k] * diffvect[k];
                }
                drn = Math.sqrt(drn);

                if (drn < EPSILON) {
                    drn = EPSILON;
                }

                double dr2 = dmatr2.getDistance(instance1, instance2);

                //Calculating the (fraction of) delta
                double delta = dr2 - drn;
                error += Math.abs(delta);
                //delta *= Math.abs(delta);
                //delta /= normfactor;
                //delta = (delta > 0) ? Math.sqrt(delta) : -Math.sqrt(Math.abs(delta));
                delta /= this.fractionDelta;

                //moving ins2 -> ins1
                for (int k = 0; k < diffvect.length; k++) {
                    aux_matrix[instance2][k] += delta * (diffvect[k] / drn);
                }
            }
        }

        error = error / ((dmatr2.getElementCount() * dmatr2.getElementCount()) - dmatr2.getElementCount());
        System.out.println("error: " + error);
    }

    public static void main(String[] args) throws IOException {
        String matrixfile = "/home/paulovich/iris-std.data";
        String projfile = "/home/paulovich/iris-std.prj.data";
        AbstractMatrix matrix = MatrixFactory.getInstance(matrixfile);
        AbstractMatrix projection = MatrixFactory.getInstance(projfile);

        InverseForceScheme ifs = new InverseForceScheme();
        AbstractMatrix newmatrix = ifs.execute(projection, matrix);
        newmatrix.save(matrixfile + "_inv.data");
    }

    private float fractionDelta = 1;
    private int[] index;
    private int nriterations = 100;
    private static final float EPSILON = 0.0000001f;
}
