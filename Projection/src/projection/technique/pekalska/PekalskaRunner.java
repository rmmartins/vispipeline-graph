/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projection.technique.pekalska;

import distance.dissimilarity.DissimilarityFactory;
import java.io.File;
import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import projection.util.ProjectionRunner;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractRunner;

/**
 *
 * @author Rafael M. Martins <rafael.martins@lnu.se>
 */
public class PekalskaRunner extends ProjectionRunner {
        
    @AbstractRunner.Param(desc = "Dissimilarity Type", pos = 0)
    public DissimilarityFactory.DissimilarityType dissType = DissimilarityFactory.DissimilarityType.EUCLIDEAN;

    @Override
    public AbstractMatrix runProjection(File f) throws IOException {        
        AbstractMatrix matrix = MatrixFactory.getInstance(f.getAbsolutePath());

        //ProjectionUtil.log(false, false);

        PekalskaProjection pp = new PekalskaProjection();
        AbstractMatrix projection = pp.project(matrix, 
                DissimilarityFactory.getInstance(dissType));

        return projection;
    }
    
    @Override
    public String getName() {
        return PekalskaProjectionComp.class.getAnnotation(VisComponent.class).name();
    }

    @Override
    public String getShortName() {
        return "pekalska";
    }
    
}
