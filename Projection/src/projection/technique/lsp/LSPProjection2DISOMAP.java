/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.technique.lsp;

import distance.DistanceMatrix;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import projection.model.ProjectionModelComp;
import projection.technique.isomap.ISOMAPProjection;
import projection.util.ProjectionUtil;
import projection.view.ProjectionFrameComp;

/**
 *
 * @author paulovich
 */
public class LSPProjection2DISOMAP extends LSPProjection2D {

    @Override
    protected AbstractMatrix projectControlPoints(DistanceMatrix dmat_cp) throws IOException {
        ISOMAPProjection isomap = new ISOMAPProjection();
        isomap.setNumberNeighbors(8);
        return isomap.project(dmat_cp);
    }

    public static void main(String args[]) throws IOException {
        String filename = "/home/paulovich/Dropbox/dados/swissroll10000.data";
        AbstractMatrix matrix = MatrixFactory.getInstance(filename);

        ProjectionUtil.log(false, false);

        long start = System.currentTimeMillis();
        LSPProjection2DISOMAP lsp = new LSPProjection2DISOMAP();
        lsp.setNumberNeighbors(8);
        lsp.setControlPointsChoice(ControlPointsType.RANDOM);
        lsp.setFractionDelta(8.0f);
        lsp.setNumberIterations(50);
        lsp.setNumberControlPoints(matrix.getRowCount() / 10);
        AbstractMatrix projection = lsp.project(matrix, new Euclidean());
        long finish = System.currentTimeMillis();
        System.out.println("LSP time: " + (finish - start) / 1000.0f + "s");

        ProjectionModelComp model = new ProjectionModelComp();
        model.input(projection);
        model.execute();

        ProjectionFrameComp frame = new ProjectionFrameComp();
        frame.input(model.output());
        frame.setTitle("LSP");
        frame.execute();
    }

}
