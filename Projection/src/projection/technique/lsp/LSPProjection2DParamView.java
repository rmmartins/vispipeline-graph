/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * LSPProjection2DParamView.java
 *
 * Created on 29/05/2009, 11:29:24
 */
package projection.technique.lsp;

import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class LSPProjection2DParamView extends AbstractParametersView {

    /** Creates new form LSPProjection2DParamView */
    public LSPProjection2DParamView(LSPProjection2DComp comp) {
        initComponents();

        this.comp = comp;

        for (DissimilarityType disstype : DissimilarityType.values()) {
            this.dissimilarityComboBox.addItem(disstype);
        }

        reset();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        improvementPanel = new javax.swing.JPanel();
        nIterationsLabel = new javax.swing.JLabel();
        deltaLabel = new javax.swing.JLabel();
        deltaTextField = new javax.swing.JTextField();
        nIterationsTextField = new javax.swing.JTextField();
        lspPanel = new javax.swing.JPanel();
        numberCPLabel = new javax.swing.JLabel();
        numberNeighborsLabel = new javax.swing.JLabel();
        numberCPTextField = new javax.swing.JTextField();
        numberNeighborsTextField = new javax.swing.JTextField();
        dissimilarityPanel = new javax.swing.JPanel();
        dissimilarityComboBox = new javax.swing.JComboBox();

        setBorder(javax.swing.BorderFactory.createTitledBorder("LSP Parameters"));
        setLayout(new java.awt.GridBagLayout());

        improvementPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Projection Improvement (Force)"));
        improvementPanel.setLayout(new java.awt.GridBagLayout());

        nIterationsLabel.setText("Number of iterations");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        improvementPanel.add(nIterationsLabel, gridBagConstraints);

        deltaLabel.setText("Fraction of delta");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        improvementPanel.add(deltaLabel, gridBagConstraints);

        deltaTextField.setColumns(5);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        improvementPanel.add(deltaTextField, gridBagConstraints);

        nIterationsTextField.setColumns(5);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        improvementPanel.add(nIterationsTextField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(improvementPanel, gridBagConstraints);

        lspPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("LSP parameters"));
        lspPanel.setLayout(new java.awt.GridBagLayout());

        numberCPLabel.setText("Number Control Points");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        lspPanel.add(numberCPLabel, gridBagConstraints);

        numberNeighborsLabel.setText("Number of Neighbors");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        lspPanel.add(numberNeighborsLabel, gridBagConstraints);

        numberCPTextField.setColumns(5);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        lspPanel.add(numberCPTextField, gridBagConstraints);

        numberNeighborsTextField.setColumns(5);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        lspPanel.add(numberNeighborsTextField, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(lspPanel, gridBagConstraints);

        dissimilarityPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Dissimilarity"));
        dissimilarityPanel.setLayout(new java.awt.BorderLayout());
        dissimilarityPanel.add(dissimilarityComboBox, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(dissimilarityPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void reset() {
        deltaTextField.setText(Float.toString(comp.getFractionDelta()));
        nIterationsTextField.setText(Integer.toString(comp.getNumberIterations()));

        int nrinstances = comp.getNumberInstances();
        comp.setNumberControlPoints(Math.max(nrinstances / 10, comp.getNumberControlPoints()));

        int nrneighbors = 0;
        if (comp.getNumberInstances() < 1500) {
            nrneighbors = 10;
        } else if (comp.getNumberInstances() < 5000) {
            nrneighbors = 15;
        } else {
            nrneighbors = 20;
        }

        comp.setNumberNeighbors(Math.max(nrneighbors, comp.getNumberNeighbors()));

        numberCPTextField.setText(Integer.toString(comp.getNumberControlPoints()));
        numberNeighborsTextField.setText(Integer.toString(comp.getNumberNeighbors()));

        if (comp.isDistanceMatrixProvided()) {
            dissimilarityComboBox.setEnabled(false);
        } else {
            dissimilarityComboBox.setEnabled(true);
            dissimilarityComboBox.setSelectedItem(comp.getDissimilarityType());
        }
    }

    @Override
    public void finished() throws IOException {
        if (deltaTextField.getText().trim().length() > 0) {
            float delta = Float.parseFloat(deltaTextField.getText());
            if (delta > 0) {
                comp.setFractionDelta(delta);
            } else {
                throw new IOException("The fraction of delta should be positive.");
            }
        } else {
            throw new IOException("The fraction of delta should be provided.");
        }

        if (nIterationsTextField.getText().trim().length() > 0) {
            int nit = Integer.parseInt(nIterationsTextField.getText());
            if (nit > -1) {
                comp.setNumberIterations(nit);
            } else {
                throw new IOException("The number of iterations should be positive.");
            }
        } else {
            throw new IOException("The number of iterations should be provided.");
        }

        if (numberCPTextField.getText().trim().length() > 0) {
            int ncp = Integer.parseInt(numberCPTextField.getText());
            if (ncp > 0) {
                comp.setNumberControlPoints(ncp);
            } else {
                throw new IOException("The number of control points should be positive.");
            }
        } else {
            throw new IOException("The number of control points should be provided.");
        }

        if (numberNeighborsTextField.getText().trim().length() > 0) {
            int nnei = Integer.parseInt(numberNeighborsTextField.getText());
            if (nnei > 0) {
                comp.setNumberNeighbors(nnei);
            } else {
                throw new IOException("The number of neighbors should be positive.");
            }
        } else {
            throw new IOException("The number of neighbors should be provided.");
        }

        if (!comp.isDistanceMatrixProvided()) {
            comp.setDissimilarityType((DissimilarityType) dissimilarityComboBox.getSelectedItem());
        }
    }

    private LSPProjection2DComp comp;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel deltaLabel;
    private javax.swing.JTextField deltaTextField;
    private javax.swing.JComboBox dissimilarityComboBox;
    private javax.swing.JPanel dissimilarityPanel;
    private javax.swing.JPanel improvementPanel;
    private javax.swing.JPanel lspPanel;
    private javax.swing.JLabel nIterationsLabel;
    private javax.swing.JTextField nIterationsTextField;
    private javax.swing.JLabel numberCPLabel;
    private javax.swing.JTextField numberCPTextField;
    private javax.swing.JLabel numberNeighborsLabel;
    private javax.swing.JTextField numberNeighborsTextField;
    // End of variables declaration//GEN-END:variables
}
