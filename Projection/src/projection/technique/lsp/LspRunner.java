/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projection.technique.lsp;

import distance.dissimilarity.DissimilarityFactory;
import java.io.File;
import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.MatrixFactory;
import projection.util.ProjectionRunner;
import projection.util.ProjectionUtil;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractRunner;

/**
 *
 * @author Rafael M. Martins <rafael.martins@lnu.se>
 */
public class LspRunner extends ProjectionRunner {
    
    @AbstractRunner.Param(desc = "Number of Neighbors", pos = 0)
    public int numberOfNeighbors = 8;
    
    @AbstractRunner.Param(desc = "Control Points Choice", pos = 1)
    public LSPProjection2D.ControlPointsType controlPointsChoice = LSPProjection2D.ControlPointsType.RANDOM;
    
    @AbstractRunner.Param(desc = "Fraction Delta", pos = 2)
    public float fractionDelta = 8.0f;

    @AbstractRunner.Param(desc = "Number of Iterations", pos = 3)
    public int numberOfIterations = 100;
    
    @AbstractRunner.Param(desc = "Dissimilarity Type", pos = 3)
    public DissimilarityFactory.DissimilarityType dissType = DissimilarityFactory.DissimilarityType.EUCLIDEAN;

    @Override
    public AbstractMatrix runProjection(File f) throws IOException {
        
        AbstractMatrix matrix = MatrixFactory.getInstance(f.getAbsolutePath());

        ProjectionUtil.log(false, false);

        long start = System.currentTimeMillis();
        LSPProjection2D lsp = new LSPProjection2D();
        
        lsp.setNumberNeighbors(numberOfNeighbors);
        lsp.setControlPointsChoice(LSPProjection2D.ControlPointsType.RANDOM);
        lsp.setFractionDelta(fractionDelta);
        lsp.setNumberIterations(numberOfIterations);
        lsp.setNumberControlPoints(matrix.getRowCount() / 10);
        
        AbstractMatrix projection = lsp.project(matrix, DissimilarityFactory.getInstance(dissType));
        
        long finish = System.currentTimeMillis();
        System.out.println("LSP time: " + (finish - start) / 1000.0f + "s");

//        ProjectionModelComp model = new ProjectionModelComp();
//        model.input(projection);
//        model.execute();
//
//        ProjectionFrameComp frame = new ProjectionFrameComp();
//        frame.input(model.output());
//        frame.setTitle("LSP");
//        frame.execute();

        return projection;
    }
    
    @Override
    public String getName() {
        return LSPProjection2DComp.class.getAnnotation(VisComponent.class).name();
    }

    @Override
    public String getShortName() {
        return "lsp";
    }
    
}
