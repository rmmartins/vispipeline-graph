
package projection.technique.lsp;

import datamining.clustering.Clustering;
import datamining.clustering.Kmeans;
import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import projection.technique.lsp.LSPProjection2D.ControlPointsType;
import textprocessing.corpus.zip.ZipCorpus;
import textprocessing.processing.Preprocessor;
import textprocessing.processing.stemmer.StemmerFactory;


public class LSPProjection2DTest {
        
    @Before
    public void setup() throws Exception {
        String url = this.getClass().getResource("../../../iv04dataset.zip").getPath();
        corpus = new ZipCorpus(url, 1);
        preproc = new Preprocessor(corpus);
        matrix = preproc.getMatrix(lowerCut, upperCut, numberGrams, stemmer);
    }

    @Test
    public void execute() throws IOException {        
        //projecting
        LSPProjection2D lsp = new LSPProjection2D();
        lsp.setFractionDelta(fracdelta);
        lsp.setNumberIterations(nriterations);
        lsp.setNumberNeighbors(nrneighbors);
        lsp.setNumberControlPoints(nrcontrolpoints);
        AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
        if (matrix != null) { //using a matrix            
            lsp.setControlPointsChoice(ControlPointsType.KMEANS);
            
            if (controlpoints != null && cpprojection != null) {
                lsp.setControlPoints(controlpoints);
                lsp.setControlPointsProjection(cpprojection);
            }            
            projection = lsp.project(matrix, diss);
        } else if (dmat != null) { //using a distance matrix
            lsp.setControlPointsChoice(ControlPointsType.KMEDOIDS);
            projection = lsp.project(dmat);
        } else {
            throw new IOException("A distance matrix or a points matrix should "
                    + "be provided.");
        }
        assertNotNull(projection);
        
        // Adding labels
        ArrayList<String> labels = new ArrayList<String>();
        for (int i = 0; i < projection.getRowCount(); i++) {
            labels.add(corpus.idToUrl(projection.getRow(i).getId()));            
        }
        
        // Adding clustering as cdata
        Clustering clt = new Kmeans(10);
        ArrayList<ArrayList<Integer>> clusters = clt.execute(new Euclidean(), 
                projection);
        for (ArrayList<Integer> cluster : clusters) {
            for (Integer inst : cluster) {
                projection.getRow(inst).setKlass(clusters.indexOf(cluster));
            }
        }
        
        projection.setLabels(labels);        
        projection.save("projection.txt");
    }
    
    public static final long serialVersionUID = 1L;
    private int nriterations = 50;
    private float fracdelta = 8.0f;
    private int nrneighbors = 10;
    private int nrcontrolpoints = 10;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private transient LSPProjection2DParamView paramview;
    private transient AbstractMatrix projection;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
    private transient ArrayList<Integer> controlpoints;
    private transient AbstractMatrix cpprojection;
    private int lowerCut = 10;
    private int upperCut = -1;
    private int numberGrams = 1;
    private StemmerFactory.StemmerType stemmer = StemmerFactory.StemmerType.ENGLISH;    
    private ZipCorpus corpus;
    private Preprocessor preproc;
    private int nrlines = 1;
}
