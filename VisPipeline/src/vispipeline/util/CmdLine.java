/*
 * The MIT License
 *
 * Copyright 2018 Rafael M. Martins <rafael.martins@lnu.se>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package vispipeline.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import vispipeline.component.ComponentsLoader;
import vispipelinebasics.interfaces.AbstractRunner;

/**
 *
 * @author Rafael M. Martins <rafael.martins@lnu.se>
 */
public class CmdLine {
    
    public static void main(String[] args) throws IOException {
        
        ComponentsLoader comploader = new ComponentsLoader();
        comploader.execute();
        
        List<AbstractRunner> runners = comploader.getRunnerClasses();
        
        if (args.length < 1 || "--help".equals(args[0]) || "-h".equals(args[0])) {
            System.out.println("VisPipeline Projection Runner");
            System.out.println("---------------");
            System.out.println("Usage: vp-run <projection> [arguments]");
            System.out.println("---------------");
            System.out.println("The following projections are available:");
            for (AbstractRunner runner : runners) {
                System.out.println(" * " + runner.getShortName() + "  \t [ " + 
                        runner.getName() + " ]");
            }
        } else {
          for (AbstractRunner runner : runners) {
              if (runner.getShortName().equals(args[0])) {
                  runner.run(Arrays.copyOfRange(args, 1, args.length));
              }
          }          
        }
        
    }
    
}
