/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline;

import vispipeline.view.VisPipeline;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new VisPipeline().setVisible(true);
            }

        });
    }

}
