/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph.layout;

import distance.DistanceMatrix;
import distance.dissimilarity.CosineBased;
import graph.ProjectionData;
import graph.input.VNAGraphData;
import graph.model.Connectivity;
import java.io.IOException;
import java.util.HashSet;
import matrix.AbstractMatrix;
import org.junit.Before;
import org.junit.Test;
import projection.technique.Projection;
import projection.technique.idmap.IDMAPProjection;

/**
 *
 * @author rmartins
 */
public class ConnectivityProjectionLayoutTest {

    @Before
    public void setup() throws Exception {
        String path = this.getClass().getResource("../iv04dataset.vna").getPath();
        gdata = new VNAGraphData(path, true);
    }

    @Test
    public void testExecute() throws Exception {
        if (gdata != null) {
            // parameters
            pdata = new ProjectionData();
            pdata.setShortestPath(false);
            pdata.setWeightAsDistance(false);
            pdata.setIgnoreWeight(false);            
            pdata.setDirectedConection(true);
            HashSet<Integer> conns = new HashSet<Integer>();
            conns.add(0);
            pdata.setConnectivitiesToConsider(conns);
            // Adjacency Matrix
            ConnectivityProjectionLayout connProj = new ConnectivityProjectionLayout();
            adjMatrix = connProj.execute(gdata, pdata);
            // Projection
            Projection projTech = new IDMAPProjection();
            projection = projTech.project(adjMatrix, new CosineBased());
        } else {
            throw new IOException("A VNA or a BIB should be provided.");
        }        
    }
    VNAGraphData gdata;
    ProjectionData pdata;
    AbstractMatrix adjMatrix;
    AbstractMatrix projection;
    DistanceMatrix dmat;
    Connectivity conn;
}
