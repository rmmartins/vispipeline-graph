/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph.input;

import graph.model.Connectivity;
import graph.model.Edge;
import textprocessing.corpus.zip.ZipCorpus;
import java.io.File;
import java.io.IOException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rmartins
 */
public class GraphDataReaderCompTest {

    GraphDataReaderComp instance;
    String input, input2;

    @Before
    public void setUp() {
        instance = new GraphDataReaderComp();
        input = getClass().getResource("../test.bib").getPath();
        input2 = getClass().getResource("../test2.bib").getPath();
    }

//    @AfterClass
    public static void tearDownClass() {
        try {
            new File(GraphDataReaderComp.class.getResource(
                    "../test_format-hetero.vna").getPath()).delete();
            new File(GraphDataReaderComp.class.getResource(
                    "../test_format.bib").getPath()).delete();
            new File(GraphDataReaderComp.class.getResource(
                    "../test_format.inv").getPath()).delete();
            new File(GraphDataReaderComp.class.getResource(
                    "../test_format.zip").getPath()).delete();
        } catch (NullPointerException e) {
            System.err.println("Temp. files not found to delete...?");
        }
    }

    /**
     * Test of execute method, of class GraphDataReaderComp.
     */
    @Test(expected = IOException.class)
    public void testExceptionWithoutInput() throws Exception {
        instance.execute();
    }

    /**
     * Test of outputGraphData method, of class GraphDataReaderComp.
     */
    @Test
    public void testOutputGraphDataNotNull() throws Exception {
        instance.setFilename(input);
        instance.execute();
        assertNotNull(instance.outputGraphData());
    }

    /**
     * Test of outputCorpus method, of class GraphDataReaderComp.
     */
    @Test
    public void testOutputCorpus() throws Exception {
        instance.setFilename(input);
        instance.execute();
        assertNotNull(instance.outputCorpus());
    }

    /**
     * Test of outputConnectivities method, of class GraphDataReaderComp.
     */
    @Test
    public void testOutputConnectivities() throws Exception {
        instance.setFilename(input);
        instance.execute();
        assertNotNull(instance.outputConnectivities());
    }

    /**
     * Test of getParametersEditor method, of class GraphDataReaderComp.
     */
    @Test
    public void testGetParametersEditor() {
        assertNotNull(instance.getParametersEditor());
    }

    /**
     * Test of reset method, of class GraphDataReaderComp.
     */
    @Test
    public void testReset() throws Exception {
        instance.reset();
        assertNull(instance.outputConnectivities());
        assertNull(instance.outputCorpus());
        assertNull(instance.outputGraphData());
    }

    @Test
    public void testBibGraphNodes() throws Exception {
        instance.setFilename(input);
        instance.execute();
        GraphData gdata = instance.outputGraphData();
        // Three authors and one paper
        assertEquals(4, gdata.getIds().size());
        String[] ids = {"Adamy, U.", "Giesen, J.", "John, M.", "adamygj00"};
        assertArrayEquals(ids, gdata.getKeys().toArray());
    }
    
    @Test
    public void testBibGraphEdges() throws Exception {
        instance.setFilename(input);
        instance.execute();
        GraphData gdata = instance.outputGraphData();
        // Edges between the authors and the paper
        Edge[] expected = {new Edge(0, 3, 1.0f), new Edge(1, 3, 1.0f), 
            new Edge(2, 3, 1.0f)};
        Connectivity conn = gdata.getConnectivities().get(0);
        assertArrayEquals(expected, conn.getEdges().toArray());
        // Edges between the three authors
        expected = new Edge[] {new Edge(0, 1, 1.0f), new Edge(0, 2, 1.0f), 
            new Edge(1, 2, 1.0f)};
        conn = gdata.getConnectivities().get(1);        
        assertArrayEquals(expected, conn.getEdges().toArray());
        // Edges between papers - need a new test file
    }
    
    @Test
    public void testBibCorpusContent() throws Exception {
        instance.setFilename(input);
        instance.execute();        
        ZipCorpus corpus = (ZipCorpus) instance.outputCorpus();
        String expected = "New techniques for topologically correct surface "
                + "reconstruction\r\n\r\nAdamy, U. and Giesen, J. and John, M.\r\n";
        assertEquals(expected, corpus.getFullContent("adamygj00"));
    }
    
    @Test
    public void testGraphDataCorpus() throws Exception {
        instance.setFilename(input2);
        instance.execute();
        assertEquals(76, instance.outputGraphData().getCorpus().getIds().size());        
    }
    
    @Test
    public void testNgrams() throws IOException, CloneNotSupportedException {
        instance.setFilename(input2);
        instance.execute();
        ZipCorpus corpus = (ZipCorpus) instance.outputCorpus();
        for (int id : corpus.getIds()) {
            System.out.println(id + ": ngrams " + corpus.getNgrams(id));
        }
    }
}
