/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph.input;


import graph.model.ScalarsFromAttributesTable;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author rmartins
 */
public class GraphDataTest {

    GraphDataReaderComp instance;
    String input;
    VNAGraphData gdata;

    @Before
    public void setUp() throws Exception {
        instance = new GraphDataReaderComp();
        input = getClass().getResource("../test2.bib").getPath();
        instance.setFilename(input);
        instance.execute();
        gdata = instance.outputGraphData();
    }
    
    @Test
    public void testScalarFromAttributes() {
        ScalarsFromAttributesTable table = gdata.getScalarsFromAttributesTable();
        assertNotNull(table);
        String expected = "[id, node_type, name, weight, rank, authors, year, "
                + "paper_type, source, keywords, abstract]";
        assertEquals(expected, 
                table.getScalarsByIndexTuple(0).getScalarList().toString());
    }
    
    @Test
    public void testTitles() {
        String expected = "[id, node_type, name, weight, rank, authors, year, "
                + "paper_type, source, keywords, abstract]";
        assertEquals(expected, gdata.getTitles().toString());
    }
    
    @Test
    public void testCorpusNgrams() throws Exception {
        assertEquals(2601, gdata.getCorpusNgrams().size());
    }
    
    @Test
    public void testInstancesNgrams() throws Exception {
        assertNotSame("", gdata.getNgrams(0));
    }
}
