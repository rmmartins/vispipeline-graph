/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph.input.bib;

import bibtex.dom.BibtexEntry;
import bibtex.dom.BibtexFile;
import bibtex.parser.BibtexParser;
import java.io.File;
import java.io.FileReader;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rmartins
 */
public class BibGraphUtilsTest {
    
    private File input;    
    BibtexParser bibParser;

    @Before
    public void setUp() {
        input = new File(getClass().getResource("../../test.bib").getPath());        
        bibParser = new BibtexParser(true);
    }
    
    @AfterClass
    public static void tearDownClass() {
        try {            
            new File(BibGraphUtils.class.getResource(
                    "../../test_format.bib").getPath()).delete();
        } catch (NullPointerException e) {
            System.err.println("Temp. files not found to delete...?");
        }
    }
    
    /**
     * Test of formatKey method, of class BibGraphUtils.
     */
    @Test
    public void testAuthorsFormatting() throws Exception {
        input = BibGraphUtils.formatInput(input);
        BibtexFile output = new BibtexFile();
        bibParser.parse(output, new FileReader(input));
        BibtexEntry entry = (BibtexEntry) output.getEntries().get(0);
        assertEquals("{Adamy, U. and Giesen, J. and John, M.}", 
                entry.getFieldValue("author").toString());
    }
}
