/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph.model;

import graph.input.GraphData;
import graph.util.CentralityMeasures;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Random;
import projection.model.ProjectionModel;
import projection.model.Scalar;
import visualizationbasics.color.ColorTable;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class GraphModel extends ProjectionModel {

    public GraphModel() {
        this.connectivities = new ArrayList<Connectivity>();
        this.selsconn = null;
        this.instancesize = 4;
        this.scalars = new ArrayList<Scalar>();
        this.initialInstances = new ArrayList<AbstractInstance>();
        this.selSizeScalar = null;
    }

    @Override
    public void draw(BufferedImage image, boolean highquality) {
        if (image != null) {
            if (selsconn != null) {
                selsconn.draw(this, image, highquality);
            }

            //first draw the non-valid instances
//            for (int i = 0; i < instances.size(); i++) {
//                GraphInstance gi = (GraphInstance) instances.get(i);
//                if (!gi.isValid()) {
//                    gi.draw(image, highquality);
//                }
//            }

            // if nothing is selected, high values should be over low values
            if (getSelectedInstances().isEmpty()) {
                //Scalar selscalar = getSelectedScalar();
                ArrayList<AbstractInstance> toDraw = new ArrayList<>(instances);
                Collections.sort(toDraw, new Comparator<AbstractInstance>() {

                    @Override
                    public int compare(AbstractInstance o1, AbstractInstance o2) {
                        float v1 = ((GraphInstance) o1).getScalarValue(selscalar);
                        float v2 = ((GraphInstance) o2).getScalarValue(selscalar);
                        return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
                    }
                });
                
                for (AbstractInstance inst : toDraw)
                    ((GraphInstance)inst).draw(image, highquality);
                
            } else {
                //first draw the non-selected instances
                for (int i = 0; i < instances.size(); i++) {
                    GraphInstance gi = (GraphInstance) instances.get(i);
                    if (gi.isValid() && !gi.isSelected()) {
                        gi.draw(image, highquality);
                    }
                }

                //then the selected instances
                for (int i = 0; i < instances.size(); i++) {
                    GraphInstance gi = (GraphInstance) instances.get(i);
                    if (gi.isValid() && gi.isSelected()) {
                        gi.draw(image, highquality);
                    }
                }
            }

        }
    }

    public ArrayList<AbstractInstance> getValidSelectedInstances() {
        ArrayList selinsts = this.getSelectedInstances();
        ArrayList<AbstractInstance> valids = new ArrayList<AbstractInstance>();
        if (selinsts != null) {
            for (int i = 0; i < selinsts.size(); i++) {
                if (((GraphInstance) (selinsts.get(i))).isValid()) {
                    valids.add((AbstractInstance) selinsts.get(i));
                }
            }
        }
        return valids;
    }

    public ArrayList<AbstractInstance> getValidInstances() {
        ArrayList selinsts = this.instances;// getInstances();
        ArrayList<AbstractInstance> valids = new ArrayList<AbstractInstance>();
        if (selinsts != null) {
            for (int i = 0; i < selinsts.size(); i++) {
                if (((GraphInstance) (selinsts.get(i))).isValid()) {
                    valids.add((AbstractInstance) selinsts.get(i));
                }
            }
        }
        return valids;
    }

    public Connectivity getSelectedConnectivity() {
        return selsconn;
    }

    public void setSelectedConnectivity(Connectivity conn) {
        if (connectivities.contains(conn)) {
            selsconn = conn;
        } else {
            selscalar = null;
        }

        setChanged();
    }

    public ArrayList<Connectivity> getConnectivities() {
        return this.connectivities;
    }

    public Connectivity getConnectivityByName(String name) {
        for (Connectivity c : this.connectivities) {
            if (c.getName().equals(name)) {
                return c;
            }
        }
        return null;
    }

    public void addConnectivity(Connectivity connectivity) {
        this.removeConnectivity(connectivity);
        this.connectivities.add(connectivity);
        connectivity.updateGraphInstancesFromEdges(this);
    }

    public void removeConnectivity(Connectivity connectivity) {
        this.connectivities.remove(connectivity);
    }

    /**
     * Returns an instance by its id
     * @param id id of the instance
     * @return the instance that has the id, or null if this instance is not on the list
     */
    public GraphInstance getInstanceById(int id) {
        // sometimes the 'id' actually means index
        if (id >= 0 && id < instances.size()) {
            return (GraphInstance) instances.get(id);
        }
        // or not
        for (int i = 0; i < this.instances.size(); i++) {
            if (this.instances.get(i).getId() == id) {
                return (GraphInstance) this.instances.get(i);
            }
        }
        return null;
    }

    public void perturb() {
        Random rand = new Random(7);

        float maxx = Float.NEGATIVE_INFINITY;
        float minx = Float.POSITIVE_INFINITY;
        float maxy = Float.NEGATIVE_INFINITY;
        float miny = Float.POSITIVE_INFINITY;

        for (int i = 0; i < instances.size(); i++) {
            GraphInstance pi = (GraphInstance) instances.get(i);

            if (maxx < pi.getX()) {
                maxx = pi.getX();
            }

            if (minx > pi.getX()) {
                minx = pi.getX();
            }

            if (maxy < pi.getY()) {
                maxy = pi.getY();
            }

            if (miny > pi.getY()) {
                miny = pi.getY();
            }
        }

        float diffx = (maxx - minx) / 1000;
        float diffy = (maxy - miny) / 1000;

        for (int i = 0; i < instances.size(); i++) {
            GraphInstance pi = (GraphInstance) instances.get(i);

            pi.setX(pi.getX() + diffx * rand.nextFloat());
            pi.setY(pi.getY() + diffy * rand.nextFloat());
        }
    }

    public void createConnectivities() {
//        try {
//            //Creating a Delaunay triangulation
//            float[][] projection = new float[instances.size()][];
//            for (int i = 0; i < projection.length; i++) {
//                projection[i] = new float[2];
//                projection[i][0] = instances.get(i).getX();
//                projection[i][1] = instances.get(i).getY();
//            }
//
//            //perturbing equal vertices
//            perturb();
//
//            try {
//                Delaunay d = new Delaunay();
//                Pair[][] neighborhood = d.execute(projection);
//                Connectivity con = new Connectivity(GraphConstants.DELAUNAY);
//                con.create(instances, neighborhood);
//                addConnectivity(con);
//            } catch (IllegalArgumentException ex) {
//                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//            }
//
//            int knnNumberNeighbors = 2; // Creating the KNN-R2 connectivity...
//            DenseMatrix dproj = new DenseMatrix();
//            for (int i = 0; i < projection.length; i++) {
//                dproj.addRow(new DenseVector(projection[i]));
//            }
//
//            String conname = "KNN-R2-" + knnNumberNeighbors;
//            Connectivity knnr2Con = new Connectivity(conname);
//            ANN appknnr2 = new ANN(knnNumberNeighbors);
//            Pair[][] neighborhood = appknnr2.execute(dproj, new Euclidean());
//            knnr2Con.create(instances, neighborhood);
//            addConnectivity(knnr2Con);
//        } catch (IOException ex) {
//            Logger.getLogger(GraphModel.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    public void calculateScalarsFromSelectedConn() throws Exception {
        Connectivity selConn = getSelectedConnectivity();
        if (selConn == null || (selConn.getEdges().isEmpty())) {
            throw new Exception("You must select a non-empty connectivity first.");
        }
        String name = getSelectedConnectivity().getName();
        Scalar s1 = this.addScalar("betweeness: " + name);
        Scalar s2 = this.addScalar("closeness: " + name);
        Scalar s3 = this.addScalar("degree: " + name);
        Scalar s4 = this.addScalar("clustering: " + name);

        ArrayList<Integer> ids = new ArrayList<Integer>();
        for (int i = 0; i < instances.size(); i++) {
            ids.add(instances.get(i).getId());
        }
        CentralityMeasures cm = new CentralityMeasures(ids, getSelectedConnectivity());

        for (AbstractInstance gi : this.instances) {
            ((GraphInstance) gi).setScalarValue(s1, cm.findBetweennessOf(gi.getId()).floatValue());
            ((GraphInstance) gi).setScalarValue(s2, cm.findClosenessOf(gi.getId()).floatValue());
            ((GraphInstance) gi).setScalarValue(s3, cm.findDegreeOf(gi.getId()));
            ((GraphInstance) gi).setScalarValue(s4, cm.findClusteringOf(gi.getId()).floatValue());
        }
//        System.out.println("Calculou centrality measures");
        this.setChanged();
        this.notifyObservers();
    }

    public void setScalars(ArrayList<Scalar> sc) {
        this.scalars = sc;
        this.setChanged();
        this.notifyObservers();
    }
    protected Connectivity selsconn;
    protected ArrayList<Connectivity> connectivities;

    public void setInstances(ArrayList<AbstractInstance> inst) {
        this.instances = inst;
        for (Connectivity conn : this.connectivities) {
            conn.updateGraphInstancesFromEdges(this);
        }
        this.setChanged();
    }

    public void setColortable(ColorTable colorTable) {
        this.colortable = colorTable;
        this.setChanged();
    }

    public void showOnlySelectedInstances() {

        if (this.selinstances != null && !this.selinstances.isEmpty()) {
            for (AbstractInstance gi : this.instances) {
                ((GraphInstance) gi).setEnabled(false);
            }

            for (AbstractInstance gi : this.selinstances) {
                ((GraphInstance) gi).setEnabled(true);
            }

            this.setChanged();
            this.notifyObservers();
        }
    }

    public void showAllInstances() {
        for (AbstractInstance gi : this.instances) {
            ((GraphInstance) gi).setEnabled(true);
            ((GraphInstance) gi).setShowLabel(false);

        }
        this.setChanged();
        this.notifyObservers();
    }

    public Connectivity getSelsconn() {
        return selsconn;
    }

    public void setSelsconn(Connectivity selsconn) {
        this.selsconn = selsconn;
    }

    public Scalar getSelectedSizeScalar() {
        return selSizeScalar;
    }

    public void setSelectedSizeScalar(Scalar scalar) {
        if (scalars.contains(scalar)) {
            selSizeScalar = scalar;

            //change the size of each instance
            for (int i = 0; i < instances.size(); i++) {
                GraphInstance gi = (GraphInstance) instances.get(i);
                if (scalar.getMin() >= 0.0f && scalar.getMax() <= 1.0f) {
                    //System.out.println(gi.getId() + " : " + gi.getScalarValue(scalar));
                    gi.setSizeFactor(gi.getScalarValue(scalar));
                } else {
                    //System.out.println(gi.getId() + " : " + gi.getScalarValue(scalar));
                    gi.setSizeFactor(gi.getNormalizedScalarValue(scalar));
                }
            }
        } else {
            selscalar = null;

            //change the size of each instance
            for (int i = 0; i < instances.size(); i++) {
                GraphInstance gi = (GraphInstance) instances.get(i);
                gi.setSizeFactor(0);
            }
        }

        setChanged();
    }

    @Override
    public void removeInstances(ArrayList<AbstractInstance> reminst) {
        HashSet<Integer> remids = new HashSet<Integer>();
        for (AbstractInstance ins : reminst) {
            if (ins.getModel() == this) {
                remids.add(ins.getId());
            }
        }

        ArrayList<AbstractInstance> newins = new ArrayList<AbstractInstance>();


        for (int i = 0; i < instances.size(); i++) {
            if (!remids.contains(instances.get(i).getId())) {
                newins.add(instances.get(i));

            } else {
                instances.get(i).setModel(null);

            }
        }

        instances = newins;
        
        this.updateAllConnectivities();
        
        setChanged();
    }

    @Override
    public void removeSelectedInstances() {
        removeInstances(selinstances);
        selinstances.clear();
        setChanged();        
    }
    
    public void updateAllConnectivities(){
         for (Connectivity conn : this.getConnectivities()) {
            conn.updateGraphInstancesFromEdges(this);
        }
    }
    
    int instancesize;

    public int getInstanceSize() {
        return instancesize;
    }

    public void setInstanceSize(int instancesize) {
        this.instancesize = instancesize;
    }
    
    GraphData graphData;

    public GraphData getGraphData() {
        return graphData;
    }

    public void setGraphData(GraphData graphData) {
        this.graphData = graphData;
    }

    public float getEdgeAlpha() {
        return edgeAlpha;
    }

    public void setEdgeAlpha(float edgeAlpha) {
        this.edgeAlpha = edgeAlpha;
        notifyObservers();
    }
    
    private ArrayList<AbstractInstance> initialInstances;
    protected Scalar selSizeScalar;
    private float edgeAlpha = 0.3f;
}
