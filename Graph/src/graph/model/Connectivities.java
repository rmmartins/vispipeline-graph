/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package graph.model;

import java.util.ArrayList;

/**
 *
 * @author Henry
 */
public class Connectivities extends ArrayList<Connectivity>{

    public Connectivities() {
    }

    @Override
    public Connectivities clone() {
        Connectivities conns = new Connectivities();
        for (Connectivity conn : this){
            ArrayList<Edge> newedges = new ArrayList<Edge>();
            for (Edge e : conn.getEdges()){
                newedges.add (new Edge(e.getSource(), e.getTarget(), e.getWeight()));                       
            }
            Connectivity newconn =  new Connectivity(conn.getName(), newedges);
            newconn.setShowArrow(conn.isShowArrow());
            conns.add(newconn);
        }
        
        return conns;
    }
    
    public void updateallEdgesFromAllConnectivities(GraphModel model){
        for(Connectivity conn: this){
            conn.updateGraphInstancesFromEdges(model);
        }
    }

}
