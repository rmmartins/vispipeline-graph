/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package graph.model;


import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import projection.model.Scalar;
import projection.util.ProjectionConstants;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Graph.Basics",
name = "Graph Model",
description = "Create a graph model to be visualized.")
public class GraphModelComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        ScalarsFromAttributes scalars;
        ArrayList<Scalar> scalarTable;
        Scalar sc;
        if (placement != null) {
            model = new GraphModel();
            Scalar cdata = model.addScalar(ProjectionConstants.CDATA); //string (name)
            Scalar dots = model.addScalar(ProjectionConstants.DOTS);

            int nrows = placement.getRowCount();

            for (int i = 0; i < nrows; i++) {
                AbstractVector row = placement.getRow(i);
                GraphInstance pi = new GraphInstance(
                        row.getId(),
                        row.getValue(0), row.getValue(1));
                pi.setModel(model);
                pi.setScalarValue(cdata, row.getKlass());
                pi.setScalarValue(dots, 0.0f);
                
                if (this.attributesTable != null) {
                    scalars = this.attributesTable.getScalarsByIndexTuple(i);
                    for (Scalar scalar : scalars.getScalarList()) {
                        try {
                            model.addScalar(scalar.getName());
                            pi.setScalarValue(scalar, scalars.getScalarValue(scalar));
                        } catch (Exception ex) {
                            Logger.getLogger(GraphModelComp.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }

            //adding the connectivities
            Connectivity dotsCon = new Connectivity(ProjectionConstants.DOTS, new ArrayList<Edge>());
            model.addConnectivity(dotsCon);

            if (this.conns != null) {
                for (int i = 0; i < conns.size(); i++) {
                    model.addConnectivity(conns.get(i));
                }
            }
        } else {
            throw new IOException("A 2D position should be provided.");
        }
    }

    public void input(@Param(name = "2D placement") AbstractMatrix placement) {
        this.placement = placement;
    }

    public void attach(@Param(name = "connectivity") Connectivities conns) {
        if (conns == null) {
            this.conns = new Connectivities();
        }else{
            this.conns = conns;
        }
    }

    public void attach(@Param(name = "connectivity") Connectivity conn) {
        if (conns == null) {
            this.conns = new Connectivities();
        }

        if (conn != null) {
            this.conns.add(conn);
        }
    }

    public void attach(@Param(name = "GraphScalars from attributes") ScalarsFromAttributesTable attributes){
        if(attributes == null){
            this.attributesTable = new ScalarsFromAttributesTable();
        }else{
            this.attributesTable = attributes;
        }
    }

    public GraphModel output() {
        return model;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        placement = null;
        model = null;
        conns = null;
        attributesTable = null;
    }

    public static final long serialVersionUID = 1L;
    private transient GraphModel model;
    private transient AbstractMatrix placement;
    private transient Connectivities conns;
    private transient ScalarsFromAttributesTable attributesTable;
}
