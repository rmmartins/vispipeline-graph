/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph.model;

import java.io.Serializable;
import java.util.ArrayList;
import projection.model.Scalar;

/**
 *
 * @author Henry Heberle
 */
public class ScalarsFromAttributes implements Serializable {        
    //armazena scalares de uma istancia(tupla)
    int rowId;
    ArrayList<Float> scalarsValues;
    ArrayList<Scalar> scalars;
    
    public ScalarsFromAttributes(int id) {
        //attribute id is not necessary, but for now it doesn't give problem; unless != count        
        this.rowId = id;
        this.scalarsValues = new ArrayList<>();
        this.scalars = new ArrayList<>();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        ScalarsFromAttributes clone = new ScalarsFromAttributes(this.rowId);
        clone.scalars = (ArrayList<Scalar>) this.scalars.clone();
        clone.scalarsValues = (ArrayList<Float>) this.scalarsValues.clone();
        return clone;
    }
    
    
    
    public void setScalar(Scalar scalar, Float value) {
        int index;
        if (scalar != null) {
            if (!this.scalars.contains(scalar)) {
                scalar.store(value);
                this.scalarsValues.add(value);
                this.scalars.add(scalar);
            }else{
                index = this.scalars.indexOf(scalar);
                this.scalarsValues.set(index, value);
                scalar.store(value);
                scalar.updateMinMax(scalarsValues);
            }
        }
    }

    public ArrayList<Scalar> getScalarList() {
        return this.scalars;
    }
    
    public float getScalarValue(Scalar scalar) {
        if (scalar != null && this.scalars.size() > scalar.getIndex() && scalar.getIndex() > -1) {            
            return this.scalarsValues.get(this.scalars.indexOf(scalar));
        } else {
            return 0.0f;
        }
    }
    
    public float getNormalizedScalarValue(Scalar scalar) {
        if (scalar != null) {
            if (scalars.size() > scalar.getIndex() && scalar.getIndex() > -1) {
                float value = this.scalarsValues.get(this.scalars.indexOf(scalar));
                return (value - scalar.getMin()) / (scalar.getMax() - scalar.getMin());
            }
        }        
        return 0.0f;
    }
    
    public int getId() {
        return this.rowId;
    }
}
