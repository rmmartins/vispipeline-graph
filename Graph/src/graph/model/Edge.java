/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */
package graph.model;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Point;
import java.io.Serializable;
import projection.model.ProjectionInstance;

/**
 * @author Fernando Vieira Paulovich
 *
 * This class represents a edge on the map.
 */
public class Edge implements Comparable, Serializable {

    public static final float NO_SIZE = -1;
    private static final long serialVersionUID = 1L;
    private boolean showArrow = false;
    private boolean showLength = false;
    private boolean colorAsWeight = false;
    private boolean calculateColor = true;

    /**
     * Constructor of the edge
     * 
     * @param length The edge's lenght
     * @param source The first vertex
     * @param target The second vertex
     */
    public Edge(GraphInstance source, GraphInstance target, float weight) {
        this(source, target);
        this.weight = weight;
    }

    public Edge(int source, int target, float weight) {
        this.sourceInt = source;
        this.targetInt = target;
        this.weight = weight;
    }

    //IMPORTANT
    //always call this method for each connectivity has got ready in GraphModel, etc
    //when you need an ID: CALL getSourceID or getTargetID;  source can be null
    //when you call getSource().getId();
    //GraphModel and Connectivity has a method that call this for each edge in 
    //the model or in the connectivity
    public void updateSourceTargetInstances(GraphModel model) {
        this.source = model.getInstanceById(sourceInt);
        this.target = model.getInstanceById(targetInt);
    }

    public GraphInstance getSourceInst() {
        return this.source;
    }

    public GraphInstance getTargetInst() {
        return this.target;
    }

    /**
     * Constructor of the edge
     * 
     * @param source The first vertex
     * @param target The second vertex
     */
    public Edge(GraphInstance source, GraphInstance target) {
        this.source = source;
        this.target = target;
        this.sourceInt = source.getId();
        this.targetInt = target.getId();
    }

    public Edge(int source, int target) {
        this.sourceInt = source;
        this.targetInt = target;
    }

    /**
     * Return the color of the edge
     * @return The color of the edge
     */
    public Color getColor() {
        return this.color;
    }

    /**
     * Changes the color of the edge
     * @param color
     */
    public void setColor(Color color) {
        this.color = color;
    }

    public int getSource() {
        return this.sourceInt;
    }

    public int getTarget() {
        return this.targetInt;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Edge) {
            Edge e = (Edge) obj;
            return (((this.sourceInt == e.sourceInt) && (this.targetInt == e.targetInt)));
//                    || ((this.sourceInt == e.targetInt) && (this.targetInt == e.sourceInt)));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3 + 5 * this.sourceInt;
        hash += 7 * this.targetInt;
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        long source_aux = 0;
        long target_aux = 0;

        if (this.sourceInt < this.targetInt) {
            source_aux = this.sourceInt;
            target_aux = this.targetInt;
        } else {
            source_aux = this.targetInt;
            target_aux = this.sourceInt;
        }

        long sourceComp = 0;
        long targetComp = 0;
        if (((Edge) o).getSource() < ((Edge) o).getTarget()) {
            sourceComp = ((Edge) o).getSource();
            targetComp = ((Edge) o).getTarget();
        } else {
            sourceComp = ((Edge) o).getTarget();
            targetComp = ((Edge) o).getSource();
        }

        if (source_aux - sourceComp < 0) {
            return -1;
        } else if (source_aux - sourceComp > 0) {
            return 1;
        } else {
            if (target_aux - targetComp < 0) {
                return -1;
            } else if (target_aux - targetComp > 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public float getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "(" + this.sourceInt + ", " + this.targetInt + "; " + weight + ")";
    }

    public void draw(java.awt.Graphics2D g2, boolean globalsel) {
//        if (((lessThanOrEqualThreshold) && (length <= threshold)) ||
//                ((!lessThanOrEqualThreshold) && (length >= threshold))) {

        if (!this.isValid() || !this.isEnabled() || !source.isEnabled() || !target.isEnabled()) { //source == null or target == null
            return;
        }

        float alpha = model.getEdgeAlpha();

        //Combines the color of the two vertex to paint the edge
        if (!this.source.isValid() && !this.target.isValid()) {
            this.color = java.awt.Color.BLACK;
        } else {
            if (this.target.isSelected() && this.source.isSelected()) {
                alpha = 1.0f;
            } 
            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, alpha));
        }

//        if (this.calculateColor) {                   
//            this.color = new Color((this.source.getColor().getRed() + this.target.getColor().getRed()) / 2,
//                    (this.source.getColor().getGreen() + this.target.getColor().getGreen()) / 2,
//                    (this.source.getColor().getBlue() + this.target.getColor().getBlue()) / 2);
//        }
//        g2.setColor(this.color);

        g2.setPaint(new GradientPaint(
                new Point((int)source.getX(), (int)source.getY()), source.getColor(), 
                new Point((int)target.getX(), (int)target.getY()), target.getColor()));

        g2.setStroke(new java.awt.BasicStroke(1.3f));
        g2.drawLine(((int) this.source.getX()), ((int) this.source.getY()),
                ((int) this.target.getX()), ((int) this.target.getY()));
        g2.setStroke(new java.awt.BasicStroke(1.0f));

        //DESENHANDO A SETA
        if (this.showArrow) {
            java.awt.Point s = new java.awt.Point((int) this.source.getX(), (int) this.source.getY());
            java.awt.Point t = new java.awt.Point((int) this.target.getX(), (int) this.target.getY());
            float r = (float) Math.sqrt(Math.pow(s.x - t.x, 2) + Math.pow(s.y - t.y, 2));
            float cos = (t.x - s.x) / r;
            float sen = (t.y - s.y) / r;

            //rodo e translado
            java.awt.Point pa = new java.awt.Point(Math.round(-sen * 4) + (t.x + s.x) / 2, Math.round(cos * 4) + (t.y + s.y) / 2);
            java.awt.Point pb = new java.awt.Point(Math.round(-sen * -4) + (t.x + s.x) / 2, Math.round(cos * -4) + (t.y + s.y) / 2);
            java.awt.Point pc = new java.awt.Point(Math.round(cos * 4) + (t.x + s.x) / 2, Math.round(sen * 4) + (t.y + s.y) / 2);

            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));

            g2.drawLine(pa.x, pa.y, pc.x, pc.y);
            g2.drawLine(pb.x, pb.y, pc.x, pc.y);
        }

        if (this.showLength) {
            String label = Float.toString(this.weight);
            float x = 5 + Math.abs(this.source.getX() - this.target.getX()) / 2
                    + Math.min(this.source.getX(), this.target.getX());
            float y = Math.abs(this.source.getY() - this.target.getY()) / 2
                    + Math.min(this.source.getY(), this.target.getY());

            //Getting the font information
            java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());

            //Getting the label size
            int width = metrics.stringWidth(label);
            int height = metrics.getAscent();

            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
            g2.setPaint(java.awt.Color.WHITE);
            g2.fill(new java.awt.Rectangle((int) x - 2, (int) y - height, width + 4, height + 4));
            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));

            g2.setColor(java.awt.Color.BLACK);
            g2.drawRect((int) x - 2, (int) y - height, width + 4, height + 4);

            g2.drawString(label, x, y);
        }
        // }
    }

    public void updateCoordinates(GraphModel model) throws Exception {
        ProjectionInstance pisrc = this.source;
        ProjectionInstance pitrg = this.target;

        if (pisrc == null || pitrg == null) {
            throw new Exception("Desconsiderando aresta de instância deletada.");
        }

        if (model != null && this.model != null) {
            if (this.model.equals(model)) {
                //se a conectividade ainda é a mesma...
                if (this.selcon.equals(model.getSelectedConnectivity().getName())) {
                    if (this.x1 == pisrc.getX()
                            && this.y1 == pisrc.getY()
                            && this.x2 == pitrg.getX()
                            && this.y2 == pitrg.getY()) {
                        return;
                    }
                }
                //se mudou a conectividade, continua... update.
                //System.out.println("Mudança de conectividade.");
            }
        } else {
            if (this.model == null && model != null) {
                this.model = model;
                this.selcon = model.getSelectedConnectivity().getName();
            }
        }

        this.x1 = pisrc.getX();
        this.y1 = pisrc.getY();

        this.x2 = pitrg.getX();
        this.y2 = pitrg.getY();

        if (this.x1 > this.x2) {
            this.maxX = this.x1;
            this.minX = this.x2;
        } else {
            this.maxX = this.x2;
            this.minX = this.x1;
        }

        if (this.y1 > this.y2) {
            this.maxY = this.y1;
            this.minY = this.y2;
        } else {
            this.maxY = this.y2;
            this.minY = this.y1;
        }

//        if (Math.abs(this.x1 - this.x2) < 10E-6 || Math.abs(this.y1 - this.y2) < 10E-6) {
//            System.out.println("Aresta: (" + this.x1 + "," + this.y1 + ")" + "---------" + "(" + this.x2 + "," + this.y2 + ")");
//        }
        //Se a reta for vertical...
        if (Math.abs(this.x1 - this.x2) <= 10E-4) {
            this.alfa = Float.POSITIVE_INFINITY;
            this.b = 0;

//apenas para teste ***************************************************************************************
//            pisrc.setColor(Color.RED);
//            pitrg.setColor(Color.RED);
//            model.setChanged();
//            model.notifyObservers();


            //se tiver inclinação ou for horizontal
        } else {
            this.alfa = (this.y2 - this.y1) / (this.x2 - this.x1);

            this.b = this.y1 - (this.x1 * this.alfa);

            float aux = this.y2 - (this.x2 * this.alfa);
        }
    }

    public Boolean crosses(Edge edge, GraphModel model) throws Exception {
        try {
            this.updateCoordinates(model);
        } catch (Exception ex) {
            return false;
        }
        try {
            edge.updateCoordinates(model);

            //todo
            //if model contains this vertices... ok else return
        } catch (Exception ex) {
            return false;
        }


        //se o cruzamento é pelas arestas terem um mesmo vértice em comum
        if (this.getSource() == edge.getSource()
                || this.getSource() == edge.getTarget()
                || this.getTarget() == edge.getSource()
                || this.getTarget() == edge.getTarget()) {
            return false;
        }

//        System.out.println();
//        System.out.println("Aresta this: (" + this.x1 + ", " + this.y1 + ")" + "-" + "(" + this.x2 + ", " + this.y2 + ")");
//        System.out.println("Aresta edge: (" + edge.x1 + ", " + edge.y1 + ")" + "-" + "(" + edge.x2 + ", " + edge.y2 + ")");

        //se o box de uma aresta não intercepta o box de outra
        Edge e1 = null;
        Edge e2 = null;
        if (this.getMaxX() < edge.getMaxX()) {
            e1 = this;
            e2 = edge;
        } else {
            e1 = edge;
            e2 = this;
        }
        if (e1.getMaxX() < e2.getMinX() && Math.abs(e1.getMaxX() - e2.getMinX()) > 10E-4) {
            return false;
        }

        if (this.getMaxY() < edge.getMaxY()) {
            e1 = this;
            e2 = edge;
        } else {
            e1 = edge;
            e2 = this;
        }
        if (e1.getMaxY() < e2.getMinY() && Math.abs(e1.getMaxY() - e2.getMinY()) > 10E-4) {
            return false;
        }



        if (Math.abs(this.x1 - this.x2) <= 10E-4 && Math.abs(this.y1 - this.y2) <= 10E-4) {
            if (Math.abs(edge.x1 - edge.x2) <= 10E-4 && Math.abs(edge.y1 - edge.y2) <= 10E-4) {

                //se os quatro vértices estão em uma mesma posição, num mesmo ponto...
                if (Math.abs(this.getMinX() - edge.getMinX()) <= 10E-4 && Math.abs(edge.getMinY() - this.getMinY()) <= 10E-4
                        && Math.abs(edge.getMaxX() - this.getMaxX()) <= 10E-4 && Math.abs(edge.maxY - this.getMaxY()) <= 10E-4) {
                    // System.out.println("***as duas arestas viraram dois pontos, com valores de x,y iguais. cruzou. this e edge");
                    throw new Exception("Duas arestas viraram pontos e estão sobrepostos.");
                } else {
                    // System.out.println("as duas arestas viraram dois pontos, mas estes estão longe um do outro, não se cruzam. this e edge.");
                    return false;
                }
            } else {
                //se apenas dois vértices de uma das arestas (este objeto - this) estiverem em um mesmo ponto
                if (edge.computeY(this.x1) <= edge.getMaxY() && edge.computeY(this.x1) >= edge.getMinY()
                        && edge.getMinX() <= this.x1 && edge.getMaxX() >= this.x1) {
//                    System.out.println("***uma aresta, dois vértices, virou ponto (this) e está contido na outra aresta. cruzou.");
                    return true;
                } else {
//                    System.out.println("uma aresta, dois vértices, virou ponto (this) mas NãO está contido na outra aresta. não cruzou");
                    //reta não passa pelo ponto "this"
                    return false;
                }
            }
        } else {

            if (Math.abs(edge.x1 - edge.x2) <= 10E-4 && Math.abs(edge.y1 - edge.y2) <= 10E-4) {
                //se apenas dois vértices de uma das arestas (do outro objeto - edge) estiverem em um mesmo ponto
                if (this.computeY(edge.getX1()) <= this.getMaxY() && this.computeY(edge.x1) >= this.getMinY()
                        && this.getMinX() <= edge.x1 && this.getMaxX() >= edge.x1) {
//                    System.out.println("***uma aresta virou ponto (edge) e está contido na outra aresta. cruzou.");
                    return true;
                } else {
                    //reta não passa pelo ponto "edge"
//                    System.out.println("uma aresta virou ponto (edge) mas NãO está contido na outra aresta. não cruzou");
                    return false;
                }
            }

            //senão, se nenhuma aresta virou ponto...

            //se as arestas forem paralelas (duas retas verticais)

            if (edge.getAlfa() == Float.POSITIVE_INFINITY && this.getAlfa() == Float.POSITIVE_INFINITY) {
//                System.out.println("arestas verticais paralelas");
                return false;
            }
//            System.out.println("alfas: " + this.getAlfa() + ", " + edge.getAlfa());
            //se forem paralelas, mas não verticais


            if (Math.abs(this.getAlfa() - edge.getAlfa()) <= 10E-4) {
                //necessário checar se são sobrepostas o?
//                System.out.println("arestas paralelas");
                return false;



                //se não forem paralelas
            } else {



                //se uma das arestas for vertical
                if (this.getAlfa() == Float.POSITIVE_INFINITY && edge.getAlfa() != Float.POSITIVE_INFINITY) {
                    float yaux = edge.computeY(this.x1);
                    if (yaux >= this.getMinY() && yaux <= this.getMaxY()) {
//                        System.out.println("***Cruzamento em aresta vertical *************");
                        return true;
                    } else {
//                        System.out.println("Não cruzamento em aresta vertical");
                        return false;
                    }
                }
                if (edge.getAlfa() == Float.POSITIVE_INFINITY && this.getAlfa() != Float.POSITIVE_INFINITY) {
                    float yaux = this.computeY(this.x1);
                    if (yaux >= edge.getMinY() && yaux <= edge.getMaxY()) {
//                        System.out.println("***Cruzamento em aresta vertical*************");
                        return true;
                    } else {
//                        System.out.println("Não cruzamento em aresta vertical");
                        return false;
                    }
                }


                //se nenhuma reta for vertical...

                //calcula o x em que as retas se cruzam (
                float xintersection = (this.getB() - edge.getB()) / (edge.getAlfa() - this.getAlfa());

                //verifica se o x está dentro da intersecção dos domínios dos segmentos de retas
                if (this.getMinX() <= xintersection && xintersection <= this.getMaxX()
                        && edge.getMinX() <= xintersection && xintersection <= edge.getMaxX()) {
//                    System.out.println("***intersecção de caso comum");
                    return true;
                }
                return false;
            }
        }
    }

    public float computeY(float x) {
        return this.alfa * x + this.b;
    }

    public float getAlfa() {
        return alfa;
    }

    public void setAlfa(float alfa) {
        this.alfa = alfa;
    }

    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }

    public float getMaxX() {
        return maxX;
    }

    public void setMaxX(float maxX) {
        this.maxX = maxX;
    }

    public float getMinX() {
        return minX;
    }

    public void setMinX(float minX) {
        this.minX = minX;
    }

    public float getX1() {
        return x1;
    }

    public void setX1(float x1) {
        this.x1 = x1;
    }

    public float getX2() {
        return x2;
    }

    public void setX2(float x2) {
        this.x2 = x2;
    }

    public float getY1() {
        return y1;
    }

    public void setY1(float y1) {
        this.y1 = y1;
    }

    public float getY2() {
        return y2;
    }

    public void setY2(float y2) {
        this.y2 = y2;
    }

    public float getMaxY() {
        return maxY;
    }

    public void setMaxY(float maxY) {
        this.maxY = maxY;
    }

    public float getMinY() {
        return minY;
    }

    public void setMinY(float minY) {
        this.minY = minY;
    }

    public boolean isValid() {
        return (source != null && target != null && source.isEnabled() && target.isEnabled() && sourceInt != targetInt);
    }

    public boolean isCrossingEdge() {
        return crossingEdge;
    }

    public void setCrossingEdge(boolean crossingEdge) {
        this.crossingEdge = crossingEdge;
    }

    public boolean isCalculateColor() {
        return calculateColor;
    }

    public void setCalculateColor(boolean calculateColor) {
        this.calculateColor = calculateColor;
    }

    public boolean isShowArrow() {
        return showArrow;
    }

    public void setShowArrow(boolean showArrow) {
        this.showArrow = showArrow;
    }

    public boolean isShowLength() {
        return showLength;
    }

    public void setShowLength(boolean showLength) {
        this.showLength = showLength;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }    
    
    void setModel(GraphModel model) {
        this.model = model;
    }
    
    private float weight = Edge.NO_SIZE;
    private Color color = Color.WHITE; //Color of the edge
    private GraphInstance source = null; //The first vertex of the edge
    private GraphInstance target = null; //The second vertex of the edge
    private int sourceInt;
    private int targetInt;
    private float alfa; //angular coef.
    private float b;
    private float x1;
    private float x2;
    private float y1;
    private float y2;
    private GraphModel model;
    private float maxX;
    private float minX;
    private float maxY;
    private float minY;
    private String selcon = null;
    private float delta = (float) 10E-04;
    private boolean crossingEdge = false;
    private boolean enabled = true;

}
