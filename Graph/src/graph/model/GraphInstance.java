/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph.model;

import graph.forcelayout.ForceData;
import java.util.ArrayList;
import projection.model.ProjectionInstance;
import projection.model.Scalar;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class GraphInstance extends ProjectionInstance {

    public GraphInstance(int id, float x, float y) {
        super(id, x, y);
        setModel(model);
        sizefactor = 0;
        showlabel = false;
        showImage = false;
    }

    public GraphInstance(int id) {
        this(id, 0.0f, 0.0f);
        setModel(model);
    }

    @Override
    public boolean isInside(int x, int y) {
        return (Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2)) <= getSize());
    }

    /**
     * @return the sizefactor
     */
    public float getSizeFactor() {
        return sizefactor;
    }

    /**
     * @param sizefactor the sizefactor to set
     */
    public void setSizeFactor(float sizefactor) {
        this.sizefactor = sizefactor * 2;
        model.setChanged();
    }
    
    public void setSizeFactor(Scalar scalar){
        this.setSizeFactor(this.getNormalizedScalarValue(scalar));        
    }

    public int getSize() {
        int inssize = ((GraphModel) model).getInstanceSize();
        return (int) (inssize + (sizefactor * inssize));
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
    
    
    
    public ForceData fdata; //Use to repositioning the points
    protected float sizefactor;
    protected boolean valid = true; //identifies if a vertex is valid

    public GraphInstance createClone(GraphModel newModel) {
        GraphInstance gi = new GraphInstance(id, x, y);
        gi.setModel(newModel);
        gi.setColor(this.getColor());
        gi.setSelected(this.isSelected());
        gi.setShowLabel(this.isShowLabel());
        gi.setSizeFactor(this.getSizeFactor());
        gi.fdata = this.fdata;
        gi.scalars = new ArrayList<Float>();

        for (int i = 0; i < this.scalars.size(); i++) {
            gi.scalars.add(this.scalars.get(i));
        }

        return gi;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isShowImage() {
        return showImage;
    }

    public void setShowImage(boolean showImage) {
        this.showImage = showImage;
    }
    
    @Override
    public void setScalarValue(Scalar scalar, float value) {
        if (scalar != null) {
            int index = ((GraphModel) model).getScalars().indexOf(scalar);
            
            //Se o Scalar ainda não existir no Model...
            if(index == -1){
                try {
                    ((GraphModel) model).addScalar(scalar.getName());
                    index = ((GraphModel) model).getScalars().indexOf(scalar);
                    
                } catch (Exception ex) {
                     System.out.println("scalar must not be null");
                }                
            }
            
            //Se existir no Model
            if (scalars.size() > index) {
                scalars.set(index, value);                
            } else {
                //se existir mas ainda não foi setado para essa instância
                int size = scalars.size();
                for (int i = 0; i < index - size; i++) {
                    scalars.add(0.0f);
                }
                scalars.add(value);
            }

            scalar.store(value);
        }
    }

    
    protected boolean enabled = true;
    protected boolean showImage = false;
    
}
