/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */
package graph.model;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import visualizationbasics.model.AbstractInstance;

/**
 * This class represents the graph connectivity.
 * 
 * @author Fernando Vieira Paulovich
 */
public class Connectivity implements Serializable {

    /** Creates a new instance of Connectivity
     * 
     * @param name The connectivity's name
     * @param edges
     */
    public Connectivity(String name, ArrayList<Edge> edges) {
        this.name = name;
        this.edges = compress(edges);
        this.showweight = false;
        this.calculateMaxWeight();
        this.setShowArrow(false);
    }

    public void updateGraphInstancesFromEdges(GraphModel model) {
        //faz update das instâncias GraphInstances de source e target
        for (Edge edge : edges) {
            edge.updateSourceTargetInstances(model);
        }

        //se alguma delas for null, se alguma foi removida, remova as aretas
        //que contiverem esses vértices nulos
        ArrayList<Edge> newEdges = new ArrayList<>();
        for (Edge edge : this.edges) {
            if (edge.isValid()) {
                newEdges.add(edge);
            }
        }
        this.edges = newEdges;
        this.calculateMaxWeight();
    }

    public void draw(GraphModel model, BufferedImage image, boolean highquality) {
        Graphics2D g2 = (Graphics2D) image.getGraphics();

        if (highquality) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }

        //ArrayList<AbstractInstance> instances = model.getInstances();
        ArrayList<AbstractInstance> selectedInstances = model.getSelectedInstances();


        int size = edges.size();
        float div = this.getMaxWeight() - this.getMinWeight();
        for (Edge e : this.edges) {
            e.setModel(model);
            if (selectedInstances == null || selectedInstances.isEmpty()) {
                e.draw(g2, false);
            } else {
                e.draw(g2, true);
            }
            float weigth = e.getWeight();
            float normalizedWeight = (weigth - this.getMinWeight()) / (div);

            //cor de acordo com o peso.. use setColor da edge
            //int color = Math.round(normalizedWeight * 23);
            //Color rgb = model.getColorTable().getColor(normalizedWeight);
            //}          
        }
    }

    public ArrayList<Edge> getEdges() {
        return edges;


    }

    public String getName() {
        return name;


    }

    private ArrayList<Edge> compress(ArrayList<Edge> edges) {
        if (edges.size() > 0) {
            Collections.sort(edges);
            ArrayList<Edge> edges_aux = edges;
            edges = new ArrayList<>();

            int n = 0;
            edges.add(edges_aux.get(0));

            for (int i = 1; i < edges_aux.size(); i++) {
                if (!edges_aux.get(n).equals(edges_aux.get(i))) {
                    edges.add(edges_aux.get(i));
                    n = i;
                }
            }
        }

        return edges;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Connectivity) {
            return this.name.equals(((Connectivity) obj).name);


        }
        return false;


    }

    @Override
    public int hashCode() {
        int hash = 3 + (this.name != null ? this.name.hashCode() : 0);


        return hash;


    }

    @Override
    public String toString() {
        return this.name;


    }

    public boolean isShowWeight() {
        return showweight;
    }

    public void setShowWeight(boolean showweight) {
        this.showweight = showweight;
        for (Edge e : this.edges) {
            e.setShowLength(showweight);
        }
    }

    public void setShowArrow(boolean showArrow) {
        this.showArrow = showArrow;
        for (Edge e : this.edges) {
            e.setShowArrow(showArrow);
        }
    }

    private void calculateMaxWeight() {
        for (Edge e : edges) {
            if (e.isValid()) { //if its vertices already exists
                if (e.getWeight() > maxValue) {
                    maxValue = e.getWeight();
                }
                if (e.getWeight() < minValue) {
                    minValue = e.getWeight();
                }
            }
        }
    }

    public Float getMaxWeight() {
        return maxValue;
    }

    public Float getMinWeight() {
        return minValue;
    }

    public boolean isShowArrow() {
        return showArrow;
    }

    public boolean isShowweight() {
        return showweight;
    }
    private Float maxValue = Float.MIN_VALUE;
    private float minValue = Float.MAX_VALUE;
    public static final long serialVersionUID = 1L;
    private final String name; //The connectivity name
    private ArrayList<Edge> edges; //The edges which composes the connectivity
    private boolean showweight = false; //to indicate if the weight is shown
    private boolean showArrow = false;
}
