/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author Henry Heberle
 */
public class ScalarsFromAttributesTable implements Serializable {

    // armazena lista de scalares de uma instâncias (tuplas)
    private HashMap<Integer, ScalarsFromAttributes> scalarsTable;

    public ScalarsFromAttributesTable() {
        scalarsTable = new HashMap<>();
    }

    public ScalarsFromAttributes getScalarsByIndexTuple(Integer index) {
        if (this.scalarsTable.containsKey(index)) {
            return this.scalarsTable.get(index);
        } else {
            ScalarsFromAttributes s = new ScalarsFromAttributes(index);
            this.scalarsTable.put(index, s);
            return s;
        }
    }

    void add(ScalarsFromAttributes o) {
        this.scalarsTable.put(o.getId(), o);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        ScalarsFromAttributesTable clone = new ScalarsFromAttributesTable();
        clone.scalarsTable = (HashMap<Integer, ScalarsFromAttributes>) scalarsTable.clone();
        return clone;
    }
    
    
}
