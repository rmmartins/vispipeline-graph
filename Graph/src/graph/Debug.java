/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.CRC32;

/**
 *
 * @author rmartins
 */
public class Debug {
    
    public static boolean ENABLED = true;
    
    public static void log(Object source, String message) {
        if (ENABLED) {
            Logger logger = Logger.getLogger(source.getClass().getName());            
            logger.log(Level.INFO, message, source);
        }
    }
    
    public static void printCrc(Object source, Object toBeChecked) {
        if (ENABLED) {
            try {
                ByteArrayOutputStream bo = new ByteArrayOutputStream();
                ObjectOutputStream os = new ObjectOutputStream(bo);
                os.writeObject(toBeChecked);
                CRC32 crc = new CRC32();
                crc.update(bo.toByteArray());
                Debug.log(source, "Object: " + toBeChecked.toString() + ", CRC: "
                        + crc.getValue());
            } catch (Exception e) {
                Debug.log(source, e.toString());
            }
        }
    }
    
}
