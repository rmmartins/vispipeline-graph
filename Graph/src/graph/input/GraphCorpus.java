/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>, 
 * Roberto Pinho <robertopinho@yahoo.com.br>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package graph.input;

import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Set;
import textprocessing.corpus.Corpus;

/**
 * This class represents a Corpus (a set of documents).
 * 
 * @author Fernando Vieira Paulovich, Roberto Pinho
 */
public abstract class GraphCorpus extends Corpus implements Serializable {

    public GraphCorpus(String url, int nrGrams) {
        super(url, nrGrams);
    }
    
    public String getFullContent(String key) throws IOException {
        return getFullContent(keyIndex.get(key));        
    }
    
    public String getFilteredContent(String key) throws IOException {
        return getFilteredContent(keyIndex.get(key));
    }

    public String getViewContent(String key) throws IOException {
        return getViewContent(keyIndex.get(key));
    }

    public String getSearchContent(String key) throws IOException {
        return getSearchContent(keyIndex.get(key));
    }   

    public Set<String> getKeys() {        
        return keyIndex.keySet();
    }

    /**
     * Get the title of a document belonging to this corpus.
     * @param nrLines The number of lines used to construct the title.
     * @param id The document id
     * @return The title of a documents.
     * @throws java.io.IOException 
     */
    public String getTitle(int nrLines, String key) throws IOException {
        return getLabel(nrLines, keyIndex.get(key));
    }
    
    protected LinkedHashMap<String, Integer> keyIndex = new LinkedHashMap<String, Integer>();
}
