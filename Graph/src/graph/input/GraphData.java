/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *
 * PEx is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */
package graph.input;

import datamining.neighbors.Pair;
import graph.Connectivity;
import graph.model.Connectivities;
import graph.model.ScalarsFromAttributesTable;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import graph.Graph;
import java.io.Serializable;
import java.util.List;
import projection.model.Scalar;
import textprocessing.processing.Ngram;

/**
 *
 * @author Gabriel de Faria Andery
 */
public abstract class GraphData extends GraphCorpus {

    public static final int VNA = 0;
    public static final int BIB = 1;

    protected Connectivities connectivities;
    protected ScalarsFromAttributesTable scalarsTable;
    protected ArrayList<String> titles;
    protected ArrayList<Scalar> scalars;

    public GraphData(String url, int nrGrams) {
        super(url, nrGrams);
    }

    @Override
    public String getFullContent(String id) throws IOException {
        return this.matrix.get(this.ids.indexOf(id)).toString();
    }

    public String getFilteredContent(String id) throws IOException {
        return this.matrix.get(this.ids.indexOf(id)).toString();
    }

    public String getSearchContent(String id) throws IOException {
        return this.matrix.get(this.ids.indexOf(id)).toString();
    }

    @Override
    public String getViewContent(String id) throws IOException {
        return this.matrix.get(this.ids.indexOf(id)).toString();
    }

    public ArrayList<Ngram> getNgrams(String id) throws IOException {
        ArrayList<Ngram> ngrams = new ArrayList<Ngram>();

        ArrayList<String> data = this.matrix.get(this.ids.indexOf(id));

        for (String value : data) {
            for (String value_split : value.split(" ")) {
                if (ngrams.contains(value_split.toLowerCase())) {
                    ngrams.get(ngrams.indexOf(value_split.toLowerCase())).frequency++;
                } else {
                    ngrams.add(new Ngram(value_split.toLowerCase()));
                }
            }
        }

        return ngrams;
    }

    @Override
    public ArrayList<Ngram> getCorpusNgrams() throws IOException {
        ArrayList<Ngram> ngrams = new ArrayList<Ngram>();

        for (Attribute att : attributes) {
            for (Enumeration<String> e = att.values.keys(); e.hasMoreElements();) {
                String value = e.nextElement();

                for (String value_split : value.split(" ")) {
                    if (ngrams.contains(value_split.toLowerCase())) {
                        ngrams.get(ngrams.indexOf(value_split.toLowerCase())).frequency++;
                    } else {
                        ngrams.add(new Ngram(value_split.toLowerCase()));
                    }
                }
            }
        }

        return ngrams;
    }

    @Override
    public float[] getClassData() {
        return new float[matrix.size()];
    }

    public ArrayList<ArrayList<String>> getMatrix() {
        return matrix;
    }

    public void setMatrix(ArrayList<ArrayList<String>> matrix) {
        this.matrix = matrix;
    }

    public ArrayList<ArrayList<String>> getVisualMatrix() {
        return visualMatrix;
    }

    public void setVisualMatrix(ArrayList<ArrayList<String>> visualMatrix) {
        this.visualMatrix = visualMatrix;
    }

    public ArrayList<ArrayList<String>> getRelationshipMatrix() {
        return relationshipMatrix;
    }

    public void setRelationshipMatrix(ArrayList<ArrayList<String>> relationshipMatrix) {
        this.relationshipMatrix = relationshipMatrix;
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    public Attribute getAttributeByName(String name) {
        for (Attribute att : attributes) {
            if (att.getName().equals(name)) {
                return att;
            }
        }
        return null;
    }

    public ArrayList<Attribute> getVisualAttributes() {
        return visualAttributes;
    }

    public Attribute getVisualAttributeByName(String name) {
        for (Attribute att : visualAttributes) {
            if (att.getName().equals(name)) {
                return att;
            }
        }
        return null;
    }

    public ArrayList<Attribute> getRelationships() {
        return relationships;
    }

    public Attribute getRelationshipByName(String name) {
        for (Attribute att : relationships) {
            if (att.getName().equals(name)) {
                return att;
            }
        }
        return null;
    }

    public Attribute getRelationshipByPosition(int pos) {
        if (pos < 0) {
            pos = 0;
        } else if (pos >= relationships.size()) {
            pos = relationships.size() - 1;
        }

        return relationships.get(pos);
    }

    public void createConnectivities(Graph graph) {
        for (int i = 2; i < relationships.size(); i++) {
            //Pair[][] uNeighbors = null;
            Pair[][] dNeighbors = null;
            //Hashtable<Integer, ArrayList<Pair>> undirected = new Hashtable<Integer, ArrayList<Pair>>();
            Hashtable<Integer, ArrayList<Pair>> directed = new Hashtable<Integer, ArrayList<Pair>>();

            for (int j = 0; j < ids.size(); j++) {
                //undirected.put(j, new ArrayList<Pair>());
                directed.put(j, new ArrayList<Pair>());
            }

            for (int j = 0; j < relationshipMatrix.size(); j++) {
                if (Float.parseFloat(relationshipMatrix.get(j).get(i)) == 0) {
                    continue;
                }

                Integer keyFrom = ids.indexOf(relationshipMatrix.get(j).get(0));
                Integer keyTo = ids.indexOf(relationshipMatrix.get(j).get(1));
                Float strength = Float.parseFloat(relationshipMatrix.get(j).get(i));

                //ArrayList<Pair> fromPairsUndirected = undirected.get(keyFrom);
                //ArrayList<Pair> toPairsUndirected = undirected.get(keyTo);
                //fromPairsUndirected.add(new Pair(keyTo, strength));
                //toPairsUndirected.add(new Pair(keyFrom, strength));

                ArrayList<Pair> fromPairsDirected = directed.get(keyFrom);
                fromPairsDirected.add(new Pair(keyTo, strength));
            }

            //if (undirected.isEmpty()) continue;

            //uNeighbors = new Pair[ids.size()][];
            dNeighbors = new Pair[ids.size()][];

            for (int j = 0; j < ids.size(); j++) {
                //uNeighbors[j] = new Pair[undirected.get(j).size()];
                dNeighbors[j] = new Pair[directed.get(j).size()];

                /*for (int k = 0; k < undirected.get(j).size(); k++) {
                uNeighbors[j][k] = undirected.get(j).get(k);
                }*/

                for (int k = 0; k < directed.get(j).size(); k++) {
                    dNeighbors[j][k] = directed.get(j).get(k);
                }
            }

            //Connectivity uCon = new Connectivity(relationships.get(i).getName()/*+ " [Undirected]"*/);
            Connectivity dCon = new Connectivity(relationships.get(i).getName()/* + " [Directed]"*/);
            //uCon.create(graph.getVertex(), uNeighbors);
            dCon.create(graph.getVertex(), dNeighbors);
            //graph.addConnectivity(uCon);
            graph.addConnectivity(dCon);
        }
    }





    /*Henry Heberle*/
    public Connectivities getConnectivities() {
        return this.connectivities;
    }

    public ScalarsFromAttributesTable getScalarsFromAttributesTable() {
        return this.scalarsTable;
    }

    public String getId(int rowIndex) {
        return this.matrix.get(rowIndex).get(0);
    }

    public boolean containsInstance(String id) {
        return this.ids.contains(id);
    }

    public String getTitleOfATuple(String attribute, int id) {
        ArrayList<String> tuple = this.matrix.get(id);
        int attrIndex = this.titles.indexOf(attribute);

        return tuple.get(attrIndex);
    }

    public ArrayList<String> getTitles() {
        return this.titles;
    }
    
    protected enum Type {

        NUMERICAL, STRING, DATE
    }

    public List<String> getAttributeNames(){
        List<String> list = new ArrayList<>();
        for(Attribute a : this.attributes){
            list.add(a.getName().toLowerCase());
        }
        return list;
    }
    
    public Attribute addAttribute(String name){
        Attribute at = new Attribute(name);
        this.attributes.add(at);
        return at;
    }
    
    public class Attribute implements Serializable {

        private String name;
        private Type type;
        private int weight = 1;
        private Hashtable<String, Integer> values;
        private SimpleDateFormat dateFormat;
        private Float min;
        private Float max;
        private int count = 0;
        private boolean useName = true;
        private boolean replaceSpaces = true;
        private boolean active;
        private boolean clazz = false;
        private boolean id = false;
        private boolean idApplicable = true;

        @Override
        protected Attribute clone() throws CloneNotSupportedException {
            Attribute newAttr = new Attribute(name);
            newAttr.type = type;
            newAttr.weight = weight;
            newAttr.values = (Hashtable<String, Integer>) values.clone();
            newAttr.min = min;
            newAttr.max = max;
            newAttr.useName = useName;
            newAttr.count = count;
            newAttr.active = active;
            newAttr.replaceSpaces = replaceSpaces;
            newAttr.clazz = clazz;
            newAttr.id = id;
            newAttr.idApplicable = idApplicable;
                    
            return newAttr;
        }

        
        public Attribute() {
            this.active = true;
            this.type = Type.NUMERICAL;
            this.values = new Hashtable<String, Integer>();
        }

        public Attribute(String name) {
            this.name = name.toLowerCase();
            this.active = true;
            this.type = Type.NUMERICAL;
            this.values = new Hashtable<String, Integer>();
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name.toLowerCase();
        }

        public String getType() {
            return type.toString().toLowerCase();
        }

        public void setType(Type type) {
            this.type = type;
        }

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        public Hashtable<String, Integer> getValues() {
            return values;
        }

        public void setValues(Hashtable<String, Integer> values) {
            this.values = values;
        }

        public void setDateFormat(String format) {
            try {
                dateFormat = new SimpleDateFormat(format);
            } catch (Exception e) {
                if (type == Type.DATE) {
                    type = Type.STRING;
                }
            }
        }

        public SimpleDateFormat getDateFormat() {
            return dateFormat;
        }

        public boolean isUseName() {
            return useName;
        }

        public void setUseName(boolean useName) {
            this.useName = useName;
        }

        public boolean isReplaceSpaces() {
            return replaceSpaces;
        }

        public void setReplaceSpaces(boolean replaceSpaces) {
            this.replaceSpaces = replaceSpaces;
        }

        public boolean isActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }

        public boolean isClazz() {
            return clazz;
        }

        public void setClazz(boolean clazz) {
            this.clazz = clazz;
        }

        public boolean isId() {
            return id;
        }

        public void setId(boolean id) throws InvalidFileException {
            if (id == true) {
                if (idApplicable) {
                    this.id = true;
                } else {
                    this.id = false;
                    throw new InvalidFileException("Ids must be unique.");
                }
            } else {
                this.id = false;
            }
        }

        public boolean containsKey(String key) {
            return (values.containsKey(key));
        }

        public void putValue(String key) {            
            if (!values.containsKey(key)) {
                values.put(key, this.count);

                if (this.type == Type.NUMERICAL) {
                    Float aux = 0.0f;
                    try {
                        if (!key.equals("[NA]"))
                            aux = Float.valueOf(key);
                    } catch (NumberFormatException e) {
                        System.err.println("Couldn't convert value \"" + key + "\" of supposedly numerical attribute [" + name + "]."
                                + " This attribute's type is now being converted to string.");
                        this.type = Type.STRING;
                        min = 0.0f;
                        max = Float.valueOf(this.count);
                    } finally {
                        if (!aux.isNaN()) {
                            if (min == null) {
                                min = aux;
                            } else if (aux < min) {
                                min = aux;
                            }

                            if (max == null) {
                                max = aux;
                            } else if (aux > max) {
                                max = aux;
                            }
                        }
                    }
                } else if (this.type == Type.DATE) {
                    try {
                        dateFormat.parse(key);
                    } catch (ParseException e) {
                        System.out.println(e.toString());
                        this.type = Type.STRING;
                    } finally {
                        max = Float.valueOf(this.count);
                    }
                } else {
                    max = Float.valueOf(this.count);
                }

                this.count++;
            } else {
                idApplicable = false;
            }
        }

        @Override
        public String toString() {
            return this.name;
        }

        public Float getMax() {
            return max;
        }

        public Float getMin() {
            return min;
        }
        
        
        
        
    }

    public class InvalidFileException extends IOException {

        public InvalidFileException(String message) {
            super(message);
        }
    }

    protected ArrayList<ArrayList<String>> matrix;
    protected ArrayList<ArrayList<String>> visualMatrix;
    protected ArrayList<ArrayList<String>> relationshipMatrix;
    protected ArrayList<Attribute> attributes;
    protected ArrayList<Attribute> visualAttributes;
    protected ArrayList<Attribute> relationships;
}
