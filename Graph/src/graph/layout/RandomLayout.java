/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph.layout;

import graph.input.GraphData;

/**
 *
 * @author henry
 */
/**
 *
 * @author Gabriel de Faria Andery
 */
class RandomLayout {
    
    public static float[][] layout(GraphData gdata) {
        float[][] layout = new float[gdata.getMatrix().size()][2];

        for (int i = 0; i < gdata.getMatrix().size(); i++) {
            layout[i][0] = (float)Math.random()*1000;
            layout[i][1] = (float)Math.random()*1000;
        }

        return layout;
    }
    
}
