package graph.layout;

import graph.input.GraphData;
import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.sparse.SparseMatrix;
import matrix.sparse.SparseVector;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Henry Heberle
 */
@VisComponent(hierarchy = "Graph.Layout",
name = "Random Layout",
description = "Randomly places nodes on the screen.")
public class RandomLayoutComp implements AbstractComponent {
    
    @Override
    public void execute() throws IOException {
        if (gdata != null) {
            float[][] randomLayout = RandomLayout.layout(gdata);
            
            matrix = new SparseMatrix();
            for (int i = 0; i < randomLayout.length; i++) {
                SparseVector row = new SparseVector(randomLayout[i], i);
                matrix.addRow(row);
            }
        } 
    }

    public void input(@Param(name = "Graph Data") GraphData gdata) throws CloneNotSupportedException {
        this.gdata = gdata;
    }
    
    public AbstractMatrix output() {
        return matrix;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {                
        gdata = null;
        matrix = null;
    }

    public static final long serialVersionUID = 1L;        
    private transient GraphData gdata = null;    
    private transient AbstractMatrix matrix = null;
}
