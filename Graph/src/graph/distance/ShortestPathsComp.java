package graph.distance;

import distance.DistanceMatrix;
import graph.input.GraphData;
import graph.util.FloydWarshallShortestPath;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;

/**
 *
 * @author Henry Heberle
 */
@VisComponent(hierarchy = "Graph.Distance",
name = "Shortest Paths",
description = "Creates a DistanceMatrix with the shortest paths between nodes.")
public class ShortestPathsComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        
        ArrayList<ArrayList<String>> rel_matrix = gdata.getRelationshipMatrix();
        ArrayList<String> ids = new ArrayList<String>(gdata.getKeys());
        int size = ids.size();

        float[][] d = new float[size][size];

        // inicialização da matriz de adj.
        for (int i = 0; i < size; i++) {
            Arrays.fill(d[i], Float.MAX_VALUE);
            d[i][i] = 0.0f;
        }

        // preenchimento da matriz de adj. com as relações
        connectivity = paramView.connectivity + 2;
        for (ArrayList<String> rel : rel_matrix) {            
            int from = ids.indexOf(rel.get(0));            
            int to = ids.indexOf(rel.get(1));            
            float val = Float.parseFloat(rel.get(connectivity));           
            d[from][to] = val;
            d[to][from] = val;
        }

        // cálculo dos shortest-paths
        FloydWarshallShortestPath.fw(d);
        
        // Float.MAX_VALUE's represent pairs of vertices without connection
        // so ignore them (it won't be a connection between them anyway,
        // even after inverting [min, max] scale)
        float max = Float.MIN_VALUE;
        for (int i = 0; i < ids.size(); i++) {
            for (int j = 0; j < ids.size(); j++) {
                if (d[i][j] < Float.MAX_VALUE && d[i][j] > max) {
                    max = d[i][j];
                }
            }
        }        
               
        // if the weights represent strength, not distance, changes values
        // inverting [min, max] scale
        strength = paramView.strength;
        if (strength) {            
            // invert scale by making (max - value)
            // as zero's represents the main diagonal, sums 1
            for (int i = 0; i < ids.size(); i++) {
                for (int j = 0; j < ids.size(); j++) {
                    if ((i != j) && (d[i][j] < Float.MAX_VALUE)) {
                        d[i][j] = max - d[i][j] + 1;
                    }                    
                }
            }
        }
        
        // criação da matriz de distâncias
        dmat = new DistanceMatrix(size);
        replaceInfinity = paramView.replace_inf;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (d[i][j] == Float.MAX_VALUE && replaceInfinity) {
                    d[i][j] = max + 1;
                }
                dmat.setDistance(i, j, d[i][j]);
            }
        }
        
        // compila os nomes de cada instância para usar como labels na dmat
        dmat.setLabels(ids);

    }

    public void input(@Param(name = "Graph Data") GraphData gdata) {
        this.gdata = gdata;
    }
    
    public DistanceMatrix output() {
        return dmat;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramView == null) {
            paramView = new ShortestPathsParamView(this);
        }
        return paramView;
    }

    @Override
    public void reset() {                
        gdata = null;
        dmat = null;
    }

    public int getConnectivity() {
        return connectivity;
    }

    public void setConnectivity(int connectivity) {
        this.connectivity = connectivity;
    }

    public boolean isReplaceInfinity() {
        return replaceInfinity;
    }

    public void setReplaceInfinity(boolean replaceInfinity) {
        this.replaceInfinity = replaceInfinity;
    }

    public boolean isStrength() {
        return strength;
    }

    public void setStrength(boolean strength) {
        this.strength = strength;
    }

    public static final long serialVersionUID = 1L;        
    
    // não transient:
    private int connectivity = 2;
    private boolean strength = true;
    private boolean replaceInfinity = true;
    
    transient GraphData gdata = null;    
    private transient DistanceMatrix dmat = null;
    private ShortestPathsParamView paramView;
    
}
