/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph.analysis;

import distance.DistanceMatrix;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author paulovich
 */
public class DistanceStressCurve extends StressCurve{

    public BufferedImage generate(Dimension size, float alpha, DistanceMatrix projectionDmat, DistanceMatrix originalDmat) throws IOException {
        BufferedImage image = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = (Graphics2D) image.getGraphics();

        //filling the background
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, size.width, size.height);       

        //finding the max and min distances
        double minrn = originalDmat.getMinDistance();
        double minr2 = projectionDmat.getMinDistance();
        double maxrn = originalDmat.getMaxDistance();
        double maxr2 = projectionDmat.getMaxDistance();
                
        ArrayList<String> projLabels = projectionDmat.getLabels();
        int[] indexMap = new int[projLabels.size()];
        for (int i = 0; i < projLabels.size(); i++) {
            String label = projLabels.get(i);
            indexMap[i] = originalDmat.getLabels().indexOf(label);
        }
                       
        //drawing the dots
        int rgbcolor = Color.BLUE.getRGB();
        for (int i = 0; i < projectionDmat.getElementCount(); i++) {
            int orig_i = indexMap[i];
            if (orig_i == -1) continue;
            for (int j = i + 1; j < projectionDmat.getElementCount(); j++) {               
                int orig_j = indexMap[j];
                if (orig_j == -1) continue;
                double distrn = (originalDmat.getDistance(orig_i,orig_j)-minrn)/(maxrn-minrn);
                double distr2 = (projectionDmat.getDistance(i,j)-minr2)/(maxr2-minr2);
                int x = 2 * SPACE + (int) ((size.width - 3 * SPACE) * distrn);
                int y = 2 * SPACE + (int) ((size.height - 3 * SPACE) * distr2);
                simulateAlpha(image, alpha, x, y, rgbcolor);
            }
        }

        //drawing the axis
        g2.setColor(Color.BLACK);
        g2.setStroke(new BasicStroke(2));
        g2.drawLine(SPACE / 2, SPACE, size.width - SPACE, SPACE);
        g2.drawLine(SPACE, SPACE / 2, SPACE, size.height - SPACE);
        
        g2.setStroke(new BasicStroke(1));
        g2.setColor(Color.RED);
        g2.drawLine(SPACE/2, SPACE / 2, size.width - SPACE, size.height - SPACE);

        //reflecting the image around x
        BufferedImage refimage = new BufferedImage(size.height, size.width,
                BufferedImage.TYPE_INT_RGB);

        for (int i = 0; i < size.height; i++) {
            for (int j = 0; j < size.width; j++) {
                refimage.setRGB(j, i, image.getRGB(j, size.height - i - 1));
            }
        }

        

        return refimage;
    }

    private void simulateAlpha(BufferedImage image, float alpha, int x, int y, int rgb) {
        try {
            //C = (alpha * (A-B)) + B
            int oldrgb = image.getRGB(x, y);
            int oldr = (oldrgb >> 16) & 0xFF;
            int oldg = (oldrgb >> 8) & 0xFF;
            int oldb = oldrgb & 0xFF;

            int newr = (int) ((alpha * (((rgb >> 16) & 0xFF) - oldr)) + oldr);
            int newg = (int) ((alpha * (((rgb >> 8) & 0xFF) - oldg)) + oldg);
            int newb = (int) ((alpha * ((rgb & 0xFF) - oldb)) + oldb);

            int newrgb = newb | (newg << 8) | (newr << 16);
            image.setRGB(x, y, newrgb);
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println(x + "," + y);
        }
    }

    private static final int SPACE = 20;
}
