/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package graph.analysis;

import distance.DistanceMatrix;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import graph.model.GraphModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import projection.model.ProjectionModel;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;

/**
 *
 * @author Jose Gustavo
 */
@VisComponent(hierarchy = "Graph.Analysis",
name = "Neighborhood Preservation",
description = "Calculate the neighboorhod preservation measure.")
public class NeighborhoodPreservationComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        ArrayList<Serie> series = new ArrayList<Serie>();

        if (dmat == null) return;

        for (int i=0;i<models.size();i++) {
            Serie serie = new Serie(models.get(i));
            series.add(serie);
        }
        
        Collections.sort(series, new Comparator<Serie>() {
            public int compare(Serie o1, Serie o2) {
                return o1.name.compareTo(o2.name);
            }            
        });
        
        NeighborhoodPreservation.getInstance(null).display(this.dmat,series,this.nrNeighbors,this.useVisEuclideanDistance,this.useWeight,this.useEuclideanAsWeights);
    }

//    public void attach(@Param(name = "tree model") TreeModel model) {
//        if (models == null)
//            models = new ArrayList<ProjectionModel>();
//        if (model != null)
//            models.add(model);
//    }

    public void attach(@Param(name = "Graph model") GraphModel model) {
        if (models == null)
            models = new ArrayList<ProjectionModel>();
        if (model != null)
            models.add(model);
    }

//    public AbstractMatrix output() {
//        return input;
//    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new NeighborhoodPreservationParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        models = null;
    }

    public boolean isUseWeight() {
        return useWeight;
    }

    public void setUseWeight(boolean useWeight) {
        this.useWeight = useWeight;
    }

    public int getNrNeighbors() {
        return nrNeighbors;
    }

    public void setNrNeighbors(int nrNeighbors) {
        this.nrNeighbors = nrNeighbors;
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public boolean isDistanceMatrixProvided() {
        return (getDmat() != null);
    }
    
    public DistanceMatrix getDmat() {
        return dmat;
    }

    public void setDmat(DistanceMatrix dmat) {
        this.dmat = dmat;
    }

    public String getDataFileName() {
        return dataFileName;
    }

    public void setDataFileName(String dataFileName) {
        this.dataFileName = dataFileName;
    }

    public int getDataFileType() {
        return dataFileType;
    }

    public void setDataFileType(int dataFileType) {
        this.dataFileType = dataFileType;
    }

    public boolean getUseVisEuclidianDistance() {
        return useVisEuclideanDistance;
    }

    public void setUseVisEuclidianDistance(boolean selected) {
        this.useVisEuclideanDistance = selected;
    }

    public boolean getUseEuclideanAsWeights() {
        return useEuclideanAsWeights;
    }

    public void setUseEuclideanAsWeights(boolean selected) {
        this.useEuclideanAsWeights = selected;
    }

    public static final long serialVersionUID = 1L;
    private ArrayList<ProjectionModel> models;
    private boolean useWeight = false;
    private int nrNeighbors = 30;
    private transient NeighborhoodPreservationParamView paramview;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private transient DistanceMatrix dmat;
    private String dataFileName = "";
    private int dataFileType = 0;
    private boolean useVisEuclideanDistance = false;
    private boolean useEuclideanAsWeights = false;
    
}
