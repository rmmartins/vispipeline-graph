/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package graph.analysis;

import datamining.neighbors.KNN;
import datamining.neighbors.Pair;
import distance.DistanceMatrix;
import distance.dissimilarity.Euclidean;
import graph.model.Connectivity;
import graph.model.Edge;
import graph.model.GraphModel;
import graph.util.EPSFilter;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.geom.Rectangle2D;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import net.sf.epsgraphics.ColorMode;
import net.sf.epsgraphics.EpsGraphics;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.DefaultDrawingSupplier;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;
import projection.model.Scalar;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.util.SaveDialog;
import visualizationbasics.view.MessageDialog;

/**
 *
 * @author  Fernando Vieira Paulovich
 */
public class NeighborhoodPreservation extends javax.swing.JDialog {

    /** Creates new form NeighborhoodPreservation */
    private NeighborhoodPreservation(javax.swing.JDialog parent) {
        super(parent);
        initComponents();
        setModal(false);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonPanel = new javax.swing.JPanel();
        saveImageButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Neighborhood Preservation");
        setModal(true);

        saveImageButton.setText("Save Image");
        saveImageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveImageButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(saveImageButton);

        closeButton.setText("Close");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(closeButton);

        getContentPane().add(buttonPanel, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void saveImageButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveImageButtonActionPerformed
        int result = SaveDialog.showSaveDialog(new EPSFilter(),this,"","image.eps");

        if (result == JFileChooser.APPROVE_OPTION) {
            String filename = SaveDialog.getFilename();

            FileOutputStream out = null;

            try {
                // Save this document to example.eps
                out = new FileOutputStream(filename);

                // Create a new document with bounding box 0 <= x <= 100 and 0 <= y <= 100.
                EpsGraphics g = new EpsGraphics(filename, out, 0, 0,
                        panel.getWidth() + 1, panel.getHeight() + 1, ColorMode.COLOR_RGB);

                freechart.draw(g, new Rectangle2D.Double(0, 0, panel.getWidth() + 1,
                        panel.getHeight() + 1));

                // Flush and close the document (don't forget to do this!)
                g.flush();
                g.close();

            } catch (IOException ex) {
                Logger.getLogger(NeighborhoodPreservation.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        "Problems saving the file", JOptionPane.ERROR_MESSAGE);
            } finally {
                if (out != null) {
                    try {
                        out.flush();
                        out.close();
                    } catch (IOException ex) {
                        Logger.getLogger(NeighborhoodPreservation.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }//GEN-LAST:event_saveImageButtonActionPerformed

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_closeButtonActionPerformed

    public static NeighborhoodPreservation getInstance(javax.swing.JDialog parent) {
        return new NeighborhoodPreservation(parent);
    }

    public void display(final DistanceMatrix dmatdata, final ArrayList<Serie> series, final int maxneigh, final boolean useVisEuclideanDistance, final boolean useWeight, final boolean useEuclideanAsWeights) {
        final MessageDialog md = MessageDialog.show(null, 
                "Calculating neighborhood preservation...");

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    NeighborhoodPreservation.this.freechart = createChart(createAllSeries(dmatdata, series, maxneigh, useVisEuclideanDistance, useWeight, useEuclideanAsWeights));
                    if (NeighborhoodPreservation.this.freechart != null) {
                        NeighborhoodPreservation.this.panel = new ChartPanel(freechart);
                        NeighborhoodPreservation.this.getContentPane().add(panel, BorderLayout.CENTER);
                        NeighborhoodPreservation.this.setPreferredSize(new Dimension(650, 400));
                        NeighborhoodPreservation.this.setSize(new Dimension(650, 400));
                        NeighborhoodPreservation.this.setLocationRelativeTo(NeighborhoodPreservation.this.getParent());
                        md.close();
                        NeighborhoodPreservation.this.setVisible(true);
                    } else md.close();
                } catch (IOException ex) {
                    Logger.getLogger(NeighborhoodPreservation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        };

        t.start();
    }

    private JFreeChart createChart(XYDataset xydataset) {
        if (xydataset == null) return null;
        JFreeChart chart = ChartFactory.createXYLineChart("Neighborhood Preservation",
                "Number of Neighbors", "Precision", xydataset, PlotOrientation.VERTICAL,
                true, true, false);

        chart.setBackgroundPaint(Color.WHITE);
        
        XYPlot xyplot = (XYPlot) chart.getPlot();
        NumberAxis numberaxis = (NumberAxis) xyplot.getRangeAxis();
        numberaxis.setAutoRangeIncludesZero(false);

        xyplot.setDomainGridlinePaint(Color.BLACK);
        xyplot.setRangeGridlinePaint(Color.BLACK);

        xyplot.setOutlinePaint(Color.BLACK);
        xyplot.setOutlineStroke(new BasicStroke(1.0f));
        xyplot.setBackgroundPaint(Color.white);
        xyplot.setDomainCrosshairVisible(true);
        xyplot.setRangeCrosshairVisible(true);

        xyplot.setDrawingSupplier(new DefaultDrawingSupplier(
                new Paint[]{Color.RED, Color.BLUE, Color.GREEN, Color.MAGENTA,
                    Color.CYAN, Color.ORANGE, Color.BLACK, Color.DARK_GRAY, Color.GRAY,
                    Color.LIGHT_GRAY, Color.YELLOW
                }, DefaultDrawingSupplier.DEFAULT_OUTLINE_PAINT_SEQUENCE,
                DefaultDrawingSupplier.DEFAULT_STROKE_SEQUENCE,
                DefaultDrawingSupplier.DEFAULT_OUTLINE_STROKE_SEQUENCE,
                DefaultDrawingSupplier.DEFAULT_SHAPE_SEQUENCE));

        XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) xyplot.getRenderer();
        xylineandshaperenderer.setBaseShapesVisible(true);
        xylineandshaperenderer.setBaseShapesFilled(true);
        xylineandshaperenderer.setDrawOutlines(true);

        return chart;
    }

    private XYDataset createAllSeries(DistanceMatrix dmatdata, ArrayList<Serie> series, int maxneigh, boolean useVisEuclideanDistance, boolean useWeight, boolean useEuclideanAsWeights) throws IOException {
        XYSeriesCollection xyseriescollection = new XYSeriesCollection();

        for (int i = 0; i < series.size(); i++) {
            double[] values = this.neighborhood(series.get(i).model,dmatdata,maxneigh,useVisEuclideanDistance,useWeight,useEuclideanAsWeights);
            if (values == null) return null;
            XYSeries xyseries = this.createSerie(series.get(i).name, values);
            xyseriescollection.addSeries(xyseries);
        }

        return xyseriescollection;
    }

    private XYSeries createSerie(String name, double[] values) {
        XYSeries xyseries = new XYSeries(name);

        for (int i = 0; i < values.length; i++) {
            xyseries.add(i + 1, values[i]);
        }

        return xyseries;
    }

    public static AbstractMatrix exportPoints(ArrayList<AbstractInstance> instances, Scalar scalar) {
        
        AbstractMatrix matrix = new DenseMatrix();

        if (scalar == null) {
            return null;
//            ArrayList<GraphScalar> scalars = model.getScalars();
//            if ((scalars != null)&&(!scalars.isEmpty())) scalar = scalars.get(0);
        }

        ArrayList<String> labels = new ArrayList<String>();

          for (int i = 0; i < instances.size(); i++) {
            float[] point = new float[2];
            point[0] = ((ProjectionInstance)instances.get(i)).getX();
            point[1] = ((ProjectionInstance)instances.get(i)).getY();

            float cdata = ((ProjectionInstance)instances.get(i)).getScalarValue(scalar);
            Integer id = ((ProjectionInstance)instances.get(i)).getId();
            labels.add(((ProjectionInstance)instances.get(i)).toString());

            matrix.addRow(new DenseVector(point, id, cdata));
        }

        matrix.setLabels(labels);

        ArrayList<String> attributes = new ArrayList<String>();
        attributes.add("x");
        attributes.add("y");

        matrix.setAttributes(attributes);

        return matrix;
    }

    public Connectivity createEuclideanWeightConnectivity(GraphModel model, Connectivity con) {

        AbstractMatrix points = exportPoints(model.getInstances(), model.addScalar("cdata"));

        ArrayList<Edge> connEdges = con.getEdges();
        ArrayList<Integer> ids = points.getIds();
        ArrayList<Edge> euclideanDistEdges = new ArrayList<Edge>();

        if (connEdges == null) return null;
        if (ids == null) return null;

        for (int i=0;i<connEdges.size();i++) {
            int source = connEdges.get(i).getSource();
            int target = connEdges.get(i).getTarget();
            if (ids.indexOf(source) != -1 && ids.indexOf(target) != -1) {
                float distance = (new Euclidean()).calculate(points.getRow(ids.indexOf(source)),points.getRow(ids.indexOf(target)));
                euclideanDistEdges.add(new Edge(source,target,distance));
            }
        }

        Connectivity conn = new Connectivity("Plane Euclidian",euclideanDistEdges);
        return conn;
    }

    public DistanceMatrix createDistanceMatrix(GraphModel model, boolean useWeights,Connectivity euclideanDistAsWeightsCon) {

        DistanceMatrix dm = new DistanceMatrix();

        float[][] dmat = new float[model.getInstances().size()][model.getInstances().size()];        
        for (int i=0;i<dmat.length;i++)
            for (int j=0;j<dmat[i].length;j++)
                if (i == j) dmat[i][j] = 0;
                else dmat[i][j] = Float.MAX_VALUE;

        ArrayList<Edge> edges = null;

        if (euclideanDistAsWeightsCon != null) {
            edges = euclideanDistAsWeightsCon.getEdges();
        }else {
            if (model.getSelectedConnectivity() != null) {
                edges = model.getSelectedConnectivity().getEdges();                
            }
            else if ((model.getConnectivities() != null)&&(model.getConnectivities().size() > 1)) {
                    edges = model.getConnectivities().get(1).getEdges();                    
            }
        }

        if (edges == null) return null;

        for (int k=0;k<edges.size();k++) {
            Edge ed = edges.get(k);
//            System.out.printf("trying edge: %d,%d\n", ed.getSource(), ed.getTarget());

            int x = model.getInstances().indexOf(ed.getSource());
            int y = model.getInstances().indexOf(ed.getTarget());
            if (useWeights) {
                dmat[x][y] = dmat[y][x] = ed.getWeight();
            } else {
                dmat[x][y] = dmat[y][x] = 1.0f;
            }
        }
        
        //Calculating the shortest path, in the tree, among all nodes (including virtual nodes)
        //Floyd Warshall algorithm.
        int n = dmat.length;        
        for (int k=0; k<n; k++)
            for (int i=0; i<n; i++)
                for (int j=0; j<n; j++) {
                    float dd = dmat[i][k] + dmat[k][j];
                    if (dmat[i][j] > dd) dmat[i][j] = dd;
                }

        int k = -1;
        ArrayList<ArrayList<Float>> ndmat = new ArrayList<ArrayList<Float>>();
        for (int i=0;i<dmat.length;i++) {                        
//            if (!model.getInstances().get(i).toString().isEmpty()) {
                k++;
                ndmat.add(new ArrayList<Float>());
                for (int j=0;j<dmat[i].length;j++) {                    
                    if (!model.getInstances().get(j).toString().isEmpty()) {
                        ndmat.get(k).add(dmat[i][j]);
                    }
                }
//            }
        }
        
        //Create and fill the distance distmatrix
        dm.setElementCount(ndmat.size());

        float maxDistance = Float.NEGATIVE_INFINITY;
        float minDistance = Float.POSITIVE_INFINITY;

        float[][] distmat = new float[ndmat.size() - 1][];
        for (int i=0; i<ndmat.size()-1; i++) {
            distmat[i] = new float[i + 1];
            for (int j=0;j<distmat[i].length; j++) {
                float distance = ndmat.get(i+1).get(j);
                if (distance < minDistance) minDistance = distance;
                if (distance > maxDistance) maxDistance = distance;
                if ((i+1)!=j) {
                    if ((i+1) < j) distmat[j - 1][(i+1)] = distance;
                    else distmat[(i+1) - 1][j] = distance;
                }
            }
        }

        dm.setMinDistance(minDistance);
        dm.setMaxDistance(maxDistance);
        dm.setDistmatrix(distmat);

        return dm;

    }

    private double[] neighborhood(ProjectionModel model, DistanceMatrix dmatdata, int maxneigh, boolean useVisEuclideanDistance, boolean useWeight, boolean useEuclideanAsWeights) throws IOException {
       
        double[] values = new double[maxneigh];
        if (model == null) return values;

        Scalar scdata = model.addScalar("cdata");
                
        DistanceMatrix projectionDmat = null;
        AbstractMatrix validPoints = null;

        // Hierarquical Projection, uses paths to determine distances...
        if (model instanceof GraphModel) {
            validPoints = exportPoints(((GraphModel)model).getValidInstances(),scdata);
            if (useVisEuclideanDistance) {
                projectionDmat = new DistanceMatrix(validPoints, new Euclidean());
            } else {
                if (useEuclideanAsWeights) {
                    Connectivity gCon = null;
                    if (((GraphModel)model).getSelectedConnectivity() != null) {
                        gCon = ((GraphModel)model).getSelectedConnectivity();
                    }
                    else if ((((GraphModel)model).getConnectivities() != null)&&(((GraphModel)model).getConnectivities().size() > 1)) {
                        gCon = ((GraphModel)model).getConnectivities().get(1);
                    }
                    if (gCon == null) throw new IOException("Error: Graph connectivity not found.");
                    Connectivity euclideanWeightsConn = createEuclideanWeightConnectivity((GraphModel)model,gCon);
                    projectionDmat = createDistanceMatrix((GraphModel)model,useWeight,euclideanWeightsConn);
                } else {                    
                    projectionDmat = createDistanceMatrix((GraphModel)model,useWeight,null);                    
                }

                ArrayList<String> labels = validPoints.getLabels();
                ArrayList<Integer> ids = validPoints.getIds();

                float[] c = validPoints.getClassData();
                for (int i = 0; i < labels.size(); i++) {
                    if (labels.get(i).isEmpty()) {
                        labels.remove(i);
                        labels.add(i, "label" + i);
                    }
                }
                projectionDmat.setLabels(labels);                
                projectionDmat.setIds(ids);
                projectionDmat.setClassData(c);
            }
        } else {
            validPoints = exportPoints(model.getInstances(),scdata);
            projectionDmat = new DistanceMatrix(validPoints, new Euclidean());
        }

        
        if (dmatdata.getElementCount() != projectionDmat.getElementCount()) {            
            System.out.println("Data set different from projection! But creating anyway...");
        
            // The distance matrix has to be rebuilt because the id's are not
            // sequential integers, they are random
            int[] indexMap = new int[projectionDmat.getElementCount()];
            for (int i = 0; i < projectionDmat.getElementCount(); i++) {
                Integer id = projectionDmat.getIds().get(i);
                indexMap[i] = dmatdata.getIds().indexOf(id);                
            }
            DistanceMatrix dmat2 = new DistanceMatrix(projectionDmat.getElementCount());
            for (int i = 0; i < projectionDmat.getElementCount(); i++) {
                for (int j = 0; j < projectionDmat.getElementCount(); j++) {
                    dmat2.setDistance(i, j, dmatdata.getDistance(indexMap[i], indexMap[j]));
                }
            }
            dmat2.setLabels(projectionDmat.getLabels());
            dmat2.setClassData(projectionDmat.getClassData());
            dmat2.setIds(projectionDmat.getIds());
            dmatdata = dmat2;
        }

        KNN knnproj = new KNN(maxneigh);
        Pair[][] nproj = knnproj.execute(projectionDmat);

        KNN knndata = new KNN(maxneigh);
        Pair[][] ndata = knndata.execute(dmatdata);

        for (int n = 0; n < maxneigh; n++) {
            float percentage = 0.0f;

            for (int i = 0; i < projectionDmat.getElementCount(); i++) {
                float total = 0.0f;

                for (int j = 0; j < n + 1; j++) {
                    if (this.contains(nproj[i], n + 1, ndata[i][j].index)) {
                        total++;
                    }
                }

                percentage += total / (n + 1);
            }

            values[n] = percentage / projectionDmat.getElementCount();
        }

        return values;
    }

    private boolean contains(Pair[] neighbors, int length, int index) {
        for (int i = 0; i < length; i++) {
            if (neighbors[i].index == index) {
                return true;
            }
        }
        return false;
    }

    private JFreeChart freechart;
    private JPanel panel;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton closeButton;
    private javax.swing.JButton saveImageButton;
    // End of variables declaration//GEN-END:variables
}
