/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx), based on the code presented
 * in:
 *
 * http://snippets.dzone.com/tag/dijkstra
 *
 * How to cite this work:
 *
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *
 * PEx is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */
package graph.util;

import graph.model.Connectivity;
import graph.model.Edge;
import graph.model.GraphInstance;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;
import visualizationbasics.model.AbstractInstance;

/**
 *
 * ==========================================
 * CentralityComputer : a Java centrality measures library
 * ==========================================
 *
 * Computes degree centrality, closeness centrality, betweenness centrality, clustering coefficient
 * for the nodes of a connected undirected unweighted graph.
 * @author Anastasia Kurdia
 * @author Gabriel de Faria Andery
 */
public class CentralityMeasures {


    private HashMap<Integer, Integer> IndexMap;
    private Integer[][] DistanceMatrix;
    private Integer[][] AdjacencyMatrix;
    private Vector<Vector<Integer>> AdjacencyList;
    private int n;
    private Double[] Cb;

//    public CentralityMeasures(ArrayList<AbstractInstance> instances, Connectivity con) {
    public CentralityMeasures(ArrayList<Integer> ids, Connectivity con) {
        n = ids.size();
        IndexMap = new HashMap();

        int i = 0;
        int j = 0;
        int k = 0;
        for (Integer id: ids) {
            IndexMap.put(id, i);
            i++;
        }

        DistanceMatrix = new Integer[n][n];
        AdjacencyMatrix = new Integer[n][n];
        AdjacencyList = new Vector();
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                DistanceMatrix[i][j] = 0;
                AdjacencyMatrix[i][j] = 0;
            }
            AdjacencyList.add(new Vector());
        }

        //calculate Adjacency matrix
        for (Edge e : con.getEdges()) {           
//            System.out.println("Tentando usar a aresta "+e);
            i = e.getSource();
            if (i < 0 || i >= ids.size())
                i = IndexMap.get(e.getSource());
            
            j = e.getTarget();
            if (j < 0 || j >= ids.size())
                j = IndexMap.get(e.getTarget());
            
            AdjacencyMatrix[i][j] = 1;
            AdjacencyMatrix[j][i] = 1;

            AdjacencyList.elementAt(i).add(j);
            AdjacencyList.elementAt(j).add(i);
        }

        Cb = new Double[n];
        Double[] sigma = new Double[n];
        Integer[] d = new Integer[n];
        Double[] delta = new Double[n];
        for (i = 0; i < n; i++) {
            Cb[i] = 0.0;
            sigma[i] = 0.0;
            d[i] = 0;
        }

        Vector<Integer> S = new Vector();

        LinkedList<Integer> Q = new LinkedList();

        Vector<Vector<Integer>> P = new Vector();
        Vector<Integer> tempVector = new Vector();
        Iterator It;

        Integer v;
        Integer w;

        Vector<Integer> AdjVec = new Vector();
        Iterator At;

        for (Integer s = 0; s < n; s++) {

            //initialization
            S.clear();
            P.clear();
            for (i = 0; i < n; i++) {
                sigma[i] = 0.0;
                d[i] = -1;
                delta[i] = 0.0;
                P.add(new Vector());
            }
            sigma[s] = 1.0;
            d[s] = 0;
            Q.clear();
            Q.add(s);

            //perform BFS
            while (!Q.isEmpty()) {
                v = Q.remove();
                S.add(v);
                //for all neighbors of v
                AdjVec = AdjacencyList.elementAt(v);
                At = AdjVec.iterator();
                while (At.hasNext()) {
                    w = (Integer) At.next();
                    //w found for the first time
                    if (d[w] < 0) {
                        d[w] = d[v] + 1;
                        Q.add(w);
                    }

                    //shortest path to w via v?
                    if (d[w] == d[v] + 1) {
                        sigma[w] = sigma[w] + sigma[v];
                        P.elementAt(w).add(v);

                    }
                }
            }

            for (i = 0; i < n; i++) {
                DistanceMatrix[s][i] = d[i];
                DistanceMatrix[i][s] = d[i];
            }


            while (!S.isEmpty()) {
                w = S.remove(S.size() - 1);
                tempVector = P.elementAt(w);
                It = tempVector.iterator();

                while (It.hasNext()) {
                    v = (Integer) It.next();
                    delta[v] = delta[v] + (sigma[v] / sigma[w]) * (1 + delta[w]);
                }
                if (w != s) {
                    Cb[w] = Cb[w] + delta[w];
                }
            }

        }

        //normalize the value of betweenness
//        if (n > 2) {
//            for (i = 0; i < n; i++) {
//                Cb[i] = Cb[i].doubleValue() / ((n - 1) * (n - 2));
//                System.out.println("Cb[" + i + "] = " + Cb[i]);
//            }
//        } else {
//            for (i = 0; i < n; i++) {
//                Cb[i] = 1.0;
//            }
//        }
    }

    /**
    * Calculates a (normalized) degree centrality of a vertex.
    * @param  vertex  the vertex for which degree centrality is computed.
    * @return  the degree centrality value of the vertex.
    */
    public Integer findDegreeOf(Integer vertex) {
        Integer d = 0;
        int v = IndexMap.get(vertex);

        for (int i = 0; i < n; i++) {
            d += AdjacencyMatrix[v][i];
        }

        return d;
    }

    /**
     * Calculates a (normalized) closeness centrality of a vertex.
     * @param  vertex the vertex for which closeness centrality is computed.
     * @return the closeness centrality value of the vertex.
     */
    public Double findClosenessOf(Integer vertex) {
        //find a sum of path from vertex to all other vertices
        int m = IndexMap.get(vertex);
        Double sum = 0.0;
        for (int i = 0; i < m; i++) {
            sum = sum + DistanceMatrix[m][i];

        }
        for (int i = m + 1; i < n; i++) {
            sum = sum + DistanceMatrix[m][i];
        }

        if ((sum != 0) && (n > 1)) {
            return (n - 1) / sum;
        } else {
            return 0.0;
        }
    }

    /**
     * Calculates a (normalized) betweenness centrality of a vertex.
     * @param  vertex the vertex for which closeness centrality is computed.
     * @return the betweenness centrality value of the vertex.
     */
    public Double findBetweennessOf(Integer vertex) {
        return Cb[IndexMap.get(vertex)];
    }

    /**
     * Calculates a clustering coefficient of a vertex.
     * @param  vertex the vertex for which clustering coefficient is computed.
     * @return the clustering coefficient value of the vertex.
     */
    public Double findClusteringOf(Integer vertex) {
        //find the neighbors of our vertex
        Integer v = IndexMap.get(vertex);
        Vector<Integer> NeighborSet = new Vector();
        NeighborSet.add(v);
        int i = 0;
        int j = 0;
        for (i = 0; i < n; i++) {
            if (AdjacencyMatrix[v][i] == 1) {
                NeighborSet.add(i);
            }
        }

        if (NeighborSet.contains(v)) {
            NeighborSet.removeElement(v);
        }

        //compute the number of edges between the neighbors of our vertex
        Integer Sum = 0;
        Integer ivertex, jvertex;
        for (i = 0; i < NeighborSet.size(); i++) {
            for (j = i + 1; j < NeighborSet.size(); j++) {
                ivertex = NeighborSet.elementAt(i);
                jvertex = NeighborSet.elementAt(j);
                if (AdjacencyMatrix[ivertex][jvertex] == 1) {
                    Sum++;
                }
            }
        }

        Integer k = NeighborSet.size();
        if (k > 1) {
            Double D = (2 * Sum.doubleValue()) / (k.doubleValue() * (k.doubleValue() - 1));
            return D;
        } else {
            return 0.0;
        }
    }

    /**
     * Calculates the number of edges on the shortest path between two vertices.
     * @param  s source vertex
     * @param  t destination vertex
     * @return the number of edges on the shortest path between two vertices.
     */
    public Integer getDistance(Integer s, Integer t) {
        return DistanceMatrix[IndexMap.get(s)][IndexMap.get(t)];
    }
}
