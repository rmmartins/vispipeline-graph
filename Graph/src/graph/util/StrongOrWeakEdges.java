package graph.util;

import java.util.ArrayList;

/**
 *
 * algorithm to find strong edges that represent strong relationship between a group of vertices
 * No-connection is represented by Float.MaxValue
 * in this version max_depth MUST BE = 3
 * A-B-C-A (cicle with 3 edges) indicates strong edges
 * 
 * @author henry
 *
 */
public class StrongOrWeakEdges {

    private float[][] G;
    private ArrayList<Integer>[] neighbors;
    private Integer MAX_DEPTH;
    private Integer[] depth;
    private float[][] strengthMatrix;
    public static final float STRONG = -3;
    public static final float WEAK = -2;
    private int counter_exec = 0;

    public StrongOrWeakEdges(float[][] G, Integer max_depth) {
        this.G = G;
        this.MAX_DEPTH = max_depth;
        this.neighbors = new ArrayList[G.length];
        //this.depth = new Integer[G.length];
        float[] dist = new float[this.G.length];
        this.strengthMatrix = new float[G.length][G.length];
        for (int i = 0; i < G.length; i++) {
            //this.depth[i] = Integer.MAX_VALUE;
            for (int j = 0; j < G.length; j++) {
                this.strengthMatrix[i][j] = Float.MAX_VALUE;
            }
        }

        this.setNeighbors();

        this.computeMatrix();
    }

    private void computeMatrix() {

        Integer v;
        float d;
        ArrayList<Integer> visited;
        /*For each vertex of the graph, find the incident edges that are strong.*/
        for (int root = 0; root < this.G.length; root++) {
            //System.out.println("\n-------------------------------------\n\ninitial root: " + root + "\n");

            visited = new ArrayList<Integer>();
            Integer depth = new Integer(0);
            Boolean found = new Boolean(false);
            this.find(root, root, root, visited, depth);
        }

        for(int i = 0; i < this.G.length; i++){
            for(Integer neighbor: this.neighbors[i]){
                if (this.strengthMatrix[i][neighbor] == Float.MAX_VALUE){
                    this.strengthMatrix[i][neighbor] = this.strengthMatrix[neighbor][i] = WEAK;
                }
            }
        }

        System.out.println("Number of vertices: "+ G.length);
        System.out.println("Vertices²: "+ G.length*G.length);
        System.out.println("Number of interactions: " + counter_exec);
    }

    private ArrayList<Integer> neighbors(int next) {
        return this.neighbors[next];
    }

    private void setNeighbors() {
        for (int i = 0; i < this.G.length; i++) {
            neighbors[i] = new ArrayList<Integer>();
            for (int j = 0; j < this.G.length; j++) {
                if (i != j) {
                    if (this.G[i][j] < Float.MAX_VALUE && this.G[i][j] > 1) {
                        neighbors[i].add(j);
                    }
                }
            }
        }
    }

    /**
     *
     * @param vertex vertex to be found in the tree
     * @param root where algorithm starts in each interaction
     */

    int [][] path = new int[3][2];
    private void find(Integer vertex, Integer currentNeighbor, Integer root, 
                      ArrayList<Integer> visited, Integer depth) {
        
        //if vertex was found, return to first level and compute to another neighbors
        if (this.strengthMatrix[currentNeighbor][vertex] < Float.MAX_VALUE) {
            return;
        }

        counter_exec++;
        Integer newDepth = depth;
        //increment the depth
        newDepth++;

        //System.out.println("newDepth: " + newDepth);

       

        ArrayList<Integer> vList = (ArrayList<Integer>) this.neighbors[root].clone();
        //System.out.println("neighbors de " + root + ": " + this.neighbors[root]);

        Integer newCurrentNeighbor;
        Integer n;

        if (!vList.isEmpty()) {
            ArrayList<Integer> newVisited = (ArrayList<Integer>) visited.clone();

            if (newDepth == 1) {
                for (int i = 0; i < vList.size(); i++) {
                    n = vList.get(i);
                    if (n != vertex) {
                        newCurrentNeighbor = n;                    
                        this.path[0][0]=vertex;
                        this.path[0][1]=n;
                        this.find(vertex, newCurrentNeighbor, n, newVisited, newDepth);
                    }
                }

            } else if (newDepth <= MAX_DEPTH) {
                if (newDepth == 2) {
                    vList.remove(vertex);
                }
                if (vList.contains(vertex)) {
//                    this.strengthMatrix[vertex][currentNeighbor] =
//                            this.strengthMatrix[currentNeighbor][vertex] = STRONG;
                    this.path[newDepth-1][0]=root;
                    this.path[newDepth-1][1]=vertex;

                    //System.out.println("Encontrou ciclo: ");
                    for(int i = 0; i < newDepth; i++){
                        //System.out.print("["+this.path[i][0]+","+this.path[i][1]+"]");
                        this.strengthMatrix[this.path[i][0]][this.path[i][1]] =
                                this.strengthMatrix[this.path[i][1]][this.path[i][0]] = STRONG;
                    }
                    //System.out.print("\n");
                    return;
                } else {
                    //we do not want to back to visited vertex (remove cycles)
                    //just de cycle vertex[i] to vertex[i] can exist
                    vList.removeAll(visited);

                    newVisited.add(root);
                    for (int i = 0; i < vList.size(); i++) {
                        n = vList.get(i);
                        if (n != vertex) {
                            this.path[newDepth-1][0]=root;
                            this.path[newDepth-1][1]=n;
                            this.find(vertex, currentNeighbor, n, newVisited, newDepth);
                        } else {
                            return;
                        }
                    }
                }
            } else { // if newDepth > MAX_DEPTH
                return;
            }
        }
    }


    public void print() {
        String str = new String();
        for (int i = 0; i < this.strengthMatrix.length; i++) {
            for (int j = 0; j < this.strengthMatrix.length; j++) {
               System.out.println("["+ i +"]" + "["+ j +"]" + "=" + this.strengthMatrix[i][j]) ;
            }
        }        
    }


    public float[][] getStrengthMatrix() {
        return strengthMatrix;
    }


    public static void main(String args[]) {

        int size = 7;
        float matrix[][] = new float[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix[i][j] = Float.MAX_VALUE;
            }
        }

        matrix[0][1] = matrix[1][0] = 1;
        matrix[0][2] = matrix[2][0] = 1;
        matrix[1][2] = matrix[2][1] = 1;
        matrix[2][3] = matrix[3][2] = 1;
        matrix[1][4] = matrix[4][1] = 1;
        matrix[4][5] = matrix[5][4] = 1;
        matrix[5][6] = matrix[6][5] = 1;
        matrix[6][1] = matrix[1][6] = 1;
        matrix[3][5] = matrix[5][3] = 1;
        matrix[6][3] = matrix[3][6] = 1;
        matrix[1][3] = matrix[3][1] = 1;

        StrongOrWeakEdges strongedges = new StrongOrWeakEdges(matrix, 3);

        System.out.println("\n---------\n");
        float[][] m = strongedges.getStrengthMatrix();
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m.length; j++) {
                System.out.println("["+i+"]"+"["+j+"]"+"= "+m[i][j]);
            }
        }
    }
}
