package graph.util;

import visualizationbasics.util.filter.AbstractFilter;

/**
 * @author Henry Heberle
 */
public class VNAFilter extends AbstractFilter{

    @Override
    public String getDescription() {
        return "VNA graph file (*.vna).";
    }

    @Override
    public String getProperty() {
        return "GraphData.DIR";
    }

    @Override
    public String getFileExtension() {
        return "vna";
    }
}
