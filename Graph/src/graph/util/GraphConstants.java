/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph.util;

/**
 *
 * @author Fernando
 */
public interface GraphConstants {

    public static final String PROPFILENAME = "graph.properties";
    public static final String DELAUNAY = "Delaunay";
    public static final String NJ = "Neighbor-Joinning";
    public static final String MST = "Minimum Spanning Tree";

}
