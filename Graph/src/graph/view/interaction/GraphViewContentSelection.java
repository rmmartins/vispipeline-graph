///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package graph.view.interaction;
//
//import graph.model.GraphInstance;
//import graph.view.GraphReportView;
//import java.io.IOException;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import java.util.ArrayList;
//import javax.swing.ImageIcon;
//import javax.swing.JOptionPane;
//import projection.util.ProjectionConstants;
//import textprocessing.corpus.Corpus;
//import tree.model.TreeInstance;
//import tree.model.TreeModel;
//import visualizationbasics.model.AbstractInstance;
//import visualizationbasics.util.ImageCollection;
//import visualizationbasics.util.ImageOpenDialog;
//import visualizationbasics.util.PropertiesManager;
//import visualizationbasics.util.filter.ZIPFilter;
//import visualizationbasics.view.ModelViewer;
//import textprocessing.processing.view.MultipleFileView;
//import textprocessing.processing.view.TextOpenDialog;
//import tree.view.MultipleImageView;
//import tree.view.TreeFrame;
//
///**
// *
// * @author Fernando Vieira Paulovich
// */
//public class GraphViewContentSelection extends TreeAbstractSelection {
//
//    public GraphViewContentSelection(ModelViewer viewer) {
//        super(viewer);
//    }
//
//    public ArrayList<AbstractInstance> excludeVirtualNodes(ArrayList<AbstractInstance> instances) {
//        ArrayList<AbstractInstance> result = new ArrayList<AbstractInstance>();
//        result.addAll(instances);
//        for (int i=0;i<result.size();i++) {
//               AbstractInstance ins = result.get(i);
//               if (ins.toString().trim().isEmpty()) {
//                   result.remove(ins);
//                   i--;
//               }
//            }
//        return result;
//    }
//
//    @Override
//    public void selected(ArrayList<AbstractInstance> selinst) {
//        if ((selinst == null)||(selinst.isEmpty())) return;
//        ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
//        if (selinst.size() == 1) {
//            instances = selectHierarchy((TreeInstance)selinst.get(0));
//            viewer.getModel().setSelectedInstances(instances);
//            instances = excludeVirtualNodes(instances);
//        }else instances = excludeVirtualNodes(selinst);
//        if (viewer.getModel() != null) {
//            viewer.getModel().setSelectedInstances(instances);
//            boolean isText = ((TreeInstance)selinst.get(0)).toString().contains(".txt");
//            ImageCollection im = null;
//            Corpus c = null;
//            if (!isText) im = ((TreeModel)viewer.getModel()).getImageCollection();
//            else c = ((TreeModel)viewer.getModel()).getCorpus();
//
//            if (!isText) {
//                if ((im != null)&&(ImageOpenDialog.checkImages(im,viewer.getModel()))) {
//                    MultipleImageView.getInstance(viewer).display(im,(TreeFrame)viewer,instances);
//                }else {
//                    try {
//                        PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
//                        im = ImageOpenDialog.openImages(spm,new ZIPFilter(), viewer);
//                        if (ImageOpenDialog.checkImages(im,viewer.getModel())) {
//                            ((TreeModel)viewer.getModel()).setImageCollection(im);
//                            ((GraphReportView)(((TreeFrame)viewer).getReportPanel())).setDataSource(im.getFilename());
//                            MultipleImageView.getInstance(viewer).display(im,(TreeFrame)viewer,instances);
//                        }else {
//                            String message = "There are non-correponding files on the chosen images file. Would like to proceed?";
//                            int answer = JOptionPane.showOptionDialog(viewer,message,"Openning Warning",
//                                                                      JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE,null,null,null);
//                            if (answer == JOptionPane.YES_OPTION) {
//                                MultipleImageView.getInstance(viewer).display(im,(TreeFrame)viewer,instances);
//                            }
//                        }
//                    } catch (IOException ex) {
//                        Logger.getLogger(GraphViewContentSelection.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//            }else {
//                if ((c != null)&&(TextOpenDialog.checkCorpus(c,viewer.getModel()))) {
//                    MultipleFileView.getInstance(viewer).display(c,instances);
//                }else {
//                    try {
//                        PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
//                        c = TextOpenDialog.openCorpus(spm,new ZIPFilter(), viewer);
//                        if (TextOpenDialog.checkCorpus(c,viewer.getModel())) {
//                            ((TreeModel)viewer.getModel()).setCorpus(c);
//                            MultipleFileView.getInstance(viewer).display(c,instances);
//                        }else {
//                            String message = "There are non-correponding files on the chosen file. Would like to proceed?";
//                            int answer = JOptionPane.showOptionDialog(viewer,message,"Openning Warning",
//                                                                      JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE,null,null,null);
//                            if (answer == JOptionPane.YES_OPTION) {
//                                MultipleFileView.getInstance(viewer).display(c,instances);
//                            }
//                        }
//                    } catch (IOException ex) {
//                        Logger.getLogger(GraphViewContentSelection.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//            }
//
////            if ((im == null)||(!OpenDialog.checkImages(im,viewer.getModel()))) {
////                try {
////                    PropertiesManager spm = PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME);
////                    im = OpenDialog.openImages(spm,new ZIPFilter(), viewer);
////                    if (OpenDialog.checkImages(im,viewer.getModel())) {
////                        ((TreeModel)viewer.getModel()).setImageCollection(im);
////                        MultipleImageView.getInstance(viewer).display(im,instances);
////                    }else {
////                        String message = "There are non-correponding files on the chosen images file. Would like to proceed?";
////                        int answer = JOptionPane.showOptionDialog(viewer,message,"Openning Warning",
////                                                                  JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE,null,null,null);
////                        if (answer == JOptionPane.YES_OPTION) {
////                            MultipleImageView.getInstance(viewer).display(im,instances);
////                        }
////                    }
////                } catch (IOException ex) {
////                    Logger.getLogger(GraphViewContentSelection.class.getName()).log(Level.SEVERE, null, ex);
////                }
////            }else {
////                MultipleImageView.getInstance(viewer).display(im,instances);
////            }
//            viewer.getModel().notifyObservers();
//        }
//    }
//
//    @Override
//    public ImageIcon getIcon() {
//        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Copy16.gif"));
//    }
//
//    @Override
//    public String toString() {
//        return "View Content";
//    }
//
//    @Override
//    public void selected(int x, int y) {
//    }
//
//    @Override
//    public void released(GraphInstance instance) {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }
//
//}
