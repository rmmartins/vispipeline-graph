package graph.view.interaction;

import graph.model.GraphInstance;
import visualizationbasics.view.ModelViewer;
import visualizationbasics.view.selection.AbstractSelection;

/**
 *
 * @author Jose Gustavo
 */
public abstract class GraphAbstractSelection extends AbstractSelection {

    public GraphAbstractSelection(ModelViewer viewer) {
        super(viewer);
    }

    public abstract void selected(int x, int y);

    public abstract void released(GraphInstance instance);

    }
