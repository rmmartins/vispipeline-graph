package graph.view.interaction;

import graph.model.Connectivity;
import graph.model.Edge;
import graph.model.GraphModel;
import java.util.ArrayList;

/**
 *
 * @author Henry Heberle
 */
public class CountCrossingEdges {

    private final GraphModel model;
    private int sobrepostos = 0;
    private int notCrossing = 0;

    public CountCrossingEdges(GraphModel model) {
        this.model = model;
    }

    public int execute() {
        Edge edgei;
        Edge edgej;
        if (model != null) {
            Connectivity selcon = model.getSelectedConnectivity();
            int numberOfEdges = selcon.getEdges().size();
            System.out.println(selcon.getName());
            System.out.println("Número de arestas:" + numberOfEdges);
            ArrayList<Edge> edges = selcon.getEdges();
            int cruzou = 0;
            this.sobrepostos = 0;
            for (int i = 0; i < numberOfEdges - 1; i++) {
                for (int j = i + 1; j < numberOfEdges; j++) {
                    try {
                        edgei = edges.get(i);
                        edgej = edges.get(j);
                        if (edgei.isValid() && edgej.isValid()) {
                            if (edgei.crosses(edgej, model)) {
                                cruzou++;
                                edgei.setCrossingEdge(true);
                                edgej.setCrossingEdge(true);
                            }
                        }
                    } catch (Exception ex) {
                        sobrepostos++;
                    }
                }
            }
            this.notCrossing = 0;
            for (Edge e : edges) {
                if (!e.isCrossingEdge() && e.isValid()) {
                    notCrossing++;
                }
            }

            //todo 
            //if vertices is enabled (visible)...
            //->>> replaced by if (edge.isValid())
            return cruzou;
        }
        return -1;
    }

    public int getSobrepostos() {
        return this.sobrepostos;
    }

    public int getNotCrossing() {
        return notCrossing;
    }
}
