package graph.view.interaction;

import graph.model.GraphInstance;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.view.ModelViewer;

/**
 *
 * @author Henry Heberle
 */
public class ShowVertexLabel extends GraphAbstractSelection {

    public ShowVertexLabel(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(int x, int y) {
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (viewer.getModel() != null) {
            for (AbstractInstance gi : selinst) {
                if (((GraphInstance) gi).isShowLabel()) {
                    ((GraphInstance) gi).setShowLabel(false);
                } else {
                    ((GraphInstance) gi).setShowLabel(true);
                }                
            }

//            for (int i = 0; i < viewer.getCoordinators().size(); i++) {
//                viewer.getCoordinators().get(i).coordinate(selinst, null);
//            }
//
//            viewer.getModel().setSelectedInstances(selinst);
        
           //viewer.getModel().cleanSelectedInstances();

            viewer.getModel().setChanged();
            viewer.getModel().notifyObservers();
            
        }
    }

    @Override
    public ImageIcon getIcon() {
        ClassLoader cldr = this.getClass().getClassLoader();
        java.net.URL imageURL = cldr.getResource("graph/icons/showSelectedLabel.gif");
        return new ImageIcon(imageURL);
    }

    @Override
    public String toString() {
        return "Show Labels of Selected Vertices";
    }

    @Override
    public void released(GraphInstance instance) {
    }
}
