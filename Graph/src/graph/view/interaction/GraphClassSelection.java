///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package graph.view.interaction;
//
//import graph.model.GraphInstance;
//import java.util.ArrayList;
//import javax.swing.ImageIcon;
//import projection.model.ProjectionInstance;
//import tree.model.TreeInstance;
//import tree.model.TreeModel;
//import tree.view.TreeFrame;
//import visualizationbasics.model.AbstractInstance;
//import visualizationbasics.model.GraphScalar;
//import visualizationbasics.view.ModelViewer;
//
///**
// *
// * @author Fernando Vieira Paulovich
// */
//public class GraphClassSelection extends GraphClassAbstractSelection {
//
//    private GraphScalar ncdata = null;
//    private float indexCdata = 0.0f;
//    private int indexGraphScalar;
//
//    public GraphClassSelection(ModelViewer viewer) {
//        super(viewer);
//    }
//
//    public boolean isElement(ArrayList<GraphScalar> GraphScalars,String name) {
//        if (GraphScalars == null || GraphScalars.isEmpty()) return false;
//        for (int i=0;i<GraphScalars.size();i++)
//            if (GraphScalars.get(i).getName().equalsIgnoreCase(name))
//                return true;
//        return false;
//    }
//
//    protected void setClass(ArrayList<AbstractInstance> selinst) {
//        //Verifying if the GraphScalar was deleted...
//            if (((TreeModel)viewer.getModel()).getGraphScalars().indexOf(ncdata)==-1) ncdata = null;
//
//            if (ncdata == null) {
//                while (isElement(((TreeModel)viewer.getModel()).getGraphScalars(),"New Class "+indexGraphScalar))
//                    indexGraphScalar++;
//                ncdata = ((TreeModel)viewer.getModel()).addGraphScalar("New Class "+indexGraphScalar);
//                indexGraphScalar++;
//                indexCdata = 0;
//                //Setting all the instances with class 0 of this GraphScalar...
//                for (int i=0;i<viewer.getModel().getInstances().size();i++) {
//                    TreeInstance pi = (TreeInstance) viewer.getModel().getInstances().get(i);
//                    pi.setGraphScalarValue(ncdata,indexCdata);
//                }
//                indexCdata++;
//            }
//            //Setting just the selected instances with the GraphScalar...
//            for (int i=0;i<selinst.size();i++) {
//                ProjectionInstance pi = (ProjectionInstance) selinst.get(i);
//                pi.setGraphScalarValue(ncdata,indexCdata);
//            }
//            indexCdata++;
//    }
//
//    @Override
//    public void selected(ArrayList<AbstractInstance> selinst) {
//        if (viewer.getModel() != null) {
//            ArrayList<AbstractInstance> instances = new ArrayList<AbstractInstance>();
//            if (selinst.size() == 1) {
//                instances = selectHierarchy((TreeInstance)selinst.get(0));
//            }else {
//                instances.addAll(selinst);
//            }
//            setClass(instances);
//            ((TreeFrame)viewer).updateGraphScalars(ncdata);
//            viewer.getModel().setSelectedInstances(instances);
//            viewer.getModel().notifyObservers();
//        }
//    }
//
//    @Override
//    public ImageIcon getIcon() {
//        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/AlignCenter16.gif"));
//    }
//
//    @Override
//    public String toString() {
//        return "Class Selection";
//    }
//
//    @Override
//    public void reset() {
//        ncdata = null;
//        indexGraphScalar = 1;
//    }
//
//    @Override
//    public void selected(int x, int y) {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }
//
//    @Override
//    public void released(GraphInstance instance) {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }
//
//}
