//package graph.view.interaction;
//
//import graph.view.interaction.GraphAbstractSelection;
//import java.util.ArrayList;
//import tree.model.TreeInstance;
//import visualizationbasics.model.AbstractInstance;
//import visualizationbasics.view.ModelViewer;
//
//public abstract class TreeAbstractSelection extends GraphAbstractSelection {
//
//    public TreeAbstractSelection(ModelViewer viewer) {
//        super(viewer);
//    }
//
//    protected ArrayList<AbstractInstance> selectHierarchy(TreeInstance node) {
//        if (node != null) {
//            ArrayList<AbstractInstance> result = new ArrayList<AbstractInstance>();
//            addNode(result,node);
//            return result;
//        }else return null;
//  }
//
//    protected void addNode(ArrayList<AbstractInstance> vertices, TreeInstance node) {
//        if (node != null) {
//           vertices.add(node);
//           ArrayList<TreeInstance> children = node.getChildren();
//           if (children != null) {
//               for (int i=0;i<children.size();i++) {
//                   TreeInstance son = children.get(i);
//                   if (son != null) {
//                       addNode(vertices,son);
//                   }
//               }
//           }
//        }
//    }
//
//
//}
