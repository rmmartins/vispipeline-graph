/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package featureextraction.waveletgaborextractor;
import java.io.*;
import javax.imageio.ImageIO;
import java.io.File;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import java.awt.image.BufferedImage;
import java.awt.Image;
import javax.swing.ImageIcon;
import java.util.List;
import visualizationbasics.util.Util;

/**
 * @author Laura Florian
 */
public class WaveletGabor {

    String path;
    int scales;
    int orientations;
    File fileOpenImag;

    private float matrixPoints[][];
    private float[] cdata;
    private ArrayList<String> nameImages;
    private ArrayList<Integer> imagesIds;

   public WaveletGabor(String pathF, int esc , int ori)
    {
        this.path=pathF;
        this.scales=esc;
        this.orientations=ori;
        load();
    }

   public float[][] getMatrixPoints() {
        return matrixPoints;
    }

   public ArrayList<String> getNameImages() {
        return nameImages;
    }

    public ArrayList<Integer> getImagesIDs() {
        return imagesIds;
    }
    public float[] getClassData() {
        return this.cdata;
    }

     /** Pega o nome das imagens e armazena em um vetor de strings */
    private ArrayList<String> getNameImages(Vector<File> filesTrainer) {
        ArrayList<String> nameImages = new ArrayList<String>();
        for (int i = 0; i < filesTrainer.size(); i++) {
            nameImages.add(filesTrainer.get(i).getName());
        }
        return nameImages;
    }

    private ArrayList<Integer> getImagesIDs(Vector<File> filesTrainer) {
        ArrayList<Integer> ids = new ArrayList<Integer>();

        for (int i = 0; i < filesTrainer.size(); i++) {
            int id = Util.convertToInt(filesTrainer.get(i).getName());
            ids.add(id);
        }
        return ids;
    }

    public void load(){
        int radio= 127;
        double Uh= 0.4;
        double Ul= 0.05;
        int sizeMask = 256;
        double[][] G_REAL = new double[sizeMask][sizeMask];
        double[][] G_IMAG = new double[sizeMask][sizeMask];
        fileOpenImag = new File (this.path);

        /*FILTERS*/
        getFilters(G_REAL, G_IMAG, orientations, scales, radio , Uh, Ul, sizeMask);

        /*FEATURES*/
        getFeatures(fileOpenImag, scales, orientations, radio, Ul, Uh, G_REAL, G_IMAG, sizeMask);
    }

    void getFilters(double [] [] G_REAL,double [] [] G_IMAG , int orientations, int scales, int radio , double Uh, double Ul, int sizeMask){
        int numMask = sizeMask/(radio*2+1);
        int mr, kr;
        double[][] Gr  = new double[radio*2+1][radio*2+1];
        double[][] Gi = new double[radio*2+1][ radio*2+1];
        double[][] G_real = new double[radio*2+1][radio*2+1];
        double[][] G_imag = new double[radio*2+1][ radio*2+1];

        for (int i = 0; i < scales; i++){
            for (int j = 0; j < orientations; j++){
                //GABOR FILTER
                Gabor(Gr, Gi, radio, i+1, j+1, Ul, Uh, scales, orientations);
                Mat_FFT2(G_real, G_imag, Gr, Gi);
                for (int m= 0; m < numMask; m++){
                    for (int k = 0; k < numMask; k++){
                        mr = m*(radio*2+1);
                        kr = k*(radio*2+1);
                        matrixCopy(G_REAL,G_real, mr, kr, 0, 0, G_real.length-1, G_real[0].length-1);
                        matrixCopy(G_IMAG,G_imag, mr, kr, 0, 0, G_imag.length-1, G_imag[0].length-1);
                    }
                }
            }
        }
    }

     void Gabor(double [][] Gr, double [][]Gi, int radio, int s, int n, double Ul, double Uh, int escala, int orientations)
    {
        double X, Y, G;
        double base = Uh/Ul;
        double a = Math.pow(base, 1.0/(double)(escala-1));
        double u0 = Uh/Math.pow(a, (double) escala-s);
        double Uvar = (a-1.0)*u0/((a+1.0)*Math.sqrt(2.0*Math.log(2.0)));
        double z = -2.0*Math.log(2.0)*(Uvar*Uvar)/u0;
        double Vvar = Math.tan(Math.PI/(2*orientations))*(u0+z)/Math.sqrt(2.0*Math.log(2.0)-z*z/(Uvar*Uvar));
        double Xvar = 1.0/(2.0*Math.PI*Uvar);
        double Yvar = 1.0/(2.0*Math.PI*Vvar);
        double t1 = Math.cos(Math.PI/orientations*(n-1.0));
        double t2 = Math.sin(Math.PI/orientations*(n-1.0));

        for (int x=0;x<2*radio+1;x++) {
            for (int y=0;y<2*radio+1;y++) {
                X = (double) (x-radio)*t1+ (double) (y-radio)*t2;
                Y = (double) -(x-radio)*t2+ (double) (y-radio)*t1;
                G = 1.0/(2.0*Math.PI*Xvar*Yvar)*Math.pow(a, (double) escala-s)*Math.exp(-0.5*((X*X)/(Xvar*Xvar)+(Y*Y)/(Yvar*Yvar)));
                Gr[x][y] = G*Math.cos(2.0*Math.PI*u0*X);
                Gi[x][y] = G*Math.sin(2.0*Math.PI*u0*X);
            }
        }
    }

    void getFeatures( File file ,  int scales, int orientations ,  int radio, double Ul, double Uh, double [][] G_REAL, double [][] G_IMAG,  int sizeMask){

        String[] filesList=file.list();
        int numFiles= filesList.length;
        int i,j;
        double [] v = new double[scales*orientations*2];
        double [][] imagenM;
        String pathImag="", patho="";

        /*Get only images*/
        Vector<File> filesTrainer = new Vector<File>();
        for (File f: new File(path).listFiles()){
            if (f.isFile()){
                filesTrainer.add(f);
            }
        }
        nameImages = getNameImages(filesTrainer);
        imagesIds = getImagesIDs(filesTrainer);
        createCdata(filesTrainer);
        List a = new ArrayList<String>();
        for( i=0; i<numFiles; i++){
            patho= path + File.separator + filesList[i];
            Image img = new ImageIcon(patho).getImage();
            if ( img.getWidth(null) >1){
                a.add(filesList[i]);
            }
        }
        String[] imagesList= new String [a.size()];
        for( i=0; i<a.size(); i++){
            imagesList[i]=(String) a.get(i);
        }

        numFiles= imagesList.length;
        matrixPoints = new float [numFiles] [scales*orientations*2];

        for( i=0; i<numFiles; i++){
            pathImag= path + File.separator + imagesList[i];
            imagenM=getImageM(pathImag);
            System.out.println((i+1) + ": " + pathImag);
            getImageFeatures(v, radio, scales, orientations, Ul, Uh, imagenM, G_REAL, G_IMAG, sizeMask);
            for ( j = 0; j < scales*orientations*2; j++){
                matrixPoints[i][j]= (float) v[j];
            }
        }
    }

    void getImageFeatures( double [] v,  int radio,  int scales, int orientations, double Ul, double Uh, double [] [] img, double [] [] G_real, double [] [] G_imag ,int sizeMask)
    {
         int highMatrix = sizeMask;
         int widthMatrix = sizeMask;
         int j=0;
         double [] mediaDeviations = new double [2];
         double [] [] img_real = new double [highMatrix][widthMatrix];
         double [] [] img_imag = new double [highMatrix][widthMatrix];
         double [] [] IMG_real = new double [highMatrix][widthMatrix];
         double [] [] IMG_imag = new double [highMatrix][widthMatrix];
         double [] [] IMG_REAL = new double [highMatrix][widthMatrix];
         double [] [] IMG_IMAG = new double [highMatrix][widthMatrix];

         Mat_FFT2(img_real, img_imag, img, img);
         for (int i=0; i < scales*orientations; i++){
                complexMultiply(img_real, img_imag, G_real, G_imag, IMG_real, IMG_imag);
                Mat_IFFT2(IMG_REAL, IMG_IMAG, IMG_real, IMG_imag);
                calculateFeatures(IMG_REAL, IMG_IMAG, mediaDeviations);
                v[j]=(mediaDeviations[0]);
                v[j+1]=(mediaDeviations[1]);
                j=j+2;
         }
    }

    void calculateFeatures(double [][] Gabor_r, double [][] Gabor_i, double [] mediaDeviations)
    {
         int i,j;
         double m, d;
         m = 0; d = 0;
         for (i=0; i< Gabor_r.length ; i++){
             for (j=0; j<Gabor_r[0].length ; j++){
                 m = Math.sqrt(Math.pow(Gabor_r[i][j],2)+ Math.pow(Gabor_i[i][j],2)) + m;
             }
         }
         m = m/(Gabor_r[0].length*Gabor_r.length);
         for (i=0; i< Gabor_r.length ; i++){
             for (j=0; j<Gabor_r[0].length ; j++){
                 d = Math.pow(Math.sqrt(Math.pow(Gabor_r[i][j],2)+Math.pow(Gabor_i[i][j],2))- m, 2) + d;
             }
         }
         d = Math.sqrt(d/(Gabor_r[0].length*Gabor_r.length));
         mediaDeviations[0] = m;
         mediaDeviations[1] = d;
    }

    double[][] getImageM(String filename) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(filename));
        }
        catch (Exception e) {
            System.err.println(filename);
            e.printStackTrace();
        }
        int height = -1;
        int width = -1;

        width = img.getWidth();
        height = img.getHeight();
        double[][] ret = new double[width][height];

        if (height != 512 || width !=512) {
            System.out.println("Could not use WaveletGabor, it need an imagem 512 x 512!");
            return null;
        }
        else{
            for (int i = 0; i < width; ++i) {
                for (int j = 0; j < height; ++j) {
                    Color c = new Color(img.getRGB(i, j));
                    ret[i][j] = (double)(c.getRed() + c.getGreen() + c.getBlue()) / 3;
                }
            }
        }
        return ret;
    }

    double log2(double num){
        return (Math.log(num)/Math.log(2));
    }

    void Mat_FFT2(double [][] Output_real, double [][] Output_imag, double [][] Input_real, double [][] Input_imag)
    {
	int xs, ys, i, j;
        xs = (int) Math.pow(2.0, Math.ceil(log2((double)(Input_real.length))));
        ys = (int) Math.pow(2.0, Math.ceil(log2((double)(Input_real[0].length))));
        double [][] R= new double [xs+1][ys+1];
        double [][] I= new double [xs+1][ys+1];
        double [][] Fr= new double [xs+1][ys+1];
        double [][] Fi= new double [xs+1][ys+1];

        for (i = 1; i <= Input_real.length; i++){
            for (j = 1; j <= Input_real[0].length; j++) {
                R[i][j] = Input_real[i-1][j-1];
                I[i][j] = Input_imag[i-1][j-1];
            }
        }
        four2(Fr, Fi, R, I, xs, ys, 1);

        for (i = 1; i <= Output_real.length; i++){
            for (j = 1; j <= Output_real[0].length; j++) {
                Output_real[i-1][j-1] = Fr[i][j];
                Output_imag[i-1][j-1] = Fi[i][j];
            }
        }
    }

    void four2(double [][]fftr, double [][]ffti, double [][]rdata, double [][]idata, int rs, int cs, int isign)
    {
        int i, j;
        double [][] T = new double [(2*rs)+1][cs+1];
        double [] tmp1 = new double [(2*rs)+1];
        double [] tmp2 = new double [(2*rs)+1];

        for (i=1;i<=rs;i++) {
            for (j=1;j<=cs;j++) {
                tmp1[j*2-1] = rdata[i][j];
                tmp1[j*2] = idata[i][j];
            }
            four1(tmp1, cs, isign);
            for (j=1;j<=cs;j++) {
                T[i*2-1][j] = tmp1[j*2-1];
                T[i*2][j] = tmp1[j*2];
           }
        }
        for (i=1;i<=cs;i++) {
            for (j=1;j<=rs;j++) {

                tmp2[j*2-1] = T[j*2-1][i];
                tmp2[j*2] = T[j*2][i];
            }
            four1(tmp2,rs,isign);
            for (j=1;j<=rs;j++) {
              fftr[j][i] = tmp2[j*2-1];
                ffti[j][i] = tmp2[j*2];
            }
        }
    }

    void SWAP(double a,double b) {
        double tempr;
         tempr=(a);
         (a)=(b);
         (b)=tempr;
     }

    void four1(double []data, int nn, int isign)
    {
	int n, mmax, m, j, istep, i;
	double wtemp, wr, wpr, wpi, wi, theta;
	double tempr, tempi;
        n = nn << 1;
        j = 1;

        for (i=1;i<n;i+=2) {
		if (j > i) {
			SWAP(data[j],data[i]);
			SWAP(data[j+1],data[i+1]);
		}
		m = n >> 1;
		while (m >= 2 && j > m) {
			j -= m;
                        m >>= 1;
		}
		j += m;
	}
	mmax = 2;

     	while (n > mmax) {
		istep = 2*mmax;
		theta = 6.28318530717959/(isign*mmax);
		wtemp = Math.sin(0.5*theta);
		wpr = -2.0*wtemp*wtemp;
		wpi = Math.sin(theta);
		wr = 1.0;
		wi = 0.0;
                for (m=1;m<mmax;m+=2) {
                        for (i=m;i<=n;i+=istep) {
				j = i+mmax;
				tempr = wr*data[j]-wi*data[j+1];
				tempi = wr*data[j+1]+wi*data[j];
				data[j] = data[i]-tempr;
				data[j+1] = data[i+1]-tempi;
				data[i] += tempr;
				data[i+1] += tempi;
			}
			wr = (wtemp=wr)*wpr-wi*wpi+wr;
			wi = wi*wpr+wtemp*wpi+wi;
		}
		mmax = istep;
	}
    }

    void Mat_IFFT2(double [][] Output_real, double [][] Output_imag, double [][] Input_real,double [][] Input_imag)
    {
            int xs, ys, i, j;
            xs = (int) Math.pow(2.0, Math.ceil(log2((double)(Input_real.length))));
            ys = (int) Math.pow(2.0, Math.ceil(log2((double)(Input_real[0].length))));

            double [][] R= new double [xs+1][ys+1];
            double [][] I= new double [xs+1][ys+1];
            double [][] Fr= new double [xs+1][ys+1];
            double [][] Fi= new double [xs+1][ys+1];

            for (i = 1; i <= Input_real.length; i++)
                for (j = 1; j <= Input_real[0].length; j++) {
                    R[i][j] = Input_real[i-1][j-1];
                    I[i][j] = Input_imag[i-1][j-1];
                }

            four2(Fr, Fi, R, I, xs, ys, -1);         // 2-D IFFT

            double NN = (double) (xs*ys);

            for (i = 1; i <= Output_real.length; i++)
                for (j = 1; j <= Output_real[0].length; j++) {
                    Output_real[i-1][j-1] = Fr[i][j]/NN;
                    Output_imag[i-1][j-1] = Fi[i][j]/NN;
                }
    }

    void matrixCopy(double [][] A, double [][] B, int rowDestin, int columDestin, int rowBegin, int columBegin, int rowEnd, int columEnd)
    {
        int i, j, f, c, row, colum;
        int highMatrix = A.length;
        int widthMatrix= A[0].length;
        int ai, aj, bi, bj;

        if ((rowDestin >= 0)&&(rowDestin < highMatrix)&&(columDestin >= 0)&&(columDestin < widthMatrix)) {
            if ((rowBegin >= 0)&&(rowBegin < B.length)&&(columBegin >= 0)&&(columBegin < B[0].length)) {
                    f = rowEnd-rowBegin+1;
                    c = columEnd-columBegin+1;
                    if ((f >= 1)&&(c >= 1)) {
                            row = rowDestin+f-1;
                            colum = columDestin+c-1;
                            if ((row < highMatrix)&&(colum < widthMatrix)) {
                                    for (i=0;i<f;i++) {
                                            for (j=0;j<c;j++) {
                                                ai=i+rowDestin;
                                                aj=j+columDestin;
                                                bi=i+rowBegin;
                                                bj=j+columBegin;
                                                A[ai][aj] = B[bi][bj];
                                            }
                                    }
                            }
                    }
            }
         }
         else {
                System.out.println("error dimension matrix!");
         }
    }

    void complexMultiply(double [][] img_real, double [][] img_imag, double [][] G_real, double [][] G_imag,
            double [][] filteredImg_real, double [][] filteredImg_imag)
    {
            int xs, ys;
            xs = img_real.length;
            ys = img_imag[0].length;
            double [][]Tmp_1 = new double[xs][ys];
            double [][]Tmp_2 = new double[xs][ys];

            multiplyMatrix(Tmp_1, G_real, img_real);
            multiplyMatrix(Tmp_2, G_imag, img_imag);
            subMatrix(filteredImg_real,Tmp_1, Tmp_2);
            multiplyMatrix(Tmp_1,G_real, img_imag);
            multiplyMatrix(Tmp_2, G_imag, img_real);
            addMatrix(filteredImg_imag,Tmp_1, Tmp_2);
    }

    void addMatrix(double [][] data, double [][] A, double [][] B)
    {
            int h, w;
            int highMatrix=data.length;
            int widthMatrix=data[0].length;

            for (h=0;h<highMatrix;h++) {
                    for (w=0;w<widthMatrix;w++) {
                            data[h][w] = A[h][w]+B[h][w];
                    }
            }
    }

    void subMatrix(double [][] data, double [][] A, double [][] B)
    {
            int h, w;
            int highMatrix=data.length;
            int widthMatrix=data[0].length;

            for (h=0;h<highMatrix;h++) {
                    for (w=0;w<widthMatrix;w++) {
                            data[h][w] = A[h][w]-B[h][w];
                    }
            }
    }

    void multiplyMatrix(double [][] data, double [][] A, double [][] B)
    {
            int h, w;
            int highMatrix=data.length;
            int widthMatrix=data[0].length;

            for (h=0;h<highMatrix;h++) {
                    for (w=0;w<widthMatrix;w++) {
                            data[h][w] = A[h][w]*B[h][w];
                    }
            }
    }
    private void createCdata(Vector<File> filesTrainer) {
        if (filesTrainer.size() > 0) {
            //Capturing the initials
            ArrayList<String> initials_aux = new ArrayList<String>();
            for (int i = 0; i < filesTrainer.size(); i++) {
                    String filename = filesTrainer.get(i).getName();
                    int begin = filename.lastIndexOf("_");
                    String ini;
                    if (begin > -1) {
                        filename = filename.substring(0, begin);
                        ini = filename;
                    } else {
                        ini = filename;
                        if (filename.length() > 2) {
                            ini = filename.substring(0, 2);
                        }
                    }
                    //Create the initials with two letters
                    if (!initials_aux.contains(ini)) {
                        initials_aux.add(ini);
                    }
            }

            String[] initials = new String[initials_aux.size()];
            for (int i = 0; i < initials_aux.size(); i++) {
                initials[i] = initials_aux.get(i);
            }

            Arrays.sort(initials);

            //Creating the cdata
            if (initials.length > 1) {
                this.cdata = new float[filesTrainer.size()];
                for (int i = 0; i < filesTrainer.size(); i++) {
                    this.cdata[i] = -1;
                    for (int j = 0; j < initials.length; j++) {

                        //Taking out the part of the file that correspond to the directory
                        String filename = filesTrainer.get(i).getName();
                        //int begin=filename.lastIndexOf(System.getProperty("file.separator"));
                        int begin = filename.lastIndexOf("_");
                        if (begin > -1) {
                            filename = filename.substring(0, begin);
                        }

                        //Given the cdata number
                        if (filename.startsWith(initials[j])) {
                            this.cdata[i] = j;
                        }
                    }
                }
            } else {
                this.cdata = new float[filesTrainer.size()];
                Arrays.fill(this.cdata, 0.0f);
            }
        }
    }
}


