/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package featureextraction.waveletgaborextractor;

import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import matrix.AbstractMatrix;
import projection.util.ProjectionConstants;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.util.PropertiesManager;

/**
 *
 * @author Laura Florian
 */
@VisComponent(hierarchy = "Feature Extraction",
name = "Wavelet Gabor Feature Extractor",
description = "extracting features from an image collection.")
public class WaveletGaborComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {

        WaveletGaborExtractor pe = new WaveletGaborExtractor();
        matrix = pe.extract(PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME), collection, this.scales , this.orientations);
        
        //saving the matrix
        if (matrix != null && saveMatrix && matrixFilename != null && !matrixFilename.isEmpty()) {
            //saving the points matrix
            System.out.println("Saving points matrix...");
            matrix.save(matrixFilename);
        }

        dmat = new DistanceMatrix(matrix,DissimilarityFactory.getInstance(disstype));

        if (dmat != null && saveDmat && dmatFilename != null && !dmatFilename.isEmpty()) {
            //saving distance matrix
            System.out.println("Saving distance matrix...");
            dmat.save(dmatFilename);
        }

    }
    
    public AbstractMatrix output() {
        return matrix;
    }

    public DistanceMatrix outputDmat() {
        return dmat;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new WaveletGaborParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        //matrixFilename = dmatFilename = collection = "";
        matrix = null;
        dmat = null;
        //saveMatrix = saveDmat = false;
        //disstype = DissimilarityFactory.getInstance(DissimilarityType.EUCLIDEAN);
    }
    
    public static final long serialVersionUID = 1L;
    private transient WaveletGaborParamView paramview;

    public String getCollection() {
        return collection;
    }

    public void setCollection(String c) {
        collection = c;
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public boolean isSaveMatrix() {
        return saveMatrix;
    }

    public void setSaveMatrix(boolean saveMatrix) {
        this.saveMatrix = saveMatrix;
    }

    public boolean isSaveDmat() {
        return saveDmat;
    }

    public void setSaveDmat(boolean saveDmat) {
        this.saveDmat = saveDmat;
    }

    public String getMatrixFilename() {
        return matrixFilename;
    }

    public void setMatrixFilename(String matrixFilename) {
        this.matrixFilename = matrixFilename;
    }

    public String getDmatFilename() {
        return dmatFilename;
    }
    
    public int getScales() {
        return scales;
    }
    
    public void setScales(String scalesText) {
        this.scales = Integer.parseInt( scalesText);
    }
    
    public int getOrientations() {
        return orientations;
    }
    
    public void setOrientations(String orientationsText) {
        this.orientations = Integer.parseInt( orientationsText);
    }

    public void setDmatFilename(String dmatFilename) {
        this.dmatFilename = dmatFilename;
    }

    private String collection;
    private int scales;
    private int orientations;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
    private transient boolean saveMatrix, saveDmat;
    private transient String matrixFilename, dmatFilename;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
        
}
