
package featureextraction.bicextractor;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.Image;
import javax.swing.ImageIcon;
import java.awt.image.BufferedImage;


/**
 * @author Laura Florian
 */
public class Imagen {

    Image imagen;
    String path;
    int rowNumber;
    int columnNumber;

   public Imagen (String ruta)
    {
        this.path= ruta;
        this.imagen = new ImageIcon(path).getImage();
        this.rowNumber = this.imagen.getHeight(null);
        this.columnNumber = this.imagen.getWidth(null);
    }

   public Imagen (Image imag)
    {
        this.path= null;
        this.imagen = imag;
        this.rowNumber = imag.getHeight(null);
        this.columnNumber = imag.getWidth(null);
    }

    public Image getImage(String s)
    {
        Toolkit tool = Toolkit.getDefaultToolkit();
        Image image = tool.getImage(s);
        return image;
    }
      
   public  int[][] getPixels(Image image) throws IOException {
       // get a image of 64 colors
        int w = image.getWidth(null);
        int h = image.getHeight(null);
        int pix[] = new int[w * h];
        PixelGrabber grabber = new PixelGrabber(image, 0, 0, w, h, pix, 0, w);

        try {
            if (grabber.grabPixels() != true) {
                throw new IOException("Grabber returned false: " + grabber.status());
            }
        } catch (InterruptedException e) {  e.printStackTrace();   }

        int pixels[][] = new int[w][h];
        for (int x = w; x-- > 0; ) {
            for (int y = h; y-- > 0; ) {
                pixels[x][y] = pix[y * w + x];
            }
        }
        return pixels;
    }

   public  int[][] quantizeImg(int num)  throws IOException{

            int pixels[][] = getPixels(this.imagen);
            int palette[] = Quantize.quantizeImage(pixels, num);
            int matrix[][] = pixels;

            return matrix;            
    }

   public int [][] getImageMoreColumnRow(int [] [] x )
    {
       // method to duplicate a row up and down and a right column, left
        int nf = x.length;
        int nc = x[0].length;
        int [][] y;
        int i,j;
        
        y= new int[nf+2][nc+2];

        for (i=0; i<nf; i++)
        {
            for (j=0; j<nc; j++)
            {
                y[i+1][j+1]= x[i][j];
            }
        }
        
        for (j=0; j<nc ; j++)
        {
            y[0][j] = x[0][j];
            y[nf+1][j] = x[nf-1][j];
        }
        
        for (i=0; i<nf ; i++)
        {
            y[i][0] = x[i][0];
            y[i][nc+1] = x[i][nc-1];
        }

        return y;
    }

   public int [] getHistogBorder(int [] [] x )
    {
       //method to find edge
        int nf = x.length;
        int nc = x[0].length;
        int [][] y =  getImageMoreColumnRow( x );
        int i,j;
        int nff= nf+2;
        int ncc= nc+2;       
        int threshold=40;
        int numbersColors=256;
        int [] h= new int[numbersColors];
        int img1, v1,v2,v3,v4;
        
        for( i=0;i < numbersColors;i++){
                h[i] = 0;
        }

        for (i=1; i<nff-1 ; i++) {
            for (j=1; j<ncc-1 ; j++){
                img1= y[i][j];
                v1= y[i-1][j];
                v2= y[i][j-1];
                v3= y[i][j+1];
                v4= y[i+1][j];
                if(((img1- v1) >= threshold) | ( (img1- v2) >= threshold) | ( ((img1)- v3) >= threshold) | ( (img1- v4) >= threshold))
                {
                    h[img1+1]= h[img1+1]+1;                   
                }                
            }
         }
            int maxValue=1;
            for( i=0;i < numbersColors;i++){
                if(h[i] > maxValue){
                  maxValue = h[i];
                }
            }
            for( i=0;i < numbersColors;i++){
                h[i] = h[i] *numbersColors / maxValue;
            }

        return h;
       
    }

  public  int [] getHistogInterior(int [] [] x )
    {
        //Internal method to find
        int nf = x.length;
        int nc = x[0].length;
        int [][] y =  getImageMoreColumnRow( x );
        int i,j;
        int nff= nf+2;
        int ncc= nc+2;
        int threshold=4;
        int numbersColors=256;
        int [] h= new int[numbersColors];
        int img1, v1,v2,v3,v4;

        for( i=0;i < numbersColors;i++){
                h[i] = 0;
        }

        for (i=1; i<nff-1 ; i++) {
            for (j=1; j<ncc-1 ; j++){
                img1= y[i][j];
                v1= y[i-1][j];
                v2= y[i][j-1];
                v3= y[i][j+1];
                v4= y[i+1][j];

                if(((img1- v1) <= threshold)&& ( (img1- v2)<= threshold) && ( ((img1)- v3) <= threshold) && ( (img1- v4) <= threshold))
                {
                    h[img1+1]= h[img1+1]+1;                  
                }                
            }
         }
            int maxValue=1;
            for( i=0;i < numbersColors;i++){
                if(h[i] > maxValue){
                  maxValue = h[i];
                }
            }
            for( i=0;i < numbersColors;i++){
                h[i] = h[i] *numbersColors / maxValue;
            }

        return h;
    }

}