/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package featureextraction.bicextractor;

import distance.DistanceMatrix;
import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import matrix.AbstractMatrix;
import projection.util.ProjectionConstants;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.util.PropertiesManager;

/**
 *
 * @author Laura Florian
 */
@VisComponent(hierarchy = "Feature Extraction",
name = "BIC Feature Extractor",
description = "extracting features from an image collection.")
public class BicComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {  

        BicExtractor pe = new BicExtractor();
        matrix = pe.extract(PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME), collection, colors);
        
        //saving the matrix
        if (matrix != null && saveMatrix && matrixFilename != null && !matrixFilename.isEmpty()) {
            //saving the points matrix
            System.out.println("Saving points matrix...");
            matrix.save(matrixFilename);
        }

        dmat = new DistanceMatrix(matrix,DissimilarityFactory.getInstance(disstype));

        if (dmat != null && saveDmat && dmatFilename != null && !dmatFilename.isEmpty()) {
            //saving distance matrix
            System.out.println("Saving distance matrix...");
            dmat.save(dmatFilename);
        }

    }
    
    public void input(String col) {
        collection = col;
    }
    
    public AbstractMatrix output() {
        return matrix;
    }

    public DistanceMatrix outputDmat() {
        return dmat;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new BicParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        //matrixFilename = dmatFilename = collection = "";
        matrix = null;
        dmat = null;
        //saveMatrix = saveDmat = false;
        //disstype = DissimilarityFactory.getInstance(DissimilarityType.EUCLIDEAN);
    }
    
    public static final long serialVersionUID = 1L;
    private transient BicParamView paramview;

    public String getCollection() {
        return collection;
    }

    public void setCollection(String c) {
        collection = c;
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public boolean isSaveMatrix() {
        return saveMatrix;
    }

    public void setSaveMatrix(boolean saveMatrix) {
        this.saveMatrix = saveMatrix;
    }

    public boolean isSaveDmat() {
        return saveDmat;
    }

    public void setSaveDmat(boolean saveDmat) {
        this.saveDmat = saveDmat;
    }

    public String getMatrixFilename() {
        return matrixFilename;
    }

    public void setMatrixFilename(String matrixFilename) {
        this.matrixFilename = matrixFilename;
    }

    public String getDmatFilename() {
        return dmatFilename;
    }

    public void setDmatFilename(String dmatFilename) {
        this.dmatFilename = dmatFilename;
    }


    public int getColors() {
        return colors;
    }

    public void setColors(String colorsText) {
        this.colors = Integer.parseInt( colorsText);
    }


    private String collection;
    private int colors;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
    private transient boolean saveMatrix, saveDmat;
    private transient String matrixFilename, dmatFilename;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
        
}
