package featureextraction.bicextractor;

import java.io.*;
import javax.imageio.ImageIO;
import java.io.File;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import java.awt.image.BufferedImage;
import java.awt.Image;
import javax.swing.ImageIcon;
import java.util.List;
import visualizationbasics.util.Util;

/**
 * @author Laura Florian
 */
public class Bic {

    String path;
    int numP;
    private float matrixPoints[][];
    private ArrayList<Integer> imagesIds;
    private ArrayList<String> nameImages;
    private float[] cdata;

   public Bic(String pathF, int num)
    {
        this.path=pathF;
        this.numP=num;
        loadBic();
    }

   public float[][] getMatrixPoints() {
        return matrixPoints;
    }

   public ArrayList<String> getNameImages() {
        return nameImages;
    }

    public ArrayList<Integer> getImagesIDs() {
        return imagesIds;
    }
    public float[] getClassData() {
        return this.cdata;
    }

     /** Pega o nome das imagens e armazena em um vetor de strings */
    private ArrayList<String> getNameImages(Vector<File> filesTrainer) {
        ArrayList<String> nameImages = new ArrayList<String>();
        for (int i = 0; i < filesTrainer.size(); i++) {
            nameImages.add(filesTrainer.get(i).getName());
        }
        return nameImages;
    }

    private ArrayList<Integer> getImagesIDs(Vector<File> filesTrainer) {
        ArrayList<Integer> ids = new ArrayList<Integer>();

        for (int i = 0; i < filesTrainer.size(); i++) {
            int id = Util.convertToInt(filesTrainer.get(i).getName());
            ids.add(id);
        }
        return ids;
    }

   public void loadBic(){
       String originalPath="", patho="";
       int i, j, length;
       int [] vector;
       int [] hb;
       int [] hi;
       int[][] matrix;
       int num= this.numP;
       File file = new File (this.path);
       String[] listaArchivos=file.list();
       originalPath= file.getAbsolutePath();
       length= listaArchivos.length;
       Vector<File> filesTrainer = new Vector<File>();
       for (File f: new File(this.path).listFiles()){
           if (f.isFile()){
                filesTrainer.add(f);
            }
       }
       nameImages = getNameImages(filesTrainer);
       imagesIds = getImagesIDs(filesTrainer);
       createCdata(filesTrainer);
       List a = new ArrayList<String>();
       for( i=0; i<length; i++){
           patho= path + File.separator + listaArchivos[i];
           Image img = new ImageIcon(patho).getImage();
           if ( img.getWidth(null) >1){
               a.add(listaArchivos[i]);
           }
       }
       String[] imagesList= new String [a.size()];
       for( i=0; i<a.size(); i++){
           imagesList[i]=(String) a.get(i);
       }
       length= imagesList.length;
       matrixPoints = new float [length] [num*2];
       for( i=0; i<length; i++){
           patho= originalPath + File.separator + imagesList[i];
           System.out.println(i + ": " + patho);
           Imagen imag = new Imagen(patho);
           try {
               matrix= imag.quantizeImg(num);
               hb= imag.getHistogBorder(matrix);
               hi= imag.getHistogInterior(matrix);
               vector = new int [num*2];
               for(j=0;j<num;j++){
                   vector[j]= hb[j];
                   vector[j+num] = hi[j];
               }
               for (j=0; j< num*2 ; j++) {
                   matrixPoints[i][j]= (float) vector[j];
               }
               } catch (IOException ex) {
                   System.out.println("Error: generar BIC" + ex.getMessage());
               }
       }                    
  }
    
    private void createCdata(Vector<File> filesTrainer) {
        if (filesTrainer.size() > 0) {
            //Capturing the initials
            ArrayList<String> initials_aux = new ArrayList<String>();
            for (int i = 0; i < filesTrainer.size(); i++) {
                    String filename = filesTrainer.get(i).getName();
                    int begin = filename.lastIndexOf("_");
                    String ini;
                    if (begin > -1) {
                        filename = filename.substring(0, begin);
                        ini = filename;
                    } else {
                        ini = filename;
                        if (filename.length() > 2) {
                            ini = filename.substring(0, 2);
                        }
                    }
                    //Create the initials with two letters
                    if (!initials_aux.contains(ini)) {
                        initials_aux.add(ini);
                    }
            }

            String[] initials = new String[initials_aux.size()];
            for (int i = 0; i < initials_aux.size(); i++) {
                initials[i] = initials_aux.get(i);
            }

            Arrays.sort(initials);

            //Creating the cdata
            if (initials.length > 1) {
                this.cdata = new float[filesTrainer.size()];
                for (int i = 0; i < filesTrainer.size(); i++) {
                    this.cdata[i] = -1;
                    for (int j = 0; j < initials.length; j++) {

                        //Taking out the part of the file that correspond to the directory
                        String filename = filesTrainer.get(i).getName();
                        //int begin=filename.lastIndexOf(System.getProperty("file.separator"));
                        int begin = filename.lastIndexOf("_");
                        if (begin > -1) {
                            filename = filename.substring(0, begin);
                        }

                        //Given the cdata number
                        if (filename.startsWith(initials[j])) {
                            this.cdata[i] = j;
                        }
                    }
                }
            } else {
                this.cdata = new float[filesTrainer.size()];
                Arrays.fill(this.cdata, 0.0f);
            }
        }
    }

}