//Extracted from http://code.google.com/p/javasurf/
package featureextraction.surfextractor;

import featureextraction.normalization.NoneNormalization;
import featureextraction.normalization.Normalization;
import featureextraction.surfextractor.base.IDescriptor;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import featureextraction.surfextractor.base.IDetector;
import featureextraction.surfextractor.base.ISURFfactory;
import featureextraction.surfextractor.base.InterestPoint;
import featureextraction.surfextractor.base.SURF;
import featureextraction.util.FeatureExtractionUtil;
import featureextraction.util.JPG2PGMConverter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Arrays;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.Util;

/**
 *
 * @author mite
 */
public class SurfExtractor {

    private float threshold = 3500;
    private float balanceValue = (float) 0.9;
    private int octaves = 5;

    public SurfExtractor(float threshold, float balance, int octaves) {
        this.threshold = threshold;
        this.balanceValue = balance;
        this.octaves = octaves;
    }

    public ArrayList<File> getRealFiles(PropertiesManager pm, String collection) {
        ArrayList<File> realFiles = null;
        pm.setProperty("UNZIP.DIR", System.getProperty("user.dir") + "\\tempDir");
        pm.setProperty("FEATURE.DIR", System.getProperty("user.dir") + "\\lib\\featureSelection");
        String unzipDirName = pm.getProperty("UNZIP.DIR");
        FeatureExtractionUtil.deleteFiles(unzipDirName);
        FeatureExtractionUtil.unzipFiles(pm,collection);
        File unzipDir = new File(unzipDirName);
        if (unzipDir != null && unzipDir.isDirectory()) {
            File[] imageFiles = unzipDir.listFiles();
            if (imageFiles != null) {
                realFiles = new ArrayList<File>();
                for (File f: imageFiles){
                    if (f.isFile()){
                        realFiles.add(f);
                    }
                }
            }
        }
        return realFiles;
    }

    public AbstractMatrix extract(PropertiesManager pm, String collection) {
        AbstractMatrix matrix = new DenseMatrix();
        ArrayList<File> realFiles = getRealFiles(pm,collection);
        if (realFiles != null) {
            ArrayList<String> nameImages = getNameImages(realFiles);
            ArrayList<Integer> imagesIds = getImagesIDs(realFiles);
            float[] cdata = createCdata(realFiles);

            for (int i=0;i<realFiles.size();i++) {
                File image = realFiles.get(i);
                try {
                    BufferedImage img = ImageIO.read(image);
                    ISURFfactory mySURF = SURF.createInstance(img, balanceValue, threshold, octaves, img);
                    IDetector detector = mySURF.createDetector();
                    ArrayList interest_points = detector.generateInterestPoints();
                    IDescriptor descriptor = mySURF.createDescriptor(interest_points);
                    descriptor.generateAllDescriptors();
                    float[] valuesImage = getAverageDescriptorsInterestPoints(interest_points);
                    if (valuesImage != null && valuesImage.length > 0)
                        matrix.addRow(new DenseVector(valuesImage,imagesIds.get(i),cdata[i]),nameImages.get(i));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            //normalization
            try {
                Normalization norm = new NoneNormalization();
                matrix = norm.execute(matrix);
            } catch (IOException ex) {
                Logger.getLogger(SurfExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }
            return matrix;
        }
        return null;
    }

    public static AbstractMatrix extractSingle(String file) {

        AbstractMatrix matrix = new DenseMatrix();
        try {
            File image = new File(file);
            BufferedImage img = ImageIO.read(image);
            ISURFfactory mySURF = SURF.createInstance(img,1,1,1,img);
            IDetector detector = mySURF.createDetector();
            ArrayList interest_points = detector.generateInterestPoints();
            IDescriptor descriptor = mySURF.createDescriptor(interest_points);
            descriptor.generateAllDescriptors();
            SurfExtractor sf = new SurfExtractor(1,1,1);
            matrix = sf.getDescriptorsMatrix(interest_points);
            //return matrix;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //normalization
        try {
            Normalization norm = new NoneNormalization();
            matrix = norm.execute(matrix);
        } catch (IOException ex) {
            Logger.getLogger(SurfExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return matrix;
    }

    public static AbstractMatrix extractSingle(BufferedImage img) {

        AbstractMatrix matrix = new DenseMatrix();
        try {
            ISURFfactory mySURF = SURF.createInstance(img,1,1,1,img);
            IDetector detector = mySURF.createDetector();
            ArrayList interest_points = detector.generateInterestPoints();
            IDescriptor descriptor = mySURF.createDescriptor(interest_points);
            descriptor.generateAllDescriptors();
            SurfExtractor sf = new SurfExtractor(1,1,1);
            matrix = sf.getDescriptorsMatrix(interest_points);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //normalization
        try {
            Normalization norm = new NoneNormalization();
            matrix = norm.execute(matrix);
        } catch (IOException ex) {
            Logger.getLogger(SurfExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return matrix;
    }

    private float[] getAverageDescriptorsInterestPoints(ArrayList interest_points) {

        System.out.println("Generating Average of Descriptors...");
        float[] values = null;
        for (int i=0;i<interest_points.size();i++) {
            float[] valuesInterestPoint = ((InterestPoint)interest_points.get(i)).getDescriptorOfTheInterestPoint();
            if (values == null) values = new float[valuesInterestPoint.length];
            for (int j=0;j<valuesInterestPoint.length;j++)
                if (!Float.isNaN(valuesInterestPoint[j]))
                    values[j] += valuesInterestPoint[j];
        }
        if (values != null) {
            for (int i=0;i<values.length;i++)
                values[i] /= interest_points.size();
        }
        return values;

    }

    public ArrayList<AbstractMatrix> extractMatrices(PropertiesManager pm, String collection) {
        ArrayList<AbstractMatrix> matrices = new ArrayList<AbstractMatrix>();
        ArrayList<File> realFiles = getRealFiles(pm,collection);
        if (realFiles != null) {
            for (int i=0;i<realFiles.size();i++) {
                File image = realFiles.get(i);
                try {
                    BufferedImage img = ImageIO.read(image);
                    ISURFfactory mySURF = SURF.createInstance(img, balanceValue, threshold, octaves, img);
                    IDetector detector = mySURF.createDetector();
                    ArrayList interest_points = detector.generateInterestPoints();
                    IDescriptor descriptor = mySURF.createDescriptor(interest_points);
                    descriptor.generateAllDescriptors();
                    AbstractMatrix valuesImage = getDescriptorsMatrix(interest_points);
                    if (valuesImage != null)
                        matrices.add(valuesImage);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            return matrices;
        }
        return null;
    }

    private AbstractMatrix getDescriptorsMatrix(ArrayList interest_points) {

        System.out.println("Generating Descriptors Matrix...");
        AbstractMatrix matrix = null;
        ArrayList<String> labels = new ArrayList<String>();
        for (int i=0;i<interest_points.size();i++) {
            if (matrix == null) matrix = new DenseMatrix();
            float[] valuesInterestPoint = ((InterestPoint)interest_points.get(i)).getDescriptorOfTheInterestPoint();
            for (int j=0;j<valuesInterestPoint.length;j++)
                if (Float.isNaN(valuesInterestPoint[j]))
                    valuesInterestPoint[j] = 0.0f;
            AbstractVector v = new DenseVector(valuesInterestPoint);
            matrix.addRow(v);
            labels.add(Integer.toString(i));
        }
        matrix.setLabels(labels);
        return matrix;
        
    }

    private float[] createCdata(ArrayList<File> filesTrainer) {
        float[] cdataL = null;
        if (filesTrainer != null && !filesTrainer.isEmpty()) {
            //Capturing the initials
            ArrayList<String> initials_aux = new ArrayList<String>();
            for (int i=0;i<filesTrainer.size();i++) {
                    String filename = filesTrainer.get(i).getName();
                    int begin = filename.lastIndexOf("_");
                    String ini;
                    if (begin > -1) {
                        filename = filename.substring(0, begin);
                        ini = filename;
                    } else {
                        ini = filename;
                        if (filename.length() > 2)
                            ini = filename.substring(0, 2);
                    }
                    //Create the initials with two letters
                    if (!initials_aux.contains(ini))
                        initials_aux.add(ini);
            }
            String[] initials = new String[initials_aux.size()];
            for (int i=0;i<initials_aux.size();i++)
                initials[i] = initials_aux.get(i);
            
            Arrays.sort(initials);

            //Creating the cdata
            if (initials.length > 1) {
                cdataL = new float[filesTrainer.size()];
                for (int i = 0; i < filesTrainer.size(); i++) {
                    cdataL[i] = -1;
                    for (int j = 0; j < initials.length; j++) {
                        //Taking out the part of the file that correspond to the directory
                        String filename = filesTrainer.get(i).getName();
                        //int begin=filename.lastIndexOf(System.getProperty("file.separator"));
                        int begin = filename.lastIndexOf("_");
                        if (begin > -1)
                            filename = filename.substring(0, begin);
                        //Given the cdata number
                        if (filename.startsWith(initials[j]))
                            cdataL[i] = j;
                    }
                }
            } else {
                cdataL = new float[filesTrainer.size()];
                Arrays.fill(cdataL, 0.0f);
            }
        }
        return cdataL;
    }

    private ArrayList<String> getNameImages(ArrayList<File> files) {
        ArrayList<String> nameImagesL = new ArrayList<String>();
        for (int i = 0; i < files.size(); i++)
            nameImagesL.add(files.get(i).getName());
        return nameImagesL;
    }

    private ArrayList<Integer> getImagesIDs(ArrayList<File> files) {
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for (int i = 0; i < files.size(); i++) {
            int id = Util.convertToInt(files.get(i).getName());
            ids.add(id);
        }
        return ids;
    }

    public static void main(String a[]) {
        try {
            AbstractMatrix m = extractSingle("E:\\i01_1.jpg");
            m.save("E:\\i01_1.data");

            BufferedImage im = JPG2PGMConverter.convert("E:\\i01_1.jpg",true);

            m = extractSingle(im);
            m.save("E:\\i01_1_2.data");

        } catch (IOException ex) {
            Logger.getLogger(SurfExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
