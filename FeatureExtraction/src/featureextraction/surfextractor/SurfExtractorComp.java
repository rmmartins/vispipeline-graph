/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package featureextraction.surfextractor;

import distance.DistanceMatrix;
import distance.dissimilarity.DissimilarityFactory;
import distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import java.io.IOException;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import projection.util.ProjectionConstants;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import visualizationbasics.util.PropertiesManager;

/**
 *
 * @author Jose Gustavo
 */
@VisComponent(hierarchy = "Feature Extraction",
name = "SURF Feature Extractor",
description = "extract the mean of 64 features from interest points in images of a collection.")
public class SurfExtractorComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {

        SurfExtractor se = new SurfExtractor(threshold, balanceValue, octaves);
        matrix = se.extract(PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME), collection);
        dmat = new DistanceMatrix(matrix,DissimilarityFactory.getInstance(disstype));

        if (saveAverageMatrix) {
            //saving the matrix
            if (matrix != null && saveMatrix && matrixFilename != null && !matrixFilename.isEmpty()) {
                //saving the points matrix
                System.out.println("Saving points matrix...");
                matrix.save(matrixFilename);
            }
            if (dmat != null && saveDmat && dmatFilename != null && !dmatFilename.isEmpty()) {
                //saving distance matrix
                System.out.println("Saving distance matrix...");
                dmat.save(dmatFilename);
            }
        }

        if (saveIndividualMatrices) {
            if (matricesDir != null && !matricesDir.isEmpty()) {
                ArrayList<AbstractMatrix> matrices = se.extractMatrices(PropertiesManager.getInstance(ProjectionConstants.PROPFILENAME), collection);
                for (int i=0;i<matrices.size();i++)
                    matrices.get(i).save(matricesDir+"\\Matrix_"+i+".data");
            }
        }

    }
    
    public AbstractMatrix output() {
        return matrix;
    }

    public DistanceMatrix outputDmat() {
        return dmat;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new SurfExtractorParamView(this);
        }
        return paramview;
    }

    @Override
    public void reset() {
        matrix = null;
        dmat = null;
    }
    
    public static final long serialVersionUID = 1L;
    private transient SurfExtractorParamView paramview;

    public String getCollection() {
        return collection;
    }

    public void setCollection(String c) {
        collection = c;
    }

    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public boolean isSaveMatrix() {
        return saveMatrix;
    }

    public void setSaveMatrix(boolean saveMatrix) {
        this.saveMatrix = saveMatrix;
    }

    public boolean isSaveDmat() {
        return saveDmat;
    }

    public void setSaveDmat(boolean saveDmat) {
        this.saveDmat = saveDmat;
    }

    public String getMatrixFilename() {
        return matrixFilename;
    }

    public void setMatrixFilename(String matrixFilename) {
        this.matrixFilename = matrixFilename;
    }

    public String getDmatFilename() {
        return dmatFilename;
    }

    public void setDmatFilename(String dmatFilename) {
        this.dmatFilename = dmatFilename;
    }

    public float getThreshold() {
        return threshold;
    }

    public void setThreshold(float threshold) {
        this.threshold = threshold;
    }

    public float getBalanceValue() {
        return balanceValue;
    }

    public void setBalanceValue(float balanceValue) {
        this.balanceValue = balanceValue;
    }

    public int getOctaves() {
        return octaves;
    }

    public void setOctaves(int octaves) {
        this.octaves = octaves;
    }

    public String getMatricesDir() {
        return matricesDir;
    }

    public void setMatricesDir(String matricesDir) {
        this.matricesDir = matricesDir;
    }

    public boolean isSaveAverageMatrix() {
        return saveAverageMatrix;
    }

    public void setSaveAverageMatrix(boolean saveAverageMatrix) {
        this.saveAverageMatrix = saveAverageMatrix;
    }

    public boolean isSaveIndividualMatrices() {
        return saveIndividualMatrices;
    }

    public void setSaveIndividualMatrices(boolean saveIndividualMatrices) {
        this.saveIndividualMatrices = saveIndividualMatrices;
    }

    private String collection;
    private transient AbstractMatrix matrix;
    private transient DistanceMatrix dmat;
    private transient boolean saveMatrix, saveDmat;
    private transient String matrixFilename, dmatFilename;
    private float threshold = 800;
    private float balanceValue = 0.9f;
    private int octaves = 5;
    private String matricesDir;
    private boolean saveAverageMatrix,saveIndividualMatrices;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
        
}
