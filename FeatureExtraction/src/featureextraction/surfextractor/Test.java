//Extracted from http://code.google.com/p/javasurf/
package featureextraction.surfextractor;

import featureextraction.surfextractor.base.IDescriptor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import featureextraction.surfextractor.base.IDetector;
import featureextraction.surfextractor.base.ISURFfactory;
import featureextraction.surfextractor.base.InterestPoint;
import featureextraction.surfextractor.base.SURF;
import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 *
 * @author mite
 */
public class Test {

    static ArrayList interest_points;
    static float threshold = 3500;
    static float balanceValue = (float) 0.9;
    static int octaves = 5;

    public static void main(String argv[]) {

        BufferedImage img = null;
        try {
            File file = new File("E:\\i01_1.jpg");
            img = ImageIO.read(file);
            ISURFfactory mySURF = SURF.createInstance(img, balanceValue, threshold, octaves, img);
            IDetector detector = mySURF.createDetector();
            interest_points = detector.generateInterestPoints();
            IDescriptor descriptor = mySURF.createDescriptor(interest_points);
            descriptor.generateAllDescriptors();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //drawInterestPoints();
        //drawDescriptors();
        File out =new File("E:\\i01_1_out.jpg");
        try {
            ImageIO.write(img, "PNG", out);
        } catch (IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        saveDescriptorsInterestPoints("E:\\i01_1_keys.txt");

//        JFrame frame = new JFrame();
//        frame.setDefaultCloseOperation(1);
//        JLabel label = new JLabel(new ImageIcon(img));
//        frame.getContentPane().add(label, BorderLayout.CENTER);
//        frame.pack();
//        frame.setVisible(true);

    }

    static void drawInterestPoints() {

        System.out.println("Drawing Interest Points...");

        for (int i = 0; i < interest_points.size(); i++) {

            InterestPoint IP = (InterestPoint) interest_points.get(i);
            IP.drawPosition(5,new Color(200,200,200));

        }

    }

    static void drawDescriptors() {

        System.out.println("Drawing Descriptors...");

        for (int i = 0; i < interest_points.size(); i++) {

            InterestPoint IP = (InterestPoint) interest_points.get(i);
            IP.drawDescriptor(new Color(255,0,0));

        }

    }

    static void saveDescriptorsInterestPoints(String file) {

        System.out.println("Saving Descriptors...");
        String text = interest_points.size()+" "+((InterestPoint)interest_points.get(0)).getDescriptorOfTheInterestPoint().length+" \n";
        for (int i=0;i<interest_points.size();i++) {
            InterestPoint IP = (InterestPoint) interest_points.get(i);
            text += IP.toString()+"\n";
        }

        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(file)));
            bw.write(text);
            bw.flush();
            bw.close();
        } catch(IOException e){
            e.printStackTrace();
        }

    }
}
