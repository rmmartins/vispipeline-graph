package featureextraction.util;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;

import com.sun.media.jai.codec.PNMEncodeParam;
import com.sun.media.jai.codec.ImageEncoder;
import com.sun.media.jai.codec.ImageCodec;
import java.awt.image.RenderedImage;
import javax.media.jai.JAI;
import javax.media.jai.RenderedImageAdapter;

public class JPG2PGMConverter {

    public static BufferedImage convert(String in, boolean raw) {

        BufferedImage bufferedImage = null;
        try{
            File inputFile = new File(in);
            String tempPath = "E:\\temp.pgm";
            Image image = ImageIO.read(inputFile);
            bufferedImage= new BufferedImage(image.getWidth(null),image.getHeight(null),BufferedImage.TYPE_BYTE_GRAY);
            bufferedImage.createGraphics().drawImage(image,0,0,null);
            FileOutputStream fileOutputStream = new FileOutputStream(tempPath);
            PNMEncodeParam param = new PNMEncodeParam();
            param.setRaw(raw);
            ImageEncoder encoder = ImageCodec.createImageEncoder("PNM",fileOutputStream,param);
            encoder.encode(bufferedImage);
            fileOutputStream.close();
            RenderedImage rendimg=JAI.create("fileload",tempPath);
            bufferedImage = new RenderedImageAdapter(rendimg).getAsBufferedImage();
        }catch(Exception e) {
            e.printStackTrace();
        }
        return bufferedImage;

    }

    public static void convert(String in, String out, boolean raw) {

        try{
            File inputFile = new File(in);
            Image image = ImageIO.read(inputFile);
            BufferedImage bufferedImage= new BufferedImage(image.getWidth(null),image.getHeight(null),BufferedImage.TYPE_BYTE_GRAY);
            bufferedImage.createGraphics().drawImage(image,0,0,null);
            FileOutputStream fileOutputStream = new FileOutputStream(out);
            PNMEncodeParam param = new PNMEncodeParam();
            param.setRaw(raw);
            ImageEncoder encoder = ImageCodec.createImageEncoder("PNM",fileOutputStream,param);
            encoder.encode(bufferedImage);
            fileOutputStream.close();
        }catch(Exception e) {
            e.printStackTrace();
        }
        
    }

    public static void main(String args[]) {

        BufferedImage img =  convert("E:\\i01_1.jpg",true);
        int x = 1;
        
//        try{
//            String inputFilePath = "E:\\i01_1.jpg";
//            String outputFilePath = "E:\\i01_1.pgm";
//
//            File inputFile = new File(inputFilePath);
//
//            Image image = ImageIO.read(inputFile);//.getScaledInstance(80,80,Image.SCALE_SMOOTH);
//            //BufferedImage bufferedImage= new BufferedImage(80,80,BufferedImage.TYPE_BYTE_GRAY);
//            BufferedImage bufferedImage= new BufferedImage(image.getWidth(null),image.getHeight(null),BufferedImage.TYPE_BYTE_GRAY);
//
//            bufferedImage.createGraphics().drawImage(image,0,0,null);
//
//            FileOutputStream fileOutputStream = new FileOutputStream(outputFilePath);
//
//            PNMEncodeParam param = new PNMEncodeParam();
//            param.setRaw(true);
//            //param.setRaw(false);
//
//            ImageEncoder encoder = ImageCodec.createImageEncoder("PNM",fileOutputStream,param);
//
//            encoder.encode(bufferedImage);
//
//            fileOutputStream.close();
//
//        }catch(Exception e) {
//                    e.printStackTrace();
//        }
    }
}

