package featureextraction.util;

import java.awt.Component;
import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import javax.swing.JFileChooser;
import visualizationbasics.model.AbstractInstance;
import visualizationbasics.model.AbstractModel;
import visualizationbasics.util.OpenDialog;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.filter.AbstractFilter;


/**
 *
 * @author Jose Gustavo de Souza Paiva
 */
public class FeatureExtractionOpenDialog extends OpenDialog {

    public static int showDirSaveDialog(Component parent, String directory) {

        dialog = new JFileChooser();
        int oldSelectionMode = dialog.getFileSelectionMode();
        String oldFilename = _filename;
        dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        dialog.setDialogTitle("Directory to save");
        dialog.setCurrentDirectory(new File(directory));
        int result = dialog.showOpenDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            _filename = dialog.getSelectedFile().getAbsolutePath();
        }
        dialog.setFileSelectionMode(oldSelectionMode);
        return result;
    }

    public static String getFilename() {
        return _filename;
    }

}
