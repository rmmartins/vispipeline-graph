/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package featureextraction.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.Util;

/**
 *
 * @author Jose Gustavo
 */
public class FeatureExtractionUtil extends Util {

    public static void deleteFiles(String directory) {
        File dir = new File(directory);
        if (dir == null) return;
        String[] files = dir.list();
        if (files == null) return;
        if (files.length > 0) {
            for (int i = 0; i < files.length; i++) {
                File f = new File(directory + "\\" + files[i]);
                f.delete();
            }
        }
    }

    public static void unzipFiles(PropertiesManager pm, String filename) {
        try {
            String unzipDir = pm.getProperty("UNZIP.DIR");
            ZipFile zf = new ZipFile(filename);
            Enumeration zipEnum = zf.entries();
            while (zipEnum.hasMoreElements()) {
                ZipEntry item = (ZipEntry) zipEnum.nextElement();

                String newfile = item.getName();
                InputStream is = zf.getInputStream(item);
                FileOutputStream fos = new FileOutputStream(unzipDir + "\\" + newfile);
                int ch;
                while ((ch = is.read()) != -1) {
                    fos.write(ch);
                }
                is.close();
                fos.close();
            }
            zf.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static void unzipSpecificFiles(PropertiesManager pm, String filename, ArrayList<String> names, boolean considerExtension) {
        try {
            String unzipDir = pm.getProperty("UNZIP.DIR");
            ZipFile zf = new ZipFile(filename);
            Enumeration zipEnum = zf.entries();
            while (zipEnum.hasMoreElements()) {
                ZipEntry item = (ZipEntry) zipEnum.nextElement();
                String newfile = item.getName();
                boolean exists = !considerExtension ? names.contains(newfile) : containsNoExtension(names,newfile);
                if (exists) {
                    InputStream is = zf.getInputStream(item);
                    FileOutputStream fos = new FileOutputStream(unzipDir + "\\" + newfile);
                    int ch;
                    while ((ch = is.read()) != -1) {
                        fos.write(ch);
                    }
                    is.close();
                    fos.close();
                }
            }
            zf.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static boolean containsNoExtension(ArrayList<String> names, String file) {
        String fileNoExtension = file.lastIndexOf(".") == -1 ? file : file.substring(0,file.lastIndexOf("."));
        for (int i=0;i<names.size();i++) {
            if (names.get(i).startsWith(fileNoExtension))
                return true;
            //String fileNoExtension2 = names.get(i).lastIndexOf(".") == -1 ? names.get(i) : names.get(i).substring(0,names.get(i).lastIndexOf("."));
        }
        return false;
    }

}
