/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package featureextraction.pexextractor;

import featureextraction.normalization.NoneNormalization;
import featureextraction.normalization.Normalization;
import featureextraction.util.FeatureExtractionUtil;
import java.io.IOException;
import java.util.ArrayList;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import matrix.dense.DenseVector;
import visualizationbasics.util.PropertiesManager;
import visualizationbasics.util.Util;

/**
 *
 * @author Jose Gustavo
 */
public class PexExtractor {

    public AbstractMatrix extract(PropertiesManager pm, String collection) throws IOException {

        AbstractMatrix matrix = new DenseMatrix();
        pm.setProperty("UNZIP.DIR", System.getProperty("user.dir") + "\\tempDir");
        pm.setProperty("FEATURE.DIR", System.getProperty("user.dir") + "\\lib\\featureSelection");
        String unzipDir = pm.getProperty("UNZIP.DIR");
        FeatureExtractionUtil.deleteFiles(unzipDir);
        //Colocar msg de unzipping images...
        FeatureExtractionUtil.unzipFiles(pm,collection);

        ExtractionManager EM = new ExtractionManager(unzipDir);
        //Colocar msg de extracting features...
        float points[][] = EM.getMatrixPoints();
        float[] cdata = EM.getClassData();
        ArrayList<Integer> ids = EM.getImagesIDs();
        ArrayList<String> labels = EM.getNameImages();

        //Construir a DenseMatrix aqui
        for (int i=0;i<points.length;i++) {
           matrix.addRow(new DenseVector(points[i], ids.get(i), cdata[i]),labels.get(i));
        }

        //normalization
        Normalization norm = new NoneNormalization();
        matrix = norm.execute(matrix);
        return matrix;
    }

}
