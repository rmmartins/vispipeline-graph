/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package errors;


import distance.DistanceMatrix;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;
import projection.model.Scalar;
import projection.util.ProjectionConstants;
import projection.util.ProjectionUtil;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;


/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Errors",
name = "Aggregated Distance Error",
description = "Create a scalar with the aggregated distance-based error.")
public class AggregatedDistanceErrorComp implements AbstractComponent {

    private String filename;
    private transient AbstractMatrix projection, originalData;
            
    @Override
    public void execute() throws IOException {
        Euclidean euclid = new Euclidean();
        DistanceMatrix projDmat = new DistanceMatrix(projection, euclid);
        DistanceMatrix origDmat = new DistanceMatrix(originalData, euclid);
        
        Double[] errors = computeAggregatedDistanceError(origDmat, projDmat, true);
        
        ProjectionModel model = new ProjectionModel();
        Scalar errorScalar = model.addScalar("Aggregated Distance-based Error");

        int nrows = projection.getRowCount();

        for (int i = 0; i < nrows; i++) {
            AbstractVector row = projection.getRow(i);            
            ProjectionInstance pi = new ProjectionInstance(row.getId(),
                    row.getValue(0), row.getValue(1));
            model.addInstance(pi);
            pi.setScalarValue(errorScalar, new Float(errors[i]));
        }
        
        ProjectionUtil.exportScalars(model, filename);
    }

    public void input(
            @Param(name = "2D projection") AbstractMatrix projection,
            @Param(name = "original data") AbstractMatrix originalData) {
        this.projection = projection;
        this.originalData = originalData;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return new AggregatedDistanceErrorParamView(this);
    }

    @Override
    public void reset() {
  
    }
    
    private Double[] computeAggregatedDistanceError(
            DistanceMatrix origDmat, DistanceMatrix projDmat, boolean norm) {
        
        double maxrn = origDmat.getMaxDistance(), maxr2 = projDmat.getMaxDistance();
        
        double maxError = Double.MIN_VALUE;
        
        Double[] errors = new Double[origDmat.getElementCount()];
        for (int i = 0; i < errors.length; i++) {
            errors[i] = 0.0;
            for (int j = 0; j < errors.length; j++) {
                double distrn = origDmat.getDistance(i, j) / maxrn;
                double distr2 = projDmat.getDistance(i, j) / maxr2;                
                errors[i] += Math.abs(distrn - distr2);
            }
            
            if(errors[i] > maxError)
                maxError = errors[i];            
        }
        
        if (norm && maxError > 0.0)
            for(int i = 0; i < errors.length; ++i) {
                errors[i] = errors[i] / maxError;
            }
        
        return errors;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    
    public static final long serialVersionUID = 1L;    
   
}
