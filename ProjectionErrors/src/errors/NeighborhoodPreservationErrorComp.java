/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package errors;


import datamining.neighbors.KNN;
import datamining.neighbors.Pair;
import distance.DistanceMatrix;
import distance.dissimilarity.Euclidean;
import java.io.IOException;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;
import projection.model.Scalar;
import projection.util.ProjectionUtil;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.interfaces.AbstractComponent;
import vispipelinebasics.interfaces.AbstractParametersView;


/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Errors",
name = "Neighborhood Preservation Error",
description = "Create a scalar with the neighborhood-preservation-based error.")
public class NeighborhoodPreservationErrorComp implements AbstractComponent {

    private String filename;
    private int k = 0;
    private transient AbstractMatrix projection, originalData;
            
    @Override
    public void execute() throws IOException {
        Euclidean euclid = new Euclidean();
        DistanceMatrix projDmat = new DistanceMatrix(projection, euclid);
        DistanceMatrix origDmat = new DistanceMatrix(originalData, euclid);
        
        if (k <= 0 || k >= origDmat.getElementCount())
            k = origDmat.getElementCount() / 10;
        
        Double[] errors = computeAggregatedDistanceError(k, origDmat, projDmat, false);
        
        ProjectionModel model = new ProjectionModel();
        Scalar errorScalar = model.addScalar("Neigh.-Pres. Error (" + k + ")");

        int nrows = projection.getRowCount();

        for (int i = 0; i < nrows; i++) {
            AbstractVector row = projection.getRow(i);            
            ProjectionInstance pi = new ProjectionInstance(row.getId(),
                    row.getValue(0), row.getValue(1));
            model.addInstance(pi);
            pi.setScalarValue(errorScalar, new Float(errors[i]));
        }
        
        ProjectionUtil.exportScalars(model, filename);
    }

    public void input(
            @Param(name = "2D projection") AbstractMatrix projection,
            @Param(name = "original data") AbstractMatrix originalData) {
        this.projection = projection;
        this.originalData = originalData;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return new NeighborhoodPreservationErrorParamView(this);
    }

    @Override
    public void reset() {
  
    }
    
    private Double[] computeAggregatedDistanceError(
            int k, DistanceMatrix origDmat, DistanceMatrix projDmat, boolean norm) throws IOException {
        
        double maxrn = origDmat.getMaxDistance(), maxr2 = projDmat.getMaxDistance();
        
        double maxError = Double.MIN_VALUE;
        
        Double[] errors = new Double[origDmat.getElementCount()];
        
        KNN knn = new KNN(k);
        Pair[][] neighborsOrigDistance = knn.execute(origDmat);
        Pair[][] neighbors3DDistance = knn.execute(projDmat);
        
        for (int i = 0; i < origDmat.getElementCount(); i++) {
            int cont = 0;
            for (int j = 0; j < k; j++){
                for (int m = 0; m < k; m++) {
                  if(neighborsOrigDistance[i][j].index == neighbors3DDistance[i][m].index){
                        cont++;
                    }
                    
                }
            }
            errors[i] = 1.0 - ((double)cont/(2*(k-cont) + cont));            
        }
        
        if (norm && maxError > 0.0)
            for(int i = 0; i < errors.length; ++i) {
                errors[i] = errors[i] / maxError;
            }
        
        return errors;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }
    
    public static final long serialVersionUID = 1L;    
   
}
