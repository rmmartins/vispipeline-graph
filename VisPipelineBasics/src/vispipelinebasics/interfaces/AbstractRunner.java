/*
 * The MIT License
 *
 * Copyright 2018 Rafael M. Martins <rafael.martins@lnu.se>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package vispipelinebasics.interfaces;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael M. Martins <rafael.martins@lnu.se>
 */
public abstract class AbstractRunner {
    
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Param {
        String desc() default "[undefined]";
        int pos() default 0;
    }
    
    public void run(String[] args) throws IOException {
        if (args.length < 1 || "--help".equals(args[0]) || "-h".equals(args[0])) {
            
            System.out.println(getName());
            System.out.println("---------------");

            // Gather parameters
            List<Field> parameters = new ArrayList<>();
            for (Field f : this.getClass().getDeclaredFields()) {
                if (f.getAnnotation(Param.class) != null) {
                    parameters.add(f);
                }
            }

            String defaultValues = "";

            if (parameters.size() > 0) {
                System.out.println("Parameters:");
                for (int i = 0; i < parameters.size(); ++i) {                
                    try {
                        Field f = parameters.get(i);
                        Param p = f.getAnnotation(Param.class);
    //                    String name = f.getName();
                        String name = p.desc();
                        Object value = f.get(this).toString();                    
                        Type type = f.getGenericType();                    
                        if(type instanceof Class && ((Class)type).isEnum()) {
                            List<String> values = new ArrayList<>();
                            for (Object v : ((Class)type).getEnumConstants()) {
                                values.add(v.toString());
                            }
                            value = values.indexOf(value);
                            defaultValues += value + " ";
                            String typeStr = ((Class)type).getSimpleName();
                            System.out.println((i + 1) + ". " + name + " (" + typeStr + ", default: " + value + ")");                        
                            for (int j = 0; j < values.size(); ++j) {
                                System.out.println("    " + j + ". " + values.get(j));
                            }
                        } else {
                            defaultValues += value + " ";
                            System.out.println((i + 1) + ". " + name + " (" + type + ", default: " + value + ")");
                        }
                    } catch (IllegalArgumentException | IllegalAccessException ex) {
                        Logger.getLogger(AbstractRunner.class.getName()).log(Level.SEVERE, null, ex);
                    }                
                }
            }

            System.out.println("---------------");
            System.out.println("Example: vp-run " + getShortName() + " filename " + defaultValues);
            
            return;
        }
        
        // Gather parameters
        List<Field> parameters = new ArrayList<>();
        for (Field f : this.getClass().getDeclaredFields()) {
            if (f.getAnnotation(Param.class) != null) {
                parameters.add(f);
            }
        }
        
        // Order them by the user-defined positions. Although the JVM should
        // return them in declaration order, it's not 100% certain.
        parameters.sort(new Comparator<Field>() {
            @Override
            public int compare(Field o1, Field o2) {
                return o1.getAnnotation(Param.class).pos() - o2.getAnnotation(Param.class).pos();
            }
        });
        
        // Process command-line arguments                
        for (int i = 1; i < args.length; ++i) {
            Field p = parameters.get(i - 1);
            try {
                if (p.getType() == Float.class || p.getType() == float.class) {
                    p.set(this, Float.parseFloat(args[i]));                
                }
                if (p.getType() == Integer.class || p.getType() == int.class) {
                    p.set(this, Integer.parseInt(args[i]));                
                }
                if (p.getType() instanceof Class && ((Class)p.getType()).isEnum()) {
                    int index = Integer.parseInt(args[i]);
                    Object value = ((Class)p.getType()).getEnumConstants()[index];
                    p.set(this, value);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(AbstractRunner.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        // Get files
        // TODO Let different runners accept different file extensions
        File fileOrDir = new File(args[0]);
        if (fileOrDir.isDirectory()) {
            dir = fileOrDir;
            files = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".data")
                            || name.toLowerCase().endsWith(".bin");
                }
            });
        } else {
            dir = fileOrDir.getParentFile();
            files = new File[]{ fileOrDir };
        }
        
    }
    
    public abstract String getName();
    
    public abstract String getShortName();
    
    protected File dir;
    
    protected File[] files = new File[0];
    
}
