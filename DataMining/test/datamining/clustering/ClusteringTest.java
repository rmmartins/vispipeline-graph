/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datamining.clustering;

import distance.dissimilarity.AbstractDissimilarity;
import distance.dissimilarity.CosineBased;
import matrix.AbstractMatrix;
import matrix.dense.DenseMatrix;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author rmartins
 */
public class ClusteringTest {
    
    @Before
    public void setup() throws Exception {
        String path = this.getClass().getResource("../projection.txt").getPath();
        matrix = new DenseMatrix();
        matrix.load(path);
        diss = new CosineBased();
    }

    @Test
    public void testExecute() throws Exception {
        Clustering clust = new Kmeans(10);
        Object res = clust.execute(diss, matrix);
        System.out.println(res.toString());
    }
    
    AbstractMatrix matrix;
    AbstractDissimilarity diss;
}
