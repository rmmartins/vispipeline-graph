/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package datamining.normalization;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vispipelinebasics.annotations.VisComponent;
import vispipelinebasics.annotations.Param;
import vispipelinebasics.interfaces.AbstractParametersView;
import vispipelinebasics.interfaces.AbstractComponent;
import matrix.AbstractMatrix;
import matrix.AbstractVector;
import matrix.dense.DenseVector;

/**
 *
 * @author Rafael M. Martins
 */

@VisComponent(hierarchy = "Transformation",
name = "Log Transformation",
description = "Transform the data to log(x+1). " +
    "If it is a Dense matrix, the original matrix will be modified.")

public class LogTransformationComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        try {
            output = (AbstractMatrix)input.clone();
            for (AbstractVector v : output.getRows()) {
                float[] values = ((DenseVector)v).getValues();
                for (int i = 0; i < values.length; ++i) {
                    Float f = ((Double)Math.log(values[i] + 1.0f)).floatValue();
                    if (f.isNaN() || f.isInfinite())
                        throw new IOException("Data cannot be log-transformed.");
                    else
                        values[i] = f;
                }
            }
        } catch (CloneNotSupportedException ex) {        
            Logger.getLogger(LogTransformationComp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void input(@Param(name = "points matrix") AbstractMatrix input) {
        this.input = input;
    }

    public AbstractMatrix output() {
        return output;
    }

    @Override
    public AbstractParametersView getParametersEditor() {        
        return null;
    }

    @Override
    public void reset() {
        input = null;
        output = null;
    }

    public static final long serialVersionUID = 1L;
    
    private transient AbstractMatrix input;
    private transient AbstractMatrix output;
}
