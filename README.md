# README #

VisPipeline is an interactive platform for building data-processing pipelines with the ultimate goal of building visualizations.

#### Disclaimer

VisPipeline was originally created by [Dr. Fernando Paulovich](https://sites.google.com/site/fpaulovich/) and was mainly developed throughout the years as a group effort in the [Visualization and Computer Graphics (VICG)](http://vicg.icmc.usp.br/vicg) research group of the University of São Paulo.

## Setup ###

VisPipeline was always developed on top of [NetBeans](https://netbeans.org/), so that is probably the easiest way to go. You might be able to compile/run it without NetBeans if you really wanted to, since it mainly relies on ant build scripts, but you are on your own if you decide to go that way.

Each of the folders in the source code is a NetBeans project which depends on some of the other projects in order to function. The main one is *VisPipeline*; by importing only this project (and its required projects, as indicated by the NetBeans interface), you should be able to compile and run a very basic version of VisPipeline. More components will be added to the interface if you import and build more projects.


